/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api;

public class Tuple<A, B> {
    private A a;
    private B b;

    private Tuple(A aIn, B bIn) {
        this.a = aIn;
        this.b = bIn;
    }

    public A getFirst() {
        return this.a;
    }

    public B getSecond() {
        return this.b;
    }

    public static <A, B> Tuple<A, B> of(A aIn, B bIn) {
        return new Tuple<A, B>(aIn, bIn);
    }
}

