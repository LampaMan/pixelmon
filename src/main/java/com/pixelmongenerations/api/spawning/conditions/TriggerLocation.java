/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.spawning.conditions;

import com.pixelmongenerations.api.spawning.conditions.LocationType;

public class TriggerLocation
extends LocationType {
    public TriggerLocation(String name) {
        super(name);
        this.setBaseBlockCondition(type -> false);
        this.setSurroundingBlockCondition(type -> false);
    }
}

