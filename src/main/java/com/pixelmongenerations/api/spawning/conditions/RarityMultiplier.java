/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.spawning.conditions;

import com.pixelmongenerations.api.spawning.SpawnInfo;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.conditions.SpawnCondition;

public class RarityMultiplier {
    public float multiplier = 1.0f;
    public SpawnCondition condition = null;
    public SpawnCondition anticondition = null;

    public float apply(SpawnInfo spawnInfo, SpawnLocation spawnLocation, float rarity) {
        if ((this.condition == null || this.condition.fits(spawnInfo, spawnLocation)) && (this.anticondition == null || this.anticondition.fits(spawnInfo, spawnLocation))) {
            return rarity * this.multiplier;
        }
        return rarity;
    }
}

