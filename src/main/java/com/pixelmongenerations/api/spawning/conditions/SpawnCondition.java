/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.spawning.conditions;

import com.pixelmongenerations.api.spawning.SpawnInfo;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.conditions.WorldTime;
import com.pixelmongenerations.api.world.WeatherType;
import com.pixelmongenerations.core.config.BetterSpawnerConfig;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import net.minecraft.block.Block;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.WorldType;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class SpawnCondition {
    public static Class<? extends SpawnCondition> targetedSpawnCondition = SpawnCondition.class;
    public transient ArrayList<Biome> biomes = new ArrayList();
    public ArrayList<WorldTime> times = null;
    public ArrayList<WeatherType> weathers = null;
    public ArrayList<String> baseBlocks = null;
    public ArrayList<String> neededNearbyBlocks = null;
    private ArrayList<String> stringBiomes = null;
    public String tag = null;
    public Boolean seesSky = null;
    public Biome.TempCategory temperature = null;
    public ArrayList<Integer> dimensions = null;
    public ArrayList<String> worlds = null;
    public Integer minX = null;
    public Integer maxX = null;
    public Integer minY = null;
    public Integer maxY = null;
    public Integer minZ = null;
    public Integer maxZ = null;
    public Integer moonPhase = null;
    public Integer minLightLevel = null;
    public Integer maxLightLevel = null;

    public void onExport() {
        this.stringBiomes = new ArrayList();
        for (Biome biome : this.biomes) {
            if (biome == null || biome.getRegistryName() == null || biome.getRegistryName().getPath() == null) {
                System.out.println(biome);
                System.out.println(biome.getRegistryName());
                System.out.println(biome.getRegistryName().getPath());
            }
            this.stringBiomes.add(biome.getRegistryName().getPath());
        }
    }

    public void onImport() {
        if (this.stringBiomes == null) {
            this.stringBiomes = new ArrayList();
        }
        for (String stringBiome : this.stringBiomes) {
            if (stringBiome == null) continue;
            if (BetterSpawnerConfig.INSTANCE.biomeCategories.containsKey(stringBiome)) {
                for (String categoryBiome : BetterSpawnerConfig.INSTANCE.biomeCategories.get(stringBiome)) {
                    Set<Map.Entry<ResourceLocation, Biome>> list = GameRegistry.findRegistry(Biome.class).getEntries();
                    for (Map.Entry<ResourceLocation, Biome> biomeEntry : list) {
                        if (categoryBiome.contains(":")) {
                            if (!biomeEntry.getKey().toString().equalsIgnoreCase(categoryBiome) || this.biomes.contains(biomeEntry.getValue())) continue;
                            this.biomes.add(biomeEntry.getValue());
                            continue;
                        }
                        if (!biomeEntry.getKey().getPath().equalsIgnoreCase(categoryBiome) || this.biomes.contains(biomeEntry.getValue())) continue;
                        this.biomes.add(biomeEntry.getValue());
                    }
                }
                continue;
            }
            Set<Map.Entry<ResourceLocation, Biome>> list = GameRegistry.findRegistry(Biome.class).getEntries();
            for (Map.Entry<ResourceLocation, Biome> biomeEntry : list) {
                if (stringBiome.contains(":")) {
                    if (!biomeEntry.getKey().toString().equalsIgnoreCase(stringBiome) || this.biomes.contains(biomeEntry.getValue())) continue;
                    this.biomes.add(biomeEntry.getValue());
                    continue;
                }
                if (!biomeEntry.getKey().getPath().equalsIgnoreCase(stringBiome) || this.biomes.contains(biomeEntry.getValue())) continue;
                this.biomes.add(biomeEntry.getValue());
            }
        }
    }

    public boolean fits(SpawnInfo spawnInfo, SpawnLocation spawnLocation) {
        int ticks = (int)(spawnLocation.location.world.getWorldTime() % 24000L);
        boolean timeFits = false;
        timeFits = this.times != null && !this.times.isEmpty() ? WorldTime.getCurrent(ticks).stream().anyMatch(this.times::contains) : true;
        if (!timeFits) {
            return false;
        }
        if (this.tag != null && !this.tag.equalsIgnoreCase(spawnInfo.tag)) {
            return false;
        }
        if (this.seesSky != null && this.seesSky != spawnLocation.seesSky) {
            return false;
        }
        if (this.temperature != null && this.temperature != spawnLocation.biome.getTempCategory()) {
            return false;
        }
        if (this.dimensions != null && !this.dimensions.isEmpty() && !this.dimensions.contains(spawnLocation.location.world.provider.getDimension())) {
            return false;
        }
        if (this.worlds != null && !this.worlds.isEmpty() && !this.worlds.contains(spawnLocation.location.world.getWorldInfo().getWorldName())) {
            return false;
        }
        if (this.minX != null && spawnLocation.location.pos.getX() < this.minX) {
            return false;
        }
        if (this.maxX != null && spawnLocation.location.pos.getX() > this.maxX) {
            return false;
        }
        if (this.minY != null && spawnLocation.location.pos.getY() < this.minY) {
            return false;
        }
        if (this.maxY != null && spawnLocation.location.pos.getY() > this.maxY) {
            return false;
        }
        if (this.minZ != null && spawnLocation.location.pos.getZ() < this.minZ) {
            return false;
        }
        if (this.maxZ != null && spawnLocation.location.pos.getZ() > this.maxZ) {
            return false;
        }
        if (this.weathers != null && !this.weathers.isEmpty() && !this.weathers.contains((Object)WeatherType.get(spawnLocation.location.world))) {
            return false;
        }
        if (spawnLocation.location.world.getWorldType() != WorldType.FLAT && this.biomes != null && !this.biomes.isEmpty() && !this.biomes.contains(spawnLocation.biome)) {
            return false;
        }
        if (this.moonPhase != null && spawnLocation.location.world.provider.getMoonPhase(spawnLocation.location.world.getWorldTime()) != this.moonPhase.intValue()) {
            return false;
        }
        if (this.minLightLevel != null && spawnLocation.location.world.getLight(spawnLocation.location.pos) < this.minLightLevel) {
            return false;
        }
        if (this.maxLightLevel != null && spawnLocation.location.world.getLight(spawnLocation.location.pos) > this.maxLightLevel) {
            return false;
        }
        if (this.baseBlocks != null && !this.baseBlocks.isEmpty()) {
            boolean confirmedBaseBlock = false;
            String blockID = spawnLocation.baseBlock.getRegistryName().toString();
            Iterator<String> var8 = this.baseBlocks.iterator();
            while (true) {
                if (!var8.hasNext()) {
                    if (confirmedBaseBlock) break;
                    return false;
                }
                String neededBaseBlock = var8.next();
                if (!neededBaseBlock.equalsIgnoreCase(blockID) && !BetterSpawnerConfig.getBlockCategory(neededBaseBlock).contains(blockID)) continue;
                confirmedBaseBlock = true;
            }
        }
        if (this.neededNearbyBlocks != null && !this.neededNearbyBlocks.isEmpty()) {
            for (String blockID : this.neededNearbyBlocks) {
                ArrayList<String> needed = new ArrayList<String>(BetterSpawnerConfig.getBlockCategory(blockID));
                if (needed.isEmpty()) {
                    needed.add(blockID);
                }
                for (String neededBlock : needed) {
                    boolean has = false;
                    for (Block uniqueBlock : spawnLocation.uniqueSurroundingBlocks) {
                        if (!neededBlock.equalsIgnoreCase(uniqueBlock.getRegistryName().toString())) continue;
                        has = true;
                    }
                    if (has) continue;
                    return false;
                }
            }
        }
        return true;
    }
}

