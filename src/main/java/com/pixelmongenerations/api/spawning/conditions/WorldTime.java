/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.apache.commons.lang3.StringUtils
 */
package com.pixelmongenerations.api.spawning.conditions;

import java.util.ArrayList;
import java.util.function.Predicate;
import org.apache.commons.lang3.StringUtils;

public enum WorldTime {
    DAWN(tick -> tick >= 22500 || tick <= 300),
    MORNING(tick -> tick <= 6000 || tick >= 22550),
    DAY(tick -> tick <= 12000),
    MIDDAY(tick -> tick >= 5500 && tick <= 6500),
    AFTERNOON(tick -> tick >= 6000 && tick <= 12000),
    DUSK(tick -> tick >= 12000 && tick <= 13800),
    NIGHT(tick -> tick >= 13450 && tick <= 22550),
    MIDNIGHT(tick -> tick >= 17500 && tick <= 18500);

    public Predicate<Integer> tickCondition;

    private WorldTime(Predicate<Integer> tickCondition) {
        this.tickCondition = tickCondition;
    }

    public static ArrayList<WorldTime> getCurrent(int tick) {
        ArrayList<WorldTime> current = new ArrayList<WorldTime>();
        for (WorldTime time : WorldTime.values()) {
            if (!time.tickCondition.test(tick)) continue;
            current.add(time);
        }
        return current;
    }

    public String getDisplayName() {
        return StringUtils.capitalize((String)this.name().toLowerCase());
    }
}

