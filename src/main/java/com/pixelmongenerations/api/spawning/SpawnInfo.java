/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.Gson
 *  com.google.gson.GsonBuilder
 */
package com.pixelmongenerations.api.spawning;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixelmongenerations.api.spawning.AbstractSpawner;
import com.pixelmongenerations.api.spawning.CompositeSpawnCondition;
import com.pixelmongenerations.api.spawning.SpawnAction;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.SpawnSet;
import com.pixelmongenerations.api.spawning.archetypes.entities.SpawnInfoEntity;
import com.pixelmongenerations.api.spawning.archetypes.entities.items.SpawnInfoItem;
import com.pixelmongenerations.api.spawning.archetypes.entities.npcs.SpawnInfoNPC;
import com.pixelmongenerations.api.spawning.archetypes.entities.pokemon.SpawnInfoPokemon;
import com.pixelmongenerations.api.spawning.conditions.LocationType;
import com.pixelmongenerations.api.spawning.conditions.RarityMultiplier;
import com.pixelmongenerations.api.spawning.conditions.SpawnCondition;
import com.pixelmongenerations.api.spawning.conditions.WorldTime;
import com.pixelmongenerations.api.spawning.util.SpawnConditionTypeAdapter;
import com.pixelmongenerations.api.spawning.util.SpawnInfoTypeAdapter;
import com.pixelmongenerations.api.world.WeatherType;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.BetterSpawnerConfig;
import java.util.ArrayList;
import java.util.HashMap;
import net.minecraft.entity.Entity;
import net.minecraft.init.Biomes;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.fml.common.registry.GameRegistry;

public abstract class SpawnInfo {
    public static final Gson GSON = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(SpawnInfo.class, (Object)new SpawnInfoTypeAdapter()).registerTypeAdapter(SpawnCondition.class, (Object)new SpawnConditionTypeAdapter()).create();
    public static HashMap<String, Class<? extends SpawnInfo>> spawnInfoTypes = new HashMap();
    public String typeID;
    public String interval = null;
    public String tag = "default";
    public SpawnCondition condition = new SpawnCondition();
    public SpawnCondition anticondition = null;
    public CompositeSpawnCondition compositeCondition = null;
    public ArrayList<RarityMultiplier> rarityMultipliers = null;
    public int requiredSpace = -1;
    public transient SpawnSet set;
    public float rarity = 50.0f;
    public transient ArrayList<LocationType> locationTypes = new ArrayList();
    public ArrayList<String> stringLocationTypes = new ArrayList();

    public SpawnInfo(String typeID) {
        this.typeID = typeID;
    }

    public abstract SpawnAction<? extends Entity> construct(AbstractSpawner var1, SpawnLocation var2);

    public boolean fits(SpawnLocation spawnLocation) {
        if (spawnLocation.radius < this.requiredSpace) {
            return false;
        }
        boolean locationFits = false;
        for (LocationType locationType : this.locationTypes) {
            if (!spawnLocation.types.contains(locationType)) continue;
            locationFits = true;
        }
        if (!locationFits) {
            return false;
        }
        if (this.condition != null && !this.condition.fits(this, spawnLocation)) {
            return false;
        }
        if (this.anticondition != null && this.anticondition.fits(this, spawnLocation)) {
            return false;
        }
        if (this.compositeCondition != null && !this.compositeCondition.fits(this, spawnLocation)) {
            return false;
        }
        return BetterSpawnerConfig.INSTANCE.globalCompositeCondition == null || BetterSpawnerConfig.INSTANCE.globalCompositeCondition.fits(this, spawnLocation);
    }

    public void onExport() {
        if (this.condition != null) {
            this.condition.onExport();
        }
        if (this.anticondition != null) {
            this.anticondition.onExport();
        }
    }

    public void onImport() {
        if (this.requiredSpace == -1) {
            this.requiredSpace = 1;
        }
        if (this.condition != null) {
            this.condition.onImport();
        }
        if (this.anticondition != null) {
            this.anticondition.onImport();
        }
        if (this.stringLocationTypes != null && !this.stringLocationTypes.isEmpty()) {
            for (String locationTypeString : this.stringLocationTypes) {
                for (LocationType locationType : LocationType.locationTypes) {
                    if (!locationType.name.equalsIgnoreCase(locationTypeString)) continue;
                    this.locationTypes.add(locationType);
                }
            }
        }
        if (this.locationTypes.isEmpty()) {
            Pixelmon.LOGGER.warn("A SpawnInfo in set: " + this.set.id + " has no LocationTypes. Was this intentional?");
        }
    }

    public float getAdjustedRarity(SpawnLocation spawnLocation) {
        float rarity = this.rarity;
        if (this.rarityMultipliers != null && !this.rarityMultipliers.isEmpty()) {
            for (RarityMultiplier rarityMultiplier : this.rarityMultipliers) {
                rarity = rarityMultiplier.apply(this, spawnLocation, rarity);
            }
        }
        return rarity;
    }

    public abstract String toString();

    public float calculateNominalRarity() {
        if (this.rarity <= 0.0f) {
            return 0.0f;
        }
        float biomeMultiplier = 1.0f;
        if (this.condition.biomes != null && !this.condition.biomes.isEmpty()) {
            biomeMultiplier = (float)this.condition.biomes.size() / (float)GameRegistry.findRegistry(Biome.class).getEntries().size();
            if (this.condition.biomes.contains(Biomes.OCEAN) || this.condition.biomes.contains(Biomes.RIVER)) {
                biomeMultiplier = 1.0f;
            } else if (this.condition.biomes.contains(Biomes.DEEP_OCEAN)) {
                biomeMultiplier = 0.5f;
            }
        }
        float timeMultiplier = 0.0f;
        if (this.condition.times == null || this.condition.times.isEmpty()) {
            timeMultiplier = 1.0f;
        } else {
            if (this.condition.times.contains((Object)WorldTime.DAY)) {
                timeMultiplier += 0.75f;
            }
            if (this.condition.times.contains((Object)WorldTime.NIGHT)) {
                timeMultiplier += 0.75f;
            }
            if (!this.condition.times.contains((Object)WorldTime.DAY) && !this.condition.times.contains((Object)WorldTime.NIGHT)) {
                if (this.condition.times.contains((Object)WorldTime.AFTERNOON) || this.condition.times.contains((Object)WorldTime.MORNING)) {
                    timeMultiplier = 0.5f;
                } else if (this.condition.times.contains((Object)WorldTime.DAWN) || this.condition.times.contains((Object)WorldTime.DUSK)) {
                    timeMultiplier = 0.3f;
                } else if (this.condition.times.contains((Object)WorldTime.MIDNIGHT) || this.condition.times.contains((Object)WorldTime.MIDDAY)) {
                    timeMultiplier = 0.1f;
                } else {
                    return 0.0f;
                }
            }
        }
        if (timeMultiplier > 1.0f) {
            timeMultiplier = 1.0f;
        }
        float weatherMultiplier = 0.0f;
        if (this.condition.weathers == null || this.condition.weathers.isEmpty()) {
            weatherMultiplier = 1.0f;
        } else {
            if (this.condition.weathers.contains((Object)WeatherType.CLEAR)) {
                weatherMultiplier += 0.7f;
            }
            if (this.condition.weathers.contains((Object)WeatherType.RAIN)) {
                weatherMultiplier += 0.3f;
            }
            if (this.condition.weathers.contains((Object)WeatherType.STORM)) {
                weatherMultiplier += 0.1f;
            }
            if (weatherMultiplier > 1.0f) {
                weatherMultiplier = 1.0f;
            }
        }
        return this.rarity * weatherMultiplier * timeMultiplier * (0.5f + biomeMultiplier / 2.0f);
    }

    static {
        spawnInfoTypes.put("pokemon", SpawnInfoPokemon.class);
        spawnInfoTypes.put("npc", SpawnInfoNPC.class);
        spawnInfoTypes.put("item", SpawnInfoItem.class);
        spawnInfoTypes.put("entity", SpawnInfoEntity.class);
    }
}

