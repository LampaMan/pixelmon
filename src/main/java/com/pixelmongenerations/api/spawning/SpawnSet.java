/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.Gson
 *  com.google.gson.GsonBuilder
 */
package com.pixelmongenerations.api.spawning;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixelmongenerations.api.spawning.AbstractSpawner;
import com.pixelmongenerations.api.spawning.SpawnInfo;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.conditions.SpawnCondition;
import com.pixelmongenerations.api.spawning.util.SetLoader;
import com.pixelmongenerations.api.spawning.util.SpawnConditionTypeAdapter;
import com.pixelmongenerations.api.spawning.util.SpawnInfoTypeAdapter;
import com.pixelmongenerations.api.spawning.util.SpawnSetTypeAdapter;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.ArrayList;

public class SpawnSet {
    private static Gson GSON = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(SpawnSet.class, (Object)new SpawnSetTypeAdapter()).registerTypeAdapter(SpawnInfo.class, (Object)new SpawnInfoTypeAdapter()).registerTypeAdapter(SpawnCondition.class, (Object)new SpawnConditionTypeAdapter()).create();
    public String id;
    public Float setSpecificShinyRate = null;
    public ArrayList<SpawnInfo> spawnInfos = new ArrayList();

    public void export(String dir) {
        if (!dir.endsWith("/")) {
            dir = dir + "/";
        }
        try {
            File file = new File(dir);
            file.mkdirs();
            file = new File(dir + this.id + ".set.json");
            if (!file.exists()) {
                file.createNewFile();
            }
            PrintWriter pw = new PrintWriter(file);
            String json = GSON.toJson((Object)this);
            pw.write(json);
            pw.flush();
            pw.close();
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void onImport() {
        for (SpawnInfo spawnInfo : this.spawnInfos) {
            spawnInfo.set = this;
            spawnInfo.onImport();
        }
    }

    public ArrayList<SpawnInfo> suitableSpawnsFor(AbstractSpawner spawner, SpawnLocation spawnLocation) {
        ArrayList<SpawnInfo> suitableSpawns = new ArrayList<SpawnInfo>();
        for (SpawnInfo spawnInfo : this.spawnInfos) {
            if (spawnInfo.rarity == 0.0f || !spawnInfo.fits(spawnLocation)) continue;
            suitableSpawns.add(spawnInfo);
        }
        return suitableSpawns;
    }

    public static SpawnSet deserialize(Reader reader) {
        return SpawnSet.deserialize(reader, SetLoader.targetedSpawnSetClass);
    }

    public static SpawnSet deserialize(Reader reader, Class<? extends SpawnSet> targetedSpawnSetClass) {
        return (SpawnSet)GSON.fromJson(reader, targetedSpawnSetClass);
    }

    public static SpawnSet deserialize(String contents) {
        return SpawnSet.deserialize(contents, SetLoader.targetedSpawnSetClass);
    }

    public static SpawnSet deserialize(String contents, Class<? extends SpawnSet> targetedSpawnSetClass) {
        return (SpawnSet)GSON.fromJson(contents, targetedSpawnSetClass);
    }
}

