/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.spawning;

import com.pixelmongenerations.api.spawning.conditions.LocationType;
import com.pixelmongenerations.api.world.MutableLocation;
import java.util.ArrayList;
import net.minecraft.block.Block;
import net.minecraft.world.biome.Biome;

public class SpawnLocation {
    public MutableLocation location;
    public ArrayList<LocationType> types;
    public Block baseBlock;
    public ArrayList<Block> uniqueSurroundingBlocks;
    public Biome biome;
    public boolean seesSky;
    public int radius;

    public SpawnLocation(MutableLocation location, ArrayList<LocationType> types, Block baseBlock, ArrayList<Block> uniqueSurroundingBlocks, Biome biome, boolean seesSky, int radius) {
        this.location = location;
        this.types = types;
        this.baseBlock = baseBlock;
        this.uniqueSurroundingBlocks = uniqueSurroundingBlocks;
        this.biome = biome;
        this.seesSky = seesSky;
        this.radius = radius;
    }
}

