/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.spawning;

import com.pixelmongenerations.api.spawning.SpawnInfo;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.conditions.SpawnCondition;
import java.util.ArrayList;

public class CompositeSpawnCondition {
    public ArrayList<SpawnCondition> conditions = new ArrayList();
    public ArrayList<SpawnCondition> anticonditions = new ArrayList();

    public CompositeSpawnCondition() {
    }

    public CompositeSpawnCondition(ArrayList<SpawnCondition> conditions, ArrayList<SpawnCondition> anticonditions) {
        this.conditions = conditions;
        this.anticonditions = anticonditions;
    }

    public boolean fits(SpawnInfo spawnInfo, SpawnLocation spawnLocation) {
        boolean fitsNormal = false;
        if (this.conditions == null || this.conditions.isEmpty()) {
            fitsNormal = true;
        } else {
            for (SpawnCondition condition : this.conditions) {
                if (!condition.fits(spawnInfo, spawnLocation)) continue;
                fitsNormal = true;
            }
        }
        boolean fitsAnti = false;
        if (this.anticonditions != null && !this.anticonditions.isEmpty()) {
            for (SpawnCondition anticondition : this.anticonditions) {
                if (!anticondition.fits(spawnInfo, spawnLocation)) continue;
                fitsAnti = true;
            }
        }
        return fitsNormal && !fitsAnti;
    }
}

