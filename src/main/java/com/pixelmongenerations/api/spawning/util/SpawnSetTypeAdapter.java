/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.JsonDeserializationContext
 *  com.google.gson.JsonDeserializer
 *  com.google.gson.JsonElement
 *  com.google.gson.JsonObject
 *  com.google.gson.JsonParseException
 */
package com.pixelmongenerations.api.spawning.util;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.pixelmongenerations.api.spawning.SpawnInfo;
import com.pixelmongenerations.api.spawning.SpawnSet;
import java.lang.reflect.Type;

public class SpawnSetTypeAdapter
implements JsonDeserializer<SpawnSet> {
    public SpawnSet deserialize(JsonElement element, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        try {
            SpawnSet spawnSet = (SpawnSet)SpawnInfo.GSON.fromJson((JsonElement)((JsonObject)element), SpawnSet.class);
            spawnSet.onImport();
            return spawnSet;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

