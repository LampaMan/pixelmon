/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.Gson
 *  com.google.gson.GsonBuilder
 *  com.google.gson.JsonDeserializationContext
 *  com.google.gson.JsonDeserializer
 *  com.google.gson.JsonElement
 *  com.google.gson.JsonObject
 *  com.google.gson.JsonParseException
 */
package com.pixelmongenerations.api.spawning.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.pixelmongenerations.api.spawning.conditions.SpawnCondition;
import java.lang.reflect.Type;

public class SpawnConditionTypeAdapter
implements JsonDeserializer<SpawnCondition> {
    public static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public SpawnCondition deserialize(JsonElement element, Type type, JsonDeserializationContext ctx) throws JsonParseException {
        JsonObject obj = (JsonObject)element;
        try {
            return (SpawnCondition)gson.fromJson((JsonElement)obj, SpawnCondition.targetedSpawnCondition);
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

