/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.JsonDeserializationContext
 *  com.google.gson.JsonDeserializer
 *  com.google.gson.JsonElement
 *  com.google.gson.JsonObject
 *  com.google.gson.JsonParseException
 *  com.google.gson.JsonSerializationContext
 *  com.google.gson.JsonSerializer
 */
package com.pixelmongenerations.api.spawning.util;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.pixelmongenerations.api.spawning.SpawnInfo;
import java.lang.reflect.Type;

public class SpawnInfoTypeAdapter
implements JsonSerializer<SpawnInfo>,
JsonDeserializer<SpawnInfo> {
    public SpawnInfo deserialize(JsonElement element, Type type, JsonDeserializationContext ctx) throws JsonParseException {
        JsonObject obj = (JsonObject)element;
        try {
            return (SpawnInfo)SpawnInfo.GSON.fromJson((JsonElement)obj, SpawnInfo.spawnInfoTypes.get(obj.get("typeID").getAsString()));
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public JsonElement serialize(SpawnInfo spawnInfo, Type type, JsonSerializationContext ctx) {
        spawnInfo.onExport();
        return ctx.serialize((Object)SpawnInfo.spawnInfoTypes.get(spawnInfo.typeID).cast(spawnInfo));
    }
}

