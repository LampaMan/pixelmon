/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.api.spawning;

import com.pixelmongenerations.api.events.spawning.SpawnEvent;
import com.pixelmongenerations.api.spawning.AbstractSpawner;
import com.pixelmongenerations.api.spawning.SpawnInfo;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.core.config.BetterSpawnerConfig;
import javax.annotation.Nullable;
import net.minecraft.entity.Entity;
import net.minecraftforge.common.MinecraftForge;

public abstract class SpawnAction<T extends Entity> {
    public SpawnInfo spawnInfo;
    public SpawnLocation spawnLocation;
    protected T entity;

    public SpawnAction(SpawnInfo spawnInfo, SpawnLocation spawnLocation) {
        this.spawnInfo = spawnInfo;
        this.spawnLocation = spawnLocation;
    }

    protected abstract T createEntity();

    public T getOrCreateEntity() {
        if (this.entity == null) {
            this.entity = this.createEntity();
        }
        return this.entity;
    }

    public void setEntity(T t) {
        if (t != null) {
            this.entity = t;
        }
    }

    @Nullable
    public T doSpawn(AbstractSpawner spawner) {
        this.getOrCreateEntity();
        if (this.spawnInfo.interval != null && !BetterSpawnerConfig.checkInterval(this.spawnInfo.interval)) {
            return null;
        }
        SpawnEvent spawnEvent = new SpawnEvent(spawner, this);
        if (MinecraftForge.EVENT_BUS.post(spawnEvent)) {
            return null;
        }
        ((Entity)this.entity).setPosition((double)this.spawnLocation.location.pos.getX() + 0.5, this.spawnLocation.location.pos.getY(), (double)this.spawnLocation.location.pos.getZ() + 0.5);
        this.spawnLocation.location.world.spawnEntity((Entity)this.entity);
        spawner.spawned.add(((Entity)this.entity).getUniqueID());
        spawner.lastSpawnTime = System.currentTimeMillis();
        if (this.spawnInfo.interval != null) {
            BetterSpawnerConfig.consumeInterval(this.spawnInfo.interval);
        }
        return this.entity;
    }
}

