/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 *  javax.annotation.Nullable
 *  org.apache.logging.log4j.Level
 */
package com.pixelmongenerations.api.spawning;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.spawning.SpawnInfo;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.SpawnSet;
import com.pixelmongenerations.api.spawning.util.SpatialData;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;
import java.util.function.Predicate;
import javax.annotation.Nullable;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.apache.logging.log4j.Level;

public abstract class AbstractSpawner {
    public final ArrayList<UUID> spawned = new ArrayList();
    public volatile boolean isBusy = false;
    public long lastSpawnTime;
    public final ArrayList<SpawnSet> spawnSets = new ArrayList();

    public boolean shouldDoSpawning() {
        return !this.isBusy;
    }

    public ArrayList<SpawnInfo> getSuitableSpawns(SpawnLocation spawnLocation) {
        ArrayList<SpawnInfo> suitableSpawns = new ArrayList<SpawnInfo>();
        for (SpawnSet set : this.spawnSets) {
            suitableSpawns.addAll(set.suitableSpawnsFor(this, spawnLocation));
        }
        return suitableSpawns;
    }

    @Nullable
    public SpawnInfo getWeightedSpawnInfo(SpawnLocation spawnLocation) {
        ArrayList<SpawnInfo> suitableSpawns = this.getSuitableSpawns(spawnLocation);
        return suitableSpawns.isEmpty() ? null : this.choose(spawnLocation, suitableSpawns);
    }

    public SpawnInfo choose(SpawnLocation spawnLocation, ArrayList<SpawnInfo> spawnInfos) {
        SpawnInfo spawnInfo;
        if (spawnInfos.isEmpty()) {
            return null;
        }
        if (spawnInfos.size() == 1) {
            return spawnInfos.get(0);
        }
        HashMap<SpawnInfo, Float> finalRarities = new HashMap<SpawnInfo, Float>();
        float raritySum = 0.0f;
        for (SpawnInfo spawnInfo2 : spawnInfos) {
            float rarity = spawnInfo2.getAdjustedRarity(spawnLocation);
            raritySum += rarity;
            finalRarities.put(spawnInfo2, Float.valueOf(rarity));
        }
        if (raritySum == 0.0f) {
            return null;
        }
        float selected = RandomHelper.getRandomNumberBetween(0.0f, raritySum);
        raritySum = 0.0f;
        Iterator<SpawnInfo> var9 = spawnInfos.iterator();
        do {
            if (var9.hasNext()) continue;
            Pixelmon.LOGGER.log(Level.WARN, "Unable to choose a SpawnInfo based on rarities. This shouldn't be possible.");
            return null;
        } while ((raritySum += ((Float)finalRarities.get(spawnInfo = var9.next())).floatValue()) < selected || ((Float)finalRarities.get(spawnInfo)).floatValue() == 0.0f);
        return spawnInfo;
    }

    public SpatialData calculateSpatialData(World world, BlockPos centre, int max, boolean includeDownwards, Predicate<Block> condition) {
        int z;
        int y;
        int xMul;
        int yOffset;
        int xOffset;
        int[] arrn;
        IBlockState state = world.getBlockState(centre);
        if (!condition.test(state.getBlock())) {
            return new SpatialData(0, world.getBlockState(centre.down()).getBlock(), Lists.newArrayList(state.getBlock()));
        }
        int r = 1;
        int[] xs = new int[]{-1, 1};
        if (includeDownwards) {
            int[] arrn2 = new int[2];
            arrn2[0] = -1;
            arrn = arrn2;
            arrn2[1] = 1;
        } else {
            int[] arrn3 = new int[1];
            arrn = arrn3;
            arrn3[0] = 1;
        }
        int[] ys = arrn;
        int[] zs = new int[]{-1, 1};
        int radius = r;
        block0: while (radius <= max) {
            xOffset = xs.length;
            for (yOffset = 0; yOffset < xOffset; ++yOffset) {
                xMul = xs[yOffset];
                y = zs.length;
                for (z = 0; z < y; ++z) {
                    int zMul = zs[z];
                    for (int yMul : ys) {
                        state = world.getBlockState(centre.add(xMul * r, yMul * r, zMul * r));
                        if (condition.test(state.getBlock())) continue;
                        r = radius;
                        break block0;
                    }
                }
            }
            r = radius++;
        }
        BlockPos.MutableBlockPos pos = new BlockPos.MutableBlockPos(0, 0, 0);
        ArrayList<Block> uniqueSurroundingBlocks = new ArrayList<Block>();
        for (xOffset = -r; xOffset <= r; ++xOffset) {
            for (yOffset = -r; yOffset <= r; ++yOffset) {
                for (xMul = -r; xMul <= r; ++xMul) {
                    int x = xOffset + centre.getX();
                    Block block = world.getBlockState(pos.setPos(x, y = yOffset + centre.getY(), z = xMul + centre.getZ())).getBlock();
                    if (uniqueSurroundingBlocks.contains(block)) continue;
                    uniqueSurroundingBlocks.add(block);
                }
            }
        }
        return new SpatialData(r, world.getBlockState(centre.down()).getBlock(), uniqueSurroundingBlocks);
    }

    public static class SpawnerBuilder<T extends AbstractSpawner> {
        protected ArrayList<SpawnSet> spawnSets = new ArrayList();

        public SpawnerBuilder addSpawnSets(SpawnSet ... spawnSets) {
            this.spawnSets.addAll(Arrays.asList(spawnSets).subList(0, spawnSets.length));
            return this;
        }

        public SpawnerBuilder addSpawnSets(Collection<SpawnSet> spawnSets) {
            this.spawnSets.addAll(spawnSets);
            return this;
        }

        public SpawnerBuilder setSpawnSets(SpawnSet ... spawnSets) {
            this.spawnSets.clear();
            return this.addSpawnSets(spawnSets);
        }

        public SpawnerBuilder setSpawnSets(Collection<SpawnSet> spawnSets) {
            this.spawnSets.clear();
            this.addSpawnSets(spawnSets);
            return this;
        }

        public T apply(T spawner) {
            ((AbstractSpawner)spawner).spawnSets.clear();
            ((AbstractSpawner)spawner).spawnSets.addAll(this.spawnSets);
            return spawner;
        }
    }
}

