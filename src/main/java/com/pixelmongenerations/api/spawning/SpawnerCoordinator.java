/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.spawning;

import com.pixelmongenerations.api.spawning.AbstractSpawner;
import com.pixelmongenerations.api.spawning.archetypes.spawners.TickingSpawner;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;

public class SpawnerCoordinator {
    public final ArrayList<AbstractSpawner> spawners = new ArrayList();
    public Processor processor;
    private boolean active = false;

    public SpawnerCoordinator(AbstractSpawner ... spawners) {
        Collections.addAll(this.spawners, spawners);
    }

    @SubscribeEvent
    public void onTick(TickEvent.ServerTickEvent event) {
        if (event.phase == TickEvent.Phase.END && event.side == Side.SERVER && this.active) {
            for (AbstractSpawner spawner : this.spawners) {
                if (!(spawner instanceof TickingSpawner) || !spawner.shouldDoSpawning()) continue;
                ((TickingSpawner)spawner).doPass(this);
            }
        }
    }

    public boolean getActive() {
        return this.active;
    }

    public SpawnerCoordinator activate() {
        this.processor = new Processor();
        new Timer().scheduleAtFixedRate((TimerTask)this.processor, 0L, 5L);
        MinecraftForge.EVENT_BUS.register(this);
        this.active = true;
        return this;
    }

    public void deactivate() {
        this.processor.cancel();
        MinecraftForge.EVENT_BUS.unregister(this);
        this.active = false;
    }

    public static class Processor
    extends TimerTask {
        private ArrayList<Runnable> processes = new ArrayList();
        private boolean threadHasBeenNamed = false;

        @Override
        public void run() {
            if (!this.threadHasBeenNamed) {
                Thread.currentThread().setName("World Spawner");
                this.threadHasBeenNamed = true;
            }
            try {
                Runnable process;
                while ((process = this.getNextProcess()) != null) {
                    process.run();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Runnable getNextProcess() {
            ArrayList<Runnable> arrayList;
            ArrayList<Runnable> arrayList2 = arrayList = this.processes;
            synchronized (arrayList2) {
                return !this.processes.isEmpty() ? this.processes.remove(0) : null;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void addProcess(Runnable process) {
            ArrayList<Runnable> arrayList;
            ArrayList<Runnable> arrayList2 = arrayList = this.processes;
            synchronized (arrayList2) {
                this.processes.add(process);
            }
        }
    }
}

