/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.spawning.archetypes.spawners;

import com.pixelmongenerations.api.spawning.AbstractSpawner;
import com.pixelmongenerations.api.spawning.SpawnAction;
import com.pixelmongenerations.api.spawning.SpawnerCoordinator;
import com.pixelmongenerations.api.spawning.calculators.ICalculateSpawnLocations;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import net.minecraft.entity.Entity;

public abstract class TickingSpawner
extends AbstractSpawner {
    public long lastCycleTime = 0L;
    public long nextSpawnTime = 0L;
    public ICalculateSpawnLocations spawnLocationCalculator = ICalculateSpawnLocations.getDefault();
    public int capacity = PixelmonConfig.entitiesPerPlayer;
    public int spawnsPerPass = PixelmonConfig.spawnsPerPass;
    public float spawnFrequency = PixelmonConfig.spawnFrequency;
    public float minDistBetweenSpawns = PixelmonConfig.minimumDistanceBetweenSpawns;
    private int pass = -1;

    public abstract ArrayList<SpawnAction<? extends Entity>> getSpawns(int var1);

    public abstract int getNumPasses();

    public boolean hasCapacity(int numSpawns) {
        return this.capacity == -1 || numSpawns + this.spawned.size() <= this.capacity;
    }

    @Override
    public boolean shouldDoSpawning() {
        return super.shouldDoSpawning() && this.hasCapacity(1) && System.currentTimeMillis() > this.nextSpawnTime;
    }

    public void onSpawnEnded() {
        this.lastCycleTime = System.currentTimeMillis();
        this.nextSpawnTime = this.lastCycleTime + (long)(60000.0 / (double)this.spawnFrequency);
    }

    public void doPass(SpawnerCoordinator coordinator) {
        this.pass = (this.pass + 1) % this.getNumPasses();
        ArrayList<SpawnAction<? extends Entity>> spawns = this.getSpawns(this.pass);
        if (spawns != null && !spawns.isEmpty()) {
            while (!this.hasCapacity(1) || spawns.size() > this.spawnsPerPass) {
                spawns.remove(RandomHelper.rand.nextInt(spawns.size()));
            }
            for (SpawnAction<? extends Entity> spawn : spawns) {
                spawn.doSpawn(this);
            }
        }
        this.onSpawnEnded();
    }

    public static abstract class TickingSpawnerBuilder<T extends TickingSpawner>
    extends AbstractSpawner.SpawnerBuilder<T> {
        protected ICalculateSpawnLocations spawnLocationCalculator = ICalculateSpawnLocations.getDefault();
        protected int capacity = PixelmonConfig.entitiesPerPlayer;
        protected int spawnsPerPass = PixelmonConfig.spawnsPerPass;
        protected float minDistBetweenSpawns = PixelmonConfig.minimumDistanceBetweenSpawns;
        protected float spawnFrequency = PixelmonConfig.spawnFrequency;

        public <E extends TickingSpawnerBuilder<T>> E setSpawnLocationCalculator(ICalculateSpawnLocations spawnLocationCalculator) {
            if (spawnLocationCalculator != null) {
                this.spawnLocationCalculator = spawnLocationCalculator;
            }
            return (E)this;
        }

        public <E extends TickingSpawnerBuilder<T>> E setSpawnFrequency(float spawnFrequency) {
            if (spawnFrequency == 0.0f) {
                spawnFrequency = 1.0E-5f;
            }
            this.spawnFrequency = spawnFrequency;
            return (E)this;
        }

        public <E extends TickingSpawnerBuilder<T>> E setCapacity(int capacity) {
            if (capacity < 0 && capacity != -1) {
                capacity = 0;
            }
            this.capacity = capacity;
            return (E)this;
        }

        public <E extends TickingSpawnerBuilder<T>> E setDistanceBetweenSpawns(float minDistBetweenSpawns) {
            if (minDistBetweenSpawns < 0.0f) {
                minDistBetweenSpawns = 0.0f;
            }
            this.minDistBetweenSpawns = minDistBetweenSpawns;
            return (E)this;
        }

        public <E extends TickingSpawnerBuilder<T>> E setSpawnsPerPass(int spawnsPerPass) {
            if (spawnsPerPass < 0) {
                spawnsPerPass = 0;
            }
            this.spawnsPerPass = spawnsPerPass;
            return (E)this;
        }

        @Override
        public T apply(T spawner) {
            super.apply(spawner);
            ((TickingSpawner)spawner).spawnLocationCalculator = this.spawnLocationCalculator;
            ((TickingSpawner)spawner).capacity = this.capacity;
            ((TickingSpawner)spawner).spawnsPerPass = this.spawnsPerPass;
            ((TickingSpawner)spawner).spawnFrequency = this.spawnFrequency;
            ((TickingSpawner)spawner).minDistBetweenSpawns = this.minDistBetweenSpawns;
            return spawner;
        }
    }
}

