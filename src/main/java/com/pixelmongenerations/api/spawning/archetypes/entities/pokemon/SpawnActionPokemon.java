/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.spawning.archetypes.entities.pokemon;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.api.spawning.AbstractSpawner;
import com.pixelmongenerations.api.spawning.SpawnAction;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.archetypes.entities.pokemon.SpawnInfoPokemon;
import com.pixelmongenerations.api.spawning.conditions.LocationType;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.NBTLink;
import com.pixelmongenerations.common.item.heldItems.SpawningAffectingHeldItem;
import com.pixelmongenerations.common.spawning.PlayerTrackingSpawner;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import java.util.Objects;
import java.util.stream.Stream;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;

public class SpawnActionPokemon
extends SpawnAction<EntityPixelmon> {
    public PokemonSpec spec;

    public SpawnActionPokemon(SpawnInfoPokemon spawnInfo, SpawnLocation spawnLocation, PokemonSpec spec) {
        super(spawnInfo, spawnLocation);
        this.spec = spec;
    }

    @Override
    protected EntityPixelmon createEntity() {
        return this.spec.create(this.spawnLocation.location.world);
    }

    @Override
    public EntityPixelmon doSpawn(AbstractSpawner spawner) {
        EntityPixelmon pokemon = (EntityPixelmon)this.getOrCreateEntity();
        if (pokemon == null) {
            return null;
        }
        for (LocationType type : this.spawnLocation.types) {
            if (!this.spawnInfo.locationTypes.contains(type)) continue;
            type.mutator.accept(this.spawnLocation);
        }
        if (this.spawnInfo.locationTypes.contains(LocationType.AIR)) {
            pokemon.setSpawnLocation(com.pixelmongenerations.core.database.SpawnLocation.AirPersistent);
        } else if (this.spawnLocation.types.contains(LocationType.WATER) || this.spawnLocation.types.contains(LocationType.UNDERGROUND_WATER) || this.spawnLocation.types.contains(LocationType.SURFACE_WATER) || this.spawnLocation.types.contains(LocationType.SEAFLOOR)) {
            pokemon.setSpawnLocation(com.pixelmongenerations.core.database.SpawnLocation.Water);
        } else {
            pokemon.setSpawnLocation(com.pixelmongenerations.core.database.SpawnLocation.Land);
        }
        pokemon.resetAI();
        super.doSpawn(spawner);
        if (spawner instanceof PlayerTrackingSpawner) {
            Stream.of(PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)((PlayerTrackingSpawner)spawner).getTrackedPlayer()).get().partyPokemon).filter(Objects::nonNull).map(NBTLink::new).map(NBTLink::getHeldItem).filter(SpawningAffectingHeldItem.class::isInstance).map(SpawningAffectingHeldItem.class::cast).findFirst().ifPresent(a -> a.modifySpawn(pokemon));
        }
        if (this.entity != null && spawner.spawned.contains(((EntityPixelmon)this.entity).getUniqueID()) && PixelmonConfig.doLegendaryEvent && (EnumSpecies.legendaries.contains(this.spec.name) || EnumSpecies.ultrabeasts.contains(this.spec.name))) {
            this.spawnLocation.location.world.getMinecraftServer().getPlayerList().sendMessage(new TextComponentTranslation("chat.type.announcement", (Object)((Object)TextFormatting.LIGHT_PURPLE) + "Pixelmon" + (Object)((Object)TextFormatting.RESET), (Object)((Object)TextFormatting.GREEN) + I18n.translateToLocalFormatted("spawn.legendarymessage", this.spec.name, this.spawnLocation.biome.biomeName)));
            BlockPos.MutableBlockPos pos = this.spawnLocation.location.pos;
            this.spawnLocation.location.world.getMinecraftServer().sendMessage(new TextComponentString((Object)((Object)TextFormatting.LIGHT_PURPLE) + "Spawned " + this.spec.name + " at: " + this.spawnLocation.location.world.getWorldInfo().getWorldName() + " x:" + pos.getX() + ", y:" + pos.getY() + ", z:" + pos.getZ()));
            ((EntityPixelmon)this.entity).legendaryTicks = 6000;
        }
        return (EntityPixelmon)this.entity;
    }
}

