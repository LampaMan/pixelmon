/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.spawning.archetypes.entities;

import com.pixelmongenerations.api.spawning.SpawnAction;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.archetypes.entities.SpawnInfoEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.util.ResourceLocation;

public class SpawnActionEntity
extends SpawnAction<Entity> {
    private final ResourceLocation name;

    public SpawnActionEntity(SpawnInfoEntity spawnInfo, SpawnLocation spawnLocation) {
        super(spawnInfo, spawnLocation);
        this.name = new ResourceLocation(spawnInfo.name);
    }

    @Override
    protected Entity createEntity() {
        return EntityList.createEntityByIDFromName(this.name, this.spawnLocation.location.world);
    }
}

