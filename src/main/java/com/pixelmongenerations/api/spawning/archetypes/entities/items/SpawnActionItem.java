/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.spawning.archetypes.entities.items;

import com.pixelmongenerations.api.spawning.SpawnAction;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.archetypes.entities.items.SpawnInfoItem;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;

public class SpawnActionItem
extends SpawnAction<EntityItem> {
    public ItemStack itemStack = null;

    public SpawnActionItem(SpawnInfoItem spawnInfo, SpawnLocation spawnLocation) {
        super(spawnInfo, spawnLocation);
        if (spawnInfo.itemStack != null) {
            this.itemStack = spawnInfo.itemStack.copy();
            this.itemStack.setCount(RandomHelper.getRandomNumberBetween(spawnInfo.minQuantity, spawnInfo.maxQuantity));
        } else {
            this.itemStack = null;
        }
    }

    @Override
    protected EntityItem createEntity() {
        if (this.itemStack != null) {
            return new EntityItem(this.spawnLocation.location.world, this.spawnLocation.location.pos.getX(), this.spawnLocation.location.pos.getY(), this.spawnLocation.location.pos.getZ(), this.itemStack);
        }
        return null;
    }
}

