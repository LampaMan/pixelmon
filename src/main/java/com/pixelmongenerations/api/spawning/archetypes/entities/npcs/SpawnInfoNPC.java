/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.spawning.archetypes.entities.npcs;

import com.pixelmongenerations.api.spawning.AbstractSpawner;
import com.pixelmongenerations.api.spawning.SpawnAction;
import com.pixelmongenerations.api.spawning.SpawnInfo;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.archetypes.entities.npcs.SpawnActionNPC;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.core.enums.EnumNPCType;

public class SpawnInfoNPC
extends SpawnInfo {
    public static final String TYPE_ID_NPC = "npc";
    public EnumNPCType npcType;

    public SpawnInfoNPC() {
        super(TYPE_ID_NPC);
    }

    public SpawnAction<EntityNPC> construct(AbstractSpawner spawner, SpawnLocation spawnLocation) {
        return new SpawnActionNPC(this, spawnLocation);
    }

    @Override
    public String toString() {
        return this.npcType.name();
    }
}

