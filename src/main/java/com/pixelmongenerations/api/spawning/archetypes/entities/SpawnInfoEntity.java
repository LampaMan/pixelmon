/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.spawning.archetypes.entities;

import com.pixelmongenerations.api.spawning.AbstractSpawner;
import com.pixelmongenerations.api.spawning.SpawnAction;
import com.pixelmongenerations.api.spawning.SpawnInfo;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.archetypes.entities.SpawnActionEntity;
import net.minecraft.entity.Entity;

public class SpawnInfoEntity
extends SpawnInfo {
    public String name;

    public SpawnInfoEntity() {
        super("entity");
    }

    @Override
    public SpawnAction<? extends Entity> construct(AbstractSpawner spawner, SpawnLocation spawnLocation) {
        return new SpawnActionEntity(this, spawnLocation);
    }

    @Override
    public String toString() {
        return this.name;
    }
}

