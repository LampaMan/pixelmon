/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.api.spawning.archetypes.spawners;

import com.pixelmongenerations.api.spawning.AbstractSpawner;
import com.pixelmongenerations.api.spawning.SpawnAction;
import com.pixelmongenerations.api.spawning.SpawnInfo;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import javax.annotation.Nullable;
import net.minecraft.entity.Entity;

public class TriggerSpawner
extends AbstractSpawner {
    public float triggerChance = 1.0f;

    public TriggerSpawner() {
    }

    public TriggerSpawner(float triggerChance) {
        this.triggerChance = triggerChance;
    }

    @Nullable
    public Entity trigger(SpawnLocation location) {
        SpawnInfo spawnInfo;
        if (RandomHelper.getRandomChance(this.triggerChance) && (spawnInfo = this.getWeightedSpawnInfo(location)) != null) {
            return spawnInfo.construct(this, location).doSpawn(this);
        }
        return null;
    }

    public SpawnAction<? extends Entity> getAction(SpawnLocation location, float triggerChance) {
        SpawnInfo spawnInfo;
        if (RandomHelper.getRandomChance(triggerChance) && (spawnInfo = this.getWeightedSpawnInfo(location)) != null) {
            return spawnInfo.construct(this, location);
        }
        return null;
    }
}

