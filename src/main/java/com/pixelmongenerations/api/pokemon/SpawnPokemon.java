/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.pokemon;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.battle.BattleFactory;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.WildPixelmonParticipant;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Optional;
import java.util.function.Function;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class SpawnPokemon {
    private Function<World, EntityPixelmon> spec;
    private float yaw;
    private float x;
    private float y;
    private float z;

    private SpawnPokemon(Function<World, EntityPixelmon> spec, float x, float y, float z, float yaw) {
        this.spec = spec;
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
    }

    public static SpawnPokemon of(EnumSpecies species, BlockPos pos, float yaw) {
        return SpawnPokemon.builder().pixelmon(species).pos(pos).yaw(yaw).build();
    }

    public static SpawnPokemon of(PokemonSpec pokemonSpec, BlockPos pos, float yaw) {
        return SpawnPokemon.builder().pixelmon(pokemonSpec).pos(pos).yaw(yaw).build();
    }

    public static SpawnPokemon of(EnumSpecies species, float x, float y, float z, float yaw) {
        return SpawnPokemon.builder().pixelmon(species).pos(x, y, z).yaw(yaw).build();
    }

    public static SpawnPokemon of(PokemonSpec pokemonSpec, float x, float y, float z, float yaw) {
        return SpawnPokemon.builder().pixelmon(pokemonSpec).pos(x, y, z).yaw(yaw).build();
    }

    public boolean spawn(EntityPlayerMP player, World world) {
        if (this.spec == null || player == null) {
            return false;
        }
        EntityPixelmon pixelmonEntity = this.spec.apply(player.world);
        if (pixelmonEntity == null) {
            return false;
        }
        pixelmonEntity.setPositionAndRotation(this.x, this.y, this.z, this.yaw, 0.0f);
        Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
        if (storageOpt.isPresent()) {
            world.spawnEntity(pixelmonEntity);
            PlayerStorage storage = storageOpt.get();
            Optional.ofNullable(storage.getFirstAblePokemon(player.world)).ifPresent(startingPixelmon -> BattleFactory.createBattle().team1(new PlayerParticipant(player, (EntityPixelmon)startingPixelmon)).team2(new WildPixelmonParticipant(true, pixelmonEntity)).startBattle());
            return true;
        }
        pixelmonEntity.setDead();
        return false;
    }

    public static Builder builder() {
        return new Builder();
    }

    public SpawnPokemon setPos(Vec3d pos) {
        this.x = (float)pos.x;
        this.y = (float)pos.y;
        this.z = (float)pos.z;
        return null;
    }

    public static class Builder {
        private Function<World, EntityPixelmon> spec = new PokemonSpec(EnumSpecies.Magikarp.name)::create;
        private float x;
        private float y;
        private float z = 0.0f;
        private float yaw;

        private Builder() {
        }

        public Builder pos(float x, float y, float z) {
            this.x = x + 0.5f;
            this.y = y + 0.5f;
            this.z = z + 0.5f;
            return this;
        }

        public Builder pos(BlockPos pos) {
            return this.pos((float)pos.getX() + 0.5f, (float)pos.getY() + 0.5f, (float)pos.getZ() + 0.5f);
        }

        public Builder pixelmon(EnumSpecies species) {
            return this.pixelmon(new PokemonSpec(species.name));
        }

        public Builder pixelmon(PokemonSpec spec) {
            return this.pixelmon(spec::create);
        }

        public Builder pixelmon(Function<World, EntityPixelmon> function) {
            this.spec = function;
            return this;
        }

        public SpawnPokemon build() {
            return new SpawnPokemon(this.spec, this.x, this.y, this.z, this.yaw);
        }

        public Builder pos(double posX, double posY, double posZ) {
            return this.pos((float)posX, (float)posY, (float)posZ);
        }

        public Builder yaw(float yaw) {
            this.yaw = yaw;
            return this;
        }
    }
}

