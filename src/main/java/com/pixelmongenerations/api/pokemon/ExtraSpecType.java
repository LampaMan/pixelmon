/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.api.pokemon;

import com.pixelmongenerations.api.pokemon.ExtraSpecValue;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.List;
import javax.annotation.Nullable;

public interface ExtraSpecType<Value> {
    public String getKey();

    @Nullable
    public List<String> getAlternativeKeys();

    @Nullable
    public Value parse(EnumSpecies var1, @Nullable String var2);

    public ExtraSpecValue<ExtraSpecType<Value>, Value> create(@Nullable Value var1);

    public String serialize();
}

