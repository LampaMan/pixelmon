/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.api.pokemon;

import com.google.common.collect.Lists;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.List;

public class SpeciesGroup {
    private final List<EnumSpecies> species;

    public SpeciesGroup(EnumSpecies ... species) {
        this.species = Lists.newArrayList(species);
    }

    public List<EnumSpecies> getSpecies() {
        return this.species;
    }

    public boolean contains(EnumSpecies pokemon) {
        return this.species.contains((Object)pokemon);
    }

    public EnumSpecies random() {
        if (this.species.size() == 1) {
            return this.species.get(0);
        }
        return this.species.get((int)(Math.random() * (double)this.species.size()));
    }
}

