/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 *  javax.annotation.Nonnull
 */
package com.pixelmongenerations.api.pokemon;

import com.google.common.collect.Lists;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumForms;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class PokemonGroup {
    private final List<PokemonData> members;

    public PokemonGroup(EnumSpecies ... species) {
        this(Stream.of(species).map(a -> new PokemonData((EnumSpecies)((Object)a), a.getFormEnum(0))));
    }

    public PokemonGroup(EnumSpecies species, IEnumForm form) {
        this.members = Lists.newArrayList(new PokemonData(species, form));
    }

    public PokemonGroup(PokemonData ... members) {
        this.members = Lists.newArrayList(members);
    }

    public PokemonGroup(@Nonnull EnumSpecies species, IEnumForm ... forms) {
        this(Stream.of(forms).map(a -> new PokemonData(species, (IEnumForm)a)));
    }

    public PokemonGroup(List<PokemonData> members) {
        this.members = members;
    }

    public PokemonGroup(Stream<PokemonData> stream) {
        this.members = stream.collect(Collectors.toList());
    }

    public List<PokemonData> getMembers() {
        return this.members;
    }

    public boolean contains(EnumSpecies species) {
        return this.members.stream().anyMatch(a -> a.contains(species, null));
    }

    public boolean contains(EnumSpecies pokemon, IEnumForm form) {
        return this.members.stream().anyMatch(a -> a.contains(pokemon, form));
    }

    public boolean contains(EnumForms form) {
        return this.members.stream().anyMatch(a -> a.contains(form));
    }

    public boolean contains(PokemonData data) {
        return this.members.stream().anyMatch(a -> a.contains(data));
    }

    public PokemonData random() {
        if (this.members.size() == 1) {
            return this.members.get(0);
        }
        return this.members.get((int)(Math.random() * (double)this.members.size()));
    }

    public static class PokemonData {
        private final EnumSpecies species;
        private final IEnumForm form;

        public PokemonData(/*@NotNull*/ EnumSpecies species, @Nullable IEnumForm form) {
            this.species = species;
            this.form = form;
        }

        public static PokemonData of(EnumSpecies species, IEnumForm form) {
            return new PokemonData(species, form);
        }

        public static PokemonData of(EnumSpecies species) {
            return PokemonData.of(species, null);
        }

        public EnumSpecies getSpecies() {
            return this.species;
        }

        public IEnumForm getForm() {
            return this.form;
        }

        public boolean contains(EnumSpecies species, IEnumForm form) {
            return this.species == species && this.contains(form);
        }

        public boolean contains(IEnumForm form) {
            return form == null || form == this.form;
        }

        public boolean contains(PokemonData data) {
            return this.contains(data.species, data.form);
        }

        public String toString() {
            return "PokemonData{species=" + (Object)((Object)this.species) + ", form=" + this.form + '}';
        }
    }
}

