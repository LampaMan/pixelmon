/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.api.pokemon.specs;

import com.pixelmongenerations.api.pokemon.specs.FlagSpec;
import java.util.Collections;
import java.util.List;
import javax.annotation.Nullable;

public class UntradeableSpec
extends FlagSpec {
    @Override
    protected FlagSpec create() {
        return new UntradeableSpec();
    }

    @Override
    public String getKey() {
        return "untradeable";
    }

    @Override
    @Nullable
    public List<String> getAlternativeKeys() {
        return Collections.singletonList("notrade");
    }
}

