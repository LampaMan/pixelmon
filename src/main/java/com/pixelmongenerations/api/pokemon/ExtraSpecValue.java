/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.pokemon;

import com.pixelmongenerations.api.pokemon.ExtraSpecType;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraft.nbt.NBTTagCompound;

public interface ExtraSpecValue<Type extends ExtraSpecType<Value>, Value> {
    public Value getValue();

    public Type getType();

    public boolean matches(EntityPixelmon var1);

    public boolean matches(NBTTagCompound var1);

    public void apply(NBTTagCompound var1);

    public void apply(EntityPixelmon var1);

    public String serialize();
}

