/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api;

import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemQuery;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemQueryList;
import com.pixelmongenerations.common.entity.pixelmon.drops.DroppedItem;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.itemDrops.ItemDropMode;
import com.pixelmongenerations.core.network.packetHandlers.itemDrops.ItemDropPacket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextComponentTranslation;

public class DropQueryFactory {
    private TextComponentTranslation customTitle;
    private ArrayList<EntityPlayerMP> targets;
    private ArrayList<DroppedItem> drops;

    public static DropQueryFactory newInstance() {
        DropQueryFactory factory = new DropQueryFactory();
        factory.targets = new ArrayList();
        factory.drops = new ArrayList();
        return factory;
    }

    public DropQueryFactory customTitle(TextComponentTranslation customTitle) {
        this.customTitle = customTitle;
        return this;
    }

    public DropQueryFactory addTarget(EntityPlayerMP targetPlayer) {
        this.targets.add(targetPlayer);
        return this;
    }

    public DropQueryFactory addTargets(EntityPlayerMP ... targetPlayers) {
        this.targets.addAll(Arrays.asList(targetPlayers));
        return this;
    }

    public DropQueryFactory addDrop(DroppedItem drop) {
        this.drops.add(drop);
        return this;
    }

    public DropQueryFactory addDrops(DroppedItem ... drops) {
        this.drops.addAll(Arrays.asList(drops));
        return this;
    }

    public DropQueryFactory addDrops(List<DroppedItem> drops) {
        this.drops.addAll(drops);
        return this;
    }

    public void send() {
        for (int i = 0; i < this.drops.size(); ++i) {
            this.drops.get((int)i).id = i;
        }
        for (EntityPlayerMP player : this.targets) {
            DropItemQuery diq = new DropItemQuery(new Vec3d(player.posX, player.posY, player.posZ), player.getUniqueID(), this.drops);
            DropItemQueryList.queryList.add(diq);
            Pixelmon.NETWORK.sendTo(new ItemDropPacket(ItemDropMode.NormalPokemon, this.customTitle != null ? this.customTitle : new TextComponentTranslation("Drops", new Object[0]), this.drops), player);
        }
    }
}

