/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.interactions;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;

@FunctionalInterface
public interface IInteraction {
    public boolean processInteract(EntityPixelmon var1, EntityPlayer var2, EnumHand var3, ItemStack var4);
}

