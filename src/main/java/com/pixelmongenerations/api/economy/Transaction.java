/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.economy;

public class Transaction {
    public final int amount;
    public final int balance;
    public final ResponseType responseType;
    public final String errorMessage;

    public Transaction(int amount, int balance, ResponseType responseType, String errorMessage) {
        this.amount = amount;
        this.balance = balance;
        this.responseType = responseType;
        this.errorMessage = errorMessage;
    }

    public boolean transactionSuccess() {
        return this.responseType == ResponseType.SUCCESS;
    }

    public static enum ResponseType {
        SUCCESS,
        FAILURE;

    }
}

