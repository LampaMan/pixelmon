/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.pc;

import com.pixelmongenerations.api.pc.BiomeUnlockedBackground;
import com.pixelmongenerations.api.pc.IBackground;
import com.pixelmongenerations.api.pc.PCBackground;
import com.pixelmongenerations.api.pc.PokemonUnlockedBackground;
import com.pixelmongenerations.api.pc.RecipeUnlockedBackground;
import com.pixelmongenerations.core.config.BetterSpawnerConfig;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.ArrayList;
import java.util.List;

public class BackgroundRegistry {
    private static ArrayList<IBackground> backgrounds = new ArrayList();

    private BackgroundRegistry() {
        throw new IllegalStateException("This is a static registry class");
    }

    public static IBackground getBackground(String id) {
        for (IBackground background : backgrounds) {
            if (!background.getId().equals(id)) continue;
            return background;
        }
        return null;
    }

    public static List<IBackground> getBackgrounds() {
        return backgrounds;
    }

    public static void addBackground(IBackground background) {
        backgrounds.add(background);
    }

    static {
        backgrounds.add(new PCBackground("box_aegislash", "Aegislash"));
        backgrounds.add(new RecipeUnlockedBackground("box_amethyst", "Amethyst", itemStack -> itemStack.getItem().getRegistryName().toString().equals("pixelmon:amethyst_block")));
        backgrounds.add(new BiomeUnlockedBackground("box_aqua_adventure", "Aqua Adventure", id -> BetterSpawnerConfig.isInCategory("oceans", id)));
        backgrounds.add(new PokemonUnlockedBackground("box_arceus", "Arceus", EnumSpecies.Arceus));
        backgrounds.add(new PokemonUnlockedBackground("box_chess", "Chess", EnumSpecies.Zamazenta, EnumSpecies.Zacian));
        backgrounds.add(new PCBackground("box_cloudy_nights", "Cloudy Nights"));
        backgrounds.add(new PCBackground("box_crystal_caverns", "Crystal Caverns"));
        backgrounds.add(new PCBackground("box_dark_brick", "Dark Brick", true));
        backgrounds.add(new PCBackground("box_dark_plaid", "Dark Plaid", true));
        backgrounds.add(new PCBackground("box_dark_stripes", "Dark Stripes", true));
        backgrounds.add(new BiomeUnlockedBackground("box_desert_mine", "Desert Mine", id -> BetterSpawnerConfig.isInCategory("dry", id)));
        backgrounds.add(new RecipeUnlockedBackground("box_emerald", "Emerald", itemStack -> itemStack.getItem().getRegistryName().toString().equals("minecraft:emerald_block")));
        backgrounds.add(new PCBackground("box_forest", "Forest", true));
        backgrounds.add(new PCBackground("box_great_outdoors", "Great Outdoors"));
        backgrounds.add(new BiomeUnlockedBackground("box_lavender_dreams", "Lavender Dreams", id -> id.equals("biomesoplenty:lavender_field")));
        backgrounds.add(new BiomeUnlockedBackground("box_mountain_railway", "Mountain Railway", id -> BetterSpawnerConfig.isInCategory("mountainous", id)));
        backgrounds.add(new PCBackground("box_mystic_library", "Mystic Library"));
        backgrounds.add(new BiomeUnlockedBackground("box_ocean_waves", "Ocean Waves", id -> BetterSpawnerConfig.isInCategory("oceans", id)));
        backgrounds.add(new BiomeUnlockedBackground("box_red_desert", "Red Desert", id -> BetterSpawnerConfig.isInCategory("mesas", id)));
        backgrounds.add(new RecipeUnlockedBackground("box_ruby", "Ruby", itemStack -> itemStack.getItem().getRegistryName().toString().equals("pixelmon:ruby_block")));
        backgrounds.add(new RecipeUnlockedBackground("box_sapphire", "Sapphire", itemStack -> itemStack.getItem().getRegistryName().toString().equals("pixelmon:sapphire_block")));
        backgrounds.add(new BiomeUnlockedBackground("box_snow_peaks", "Snow Peaks", id -> BetterSpawnerConfig.isInCategory("cold mountains", id)));
        backgrounds.add(new PCBackground("box_21", "Background #21"));
        backgrounds.add(new PCBackground("box_22", "Background #22"));
    }
}

