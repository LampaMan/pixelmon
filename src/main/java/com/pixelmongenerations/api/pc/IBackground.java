/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.pc;

import net.minecraft.entity.player.EntityPlayerMP;

public interface IBackground {
    public String getId();

    public String getName();

    public boolean hasUnlocked(EntityPlayerMP var1);
}

