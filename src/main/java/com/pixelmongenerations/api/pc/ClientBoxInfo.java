/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.pc;

public class ClientBoxInfo {
    private int number;
    private String name;
    private String background;

    private ClientBoxInfo(int number, String name, String background) {
        this.number = number;
        this.name = name;
        this.background = background;
    }

    public int getNumber() {
        return this.number;
    }

    public String getName() {
        return this.name;
    }

    public String getBackground() {
        return this.background;
    }

    public static ClientBoxInfo of(int number, String name, String background) {
        return new ClientBoxInfo(number, name, background);
    }
}

