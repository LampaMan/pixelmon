/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.pc;

import com.pixelmongenerations.api.pc.IBackground;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import net.minecraft.entity.player.EntityPlayerMP;

public class PCBackground
implements IBackground {
    private String id;
    private String name;
    private boolean defaultBg;

    public PCBackground(String id, String name) {
        this(id, name, false);
    }

    public PCBackground(String id, String name, boolean defaultBg) {
        this.id = id;
        this.name = name;
        this.defaultBg = defaultBg;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public boolean hasUnlocked(EntityPlayerMP player) {
        if (this.defaultBg) {
            return true;
        }
        PlayerComputerStorage storage = PixelmonStorage.computerManager.getPlayerStorage(player);
        if (storage == null) {
            Pixelmon.LOGGER.warn(String.format("Could not get computer for player %s in PCBackground", player.getName()));
            return false;
        }
        return storage.getUnlockedBackgrounds().contains(this.id);
    }
}

