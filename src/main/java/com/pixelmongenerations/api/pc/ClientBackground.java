/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.pc;

import com.pixelmongenerations.api.pc.IBackground;
import net.minecraft.entity.player.EntityPlayerMP;

public class ClientBackground
implements IBackground {
    private String id;
    private String name;
    private boolean unlocked;

    private ClientBackground(String id, String name, boolean unlocked) {
        this.id = id;
        this.name = name;
        this.unlocked = unlocked;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public boolean hasUnlocked(EntityPlayerMP player) {
        return this.unlocked;
    }

    public static ClientBackground of(String id, String name, boolean unlocked) {
        return new ClientBackground(id, name, unlocked);
    }
}

