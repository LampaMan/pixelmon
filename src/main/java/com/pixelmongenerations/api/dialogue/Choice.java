/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.api.dialogue;

import com.pixelmongenerations.api.events.dialogue.DialogueChoiceEvent;
import io.netty.buffer.ByteBuf;
import java.util.HashMap;
import java.util.UUID;
import java.util.function.Consumer;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class Choice {
    public static final HashMap<UUID, HashMap<Integer, Consumer<DialogueChoiceEvent>>> handleMap = new HashMap();
    private static int nextID = 0;
    public final int choiceID;
    public final String text;
    public final Consumer<DialogueChoiceEvent> handle;

    public Choice(String text, Consumer<DialogueChoiceEvent> handle) {
        this.choiceID = nextID++;
        this.text = text;
        this.handle = handle;
    }

    public Choice(ByteBuf buffer) {
        this.choiceID = buffer.readInt();
        this.text = ByteBufUtils.readUTF8String(buffer);
        this.handle = null;
    }

    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.choiceID);
        ByteBufUtils.writeUTF8String(buffer, this.text);
    }

    public static ChoiceBuilder builder() {
        return new ChoiceBuilder();
    }

    public static class ChoiceBuilder {
        private String text;
        private Consumer<DialogueChoiceEvent> handle;

        public ChoiceBuilder setText(String text) {
            this.text = text;
            return this;
        }

        public ChoiceBuilder setHandle(Consumer<DialogueChoiceEvent> handle) {
            this.handle = handle;
            return this;
        }

        public Choice build() {
            return new Choice(this.text, this.handle);
        }
    }
}

