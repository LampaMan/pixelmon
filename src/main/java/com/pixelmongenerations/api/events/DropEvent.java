/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableList
 */
package com.pixelmongenerations.api.events;

import com.google.common.collect.ImmutableList;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.drops.DroppedItem;
import com.pixelmongenerations.core.network.packetHandlers.itemDrops.ItemDropMode;
import java.util.ArrayList;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class DropEvent
extends Event {
    public final EntityPlayerMP player;
    public final EntityCreature entity;
    public final ItemDropMode dropMode;
    private final ArrayList<DroppedItem> drops;
    private int id;

    public DropEvent(EntityPlayerMP player, EntityCreature entity, ItemDropMode dropMode, ArrayList<DroppedItem> items) {
        this.player = player;
        this.entity = entity;
        this.dropMode = dropMode;
        this.drops = items;
        for (DroppedItem item : items) {
            if (this.id >= item.id) continue;
            this.id = item.id;
        }
    }

    public ImmutableList<DroppedItem> getDrops() {
        return (ImmutableList) ImmutableList.builder().addAll(this.drops).build();
    }

    public void addDrop(ItemStack drop) {
        if (drop != null) {
            this.drops.add(new DroppedItem(drop, ++this.id));
        }
    }

    public void addDrop(DroppedItem item) {
        item.id = this.id++;
        this.drops.add(item);
    }

    public void removeDrop(DroppedItem drop) {
        boolean droppingID = false;
        for (int i = 0; i < this.drops.size(); ++i) {
            if (droppingID) {
                --this.drops.get((int)i).id;
                continue;
            }
            if (this.drops.get(i) != drop) continue;
            this.drops.remove(i);
            --i;
            droppingID = true;
        }
    }

    public boolean isPokemon() {
        return this.entity instanceof EntityPixelmon;
    }

    public boolean isTrainer() {
        return this.entity instanceof NPCTrainer;
    }
}

