/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.world.gen.structure.util.GeneralScattered;
import net.minecraft.world.World;
import net.minecraft.world.gen.structure.StructureBoundingBox;
import net.minecraftforge.fml.common.eventhandler.Event;

public class PixelmonStructurePlacedEvent
extends Event {
    private GeneralScattered generalScattered;
    private World world;
    private StructureBoundingBox boundingBox;

    public PixelmonStructurePlacedEvent(GeneralScattered generalScattered, World world, StructureBoundingBox boundingBox) {
        this.generalScattered = generalScattered;
        this.world = world;
        this.boundingBox = boundingBox;
    }

    public GeneralScattered getGeneralScattered() {
        return this.generalScattered;
    }

    public World getWorld() {
        return this.world;
    }

    public StructureBoundingBox getBoundingBox() {
        return this.boundingBox;
    }
}

