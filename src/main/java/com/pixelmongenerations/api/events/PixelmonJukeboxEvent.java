/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class PixelmonJukeboxEvent
extends Event {
    private EntityPlayerMP player;
    private BlockPos pos;

    public PixelmonJukeboxEvent(EntityPlayerMP player, BlockPos pos) {
        this.player = player;
        this.pos = pos;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public BlockPos getPos() {
        return this.pos;
    }
}

