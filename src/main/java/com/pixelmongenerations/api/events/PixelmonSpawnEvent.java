/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class PixelmonSpawnEvent
extends Event {
    private final World world;
    private final BlockPos pos;

    public PixelmonSpawnEvent(World world, int x, int y, int z) {
        this.world = world;
        this.pos = new BlockPos(x, y, z);
    }

    public World getWorld() {
        return this.world;
    }

    public BlockPos getBlockPos() {
        return this.pos;
    }
}

