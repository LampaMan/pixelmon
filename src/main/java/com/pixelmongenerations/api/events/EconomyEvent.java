/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.api.economy.TransactionType;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class EconomyEvent
extends Event {
    private final EntityPlayerMP player;

    public EconomyEvent(EntityPlayerMP player) {
        this.player = player;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public static class PostTransactionEvent
    extends EconomyEvent {
        private final TransactionType type;
        private final int oldBalance;
        private final int newBalance;

        public PostTransactionEvent(EntityPlayerMP player, TransactionType type, int oldBalance, int newBalance) {
            super(player);
            this.type = type;
            this.oldBalance = oldBalance;
            this.newBalance = newBalance;
        }

        public TransactionType getTransactionType() {
            return this.type;
        }

        public int getOldBalance() {
            return this.oldBalance;
        }

        public int getNewBalance() {
            return this.newBalance;
        }
    }

    public static class PreTransactionEvent
    extends EconomyEvent {
        private final TransactionType type;
        private int balance;
        private int amount;

        public PreTransactionEvent(EntityPlayerMP player, TransactionType type, int balance, int amount) {
            super(player);
            this.type = type;
            this.balance = balance;
            this.amount = amount;
        }

        public TransactionType getTransactionType() {
            return this.type;
        }

        public int getBalance() {
            return this.balance;
        }

        public void setBalance(int balance) {
            this.balance = balance;
        }

        public int getAmount() {
            return this.amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }
    }
}

