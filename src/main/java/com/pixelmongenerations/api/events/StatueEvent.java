/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.entity.pixelmon.EntityStatue;
import com.pixelmongenerations.core.network.packetHandlers.statueEditor.EnumStatuePacketMode;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

public abstract class StatueEvent
extends Event {
    private final EntityPlayerMP player;

    public StatueEvent(EntityPlayerMP player) {
        this.player = player;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    @Cancelable
    public static class CreateStatue
    extends StatueEvent {
        private final WorldServer world;
        private final BlockPos location;
        private EntityStatue statue;

        public CreateStatue(EntityPlayerMP player, WorldServer world, BlockPos location, EntityStatue statue) {
            super(player);
            this.world = world;
            this.location = location;
            this.statue = statue;
        }

        public EntityStatue getStatue() {
            return this.statue;
        }

        public void setStatue(EntityStatue statue) {
            if (statue != null) {
                this.statue = statue;
            }
        }

        public WorldServer getWorld() {
            return this.world;
        }

        public BlockPos getLocation() {
            return this.location;
        }
    }

    @Cancelable
    public static class ModifyStatue
    extends StatueEvent {
        private EntityStatue statue;
        private final EnumStatuePacketMode changeType;
        private Object value;

        public ModifyStatue(EntityPlayerMP player, EntityStatue statue, EnumStatuePacketMode changeType, Object value) {
            super(player);
            this.statue = statue;
            this.changeType = changeType;
            this.value = value;
        }

        public Object getValue() {
            return this.value;
        }

        public void setValue(Object value) {
            if (value != null && value.getClass() == this.value.getClass()) {
                this.value = value;
            }
        }

        public EntityStatue getStatue() {
            return this.statue;
        }

        public void setStatue(EntityStatue statue) {
            if (statue != null) {
                this.statue = statue;
            }
        }

        public EnumStatuePacketMode getChangeType() {
            return this.changeType;
        }
    }

    @Cancelable
    public static class DestroyStatue
    extends StatueEvent {
        public final EntityStatue statue;

        public DestroyStatue(EntityPlayerMP player, EntityStatue statue) {
            super(player);
            this.statue = statue;
        }
    }
}

