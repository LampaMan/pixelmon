/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class AggressionEvent
extends Event {
    private final EntityLiving aggressor;
    private final EntityPlayerMP player;

    public AggressionEvent(EntityLiving aggressor, EntityPlayerMP player) {
        this.aggressor = aggressor;
        this.player = player;
    }

    public EntityLiving getAggressor() {
        return this.aggressor;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }
}

