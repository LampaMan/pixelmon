/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nonnull
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.item.ItemFossil;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import javax.annotation.Nonnull;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;
import org.jetbrains.annotations.NotNull;

public abstract class FossilMachineEvent
extends Event {
    private final World world;
    private final BlockPos pos;
    private final EntityPlayer player;

    protected FossilMachineEvent(World world, BlockPos pos, EntityPlayer player) {
        this.world = world;
        this.pos = pos;
        this.player = player;
    }

    public World getWorld() {
        return this.world;
    }

    public BlockPos getPos() {
        return this.pos;
    }

    public EntityPlayer getPlayer() {
        return this.player;
    }

    @Cancelable
    public static abstract class Pokeball
    extends FossilMachineEvent {
        @Nonnull
        private EnumPokeball pokeballType;

        protected Pokeball(World world, BlockPos pos, EntityPlayer player, EnumPokeball pokeball) {
            super(world, pos, player);
            this.pokeballType = pokeball;
        }

        public EnumPokeball getPokeball() {
            return this.pokeballType;
        }

        public void setPokeball(EnumPokeball pokeballType) {
            this.pokeballType = pokeballType;
        }

        @Cancelable
        public static class Insert
        extends Pokeball {
            public Insert(World world, BlockPos pos, EntityPlayer player, EnumPokeball pokeball) {
                super(world, pos, player, pokeball);
            }
        }

        @Cancelable
        public static class Take
        extends Pokeball {
            public Take(World world, BlockPos pos, EntityPlayer player, EnumPokeball pokeball) {
                super(world, pos, player, pokeball);
            }
        }
    }

    public static abstract class Fossil
    extends FossilMachineEvent {
        @Nonnull
        private ItemFossil item;

        protected Fossil(World world, BlockPos pos, EntityPlayer player, ItemFossil item) {
            super(world, pos, player);
            this.item = item;
        }

        public void setFossil(@NotNull ItemFossil fossil) {
            if (fossil.getFossil().isMachineUsable()) {
                this.item = fossil;
            }
        }

        public ItemFossil getFossil() {
            return this.item;
        }

        @Cancelable
        public static class Insert
        extends Fossil {
            public Insert(World world, BlockPos pos, EntityPlayer player, ItemFossil fossil) {
                super(world, pos, player, fossil);
            }
        }

        @Cancelable
        public static class Take
        extends Fossil {
            public Take(World world, BlockPos pos, EntityPlayer player, ItemFossil fossil) {
                super(world, pos, player, fossil);
            }
        }
    }

    @Cancelable
    public static class TakePokemon
    extends FossilMachineEvent {
        private final ItemFossil fossil;
        private final EnumPokeball pokeball;

        public TakePokemon(World world, BlockPos pos, EntityPlayer player, ItemFossil fossil, EnumPokeball pokeball) {
            super(world, pos, player);
            this.fossil = fossil;
            this.pokeball = pokeball;
        }

        public ItemFossil getFossil() {
            return this.fossil;
        }

        public EnumPokeball getPokeball() {
            return this.pokeball;
        }
    }
}

