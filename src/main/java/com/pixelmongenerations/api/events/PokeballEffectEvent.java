/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.entity.pokeballs.EntityPokeBall;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

public abstract class PokeballEffectEvent
extends Event {
    private final EntityPokeBall pokeBall;

    private PokeballEffectEvent(EntityPokeBall pokeBall) {
        this.pokeBall = pokeBall;
    }

    public EntityPokeBall getPokeBall() {
        return this.pokeBall;
    }

    @Cancelable
    public static class SentOutEffect
    extends PokeballEffectEvent {
        public SentOutEffect(EntityPokeBall pokeBall) {
            super(pokeBall);
        }
    }

    @Cancelable
    public static class SuccessfullCaptureEffect
    extends PokeballEffectEvent {
        public SuccessfullCaptureEffect(EntityPokeBall pokeBall) {
            super(pokeBall);
        }
    }

    @Cancelable
    public static class StartCaptureEffect
    extends PokeballEffectEvent {
        public StartCaptureEffect(EntityPokeBall pokeBall) {
            super(pokeBall);
        }
    }
}

