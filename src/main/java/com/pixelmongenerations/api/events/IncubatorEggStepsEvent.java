/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.block.tileEntities.TileEntityEggIncubator;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class IncubatorEggStepsEvent
extends Event {
    private TileEntityEggIncubator incubator;
    private int stepsRequired;

    public IncubatorEggStepsEvent(TileEntityEggIncubator incubator, int stepsRequired) {
        this.incubator = incubator;
        this.stepsRequired = stepsRequired;
    }

    public TileEntityEggIncubator getIncubator() {
        return this.incubator;
    }

    public int getStepsRequired() {
        return this.stepsRequired;
    }

    public void setStepsRequired(int stepsRequired) {
        this.stepsRequired = stepsRequired;
    }
}

