/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class PixelmonSendOutEvent
extends Event {
    private final EntityPlayerMP player;
    private final NBTTagCompound nbt;

    public PixelmonSendOutEvent(EntityPlayerMP player, NBTTagCompound nbt) {
        this.player = player;
        this.nbt = nbt;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public NBTTagCompound getTagCompound() {
        return this.nbt;
    }
}

