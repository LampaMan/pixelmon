/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Event;

public class PixelmonFaintEvent
extends Event {
    private final EntityPlayerMP player;
    private final EntityPixelmon pokemon;

    public PixelmonFaintEvent(EntityPlayerMP player, EntityPixelmon pokemon) {
        this.player = player;
        this.pokemon = pokemon;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public EntityPixelmon getPokemon() {
        return this.pokemon;
    }
}

