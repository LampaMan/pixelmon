/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class RidePokemonEvent
extends Event {
    private EntityPlayerMP player;
    private EntityPixelmon pixelmon;

    public RidePokemonEvent(EntityPlayerMP player, EntityPixelmon pixelmon) {
        this.player = player;
        this.pixelmon = pixelmon;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public EntityPixelmon getPokemon() {
        return this.pixelmon;
    }
}

