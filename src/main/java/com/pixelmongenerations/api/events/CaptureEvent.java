/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pokeballs.EntityPokeBall;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

public abstract class CaptureEvent
extends Event {
    private final EntityPlayerMP player;
    private EntityPixelmon pokemon;
    private final EntityPokeBall pokeball;

    private CaptureEvent(EntityPlayerMP player, EntityPixelmon pokemon, EntityPokeBall pokeball) {
        this.player = player;
        this.pokemon = pokemon;
        this.pokeball = pokeball;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public EntityPixelmon getPokemon() {
        return this.pokemon;
    }

    public void setPokemon(EntityPixelmon pokemon) {
        if (pokemon == null) {
            return;
        }
        this.pokemon = pokemon;
    }

    public EntityPokeBall getPokeBall() {
        return this.pokeball;
    }

    @Cancelable
    public static class SuccessfulCaptureEvent
    extends CaptureEvent {
        public SuccessfulCaptureEvent(EntityPlayerMP player, EntityPixelmon pokemon, EntityPokeBall pokeball) {
            super(player, pokemon, pokeball);
        }
    }

    public static class FailedCaptureEvent
    extends CaptureEvent {
        public FailedCaptureEvent(EntityPlayerMP player, EntityPixelmon pokemon, EntityPokeBall pokeball) {
            super(player, pokemon, pokeball);
        }
    }

    @Cancelable
    public static class StartCaptureEvent
    extends CaptureEvent {
        private int catchRate;
        private double ballBonus;

        public StartCaptureEvent(EntityPlayerMP player, EntityPixelmon pokemon, EntityPokeBall pokeball, int catchRate, double ballBonus) {
            super(player, pokemon, pokeball);
            this.catchRate = catchRate;
            this.ballBonus = ballBonus;
        }

        public int getCatchRate() {
            return this.catchRate;
        }

        public void setCatchRate(int catchRate) {
            this.catchRate = Math.max(1, Math.min(255, catchRate));
        }

        public double getBallBonus() {
            return this.ballBonus;
        }

        public void setBallBonus(double ballBonus) {
            this.ballBonus = Math.max(0.0, ballBonus);
        }
    }
}

