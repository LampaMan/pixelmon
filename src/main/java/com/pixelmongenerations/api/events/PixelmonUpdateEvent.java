/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public class PixelmonUpdateEvent
extends Event {
    private final EntityPixelmon pokemon;
    private final TickEvent.Phase phase;

    public PixelmonUpdateEvent(EntityPixelmon pokemon, TickEvent.Phase phase) {
        this.pokemon = pokemon;
        this.phase = phase;
    }

    public EntityPixelmon getPokemon() {
        return this.pokemon;
    }

    public TickEvent.Phase getTickPhase() {
        return this.phase;
    }

    @Override
    public boolean isCancelable() {
        return this.phase != TickEvent.Phase.END;
    }
}

