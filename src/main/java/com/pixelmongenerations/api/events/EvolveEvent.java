/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.Evolution;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

public abstract class EvolveEvent
extends Event {
    private final EntityPlayerMP player;
    private final EntityPixelmon preEvo;
    private PokemonSpec postEvo;
    private final Evolution evo;

    private EvolveEvent(EntityPlayerMP player, EntityPixelmon preEvo, Evolution evo) {
        this.player = player;
        this.preEvo = preEvo;
        this.postEvo = evo.to;
        this.evo = evo;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public EntityPixelmon getPokemon() {
        return this.preEvo;
    }

    public Evolution getEvolution() {
        return this.evo;
    }

    public PokemonSpec getPostEvolutionSpec() {
        return this.postEvo;
    }

    @Cancelable
    public static class PreEvolve
    extends EvolveEvent {
        public PreEvolve(EntityPlayerMP player, EntityPixelmon preEvo, Evolution evo) {
            super(player, preEvo, evo);
        }
    }

    public static class PostEvolve
    extends EvolveEvent {
        private final EntityPixelmon pokemon;

        public PostEvolve(EntityPlayerMP player, EntityPixelmon preEvo, Evolution evo, EntityPixelmon pokemon) {
            super(player, preEvo, evo);
            this.pokemon = pokemon;
        }

        @Override
        public EntityPixelmon getPokemon() {
            return this.pokemon;
        }
    }
}

