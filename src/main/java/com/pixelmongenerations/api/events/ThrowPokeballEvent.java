/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.core.enums.items.EnumPokeball;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.eventhandler.Event;

public class ThrowPokeballEvent
extends Event {
    private EntityPlayer player;
    private ItemStack itemStack;
    private EnumPokeball type;
    private boolean usedInBattle;

    public ThrowPokeballEvent(EntityPlayer player, ItemStack itemStack, EnumPokeball type, boolean usedInBattle) {
        this.player = player;
        this.itemStack = itemStack;
        this.type = type;
        this.usedInBattle = usedInBattle;
    }

    public EntityPlayer getPlayer() {
        return this.player;
    }

    public ItemStack getItemStack() {
        return this.itemStack;
    }

    public void setItemStack(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public EnumPokeball getBallType() {
        return this.type;
    }

    public boolean isInBattle() {
        return this.usedInBattle;
    }

    @Override
    public boolean isCancelable() {
        return !this.usedInBattle;
    }

    @Override
    public void setCanceled(boolean cancel) {
        if (this.usedInBattle && cancel) {
            throw new IllegalArgumentException("You cannot cancel this event when it is from in battle");
        }
        super.setCanceled(cancel);
    }
}

