/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.block.tileEntities.TileEntityPokeChest;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class PokeLootClaimedEvent
extends Event {
    private TileEntityPokeChest chest;
    private EntityPlayerMP player;

    public PokeLootClaimedEvent(EntityPlayerMP player, TileEntityPokeChest tile) {
        this.player = player;
        this.chest = tile;
    }

    public TileEntityPokeChest getLootTileEntity() {
        return this.chest;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }
}

