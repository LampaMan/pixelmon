/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.heldItems.ItemMegaStone;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public final class MegaEvolutionEvent
extends Event {
    private final PixelmonWrapper pw;
    private final ItemMegaStone stone;

    public MegaEvolutionEvent(PixelmonWrapper pw, ItemMegaStone stone) {
        this.pw = pw;
        this.stone = stone;
    }

    public PixelmonWrapper getPokemon() {
        return this.pw;
    }

    public ItemMegaStone getMegaStoneItem() {
        return this.stone;
    }
}

