/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.block.tileEntities.TileEntityApricornTree;
import com.pixelmongenerations.core.enums.items.EnumApricorns;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

public abstract class ApricornEvent
extends Event {
    private final EnumApricorns apricorn;
    private final BlockPos pos;

    protected ApricornEvent(EnumApricorns apricorn, BlockPos pos) {
        this.apricorn = apricorn;
        this.pos = pos;
    }

    public EnumApricorns getApricornType() {
        return this.apricorn;
    }

    public BlockPos getBlockPos() {
        return this.pos;
    }

    @Cancelable
    public static class PickApricornEvent
    extends ApricornEvent {
        private final EntityPlayerMP player;
        private final TileEntityApricornTree tree;
        private ItemStack pickedStack;

        public PickApricornEvent(EnumApricorns apricorn, BlockPos pos, EntityPlayerMP player, TileEntityApricornTree tree, ItemStack pickedStack) {
            super(apricorn, pos);
            this.player = player;
            this.tree = tree;
            this.pickedStack = pickedStack;
        }

        public EntityPlayerMP getPlayer() {
            return this.player;
        }

        public TileEntityApricornTree getApricornTileEntity() {
            return this.tree;
        }

        public ItemStack getPickedStack() {
            return this.pickedStack;
        }

        public void setPickedStack(ItemStack pickedStack) {
            if (pickedStack == null) {
                throw new IllegalArgumentException("ItemStack can not be null in setPickedStack");
            }
            this.pickedStack = pickedStack;
        }
    }

    public static class ApricornReadyEvent
    extends ApricornEvent {
        private final TileEntityApricornTree tree;

        public ApricornReadyEvent(EnumApricorns apricorn, BlockPos pos, TileEntityApricornTree tree) {
            super(apricorn, pos);
            this.tree = tree;
        }

        public TileEntityApricornTree getApricornTileEntity() {
            return this.tree;
        }
    }

    @Cancelable
    public static class GrowthChanceEvent
    extends ApricornEvent {
        private final TileEntityApricornTree tree;
        private float growthChance;

        public GrowthChanceEvent(EnumApricorns apricorn, BlockPos pos, TileEntityApricornTree tree, float growthChance) {
            super(apricorn, pos);
            this.tree = tree;
            this.growthChance = growthChance;
        }

        public TileEntityApricornTree getApricornTileEntity() {
            return this.tree;
        }

        public float getGrowthChance() {
            return this.growthChance;
        }

        public void setGrowthChance(float growthChance) {
            growthChance = Math.max(0.0f, Math.min(1.0f, growthChance));
        }
    }

    @Cancelable
    public static class ApricornWateredEvent
    extends ApricornEvent {
        private final EntityPlayerMP player;
        private final TileEntityApricornTree tree;

        public ApricornWateredEvent(EnumApricorns apricorn, BlockPos pos, EntityPlayerMP player, TileEntityApricornTree tree) {
            super(apricorn, pos);
            this.player = player;
            this.tree = tree;
        }

        public EntityPlayerMP getPlayer() {
            return this.player;
        }

        public TileEntityApricornTree getApricornTileEntity() {
            return this.tree;
        }
    }

    @Cancelable
    public static class ApricornPlantedEvent
    extends ApricornEvent {
        private final EntityPlayerMP player;

        public ApricornPlantedEvent(EnumApricorns apricorn, BlockPos pos, EntityPlayerMP player) {
            super(apricorn, pos);
            this.player = player;
        }

        public EntityPlayerMP getPlayer() {
            return this.player;
        }
    }
}

