/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.dialogue;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Event;

public class DialogueEndedEvent
extends Event {
    private final EntityPlayerMP player;

    public DialogueEndedEvent(EntityPlayerMP player) {
        this.player = player;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }
}

