/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.pokedex.EnumPokedexRegisterStatus;
import com.pixelmongenerations.common.pokedex.Pokedex;
import com.pixelmongenerations.core.enums.EnumSpecies;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class PokedexEvent
extends Event {
    private EntityPlayerMP player;
    private EnumSpecies species;
    private Pokedex pokedex;
    private EnumPokedexRegisterStatus currentStatus;
    private EnumPokedexRegisterStatus newStatus;

    public PokedexEvent(EntityPlayerMP player, Pokedex pokedex, EnumSpecies species, EnumPokedexRegisterStatus newStatus) {
        this.player = player;
        this.pokedex = pokedex;
        this.species = species;
        this.currentStatus = pokedex.get(species.getNationalPokedexInteger());
        this.newStatus = newStatus;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public Pokedex getPokedex() {
        return this.pokedex;
    }

    public EnumSpecies getSpecies() {
        return this.species;
    }

    public EnumPokedexRegisterStatus getCurrentStatus() {
        return this.currentStatus;
    }

    public EnumPokedexRegisterStatus getNewStatus() {
        return this.newStatus;
    }

    public boolean willBeChanged() {
        return this.currentStatus.ordinal() < this.newStatus.ordinal();
    }
}

