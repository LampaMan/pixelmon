/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.player;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.entity.pixelmon.stats.EVsStore;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class PlayerBattleGainEvent
extends Event {
    private PixelmonWrapper expReceiver;
    private int amount;
    private EVsStore evGain;

    public PlayerBattleGainEvent(PixelmonWrapper expReceiver, int amount, EVsStore evGain) {
        this.expReceiver = expReceiver;
        this.amount = amount;
        this.evGain = evGain;
    }

    public EntityPlayerMP getPlayer() {
        return ((PlayerParticipant)this.expReceiver.getParticipant()).player;
    }

    public PixelmonWrapper getWrapper() {
        return this.expReceiver;
    }

    public int getExp() {
        return this.amount;
    }

    public void setExp(int amount) {
        this.amount = amount;
    }

    public void addExp(int amount) {
        this.amount += amount;
    }

    public void multiplyExp(float times) {
        this.amount = Math.round((float)this.amount * times);
    }

    public EVsStore getEVGain() {
        return this.evGain;
    }

    public void setEVGain(EVsStore store) {
        this.evGain = store;
    }
}

