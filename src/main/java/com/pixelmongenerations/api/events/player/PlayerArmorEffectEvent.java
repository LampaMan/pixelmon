/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.player;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemArmor;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class PlayerArmorEffectEvent
extends Event {
    private EntityPlayer player;
    private ItemArmor.ArmorMaterial material;

    public PlayerArmorEffectEvent(EntityPlayer player, ItemArmor.ArmorMaterial material) {
        this.player = player;
        this.material = material;
    }

    public EntityPlayer getPlayer() {
        return this.player;
    }

    public ItemArmor.ArmorMaterial getMaterial() {
        return this.material;
    }
}

