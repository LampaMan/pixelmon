/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.player;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class PlayerEggHatchEvent
extends Event {
    private EntityPlayerMP player;
    private NBTTagCompound eggNBT;

    public PlayerEggHatchEvent(EntityPlayerMP player, NBTTagCompound eggNBT) {
        this.player = player;
        this.eggNBT = eggNBT;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public NBTTagCompound getEggNBT() {
        return this.eggNBT;
    }

    public void setEggNBT(NBTTagCompound eggNBT) {
        this.eggNBT = eggNBT;
    }
}

