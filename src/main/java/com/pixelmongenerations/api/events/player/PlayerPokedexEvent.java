/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.player;

import com.pixelmongenerations.common.pokedex.EnumPokedexRegisterStatus;
import com.pixelmongenerations.common.pokedex.Pokedex;
import com.pixelmongenerations.core.enums.EnumSpecies;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Event;

public class PlayerPokedexEvent
extends Event {
    private Pokedex pokedex;
    private final EntityPlayerMP player;

    public PlayerPokedexEvent(Pokedex pokedex, EntityPlayerMP player) {
        this.player = player;
    }

    public Pokedex getPokedex() {
        return this.pokedex;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public static class PokedexEntryEvent
    extends PlayerPokedexEvent {
        private EnumSpecies species;
        private EnumPokedexRegisterStatus registerStatus;

        public PokedexEntryEvent(Pokedex pokedex, EntityPlayerMP player, int nationalDex, EnumPokedexRegisterStatus status) {
            super(pokedex, player);
            this.species = EnumSpecies.getFromDex(nationalDex).get();
            this.registerStatus = status;
        }

        public EnumSpecies getSpecies() {
            return this.species;
        }

        public EnumPokedexRegisterStatus getEntryStatus() {
            return this.registerStatus;
        }
    }

    public static class PokedexCompletionEvent
    extends PlayerPokedexEvent {
        public PokedexCompletionEvent(Pokedex pokedex, EntityPlayerMP player) {
            super(pokedex, player);
        }
    }
}

