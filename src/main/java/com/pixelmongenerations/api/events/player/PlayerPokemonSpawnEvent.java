/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.player;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Event;

public class PlayerPokemonSpawnEvent
extends Event {
    private final EntityPlayerMP player;
    private final SpawnLocation spawnLocation;
    private PokemonSpec pokemonSpec;

    public PlayerPokemonSpawnEvent(EntityPlayerMP player, SpawnLocation spawnLocation, PokemonSpec pokemonSpec) {
        this.player = player;
        this.spawnLocation = spawnLocation;
        this.pokemonSpec = pokemonSpec;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public SpawnLocation getSpawnLocation() {
        return this.spawnLocation;
    }

    public PokemonSpec getPokemonSpec() {
        return this.pokemonSpec;
    }

    public void setPokemonSpec(PokemonSpec pokemonSpec) {
        this.pokemonSpec = pokemonSpec;
    }
}

