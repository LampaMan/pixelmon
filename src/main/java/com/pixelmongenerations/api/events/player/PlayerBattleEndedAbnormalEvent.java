/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.player;

import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Event;

@Deprecated
public class PlayerBattleEndedAbnormalEvent
extends Event {
    private final EntityPlayerMP player;
    private final BattleControllerBase battleController;

    public PlayerBattleEndedAbnormalEvent(EntityPlayerMP player, BattleControllerBase battleControllerBase) {
        this.player = player;
        this.battleController = battleControllerBase;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public BattleControllerBase getBattleController() {
        return this.battleController;
    }
}

