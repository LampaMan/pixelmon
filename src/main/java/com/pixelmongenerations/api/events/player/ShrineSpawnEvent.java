/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.player;

import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class ShrineSpawnEvent
extends Event {
    private final EnumSpecies species;
    private final IEnumForm form;
    private final EntityPlayerMP player;
    private final BlockPos pos;
    private final World world;

    public ShrineSpawnEvent(EnumSpecies species, IEnumForm form, EntityPlayerMP player, World world, BlockPos pos) {
        this.species = species;
        this.form = form;
        this.player = player;
        this.pos = pos;
        this.world = world;
    }

    public EnumSpecies getSpecies() {
        return this.species;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public BlockPos getPos() {
        return this.pos;
    }

    public World getWorld() {
        return this.world;
    }

    public IEnumForm getForm() {
        return this.form;
    }
}

