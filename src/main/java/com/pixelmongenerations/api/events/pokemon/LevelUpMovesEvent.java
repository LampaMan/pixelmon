/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.api.events.pokemon;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.pixelmon.Entity6CanBattle;
import com.pixelmongenerations.core.database.DatabaseMoves;
import java.util.ArrayList;
import javax.annotation.Nullable;
import net.minecraftforge.fml.common.eventhandler.Event;

public class LevelUpMovesEvent
extends Event {
    private final Entity6CanBattle pokemon;
    private final int level;
    private final ArrayList<Attack> attacks;

    public LevelUpMovesEvent(Entity6CanBattle pokemon, int level, ArrayList<Attack> levelUpMoves) {
        this.pokemon = pokemon;
        this.level = level;
        this.attacks = levelUpMoves;
    }

    public Entity6CanBattle getPokemon() {
        return this.pokemon;
    }

    public int getLevel() {
        return this.level;
    }

    public ArrayList<Attack> getAttacks() {
        return this.attacks;
    }

    @Nullable
    public Attack getAttack(String moveName) {
        return DatabaseMoves.getAttack(moveName);
    }
}

