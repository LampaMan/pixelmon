/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.api.events.pokemon;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import javax.annotation.Nullable;
import net.minecraftforge.fml.common.eventhandler.Event;

public abstract class MovesetEvent
extends Event {
    private final PokemonLink pokemonLink;
    private final Moveset moveset;

    protected MovesetEvent(PokemonLink pokemonLink, Moveset moveset) {
        this.pokemonLink = pokemonLink;
        this.moveset = moveset;
    }

    public PokemonLink getPokemon() {
        return this.pokemonLink;
    }

    public Moveset getMoveset() {
        return this.moveset;
    }

    public static class LearntMoveEvent
    extends MovesetEvent {
        @Nullable
        private final Attack replacedAttack;
        private final Attack learntAttack;

        public LearntMoveEvent(PokemonLink pokemonLink, Moveset moveset, Attack replacedAttack, Attack learntAttack) {
            super(pokemonLink, moveset);
            this.replacedAttack = replacedAttack;
            this.learntAttack = learntAttack;
        }

        public boolean hasReplacedAttack() {
            return this.replacedAttack != null;
        }

        public Attack getReplacedAttack() {
            return this.replacedAttack;
        }

        public Attack getLearntAttack() {
            return this.learntAttack;
        }
    }

    public static class ForgotMoveEvent
    extends MovesetEvent {
        private final Attack forgottenAttack;

        public ForgotMoveEvent(PokemonLink pokemonLink, Moveset moveset, Attack forgottenAttack) {
            super(pokemonLink, moveset);
            this.forgottenAttack = forgottenAttack;
        }

        public Attack getForgottenAttack() {
            return this.forgottenAttack;
        }
    }
}

