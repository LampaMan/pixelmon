/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.pokemon;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Event;

public class MaxFriendshipEvent
extends Event {
    private EntityPlayerMP player;
    private EntityPixelmon pixelmon;

    public MaxFriendshipEvent(EntityPlayerMP player, EntityPixelmon pixelmon) {
        this.player = player;
        this.pixelmon = pixelmon;
    }

    public EntityPixelmon getPokemon() {
        return this.pixelmon;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }
}

