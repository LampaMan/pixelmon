/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.pokemon;

import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import java.util.Optional;
import net.minecraftforge.fml.common.eventhandler.Event;

public class BaseStatsLoadEvent
extends Event {
    private final String name;
    private final int form;
    private Optional<BaseStats> baseStats;

    public BaseStatsLoadEvent(String name, int form, Optional<BaseStats> baseStats) {
        if (name == null) {
            name = "";
        }
        this.name = name;
        this.form = form;
        this.baseStats = baseStats;
    }

    public String getName() {
        return this.name;
    }

    public int getForm() {
        return this.form;
    }

    public Optional<BaseStats> getBaseStats() {
        return this.baseStats;
    }

    public void setBaseStats(BaseStats baseStats) {
        this.baseStats = baseStats == null ? Optional.empty() : Optional.of(baseStats);
    }

    public void setBaseStats(Optional<BaseStats> baseStats) {
        this.baseStats = baseStats;
    }
}

