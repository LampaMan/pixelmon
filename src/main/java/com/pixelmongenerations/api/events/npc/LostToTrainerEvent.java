/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.npc;

import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Event;

public class LostToTrainerEvent
extends Event {
    private final EntityPlayerMP player;
    private final NPCTrainer trainer;

    public LostToTrainerEvent(EntityPlayerMP player, NPCTrainer trainer) {
        this.player = player;
        this.trainer = trainer;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public NPCTrainer getTrainer() {
        return this.trainer;
    }
}

