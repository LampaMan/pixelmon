/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.npc;

import com.pixelmongenerations.common.block.machines.BlockVendingMachine;
import com.pixelmongenerations.common.entity.npcs.NPCShopkeeper;
import com.pixelmongenerations.common.entity.npcs.registry.EnumBuySell;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

public class NPCTransactionEvent
extends Event {
    private final EntityPlayerMP player;
    private ItemStack itemStack;
    private int cost;

    private NPCTransactionEvent(EntityPlayerMP player, ItemStack itemStack, int cost) {
        this.player = player;
        this.itemStack = itemStack;
        this.cost = cost;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public ItemStack getItem() {
        return this.itemStack;
    }

    public void setItem(ItemStack itemStack) throws IllegalArgumentException {
        if (itemStack == null) {
            throw new IllegalArgumentException("ItemStack can not be null in setItem");
        }
        this.itemStack = itemStack;
    }

    public int getItemWorth() {
        return this.cost;
    }

    public void setItemWorth(int cost) {
        this.cost = cost;
    }

    @Cancelable
    public static class Vending
    extends NPCTransactionEvent {
        private final BlockVendingMachine block;
        private final BlockPos blockPos;

        public Vending(EntityPlayerMP player, BlockVendingMachine block, BlockPos blockPos, ItemStack itemStack, int cost) {
            super(player, itemStack, cost);
            this.block = block;
            this.blockPos = blockPos;
        }

        public BlockVendingMachine getBlock() {
            return this.block;
        }

        public BlockPos getBlockPos() {
            return this.blockPos;
        }
    }

    @Cancelable
    public static class Shopkeeper
    extends NPCTransactionEvent {
        private final NPCShopkeeper npc;
        private final EnumBuySell type;
        private final int quantity;

        public Shopkeeper(EntityPlayerMP player, NPCShopkeeper npc, EnumBuySell type, ItemStack itemStack, int quantity, int cost) {
            super(player, itemStack, cost);
            this.npc = npc;
            this.type = type;
            this.quantity = quantity;
        }

        public NPCShopkeeper getShopkeeper() {
            return this.npc;
        }

        public EnumBuySell getTransactionType() {
            return this.type;
        }

        public int getQuantity() {
            return this.quantity;
        }
    }
}

