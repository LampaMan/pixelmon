/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.npc;

import com.pixelmongenerations.common.entity.npcs.NPCChatting;
import java.util.ArrayList;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class NPCChatEvent
extends Event {
    public final NPCChatting npc;
    public final EntityPlayer player;
    private ArrayList<String> chatlines;

    public NPCChatEvent(NPCChatting npc, EntityPlayer player, ArrayList<String> chatlines) {
        this.npc = npc;
        this.player = player;
        this.chatlines = chatlines;
    }

    public ArrayList<String> getChat() {
        return this.chatlines;
    }

    public void setChat(ArrayList<String> chatlines) {
        if (chatlines == null || chatlines.isEmpty()) {
            return;
        }
        this.chatlines = chatlines;
    }
}

