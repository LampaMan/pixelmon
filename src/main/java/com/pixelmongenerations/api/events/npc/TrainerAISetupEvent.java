/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.npc;

import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.core.enums.EnumTrainerAI;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraftforge.fml.common.eventhandler.Event;

public class TrainerAISetupEvent
extends Event {
    private NPCTrainer trainer;

    public TrainerAISetupEvent(NPCTrainer trainer) {
        this.trainer = trainer;
    }

    public NPCTrainer getTrainer() {
        return this.trainer;
    }

    public void addTask(int priority, EntityAIBase task) {
        this.trainer.tasks.addTask(priority, task);
    }

    public EnumTrainerAI getAIMode() {
        return this.trainer.getAIMode();
    }

    public void setAIMode(EnumTrainerAI mode) {
        this.trainer.setAIMode(mode);
    }
}

