/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.npc;

import com.pixelmongenerations.core.data.trades.PokemonTrade;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.ArrayList;
import java.util.function.Predicate;
import net.minecraftforge.fml.common.eventhandler.Event;

public class NPCTradesEvent
extends Event {
    private ArrayList<PokemonTrade> trades;

    public NPCTradesEvent(ArrayList<PokemonTrade> trades) {
        this.trades = trades;
    }

    public void addTrade(EnumSpecies wants, EnumSpecies gives) {
        this.trades.add(new PokemonTrade(wants, gives));
    }

    public void removeTrade(Predicate<PokemonTrade> removePred) {
        this.trades.removeIf(removePred);
    }

    public ArrayList<PokemonTrade> getTrades() {
        return this.trades;
    }
}

