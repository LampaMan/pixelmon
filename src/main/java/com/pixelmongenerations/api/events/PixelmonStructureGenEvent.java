/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.world.gen.structure.StructureInfo;
import com.pixelmongenerations.common.world.gen.structure.util.StructureScattered;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

public class PixelmonStructureGenEvent
extends Event {
    private final World world;
    private final StructureScattered structureScattered;
    private final StructureInfo structureData;
    private final BlockPos pos;

    private PixelmonStructureGenEvent(World world, StructureScattered structureScattered, StructureInfo structure, BlockPos pos) {
        this.world = world;
        this.structureScattered = structureScattered;
        this.structureData = structure;
        this.pos = pos;
    }

    public World getWorld() {
        return this.world;
    }

    public StructureScattered getStructureScattered() {
        return this.structureScattered;
    }

    public StructureInfo getStructureData() {
        return this.structureData;
    }

    public BlockPos getPos() {
        return this.pos;
    }

    public static class Post
    extends PixelmonStructureGenEvent {
        public final boolean isGenerated;

        public Post(World world, StructureScattered structureScattered, StructureInfo structure, BlockPos pos, boolean isGenerated) {
            super(world, structureScattered, structure, pos);
            this.isGenerated = isGenerated;
        }
    }

    @Cancelable
    public static class Pre
    extends PixelmonStructureGenEvent {
        public Pre(World world, StructureScattered structureScattered, StructureInfo structure, BlockPos pos) {
            super(world, structureScattered, structure, pos);
        }
    }
}

