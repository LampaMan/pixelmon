/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.block.ranch.RanchBounds;
import com.pixelmongenerations.common.block.tileEntities.TileEntityRanchBase;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.enums.EnumBreedingStrength;
import java.util.UUID;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

public abstract class BreedEvent
extends Event {
    private final UUID owner;
    private final TileEntityRanchBase ranch;

    private BreedEvent(UUID owner, TileEntityRanchBase ranch) {
        this.owner = owner;
        this.ranch = ranch;
    }

    public UUID getOwner() {
        return this.owner;
    }

    public TileEntityRanchBase getRanchTileEntity() {
        return this.ranch;
    }

    public EntityPixelmon getPartner(EntityPixelmon pokemon) {
        return this.ranch.getFirstBreedingPartner(pokemon);
    }

    public static class BreedingTickEvent
    extends BreedEvent {
        private final EntityPixelmon pokemon;
        private int breedingTicks;

        public BreedingTickEvent(UUID owner, TileEntityRanchBase ranch, EntityPixelmon pokemon, int breedingTicks) {
            super(owner, ranch);
            this.pokemon = pokemon;
            this.breedingTicks = breedingTicks;
        }

        public EntityPixelmon getPokemon() {
            return this.pokemon;
        }

        public void setBreedingTicks(int breedingTicks) {
            this.breedingTicks = breedingTicks < 20 ? 20 : breedingTicks;
        }

        public int getBreedingTicks() {
            return this.breedingTicks;
        }
    }

    public static class EnvironmentStrengthEvent
    extends BreedEvent {
        private final EntityPixelmon pokemon;
        private final RanchBounds ranchBounds;
        private float breedingStrength;

        public EnvironmentStrengthEvent(UUID owner, TileEntityRanchBase ranch, EntityPixelmon pokemon, RanchBounds ranchBounds, float breedingStrength) {
            super(owner, ranch);
            this.pokemon = pokemon;
            this.ranchBounds = ranchBounds;
            this.breedingStrength = breedingStrength;
        }

        public EntityPixelmon getPokemon() {
            return this.pokemon;
        }

        public RanchBounds getRanchBounds() {
            return this.ranchBounds;
        }

        public float getBreedingStrength() {
            return this.breedingStrength;
        }

        public void setBreedingStrength(EnumBreedingStrength strength) {
            this.breedingStrength = strength.value;
        }
    }

    public static class BreedingLevelChangedEvent
    extends BreedEvent {
        private final EntityPixelmon pokemon;
        private final int oldLevel;
        private int newLevel;

        public BreedingLevelChangedEvent(UUID owner, TileEntityRanchBase ranch, EntityPixelmon pokemon, int oldLevel, int newLevel) {
            super(owner, ranch);
            this.pokemon = pokemon;
            this.oldLevel = oldLevel;
            this.newLevel = newLevel;
        }

        public EntityPixelmon getPokemon() {
            return this.pokemon;
        }

        public int getOldLevel() {
            return this.oldLevel;
        }

        public int getNewLevel() {
            return this.newLevel;
        }

        public void setNewLevel(int level) {
            if (level < 0) {
                level = 0;
            } else if (level > 5) {
                level = 5;
            }
            this.newLevel = level;
        }
    }

    @Cancelable
    public static class MakeEggEvent
    extends BreedEvent {
        private EntityPixelmon egg;
        private final EntityPixelmon parent1;
        private final EntityPixelmon parent2;

        public MakeEggEvent(UUID owner, TileEntityRanchBase ranch, EntityPixelmon egg, EntityPixelmon parent1, EntityPixelmon parent2) {
            super(owner, ranch);
            this.egg = egg;
            this.parent1 = parent1;
            this.parent2 = parent2;
        }

        public EntityPixelmon getParentOne() {
            return this.parent1;
        }

        public EntityPixelmon getParentTwo() {
            return this.parent2;
        }

        public EntityPixelmon getEgg() {
            return this.egg;
        }

        public void setEgg(EntityPixelmon egg) {
            if (egg != null) {
                this.egg = egg;
            }
        }
    }

    public static class CollectEggEvent
    extends BreedEvent {
        private EntityPixelmon egg;

        public CollectEggEvent(UUID owner, TileEntityRanchBase ranch, EntityPixelmon egg) {
            super(owner, ranch);
            this.egg = egg;
        }

        public void setEgg(EntityPixelmon egg) {
            if (egg != null) {
                this.egg = egg;
            }
        }

        public EntityPixelmon getEgg() {
            return this.egg;
        }
    }
}

