/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

public abstract class SpectateEvent
extends Event {
    private final EntityPlayerMP spectator;
    private final BattleControllerBase battleController;

    private SpectateEvent(EntityPlayerMP spectator, BattleControllerBase battleController) {
        this.spectator = spectator;
        this.battleController = battleController;
    }

    public EntityPlayerMP getSpectator() {
        return this.spectator;
    }

    public BattleControllerBase getBattleController() {
        return this.battleController;
    }

    @Cancelable
    public static class StartSpectate
    extends SpectateEvent {
        private final EntityPlayerMP target;

        public StartSpectate(EntityPlayerMP spectator, BattleControllerBase battleController, EntityPlayerMP target) {
            super(spectator, battleController);
            this.target = target;
        }

        public EntityPlayerMP getTarget() {
            return this.target;
        }
    }

    public static class StopSpectate
    extends SpectateEvent {
        public StopSpectate(EntityPlayerMP spectator, BattleControllerBase battleController) {
            super(spectator, battleController);
        }
    }
}

