/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.battles;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class HordeTriggeredEvent
extends Event {
    private EntityPixelmon hordePokemon;
    private EntityPlayerMP player;

    public HordeTriggeredEvent(EntityPixelmon hordePokemon, EntityPlayerMP player) {
        this.hordePokemon = hordePokemon;
        this.player = player;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public EntityPixelmon getHordePokemon() {
        return this.hordePokemon;
    }

    public void setHordePokemon(EntityPixelmon pokemon) {
        this.hordePokemon = pokemon;
    }

    public World getWorld() {
        return this.player.getEntityWorld();
    }
}

