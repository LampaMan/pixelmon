/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.battles;

import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import net.minecraftforge.fml.common.eventhandler.Event;

public class TurnEndEvent
extends Event {
    private final BattleParticipant participant;
    private final BattleControllerBase bc;

    public TurnEndEvent(BattleControllerBase bc, BattleParticipant participant) {
        this.bc = bc;
        this.participant = participant;
    }

    public BattleParticipant getParticipant() {
        return this.participant;
    }

    public BattleControllerBase getBattleController() {
        return this.bc;
    }
}

