/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.battles;

import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.ai.BattleAIBase;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import net.minecraftforge.fml.common.eventhandler.Event;

public class SetBattleAIEvent
extends Event {
    private final BattleControllerBase bc;
    private final BattleParticipant participant;
    private BattleAIBase ai;

    public SetBattleAIEvent(BattleControllerBase bc, BattleParticipant participant, BattleAIBase ai) {
        this.bc = bc;
        this.participant = participant;
        this.ai = ai;
    }

    public BattleControllerBase getBattleController() {
        return this.bc;
    }

    public BattleParticipant getBattleParticipant() {
        return this.participant;
    }

    public BattleAIBase getAI() {
        return this.ai;
    }

    public void setAI(BattleAIBase ai) {
        if (ai != null) {
            this.ai = ai;
        }
    }
}

