/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.commands;

import com.pixelmongenerations.api.command.PixelmonCommand;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class RegisterSubcommandEvent
extends Event {
    public final PixelmonCommand command;
    public final PixelmonCommand subcommand;

    public RegisterSubcommandEvent(PixelmonCommand command, PixelmonCommand subcommand) {
        this.command = command;
        this.subcommand = subcommand;
    }

    private PixelmonCommand getCommand() {
        return this.command;
    }

    private PixelmonCommand getSubcommand() {
        return this.subcommand;
    }
}

