/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.commands;

import com.pixelmongenerations.api.command.PixelmonCommand;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class DeregisterSubcommandEvent
extends Event {
    private final PixelmonCommand command;
    private final PixelmonCommand subcommand;

    public DeregisterSubcommandEvent(PixelmonCommand command, PixelmonCommand subcommand) {
        this.command = command;
        this.subcommand = subcommand;
    }

    public PixelmonCommand getCommand() {
        return this.command;
    }

    public PixelmonCommand getSubcommand() {
        return this.subcommand;
    }
}

