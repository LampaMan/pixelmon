/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.block.machines.BlockElevator;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.eventhandler.Event;

public class ElevatorEvent
extends Event {
    private final EntityPlayerMP player;
    private final BlockElevator elevator;
    private final boolean goingUp;
    private BlockPos destination;

    public ElevatorEvent(EntityPlayerMP player, BlockElevator elevator, boolean goingUp, BlockPos destination) {
        this.player = player;
        this.elevator = elevator;
        this.goingUp = goingUp;
        this.destination = destination;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public boolean isGoingUp() {
        return this.goingUp;
    }

    public BlockPos getDestination() {
        return this.destination;
    }

    public void setDestination(BlockPos destination) {
        if (destination != null) {
            this.destination = destination;
        }
    }
}

