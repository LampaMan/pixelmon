/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.api.dialogue.Dialogue;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class EquipmentMenuOpenEvent
extends Event {
    private Dialogue.DialogueBuilder dialogueBuilder;
    private final EntityPlayerMP player;

    public EquipmentMenuOpenEvent(Dialogue.DialogueBuilder dialogueBuilder, EntityPlayerMP player) {
        this.dialogueBuilder = dialogueBuilder;
        this.player = player;
    }

    public Dialogue.DialogueBuilder getDialogueBuilder() {
        return this.dialogueBuilder;
    }

    public void setDialogueBuilder(Dialogue.DialogueBuilder dialogueBuilder) {
        this.dialogueBuilder = dialogueBuilder;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }
}

