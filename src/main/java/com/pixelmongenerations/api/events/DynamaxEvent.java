/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class DynamaxEvent
extends Event {
    private int[] pokemonId;
    private static BattleParticipant battleParticipant;

    public DynamaxEvent(int[] pokemonId, BattleParticipant battleParticipant) {
        this.pokemonId = pokemonId;
        DynamaxEvent.battleParticipant = battleParticipant;
    }

    public BattleParticipant getBattleParticipant() {
        return battleParticipant;
    }

    public int[] getPokemonId() {
        return this.pokemonId;
    }

    public static class EvolvingEvent
    extends DynamaxEvent {
        public EvolvingEvent(int[] pokemonId, BattleParticipant participant) {
            super(pokemonId, battleParticipant);
        }
    }
}

