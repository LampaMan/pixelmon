/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.api.spawning.SpawnAction;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.projectiles.EntityHook;
import com.pixelmongenerations.core.enums.items.EnumRodType;
import java.util.Optional;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

public abstract class FishingEvent
extends Event {
    private final EntityPlayerMP player;
    private final EntityHook fishHook;

    protected FishingEvent(EntityPlayerMP player, EntityHook fishHook) {
        this.player = player;
        this.fishHook = fishHook;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public EntityHook getFishHook() {
        return this.fishHook;
    }

    public EnumRodType getRodType() {
        return this.fishHook.rodType;
    }

    public static class FishingCastEvent
    extends FishingEvent {
        private int ticksUntilCatch;
        private float chanceOfNothing;

        public FishingCastEvent(EntityPlayerMP player, EntityHook fishHook, int ticksUntilCatch, float chanceOfNothing) {
            super(player, fishHook);
            this.ticksUntilCatch = ticksUntilCatch;
            this.chanceOfNothing = chanceOfNothing;
        }

        public int getTicksUntilCatch() {
            return this.ticksUntilCatch;
        }

        public void setTicksUntilCatch(int ticksUntilCatch) {
            if (ticksUntilCatch <= 0) {
                ticksUntilCatch = 1;
            }
            this.ticksUntilCatch = ticksUntilCatch;
        }

        public float getChanceOfNothing() {
            return this.chanceOfNothing;
        }

        public void setChanceOfNothing(float chanceOfNothing) {
            this.chanceOfNothing = MathHelper.clamp(chanceOfNothing, 0.0f, 1.0f);
        }
    }

    @Cancelable
    public static class FishingCatchEvent
    extends FishingEvent {
        private SpawnAction<? extends Entity> plannedSpawn;
        private int ticksTillEscape;
        private int displayedMarks;

        public FishingCatchEvent(EntityPlayerMP player, EntityHook fishHook, SpawnAction<? extends Entity> plannedSpawn, int ticksTillEscape, int displayedMarks) {
            super(player, fishHook);
            this.plannedSpawn = plannedSpawn;
            this.ticksTillEscape = ticksTillEscape;
            this.displayedMarks = displayedMarks;
        }

        public SpawnAction<? extends Entity> getPlannedSpawn() {
            return this.plannedSpawn;
        }

        public void setPlannedSpawn(SpawnAction<? extends Entity> spawnAction) {
            this.plannedSpawn = spawnAction;
        }

        public int getDisplayedMarks() {
            return this.displayedMarks;
        }

        public void setDisplayedMarks(int displayedMarks) {
            if (displayedMarks < 0) {
                displayedMarks = 0;
            }
            this.displayedMarks = displayedMarks;
        }

        public int getTicksTillEscape() {
            return this.ticksTillEscape;
        }

        public void setTicksTillEscape(int ticksTillEscape) {
            if (ticksTillEscape < 0 && ticksTillEscape != -1) {
                ticksTillEscape = 0;
            }
            this.ticksTillEscape = ticksTillEscape;
        }
    }

    public static class FishingReelEvent
    extends FishingEvent {
        private final Optional<Entity> entityOpt;

        public FishingReelEvent(EntityPlayerMP player, EntityHook fishHook, Entity entity) {
            super(player, fishHook);
            this.entityOpt = entity == null ? Optional.empty() : Optional.of(entity);
        }

        public Optional<Entity> getEntity() {
            return this.entityOpt;
        }

        public boolean isPokemon() {
            return this.entityOpt.isPresent() && this.entityOpt.get() instanceof EntityPixelmon;
        }
    }
}

