/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.drops;

import com.pixelmongenerations.api.Tuple;
import com.pixelmongenerations.api.drops.PixelmonDrops;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.drops.DroppedItem;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class PokemonDrop {
    private Predicate<EntityPixelmon> filter;
    private ArrayList<Tuple<Supplier<Float>, DroppedItem>> drops;

    private PokemonDrop(Predicate<EntityPixelmon> filter, ArrayList<Tuple<Supplier<Float>, DroppedItem>> drops) {
        this.filter = filter;
        this.drops = drops;
    }

    public boolean accepts(EntityPixelmon pokemon) {
        return this.filter.test(pokemon);
    }

    public ArrayList<DroppedItem> getDrops() {
        ArrayList<DroppedItem> drops = new ArrayList<DroppedItem>();
        this.drops.forEach(container -> {
            float value = container.getFirst().get().floatValue();
            if (RandomHelper.getRandomChance(1.0f / value)) {
                drops.add(container.getSecond());
            }
        });
        return drops;
    }

    public static Builder createBuilder() {
        return new Builder();
    }

    public static class Builder {
        private EnumSpecies[] species;
        private IEnumForm form;
        private Boolean shiny;
        private Boolean totem;
        private Predicate<EntityPixelmon> filter;
        private ArrayList<Tuple<Supplier<Float>, DroppedItem>> drops;

        public Builder species(EnumSpecies ... species) {
            this.species = species;
            return this;
        }

        public Builder form(IEnumForm form) {
            this.form = form;
            return this;
        }

        public Builder shiny(boolean shiny) {
            this.shiny = shiny;
            return this;
        }

        public Builder totem(boolean totem) {
            this.totem = totem;
            return this;
        }

        public Builder drops(ArrayList<Tuple<Supplier<Float>, DroppedItem>> drops) {
            if (this.drops == null) {
                this.drops = drops;
            } else {
                this.drops.addAll(drops);
            }
            return this;
        }

        public Builder drop(Tuple<Supplier<Float>, DroppedItem> drop) {
            if (this.drops == null) {
                this.drops = new ArrayList();
            }
            this.drops.add(drop);
            return this;
        }

        public Builder filter(Predicate<EntityPixelmon> filter) {
            this.filter = filter;
            return this;
        }

        public PokemonDrop build() {
            return this.build(true);
        }

        public PokemonDrop build(boolean register) {
            if (this.drops == null) {
                Pixelmon.LOGGER.error("Tried to build a PokemonDrop but there was no drops attached!");
                this.drops = new ArrayList();
            }
            if (this.filter == null) {
                this.filter = new Predicate<EntityPixelmon>(){

                    @Override
                    public boolean test(EntityPixelmon pokemon) {
                        if (species != null && Stream.of(species).noneMatch(species -> species == pokemon.getSpecies())) {
                            return false;
                        }
                        if (form != null && pokemon.getFormEnum().getForm() != form.getForm()) {
                            return false;
                        }
                        if (shiny != null && pokemon.isShiny() != shiny.booleanValue()) {
                            return false;
                        }
                        return totem == null || pokemon.isTotem() == totem.booleanValue();
                    }
                };
            }
            PokemonDrop drop = new PokemonDrop(this.filter, this.drops);
            if (register) {
                PixelmonDrops.addPokemonDrop(drop);
            }
            return drop;
        }
    }
}

