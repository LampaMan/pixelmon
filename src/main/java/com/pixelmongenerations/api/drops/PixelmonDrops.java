/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.drops;

import com.pixelmongenerations.api.drops.PokemonDrop;
import com.pixelmongenerations.api.events.DropEvent;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import java.util.ArrayList;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class PixelmonDrops {
    private static ArrayList<PokemonDrop> drops = new ArrayList();

    @SubscribeEvent
    public void onDropEvent(DropEvent event) {
        if (event.entity instanceof EntityPixelmon) {
            EntityPixelmon pokemon = (EntityPixelmon)event.entity;
            drops.stream().filter(drop -> drop.accepts(pokemon)).forEach(drop -> drop.getDrops().forEach(item -> event.addDrop(item.copy())));
        }
    }

    public static void addPokemonDrop(PokemonDrop drop) {
        drops.add(drop);
    }
}

