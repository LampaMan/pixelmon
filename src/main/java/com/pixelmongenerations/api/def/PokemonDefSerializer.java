/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.Gson
 *  com.google.gson.GsonBuilder
 */
package com.pixelmongenerations.api.def;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import java.io.Reader;

public class PokemonDefSerializer {
    private static Gson gsonInstance = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();

    public static String serialize(BaseStats def) {
        return gsonInstance.toJson((Object)def);
    }

    public static BaseStats deserialize(String contents) {
        return (BaseStats)gsonInstance.fromJson(contents, BaseStats.class);
    }

    public static BaseStats deserialize(Reader reader) {
        return (BaseStats)gsonInstance.fromJson(reader, BaseStats.class);
    }
}

