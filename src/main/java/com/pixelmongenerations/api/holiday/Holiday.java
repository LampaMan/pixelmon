/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.holiday;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.minecraft.entity.player.EntityPlayerMP;

public class Holiday {
    private List<EnumSpecies> pokemon;
    private String name;

    public Holiday(String name, EnumSpecies ... pokemon) {
        this.name = name;
        this.pokemon = pokemon.length != 0 ? Arrays.asList(pokemon) : new ArrayList<EnumSpecies>();
    }

    public void onWildPokemonSpawn(EntityPlayerMP player, PokemonSpec pokemonSpec) {
    }

    public void onWildPokemonDefeated(EntityPlayerMP player, PixelmonWrapper pokemonWrapper) {
    }

    public boolean isHolidayPokemon(EnumSpecies pokemon) {
        return pokemon != null && (this.pokemon.isEmpty() || this.pokemon.contains((Object)pokemon));
    }

    public boolean isWithinDate(int month, int dayMin, int dayMax) {
        LocalDate date = LocalDate.now();
        int currentMonth = date.getMonthValue();
        int currentDay = date.getDayOfMonth();
        boolean a = currentMonth == month;
        boolean b = currentDay >= dayMin;
        boolean c = currentDay <= dayMax;
        boolean d = b && c;
        boolean e = a && d;
        return e;
    }

    public int getDay() {
        return LocalDate.now().getDayOfMonth();
    }

    public boolean isActive() {
        return false;
    }

    public String getName() {
        return this.name;
    }
}

