/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.world;

import net.minecraft.world.World;

public enum WeatherType {
    CLEAR,
    RAIN,
    STORM;


    public static WeatherType get(World world) {
        if (world.isThundering()) {
            return STORM;
        }
        if (world.isRaining()) {
            return RAIN;
        }
        return CLEAR;
    }
}

