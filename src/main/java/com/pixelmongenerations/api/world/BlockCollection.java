/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.api.world;

import javax.annotation.Nullable;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Biomes;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.Chunk;

public class BlockCollection {
    public final World world;
    public final int minX;
    public final int maxX;
    public final int minY;
    public final int maxY;
    public final int minZ;
    public final int maxZ;
    private IBlockState[][][] blockData;
    private Biome[][] biomeData;

    public BlockCollection(World world, int minX, int maxX, int minY, int maxY, int minZ, int maxZ) {
        this.world = world;
        this.minX = minX;
        this.maxX = maxX;
        this.minY = MathHelper.clamp(minY, 0, 255);
        this.maxY = MathHelper.clamp(maxY, 0, 255);
        this.minZ = minZ;
        this.maxZ = maxZ;
        this.blockData = new IBlockState[maxX - minX + 1][256][maxZ - minZ + 1];
        this.biomeData = new Biome[maxX - minX + 1][maxZ - minZ + 1];
        BlockPos.MutableBlockPos pos = new BlockPos.MutableBlockPos(minX, minY, minZ);
        Chunk chunk = null;
        for (int x = 0; x < maxX - minX + 1; ++x) {
            for (int z = 0; z < maxZ - minZ + 1; ++z) {
                if (chunk == null || x + minX >> 4 != chunk.x || z + minZ >> 4 != chunk.z) {
                    if (!world.isChunkGeneratedAt(x + minX >> 4, z + minZ >> 4)) {
                        this.biomeData[x][z] = Biomes.VOID;
                        continue;
                    }
                    chunk = world.getChunk(pos.setPos(x + minX, maxY, z + minZ));
                }
                this.biomeData[x][z] = chunk.getBiome(pos.setPos(x + minX, 1, z + minZ), world.getBiomeProvider());
                for (int y = 0; y < 256; ++y) {
                    this.blockData[x][y][z] = chunk.getBlockState(x + minX, y, z + minZ);
                }
            }
        }
    }

    public Block getBlock(int x, int y, int z) {
        return this.blockData[x - this.minX][y][z - this.minZ] == null ? Blocks.STONE : this.blockData[x - this.minX][y][z - this.minZ].getBlock();
    }

    @Nullable
    public IBlockState getBlockState(int x, int y, int z) {
        return this.blockData[x - this.minX][y][z - this.minZ];
    }

    public Biome getBiome(int x, int z) {
        return this.biomeData[x - this.minX][z - this.minZ];
    }
}

