/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HexColor {
    private static final Pattern pattern = Pattern.compile("\u00a7#([A-Fa-f0-9]{6})");
    private static final Pattern patternUnformatted = Pattern.compile("#([A-Fa-f0-9]{6})");
    private static final String HEX_PATTERN_UNFORMATTED = "#([A-Fa-f0-9]{6})";
    private static final String HEX_PATTERN = "\u00a7#([A-Fa-f0-9]{6})";

    private HexColor() {
        throw new IllegalStateException("This is a static utility class");
    }

    public static String of(String message) {
        return HexColor.formatHex(message);
    }

    public static boolean isHex(String text) {
        Matcher matcher = pattern.matcher('\u00a7' + text);
        return matcher.matches();
    }

    public static String stripHex(String text) {
        StringBuffer buf = new StringBuffer();
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            matcher.appendReplacement(buf, "");
        }
        matcher.appendTail(buf);
        return buf.toString();
    }

    public static String formatHex(String text) {
        StringBuffer buf = new StringBuffer();
        Matcher matcher = patternUnformatted.matcher(text);
        while (matcher.find()) {
            if (matcher.start(1) == -1) continue;
            matcher.appendReplacement(buf, "\u00a7#" + matcher.group(1));
        }
        matcher.appendTail(buf);
        return buf.toString();
    }
}

