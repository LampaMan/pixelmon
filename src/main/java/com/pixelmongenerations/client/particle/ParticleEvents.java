/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.particle;

import com.pixelmongenerations.client.particle.ParticleArcanery;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.profiler.Profiler;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.lwjgl.opengl.GL11;

public class ParticleEvents {
    public static int arcaneryParticleCount = 0;

    @SubscribeEvent
    public void renderLast(RenderWorldLastEvent event) {
        Profiler profiler = Minecraft.getMinecraft().profiler;
        profiler.startSection("advanced_particles");
        Tessellator tessellator = Tessellator.getInstance();
        GL11.glPushAttrib((int)64);
        GlStateManager.depthMask(false);
        GlStateManager.enableBlend();
        GlStateManager.pushMatrix();
        GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE);
        GlStateManager.alphaFunc(516, 0.003921569f);
        ParticleArcanery.dispatch(tessellator, event.getPartialTicks());
        GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
        GlStateManager.disableBlend();
        GlStateManager.alphaFunc(516, 0.1f);
        GlStateManager.depthMask(true);
        GlStateManager.popMatrix();
        GL11.glPopAttrib();
        profiler.endSection();
    }

    public void drawTexturedModalRect(int x, int y, int textureX, int textureY, int width, int height, double zLevel) {
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuffer();
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
        bufferbuilder.pos(x, y + height, zLevel).tex((float)textureX * 0.00390625f, (float)(textureY + height) * 0.00390625f).endVertex();
        bufferbuilder.pos(x + width, y + height, zLevel).tex((float)(textureX + width) * 0.00390625f, (float)(textureY + height) * 0.00390625f).endVertex();
        bufferbuilder.pos(x + width, y, zLevel).tex((float)(textureX + width) * 0.00390625f, (float)textureY * 0.00390625f).endVertex();
        bufferbuilder.pos(x, y, zLevel).tex((float)textureX * 0.00390625f, (float)textureY * 0.00390625f).endVertex();
        tessellator.draw();
    }
}

