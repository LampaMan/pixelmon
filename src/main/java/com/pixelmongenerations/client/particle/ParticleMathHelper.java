/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.particle;

import java.util.Random;
import net.minecraft.util.math.Vec3d;

public class ParticleMathHelper {
    public static Vec3d generatePointInSphere(double r, Random rand) {
        double radius = (double)((float)rand.nextDouble()) * r;
        double phi = (float)(rand.nextDouble() * Math.PI);
        double theta = (float)(rand.nextDouble() * Math.PI * 2.0);
        double tempsetX = (float)(radius * Math.cos(theta) * Math.sin(phi));
        double tempsetY = (float)(radius * Math.sin(theta) * Math.sin(phi));
        double tempsetZ = (float)(radius * Math.cos(phi));
        return new Vec3d(tempsetX, tempsetY, tempsetZ);
    }
}

