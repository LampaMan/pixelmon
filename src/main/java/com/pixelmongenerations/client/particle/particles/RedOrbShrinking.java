/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.particle.particles;

import com.pixelmongenerations.client.particle.ParticleArcanery;
import com.pixelmongenerations.client.particle.ParticleEffect;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public class RedOrbShrinking
extends ParticleEffect {
    private double tX;
    private double tY;
    private double tZ;
    private static final ResourceLocation tex = new ResourceLocation("pixelmon", "textures/particles/red_orb.png");

    public RedOrbShrinking(double tX, double tY, double tZ) {
        this.tX = tX;
        this.tY = tY;
        this.tZ = tZ;
    }

    @Override
    public void init(ParticleArcanery particle, World world, double x, double y, double z, double vx, double vy, double vz, float size) {
        particle.setRGBA(1.0f, 1.0f, 1.0f, 1.0f);
        particle.setHeading(-(x - this.tX), -(y - this.tY), -(z - this.tZ), 0.4f, 0.0f);
        particle.setScale(0.2f);
        particle.setMaxAge(20);
    }

    @Override
    public void update(ParticleArcanery particle) {
        particle.setPrevPos(particle.getX(), particle.getY(), particle.getZ());
        particle.incrementAge();
        if (particle.getAge() >= particle.getMaxAge()) {
            particle.setExpired();
        }
        particle.move(particle.getMotionX(), particle.getMotionY(), particle.getMotionZ());
        particle.setScale(0.2f * (1.0f - (float)particle.getAge() / (float)particle.getMaxAge()));
    }

    @Override
    public ResourceLocation texture() {
        return tex;
    }
}

