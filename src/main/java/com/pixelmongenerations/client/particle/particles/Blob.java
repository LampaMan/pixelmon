/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.particle.particles;

import com.pixelmongenerations.client.particle.ParticleArcanery;
import com.pixelmongenerations.client.particle.ParticleEffect;
import java.util.Random;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public class Blob
extends ParticleEffect {
    private double random;
    private double mx;
    private double mz;
    private double theta;
    private double wu;
    private double r;
    private float red;
    private float green;
    private float blue;
    private boolean white;
    private int maxAge;
    private int colorPattern;
    private float scale;
    private EnumFacing facing;
    private static final ResourceLocation tex = new ResourceLocation("pixelmon", "textures/particles/ultra.png");

    public Blob(double random, double theta, double wu, double r, float red, float green, float blue, boolean white, int maxAge, int colorPattern, float scale, EnumFacing facing) {
        this.random = random;
        this.theta = theta;
        this.wu = wu;
        this.r = r;
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.white = white;
        this.maxAge = maxAge;
        this.colorPattern = colorPattern;
        this.scale = scale;
        this.facing = facing;
    }

    @Override
    public void preRender(ParticleArcanery particle, float partialTicks) {
        GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
        GlStateManager.color(this.red, this.green, this.blue, particle.getAlphaF());
    }

    @Override
    public void init(ParticleArcanery particle, World world, double x, double y, double z, double vx, double vy, double vz, float size) {
        Random rand = new Random();
        particle.setRGBA(this.white ? 0.9f : this.red, this.white ? 0.9f : this.green, this.white ? 1.0f : this.blue, this.white ? 0.1f : 0.4f);
        particle.setMotion(particle.getMotionX() * (double)0.1f + this.random * (rand.nextGaussian() - 0.5), particle.getMotionY() * (double)0.1f + this.random * (rand.nextGaussian() - 0.5), particle.getMotionZ() * (double)0.1f);
        particle.setMotion(particle.getMotionX() + vx, particle.getMotionY() + vy, particle.getMotionZ() + vz);
        this.mx = vx;
        this.mz = vz;
        particle.setScale(this.scale);
        particle.setMaxAge(this.maxAge);
    }

    @Override
    public void update(ParticleArcanery particle) {
        particle.setPrevPos(particle.getX(), particle.getY(), particle.getZ());
        particle.incrementAge();
        if (particle.getAge() >= particle.getMaxAge()) {
            particle.setExpired();
        }
        float newScale = this.scale * (1.0f - (float)particle.getAge() / (float)particle.getMaxAge());
        particle.setScale(newScale);
        particle.move(particle.getMotionX(), particle.getMotionY(), particle.getMotionZ());
        this.theta += this.wu * (double)particle.getAge() / 2.0;
        switch (this.facing) {
            case NORTH: {
                this.mx = this.r * Math.cos(this.theta) / 18.0;
                this.mz = -this.r * Math.sin(this.theta) / 18.0;
                particle.setMotion(this.mx, this.mz, particle.getMotionZ() - 0.03);
                if (!(particle.getMotionZ() < -0.75)) break;
                particle.setMotion(this.mx, this.mz, -0.75);
                break;
            }
            case SOUTH: {
                this.mx = this.r * -Math.cos(this.theta) / 18.0;
                this.mz = this.r * -Math.sin(this.theta) / 18.0;
                particle.setMotion(this.mx, this.mz, particle.getMotionZ() + 0.03);
                if (!(particle.getMotionZ() > 0.75)) break;
                particle.setMotion(this.mx, this.mz, 0.75);
                break;
            }
            case WEST: {
                this.mx = this.r * Math.cos(this.theta) / 18.0;
                this.mz = this.r * -Math.sin(this.theta) / 18.0;
                particle.setMotion(particle.getMotionX() - 0.03, this.mz, this.mx);
                if (!(particle.getMotionX() < -0.75)) break;
                particle.setMotion(particle.getMotionX() - 0.75, this.mz, this.mx);
                break;
            }
            case EAST: {
                this.mx = this.r * -Math.cos(this.theta) / 18.0;
                this.mz = this.r * -Math.sin(this.theta) / 18.0;
                particle.setMotion(particle.getMotionX() + 0.03, this.mz, this.mx);
                if (!(particle.getMotionX() > 0.75)) break;
                particle.setMotion(particle.getMotionX() + 0.75, this.mz, this.mx);
                break;
            }
        }
        if (this.white) {
            particle.setRGBA(0.9f, 0.9f, 1.0f, 0.1f);
        } else {
            int colorSpeed = 40;
            int colorDeterminant = (particle.getAge() + (int)particle.getWorld().getTotalWorldTime() % colorSpeed) % colorSpeed;
            particle.setRGBA(this.calcR(colorSpeed, colorDeterminant), this.calcG(colorSpeed, colorDeterminant), this.calcB(colorSpeed, colorDeterminant), 0.4f);
        }
    }

    public float calcChangingColor(int colorSpeed, int colorDeterminant) {
        float c1 = 1.0f / (float)colorSpeed;
        float c2 = c1 * (float)colorDeterminant;
        if (c2 > 0.5f) {
            float c3 = c2 - 0.5f;
            c2 -= c3 * 2.0f;
        }
        return c2;
    }

    public float calcR(int colorSpeed, int colorDeterminant) {
        switch (this.colorPattern) {
            case 0: 
            case 1: {
                return this.calcChangingColor(colorSpeed, colorDeterminant);
            }
            case 2: 
            case 3: {
                return 0.2f;
            }
            case 4: 
            case 5: {
                return 0.7f;
            }
        }
        return 0.0f;
    }

    public float calcG(int colorSpeed, int colorDeterminant) {
        switch (this.colorPattern) {
            case 0: 
            case 5: {
                return 0.2f;
            }
            case 1: 
            case 3: {
                return 0.7f;
            }
            case 2: 
            case 4: {
                return this.calcChangingColor(colorSpeed, colorDeterminant);
            }
        }
        return 0.0f;
    }

    public float calcB(int colorSpeed, int colorDeterminant) {
        switch (this.colorPattern) {
            case 0: 
            case 2: {
                return 0.7f;
            }
            case 1: 
            case 4: {
                return 0.2f;
            }
            case 3: 
            case 5: {
                return this.calcChangingColor(colorSpeed, colorDeterminant);
            }
        }
        return 0.0f;
    }

    @Override
    public ResourceLocation texture() {
        return tex;
    }
}

