/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.particle.particles;

import com.pixelmongenerations.client.particle.ParticleArcanery;
import com.pixelmongenerations.client.particle.ParticleEffect;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public class BlueMagic
extends ParticleEffect {
    private static final ResourceLocation tex = new ResourceLocation("pixelmon", "textures/particles/blue_magic.png");

    @Override
    public void init(ParticleArcanery particle, World world, double x, double y, double z, double vx, double vy, double vz, float size) {
        particle.setRGBA(1.0f, 1.0f, 1.0f, 0.5f);
        particle.setMotion(particle.getMotionX() * (double)0.1f, particle.getMotionY() * (double)0.1f, particle.getMotionZ() * (double)0.1f);
        particle.setMotion(particle.getMotionX() + vx * 0.4, particle.getMotionY() + vy * 0.4, particle.getMotionZ() + vz * 0.4);
        particle.setScale(0.1f);
        particle.setMaxAge((int)(6.0 / (Math.random() * 0.8 + 0.6)));
        particle.setMaxAge((int)((float)particle.getMaxAge() * size));
    }

    @Override
    public void update(ParticleArcanery particle) {
        particle.setPrevPos(particle.getX(), particle.getY(), particle.getZ());
        particle.incrementAge();
        if (particle.getAge() >= particle.getMaxAge()) {
            particle.setExpired();
        }
        particle.move(particle.getMotionX(), particle.getMotionY(), particle.getMotionZ());
        particle.setMotion(particle.getMotionX() * (double)0.7f, particle.getMotionY() * (double)0.7f - (double)0.02f, particle.getMotionZ() * (double)0.7f);
        if (particle.onGround()) {
            particle.setMotion(particle.getMotionX() * (double)0.7f, particle.getMotionY(), particle.getMotionZ() * (double)0.7f);
        }
    }

    @Override
    public ResourceLocation texture() {
        return tex;
    }
}

