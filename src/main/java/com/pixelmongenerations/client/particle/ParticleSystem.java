/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.particle;

import net.minecraft.client.Minecraft;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class ParticleSystem {
    @SideOnly(value=Side.CLIENT)
    public void execute(Minecraft mc, World w, double x, double y, double z, float scale, boolean shiny, double ... args) {
    }
}

