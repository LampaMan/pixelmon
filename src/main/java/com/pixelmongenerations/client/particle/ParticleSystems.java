/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.particle;

import com.pixelmongenerations.client.particle.ParticleSystem;
import com.pixelmongenerations.client.particle.systems.Bloom;
import com.pixelmongenerations.client.particle.systems.Discharge;
import com.pixelmongenerations.client.particle.systems.Drain;
import com.pixelmongenerations.client.particle.systems.Heal;
import com.pixelmongenerations.client.particle.systems.RadialThunder;
import com.pixelmongenerations.client.particle.systems.RedChainPortal;
import com.pixelmongenerations.client.particle.systems.SlingRing;
import com.pixelmongenerations.client.particle.systems.UltraWormhole;

public enum ParticleSystems {
    HEAL(new Heal()),
    REDCHAINPORTAL(new RedChainPortal()),
    DRAIN(new Drain()),
    BLOOM(new Bloom()),
    DISCHARGE(new Discharge()),
    RADIALTHUNDER(new RadialThunder()),
    SLINGRING(new SlingRing()),
    ULTRAWORMHOLE(new UltraWormhole());

    private ParticleSystem system;

    private ParticleSystems(ParticleSystem system) {
        this.system = system;
    }

    public ParticleSystem getSystem() {
        return this.system;
    }

    public static ParticleSystem get(int i) {
        return ParticleSystems.values()[i].system;
    }
}

