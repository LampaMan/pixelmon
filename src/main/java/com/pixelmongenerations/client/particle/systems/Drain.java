/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.particle.systems;

import com.pixelmongenerations.client.particle.ParticleArcanery;
import com.pixelmongenerations.client.particle.ParticleSystem;
import com.pixelmongenerations.client.particle.particles.RedOrbShrinking;
import java.util.Random;
import net.minecraft.client.Minecraft;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class Drain
extends ParticleSystem {
    @Override
    @SideOnly(value=Side.CLIENT)
    public void execute(Minecraft mc, World w, double x, double y, double z, float scale, boolean shiny, double ... args) {
        int totalPoints = 3;
        for (int i = 1; i <= totalPoints; ++i) {
            Random rand = new Random();
            mc.effectRenderer.addEffect(new ParticleArcanery(w, x + rand.nextDouble() - 0.5, y + rand.nextDouble() - 0.5, z + rand.nextDouble() - 0.5, 0.0, 0.0, 0.0, new RedOrbShrinking(args[0], args[1], args[2])));
        }
    }
}

