/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.particle.systems;

import com.pixelmongenerations.client.particle.ParticleArcanery;
import com.pixelmongenerations.client.particle.ParticleSystem;
import com.pixelmongenerations.client.particle.particles.CycloneBlob;
import net.minecraft.client.Minecraft;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class RedChainPortal
extends ParticleSystem {
    @Override
    @SideOnly(value=Side.CLIENT)
    public void execute(Minecraft mc, World w, double x, double y, double z, float scale, boolean shiny, double ... args) {
        int totalPoints = 48;
        for (int i = 1; i <= totalPoints; ++i) {
            double theta = Math.PI * 2 / (double)totalPoints;
            double angle = theta * (double)i;
            double radius = 4.0;
            double dx = -(radius * Math.cos(angle));
            double dz = -(radius * Math.sin(angle));
            mc.effectRenderer.addEffect(new ParticleArcanery(w, x + dx, y + 0.025, z + dz, dz / 6.0, 0.0, -dx / 6.0, new CycloneBlob(0.35, angle, 0.01, radius, args[0])));
        }
    }
}

