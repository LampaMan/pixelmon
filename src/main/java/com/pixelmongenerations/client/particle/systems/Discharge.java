/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.particle.systems;

import com.pixelmongenerations.client.particle.ParticleArcanery;
import com.pixelmongenerations.client.particle.ParticleSystem;
import com.pixelmongenerations.client.particle.particles.Electric;
import net.minecraft.client.Minecraft;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class Discharge
extends ParticleSystem {
    @Override
    @SideOnly(value=Side.CLIENT)
    public void execute(Minecraft mc, World w, double x, double y, double z, float scale, boolean shiny, double ... args) {
        for (int i = 0; i < 200; ++i) {
            double posX = x - 0.5 + w.rand.nextDouble();
            double posY = y + 1.0 + w.rand.nextDouble();
            double posZ = z - 0.5 + w.rand.nextDouble();
            ParticleArcanery parent = new ParticleArcanery(w, posX, posY, posZ, posX, posY, posZ, new Electric(13 + w.rand.nextInt(4), true, w.rand.nextFloat() * 360.0f, w.rand.nextFloat() * 360.0f, 1.0f, 0.5f, args[3] == 1.0 ? w.rand.nextFloat() : (float)args[0], args[3] == 1.0 ? w.rand.nextFloat() : (float)args[1], args[3] == 1.0 ? w.rand.nextFloat() : (float)args[2]));
            Minecraft.getMinecraft().effectRenderer.addEffect(parent);
        }
    }
}

