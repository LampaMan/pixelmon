/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.particle.systems;

import com.pixelmongenerations.client.particle.ParticleArcanery;
import com.pixelmongenerations.client.particle.ParticleSystem;
import com.pixelmongenerations.client.particle.particles.Blob;
import com.pixelmongenerations.client.particle.particles.Electric;
import net.minecraft.client.Minecraft;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class UltraWormhole
extends ParticleSystem {
    @Override
    @SideOnly(value=Side.CLIENT)
    public void execute(Minecraft mc, World w, double x, double y, double z, float scale, boolean shiny, double ... args) {
        double posZ;
        double posY;
        double posX;
        double rotation = args[1] + 180.0;
        EnumFacing facing = EnumFacing.fromAngle(rotation);
        byte centerBlobOffsetX = (byte)(facing.getDirectionVec().getX() * 7);
        byte lightningOffsetX = (byte)(facing.getDirectionVec().getX() * 8);
        byte centerBlobOffsetZ = (byte)(facing.getDirectionVec().getZ() * 7);
        byte lightningOffsetZ = (byte)(facing.getDirectionVec().getZ() * 8);
        double rad = 2.0;
        for (int i = 1; i <= 20; ++i) {
            posX = w.rand.nextDouble() * rad * 2.0 - rad;
            if (!(Math.sqrt(posX * posX + (posY = w.rand.nextDouble() * rad * 2.0 - rad) * posY + (posZ = w.rand.nextDouble() * rad * 2.0 - rad) * posZ) <= rad)) continue;
            mc.effectRenderer.addEffect(new ParticleArcanery(w, x + posX + (double)centerBlobOffsetX, y + posY, z + posZ + (double)centerBlobOffsetZ, 0.0, 0.0, 0.0, new Blob(0.0, 0.0, 0.0, 0.0, 0.0f, 0.0f, 0.0f, true, 20, (int)args[0], 0.7f, facing)));
        }
        int totalPoints = 26;
        for (int i = 1; i <= totalPoints; ++i) {
            double theta = Math.PI * 2 / (double)totalPoints;
            double angle = theta * (double)i;
            double radius = 4.0;
            double dx = radius * Math.cos(angle);
            double dy = radius * Math.sin(angle);
            double dz = dx * (double)facing.getDirectionVec().getX();
            mc.effectRenderer.addEffect(new ParticleArcanery(w, x + (dx *= (double)facing.getDirectionVec().getZ()), y + dy, z + dz, 0.0, 0.0, 0.0, new Blob(0.0, angle, 0.0, 2.0, 0.2f, 0.0f, 0.2f, false, 30, (int)args[0], 0.7f, facing)));
        }
        if (w.rand.nextDouble() < 0.1) {
            posX = x - 1.0 + w.rand.nextDouble() * 2.0 + (double)lightningOffsetX;
            posY = y - 1.0 + w.rand.nextDouble() * 2.0;
            posZ = z - 1.0 + w.rand.nextDouble() * 2.0 + (double)lightningOffsetZ;
            ParticleArcanery parent = new ParticleArcanery(w, posX, posY, posZ, posX, posY, posZ, new Electric(40 + w.rand.nextInt(2), true, w.rand.nextFloat() * 40.0f + 90.0f + 70.0f + (float)rotation, w.rand.nextFloat() * 40.0f + 90.0f + 70.0f + (float)rotation, 0.15f, 0.4f, 0.3f, 0.3f, 0.7f, 0.75f));
            Minecraft.getMinecraft().effectRenderer.addEffect(parent);
        }
    }
}

