/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.particle.systems;

import com.pixelmongenerations.client.particle.ParticleArcanery;
import com.pixelmongenerations.client.particle.ParticleSystem;
import com.pixelmongenerations.client.particle.particles.SmallRising;
import net.minecraft.client.Minecraft;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class Heal
extends ParticleSystem {
    @Override
    @SideOnly(value=Side.CLIENT)
    public void execute(Minecraft mc, World w, double x, double y, double z, float scale, boolean shiny, double ... args) {
        int totalPoints = 72;
        for (int i = 1; i <= totalPoints; ++i) {
            double theta = Math.PI * 2 / (double)totalPoints;
            double angle = theta * (double)i;
            double dx = 0.75 * Math.cos(angle);
            double dz = 0.75 * Math.sin(angle);
            for (int j = 1; j <= 6; ++j) {
                mc.effectRenderer.addEffect(new ParticleArcanery(w, x + dx, y, z + dz, 0.0, 0.0, 0.0, new SmallRising(1.0f, 1.0f, 0.0f, 0.5f)));
            }
        }
    }
}

