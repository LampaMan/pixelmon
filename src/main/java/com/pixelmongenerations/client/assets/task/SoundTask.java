/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.assets.task;

import com.pixelmongenerations.client.assets.resource.SoundResource;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.data.asset.ManifestTask;
import com.pixelmongenerations.core.data.asset.entry.SoundAsset;
import com.pixelmongenerations.core.data.asset.manifest.SoundManifest;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import net.minecraft.client.Minecraft;

public class SoundTask
extends TimerTask
implements ManifestTask<SoundManifest> {
    private ExecutorService executorService = Executors.newFixedThreadPool(10);
    private ConcurrentLinkedQueue<SoundResource> buffer;
    private ConcurrentLinkedQueue<SoundResource> cacheQueue;
    private int waitTime = 10;

    @Override
    public void formBuffer(SoundManifest manifest) {
        this.buffer = new ConcurrentLinkedQueue();
        this.cacheQueue = new ConcurrentLinkedQueue();
        for (SoundAsset entry : manifest.getAssets()) {
            if (entry.soundId == null || entry.soundName == null) {
                Pixelmon.LOGGER.error("Prevented Invalid SoundEntry: " + entry.toString());
                continue;
            }
            this.buffer.add(new SoundResource(entry));
        }
        Pixelmon.LOGGER.info(String.format("Running task for %s sound(s) from manifest.", this.getRemainingItems()));
        while (this.buffer.peek() != null) {
            SoundResource resource = this.buffer.poll();
            this.executorService.submit(() -> {
                try {
                    resource.download();
                    this.cacheQueue.add(resource);
                }
                catch (Exception e) {
                    Pixelmon.LOGGER.error("Exception when downloading SoundResource: " + resource.entry);
                    e.printStackTrace();
                }
            });
        }
    }

    @Override
    public int getRemainingItems() {
        return this.buffer.size();
    }

    @Override
    public void run() {
        if (Minecraft.getMinecraft().player == null || Minecraft.getMinecraft().player.connection == null) {
            this.buffer.clear();
            this.executorService.shutdown();
            this.cancel();
            Pixelmon.LOGGER.info("Shutdown SoundTask due to leaving the server.");
            return;
        }
        if (this.buffer.isEmpty() && this.cacheQueue.isEmpty()) {
            if (this.waitTime == 0) {
                this.cancel();
                this.executorService.shutdown();
                Pixelmon.LOGGER.info("Completed loading of the sound manifest.");
                return;
            }
            --this.waitTime;
        } else {
            this.waitTime = 10;
        }
        while (this.cacheQueue.peek() != null) {
            SoundResource resource = this.cacheQueue.peek();
            if (!resource.getState().shouldRemove()) continue;
            this.cacheQueue.poll();
        }
    }
}

