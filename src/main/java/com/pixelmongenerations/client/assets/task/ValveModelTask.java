/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.assets.task;

import com.pixelmongenerations.client.assets.resource.ValveModelResource;
import com.pixelmongenerations.client.models.PixelmonModelRegistry;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.data.asset.AssetState;
import com.pixelmongenerations.core.data.asset.ManifestTask;
import com.pixelmongenerations.core.data.asset.entry.ValveModelAsset;
import com.pixelmongenerations.core.data.asset.manifest.ValveModelManifest;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import net.minecraft.client.Minecraft;

public class ValveModelTask
extends TimerTask
implements ManifestTask<ValveModelManifest> {
    private ExecutorService executorService = Executors.newFixedThreadPool(10);
    private ConcurrentLinkedQueue<ValveModelResource> buffer;
    private ConcurrentLinkedQueue<ValveModelResource> cacheQueue;
    private int waitTime = 10;

    @Override
    public void formBuffer(ValveModelManifest manifest) {
        this.buffer = new ConcurrentLinkedQueue();
        this.cacheQueue = new ConcurrentLinkedQueue();
        for (ValveModelAsset entry : manifest.getAssets()) {
            if (entry.modelId == null || entry.modelName == null) {
                Pixelmon.LOGGER.error("Prevented Invalid ModelEntry: " + entry.toString());
                continue;
            }
            this.buffer.add(new ValveModelResource(entry));
        }
        Pixelmon.LOGGER.info(String.format("Running task for %s model(s) from manifest.", this.getRemainingItems()));
        while (this.buffer.peek() != null) {
            ValveModelResource resource = this.buffer.poll();
            this.executorService.submit(() -> {
                try {
                    resource.download();
                    this.cacheQueue.add(resource);
                }
                catch (Exception e) {
                    Pixelmon.LOGGER.error("Exception when downloading Valve Model Asset: " + resource.entry);
                    e.printStackTrace();
                }
            });
        }
    }

    @Override
    public int getRemainingItems() {
        return this.buffer.size();
    }

    @Override
    public void run() {
        if (Minecraft.getMinecraft().player == null || Minecraft.getMinecraft().player.connection == null) {
            this.buffer.clear();
            this.executorService.shutdown();
            this.cancel();
            Pixelmon.LOGGER.info("Shutdown valve model task due to leaving the server.");
            return;
        }
        if (this.buffer.isEmpty() && this.cacheQueue.isEmpty()) {
            if (this.waitTime == 0) {
                this.cancel();
                this.executorService.shutdown();
                Pixelmon.LOGGER.info("Completed loading of the valve model manifest.");
                return;
            }
            --this.waitTime;
        } else {
            this.waitTime = 10;
        }
        while (this.cacheQueue.peek() != null) {
            ValveModelResource resource = this.cacheQueue.peek();
            if (resource.getState() == AssetState.Downloaded || resource.getState() == AssetState.Loading || resource.getState() == AssetState.Loaded) {
                ClientProxy.VALVE_MODEL_STORE.addObject(resource.getKey(), resource);
                if (resource.getPokemon() != null) {
                    PixelmonModelRegistry.addDynamicSpecialTextureModel(resource);
                }
            }
            if (!resource.getState().shouldRemove()) continue;
            this.cacheQueue.poll();
        }
    }
}

