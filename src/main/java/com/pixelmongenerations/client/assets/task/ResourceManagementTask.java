/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.assets.task;

import com.pixelmongenerations.core.data.asset.DynamicAsset;
import com.pixelmongenerations.core.data.asset.IDynamicAsset;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;
import net.minecraft.client.Minecraft;

public class ResourceManagementTask
extends TimerTask {
    private ConcurrentLinkedQueue<DynamicAsset> buffer = new ConcurrentLinkedQueue();
    private int counter = 0;

    @Override
    public void run() {
        ++this.counter;
        if (this.counter >= 600) {
            this.counter = 0;
            Minecraft.getMinecraft().addScheduledTask(() -> {
                ClientProxy.MINECRAFT_MODEL_STORE.checkForExpiredObjects();
                ClientProxy.VALVE_MODEL_STORE.checkForExpiredObjects();
                ClientProxy.TEXTURE_STORE.checkForExpiredObjects();
            });
        }
        if (this.buffer.isEmpty()) {
            return;
        }
        while (!this.buffer.isEmpty()) {
            IDynamicAsset asset = this.buffer.poll();
            asset.load();
        }
    }

    public void queueLoad(DynamicAsset asset) {
        this.buffer.add(asset);
    }
}

