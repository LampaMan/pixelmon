/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.apache.commons.io.FileUtils
 *  org.apache.commons.io.FilenameUtils
 *  org.apache.commons.io.IOUtils
 */
package com.pixelmongenerations.client.assets.resource;

import com.pixelmongenerations.client.util.TextureAtlasDynamic;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.data.asset.AssetState;
import com.pixelmongenerations.core.data.asset.DynamicAsset;
import com.pixelmongenerations.core.data.asset.entry.MinecraftModelAsset;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ModelBlock;
import net.minecraft.client.renderer.block.model.ModelManager;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.model.ModelRotation;
import net.minecraft.client.resources.IResource;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

public class MinecraftModelResource
extends DynamicAsset<MinecraftModelAsset> {
    private URL modelURL;
    private IBakedModel model;
    public static ModelManager modelManager;
    public static ModelLoader modelLoader;

    public MinecraftModelResource(MinecraftModelAsset entry) {
        super(entry);
        this.setState(AssetState.Start);
        this.formURLs();
    }

    @Override
    public void formURLs() {
        try {
            if (((MinecraftModelAsset)this.entry).modelURL != null) {
                this.modelURL = new URL(((MinecraftModelAsset)this.entry).modelURL);
            }
        }
        catch (MalformedURLException e) {
            Pixelmon.LOGGER.error("ModelEntry with id '%s' has a Invalid URL and was prevented from loading.", (Object)((MinecraftModelAsset)this.entry).modelId);
            this.setState(AssetState.Failed);
        }
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    @Override
    public void download() {
        if (this.getState() == AssetState.Failed) {
            return;
        }
        this.setState(AssetState.Downloading);
        NetHandlerPlayClient netHandler = Minecraft.getMinecraft().player.connection;
        String remoteAdd = netHandler.getNetworkManager().getRemoteAddress().toString();
        if (remoteAdd.contains("/")) {
            remoteAdd = remoteAdd.split("/")[1];
        }
        remoteAdd = remoteAdd.replaceAll(":", "-");
        File dir = new File("./resource-cache/" + (remoteAdd.contains("local") ? "singleplayer" : remoteAdd) + "/");
        dir.mkdirs();
        try {
            File modelFile = new File(dir, ((MinecraftModelAsset)this.entry).modelId + ".json");
            if (!modelFile.exists()) {
                if (!FilenameUtils.getExtension((String)modelFile.getPath()).equalsIgnoreCase("json")) {
                    Pixelmon.LOGGER.error(String.format("Skipped download of non Json file: %s", this.modelURL.getPath()));
                    this.setState(AssetState.Failed);
                    return;
                }
                FileUtils.copyURLToFile((URL)this.modelURL, (File)modelFile, (int)5000, (int)5000);
                this.setState(AssetState.Downloaded);
            } else {
                this.setState(AssetState.Downloaded);
            }
            if (!((MinecraftModelAsset)this.entry).preLoad) return;
            this.load();
            return;
        }
        catch (Exception e) {
            e.printStackTrace();
            this.setState(AssetState.Failed);
        }
    }

    @Override
    public void load() {
        this.setState(AssetState.Loading);
        ResourceLocation modelLocation = new ResourceLocation("dynpixelmon:customassets/" + this.getKey() + ".json");
        try {
            ResourceLocation textureLocation = new ResourceLocation("dynpixelmon:customicon/" + ((MinecraftModelAsset)this.entry).defaultTextureName + ".png");
            while (!Pixelmon.PROXY.resourceLocationExists(modelLocation)) {
                Thread.sleep(2000L);
            }
            TextureAtlasDynamic sprite = new TextureAtlasDynamic(textureLocation);
            MinecraftModelResource.modelLoader.sprites.put(textureLocation, sprite);
            ModelBlock loadedModel = this.loadModel(modelLocation);
            this.model = modelLoader.bakeModel(loadedModel, ModelRotation.X0_Y0, false);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        if (this.model != null) {
            this.setState(AssetState.Loaded);
            ModelResourceLocation modelresourcelocation = new ModelResourceLocation(modelLocation.toString(), null);
            MinecraftModelResource.modelManager.modelRegistry.putObject(modelresourcelocation, this.model);
            return;
        }
        this.setState(AssetState.Failed);
    }

    @Override
    public void unload() {
        this.model = null;
        this.setState(AssetState.Downloaded);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected ModelBlock loadModel(ResourceLocation location) throws IOException {
        IResource iresource;
        InputStreamReader reader;
        block2: {
            ModelBlock modelBlock;
            reader = null;
            iresource = null;
            try {
                ModelBlock modelblock1;
                String s = location.getPath();
                if ("builtin/generated".equals(s)) break block2;
                iresource = Minecraft.getMinecraft().resourceManager.getResource(location);
                reader = new InputStreamReader(iresource.getInputStream(), StandardCharsets.UTF_8);
                ModelBlock lvt_5_2_ = ModelBlock.deserialize(reader);
                lvt_5_2_.name = location.toString();
                modelBlock = modelblock1 = lvt_5_2_;
            }
            catch (Throwable throwable) {
                IOUtils.closeQuietly(reader);
                IOUtils.closeQuietly(iresource);
                throw throwable;
            }
            IOUtils.closeQuietly((Reader)reader);
            IOUtils.closeQuietly((Closeable)iresource);
            return modelBlock;
        }
        IOUtils.closeQuietly(reader);
        IOUtils.closeQuietly(iresource);
        return null;
    }

    public IBakedModel getModel() {
        try {
            switch (this.getState()) {
                case Loaded: {
                    return this.model;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getKey() {
        return ((MinecraftModelAsset)this.entry).modelId;
    }

    @Override
    public String getName() {
        return ((MinecraftModelAsset)this.entry).modelName;
    }
}

