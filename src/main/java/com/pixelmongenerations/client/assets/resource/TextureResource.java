/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.apache.commons.io.FilenameUtils
 */
package com.pixelmongenerations.client.assets.resource;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.data.asset.AssetState;
import com.pixelmongenerations.core.data.asset.DynamicAsset;
import com.pixelmongenerations.core.data.asset.entry.TextureAsset;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.imageio.ImageIO;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.client.renderer.texture.DynamicTexture;
import org.apache.commons.io.FilenameUtils;

public class TextureResource
extends DynamicAsset<TextureAsset> {
    private URL textureURL;
    private URL glowURL;
    private DynamicTexture imageTexture;
    private DynamicTexture glowImageTexture;

    public TextureResource(TextureAsset entry) {
        super(entry);
        this.setState(AssetState.Start);
        this.formURLs();
    }

    @Override
    public void formURLs() {
        try {
            if (((TextureAsset)this.entry).textureURL != null) {
                this.textureURL = new URL(((TextureAsset)this.entry).textureURL);
            }
            if (((TextureAsset)this.entry).glowURL != null) {
                this.glowURL = new URL(((TextureAsset)this.entry).glowURL);
            }
        }
        catch (MalformedURLException e) {
            Pixelmon.LOGGER.error("TextureEntry with id '" + ((TextureAsset)this.entry).textureId + "' has a Invalid URL and was prevented from loading.");
            this.setState(AssetState.Failed);
        }
    }

    @Override
    public void download() {
        boolean exists;
        Image image;
        File dynImage;
        String textureName;
        if (this.getState() == AssetState.Failed) {
            return;
        }
        this.setState(AssetState.Downloading);
        if (Minecraft.getMinecraft().player == null || Minecraft.getMinecraft().player.connection == null) {
            this.setState(AssetState.Failed);
            return;
        }
        NetHandlerPlayClient netHandler = Minecraft.getMinecraft().player.connection;
        String remoteAdd = netHandler.getNetworkManager().getRemoteAddress().toString();
        if (remoteAdd.contains("/")) {
            remoteAdd = remoteAdd.split("/")[1];
        }
        remoteAdd = remoteAdd.replaceAll(":", "-");
        File dir = new File("./resource-cache/" + (remoteAdd.contains("local") ? "singleplayer" : remoteAdd) + "/");
        dir.mkdirs();
        try {
            textureName = ((TextureAsset)this.entry).textureId + ".png";
            dynImage = new File(dir, textureName);
            image = null;
            if (!dynImage.exists() || ((TextureAsset)this.entry).override) {
                if (FilenameUtils.getExtension((String)this.textureURL.getPath()).equalsIgnoreCase("png")) {
                    image = ImageIO.read(this.textureURL);
                    if (image != null) {
                        ImageIO.write((RenderedImage)((Object)image), "png", dynImage);
                    }
                } else {
                    Pixelmon.LOGGER.error(String.format("Skipped download of non PNG file: %s", textureName));
                    this.setState(AssetState.Failed);
                    return;
                }
            }
            if (image != null) {
                image.flush();
            }
            this.setState((exists = dynImage.exists()) ? AssetState.Downloaded : AssetState.Failed);
        }
        catch (IOException e) {
            Pixelmon.LOGGER.error(((TextureAsset)this.entry).toString());
            e.printStackTrace();
            this.setState(AssetState.Failed);
        }
        if (this.glowURL != null) {
            textureName = ((TextureAsset)this.entry).textureId + "-glow.png";
            try {
                dynImage = new File(dir, textureName);
                image = null;
                if (!dynImage.exists() || ((TextureAsset)this.entry).override) {
                    if (FilenameUtils.getExtension((String)this.glowURL.getPath()).equalsIgnoreCase("png")) {
                        image = ImageIO.read(this.glowURL);
                        if (image != null) {
                            ImageIO.write((RenderedImage)((Object)image), "png", dynImage);
                        }
                    } else {
                        Pixelmon.LOGGER.error(String.format("Skipped download of non PNG file: %s", textureName));
                        this.setState(AssetState.Failed);
                        return;
                    }
                }
                if (image != null) {
                    image.flush();
                }
                this.setState((exists = dynImage.exists()) ? AssetState.Downloaded : AssetState.Failed);
            }
            catch (IOException e) {
                Pixelmon.LOGGER.error(((TextureAsset)this.entry).toString());
                e.printStackTrace();
                this.setState(AssetState.Failed);
            }
        }
        if (((TextureAsset)this.entry).preLoad) {
            this.load();
        }
    }

    @Override
    public void load() {
        BufferedImage image;
        File dynImage;
        String textureName;
        this.setState(AssetState.Loading);
        NetHandlerPlayClient netHandler = Minecraft.getMinecraft().player.connection;
        String remoteAdd = netHandler.getNetworkManager().getRemoteAddress().toString();
        if (remoteAdd.contains("/")) {
            remoteAdd = remoteAdd.split("/")[1];
        }
        remoteAdd = remoteAdd.replaceAll(":", "-");
        File dir = new File("./resource-cache/" + (remoteAdd.contains("local") ? "singleplayer" : remoteAdd) + "/");
        dir.mkdirs();
        try {
            textureName = ((TextureAsset)this.entry).textureId + ".png";
            dynImage = new File(dir, textureName);
            image = null;
            if (dynImage.exists()) {
                if (FilenameUtils.getExtension((String)dynImage.getPath()).equalsIgnoreCase("png")) {
                    image = ImageIO.read(dynImage);
                } else {
                    Pixelmon.LOGGER.error(String.format("Skipped load of non PNG file: %s", textureName));
                    this.setState(AssetState.Failed);
                    return;
                }
            }
            if (image == null) {
                this.setState(AssetState.Failed);
                Pixelmon.LOGGER.error(String.format("Failed to load image: %s", textureName));
                return;
            }
            BufferedImage finalImage = image;
            Minecraft.getMinecraft().addScheduledTask(() -> {
                this.imageTexture = new DynamicTexture(finalImage);
                this.setState(AssetState.Loaded);
            });
        }
        catch (IOException e) {
            Pixelmon.LOGGER.error(((TextureAsset)this.entry).toString());
            e.printStackTrace();
            this.setState(AssetState.Failed);
        }
        if (this.glowURL != null) {
            try {
                textureName = ((TextureAsset)this.entry).textureId + "-glow.png";
                dynImage = new File(dir, textureName);
                image = null;
                if (dynImage.exists()) {
                    if (FilenameUtils.getExtension((String)dynImage.getPath()).equalsIgnoreCase("png")) {
                        image = ImageIO.read(dynImage);
                    } else {
                        Pixelmon.LOGGER.error(String.format("Skipped load of non PNG file: %s", textureName));
                        this.setState(AssetState.Failed);
                        return;
                    }
                }
                if (image == null) {
                    this.setState(AssetState.Failed);
                    Pixelmon.LOGGER.error(String.format("Failed to load image: %s", textureName));
                    return;
                }
                BufferedImage finalImage = image;
                Minecraft.getMinecraft().addScheduledTask(() -> {
                    this.glowImageTexture = new DynamicTexture(finalImage);
                    this.setState(AssetState.Loaded);
                });
            }
            catch (IOException e) {
                Pixelmon.LOGGER.error(((TextureAsset)this.entry).toString());
                e.printStackTrace();
                this.setState(AssetState.Failed);
            }
        }
    }

    @Override
    public void unload() {
        this.imageTexture = null;
        this.glowImageTexture = null;
        this.setState(AssetState.Downloaded);
    }

    @Override
    public String getKey() {
        return ((TextureAsset)this.entry).textureId;
    }

    @Override
    public String getName() {
        return ((TextureAsset)this.entry).textureName;
    }

    public boolean bindTexture() {
        return this.bindTexture(false);
    }

    public boolean bindTexture(boolean glow) {
        try {
            switch (this.getState()) {
                case Loaded: {
                    if (!glow) {
                        this.imageTexture.updateDynamicTexture();
                        break;
                    }
                    this.glowImageTexture.updateDynamicTexture();
                    break;
                }
                case Downloaded: {
                    this.imageTexture = this.getTexture(glow);
                    if (this.imageTexture == null) break;
                    this.setState(AssetState.Loaded);
                    break;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public DynamicTexture getTexture() {
        return this.getTexture(false);
    }

    public DynamicTexture getTexture(boolean glow) {
        if (!glow && this.imageTexture != null) {
            return this.imageTexture;
        }
        if (this.glowImageTexture != null) {
            return this.glowImageTexture;
        }
        return null;
    }

    public boolean hasGlow() {
        return this.glowURL != null;
    }

    public boolean hasGlowRainbow() {
        return ((TextureAsset)this.entry).glowRainbow;
    }
}

