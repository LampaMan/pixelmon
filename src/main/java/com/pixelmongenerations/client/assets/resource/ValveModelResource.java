/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.apache.commons.io.FileUtils
 *  org.apache.commons.io.FilenameUtils
 */
package com.pixelmongenerations.client.assets.resource;

import com.pixelmongenerations.client.models.obj.ObjLoader;
import com.pixelmongenerations.client.models.smd.ValveStudioModelLoader;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.data.asset.AssetState;
import com.pixelmongenerations.core.data.asset.DynamicAsset;
import com.pixelmongenerations.core.data.asset.entry.ValveModelAsset;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModel;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

public class ValveModelResource
extends DynamicAsset<ValveModelAsset> {
    private EnumSpecies species;
    private URL pqcURL;
    private ArrayList<URL> modelURLs;
    private IModel model;

    public ValveModelResource(ValveModelAsset entry) {
        super(entry);
        this.setState(AssetState.Start);
        this.formURLs();
        this.species = entry.species;
    }

    @Override
    public void formURLs() {
        try {
            if (((ValveModelAsset)this.entry).pqcURL != null) {
                this.pqcURL = new URL(((ValveModelAsset)this.entry).pqcURL);
            }
            this.modelURLs = new ArrayList();
            for (String url : ((ValveModelAsset)this.entry).smdURLs) {
                this.modelURLs.add(new URL(url));
            }
        }
        catch (MalformedURLException e) {
            Pixelmon.LOGGER.error("ModelEntry with id '%s' has a Invalid URL and was prevented from loading.", (Object)((ValveModelAsset)this.entry).modelId);
            this.setState(AssetState.Failed);
        }
    }

    @Override
    public void download() {
        if (this.getState() == AssetState.Failed) {
            return;
        }
        this.setState(AssetState.Downloading);
        NetHandlerPlayClient netHandler = Minecraft.getMinecraft().player.connection;
        String remoteAdd = netHandler.getNetworkManager().getRemoteAddress().toString();
        if (remoteAdd.contains("/")) {
            remoteAdd = remoteAdd.split("/")[1];
        }
        remoteAdd = remoteAdd.replaceAll(":", "-");
        File dir = new File("./resource-cache/" + (remoteAdd.contains("local") ? "singleplayer" : remoteAdd) + "/");
        dir.mkdirs();
        try {
            File pqcFile = new File(dir, ((ValveModelAsset)this.entry).modelId + ".pqc");
            if (!pqcFile.exists()) {
                if (FilenameUtils.getExtension((String)this.pqcURL.getPath()).equalsIgnoreCase("pqc")) {
                    FileUtils.copyURLToFile((URL)this.pqcURL, (File)pqcFile, (int)5000, (int)5000);
                } else {
                    Pixelmon.LOGGER.error(String.format("Skipped download of non PQC file: %s", this.pqcURL.getPath()));
                    this.setState(AssetState.Failed);
                    return;
                }
            }
            for (URL url : this.modelURLs) {
                File modelFile = new File(dir, FilenameUtils.getName((String)url.getPath()));
                if (modelFile.exists()) continue;
                if (FilenameUtils.getExtension((String)url.getPath()).equalsIgnoreCase("smdx")) {
                    FileUtils.copyURLToFile((URL)url, (File)modelFile, (int)5000, (int)5000);
                    continue;
                }
                Pixelmon.LOGGER.error(String.format("Skipped download of non SMDX file: %s", FilenameUtils.getName((String)url.getPath())));
                this.setState(AssetState.Failed);
                return;
            }
            this.setState(AssetState.Downloaded);
            if (((ValveModelAsset)this.entry).preLoad) {
                this.load();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            this.setState(AssetState.Failed);
        }
    }

    @Override
    public void load() {
        this.setState(AssetState.Loading);
        IModel downloadedModel = null;
        ResourceLocation rl = new ResourceLocation("dynpixelmon:customassets/" + ((ValveModelAsset)this.entry).modelId + ".pqc");
        if (ValveStudioModelLoader.instance.accepts(rl)) {
            downloadedModel = ValveStudioModelLoader.instance.loadModel(rl);
        } else if (ObjLoader.accepts(rl)) {
            downloadedModel = ObjLoader.loadModel(rl);
        }
        if (downloadedModel != null) {
            this.setState(AssetState.Loaded);
            this.model = downloadedModel;
            return;
        }
        this.setState(AssetState.Failed);
    }

    @Override
    public void unload() {
        this.model = null;
        this.setState(AssetState.Downloaded);
    }

    public IModel getModel() {
        try {
            switch (this.getState()) {
                case Loaded: {
                    return this.model;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public EnumSpecies getPokemon() {
        return this.species;
    }

    @Override
    public String getKey() {
        return ((ValveModelAsset)this.entry).modelId;
    }

    @Override
    public String getName() {
        return ((ValveModelAsset)this.entry).modelName;
    }
}

