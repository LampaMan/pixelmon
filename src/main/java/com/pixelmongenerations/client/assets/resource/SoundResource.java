/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.apache.commons.io.FileUtils
 *  org.apache.commons.io.FilenameUtils
 */
package com.pixelmongenerations.client.assets.resource;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.data.asset.AssetState;
import com.pixelmongenerations.core.data.asset.DynamicAsset;
import com.pixelmongenerations.core.data.asset.entry.SoundAsset;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.CompletableFuture;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.util.ResourceLocation;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

public class SoundResource
extends DynamicAsset<SoundAsset> {
    private URL soundURL;
    private CompletableFuture<Void> future;

    public SoundResource(SoundAsset entry) {
        super(entry);
        this.setState(AssetState.Start);
        this.formURLs();
    }

    @Override
    public void formURLs() {
        try {
            if (((SoundAsset)this.entry).soundURL != null) {
                this.soundURL = new URL(((SoundAsset)this.entry).soundURL);
            }
        }
        catch (MalformedURLException e) {
            Pixelmon.LOGGER.error("SoundEntry with id '%s' has a Invalid URL and was prevented from loading.", (Object)((SoundAsset)this.entry).soundId);
            this.setState(AssetState.Failed);
        }
    }

    @Override
    public void download() {
        if (this.getState() == AssetState.Failed) {
            return;
        }
        this.setState(AssetState.Downloading);
        NetHandlerPlayClient netHandler = Minecraft.getMinecraft().player.connection;
        String remoteAdd = netHandler.getNetworkManager().getRemoteAddress().toString();
        if (remoteAdd.contains("/")) {
            remoteAdd = remoteAdd.split("/")[1];
        }
        remoteAdd = remoteAdd.replaceAll(":", "-");
        File dir = new File("./resource-cache/" + (remoteAdd.contains("local") ? "singleplayer" : remoteAdd) + "/");
        dir.mkdirs();
        this.future = CompletableFuture.supplyAsync(() -> {
            try {
                ResourceLocation rl;
                File soundFile = new File(dir, ((SoundAsset)this.entry).soundId + ".ogg");
                if (!soundFile.exists()) {
                    if (FilenameUtils.getExtension((String)this.soundURL.getPath()).equalsIgnoreCase("ogg")) {
                        FileUtils.copyURLToFile((URL)this.soundURL, (File)soundFile, (int)5000, (int)5000);
                    } else {
                        Pixelmon.LOGGER.error(String.format("Skipped download of non OGG file: %s", this.soundURL.getPath()));
                        this.setState(AssetState.Failed);
                        return null;
                    }
                }
                if (Pixelmon.PROXY.resourceLocationExists(rl = new ResourceLocation("dynpixelmon:customsound/" + ((SoundAsset)this.entry).soundId + ".ogg"))) {
                    this.setState(AssetState.Downloaded);
                    return null;
                }
                this.setState(AssetState.Failed);
                return null;
            }
            catch (Exception e) {
                e.printStackTrace();
                this.setState(AssetState.Failed);
                return null;
            }
        });
        try {
            this.future.get();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getKey() {
        return ((SoundAsset)this.entry).soundId;
    }

    @Override
    public String getName() {
        return ((SoundAsset)this.entry).soundName;
    }
}

