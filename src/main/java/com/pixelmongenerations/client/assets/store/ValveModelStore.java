/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.assets.store;

import com.pixelmongenerations.client.assets.resource.ValveModelResource;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.data.asset.AssetState;
import com.pixelmongenerations.core.data.asset.ObjectStore;
import com.pixelmongenerations.core.data.asset.entry.ValveModelAsset;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class ValveModelStore
implements ObjectStore<ValveModelResource> {
    private static Map<String, ValveModelResource> modelStore = new HashMap<String, ValveModelResource>();

    @Override
    public void addObject(String key, ValveModelResource texture) {
        modelStore.put(key, texture);
    }

    @Override
    public ValveModelResource getObject(String key) {
        ValveModelResource resource = modelStore.get(key);
        if (resource != null) {
            if (resource.getState() == AssetState.Downloaded) {
                resource.load();
            }
            resource.lastAccessed = Instant.now();
        }
        return resource;
    }

    @Override
    public void checkForExpiredObjects() {
        Instant cleanupPoint = Instant.now().minusSeconds(900L);
        int count = 0;
        for (ValveModelResource resource : modelStore.values()) {
            if (((ValveModelAsset)resource.entry).preLoad || resource.getState() != AssetState.Loaded || !resource.lastAccessed.isBefore(cleanupPoint)) continue;
            resource.unload();
            ++count;
        }
        if (count > 0) {
            Pixelmon.LOGGER.info("[ValveModelStore] Unloaded " + count + " recently unused resources.");
        }
    }
}

