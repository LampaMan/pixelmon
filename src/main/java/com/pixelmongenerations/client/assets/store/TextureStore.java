/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.assets.store;

import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.data.asset.AssetState;
import com.pixelmongenerations.core.data.asset.ObjectStore;
import com.pixelmongenerations.core.data.asset.entry.TextureAsset;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class TextureStore
implements ObjectStore<TextureResource> {
    private static Map<String, TextureResource> textureStore = new HashMap<String, TextureResource>();

    @Override
    public void addObject(String key, TextureResource object) {
        textureStore.put(key, object);
    }

    @Override
    public TextureResource getObject(String key) {
        TextureResource resource = textureStore.get(key);
        if (resource != null) {
            if (resource.getState() == AssetState.Downloaded) {
                ClientProxy.RESOURCE_MANAGEMENT_TASK.queueLoad(resource);
            }
            resource.lastAccessed = Instant.now();
        }
        return resource;
    }

    @Override
    public void checkForExpiredObjects() {
        Instant cleanupPoint = Instant.now().minusSeconds(900L);
        int count = 0;
        for (TextureResource resource : textureStore.values()) {
            if (((TextureAsset)resource.entry).preLoad || resource.getState() != AssetState.Loaded || !resource.lastAccessed.isBefore(cleanupPoint)) continue;
            resource.unload();
            ++count;
        }
        if (count > 0) {
            Pixelmon.LOGGER.info("[TextureStore] Unloaded " + count + " recently unused resources.");
        }
    }
}

