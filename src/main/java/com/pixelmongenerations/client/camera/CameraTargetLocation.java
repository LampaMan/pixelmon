/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.camera;

import com.pixelmongenerations.client.camera.ICameraTarget;

public class CameraTargetLocation
implements ICameraTarget {
    int x;
    int y;
    int z;

    public CameraTargetLocation(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public double getX() {
        return this.x;
    }

    @Override
    public double getXSeeOffset() {
        return 0.0;
    }

    @Override
    public double getY() {
        return this.y;
    }

    @Override
    public double getYSeeOffset() {
        return 0.0;
    }

    @Override
    public double getZ() {
        return this.z;
    }

    @Override
    public double getZSeeOffset() {
        return 0.0;
    }

    @Override
    public double minimumCameraDistance() {
        return 0.0;
    }

    @Override
    public boolean isValidTarget() {
        return true;
    }

    @Override
    public boolean hasChanged() {
        return false;
    }

    @Override
    public boolean setTargetData(Object o) {
        if (o instanceof int[] && ((int[])o).length == 3) {
            this.x = ((int[])o)[0];
            this.y = ((int[])o)[1];
            this.z = ((int[])o)[2];
            return true;
        }
        return false;
    }

    @Override
    public Object getTargetData() {
        return new int[]{this.x, this.y, this.z};
    }
}

