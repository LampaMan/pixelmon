/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.camera;

public interface ICameraTarget {
    public double getX();

    public double getXSeeOffset();

    public double getY();

    public double getYSeeOffset();

    public double getZ();

    public double getZSeeOffset();

    public double minimumCameraDistance();

    public boolean isValidTarget();

    public boolean hasChanged();

    public boolean setTargetData(Object var1);

    public Object getTargetData();
}

