/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.camera;

import com.pixelmongenerations.client.camera.EntityCamera;
import com.pixelmongenerations.client.camera.GuiCamera;
import com.pixelmongenerations.client.gui.elements.GuiChatExtension;
import com.pixelmongenerations.core.config.PixelmonConfig;
import java.io.IOException;
import net.minecraft.client.Minecraft;

public class GuiChattableCamera
extends GuiCamera {
    private GuiChatExtension chat = new GuiChatExtension(this, 74);

    public GuiChattableCamera() {
        this(new EntityCamera(Minecraft.getMinecraft().world), -102, -102, -102);
    }

    public GuiChattableCamera(int posX) {
        this(new EntityCamera(Minecraft.getMinecraft().world), posX, -102, -102);
    }

    public GuiChattableCamera(int posX, int posY) {
        this(new EntityCamera(Minecraft.getMinecraft().world), posX, posY, -102);
    }

    public GuiChattableCamera(int posX, int posY, int width) {
        this(new EntityCamera(Minecraft.getMinecraft().world), posX, posY, width);
    }

    public GuiChattableCamera(EntityCamera cam, int posX, int posY, int width) {
        super(cam);
    }

    @Override
    public void handleKeyboardInput() {
        this.chat.handleKeyboardInput();
        super.handleKeyboardInput();
    }

    @Override
    public void initGui() {
        super.initGui();
        this.chat.initGui();
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        this.chat.onGuiClosed();
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        this.chat.updateScreen();
    }

    @Override
    public void drawScreen(int par1, int par2, float par3) {
        this.chat.drawScreen(par1, par2, par3);
    }

    @Override
    protected void keyTyped(char par1, int par2) {
    }

    @Override
    public void handleMouseInput() throws IOException {
        try {
            super.handleMouseInput();
        }
        catch (NullPointerException e) {
            if (PixelmonConfig.printErrors) {
                e.printStackTrace();
            }
            return;
        }
        this.chat.handleMouseInput();
    }

    @Override
    protected void mouseClicked(int par1, int par2, int par3) throws IOException {
        this.chat.mouseClicked(par1, par2, par3);
        super.mouseClicked(par1, par2, par3);
    }
}

