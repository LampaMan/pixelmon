/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.util.vector.Matrix4f
 *  org.lwjgl.util.vector.ReadableVector3f
 *  org.lwjgl.util.vector.Vector3f
 */
package com.pixelmongenerations.client.camera.movement;

import com.pixelmongenerations.client.camera.EntityCamera;
import com.pixelmongenerations.client.camera.ICameraTarget;
import com.pixelmongenerations.client.camera.movement.CameraMovement;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.util.ArrayList;
import java.util.Random;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.Matrix4f;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.lwjgl.util.vector.ReadableVector3f;
import org.lwjgl.util.vector.Vector3f;

public class PositionedMovement
extends CameraMovement {
    int ticksToChange = 0;
    int currentPos = 0;
    ArrayList<BlockPos> cameraPositionList = new ArrayList();
    boolean initPokePos = false;
    boolean initDiagonals = false;

    public PositionedMovement(World worldObj, EntityCamera camera) {
        super(worldObj, camera);
    }

    @Override
    public void onLivingUpdate() {
        if ((this.initDiagonals || this.initPokePos) && !this.camera.isDead && ClientProxy.battleManager.getViewEntity() != this.camera) {
            ClientProxy.battleManager.setViewEntity(this.camera);
            ClientProxy.camera = this.camera;
        }
        if (!PixelmonConfig.playerControlCamera && this.cameraPositionList.size() > this.currentPos) {
            BlockPos cameraPosition = this.cameraPositionList.get(this.currentPos);
            if (!this.camera.getPosition().equals(cameraPosition)) {
                this.camera.setPosition(cameraPosition.getX(), cameraPosition.getY(), cameraPosition.getZ());
            }
        }
        if (!this.initPokePos && ClientProxy.battleManager.getUserPokemon(this.camera.mode) != null) {
            this.generateCameraPositionsForPokemon();
        }
        if (!this.initDiagonals && ClientProxy.battleManager.getUserPokemon(this.camera.mode) != null) {
            this.generateCameraDiagonalsForPlayer();
        }
        --this.ticksToChange;
        int positionListSize = this.cameraPositionList.size();
        if (this.ticksToChange <= 0 && positionListSize > 0) {
            Random random = this.world.rand;
            this.currentPos = random.nextInt(positionListSize);
            this.ticksToChange = random.nextInt(40) + 70;
        }
    }

    @Override
    public void generatePositions() {
        this.cameraPositionList.clear();
        this.initPokePos = false;
        this.initDiagonals = false;
        this.currentPos = 0;
        this.ticksToChange = this.world.rand.nextInt(100) + 80;
        if (ClientProxy.battleManager.getUserPokemon(this.camera.mode) != null) {
            if (!this.initPokePos) {
                this.generateCameraPositionsForPokemon();
            }
            if (!this.initDiagonals) {
                this.generateCameraDiagonalsForPlayer();
            }
        }
    }

    private void generateCameraPositionsForPokemon() {
        this.initPokePos = true;
        EntityPixelmon pix = ClientProxy.battleManager.getUserPokemon(this.camera.mode);
        if (pix != null) {
            BlockPos pokemonPos = pix.getPosition();
            EntityPlayer player = ClientProxy.battleManager.getViewPlayer();
            BlockPos playerPos = player.getPosition();
            for (int i = 0; i < 8; ++i) {
                BlockPos basePos = this.getRandomCameraPositionForPokemon(pokemonPos);
                Material mat = this.world.getBlockState(basePos).getMaterial();
                if (mat != Material.AIR && mat != Material.WATER || !this.canPosSee(basePos, playerPos)) continue;
                this.cameraPositionList.add(basePos);
            }
        }
        ClientProxy.battleManager.setViewEntity(this.camera);
    }

    private BlockPos getRandomCameraPositionForPokemon(BlockPos pokemonPos) {
        Random random = this.world.rand;
        int x = pokemonPos.getX() + random.nextInt(10) - 5;
        int y = pokemonPos.getY() + random.nextInt(7) + 1;
        int z = pokemonPos.getZ() + random.nextInt(10) - 5;
        return new BlockPos(x, y, z);
    }

    private void generateCameraDiagonalsForPlayer() {
        EntityPixelmon pix = ClientProxy.battleManager.getUserPokemon(this.camera.mode);
        if (pix != null) {
            BlockPos pokemonPos = pix.getPosition();
            Vector3f pokemonVec = new Vector3f((float)pokemonPos.getX(), (float)pokemonPos.getY(), (float)pokemonPos.getZ());
            pokemonVec.y += pix.getEyeHeight();
            if (!this.checkDiagonals(pokemonVec)) {
                return;
            }
        }
        this.initDiagonals = true;
    }

    private boolean checkDiagonals(Vector3f pokemonVec) {
        EntityPlayer player = ClientProxy.battleManager.getViewPlayer();
        BlockPos playerPos = player.getPosition();
        Vector3f playerVec = new Vector3f((float)playerPos.getX(), (float)playerPos.getY(), (float)playerPos.getZ());
        playerVec.y += player.getEyeHeight();
        Vector3f distanceVec = new Vector3f();
        Vector3f.sub((Vector3f)playerVec, (Vector3f)pokemonVec, (Vector3f)distanceVec);
        distanceVec.y = 0.0f;
        if (distanceVec.length() == 0.0f) {
            return false;
        }
        Vector3f firstDiag = new Vector3f((ReadableVector3f)distanceVec);
        firstDiag.normalise();
        float angle = 0.3926991f;
        Vector3f perp = new Vector3f();
        Vector3f.cross((Vector3f)firstDiag, (Vector3f)new Vector3f(0.0f, 1.0f, 0.0f), (Vector3f)perp);
        Vector3f rotated = this.rotateRoundGround(firstDiag, perp, angle);
        perp.normalise();
        for (int i = 0; i < 6; ++i) {
            this.generateDiagonalPos(playerPos, playerVec, rotated, perp);
        }
        return true;
    }

    private void generateDiagonalPos(BlockPos playerPos, Vector3f playerVec, Vector3f rotated, Vector3f perp) {
        Vector3f newPos = new Vector3f();
        Random random = this.world.rand;
        float scale = random.nextInt(2) + 3;
        Vector3f rotatedScaled = new Vector3f((ReadableVector3f)rotated);
        rotatedScaled.x *= scale;
        rotatedScaled.y *= scale;
        rotatedScaled.z *= scale;
        scale = random.nextInt(9) - 4;
        Vector3f perpScaled = new Vector3f((ReadableVector3f)perp);
        perpScaled.x *= scale;
        perpScaled.y *= scale;
        perpScaled.z *= scale;
        Vector3f.add((Vector3f)playerVec, (Vector3f)rotatedScaled, (Vector3f)newPos);
        Vector3f.add((Vector3f)newPos, (Vector3f)perpScaled, (Vector3f)newPos);
        BlockPos basePos = new BlockPos(newPos.x, newPos.y, newPos.z);
        Material mat = this.world.getBlockState(basePos).getMaterial();
        if ((mat == Material.AIR || mat == Material.WATER) && this.canPosSee(basePos, playerPos)) {
            this.cameraPositionList.add(basePos);
        }
    }

    private Vector3f rotateRoundGround(Vector3f vector, Vector3f rotateVector, float angle) {
        Matrix4f matrix = new Matrix4f();
        matrix.m03 = vector.x;
        matrix.m13 = vector.y;
        matrix.m23 = vector.z;
        org.lwjgl.util.vector.Matrix4f.rotate((float)angle, (Vector3f)rotateVector, (org.lwjgl.util.vector.Matrix4f)matrix, (org.lwjgl.util.vector.Matrix4f)matrix);
        if (matrix.m13 < 0.0f) {
            return this.rotateRoundGround(vector, rotateVector, -1.0f * angle);
        }
        return new Vector3f(matrix.m03, matrix.m13, matrix.m23);
    }

    @Override
    public void setRandomPosition(ICameraTarget t) {
        BlockPos randomPos = this.getRandomCameraPositionForPokemon(new BlockPos(t.getX(), t.getY(), t.getZ()));
        this.camera.setPosition(randomPos.getX(), randomPos.getY(), randomPos.getZ());
    }

    @Override
    public void updatePositionAndRotation() {
        this.camera.lastTickPosX = this.camera.prevPosX = this.camera.posX;
        this.camera.lastTickPosY = this.camera.prevPosY = this.camera.posY + this.camera.getYOffset();
        this.camera.lastTickPosZ = this.camera.prevPosZ = this.camera.posZ;
    }
}

