/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.camera.movement;

import com.pixelmongenerations.client.camera.EntityCamera;
import com.pixelmongenerations.client.camera.ICameraTarget;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public abstract class CameraMovement {
    protected World world;
    protected EntityCamera camera;

    public CameraMovement(World worldObj, EntityCamera camera) {
        this.world = worldObj;
        this.camera = camera;
    }

    protected boolean canPosSee(BlockPos pos1, BlockPos pos2) {
        return this.world.rayTraceBlocks(new Vec3d(pos1.getX(), pos1.getY(), pos1.getZ()), new Vec3d(pos2.getX(), pos2.getY(), pos2.getZ())) == null;
    }

    public abstract void setRandomPosition(ICameraTarget var1);

    public abstract void onLivingUpdate();

    public abstract void generatePositions();

    public void handleKeyboardInput() {
    }

    public void updatePositionAndRotation() {
        this.camera.prevPosX = this.camera.posX;
        this.camera.prevPosY = this.camera.posY + this.camera.getYOffset();
        this.camera.prevPosZ = this.camera.posZ;
    }

    public void handleMouseMovement(int dx, int dy, int dwheel) {
    }
}

