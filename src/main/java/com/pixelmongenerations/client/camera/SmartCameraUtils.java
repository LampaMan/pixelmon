/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.camera;

import com.pixelmongenerations.client.camera.EntityCamera;
import com.pixelmongenerations.client.camera.ICameraTarget;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class SmartCameraUtils {
    public static boolean canCameraSeeTargetFrom(EntityCamera camera, ICameraTarget t, double x, double y, double z) {
        return camera.world.rayTraceBlocks(new Vec3d(x, y, z), new Vec3d(t.getX() + t.getXSeeOffset(), t.getY() + t.getYSeeOffset(), t.getZ() + t.getZSeeOffset())) == null;
    }

    public static boolean canCameraSeeTargetFrom(EntityCamera camera, double x, double y, double z) {
        return SmartCameraUtils.canCameraSeeTargetFrom(camera, camera.getTarget(), x, y, z);
    }

    public static boolean canCameraSeeTargetFrom(EntityCamera camera, BlockPos pos) {
        return SmartCameraUtils.canCameraSeeTargetFrom(camera, camera.getTarget(), pos.getX(), pos.getY(), pos.getZ());
    }

    public static boolean canCameraSeeTarget(EntityCamera camera, ICameraTarget t) {
        return camera.world.rayTraceBlocks(new Vec3d(camera.posX, camera.posY, camera.posZ), new Vec3d(t.getX() + t.getXSeeOffset(), t.getY() + t.getYSeeOffset(), t.getZ() + t.getZSeeOffset())) == null;
    }

    public static boolean canCameraSeeTarget(EntityCamera camera) {
        return SmartCameraUtils.canCameraSeeTarget(camera, camera.getTarget());
    }
}

