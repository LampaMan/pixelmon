/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Mouse
 */
package com.pixelmongenerations.client.camera;

import com.pixelmongenerations.client.camera.CameraMode;
import com.pixelmongenerations.client.camera.EntityCamera;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.io.IOException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.Entity;
import org.lwjgl.input.Mouse;

public class GuiCamera
extends GuiContainer {
    boolean mouseDown = false;

    public GuiCamera() {
        this(CameraMode.Battle);
    }

    public GuiCamera(CameraMode mode) {
        this(new EntityCamera(Minecraft.getMinecraft().world, mode));
    }

    public GuiCamera(EntityCamera cam) {
        super(new ContainerEmpty());
        if (PixelmonConfig.useBattleCamera) {
            Minecraft minecraft = Minecraft.getMinecraft();
            minecraft.addScheduledTask(() -> {
                ClientProxy.camera = cam;
                Entity renderEntity = minecraft.getRenderViewEntity();
                this.getCamera().setPosition(renderEntity.posX, renderEntity.posY, renderEntity.posZ);
                minecraft.world.addEntityToWorld(ClientProxy.camera.getEntityId(), ClientProxy.camera);
                cam.getMovement().generatePositions();
            });
        }
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }

    @Override
    public void handleKeyboardInput() {
        if (ClientProxy.camera != null) {
            ClientProxy.camera.getMovement().handleKeyboardInput();
        }
    }

    @Override
    public void handleMouseInput() throws IOException {
        int dx = 0;
        int dy = 0;
        if (Mouse.isButtonDown((int)1)) {
            if (!this.mouseDown) {
                Mouse.getDX();
                Mouse.getDY();
            }
            if (this.mouseDown) {
                dx = Mouse.getDX();
                dy = Mouse.getDY();
            }
            this.mouseDown = true;
        } else if (this.mouseDown) {
            this.mouseDown = false;
        }
        if (ClientProxy.camera != null) {
            ClientProxy.camera.getMovement().handleMouseMovement(dx, dy, Mouse.getDWheel());
        }
        super.handleMouseInput();
    }

    @Override
    public void drawBackground(int par1) {
    }

    @Override
    public void drawDefaultBackground() {
    }

    public EntityCamera getCamera() {
        return ClientProxy.camera;
    }

    @Override
    public void initGui() {
        super.initGui();
        this.allowUserInput = true;
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        this.mc.setIngameFocus();
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3) {
    }
}

