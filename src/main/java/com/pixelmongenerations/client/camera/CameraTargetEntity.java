/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.camera;

import com.pixelmongenerations.client.camera.ICameraTarget;
import net.minecraft.entity.Entity;

public class CameraTargetEntity
implements ICameraTarget {
    Entity entity;
    private double lastX = 0.0;
    private double lastY = 0.0;
    private double lastZ = 0.0;

    public CameraTargetEntity(Entity e) {
        this.entity = e;
    }

    @Override
    public double getX() {
        return this.entity.posX;
    }

    @Override
    public double getY() {
        return this.entity.posY;
    }

    @Override
    public double getZ() {
        return this.entity.posZ;
    }

    @Override
    public boolean isValidTarget() {
        return this.entity != null && !this.entity.isDead;
    }

    @Override
    public double getXSeeOffset() {
        return 0.0;
    }

    @Override
    public double getYSeeOffset() {
        return this.entity.getEyeHeight();
    }

    @Override
    public double getZSeeOffset() {
        return 0.0;
    }

    @Override
    public double minimumCameraDistance() {
        return 0.0;
    }

    @Override
    public boolean hasChanged() {
        boolean r = false;
        if (this.entity.posX != this.lastX || this.entity.posY != this.lastY || this.entity.posZ != this.lastZ) {
            r = true;
        }
        this.lastX = this.entity.posX;
        this.lastY = this.entity.posY;
        this.lastZ = this.entity.posZ;
        return r;
    }

    @Override
    public boolean setTargetData(Object o) {
        if (o instanceof Entity) {
            this.entity = (Entity)o;
            return true;
        }
        return false;
    }

    @Override
    public Object getTargetData() {
        return this.entity;
    }
}

