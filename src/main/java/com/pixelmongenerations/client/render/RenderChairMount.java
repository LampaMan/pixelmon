/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render;

import com.pixelmongenerations.common.entity.EntityChairMount;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;

public class RenderChairMount
extends Render<EntityChairMount> {
    public RenderChairMount(RenderManager renderManager) {
        super(renderManager);
    }

    @Override
    public void doRender(EntityChairMount entity, double x, double y, double z, float entityYaw, float partialTicks) {
    }

    @Override
    protected ResourceLocation getEntityTexture(EntityChairMount entity) {
        return null;
    }
}

