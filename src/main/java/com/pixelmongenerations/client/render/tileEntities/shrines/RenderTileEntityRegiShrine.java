/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities.shrines;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.shrines.RenderTileEntityShrine;
import com.pixelmongenerations.common.block.spawnmethod.BlockRegiShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityRegiShrine;
import com.pixelmongenerations.core.enums.EnumSpecies;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.ResourceLocation;

public class RenderTileEntityRegiShrine
extends RenderTileEntityShrine<TileEntityRegiShrine> {
    private static final BlockModelHolder<GenericSmdModel> regice_model = new BlockModelHolder("blocks/shrines/regis/regice_shrine.pqc");
    private static final ResourceLocation regice_texture = new ResourceLocation("pixelmon", "textures/blocks/shrines/regis/regice_shrine.png");
    private static final BlockModelHolder<GenericSmdModel> regirock_model = new BlockModelHolder("blocks/shrines/regis/regirock_shrine.pqc");
    private static final ResourceLocation regirock_texture = new ResourceLocation("pixelmon", "textures/blocks/shrines/regis/regirock_shrine.png");
    private static final BlockModelHolder<GenericSmdModel> registeel_model = new BlockModelHolder("blocks/shrines/regis/registeel_shrine.pqc");
    private static final ResourceLocation registeel_texture = new ResourceLocation("pixelmon", "textures/blocks/shrines/regis/registeel_shrine.png");
    private static final BlockModelHolder<GenericSmdModel> regidrago_model = new BlockModelHolder("blocks/shrines/regis/regidrago_shrine.pqc");
    private static final ResourceLocation regidrago_texture = new ResourceLocation("pixelmon", "textures/blocks/shrines/regis/regidrago_shrine.png");
    private static final BlockModelHolder<GenericSmdModel> regieleki_model = new BlockModelHolder("blocks/shrines/regis/regieleki_shrine.pqc");
    private static final ResourceLocation regieleki_texture = new ResourceLocation("pixelmon", "textures/blocks/shrines/regis/regieleki_shrine.png");

    public RenderTileEntityRegiShrine() {
        super("blocks/shrines/lugia_shrine/lugia_shrine.pqc", "lugia_shrine.png");
    }

    @Override
    public void renderTileEntity(TileEntityRegiShrine shrineBlock, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        if (state.getBlock() instanceof BlockRegiShrine) {
            switch (((BlockRegiShrine)state.getBlock()).getSpecies()) {
                case Regice: {
                    this.render(regice_model, regice_texture);
                    if (shrineBlock.getTick() > 0) {
                        this.processAnimation(shrineBlock, EnumSpecies.Regice);
                    }
                    return;
                }
                case Regirock: {
                    this.render(regirock_model, regirock_texture);
                    if (shrineBlock.getTick() > 0) {
                        this.processAnimation(shrineBlock, EnumSpecies.Regirock);
                    }
                    return;
                }
                case Registeel: {
                    this.render(registeel_model, registeel_texture);
                    if (shrineBlock.getTick() > 0) {
                        this.processAnimation(shrineBlock, EnumSpecies.Registeel);
                    }
                    return;
                }
                case Regidrago: {
                    this.render(regidrago_model, regidrago_texture);
                    if (shrineBlock.getTick() > 0) {
                        this.processAnimation(shrineBlock, EnumSpecies.Regidrago);
                    }
                    return;
                }
                case Regieleki: {
                    this.render(regieleki_model, regieleki_texture);
                    if (shrineBlock.getTick() > 0) {
                        this.processAnimation(shrineBlock, EnumSpecies.Regieleki);
                    }
                    return;
                }
            }
        }
    }

    private void render(BlockModelHolder<GenericSmdModel> holder, ResourceLocation texture) {
        this.bindTexture(texture);
        holder.render();
    }
}

