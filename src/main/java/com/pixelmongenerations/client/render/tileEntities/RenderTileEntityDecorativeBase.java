/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.ModelEntityBlock;
import com.pixelmongenerations.common.block.decorative.BlockContainerPlus;
import com.pixelmongenerations.common.block.enums.EnumAxis;
import com.pixelmongenerations.common.block.tileEntities.TileEntityDecorativeBase;
import java.util.HashMap;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;

public class RenderTileEntityDecorativeBase
extends TileEntitySpecialRenderer<TileEntityDecorativeBase> {
    static HashMap<BlockContainerPlus, ModelEntityBlock> modelMap = new HashMap();

    @Override
    public void render(TileEntityDecorativeBase tileentity, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        GlStateManager.pushMatrix();
        try {
            GlStateManager.translate((float)x + 0.5f, (float)y + 0.5f, (float)z + 0.5f);
            GlStateManager.scale(-1.0f, -1.0f, 1.0f);
            IBlockState state = tileentity.getWorld().getBlockState(tileentity.getPos());
            if (state.getBlock() instanceof BlockContainerPlus) {
                this.doRotation(state);
                ModelEntityBlock theModel = RenderTileEntityDecorativeBase.getModelFor((BlockContainerPlus)state.getBlock());
                theModel.renderTileEntity(tileentity, 0.0625f);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        GlStateManager.popMatrix();
    }

    public void doRotation(IBlockState state) {
        EnumAxis axis;
        try {
            axis = state.getValue(BlockContainerPlus.AXIS);
        }
        catch (IllegalArgumentException e) {
            return;
        }
        switch (axis) {
            case X: {
                GlStateManager.rotate(-90.0f, 0.0f, 0.0f, 1.0f);
                break;
            }
            case Z: {
                GlStateManager.rotate(-90.0f, 1.0f, 0.0f, 0.0f);
                break;
            }
        }
    }

    public static ModelEntityBlock addModelFor(BlockContainerPlus block) {
        ModelEntityBlock model = null;
        try {
            model = (ModelEntityBlock)block.modelClass.newInstance();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        modelMap.put(block, model);
        return model;
    }

    public static ModelEntityBlock getModelFor(BlockContainerPlus block) {
        return modelMap.containsKey(block) ? modelMap.get(block) : RenderTileEntityDecorativeBase.addModelFor(block);
    }
}

