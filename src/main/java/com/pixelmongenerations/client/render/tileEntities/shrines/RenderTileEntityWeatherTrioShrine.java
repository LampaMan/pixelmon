/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities.shrines;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.shrines.RenderTileEntityShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityWeatherTrioShrine;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.enums.EnumSpecies;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.ResourceLocation;

public class RenderTileEntityWeatherTrioShrine
extends RenderTileEntityShrine<TileEntityWeatherTrioShrine> {
    private static final BlockModelHolder<GenericSmdModel> groudon_shrine = new BlockModelHolder("blocks/shrines/weather_trio/groudon_shrine.pqc");
    private static final ResourceLocation groudon_activated = new ResourceLocation("pixelmon", "textures/blocks/shrines/weather_trio/groudon_activated.png");
    private static final ResourceLocation groudon_deactivated = new ResourceLocation("pixelmon", "textures/blocks/shrines/weather_trio/groudon_deactivated.png");
    private static final BlockModelHolder<GenericSmdModel> red_orb = new BlockModelHolder("blocks/shrines/weather_trio/red_orb.pqc");
    private static final ResourceLocation red_orb_texture = new ResourceLocation("pixelmon", "textures/blocks/shrines/weather_trio/red_orb.png");
    private static final BlockModelHolder<GenericSmdModel> kyogre_shrine = new BlockModelHolder("blocks/shrines/weather_trio/kyogre_shrine.pqc");
    private static final ResourceLocation kyogre_activated = new ResourceLocation("pixelmon", "textures/blocks/shrines/weather_trio/kyogre_activated.png");
    private static final ResourceLocation kyogre_deactivated = new ResourceLocation("pixelmon", "textures/blocks/shrines/weather_trio/kyogre_deactivated.png");
    private static final BlockModelHolder<GenericSmdModel> blue_orb = new BlockModelHolder("blocks/shrines/weather_trio/blue_orb.pqc");
    private static final ResourceLocation blue_orb_texture = new ResourceLocation("pixelmon", "textures/blocks/shrines/weather_trio/blue_orb.png");

    public RenderTileEntityWeatherTrioShrine() {
        super("blocks/shrines/weather_trio/groudon_shrine.pqc", "groudon_deactivated.png");
    }

    @Override
    public void renderTileEntity(TileEntityWeatherTrioShrine shrineBlock, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        boolean active = shrineBlock.getActive();
        if (state.getBlock() == PixelmonBlocks.groudonShrine) {
            this.bindTexture(active ? groudon_activated : groudon_deactivated);
            groudon_shrine.render();
            if (active) {
                this.bindTexture(red_orb_texture);
                red_orb.render();
                this.processAnimation(shrineBlock, EnumSpecies.Groudon);
            }
        } else {
            this.bindTexture(active ? kyogre_activated : kyogre_deactivated);
            kyogre_shrine.render();
            if (active) {
                this.bindTexture(blue_orb_texture);
                blue_orb.render();
                this.processAnimation(shrineBlock, EnumSpecies.Kyogre);
            }
        }
    }
}

