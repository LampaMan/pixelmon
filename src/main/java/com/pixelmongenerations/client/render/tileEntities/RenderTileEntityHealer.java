/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.models.pokeballs.ModelPokeballs;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.SharedModels;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.machines.BlockHealer;
import com.pixelmongenerations.common.block.tileEntities.TileEntityHealer;
import com.pixelmongenerations.common.entity.pokeballs.EntityPokeBall;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

public class RenderTileEntityHealer
extends TileEntityRenderer<TileEntityHealer> {
    private static final BlockModelHolder<GenericSmdModel> modelHealer = new BlockModelHolder("blocks/healer/healer.pqc");
    private static final ResourceLocation texture = new ResourceLocation("pixelmon:textures/blocks/healer.png");

    public RenderTileEntityHealer() {
        this.yOffset = 1.5f;
        this.correctionAngles = 180;
    }

    @Override
    public void renderTileEntity(TileEntityHealer healer, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        this.bindTexture(texture);
        GlStateManager.translate(0.0, 1.5, 0.0);
        modelHealer.render(0.8f);
        if (healer.stayDark) {
            healer.rotation = 0.0f;
        }
        GlStateManager.translate(0.0, -0.701, 0.0);
        if (healer.beingUsed) {
            for (int slot = 0; slot < healer.pokeballType.length; ++slot) {
                if (healer.pokeballType[slot] == null) continue;
                this.renderPokeball(slot, healer);
            }
        }
    }

    @Override
    protected int getRotation(IBlockState state) {
        if (state.getBlock() instanceof BlockHealer) {
            EnumFacing facing = state.getValue(BlockHealer.FACING);
            if (facing == EnumFacing.EAST) {
                return 270;
            }
            if (facing == EnumFacing.NORTH) {
                return 0;
            }
            if (facing == EnumFacing.WEST) {
                return 90;
            }
            if (facing == EnumFacing.SOUTH) {
                return 180;
            }
        }
        return 0;
    }

    private void renderPokeball(int slot, TileEntityHealer tile) {
        GlStateManager.pushMatrix();
        float y = 0.0f;
        switch (slot) {
            case 0: {
                GlStateManager.translate(-0.114f, y, 0.172f);
                break;
            }
            case 1: {
                GlStateManager.translate(0.114f, y, 0.172f);
                break;
            }
            case 2: {
                GlStateManager.translate(-0.114f, y, 0.0f);
                break;
            }
            case 3: {
                GlStateManager.translate(0.114f, y, 0.0f);
                break;
            }
            case 4: {
                GlStateManager.translate(-0.114f, y, -0.172f);
                break;
            }
            case 5: {
                GlStateManager.translate(0.114f, y, -0.172f);
            }
        }
        GlStateManager.rotate(tile.rotation + 180.0f, 0.0f, 1.0f, 0.0f);
        if (tile.allPlaced) {
            if (tile.flashTimer >= 6 && !tile.stayDark) {
                GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            } else {
                GlStateManager.color(0.3f, 0.3f, 0.3f, 1.0f);
            }
            if (tile.flashTimer >= 10) {
                tile.flashTimer = 0;
            }
        }
        this.bindTexture(tile.pokeballType[slot].getTextureLocation());
        BlockModelHolder<? extends ModelPokeballs> pokeball = SharedModels.getPokeballModel(tile.pokeballType[slot]);
        GlStateManager.rotate(180.0f, 1.0f, 0.0f, 60.6f);
        ModelPokeballs model = (ModelPokeballs)pokeball.getModel();
        model.theModel.setAnimation("idle");
        model.render(null, EntityPokeBall.scale * 0.75f);
        GlStateManager.popMatrix();
    }
}

