/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.ModelVendingMachine;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.TileEntityVendingMachine;
import net.minecraft.block.state.IBlockState;

public class VendingMachineRenderer
extends TileEntityRenderer<TileEntityVendingMachine> {
    private static final BlockModelHolder<ModelVendingMachine> model = new BlockModelHolder<ModelVendingMachine>(ModelVendingMachine.class);

    public VendingMachineRenderer() {
        this.yOffset = 1.5f;
        this.scale = 0.0625f;
        this.correctionAngles = 180;
    }

    @Override
    public void renderTileEntity(TileEntityVendingMachine vendingMachine, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        this.bindTexture(vendingMachine.getTexture());
        model.render();
    }
}

