/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.tileEntities.GenericRenderer;
import com.pixelmongenerations.client.render.tileEntities.RenderTileEntityPicketFence;
import com.pixelmongenerations.common.block.BlockCustomPicketFence;
import com.pixelmongenerations.common.block.BlockPicketFence;
import com.pixelmongenerations.common.block.tileEntities.TileEntityPicketFenceNormal;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.tileentity.TileEntity;

public class RenderCustomTileEntityPicketFence
extends GenericRenderer<RenderTileEntityPicketFence, TileEntity> {
    public final GenericSmdModel connectorModel = new GenericSmdModel("models/blocks/picket_fence", "connector.pqc", true);

    public RenderCustomTileEntityPicketFence() {
        super("picketFenceTexture.png", new GenericSmdModel("models/blocks/picket_fence", "fence_post.pqc", true), 0);
        this.disableCulling = true;
    }

    @Override
    public void render(TileEntity te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        IBlockState state = this.getWorld().getBlockState(te.getPos());
        if (state.getBlock() instanceof BlockCustomPicketFence) {
            TileEntityPicketFenceNormal fence = (TileEntityPicketFenceNormal)te;
            BlockPicketFence block = (BlockPicketFence)state.getBlock();
            this.bindTexture(fence.getTexture());
            GlStateManager.enableRescaleNormal();
            GlStateManager.pushMatrix();
            if (this.blend) {
                GlStateManager.enableBlend();
                GlStateManager.blendFunc(770, 771);
                GlStateManager.depthMask(false);
                GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            }
            if (this.disableCulling) {
                GlStateManager.disableCull();
            }
            if (this.disableLighting) {
                GlStateManager.disableLighting();
            }
            GlStateManager.translate((float)x + 0.5f, (float)y + 0.0f, (float)z + 0.5f);
            GlStateManager.rotate(this.rotateDegrees + this.correctionAngles, 0.0f, 1.0f, 0.0f);
            GlStateManager.rotate(180.0f, 0.0f, 0.0f, 1.0f);
            this.model.render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, 1.0f);
            if (block.canConnectTo(this.getWorld(), te.getPos().north())) {
                GlStateManager.rotate(-90.0f, 0.0f, 1.0f, 0.0f);
                this.connectorModel.render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, 1.0f);
                GlStateManager.rotate(90.0f, 0.0f, 1.0f, 0.0f);
            }
            if (block.canConnectTo(this.getWorld(), te.getPos().south())) {
                GlStateManager.rotate(90.0f, 0.0f, 1.0f, 0.0f);
                this.connectorModel.render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, 1.0f);
                GlStateManager.rotate(-90.0f, 0.0f, 1.0f, 0.0f);
            }
            if (block.canConnectTo(this.getWorld(), te.getPos().east())) {
                this.connectorModel.render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, 1.0f);
            }
            if (block.canConnectTo(this.getWorld(), te.getPos().west())) {
                GlStateManager.rotate(-180.0f, 0.0f, 1.0f, 0.0f);
                this.connectorModel.render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, 1.0f);
                GlStateManager.rotate(180.0f, 0.0f, 1.0f, 0.0f);
            }
            if (this.blend) {
                GlStateManager.disableBlend();
                GlStateManager.depthMask(true);
            }
            if (this.disableCulling) {
                GlStateManager.enableCull();
            }
            if (this.disableLighting) {
                GlStateManager.enableLighting();
            }
            GlStateManager.disableRescaleNormal();
            GlStateManager.popMatrix();
        }
    }
}

