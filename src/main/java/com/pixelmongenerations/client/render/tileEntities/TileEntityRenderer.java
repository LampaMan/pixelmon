/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import net.minecraft.block.BlockHorizontal;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;

public abstract class TileEntityRenderer<T extends TileEntity>
extends TileEntitySpecialRenderer<T> {
    public static boolean profileTileEntityRender = false;
    protected boolean blend = false;
    protected boolean disableCulling = false;
    protected boolean disableLighting = false;
    protected boolean flip = true;
    protected float scale = 0.0f;
    protected float yOffset = 0.0f;
    protected int correctionAngles = 0;

    @Override
    public void render(T te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        if (profileTileEntityRender) {
            Minecraft.getMinecraft().profiler.startSection(te.getClass().getSimpleName());
        }
        IBlockState state = this.getWorld().getBlockState(((TileEntity)te).getPos());
        int rotateDegrees = this.getRotation(state);
        GlStateManager.enableRescaleNormal();
        GlStateManager.pushMatrix();
        if (this.blend) {
            GlStateManager.enableBlend();
            GlStateManager.blendFunc(770, 771);
            GlStateManager.depthMask(false);
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        }
        if (this.disableCulling) {
            GlStateManager.disableCull();
        }
        if (this.disableLighting) {
            GlStateManager.disableLighting();
        }
        GlStateManager.translate((float)x + 0.5f, (float)y + this.yOffset, (float)z + 0.5f);
        GlStateManager.rotate(rotateDegrees + this.correctionAngles, 0.0f, 1.0f, 0.0f);
        if (this.flip) {
            GlStateManager.rotate(180.0f, 0.0f, 0.0f, 1.0f);
        }
        if (this.scale != 0.0f) {
            GlStateManager.scale(this.scale, this.scale, this.scale);
        }
        this.renderTileEntity(te, state, x, y, z, partialTicks, destroyStage);
        if (this.blend) {
            GlStateManager.disableBlend();
            GlStateManager.depthMask(true);
        }
        if (this.disableCulling) {
            GlStateManager.enableCull();
        }
        if (this.disableLighting) {
            GlStateManager.enableLighting();
        }
        GlStateManager.disableRescaleNormal();
        GlStateManager.popMatrix();
        if (profileTileEntityRender) {
            Minecraft.getMinecraft().profiler.endSection();
        }
    }

    public abstract void renderTileEntity(T var1, IBlockState var2, double var3, double var5, double var7, float var9, int var10);

    protected int getRotation(IBlockState state) {
        if (state.getPropertyKeys().contains(BlockHorizontal.FACING)) {
            EnumFacing facing = state.getValue(BlockHorizontal.FACING);
            if (facing == EnumFacing.EAST) {
                return 270;
            }
            if (facing == EnumFacing.NORTH) {
                return 0;
            }
            if (facing == EnumFacing.WEST) {
                return 90;
            }
            if (facing == EnumFacing.SOUTH) {
                return 180;
            }
        }
        return 0;
    }
}

