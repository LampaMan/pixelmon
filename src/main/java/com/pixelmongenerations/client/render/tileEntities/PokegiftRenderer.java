/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.pokeballs.ModelPokeballs;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.enums.EnumPokechestVisibility;
import com.pixelmongenerations.common.block.loot.BlockPokeChest;
import com.pixelmongenerations.common.block.tileEntities.TileEntityPokegift;
import com.pixelmongenerations.core.enums.EnumCustomModel;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

public class PokegiftRenderer
extends TileEntityRenderer<TileEntityPokegift> {
    private static final BlockModelHolder<ModelPokeballs> cherishBall = new BlockModelHolder<ModelPokeballs>(ModelPokeballs.class, new Object[]{EnumCustomModel.Cherishball});
    private static final ResourceLocation pokegift = new ResourceLocation("pixelmon", "textures/pokeballs/cherishball.png");

    public PokegiftRenderer() {
        this.scale = 0.1f;
        this.yOffset = 0.03f;
        this.flip = false;
    }

    @Override
    public void renderTileEntity(TileEntityPokegift te, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        if (te.getVisibility() == EnumPokechestVisibility.Hidden) {
            return;
        }
        this.bindTexture(pokegift);
        ((ModelPokeballs)cherishBall.getModel()).render(null, 0.0625f);
    }

    @Override
    protected int getRotation(IBlockState state) {
        if (state.getBlock() instanceof BlockPokeChest) {
            EnumFacing facing = state.getValue(BlockPokeChest.FACING);
            if (facing == EnumFacing.EAST) {
                return 270;
            }
            if (facing == EnumFacing.NORTH) {
                return 0;
            }
            if (facing == EnumFacing.WEST) {
                return 90;
            }
            if (facing == EnumFacing.SOUTH) {
                return 180;
            }
        }
        return 0;
    }
}

