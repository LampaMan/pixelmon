/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.google.common.collect.Lists;
import com.pixelmongenerations.client.models.obj.ObjLoader;
import com.pixelmongenerations.client.models.obj.WavefrontObject;
import com.pixelmongenerations.common.block.apricornTrees.BlockApricornTree;
import com.pixelmongenerations.common.block.tileEntities.TileEntityApricornTree;
import com.pixelmongenerations.core.Pixelmon;
import java.util.ArrayList;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;

public class RenderTileEntityApricornTrees
extends TileEntitySpecialRenderer<TileEntityApricornTree> {
    private static ArrayList<ResourceLocation> TEXTURES = new ArrayList();

    @Override
    public void render(TileEntityApricornTree tile, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        if (tile.isCulled()) {
            return;
        }
        IBlockState state = Minecraft.getMinecraft().world.getBlockState(tile.getPos());
        if (!(state.getBlock() instanceof BlockApricornTree)) {
            Pixelmon.LOGGER.error("Apricorn tile entity without a block at " + tile.getPos().getX() + ", " + tile.getPos().getY() + ", " + tile.getPos().getZ());
            return;
        }
        BlockPos pos = Minecraft.getMinecraft().player.getPosition();
        boolean far = tile.getPos().getDistance(pos.getX(), pos.getY(), pos.getZ()) > 16.0;
        try {
            this.bindTexture(TEXTURES.get(tile.tree.ordinal() + (far ? 7 : 0)));
        }
        catch (NullPointerException e) {
            Pixelmon.LOGGER.error("Can't find texture: " + TEXTURES.get(tile.tree.ordinal() + (far ? 7 : 0)).getPath());
            return;
        }
        catch (ArrayIndexOutOfBoundsException e) {
            Pixelmon.LOGGER.error("Bad apricorn tree ordinal: " + tile.tree.ordinal() + (far ? 7 : 0));
            return;
        }
        GlStateManager.pushMatrix();
        GlStateManager.translate((float)x + 0.5f, (float)y, (float)z + 0.5f);
        GlStateManager.scale(-0.25f, 0.28f, -0.25f);
        GlStateManager.disableNormalize();
        GlStateManager.shadeModel(7425);
        int stage = tile.getStage();
        if (stage > 5) {
            stage = 5;
        }
        EnumApricornModel stageE = EnumApricornModel.values()[stage];
        if (far) {
            stageE.getFarModel().render();
        } else {
            stageE.getModel().render();
        }
        GlStateManager.popMatrix();
    }

    static {
        for (String ending : Lists.newArrayList(".png", "_far.png")) {
            for (String color : Lists.newArrayList("black", "white", "pink", "green", "blue", "yellow", "red")) {
                TEXTURES.add(new ResourceLocation("pixelmon:textures/blocks/apricorn_trees/apricorn" + color + ending));
            }
        }
    }

    public static enum EnumApricornModel {
        SEED("seed"),
        STAGE1("stage1"),
        STAGE2("stage2"),
        MIDDLE("middle"),
        STAGE3("stage3"),
        FINAL("final");

        public WavefrontObject obj;
        public WavefrontObject farObj;
        public final ResourceLocation path;
        public final ResourceLocation farPath;

        private EnumApricornModel(String name) {
            this.path = new ResourceLocation("pixelmon:models/blocks/apricorn_trees/" + name + ".obj");
            this.farPath = new ResourceLocation("pixelmon:models/blocks/apricorn_trees/" + name + "_far.obj");
        }

        public WavefrontObject getModel() {
            if (this.obj == null) {
                try {
                    if (ObjLoader.accepts(this.path)) {
                        this.obj = ObjLoader.loadModel(this.path);
                    }
                }
                catch (Exception e) {
                    System.out.println("Could not load the model: " + this.path.getPath());
                    e.printStackTrace();
                }
            }
            return this.obj;
        }

        public WavefrontObject getFarModel() {
            if (this.farObj == null) {
                try {
                    if (ObjLoader.accepts(this.farPath)) {
                        this.farObj = ObjLoader.loadModel(this.farPath);
                    }
                }
                catch (Exception e) {
                    System.out.println("Could not load the model: " + this.farPath.getPath());
                    e.printStackTrace();
                }
            }
            return this.farObj;
        }
    }
}

