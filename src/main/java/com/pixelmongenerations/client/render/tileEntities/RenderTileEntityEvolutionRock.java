/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.TileEntityEvolutionRock;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.ResourceLocation;

public class RenderTileEntityEvolutionRock
extends TileEntityRenderer<TileEntityEvolutionRock> {
    private static final BlockModelHolder<GenericSmdModel> IcyRock = new BlockModelHolder("blocks/evoRock/icyrock.pqc");
    private static final ResourceLocation iceTexture = new ResourceLocation("pixelmon", "textures/blocks/icyRockTex.png");
    private static final BlockModelHolder<GenericSmdModel> MossyRock = new BlockModelHolder("blocks/evoRock/mossyrock.pqc");
    private static final ResourceLocation mossyTexture = new ResourceLocation("pixelmon", "textures/blocks/mossyRockTex.png");

    public RenderTileEntityEvolutionRock() {
        this.correctionAngles = 180;
    }

    @Override
    public void renderTileEntity(TileEntityEvolutionRock rock, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        if (rock.getBlockType() == PixelmonBlocks.icyRock) {
            this.bindTexture(iceTexture);
            IcyRock.render();
        } else {
            this.bindTexture(mossyTexture);
            MossyRock.render();
        }
    }
}

