/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.TileEntityZygardeMachine;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.ResourceLocation;

public class RenderZygardeMachine
extends TileEntityRenderer<TileEntityZygardeMachine> {
    private static final ResourceLocation MACHINE_TEXTURE = new ResourceLocation("pixelmon", "textures/blocks/zygardemachine.png");
    private static final BlockModelHolder<GenericSmdModel> machineBase = new BlockModelHolder("blocks/zygardemachine/zygardemachine.pqc");
    private static final GenericSmdModel machineGlass = new GenericSmdModel("models/blocks/zygardemachine", "zygardemachineglass.pqc");

    public RenderZygardeMachine() {
        RenderZygardeMachine.machineGlass.modelRenderer.setTransparent(0.5f);
        this.correctionAngles = 180;
    }

    @Override
    public void renderTileEntity(TileEntityZygardeMachine machine, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        this.bindTexture(MACHINE_TEXTURE);
        if (machine.renderPass == 1) {
            machineGlass.renderModel(1.0f);
            return;
        }
        ((GenericSmdModel)machineBase.getModel()).renderModel(1.0f);
    }
}

