/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.vecmath.Vector3f
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.ICullable;
import com.pixelmongenerations.common.block.tileEntities.IFrameCounter;
import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import com.pixelmongenerations.common.block.tileEntities.TileEntityBookshelf;
import com.pixelmongenerations.common.block.tileEntities.TileEntityBush;
import com.pixelmongenerations.common.block.tileEntities.TileEntityCouch;
import com.pixelmongenerations.common.block.tileEntities.TileEntityDesk;
import com.pixelmongenerations.common.block.tileEntities.TileEntityDyeableFurniture;
import com.pixelmongenerations.common.block.tileEntities.TileEntityHouseLamp;
import com.pixelmongenerations.common.block.tileEntities.TileEntityPokeballPillar;
import com.pixelmongenerations.common.block.tileEntities.TileEntityWorkDesk;
import com.pixelmongenerations.core.proxy.ClientProxy;
import javax.vecmath.Vector3f;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.Vec3d;
import org.lwjgl.opengl.GL11;

public class GenericSmdRenderer<T extends TileEntity>
extends TileEntityRenderer<T> {
    private BlockModelHolder<GenericSmdModel> holder;
    ResourceLocation texture;
    private float angle = 0.0f;
    private Vector3f axis = new Vector3f(1.0f, 0.0f, 0.0f);
    private Vec3d offset;
    private Vec3d scaleVec;

    public GenericSmdRenderer(BlockModelHolder<GenericSmdModel> modelHolder, ResourceLocation texture) {
        this.holder = modelHolder;
        this.texture = texture;
    }

    public GenericSmdRenderer(ResourceLocation model, ResourceLocation texture) {
        this(new BlockModelHolder<GenericSmdModel>(model), texture);
    }

    public GenericSmdRenderer(String pqcPath, String texture) {
        this(new ResourceLocation("pixelmon", "models/" + pqcPath), texture == null ? null : new ResourceLocation("pixelmon:textures/blocks/" + texture));
    }

    @Override
    public void renderTileEntity(T te, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        TextureResource textureOpt;
        TileEntityDyeableFurniture tileEntity;
        boolean binded = false;
        if (te instanceof ICullable && ((ICullable)te).isCulled()) {
            return;
        }
        if (te instanceof TileEntityDyeableFurniture && (tileEntity = (TileEntityDyeableFurniture)te).getCustomTexture() != null && (textureOpt = ClientProxy.TEXTURE_STORE.getObject(tileEntity.getCustomTexture())) != null) {
            textureOpt.bindTexture();
            binded = true;
        }
        if (!binded) {
            if (te instanceof ISpecialTexture) {
                this.bindTexture(((ISpecialTexture)te).getTexture());
            } else {
                this.bindTexture(this.texture);
            }
        }
        GenericSmdModel model = (GenericSmdModel)this.holder.getModel();
        if (te instanceof IFrameCounter) {
            model.setFrame(((IFrameCounter)te).getFrame());
        }
        GlStateManager.pushMatrix();
        GlStateManager.rotate(this.angle, this.axis.x, this.axis.y, this.axis.z);
        GlStateManager.disableNormalize();
        GlStateManager.shadeModel(7425);
        GlStateManager.disableCull();
        if (this.offset != null) {
            GL11.glTranslated((double)this.offset.x, (double)this.offset.y, (double)this.offset.z);
        }
        if (this.scaleVec != null) {
            GL11.glScaled((double)this.scaleVec.x, (double)this.scaleVec.y, (double)this.scaleVec.z);
        }
        if (te instanceof TileEntityPokeballPillar) {
            GlStateManager.rotate(180.0f, 0.0f, 0.0f, 1.0f);
            GlStateManager.rotate(90.0f, 1.0f, 0.0f, 0.0f);
            GlStateManager.scale(11.0f, 11.0f, 11.0f);
        }
        if (te instanceof TileEntityBush) {
            GlStateManager.scale(1.2f, 1.2f, 1.2f);
            GlStateManager.translate(0.023, 0.0, 0.02);
        }
        if (te instanceof TileEntityBookshelf) {
            GlStateManager.rotate(180.0f, 0.0f, 1.0f, 0.0f);
            GlStateManager.scale(0.75f, 1.05f, 0.75f);
            GlStateManager.translate(1.38, 0.0, 0.1);
        }
        if (te instanceof TileEntityDesk) {
            GlStateManager.rotate(180.0f, 0.0f, 1.0f, 0.0f);
            GlStateManager.scale(0.73f, 0.73f, 0.73f);
            GlStateManager.translate(1.38, 0.0, 0.0);
        }
        if (te instanceof TileEntityWorkDesk) {
            GlStateManager.rotate(180.0f, 0.0f, 1.0f, 0.0f);
            GlStateManager.scale(0.7f, 0.72f, 0.7f);
            GlStateManager.translate(1.43, 0.0, 0.0);
        }
        if (te instanceof TileEntityHouseLamp) {
            GlStateManager.scale(0.7f, 0.7f, 0.7f);
        }
        if (te instanceof TileEntityCouch) {
            GlStateManager.rotate(180.0f, 0.0f, 1.0f, 0.0f);
            GlStateManager.scale(0.9f, 0.95f, 0.7f);
            GlStateManager.translate(1.3, 0.0, 0.0);
        }
        model.render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, 1.0f);
        GlStateManager.popMatrix();
    }

    public GenericSmdRenderer<T> enableBlend() {
        this.blend = true;
        return this;
    }

    public GenericSmdRenderer<T> disableCulling() {
        this.disableCulling = true;
        return this;
    }

    public GenericSmdRenderer<T> disableLighting() {
        this.disableLighting = true;
        return this;
    }

    public GenericSmdRenderer<T> setCorrectionAngles(int correctionAngles) {
        this.correctionAngles = correctionAngles;
        return this;
    }

    public GenericSmdRenderer<T> setYOffset(float yOffset) {
        this.yOffset = yOffset;
        return this;
    }

    public GenericSmdRenderer<T> rotate(float d, float axisX, float axisY, float axisZ) {
        this.angle = d;
        this.axis = new Vector3f(axisX, axisY, axisZ);
        return this;
    }

    public GenericSmdRenderer<T> scale(float scale) {
        this.scale = scale;
        return this;
    }

    public GenericSmdRenderer<T> scale(double x, double y, double z) {
        this.scaleVec = new Vec3d(x, y, z);
        return this;
    }

    public GenericSmdRenderer<T> offset(double x, double y, double z) {
        this.offset = new Vec3d(x, y, z);
        return this;
    }
}

