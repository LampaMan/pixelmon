/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.TileEntityCookingPot;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class CookingPotRenderer
extends TileEntityRenderer<TileEntityCookingPot> {
    private static final ResourceLocation COOKING_POT_TEXTURE = new ResourceLocation("pixelmon", "textures/blocks/cooking_pot.png");
    private static final ResourceLocation UNLIT_CAMPFIRE_TEXTURE = new ResourceLocation("pixelmon", "textures/blocks/unlit_campfire.png");
    private static final ResourceLocation LIT_CAMPFIRE_TEXTURE = new ResourceLocation("pixelmon", "textures/blocks/lit_campfire.png");
    private static final BlockModelHolder<GenericSmdModel> cookingPot = new BlockModelHolder("blocks/cookingpot/cookingpot.pqc");
    private static final BlockModelHolder<GenericSmdModel> campfire = new BlockModelHolder("blocks/cookingpot/campfire/campfire.pqc");

    public CookingPotRenderer() {
        this.correctionAngles = 180;
    }

    @Override
    public void renderTileEntity(TileEntityCookingPot pot, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        this.bindTexture(pot.isCooking() ? LIT_CAMPFIRE_TEXTURE : UNLIT_CAMPFIRE_TEXTURE);
        GL11.glPushMatrix();
        GL11.glScalef((float)0.01f, (float)0.01f, (float)0.01f);
        campfire.render();
        this.bindTexture(COOKING_POT_TEXTURE);
        cookingPot.render();
        GL11.glPopMatrix();
        GlStateManager.pushMatrix();
        GlStateManager.translate(0.0f, -0.6875f, 0.0f);
        if (pot.getOuput().isEmpty()) {
            CookingPotRenderer.renderStack(pot.getIngredient(), 0.25);
            for (int i = 0; i < 10; ++i) {
                ItemStack berry = pot.getBerry(i);
                GlStateManager.pushMatrix();
                GlStateManager.rotate(i * 36, 0.0f, 1.0f, 0.0f);
                GlStateManager.translate(0.25, 0.03 + -0.001 * (double)i, 0.0);
                CookingPotRenderer.renderStack(berry, 0.25);
                GlStateManager.popMatrix();
            }
        } else {
            CookingPotRenderer.renderStack(pot.getOuput(), 0.75);
        }
        GlStateManager.popMatrix();
    }

    public static void renderStack(ItemStack stack, double scale) {
        GlStateManager.pushMatrix();
        GlStateManager.rotate(90.0f, 1.0f, 0.0f, 0.0f);
        GlStateManager.pushMatrix();
        GlStateManager.scale(scale, scale, scale);
        Minecraft.getMinecraft().getRenderItem().renderItem(stack, ItemCameraTransforms.TransformType.FIXED);
        GlStateManager.popMatrix();
        GlStateManager.popMatrix();
    }
}

