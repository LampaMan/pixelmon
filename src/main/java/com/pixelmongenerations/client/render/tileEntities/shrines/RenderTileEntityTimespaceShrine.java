/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities.shrines;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.shrines.RenderTileEntityShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityTimespaceAltar;
import com.pixelmongenerations.core.enums.EnumSpecies;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;

public class RenderTileEntityTimespaceShrine
extends RenderTileEntityShrine<TileEntityTimespaceAltar> {
    private static final BlockModelHolder<GenericSmdModel> main_shrine = new BlockModelHolder("blocks/shrines/creation_trio/time_space_altar.pqc");
    private static final ResourceLocation main_texture = new ResourceLocation("pixelmon", "textures/blocks/shrines/timespace/timespacealtar.png");
    private static final BlockModelHolder<GenericSmdModel> lustrous_orb = new BlockModelHolder("blocks/shrines/creation_trio/lustrous_orb.pqc");
    private static final ResourceLocation lustrous_orb_texture = new ResourceLocation("pixelmon", "textures/blocks/shrines/timespace/lustrous_orb.png");
    private static final BlockModelHolder<GenericSmdModel> adamant_orb = new BlockModelHolder("blocks/shrines/creation_trio/adamant_orb.pqc");
    private static final ResourceLocation adamant_orb_texture = new ResourceLocation("pixelmon", "textures/blocks/shrines/timespace/adamant_orb.png");
    private static final BlockModelHolder<GenericSmdModel> griseous_orb = new BlockModelHolder("blocks/shrines/creation_trio/griseous_orb.pqc");
    private static final ResourceLocation griseous_orb_texture = new ResourceLocation("pixelmon", "textures/blocks/shrines/timespace/griseous_orb.png");
    private double theta = 0.0;

    public RenderTileEntityTimespaceShrine() {
        super("blocks/shrines/lugia_shrine/lugia_shrine.pqc", "lugia_shrine.png");
    }

    @Override
    public void renderTileEntity(TileEntityTimespaceAltar shrineBlock, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        GlStateManager.pushMatrix();
        EnumSpecies species = null;
        switch (shrineBlock.orbIn) {
            case 1: {
                this.bindTexture(lustrous_orb_texture);
                lustrous_orb.render();
                species = EnumSpecies.Palkia;
                break;
            }
            case 2: {
                this.bindTexture(adamant_orb_texture);
                adamant_orb.render();
                species = EnumSpecies.Dialga;
                break;
            }
            case 3: {
                this.bindTexture(griseous_orb_texture);
                griseous_orb.render();
                species = EnumSpecies.Giratina;
            }
        }
        this.bindTexture(main_texture);
        main_shrine.render();
        GlStateManager.popMatrix();
        if (shrineBlock.getTick() > 0) {
            this.processAnimation(shrineBlock, species);
        }
    }
}

