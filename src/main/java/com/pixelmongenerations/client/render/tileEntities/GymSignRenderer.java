/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.TileEntityGymSign;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class GymSignRenderer
extends TileEntityRenderer<TileEntityGymSign> {
    private static final BlockModelHolder<GenericSmdModel> model = new BlockModelHolder("pixelutilities/GymSign/GymSign.pqc");
    private static final BlockModelHolder<GenericSmdModel> m3d = new BlockModelHolder("pixelutilities/GymSign/3d.pqc");
    private static final BlockModelHolder<GenericSmdModel> m2d = new BlockModelHolder("pixelutilities/GymSign/2d.pqc");

    public GymSignRenderer() {
        this.disableCulling = true;
    }

    @Override
    public void renderTileEntity(TileEntityGymSign gymSign, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        this.bindTexture(gymSign.getTexture());
        model.render();
        ItemStack item = gymSign.getItemInSign();
        if (item != null) {
            try {
                ResourceLocation resource;
                IBakedModel model = Minecraft.getMinecraft().getRenderItem().getItemModelMesher().getItemModel(item);
                String[] split = model.getOverrides().handleItemState(model, item, gymSign.getWorld(), null).getParticleTexture().getIconName().split(":");
                if (split.length < 1 || split.length > 2) {
                    return;
                }
                ResourceLocation resourceLocation = resource = split.length == 1 ? new ResourceLocation(split[0]) : new ResourceLocation(split[0], "textures/" + split[1] + ".png");
                if (Minecraft.getMinecraft().getRenderItem().getItemModelMesher().getItemModel(item).isGui3d()) {
                    this.bindTexture(resource);
                    m3d.render();
                } else {
                    this.bindTexture(resource);
                    m2d.render();
                }
            }
            catch (NullPointerException nullPointerException) {
                // empty catch block
            }
        }
    }
}

