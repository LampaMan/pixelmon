/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.ModelTV;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.TileEntityTV;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.ResourceLocation;

public class TVRenderer
extends TileEntityRenderer<TileEntityTV> {
    private static final BlockModelHolder<ModelTV> model = new BlockModelHolder<ModelTV>(ModelTV.class);
    private static final ResourceLocation texture = new ResourceLocation("pixelmon", "textures/blocks/TVModel.png");

    public TVRenderer() {
        this.correctionAngles = 180;
        this.scale = 0.0625f;
        this.yOffset = 1.5f;
    }

    @Override
    public void renderTileEntity(TileEntityTV te, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        this.bindTexture(texture);
        model.render();
    }
}

