/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.TileEntityRanchBlock;
import com.pixelmongenerations.core.network.PixelmonData;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.ResourceLocation;

public class RenderTileEntityRanch
extends TileEntityRenderer<TileEntityRanchBlock> {
    private static final BlockModelHolder<GenericSmdModel> model = new BlockModelHolder("blocks/ranch/ranch.pqc");
    private static final ResourceLocation texture = new ResourceLocation("pixelmon", "textures/blocks/ranchblock.png");
    private static final BlockModelHolder<GenericSmdModel> Egg = new BlockModelHolder("blocks/ranch/ranch_egg.pqc");
    private static final ResourceLocation togepiEgg = new ResourceLocation("pixelmon", "textures/eggs/eggTogepi.png");
    private static final ResourceLocation greenEgg = new ResourceLocation("pixelmon", "textures/eggs/eggGreen.png");

    public RenderTileEntityRanch() {
        this.correctionAngles = 180;
    }

    @Override
    public void renderTileEntity(TileEntityRanchBlock ranch, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        PixelmonData eggData;
        this.bindTexture(texture);
        int ranchCurrentFrame = ranch.percentAbove / 5;
        ((GenericSmdModel)model.getModel()).setFrame(ranchCurrentFrame);
        model.render();
        if (ranch.hasEgg() && (eggData = ranch.getPokemonEggData()) != null) {
            if (eggData.name.equals("Togepi")) {
                this.bindTexture(togepiEgg);
            } else {
                this.bindTexture(greenEgg);
            }
            ((GenericSmdModel)Egg.getModel()).setFrame(ranchCurrentFrame);
            Egg.render();
        }
    }
}

