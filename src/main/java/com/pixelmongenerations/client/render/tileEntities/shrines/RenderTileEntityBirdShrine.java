/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities.shrines;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.particle.ParticleRedstone;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.shrines.RenderTileEntityShrine;
import com.pixelmongenerations.common.block.enums.EnumUsed;
import com.pixelmongenerations.common.block.spawnmethod.BlockBirdShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.SpawnColors;
import java.awt.Color;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.Vec3d;

public class RenderTileEntityBirdShrine
extends RenderTileEntityShrine<TileEntityShrine> {
    private static final BlockModelHolder<GenericSmdModel> ArticunoShrine = new BlockModelHolder("blocks/shrines/legendary_birds/articuno_shrine.pqc");
    private static final ResourceLocation articuno_texture = new ResourceLocation("pixelmon", "textures/blocks/shrines/legendary_birds/articuno_shrine.png");
    private static final BlockModelHolder<GenericSmdModel> MoltresShrine = new BlockModelHolder("blocks/shrines/legendary_birds/moltres_shrine.pqc");
    private static final ResourceLocation moltres_texture = new ResourceLocation("pixelmon", "textures/blocks/shrines/legendary_birds/moltres_shrine.png");
    private static final BlockModelHolder<GenericSmdModel> ZapdosShrine = new BlockModelHolder("blocks/shrines/legendary_birds/zapdos_shrine.pqc");
    private static final ResourceLocation zapdos_texture = new ResourceLocation("pixelmon", "textures/blocks/shrines/legendary_birds/zapdos_shrine.png");

    public RenderTileEntityBirdShrine() {
        super("blocks/shrines/lugia_shrine/lugia_shrine.pqc", "lugia_shrine.png");
    }

    @Override
    public void renderTileEntity(TileEntityShrine shrineBlock, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        boolean used;
        boolean bl = used = (state.getBlock() instanceof BlockBirdShrine ? state.getValue(BlockBirdShrine.USED) : EnumUsed.NO) == EnumUsed.YES;
        if (state.getBlock() == PixelmonBlocks.shrineUno) {
            this.renderShrine(ArticunoShrine, articuno_texture);
            if (shrineBlock.getTick() > 0) {
                this.processAnimation(shrineBlock, EnumSpecies.Articuno);
            } else if (used) {
                RenderTileEntityBirdShrine.processOrb(shrineBlock, EnumSpecies.Articuno);
            }
        } else if (state.getBlock() == PixelmonBlocks.shrineTres) {
            this.renderShrine(MoltresShrine, moltres_texture);
            if (shrineBlock.getTick() > 0) {
                this.processAnimation(shrineBlock, EnumSpecies.Moltres);
            } else if (used) {
                RenderTileEntityBirdShrine.processOrb(shrineBlock, EnumSpecies.Moltres);
            }
        } else {
            this.renderShrine(ZapdosShrine, zapdos_texture);
            if (shrineBlock.getTick() > 0) {
                this.processAnimation(shrineBlock, EnumSpecies.Zapdos);
            } else if (used) {
                RenderTileEntityBirdShrine.processOrb(shrineBlock, EnumSpecies.Zapdos);
            }
        }
    }

    private static void processOrb(TileEntityShrine shrineBlock, EnumSpecies species) {
        Vec3d center = new Vec3d(shrineBlock.getSpawnPos()).add(0.5, 0.5, 0.5);
        double theta = (double)shrineBlock.getWorld().getTotalWorldTime() % (double)shrineBlock.maxTick() / (double)shrineBlock.maxTick() * 2.0 * Math.PI;
        double radius = 1.5;
        double dx = radius * Math.cos(theta);
        double dy = radius * Math.sin(theta);
        Color c = SpawnColors.WHITE;
        switch (species) {
            case Articuno: {
                c = SpawnColors.LIGHT_BLUE;
                break;
            }
            case Zapdos: {
                c = SpawnColors.YELLOW;
                break;
            }
            case Moltres: {
                c = SpawnColors.ORANGE;
                break;
            }
        }
        Minecraft.getMinecraft().effectRenderer.addEffect(new ParticleRedstone(shrineBlock.getWorld(), center.x + dx, center.y, center.z + dy, (float)c.getRed() / 255.0f, (float)c.getGreen() / 255.0f, (float)c.getBlue() / 255.0f));
    }

    private void renderShrine(BlockModelHolder<GenericSmdModel> shrine, ResourceLocation texture) {
        this.bindTexture(texture);
        GlStateManager.pushMatrix();
        shrine.render();
        GlStateManager.popMatrix();
    }
}

