/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.MultiBlock;
import com.pixelmongenerations.common.block.spawnmethod.BlockPrisonBottleStem;
import com.pixelmongenerations.common.block.tileEntities.TileEntityPrisonBottleStem;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

public class RenderPrisonBottle
extends TileEntityRenderer<TileEntityPrisonBottleStem> {
    private static final ResourceLocation BOTTLE_TEXTURE = new ResourceLocation("pixelmon", "textures/blocks/prison_bottle.png");
    private static final ResourceLocation ACTIVATED_BOTTLE_TEXTURE = new ResourceLocation("pixelmon", "textures/blocks/prison_bottle_activated.png");
    private static BlockModelHolder<GenericSmdModel> machineBase = new BlockModelHolder("blocks/prisonbottle/prison_bottle.pqc");
    private static BlockModelHolder<GenericSmdModel> ring1 = new BlockModelHolder("blocks/prisonbottle/prison_bottle_1.pqc");
    private static BlockModelHolder<GenericSmdModel> ring2 = new BlockModelHolder("blocks/prisonbottle/prison_bottle_2.pqc");
    private static BlockModelHolder<GenericSmdModel> ring3 = new BlockModelHolder("blocks/prisonbottle/prison_bottle_3.pqc");
    private static BlockModelHolder<GenericSmdModel> ring4 = new BlockModelHolder("blocks/prisonbottle/prison_bottle_4.pqc");
    private static BlockModelHolder<GenericSmdModel> ring5 = new BlockModelHolder("blocks/prisonbottle/prison_bottle_5.pqc");
    private static BlockModelHolder<GenericSmdModel> ring6 = new BlockModelHolder("blocks/prisonbottle/prison_bottle_6.pqc");

    public RenderPrisonBottle() {
        this.correctionAngles = 180;
    }

    private void render(IBlockState state, BlockModelHolder<GenericSmdModel> model) {
        GlStateManager.pushMatrix();
        GlStateManager.rotate(90.0f, -1.0f, 0.0f, 0.0f);
        GlStateManager.rotate(180.0f, 0.0f, 0.0f, this.getRotation(state));
        ((GenericSmdModel)model.getModel()).renderModel(1.0f);
        GlStateManager.popMatrix();
    }

    @Override
    public void renderTileEntity(TileEntityPrisonBottleStem machine, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        if (machine.getRingCount() != 6) {
            this.bindTexture(BOTTLE_TEXTURE);
        } else {
            this.bindTexture(ACTIVATED_BOTTLE_TEXTURE);
        }
        switch (machine.getRingCount()) {
            case 1: {
                this.render(state, ring1);
                break;
            }
            case 2: {
                this.render(state, ring2);
                break;
            }
            case 3: {
                this.render(state, ring3);
                break;
            }
            case 4: {
                this.render(state, ring4);
                break;
            }
            case 5: {
                this.render(state, ring5);
                break;
            }
            case 6: {
                this.render(state, ring6);
                break;
            }
            default: {
                this.render(state, machineBase);
            }
        }
    }

    @Override
    protected int getRotation(IBlockState state) {
        if (state.getBlock() instanceof BlockPrisonBottleStem) {
            EnumFacing facing = state.getValue(MultiBlock.FACING);
            if (facing == EnumFacing.WEST) {
                return 90;
            }
            if (facing == EnumFacing.SOUTH) {
                return 180;
            }
            if (facing == EnumFacing.EAST) {
                return 270;
            }
            return 360;
        }
        return 0;
    }

    static {
        machineBase.getModel();
        ring1.getModel();
        ring2.getModel();
        ring3.getModel();
        ring4.getModel();
        ring5.getModel();
        ring6.getModel();
    }
}

