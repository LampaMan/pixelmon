/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.models.pokeballs.ModelPokeballs;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.core.enums.EnumCustomModel;
import com.pixelmongenerations.core.enums.items.EnumPokeball;

public class SharedModels {
    private static final BlockModelHolder<ModelPokeballs> baseBall = new BlockModelHolder<ModelPokeballs>(ModelPokeballs.class, new Object[]{EnumCustomModel.Pokeball});
    private static final BlockModelHolder<ModelPokeballs> greatBall = new BlockModelHolder<ModelPokeballs>(ModelPokeballs.class, new Object[]{EnumCustomModel.Greatball});
    private static final BlockModelHolder<ModelPokeballs> heavyBall = new BlockModelHolder<ModelPokeballs>(ModelPokeballs.class, new Object[]{EnumCustomModel.Heavyball});
    private static final BlockModelHolder<ModelPokeballs> netBall = new BlockModelHolder<ModelPokeballs>(ModelPokeballs.class, new Object[]{EnumCustomModel.Netball});
    private static final BlockModelHolder<ModelPokeballs> timerBall = new BlockModelHolder<ModelPokeballs>(ModelPokeballs.class, new Object[]{EnumCustomModel.Timerball});
    private static final BlockModelHolder<ModelPokeballs> cherishBall = new BlockModelHolder<ModelPokeballs>(ModelPokeballs.class, new Object[]{EnumCustomModel.Cherishball});
    private static final BlockModelHolder<ModelPokeballs> masterBall = new BlockModelHolder<ModelPokeballs>(ModelPokeballs.class, new Object[]{EnumCustomModel.Masterball});
    private static final BlockModelHolder<ModelPokeballs> beastBall = new BlockModelHolder<ModelPokeballs>(ModelPokeballs.class, new Object[]{EnumCustomModel.Beastball});
    public static final BlockModelHolder<GenericSmdModel> helixFossil = new BlockModelHolder("fossils/helix.pqc");
    public static final BlockModelHolder<GenericSmdModel> domeFossil = new BlockModelHolder("fossils/dome.pqc");
    public static final BlockModelHolder<GenericSmdModel> oldAmber = new BlockModelHolder("fossils/oldamber.pqc");
    public static final BlockModelHolder<GenericSmdModel> rootFossil = new BlockModelHolder("fossils/root.pqc");
    public static final BlockModelHolder<GenericSmdModel> clawFossil = new BlockModelHolder("fossils/claw.pqc");
    public static final BlockModelHolder<GenericSmdModel> skullFossil = new BlockModelHolder("fossils/skull.pqc");
    public static final BlockModelHolder<GenericSmdModel> armorFossil = new BlockModelHolder("fossils/armor.pqc");
    public static final BlockModelHolder<GenericSmdModel> coverFossil = new BlockModelHolder("fossils/cover.pqc");
    public static final BlockModelHolder<GenericSmdModel> plumeFossil = new BlockModelHolder("fossils/plume.pqc");
    public static final BlockModelHolder<GenericSmdModel> sailFossil = new BlockModelHolder("fossils/sail.pqc");
    public static final BlockModelHolder<GenericSmdModel> jawFossil = new BlockModelHolder("fossils/jaw.pqc");
    public static final BlockModelHolder<GenericSmdModel> birdFossil = new BlockModelHolder("fossils/bird.pqc");
    public static final BlockModelHolder<GenericSmdModel> drakeFossil = new BlockModelHolder("fossils/drake.pqc");
    public static final BlockModelHolder<GenericSmdModel> dinoFossil = new BlockModelHolder("fossils/dino.pqc");
    public static final BlockModelHolder<GenericSmdModel> fishFossil = new BlockModelHolder("fossils/fish.pqc");
    public static final BlockModelHolder<GenericSmdModel> arctovishFossil = new BlockModelHolder("fossils/arctovish.pqc");
    public static final BlockModelHolder<GenericSmdModel> arctozoltFossil = new BlockModelHolder("fossils/arctozolt.pqc");
    public static final BlockModelHolder<GenericSmdModel> dracovishFossil = new BlockModelHolder("fossils/dracovish.pqc");
    public static final BlockModelHolder<GenericSmdModel> dracozoltFossil = new BlockModelHolder("fossils/dracozolt.pqc");
    public static final BlockModelHolder<GenericSmdModel> pokechestBase = new BlockModelHolder("blocks/pokechest/base.pqc");
    public static final BlockModelHolder<GenericSmdModel> pokechestMaster = new BlockModelHolder("blocks/pokechest/masterball.pqc");
    public static final BlockModelHolder<GenericSmdModel> pokechestBeast = new BlockModelHolder("blocks/pokechest/beastball.pqc");
    public static final BlockModelHolder<GenericSmdModel> pokeStop = new BlockModelHolder("blocks/pokechest/pokestop.pqc");

    public static BlockModelHolder<? extends ModelPokeballs> getPokeballModel(EnumPokeball pokeball) {
        switch (pokeball) {
            case GreatBall: {
                return greatBall;
            }
            case HeavyBall: {
                return heavyBall;
            }
            case NetBall: {
                return netBall;
            }
            case TimerBall: {
                return timerBall;
            }
            case CherishBall: {
                return cherishBall;
            }
            case MasterBall: {
                return masterBall;
            }
            case BeastBall: {
                return beastBall;
            }
        }
        return baseBall;
    }
}

