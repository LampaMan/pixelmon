/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.models.blocks.ModelMechanicalAnvil;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.TileEntityMechanicalAnvil;
import com.pixelmongenerations.core.config.PixelmonItemsPokeballs;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class RenderTileEntityMechanicalAnvil
extends TileEntityRenderer<TileEntityMechanicalAnvil> {
    private static final BlockModelHolder<ModelMechanicalAnvil> model = new BlockModelHolder<ModelMechanicalAnvil>(ModelMechanicalAnvil.class);
    private static final ResourceLocation anvilTextureRunning = new ResourceLocation("pixelmon", "textures/blocks/mechanvil_on.png");
    private static final ResourceLocation anvilTextureIdle = new ResourceLocation("pixelmon", "textures/blocks/mechanvil_off.png");
    private static final BlockModelHolder<GenericSmdModel> baseBallModel = new BlockModelHolder("blocks/mech_anvil/balls/base_ball/base_ball.pqc");
    private static final BlockModelHolder<GenericSmdModel> greatBallModel = new BlockModelHolder("blocks/mech_anvil/balls/great_ball/great_ball.pqc");
    private static final BlockModelHolder<GenericSmdModel> heavyBallModel = new BlockModelHolder("blocks/mech_anvil/balls/heavy_ball/heavy_ball.pqc");
    private static final BlockModelHolder<GenericSmdModel> netBallModel = new BlockModelHolder("blocks/mech_anvil/balls/net_ball/net_ball.pqc");
    private static final BlockModelHolder<GenericSmdModel> timerBallModel = new BlockModelHolder("blocks/mech_anvil/balls/timer_ball/timer_ball.pqc");

    @Override
    public void renderTileEntity(TileEntityMechanicalAnvil mechAnvil, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        this.bindTexture(mechAnvil.isRunning ? anvilTextureRunning : anvilTextureIdle);
        if ((double)partialTicks > 0.5 && mechAnvil.frame != 0) {
            ((ModelMechanicalAnvil)model.getModel()).setFrame(mechAnvil.frame + 1);
        } else {
            ((ModelMechanicalAnvil)model.getModel()).setFrame(mechAnvil.frame);
        }
        model.render();
        ItemStack stackInSlot = mechAnvil.getStackInSlot(0);
        if (stackInSlot != null) {
            Item item = stackInSlot.getItem();
            BlockModelHolder<GenericSmdModel> model = null;
            if (item == PixelmonItemsPokeballs.pokeBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/PokeBall.png"));
                model = baseBallModel;
            } else if (item == PixelmonItemsPokeballs.greatBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/GreatBall.png"));
                model = greatBallModel;
            } else if (item == PixelmonItemsPokeballs.ultraBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/UltraBall.png"));
                model = baseBallModel;
            } else if (item == PixelmonItemsPokeballs.levelBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/LevelBall.png"));
                model = baseBallModel;
            } else if (item == PixelmonItemsPokeballs.moonBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/MoonBall.png"));
                model = baseBallModel;
            } else if (item == PixelmonItemsPokeballs.friendBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/FriendBall.png"));
                model = baseBallModel;
            } else if (item == PixelmonItemsPokeballs.loveBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/LoveBall.png"));
                model = baseBallModel;
            } else if (item == PixelmonItemsPokeballs.safariBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/SafariBall.png"));
                model = baseBallModel;
            } else if (item == PixelmonItemsPokeballs.heavyBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/HeavyBall.png"));
                model = heavyBallModel;
            } else if (item == PixelmonItemsPokeballs.fastBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/FastBall.png"));
                model = baseBallModel;
            } else if (item == PixelmonItemsPokeballs.repeatBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/RepeatBall.png"));
                model = baseBallModel;
            } else if (item == PixelmonItemsPokeballs.timerBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/TimerBall.png"));
                model = timerBallModel;
            } else if (item == PixelmonItemsPokeballs.nestBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/NestBall.png"));
                model = baseBallModel;
            } else if (item == PixelmonItemsPokeballs.netBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/NetBall.png"));
                model = netBallModel;
            } else if (item == PixelmonItemsPokeballs.diveBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/DiveBall.png"));
                model = baseBallModel;
            } else if (item == PixelmonItemsPokeballs.luxuryBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/LuxuryBall.png"));
                model = baseBallModel;
            } else if (item == PixelmonItemsPokeballs.healBall) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/HealBall.png"));
                model = baseBallModel;
            } else if (item == PixelmonItemsPokeballs.duskBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/DuskBall.png"));
                model = baseBallModel;
            } else if (item == PixelmonItemsPokeballs.premierBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/PremierBall.png"));
                model = baseBallModel;
            } else if (item == PixelmonItemsPokeballs.sportBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/SportBall.png"));
                model = baseBallModel;
            } else if (item == PixelmonItemsPokeballs.quickBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/QuickBall.png"));
                model = baseBallModel;
            } else if (item == PixelmonItemsPokeballs.lureBallDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/LureBall.png"));
                model = baseBallModel;
            } else if (item == PixelmonItemsPokeballs.ironDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/IronAluminium.png"));
                model = baseBallModel;
            } else if (item == PixelmonItemsPokeballs.aluDisc) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/mechanvilballs/IronAluminium.png"));
                model = baseBallModel;
            }
            if (model == null) {
                return;
            }
            ((GenericSmdModel)model.getModel()).setFrame(mechAnvil.frame);
            model.render();
        }
    }
}

