/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.TileEntityPokeDoll;
import net.minecraft.block.state.IBlockState;
import org.lwjgl.opengl.GL11;

public class RenderTileEntityPokedoll
extends TileEntityRenderer<TileEntityPokeDoll> {
    public RenderTileEntityPokedoll() {
        this.scale = 0.7f;
        this.disableCulling = true;
    }

    @Override
    public void renderTileEntity(TileEntityPokeDoll te, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        if (te.isCulled()) {
            return;
        }
        this.bindTexture(te.getTexture());
        GL11.glPushMatrix();
        GL11.glRotatef((float)(45.0f * (float)te.getRotationIndex()), (float)0.0f, (float)1.0f, (float)0.0f);
        te.getModel().render(te.getScale());
        GL11.glPopMatrix();
    }
}

