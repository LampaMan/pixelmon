/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.render.custom;

import com.pixelmongenerations.client.assets.resource.MinecraftModelResource;
import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.common.cosmetic.PositionOperation;
import com.pixelmongenerations.core.data.asset.entry.MinecraftModelAsset;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.util.Arrays;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.tileentity.TileEntityItemStackRenderer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.client.ForgeHooksClient;
import org.lwjgl.opengl.GL11;

public class PixelmonItemStackRenderer
extends TileEntityItemStackRenderer {
    private Minecraft minecraft = Minecraft.getMinecraft();
    private BlockRendererDispatcher blockrendererdispatcher = this.minecraft.getBlockRendererDispatcher();

    @Override
    public void renderByItem(ItemStack itemStackIn) {
        this.renderByItem(itemStackIn, 1.0f);
    }

    @Override
    public void renderByItem(ItemStack itemStackIn, float partialTicks) {
        String serverItemTexture;
        if (itemStackIn.getTagCompound() == null) {
            return;
        }
        NBTTagCompound compound = itemStackIn.getTagCompound();
        String serverItemModel = compound.getString("ServerItemModel");
        if (serverItemModel.isEmpty() || (serverItemTexture = compound.getString("ServerItemTexture")).isEmpty()) {
            return;
        }
        MinecraftModelResource model = ClientProxy.MINECRAFT_MODEL_STORE.getObject(serverItemModel);
        TextureResource texture = ClientProxy.TEXTURE_STORE.getObject(serverItemTexture);
        if (model != null && model.getModel() != null && texture != null) {
            PositionOperation[] operations;
            IBakedModel bakedmodel = model.getModel();
            GL11.glPushMatrix();
            texture.bindTexture();
            String stackTrace = Arrays.toString(Thread.currentThread().getStackTrace());
            if (stackTrace.contains("renderItemModelIntoGUI") || stackTrace.contains("func_191962_a") || stackTrace.contains("func_184391_a")) {
                bakedmodel = ForgeHooksClient.handleCameraTransforms(bakedmodel, ItemCameraTransforms.TransformType.GUI, false);
                GL11.glTranslatef((float)-0.365f, (float)-0.055f, (float)0.0f);
                operations = ((MinecraftModelAsset)model.entry).guiOperations;
            } else {
                operations = stackTrace.contains("RenderArmorStand") ? ((MinecraftModelAsset)model.entry).armorStandOperations : (stackTrace.contains("renderHeldItem") || stackTrace.contains("func_188358_a") ? ((MinecraftModelAsset)model.entry).thirdPersonOperations : ((MinecraftModelAsset)model.entry).operations);
            }
            if (operations != null) {
                block7: for (PositionOperation op : operations) {
                    switch (op.getType()) {
                        case Rotate: {
                            GlStateManager.rotate(op.getAngle(), op.getX(), op.getY(), op.getZ());
                            continue block7;
                        }
                        case Scale: {
                            GlStateManager.scale(op.getX(), op.getY(), op.getZ());
                            continue block7;
                        }
                        case Translate: {
                            GlStateManager.translate(op.getX(), op.getY(), op.getZ());
                            continue block7;
                        }
                    }
                }
            }
            try {
                if (this.blockrendererdispatcher == null) {
                    this.blockrendererdispatcher = this.minecraft.getBlockRendererDispatcher();
                }
                this.blockrendererdispatcher.getBlockModelRenderer().renderModelBrightnessColor(bakedmodel, 1.0f, 1.0f, 1.0f, 1.0f);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            GL11.glPopMatrix();
        }
    }
}

