/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.custom;

import com.pixelmongenerations.common.entity.custom.EntityPixelmonPainting;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.block.model.ModelManager;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.culling.ICamera;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.client.model.PerspectiveMapWrapper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(value=Side.CLIENT)
public class RenderPixelmonPainting
extends Render<EntityPixelmonPainting> {
    private final Minecraft mc = Minecraft.getMinecraft();
    private final ModelResourceLocation itemFrameModel = new ModelResourceLocation("item_frame", "normal");
    private RenderItem itemRenderer = Minecraft.getMinecraft().getRenderItem();

    public RenderPixelmonPainting(RenderManager renderManagerIn) {
        super(renderManagerIn);
    }

    @Override
    public boolean shouldRender(EntityPixelmonPainting entity, ICamera camera, double camX, double camY, double camZ) {
        return true;
    }

    @Override
    public void doRender(EntityPixelmonPainting entity, double x, double y, double z, float entityYaw, float partialTicks) {
        BlockRendererDispatcher blockrendererdispatcher = this.mc.getBlockRendererDispatcher();
        ModelManager modelmanager = blockrendererdispatcher.getBlockModelShapes().getModelManager();
        IBakedModel model = modelmanager.getModel(this.itemFrameModel);
        PerspectiveMapWrapper iPerspectiveAwareModel = null;
        if (model instanceof PerspectiveMapWrapper) {
            iPerspectiveAwareModel = (PerspectiveMapWrapper)model;
        }
        GlStateManager.pushMatrix();
        BlockPos blockpos = entity.getHangingPosition();
        double d3 = (double)blockpos.getX() - entity.posX + x;
        double d4 = (double)blockpos.getY() - entity.posY + y;
        double d5 = (double)blockpos.getZ() - entity.posZ + z;
        GlStateManager.translate(d3 + 0.5, d4 + 0.5, d5 + 0.5);
        GlStateManager.rotate(180.0f - entity.rotationYaw, 0.0f, 1.0f, 0.0f);
        this.renderManager.renderEngine.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
        GlStateManager.pushMatrix();
        GlStateManager.scale(2.67, 2.67, 2.67);
        GlStateManager.translate(-0.69, -0.69, -0.815);
        if (iPerspectiveAwareModel == null) {
            blockrendererdispatcher.getBlockModelRenderer().renderModelBrightnessColor(model, 1.0f, 1.0f, 1.0f, 1.0f);
        } else {
            blockrendererdispatcher.getBlockModelRenderer().renderModelBrightnessColor(iPerspectiveAwareModel, 1.0f, 1.0f, 1.0f, 1.0f);
        }
        GlStateManager.popMatrix();
        GlStateManager.translate(0.0f, 0.0f, 0.4375f);
        this.renderItem(entity);
        GlStateManager.popMatrix();
    }

    private void renderItem(EntityPixelmonPainting itemFrame) {
        ItemStack itemstack = itemFrame.getDisplayedItem();
        if (itemstack != null) {
            EntityItem entityitem = new EntityItem(itemFrame.world, 0.0, 0.0, 0.0, itemstack);
            entityitem.getItem().setCount(1);
            entityitem.hoverStart = 0.0f;
            GlStateManager.pushMatrix();
            GlStateManager.disableLighting();
            GlStateManager.scale(1.6, 1.6, 1.6);
            GlStateManager.translate(-0.315, -0.315, 0.0);
            if (!this.itemRenderer.shouldRenderItemIn3D(entityitem.getItem())) {
                GlStateManager.rotate(180.0f, 0.0f, 1.0f, 0.0f);
            }
            GlStateManager.pushAttrib();
            RenderHelper.enableStandardItemLighting();
            this.itemRenderer.renderItem(entityitem.getItem(), ItemCameraTransforms.TransformType.FIXED);
            RenderHelper.disableStandardItemLighting();
            GlStateManager.popAttrib();
            GlStateManager.enableLighting();
            GlStateManager.popMatrix();
        }
    }

    @Override
    protected ResourceLocation getEntityTexture(EntityPixelmonPainting entity) {
        return null;
    }
}

