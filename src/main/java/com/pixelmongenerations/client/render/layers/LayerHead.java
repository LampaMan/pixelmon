/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.client.render.layers;

import com.google.common.collect.Lists;
import com.pixelmongenerations.client.assets.resource.MinecraftModelResource;
import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.client.models.IPixelmonModel;
import com.pixelmongenerations.common.cosmetic.CosmeticCategory;
import com.pixelmongenerations.common.cosmetic.CosmeticData;
import com.pixelmongenerations.common.cosmetic.CosmeticEntry;
import com.pixelmongenerations.common.cosmetic.LocalCosmeticCache;
import com.pixelmongenerations.common.cosmetic.PositionOperation;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(value=Side.CLIENT)
public class LayerHead
implements LayerRenderer<EntityPlayer> {
    private final ModelRenderer modelRenderer;
    private final RenderPlayer renderer;
    private final ResourceLocation locationRainbowBG;
    private final DynamicTexture rainbowBG;

    public LayerHead(RenderPlayer renderer, ModelRenderer modelRenderer) {
        this.modelRenderer = modelRenderer;
        this.renderer = renderer;
        this.rainbowBG = new DynamicTexture(1, 1);
        this.rainbowBG.getTextureData()[0] = -1;
        this.rainbowBG.updateDynamicTexture();
        this.locationRainbowBG = Minecraft.getMinecraft().getTextureManager().getDynamicTextureLocation("sashBG1", this.rainbowBG);
    }

    @Override
    public void doRenderLayer(EntityPlayer player, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
        CosmeticData data = LocalCosmeticCache.getCosmeticData(player.getUniqueID());
        if (data == null) {
            return;
        }
        ArrayList<CosmeticEntry> entries = Lists.newArrayList(data.getCosmeticInSlot(CosmeticCategory.Head), data.getCosmeticInSlot(CosmeticCategory.Face));
        for (CosmeticEntry entry : entries) {
            if (entry == null || entry.getModel() == null) continue;
            GlStateManager.pushMatrix();
            Object model = entry.getModel();
            if (player.isSneaking()) {
                GlStateManager.translate(0.0f, -0.75f, 0.0f);
            }
            this.modelRenderer.postRender(1.0f);
            GlStateManager.translate(0.0f, 1.5f, 0.0f);
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            GlStateManager.enableRescaleNormal();
            String textureShort = entry.getModelName() + "-" + entry.getTextureName();
            String texture = "pixelmon:textures/cosmetics/" + entry.getCategory().name().toLowerCase() + "/" + textureShort + ".png";
            ArrayList cosmetics = Lists.newArrayList((Object[])new String[]{"thickglasses", "sunglasses", "letsgocap", "flowers", "cowboyhat"});
            if (cosmetics.contains(entry.getModelName())) {
                GlStateManager.translate(0.0, -1.47, 0.0);
            } else if (entry.getModelName().equals("aviatorshades")) {
                GlStateManager.translate(0.0, -1.46, -0.65);
            }
            PositionOperation[] operations = entry.getOperations();
            if (operations != null) {
                block6: for (PositionOperation op : operations) {
                    switch (op.getType()) {
                        case Rotate: {
                            GlStateManager.rotate(op.getAngle(), op.getX(), op.getY(), op.getZ());
                            continue block6;
                        }
                        case Scale: {
                            GlStateManager.scale(op.getX(), op.getY(), op.getZ());
                            continue block6;
                        }
                        case Translate: {
                            GlStateManager.translate(op.getX(), op.getY(), op.getZ());
                            continue block6;
                        }
                    }
                }
            }
            TextureResource dynTexture = ClientProxy.TEXTURE_STORE.getObject(textureShort);
            if (dynTexture == null) dynTexture = ClientProxy.TEXTURE_STORE.getObject(entry.getTextureName());
            if (dynTexture != null) {
                if (model instanceof IPixelmonModel) {
                    dynTexture.bindTexture();
                    ((IPixelmonModel)model).renderAll();
                } else if (model instanceof MinecraftModelResource) {
                    dynTexture.bindTexture();
                    Minecraft minecraft = Minecraft.getMinecraft();
                    BlockRendererDispatcher blockRendererDispatcher = minecraft.getBlockRendererDispatcher();
                    IBakedModel bakedModel = ((MinecraftModelResource)model).getModel();
                    if (bakedModel != null) {
                        blockRendererDispatcher.getBlockModelRenderer().renderModelBrightnessColor(bakedModel, 1.0f, 1.0f, 1.0f, 1.0f);
                    }
                }
            } else {
                ResourceLocation resourceLocation = new ResourceLocation(texture);
                if (Pixelmon.PROXY.resourceLocationExists(resourceLocation)) {
                    if (model instanceof IPixelmonModel) {
                        this.renderer.bindTexture(resourceLocation);
                        ((IPixelmonModel)model).renderAll();
                    } else if (model instanceof MinecraftModelResource) {
                        this.renderer.bindTexture(resourceLocation);
                        Minecraft minecraft = Minecraft.getMinecraft();
                        BlockRendererDispatcher blockRendererDispatcher = minecraft.getBlockRendererDispatcher();
                        IBakedModel bakedModel = ((MinecraftModelResource)model).getModel();
                        if (bakedModel != null) {
                            blockRendererDispatcher.getBlockModelRenderer().renderModelBrightnessColor(bakedModel, 1.0f, 1.0f, 1.0f, 1.0f);
                        }
                    }
                }
            }
            GlStateManager.popMatrix();
        }
    }

    @Override
    public boolean shouldCombineTextures() {
        return true;
    }
}

