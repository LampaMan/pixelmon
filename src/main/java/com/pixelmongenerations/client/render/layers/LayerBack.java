/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 *  com.google.common.collect.Maps
 */
package com.pixelmongenerations.client.render.layers;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.pixelmongenerations.client.assets.resource.MinecraftModelResource;
import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.client.models.IPixelmonModel;
import com.pixelmongenerations.client.models.pokeballs.ModelPokeballs;
import com.pixelmongenerations.client.render.tileEntities.RenderTileEggIncubator;
import com.pixelmongenerations.common.cosmetic.CosmeticCategory;
import com.pixelmongenerations.common.cosmetic.CosmeticData;
import com.pixelmongenerations.common.cosmetic.CosmeticEntry;
import com.pixelmongenerations.common.cosmetic.LocalCosmeticCache;
import com.pixelmongenerations.common.cosmetic.PositionOperation;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumEggType;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.event.EntityPlayerExtension;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.util.ArrayList;
import java.util.HashMap;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(value=Side.CLIENT)
public class LayerBack
implements LayerRenderer<EntityPlayer> {
    private final ModelRenderer modelRenderer;
    private final RenderPlayer renderer;
    private final ResourceLocation developerSash = new ResourceLocation("pixelmon:textures/playeritems/developer.png");
    private final HashMap<EnumPokeball, ModelPokeballs> pokeballModels = Maps.newHashMap();
    private final ResourceLocation locationSashBG;
    private final DynamicTexture sashBG;

    public LayerBack(RenderPlayer renderer, ModelRenderer modelRenderer) {
        this.modelRenderer = modelRenderer;
        this.renderer = renderer;
        this.sashBG = new DynamicTexture(1, 1);
        this.sashBG.getTextureData()[0] = -1;
        this.sashBG.updateDynamicTexture();
        this.locationSashBG = Minecraft.getMinecraft().getTextureManager().getDynamicTextureLocation("sashBG2", this.sashBG);
    }

    @Override
    public void doRenderLayer(EntityPlayer player, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
        CosmeticData data = LocalCosmeticCache.getCosmeticData(player.getUniqueID());
        if (data == null) {
            return;
        }
        ArrayList<CosmeticEntry> entries = Lists.newArrayList(data.getCosmeticInSlot(CosmeticCategory.Back), data.getCosmeticInSlot(CosmeticCategory.Body), data.getCosmeticInSlot(CosmeticCategory.Shirt), data.getCosmeticInSlot(CosmeticCategory.Neck));
        if (entries.isEmpty()) {
            return;
        }
        scale = 1.0f;
        if (player.isSneaking()) {
            ModelRenderer bodyRender = this.renderer.getMainModel().bipedBodyWear;
            bodyRender.rotationPointY = (float)((double)bodyRender.rotationPointY + (double)scale * 0.2);
        }
        for (CosmeticEntry entry : entries) {
            if (entry == null || entry.getModel() == null) continue;
            GlStateManager.pushMatrix();
            Object model = entry.getModel();
            this.renderer.getMainModel().bipedBodyWear.postRender(scale);
            GlStateManager.translate(0.0, 1.5, 0.0);
            ArrayList cosmetics = Lists.newArrayList("sportybackpack", "scoutbackpack", "leatherbackpack");
            if (cosmetics.contains(entry.getModelName())) {
                GlStateManager.translate(0.0, -1.47, 0.0);
            }
            if (player.inventory.armorInventory.get(2) != ItemStack.EMPTY) {
                GlStateManager.scale(scale, scale, scale + 0.31f);
            } else {
                GlStateManager.scale(scale, scale, scale);
            }
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            GlStateManager.enableRescaleNormal();
            String textureShort = entry.getModelName() + "-" + entry.getTextureName();
            String texture = "pixelmon:textures/cosmetics/" + entry.getCategory().name().toLowerCase() + "/" + textureShort + ".png";
            PositionOperation[] operations = entry.getOperations();
            if (operations != null) {
                block6: for (PositionOperation op : operations) {
                    switch (op.getType()) {
                        case Rotate: {
                            GlStateManager.rotate(op.getAngle(), op.getX(), op.getY(), op.getZ());
                            continue block6;
                        }
                        case Scale: {
                            GlStateManager.scale(op.getX(), op.getY(), op.getZ());
                            continue block6;
                        }
                        case Translate: {
                            GlStateManager.translate(op.getX(), op.getY(), op.getZ());
                            continue block6;
                        }
                    }
                }
            }
            TextureResource dynTexture = ClientProxy.TEXTURE_STORE.getObject(textureShort);
            if (dynTexture == null) dynTexture = ClientProxy.TEXTURE_STORE.getObject(entry.getTextureName());
            if (dynTexture != null) {
                if (model instanceof IPixelmonModel) {
                    dynTexture.bindTexture();
                    ((IPixelmonModel)model).renderAll();
                } else if (model instanceof MinecraftModelResource) {
                    dynTexture.bindTexture();
                    Minecraft minecraft = Minecraft.getMinecraft();
                    BlockRendererDispatcher blockRendererDispatcher = minecraft.getBlockRendererDispatcher();
                    IBakedModel bakedModel = ((MinecraftModelResource)model).getModel();
                    if (bakedModel != null) {
                        blockRendererDispatcher.getBlockModelRenderer().renderModelBrightnessColor(bakedModel, 1.0f, 1.0f, 1.0f, 1.0f);
                    }
                }
            } else {
                ResourceLocation resourceLocation = new ResourceLocation(texture);
                if (Pixelmon.PROXY.resourceLocationExists(resourceLocation)) {
                    if (model instanceof IPixelmonModel) {
                        this.renderer.bindTexture(resourceLocation);
                        ((IPixelmonModel)model).renderAll();
                    } else if (model instanceof MinecraftModelResource) {
                        this.renderer.bindTexture(resourceLocation);
                        Minecraft minecraft = Minecraft.getMinecraft();
                        BlockRendererDispatcher blockRendererDispatcher = minecraft.getBlockRendererDispatcher();
                        IBakedModel bakedModel = ((MinecraftModelResource)model).getModel();
                        if (bakedModel != null) {
                            blockRendererDispatcher.getBlockModelRenderer().renderModelBrightnessColor(bakedModel, 1.0f, 1.0f, 1.0f, 1.0f);
                        }
                    }
                }
            }
            if (entry.getModelName().equals("sash")) {
                if (entry.getTextureName().contains("egg")) {
                    GlStateManager.rotate(180.0f, 0.0f, -1.0f, 0.0f);
                    GlStateManager.rotate(180.0f, 0.0f, 0.0f, -1.0f);
                    GlStateManager.translate(-0.175, 1.55, 0.15);
                    for (int egg : EntityPlayerExtension.getPlayerEggs(player)) {
                        if (egg < 0) continue;
                        EnumEggType eggType = EnumEggType.values()[egg];
                        this.renderer.bindTexture(eggType.getModelTexture());
                        GlStateManager.translate(0.033, -0.06, 0.0);
                        RenderTileEggIncubator.incubatorEgg.render(0.15f);
                    }
                } else {
                    GlStateManager.rotate(180.0f, 0.0f, 1.0f, 0.0f);
                    GlStateManager.rotate(180.0f, 0.0f, 0.0f, 1.0f);
                    GlStateManager.translate(-0.175, 1.44, 0.15);
                    for (int ball : EntityPlayerExtension.getPlayerPokeballs(player)) {
                        if (ball < 0) continue;
                        EnumPokeball pokeball = EnumPokeball.getFromIndex(ball);
                        if (this.pokeballModels.get(pokeball) == null) {
                            this.pokeballModels.put(pokeball, pokeball.getModel());
                        }
                        this.renderer.bindTexture(new ResourceLocation(pokeball.getTextureDirectory() + pokeball.getTexture()));
                        GlStateManager.translate(0.0275, -0.05, 0.0);
                        this.pokeballModels.get(pokeball).render(null, 6.0E-4f);
                    }
                }
            }
            GlStateManager.popMatrix();
        }
    }

    @Override
    public boolean shouldCombineTextures() {
        return true;
    }
}

