/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.layers;

import com.pixelmongenerations.client.models.IPixelmonModel;
import com.pixelmongenerations.core.enums.EnumCustomModel;
import com.pixelmongenerations.core.enums.EnumDynamaxItem;
import com.pixelmongenerations.core.enums.EnumMegaItem;
import com.pixelmongenerations.core.event.EntityPlayerExtension;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.ResourceLocation;

public class LayerMegaBracelet
implements LayerRenderer<EntityPlayer> {
    private final ResourceLocation megaBraceletTexture = new ResourceLocation("pixelmon:textures/playeritems/megabraceletoras.png");
    private final ResourceLocation dynamaxBandTexture = new ResourceLocation("pixelmon:textures/playeritems/dynamaxband.png");
    private final IPixelmonModel braceletModel = (IPixelmonModel)((Object)EnumCustomModel.MegaBraceletORAS.getModel());
    private final IPixelmonModel stoneModel = (IPixelmonModel)((Object)EnumCustomModel.MegaBraceletORASStone.getModel());
    private final IPixelmonModel dynamaxBandModel = (IPixelmonModel)((Object)EnumCustomModel.DynamaxBand.getModel());
    private final RenderPlayer renderer;

    public LayerMegaBracelet(RenderPlayer renderer) {
        this.renderer = renderer;
    }

    @Override
    public void doRenderLayer(EntityPlayer player, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
        if (player.isInvisible()) {
            return;
        }
        if (EntityPlayerExtension.getPlayerDynamaxItem(player) == EnumDynamaxItem.DynamaxBand) {
            GlStateManager.pushMatrix();
            if (player.isSneaking()) {
                GlStateManager.translate(0.0f, 0.2f, 0.0f);
            }
            scale = this.renderer.getMainModel().bipedLeftArm.cubeList.get((int)0).posX2 == 2.0f ? 0.17f : 0.19f;
            this.renderer.getMainModel().postRenderArm(0.0625f, EnumHandSide.LEFT);
            GlStateManager.translate(0.0f, 0.42f, 0.0f);
            GlStateManager.scale((double)scale - 0.02, (double)scale - 0.04, (double)scale);
            GlStateManager.translate(0.37f, 0.0f, 0.0f);
            GlStateManager.disableLighting();
            this.renderer.bindTexture(this.dynamaxBandTexture);
            GlStateManager.enableCull();
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            this.dynamaxBandModel.renderAll();
            GlStateManager.enableBlend();
            GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
            GlStateManager.disableBlend();
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            GlStateManager.enableLighting();
            GlStateManager.popMatrix();
        }
        if (EntityPlayerExtension.getPlayerMegaItem(player) == EnumMegaItem.BraceletORAS) {
            GlStateManager.pushMatrix();
            if (player.isSneaking()) {
                GlStateManager.translate(0.0f, 0.2f, 0.0f);
            }
            scale = this.renderer.getMainModel().bipedLeftArm.cubeList.get((int)0).posX2 == 2.0f ? 0.17f : 0.19f;
            this.renderer.getMainModel().postRenderArm(0.0625f, EnumHandSide.LEFT);
            GlStateManager.translate(0.0f, 0.4f, 0.0f);
            GlStateManager.scale(scale, scale, scale);
            GlStateManager.translate(0.35f, 0.0f, 0.0f);
            this.renderer.bindTexture(this.megaBraceletTexture);
            GlStateManager.enableCull();
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            this.braceletModel.renderAll();
            GlStateManager.enableBlend();
            GlStateManager.disableLighting();
            GlStateManager.color(1.0f, 1.0f, 1.0f, 0.4f);
            GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
            this.stoneModel.renderAll();
            GlStateManager.disableBlend();
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            GlStateManager.popMatrix();
        }
    }

    @Override
    public boolean shouldCombineTextures() {
        return true;
    }
}

