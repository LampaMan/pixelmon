/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.client.render;

import com.pixelmongenerations.client.models.BikeModelSmd;
import com.pixelmongenerations.client.models.PixelmonModelRegistry;
import com.pixelmongenerations.common.entity.bikes.EntityBike;
import javax.annotation.Nullable;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(value=Side.CLIENT)
public class RenderBike
extends RenderLiving<EntityBike> {
    public RenderBike(RenderManager manager) {
        super(manager, (ModelBase)PixelmonModelRegistry.bikeModel.getModel(), 0.5f);
    }

    @Override
    public void doRender(EntityBike statue, double x, double y, double z, float yaw, float partialTicks) {
        GlStateManager.alphaFunc(516, 0.1f);
        this.renderBike(statue, x, y, z, yaw, partialTicks);
    }

    public void renderBike(EntityBike bike, double x, double y, double z, float yaw, float partialTicks) {
        this.mainModel = PixelmonModelRegistry.bikeModel.getModel();
        GlStateManager.pushMatrix();
        GlStateManager.disableCull();
        this.mainModel.swingProgress = this.getSwingProgress(bike, partialTicks);
        this.mainModel.isRiding = bike.isRiding();
        this.mainModel.isChild = bike.isChild();
        try {
            if (this.mainModel instanceof BikeModelSmd && ((BikeModelSmd)this.mainModel).theModel.hasAnimations()) {
                ((BikeModelSmd)this.mainModel).setupForRender(bike);
            }
            float f2 = this.interpolateRotation(bike.prevRenderYawOffset, bike.renderYawOffset, partialTicks);
            float f3 = this.interpolateRotation(bike.prevRotationYawHead, bike.rotationYawHead, partialTicks);
            float f4 = f3 - f2;
            float f9 = bike.prevRotationPitch + (bike.rotationPitch - bike.prevRotationPitch) * partialTicks;
            this.renderLivingAt(bike, x, y, z);
            float f5 = 0.0f;
            this.applyRotations(bike, f5, f2, partialTicks);
            GlStateManager.enableRescaleNormal();
            GlStateManager.scale(-0.3f, -0.3f, 0.3f);
            GlStateManager.translate(0.0, -1.5, 0.0);
            this.preRenderCallback(bike, partialTicks);
            float f7 = bike.prevLimbSwingAmount + (bike.limbSwingAmount - bike.prevLimbSwingAmount) * partialTicks;
            float f8 = bike.limbSwing - bike.limbSwingAmount * (1.0f - partialTicks);
            GlStateManager.enableAlpha();
            this.mainModel.setLivingAnimations(bike, f8, f7, partialTicks);
            this.mainModel.setRotationAngles(f8, f7, f5, f4, f9, 0.0625f, bike);
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            boolean flag = this.setDoRenderBrightness(bike, partialTicks);
            this.bindEntityTexture(bike);
            this.renderModel(bike, f8, f7, f5, 0.0f, 0.0f, 0.0625f);
            if (flag) {
                this.unsetBrightness();
            }
            GlStateManager.depthMask(true);
            GlStateManager.disableRescaleNormal();
        }
        catch (Exception exc) {
            exc.printStackTrace();
        }
        OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        GlStateManager.enableTexture2D();
        OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
        GlStateManager.enableCull();
        GlStateManager.popMatrix();
        this.renderName(bike, x, y, z);
    }

    @Override
    @Nullable
    protected ResourceLocation getEntityTexture(EntityBike entity) {
        return entity.getTexture();
    }
}

