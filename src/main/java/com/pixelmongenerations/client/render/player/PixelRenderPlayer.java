/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.player;

import com.pixelmongenerations.common.cosmetic.CosmeticCategory;
import com.pixelmongenerations.common.cosmetic.CosmeticData;
import com.pixelmongenerations.common.cosmetic.LocalCosmeticCache;
import com.pixelmongenerations.common.item.server.ServerItem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelPlayer;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.client.renderer.entity.layers.LayerCape;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EnumPlayerModelParts;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHandSide;

public class PixelRenderPlayer
extends RenderPlayer {
    public PixelRenderPlayer(RenderManager renderManager) {
        super(renderManager);
        this.layerRenderers.removeIf(layer -> layer instanceof LayerCape);
    }

    public PixelRenderPlayer(RenderManager renderManager, boolean useSmallArms) {
        super(renderManager, useSmallArms);
        this.layerRenderers.removeIf(layer -> layer instanceof LayerCape);
    }

    @Override
    public void doRender(AbstractClientPlayer abstractClientPlayer, double x, double y, double z, float entityYaw, float partialTicks) {
        boolean cameraIsPlayer = Minecraft.getMinecraft().getRenderViewEntity() instanceof EntityPlayer;
        if (!cameraIsPlayer || !abstractClientPlayer.isUser() || this.renderManager.renderViewEntity == abstractClientPlayer) {
            double modY = y;
            if (abstractClientPlayer.isSneaking() && !(abstractClientPlayer instanceof EntityPlayerSP)) {
                modY = y - 0.125;
            }
            Entity livingPlayer = this.renderManager.renderViewEntity;
            if (!cameraIsPlayer) {
                this.setBattleModelVisibilities(abstractClientPlayer);
                this.renderManager.renderViewEntity = abstractClientPlayer;
            } else {
                super.setModelVisibilities(abstractClientPlayer);
            }
            super.doRender(abstractClientPlayer, x, modY, z, entityYaw, partialTicks);
            if (!cameraIsPlayer) {
                this.renderManager.renderViewEntity = livingPlayer;
            }
        }
    }

    public void setModelVisibilities(AbstractClientPlayer clientPlayer) {
        ModelPlayer modelplayer = this.getMainModel();
        CosmeticData data = LocalCosmeticCache.getCosmeticData(clientPlayer.getUniqueID());
        if (data != null && data.getCosmeticInSlot(CosmeticCategory.Body) != null) {
            this.shadowSize = 0.0f;
            ItemStack itemstack = clientPlayer.getHeldItemMainhand();
            ItemStack itemstack1 = clientPlayer.getHeldItemOffhand();
            modelplayer.bipedBody.offsetX = 255.0f;
            modelplayer.bipedLeftArm.offsetX = 255.0f;
            modelplayer.bipedRightArm.offsetX = 255.0f;
            modelplayer.bipedLeftLeg.offsetX = 255.0f;
            modelplayer.bipedRightLeg.offsetX = 255.0f;
            modelplayer.bipedHead.offsetX = 255.0f;
            modelplayer.bipedHeadwear.offsetX = 255.0f;
            modelplayer.bipedBodyWear.offsetX = 255.0f;
            modelplayer.bipedLeftLegwear.offsetX = 255.0f;
            modelplayer.bipedRightLegwear.offsetX = 255.0f;
            modelplayer.bipedLeftArmwear.offsetX = 255.0f;
            modelplayer.bipedRightArmwear.offsetX = 255.0f;
            modelplayer.bipedHeadwear.showModel = clientPlayer.isWearing(EnumPlayerModelParts.HAT);
            modelplayer.bipedBodyWear.showModel = clientPlayer.isWearing(EnumPlayerModelParts.JACKET);
            modelplayer.bipedLeftLegwear.showModel = clientPlayer.isWearing(EnumPlayerModelParts.LEFT_PANTS_LEG);
            modelplayer.bipedRightLegwear.showModel = clientPlayer.isWearing(EnumPlayerModelParts.RIGHT_PANTS_LEG);
            modelplayer.bipedLeftArmwear.showModel = clientPlayer.isWearing(EnumPlayerModelParts.LEFT_SLEEVE);
            modelplayer.bipedRightArmwear.showModel = clientPlayer.isWearing(EnumPlayerModelParts.RIGHT_SLEEVE);
            modelplayer.isSneak = clientPlayer.isSneaking();
            ModelBiped.ArmPose modelbiped$armpose = ModelBiped.ArmPose.EMPTY;
            ModelBiped.ArmPose modelbiped$armpose1 = ModelBiped.ArmPose.EMPTY;
            if (!itemstack.isEmpty()) {
                modelbiped$armpose = ModelBiped.ArmPose.ITEM;
                if (clientPlayer.getItemInUseCount() > 0) {
                    EnumAction enumaction = itemstack.getItemUseAction();
                    if (enumaction == EnumAction.BLOCK) {
                        modelbiped$armpose = ModelBiped.ArmPose.BLOCK;
                    } else if (enumaction == EnumAction.BOW) {
                        modelbiped$armpose = ModelBiped.ArmPose.BOW_AND_ARROW;
                    }
                }
            }
            if (!itemstack1.isEmpty()) {
                modelbiped$armpose1 = ModelBiped.ArmPose.ITEM;
                if (clientPlayer.getItemInUseCount() > 0) {
                    EnumAction enumaction1 = itemstack1.getItemUseAction();
                    if (enumaction1 == EnumAction.BLOCK) {
                        modelbiped$armpose1 = ModelBiped.ArmPose.BLOCK;
                    } else if (enumaction1 == EnumAction.BOW) {
                        modelbiped$armpose1 = ModelBiped.ArmPose.BOW_AND_ARROW;
                    }
                }
            }
            if (clientPlayer.getPrimaryHand() == EnumHandSide.RIGHT) {
                modelplayer.rightArmPose = modelbiped$armpose;
                modelplayer.leftArmPose = modelbiped$armpose1;
            } else {
                modelplayer.rightArmPose = modelbiped$armpose1;
                modelplayer.leftArmPose = modelbiped$armpose;
            }
        } else {
            this.shadowSize = 0.5f;
            modelplayer.bipedBody.offsetX = 0.0f;
            modelplayer.bipedLeftArm.offsetX = 0.0f;
            modelplayer.bipedRightArm.offsetX = 0.0f;
            modelplayer.bipedLeftLeg.offsetX = 0.0f;
            modelplayer.bipedRightLeg.offsetX = 0.0f;
            modelplayer.bipedHead.offsetX = 0.0f;
            modelplayer.bipedHeadwear.offsetX = 0.0f;
            modelplayer.bipedBodyWear.offsetX = 0.0f;
            modelplayer.bipedLeftLegwear.offsetX = 0.0f;
            modelplayer.bipedRightLegwear.offsetX = 0.0f;
            modelplayer.bipedLeftArmwear.offsetX = 0.0f;
            modelplayer.bipedRightArmwear.offsetX = 0.0f;
            if (clientPlayer.isSpectator()) {
                modelplayer.setVisible(false);
                modelplayer.bipedHead.showModel = true;
                modelplayer.bipedHeadwear.showModel = true;
            } else {
                ItemStack itemstack = clientPlayer.getHeldItemMainhand();
                ItemStack itemstack1 = clientPlayer.getHeldItemOffhand();
                modelplayer.setVisible(true);
                modelplayer.bipedHeadwear.showModel = clientPlayer.isWearing(EnumPlayerModelParts.HAT);
                modelplayer.bipedBodyWear.showModel = clientPlayer.isWearing(EnumPlayerModelParts.JACKET);
                modelplayer.bipedLeftLegwear.showModel = clientPlayer.isWearing(EnumPlayerModelParts.LEFT_PANTS_LEG);
                modelplayer.bipedRightLegwear.showModel = clientPlayer.isWearing(EnumPlayerModelParts.RIGHT_PANTS_LEG);
                modelplayer.bipedLeftArmwear.showModel = clientPlayer.isWearing(EnumPlayerModelParts.LEFT_SLEEVE);
                modelplayer.bipedRightArmwear.showModel = clientPlayer.isWearing(EnumPlayerModelParts.RIGHT_SLEEVE);
                modelplayer.isSneak = clientPlayer.isSneaking();
                ModelBiped.ArmPose modelbiped$armpose = ModelBiped.ArmPose.EMPTY;
                ModelBiped.ArmPose modelbiped$armpose1 = ModelBiped.ArmPose.EMPTY;
                if (!itemstack.isEmpty()) {
                    EnumAction enumaction;
                    modelbiped$armpose = ModelBiped.ArmPose.ITEM;
                    if (clientPlayer.getItemInUseCount() > 0) {
                        enumaction = itemstack.getItemUseAction();
                        if (enumaction == EnumAction.BLOCK) {
                            modelbiped$armpose = ModelBiped.ArmPose.BLOCK;
                        } else if (enumaction == EnumAction.BOW) {
                            modelbiped$armpose = ModelBiped.ArmPose.BOW_AND_ARROW;
                        }
                    } else if (itemstack.getItem() instanceof ServerItem) {
                        enumaction = itemstack.getItemUseAction();
                        if (enumaction == EnumAction.BLOCK) {
                            modelbiped$armpose = ModelBiped.ArmPose.BLOCK;
                        } else if (enumaction == EnumAction.BOW) {
                            modelbiped$armpose = ModelBiped.ArmPose.BOW_AND_ARROW;
                        }
                    }
                }
                if (!itemstack1.isEmpty()) {
                    modelbiped$armpose1 = ModelBiped.ArmPose.ITEM;
                    if (clientPlayer.getItemInUseCount() > 0) {
                        EnumAction enumaction1 = itemstack1.getItemUseAction();
                        if (enumaction1 == EnumAction.BLOCK) {
                            modelbiped$armpose1 = ModelBiped.ArmPose.BLOCK;
                        } else if (enumaction1 == EnumAction.BOW) {
                            modelbiped$armpose1 = ModelBiped.ArmPose.BOW_AND_ARROW;
                        }
                    }
                }
                if (clientPlayer.getPrimaryHand() == EnumHandSide.RIGHT) {
                    modelplayer.rightArmPose = modelbiped$armpose;
                    modelplayer.leftArmPose = modelbiped$armpose1;
                } else {
                    modelplayer.rightArmPose = modelbiped$armpose1;
                    modelplayer.leftArmPose = modelbiped$armpose;
                }
            }
        }
    }

    public void setBattleModelVisibilities(AbstractClientPlayer clientPlayer) {
        ModelPlayer modelplayer = this.getMainModel();
        if (clientPlayer.isSpectator()) {
            modelplayer.setVisible(false);
            modelplayer.bipedHead.showModel = true;
            modelplayer.bipedHeadwear.showModel = true;
        } else {
            modelplayer.setVisible(false);
            modelplayer.bipedHeadwear.showModel = clientPlayer.isWearing(EnumPlayerModelParts.HAT);
            modelplayer.bipedBodyWear.showModel = clientPlayer.isWearing(EnumPlayerModelParts.JACKET);
            modelplayer.bipedLeftLegwear.showModel = clientPlayer.isWearing(EnumPlayerModelParts.LEFT_PANTS_LEG);
            modelplayer.bipedRightLegwear.showModel = clientPlayer.isWearing(EnumPlayerModelParts.RIGHT_PANTS_LEG);
            modelplayer.bipedLeftArmwear.showModel = clientPlayer.isWearing(EnumPlayerModelParts.LEFT_SLEEVE);
            modelplayer.bipedRightArmwear.showModel = clientPlayer.isWearing(EnumPlayerModelParts.RIGHT_SLEEVE);
            modelplayer.isSneak = clientPlayer.isSneaking();
        }
    }
}

