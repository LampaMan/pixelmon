/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableMap
 *  com.google.common.collect.Lists
 *  javax.vecmath.Matrix4f
 *  org.apache.commons.lang3.tuple.Pair
 */
package com.pixelmongenerations.client.render.item;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.pixelmongenerations.core.config.PixelmonItems;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.vecmath.Matrix4f;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.block.model.ItemOverrideList;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.client.model.PerspectiveMapWrapper;
import net.minecraftforge.common.model.TRSRTransformation;
import org.apache.commons.lang3.tuple.Pair;

public class ItemRendererShrineOrb
extends PerspectiveMapWrapper {
    public static ModelResourceLocation uno = new ModelResourceLocation("pixelmon:uno_orb", "inventory");
    public static ModelResourceLocation dos = new ModelResourceLocation("pixelmon:dos_orb", "inventory");
    public static ModelResourceLocation tres = new ModelResourceLocation("pixelmon:tres_orb", "inventory");
    IBakedModel model;
    private float percent;
    private float height;
    Item orbType;

    public ItemRendererShrineOrb(IBakedModel model) {
        super(model, TRSRTransformation.identity());
        this.model = model;
    }

    @Override
    public boolean isBuiltInRenderer() {
        return false;
    }

    @Override
    public TextureAtlasSprite getParticleTexture() {
        return this.model.getParticleTexture();
    }

    @Override
    public boolean isGui3d() {
        return this.model.isGui3d();
    }

    @Override
    public ItemCameraTransforms getItemCameraTransforms() {
        return this.model.getItemCameraTransforms();
    }

    @Override
    public ItemOverrideList getOverrides() {
        return new OverrideList();
    }

    @Override
    public boolean isAmbientOcclusion() {
        return this.model.isAmbientOcclusion();
    }

    @Override
    public List<BakedQuad> getQuads(IBlockState state, EnumFacing side, long rand) {
        ArrayList<BakedQuad> quadList = new ArrayList<BakedQuad>(6);
        TextureAtlasSprite orbTexture = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite("pixelmon:items/back");
        this.addQuad(quadList, orbTexture, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 16.0f, 16.0f);
        if (this.orbType == PixelmonItems.unoOrb) {
            orbTexture = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite("pixelmon:items/unoorb");
        } else if (this.orbType == PixelmonItems.dosOrb) {
            orbTexture = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite("pixelmon:items/dosorb");
        } else if (this.orbType == PixelmonItems.tresOrb) {
            orbTexture = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite("pixelmon:items/tresorb");
        }
        this.addQuad(quadList, orbTexture, 1.0f, this.height, 0.001f, 0.0f, 16.0f - this.percent, 16.0f, 16.0f);
        orbTexture = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite("pixelmon:items/front");
        this.addQuad(quadList, orbTexture, 1.0f, 1.0f, 0.002f, 0.0f, 0.0f, 16.0f, 16.0f);
        return quadList;
    }

    private void addQuad(List<BakedQuad> quadList, TextureAtlasSprite texture, float xEnd, float yEnd, float zEnd, float uStart, float vStart, float uEnd, float vEnd) {
        int x = Float.floatToRawIntBits(xEnd);
        int y = Float.floatToRawIntBits(yEnd);
        int z = Float.floatToRawIntBits(zEnd);
        int us = Float.floatToRawIntBits(texture.getInterpolatedU(uStart));
        int ue = Float.floatToRawIntBits(texture.getInterpolatedU(uEnd));
        int vs = Float.floatToIntBits(texture.getInterpolatedV(vStart));
        int ve = Float.floatToIntBits(texture.getInterpolatedV(vEnd));
        int white = Color.white.getRGB();
        BakedQuad orbQuad = new BakedQuad(new int[]{x, 0, z, white, ue, ve, 0, x, y, z, white, ue, vs, 0, 0, y, z, white, us, vs, 0, 0, 0, z, white, us, ve, 0}, 0, EnumFacing.SOUTH, texture, false, Tessellator.getInstance().getBuffer().getVertexFormat());
        quadList.add(orbQuad);
        int zn = Float.floatToRawIntBits(-zEnd);
        orbQuad = new BakedQuad(new int[]{0, 0, zn, white, ue, ve, 0, 0, y, zn, white, ue, vs, 0, x, y, zn, white, us, vs, 0, x, 0, zn, white, us, ve, 0}, 0, EnumFacing.NORTH, texture, false, Tessellator.getInstance().getBuffer().getVertexFormat());
        quadList.add(orbQuad);
    }

    @Override
    public Pair<? extends IBakedModel, Matrix4f> handlePerspective(ItemCameraTransforms.TransformType cameraTransformType) {
        ImmutableMap<ItemCameraTransforms.TransformType, TRSRTransformation> map = PerspectiveMapWrapper.getTransforms(TRSRTransformation.identity());
        return ItemRendererShrineOrb.handlePerspective((IBakedModel)this, map, cameraTransformType);
    }

    private static class OverrideList
    extends ItemOverrideList {
        public OverrideList() {
            super(Lists.newArrayList());
        }

        @Override
        public IBakedModel handleItemState(IBakedModel originalModel, ItemStack stack, World world, EntityLivingBase entity) {
            if (originalModel instanceof ItemRendererShrineOrb) {
                ItemRendererShrineOrb orb = (ItemRendererShrineOrb)originalModel;
                int pokemonKilled = stack.getItemDamage();
                orb.height = (float)pokemonKilled / 375.0f;
                orb.percent = orb.height * 16.0f;
                orb.orbType = stack.getItem();
            }
            return originalModel;
        }
    }
}

