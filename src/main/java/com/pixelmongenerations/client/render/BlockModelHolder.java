/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render;

import com.pixelmongenerations.client.models.ModelHolder;
import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import java.lang.reflect.InvocationTargetException;
import net.minecraft.client.model.ModelBase;
import net.minecraft.util.ResourceLocation;

public class BlockModelHolder<M extends ModelBase>
extends ModelHolder<M> {
    private Class<M> type;
    private ResourceLocation resource;
    private Object[] parameters;

    public BlockModelHolder(Class<M> clazz, ResourceLocation resource) {
        this.type = clazz;
        this.resource = resource;
    }

    public BlockModelHolder(Class<M> clazz, Object[] parameters) {
        this(clazz, (ResourceLocation)null);
        this.parameters = parameters;
    }

    public BlockModelHolder(Class<M> clazz) {
        this(clazz, (ResourceLocation)null);
    }

    public BlockModelHolder(ResourceLocation model) {
        this((Class<M>) GenericSmdModel.class, model);
    }

    public BlockModelHolder(String modelPath) {
        this(new ResourceLocation("pixelmon", "models/" + modelPath));
    }

    public void render() {
        ((ModelBase)this.getModel()).render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, 1.0f);
    }

    public void render(float scale) {
        ((ModelBase)this.getModel()).render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, scale);
    }

    @Override
    protected M loadModel() {
        if (this.type == GenericSmdModel.class) {
            return (M)new GenericSmdModel(this.resource, false);
        }
        try {
            if (this.parameters != null) {
                return (M)((ModelBase)this.type.getConstructors()[0].newInstance(this.parameters));
            }
            return (M)((ModelBase)this.type.getConstructor(new Class[0]).newInstance(new Object[0]));
        }
        catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }
}

