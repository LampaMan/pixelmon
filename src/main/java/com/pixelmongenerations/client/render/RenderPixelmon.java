/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.render;

import com.pixelmongenerations.api.HexColor;
import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.client.assets.resource.ValveModelResource;
import com.pixelmongenerations.client.models.PixelmonModelBase;
import com.pixelmongenerations.client.models.PixelmonModelRegistry;
import com.pixelmongenerations.client.models.PixelmonModelSmd;
import com.pixelmongenerations.common.cosmetic.PositionOperation;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumTextures;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.data.asset.entry.ValveModelAsset;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.packetHandlers.evolution.EvolutionStage;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.awt.Color;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.opengl.GL11;

public class RenderPixelmon
extends RenderLiving<EntityPixelmon> {
    private int defaultNameRenderDistance = 8;
    private int defaultBossNameRenderDistanceExtension = 8;
    private int configNameRenderMultiplier = Math.max(1, Math.min(PixelmonConfig.nameplateRangeModifier, 3));
    private int nameRenderDistanceNormal = this.defaultNameRenderDistance * this.configNameRenderMultiplier;
    private int nameRenderDistanceBoss = this.nameRenderDistanceNormal + this.defaultBossNameRenderDistanceExtension;
    private float distance = 0.0f;
    String lvlTag;
    String bossTag;
    private static float lightmapLastX;
    private static float lightmapLastY;
    private static boolean optifineBreak;
    private static boolean glowing;

    public RenderPixelmon(RenderManager manager) {
        super(manager, null, 0.5f);
    }

    @Override
    public void doRender(EntityPixelmon pixelmon, double d, double d1, double d2, float f, float f1) {
        if (pixelmon.stopRender) {
            return;
        }
        if (pixelmon.getPokemonName().equals("")) {
            return;
        }
        this.distance = pixelmon.getDistance(this.renderManager.renderViewEntity);
        GlStateManager.alphaFunc(516, 0.1f);
        if (!pixelmon.isInitialised) {
            pixelmon.init(pixelmon.getPokemonName());
        }
        if (pixelmon.getModel() != null) {
            this.renderPixelmon(pixelmon, d, d1, d2, f, f1, false);
        }
    }

    public void renderPixelmon(EntityPixelmon pixelmon, double x, double y, double z, float entityYaw, float partialTicks, boolean fromPokedex) {
        GlStateManager.pushMatrix();
        this.mainModel = pixelmon.getModel();
        if (this.mainModel == null || this.mainModel instanceof PixelmonModelSmd && ((PixelmonModelSmd)this.mainModel).theModel == null) {
            GlStateManager.popMatrix();
            return;
        }
        this.mainModel.swingProgress = this.getSwingProgress(pixelmon, partialTicks);
        this.mainModel.isRiding = pixelmon.isRiding();
        this.mainModel.isChild = pixelmon.isChild();
        if (this.mainModel instanceof PixelmonModelSmd && ((PixelmonModelSmd)this.mainModel).theModel.hasAnimations()) {
            ((PixelmonModelSmd)this.mainModel).setupForRender(pixelmon);
        }
        try {
            Color color;
            PositionOperation[] operations;
            ValveModelResource modelInstance;
            float f2 = this.interpolateRotation(pixelmon.prevRenderYawOffset, pixelmon.renderYawOffset, partialTicks);
            float f3 = this.interpolateRotation(pixelmon.prevRotationYawHead, pixelmon.rotationYawHead, partialTicks);
            float f4 = f3 - f2;
            float f9 = pixelmon.prevRotationPitch + (pixelmon.rotationPitch - pixelmon.prevRotationPitch) * partialTicks;
            this.renderLivingAt(pixelmon, x, y, z);
            float f5 = this.handleRotationFloat(pixelmon, partialTicks);
            this.applyRotations(pixelmon, f5, f2, partialTicks);
            GlStateManager.enableRescaleNormal();
            GlStateManager.enableLighting();
            GlStateManager.scale(-1.0f, -1.0f, 1.0f);
            this.renderEvolutionLightBeams(pixelmon);
            this.preRenderCallback(pixelmon, partialTicks);
            GlStateManager.translate(0.0f, -1.5078125f, 0.0f);
            float f7 = pixelmon.prevLimbSwingAmount + (pixelmon.limbSwingAmount - pixelmon.prevLimbSwingAmount) * partialTicks;
            float f8 = pixelmon.limbSwing - pixelmon.limbSwingAmount * (1.0f - partialTicks);
            if (pixelmon.isChild()) {
                f8 *= 3.0f;
            }
            if (f7 > 1.0f) {
                f7 = 1.0f;
            }
            GlStateManager.enableAlpha();
            this.mainModel.setLivingAnimations(pixelmon, f8, f7, partialTicks);
            this.mainModel.setRotationAngles(f8, f7, f5, f4, f9, 0.0625f, pixelmon);
            if (!(this.mainModel instanceof PixelmonModelSmd)) {
                GlStateManager.disableNormalize();
                GlStateManager.shadeModel(7425);
            }
            if ((modelInstance = ClientProxy.VALVE_MODEL_STORE.getObject(pixelmon.getCustomTexture())) != null && modelInstance.entry != null && (operations = ((ValveModelAsset)modelInstance.entry).operations) != null) {
                block19: for (PositionOperation op : operations) {
                    switch (op.getType()) {
                        case Rotate: {
                            GlStateManager.rotate(op.getAngle(), op.getX(), op.getY(), op.getZ());
                            continue block19;
                        }
                        case Scale: {
                            GlStateManager.scale(op.getX(), op.getY(), op.getZ());
                            continue block19;
                        }
                        case Translate: {
                            GlStateManager.translate(op.getX(), op.getY(), op.getZ());
                            continue block19;
                        }
                    }
                }
            }
            boolean flag = this.setDoRenderBrightness(pixelmon, partialTicks);
            if (pixelmon.getIsRed()) {
                GlStateManager.color(0.0f, 0.0f, 0.0f, 1.0f);
                this.renderModel(pixelmon, f8, f7, f5, f4, f9, 0.0625f);
            } else if (pixelmon.getBossMode().isBossPokemon() && !pixelmon.baseStats.pokemon.hasMega()) {
                if (pixelmon.hasColorStrobe()) {
                    this.colorStrobePokemon(pixelmon, 1.0f);
                } else if (pixelmon.hasColorTint()) {
                    try {
                        color = Color.decode(pixelmon.getColorTint());
                        this.colorTint((float)color.getRed() / 100.0f, (float)color.getGreen() / 100.0f, (float)color.getBlue() / 100.0f, 1.0f);
                    }
                    catch (NumberFormatException e) {
                        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
                    }
                } else {
                    GlStateManager.color(pixelmon.getBossMode().r, pixelmon.getBossMode().g, pixelmon.getBossMode().b, 1.0f);
                }
                this.renderModel(pixelmon, f8, f7, f5, f4, f9, 0.0625f);
            } else if (pixelmon.evoStage != null) {
                this.renderModel(pixelmon, f8, f7, f5, f4, f9, 0.0625f);
                GlStateManager.enableBlend();
                GlStateManager.disableTexture2D();
                if (pixelmon.hasColorStrobe()) {
                    this.colorStrobePokemon(pixelmon, (float)pixelmon.fadeCount / 20.0f);
                } else if (pixelmon.hasColorTint()) {
                    try {
                        color = Color.decode(pixelmon.getColorTint());
                        this.colorTint((float)color.getRed() / 100.0f, (float)color.getGreen() / 100.0f, (float)color.getBlue() / 100.0f, (float)pixelmon.fadeCount / 20.0f);
                    }
                    catch (NumberFormatException e) {
                        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
                    }
                } else {
                    GlStateManager.color(1.0f, 1.0f, 1.0f, (float)pixelmon.fadeCount / 20.0f);
                }
                this.renderModel(pixelmon, f8, f7, f5, f4, f9, 0.0625f);
                GlStateManager.enableTexture2D();
            } else {
                if (pixelmon.hasColorStrobe()) {
                    this.colorStrobePokemon(pixelmon, 1.0f);
                } else if (pixelmon.hasColorTint()) {
                    try {
                        color = Color.decode(pixelmon.getColorTint());
                        this.colorTint((float)color.getRed() / 100.0f, (float)color.getGreen() / 100.0f, (float)color.getBlue() / 100.0f, 1.0f);
                    }
                    catch (NumberFormatException e) {
                        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
                    }
                } else {
                    GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
                }
                this.renderModel(pixelmon, f8, f7, f5, f4, f9, 0.0625f);
            }
            ResourceLocation resourcelocation = this.getEntityTexture(pixelmon);
            if (resourcelocation != null) {
                if (pixelmon.getSpecialTextureEnum().hasGlow()) {
                    try {
                        RenderPixelmon.glowOn();
                        if (pixelmon.getSpecialTextureEnum().hasGlowRainbow()) {
                            this.colorStrobePokemon(pixelmon, 1.0f);
                        }
                        this.renderModel(pixelmon, f8, f7, f5, f4, f9, 0.0625f);
                    }
                    finally {
                        RenderPixelmon.glowOff();
                    }
                }
                String[] splits = resourcelocation.getPath().split("/");
                String textureName = splits[splits.length - 1].replaceAll(".png", "");
                TextureResource dynTexture = ClientProxy.TEXTURE_STORE.getObject(textureName);
                if (dynTexture != null && dynTexture.hasGlow()) {
                    try {
                        RenderPixelmon.glowOn();
                        if (dynTexture.hasGlowRainbow()) {
                            this.colorStrobePokemon(pixelmon, 1.0f);
                        }
                        this.renderModel(pixelmon, f8, f7, f5, f4, f9, 0.0625f);
                    }
                    finally {
                        RenderPixelmon.glowOff();
                    }
                }
            }
            if (flag) {
                this.unsetBrightness();
            }
            GlStateManager.depthMask(true);
            GlStateManager.disableRescaleNormal();
        }
        catch (Exception var25) {
            var25.printStackTrace();
        }
        OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        if (!fromPokedex) {
            GlStateManager.enableTexture2D();
        }
        OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
        GlStateManager.popMatrix();
        if (!fromPokedex) {
            this.renderName(pixelmon, x, y, z);
        }
    }

    public static void glowOn() {
        RenderPixelmon.glowOn(15);
    }

    public static void glowOn(int glow) {
        glowing = true;
        GL11.glPushAttrib((int)64);
        GL11.glEnable((int)3042);
        GL11.glBlendFunc((int)1, (int)1);
        try {
            lightmapLastX = OpenGlHelper.lastBrightnessX;
            lightmapLastY = OpenGlHelper.lastBrightnessY;
        }
        catch (NoSuchFieldError e) {
            optifineBreak = true;
        }
        float glowRatioX = Math.min((float)glow / 15.0f * 240.0f + lightmapLastX, 240.0f);
        float glowRatioY = Math.min((float)glow / 15.0f * 240.0f + lightmapLastY, 240.0f);
        if (!optifineBreak) {
            OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, glowRatioX, glowRatioY);
        }
    }

    public static void glowOff() {
        GL11.glEnable((int)2896);
        if (!optifineBreak) {
            OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, lightmapLastX, lightmapLastY);
        }
        GL11.glPopAttrib();
        glowing = false;
    }

    private void colorStrobePokemon(EntityPixelmon pokemon, float a) {
        try {
            String[] tint = (pokemon.getColorTint().isEmpty() ? "255/0/0" : pokemon.getColorTint()).split("/");
            int r = Integer.parseInt(tint[0]);
            int g = Integer.parseInt(tint[1]);
            int b = Integer.parseInt(tint[2]);
            int increment = 2;
            int min = 75;
            if (r > 75 && b <= 75) {
                r -= 2;
                g += 2;
            }
            if (g > 75 && r <= 75) {
                g -= 2;
                b += 2;
            }
            if (b > 75 && g <= 75) {
                r += 2;
                b -= 2;
            }
            if (r < 75) {
                r = 75;
            }
            if (g < 75) {
                g = 75;
            }
            if (b < 75) {
                b = 75;
            }
            pokemon.setColorTint(String.format("%s/%s/%s", r, g, b), true);
            this.colorTint((float)r / 255.0f, (float)g / 255.0f, (float)b / 255.0f, a);
        }
        catch (Exception exception) {
            pokemon.setColorTint("255/0/0", true);
        }
    }

    private void colorTint(float r, float g, float b, float a) {
        GlStateManager.color(r, g, b, a);
    }

    private void renderEvolutionLightBeams(EntityPixelmon pixelmon) {
        if (pixelmon.evoStage == EvolutionStage.PreAnimation || pixelmon.evoStage == EvolutionStage.PostAnimation) {
            float ticks = pixelmon.evoAnimTicks;
            if (pixelmon.evoStage == EvolutionStage.PostAnimation) {
                ticks += (float)EvolutionStage.PreAnimation.ticks;
            }
            Tessellator tessellator = Tessellator.getInstance();
            BufferBuilder worldRenderer = tessellator.getBuffer();
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            GlStateManager.disableTexture2D();
            GlStateManager.disableLighting();
            GlStateManager.enableBlend();
            float width = pixelmon.baseStats.width;
            float height = pixelmon.baseStats.height;
            float scale = ticks / ((float)EvolutionStage.PreAnimation.ticks + (float)EvolutionStage.PostAnimation.ticks);
            if (scale > 1.0f) {
                scale = 1.0f;
            }
            float hScale = scale * pixelmon.heightDiff / pixelmon.baseStats.height;
            GlStateManager.pushMatrix();
            if (pixelmon.evoStage == EvolutionStage.PreAnimation) {
                GlStateManager.translate(0.0f, -1.0f * (height * (1.0f + hScale)) / 2.0f, 0.0f);
            } else {
                GlStateManager.translate(0.0f, -1.0f * (height * (1.0f - hScale)) / 2.0f, 0.0f);
            }
            float length = scale * 18.0f + 2.0f;
            if (ticks > 20.0f && pixelmon.fadeCount > 18) {
                GlStateManager.pushMatrix();
                GlStateManager.rotate(ticks * 3.0f, 0.0f, 1.0f, 0.0f);
                GlStateManager.rotate(15.0f, 1.0f, 0.0f, 1.0f);
                this.drawBeam(width / 10.0f, length, worldRenderer, tessellator);
                GlStateManager.popMatrix();
            }
            if (ticks > 40.0f && pixelmon.fadeCount > 15) {
                GlStateManager.pushMatrix();
                GlStateManager.rotate(ticks * 3.0f + 180.0f, 0.0f, 1.0f, 0.0f);
                GlStateManager.rotate(15.0f, 1.0f, 0.0f, 1.0f);
                this.drawBeam(width / 10.0f, length, worldRenderer, tessellator);
                GlStateManager.popMatrix();
            }
            if (ticks > 60.0f && pixelmon.fadeCount > 12) {
                GlStateManager.pushMatrix();
                GlStateManager.rotate(ticks * 3.0f + 270.0f, 0.0f, 1.0f, 0.0f);
                GlStateManager.rotate(40.0f, 1.0f, 0.0f, 1.0f);
                this.drawBeam(width / 10.0f, length, worldRenderer, tessellator);
                GlStateManager.popMatrix();
            }
            if (ticks > 80.0f && pixelmon.fadeCount > 9) {
                GlStateManager.pushMatrix();
                GlStateManager.rotate(ticks * 3.0f + 90.0f, 0.0f, 1.0f, 0.0f);
                GlStateManager.rotate(40.0f, 1.0f, 0.0f, 1.0f);
                this.drawBeam(width / 10.0f, length, worldRenderer, tessellator);
                GlStateManager.popMatrix();
            }
            if (ticks > 100.0f && pixelmon.fadeCount > 6) {
                GlStateManager.pushMatrix();
                GlStateManager.rotate(ticks * 3.0f + 135.0f, 0.0f, 1.0f, 0.0f);
                GlStateManager.rotate(65.0f, 1.0f, 0.0f, 1.0f);
                this.drawBeam(width / 10.0f, length, worldRenderer, tessellator);
                GlStateManager.popMatrix();
            }
            if (ticks > 120.0f && pixelmon.fadeCount > 3) {
                GlStateManager.pushMatrix();
                GlStateManager.rotate(ticks * 3.0f + 315.0f, 0.0f, 1.0f, 0.0f);
                GlStateManager.rotate(65.0f, 1.0f, 0.0f, 1.0f);
                this.drawBeam(width / 10.0f, length, worldRenderer, tessellator);
                GlStateManager.popMatrix();
            }
            GlStateManager.popMatrix();
            GlStateManager.disableBlend();
            GlStateManager.enableLighting();
            GlStateManager.enableTexture2D();
        }
    }

    private void drawBeam(float width, float length, BufferBuilder worldRenderer, Tessellator tessellator) {
        float alpha = 0.6f;
        worldRenderer.begin(7, DefaultVertexFormats.POSITION_COLOR);
        worldRenderer.pos(-width, 0.0f * length, 0.0).color(1.0f, 1.0f, 1.0f, alpha).endVertex();
        worldRenderer.pos(width, 0.0f * length, 0.0).color(1.0f, 1.0f, 1.0f, alpha).endVertex();
        worldRenderer.pos(width * 3.0f, -length, 0.0).color(1.0f, 1.0f, 1.0f, alpha).endVertex();
        worldRenderer.pos(-width * 3.0f, -length, 0.0).color(1.0f, 1.0f, 1.0f, alpha).endVertex();
        tessellator.draw();
        worldRenderer.begin(7, DefaultVertexFormats.POSITION_COLOR);
        worldRenderer.pos(0.0, 0.0f * length, -width).color(1.0f, 1.0f, 1.0f, alpha).endVertex();
        worldRenderer.pos(0.0, 0.0f * length, width).color(1.0f, 1.0f, 1.0f, alpha).endVertex();
        worldRenderer.pos(0.0, -length, width * 3.0f).color(1.0f, 1.0f, 1.0f, alpha).endVertex();
        worldRenderer.pos(0.0, -length, -width * 3.0f).color(1.0f, 1.0f, 1.0f, alpha).endVertex();
        tessellator.draw();
    }

    public void drawNameTag(EntityPixelmon entityPixelmon, double x, double y, double z, boolean owned) {
        if (Minecraft.isGuiEnabled() && !entityPixelmon.isSneaking() && entityPixelmon.getControllingPassenger() != Minecraft.getMinecraft().player) {
            this.renderLabel(entityPixelmon, x, y, z, owned);
        }
    }

    protected void renderLabel(EntityPixelmon pixelmon, double x, double y, double z, boolean owned) {
        String line1;
        String line2;
        float scaleFactor;
        FontRenderer fontRenderer = this.getFontRendererFromRenderManager();
        float var13 = 1.6f;
        float baseScale = 0.016666668f * var13;
        GlStateManager.pushMatrix();
        float f = scaleFactor = PixelmonConfig.scaleModelsUp ? 1.3f : 1.0f;
        if (pixelmon.isTotem()) {
            scaleFactor += 2.0f;
        }
        float baseHeight = pixelmon.baseStats.height;
        if (pixelmon.getSpecies() == EnumSpecies.Squirtle && pixelmon.getSpecialTextureIndex() == 3) {
            baseHeight = (float)((double)baseHeight + 0.25);
        }
        float height = (float)y + 0.7f + (pixelmon.baseStats.doesHover ? 1.0f : 0.0f) + baseHeight * pixelmon.getPixelmonScale() * (scaleFactor *= pixelmon.getScaleFactor());
        if (pixelmon.getSpecies() == EnumSpecies.Mew && (pixelmon.getSpecialTextureIndex() == 2 || pixelmon.getSpecialTextureIndex() == 3)) {
            height = (float)((double)height + 0.9);
        }
        if (pixelmon.isPokemon(EnumSpecies.Bellsprout) && pixelmon.getSpecialTextureEnum() == EnumTextures.Classic) {
            height += 0.4f;
        }
        GlStateManager.translate((float)x + 0.0f, height, (float)z);
        GL11.glNormal3f((float)0.0f, (float)1.0f, (float)0.0f);
        GlStateManager.rotate(-this.renderManager.playerViewY, 0.0f, 1.0f, 0.0f);
        GlStateManager.rotate(this.renderManager.playerViewX, 1.0f, 0.0f, 0.0f);
        GlStateManager.scale(-baseScale, -baseScale, baseScale);
        GlStateManager.disableLighting();
        GlStateManager.enableDepth();
        GlStateManager.disableAlpha();
        GlStateManager.color(1.0f, 1.0f, 1.0f);
        String line3 = null;
        int color1 = 0x20FFFFFF;
        int color2 = 0x20FFFFFF;
        int pokemonLevel = pixelmon.getLvl().getLevel();
        if (owned) {
            line2 = pixelmon.getNickname();
            line1 = Minecraft.getMinecraft().player.getName();
            line3 = I18n.translateToLocal("gui.screenpokechecker.lvl") + " " + pokemonLevel;
        } else if (pixelmon.getOwner() != null) {
            line2 = pixelmon.getNickname();
            line1 = pixelmon.getOwner().getName() + ":";
            line3 = I18n.translateToLocal("gui.screenpokechecker.lvl") + " " + pokemonLevel;
        } else {
            EnumBossMode enumBossMode = pixelmon.getBossMode();
            if (enumBossMode == EnumBossMode.NotBoss) {
                if (this.lvlTag == null) {
                    this.lvlTag = I18n.translateToLocal("gui.screenpokechecker.lvl");
                }
                line1 = this.lvlTag;
                line2 = "" + pokemonLevel;
            } else {
                if (this.bossTag == null) {
                    this.bossTag = I18n.translateToLocal("gui.boss.text");
                }
                line1 = this.bossTag;
                line2 = enumBossMode.getBossText();
                int color = enumBossMode.getColourInt();
                if (PixelmonConfig.showWildNames) {
                    color1 = color;
                } else {
                    color2 = color;
                }
            }
            if (PixelmonConfig.showWildNames) {
                line1 = line1 + " " + line2;
                line2 = pixelmon.getNickname();
            }
            if (pixelmon.isTotem()) {
                line1 = HexColor.of("#fad955Totem");
                line2 = HexColor.of("#f7c90f" + pixelmon.getName());
            }
        }
        int line2Width = fontRenderer.getStringWidth(line2);
        int line2Pos = line2Width / 2 * -1;
        GlStateManager.pushMatrix();
        GlStateManager.translate(-2.5 + (double)line2Pos, -4.5, 0.0);
        GlStateManager.scale(0.5, 0.5, 0.5);
        fontRenderer.drawString(line1, 0, 0, color1);
        GlStateManager.popMatrix();
        fontRenderer.drawString(line2, -line2Width / 2, 0, color2);
        if (line3 != null) {
            GlStateManager.pushMatrix();
            GlStateManager.translate(2.5 + (double)line2Pos, 9.0, 0.0);
            GlStateManager.scale(0.5, 0.5, 0.5);
            fontRenderer.drawString(line3, 0, 0, color1);
            GlStateManager.popMatrix();
        }
        GlStateManager.enableDepth();
        GlStateManager.depthMask(true);
        GlStateManager.disableTexture2D();
        int center = 50;
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder vertexBuffer = tessellator.getBuffer();
        vertexBuffer.begin(7, DefaultVertexFormats.POSITION_COLOR);
        if (PixelmonConfig.drawHealthBars) {
            this.drawHealthBars(pixelmon, center, owned, vertexBuffer);
        }
        tessellator.draw();
        GlStateManager.disableBlend();
        GlStateManager.enableTexture2D();
        GlStateManager.enableLighting();
        GlStateManager.disableBlend();
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GlStateManager.popMatrix();
    }

    private void drawHealthBars(EntityPixelmon pixelmon, int center, boolean owned, BufferBuilder vertexBuffer) {
        if (owned && pixelmon.getNumBreedingLevels() == -1) {
            float exp = pixelmon.getLvl().getExp();
            float expNext = pixelmon.getLvl().getExpForNextLevelClient();
            if (exp >= expNext) {
                exp = expNext;
            }
            float f8 = 50.0f * (exp / expNext);
            vertexBuffer.pos(-25.0f + f8, 12.0, 0.0).color(0.0039f, 0.03137f, 0.4196f, 1.0f).endVertex();
            vertexBuffer.pos(-25.0f + f8, 13.0, 0.0).color(0.0039f, 0.03137f, 0.4196f, 1.0f).endVertex();
            vertexBuffer.pos(25.0, 13.0, 0.0).color(0.0039f, 0.03137f, 0.4196f, 1.0f).endVertex();
            vertexBuffer.pos(25.0, 12.0, 0.0).color(0.0039f, 0.03137f, 0.4196f, 1.0f).endVertex();
            vertexBuffer.pos(-25.0, 12.0, 0.0).color(0.0f, 0.8901f, 0.8901f, 1.0f).endVertex();
            vertexBuffer.pos(-25.0, 13.0, 0.0).color(0.0f, 0.8901f, 0.8901f, 1.0f).endVertex();
            vertexBuffer.pos(f8 - 25.0f, 13.0, 0.0).color(0.0f, 0.8901f, 0.8901f, 1.0f).endVertex();
            vertexBuffer.pos(f8 - 25.0f, 12.0, 0.0).color(0.0f, 0.8901f, 0.8901f, 1.0f).endVertex();
        }
        if (pixelmon.getNumBreedingLevels() == -1) {
            float hp = pixelmon.getHealth();
            float maxHp = pixelmon.getMaxHealth();
            float healthWidth = 50.0f * (hp / maxHp);
            vertexBuffer.pos(-25.0f + healthWidth, 10.0, 0.0).color(0.5f, 0.5f, 0.5f, 1.0f).endVertex();
            vertexBuffer.pos(-25.0f + healthWidth, 11.0, 0.0).color(0.5f, 0.5f, 0.5f, 1.0f).endVertex();
            vertexBuffer.pos(25.0, 11.0, 0.0).color(0.5f, 0.5f, 0.5f, 1.0f).endVertex();
            vertexBuffer.pos(25.0, 10.0, 0.0).color(0.5f, 0.5f, 0.5f, 1.0f).endVertex();
            float r = 0.0f;
            float g = 0.0f;
            float b = 0.0f;
            float a = 1.0f;
            if (hp <= maxHp / 5.0f) {
                r = 0.8f;
                g = 0.0f;
                b = 0.0f;
            } else if (hp <= maxHp / 2.0f) {
                r = 1.0f;
                g = 1.0f;
                b = 0.2f;
            } else {
                r = 0.2f;
                g = 1.0f;
                b = 0.2f;
            }
            vertexBuffer.pos(-25.0, 10.0, 0.0).color(r, g, b, a).endVertex();
            vertexBuffer.pos(-25.0, 11.0, 0.0).color(r, g, b, a).endVertex();
            vertexBuffer.pos(healthWidth - 25.0f, 11.0, 0.0).color(r, g, b, a).endVertex();
            vertexBuffer.pos(healthWidth - 25.0f, 10.0, 0.0).color(r, g, b, a).endVertex();
        }
    }

    protected void preRenderScale(EntityPixelmon entity, float f) {
        float lScale;
        float hScale;
        float wScale;
        float scale;
        float scaleFactor = PixelmonConfig.scaleModelsUp ? 1.3f : 1.0f;
        scaleFactor *= entity.getScaleFactor();
        if (entity.getModel() instanceof PixelmonModelBase) {
            scaleFactor *= ((PixelmonModelBase)entity.getModel()).getScale();
            if (entity.isTotem()) {
                scaleFactor *= 2.0f;
            }
        }
        float giScale = entity.baseStats.giScale;
        if (entity.getSpecies() == EnumSpecies.Charmander && entity.getSpecialTextureIndex() == 2) {
            giScale = 1.0f;
        } else if (entity.isPokemon(EnumSpecies.Aerodactyl) && entity.getSpecialTextureEnum() == EnumTextures.Classic) {
            giScale = 1.0f;
        } else if (entity.isPokemon(EnumSpecies.Abra) && entity.getSpecialTextureEnum() == EnumTextures.Classic) {
            giScale = 2.0f;
        } else if (entity.isPokemon(EnumSpecies.Bellsprout) && entity.getSpecialTextureEnum() == EnumTextures.Classic) {
            giScale = 0.3f;
        }
        GlStateManager.scale(scaleFactor * entity.getPixelmonScale() * giScale, scaleFactor * entity.getPixelmonScale() * giScale, scaleFactor * entity.getPixelmonScale() * giScale);
        if (entity.evoStage == EvolutionStage.PreAnimation) {
            scale = (float)entity.evoAnimTicks / ((float)EvolutionStage.PreAnimation.ticks + (float)EvolutionStage.PostAnimation.ticks);
            if (scale > 1.0f) {
                scale = 1.0f;
            }
            wScale = scale * entity.widthDiff / entity.baseStats.width;
            hScale = scale * entity.heightDiff / entity.baseStats.height;
            lScale = scale * entity.lengthDiff / entity.baseStats.length;
            GlStateManager.scale(1.0f + wScale, 1.0f + hScale, 1.0f + lScale);
        }
        if (entity.evoStage == EvolutionStage.PostAnimation) {
            scale = ((float)entity.evoAnimTicks + (float)EvolutionStage.PreAnimation.ticks) / ((float)EvolutionStage.PreAnimation.ticks + (float)EvolutionStage.PostAnimation.ticks);
            if (scale > 1.0f) {
                scale = 1.0f;
            }
            scale = 1.0f - scale;
            wScale = scale * entity.widthDiff / entity.baseStats.width;
            hScale = scale * entity.heightDiff / entity.baseStats.height;
            lScale = scale * entity.lengthDiff / entity.baseStats.length;
            GlStateManager.scale(1.0f - wScale, 1.0f - hScale, 1.0f - lScale);
        }
        if (entity.baseStats.doesHover) {
            GlStateManager.translate(0.0f, -1.0f * entity.baseStats.hoverHeight, 0.0f);
        }
    }

    @Override
    protected void preRenderCallback(EntityPixelmon entityPixelmon, float f) {
        this.preRenderScale(entityPixelmon, f);
    }

    @Override
    protected ResourceLocation getEntityTexture(EntityPixelmon entityPixelmon) {
        return entityPixelmon.getTexture();
    }

    @Override
    protected boolean bindEntityTexture(EntityPixelmon entity) {
        ResourceLocation resourcelocation = this.getEntityTexture(entity);
        if (resourcelocation != null) {
            try {
                if (entity.isDynamaxed() && entity.hasGmaxFactor() && entity.getSpecies().hasGmaxForm()) {
                    if (entity.getSpecies() == EnumSpecies.Alcremie) {
                        this.bindTexture(new ResourceLocation("pixelmon", entity.isShiny() ? "textures/pokemon/pokemon-shiny/shinyalcremie-gigantamax.png" : "textures/pokemon/alcremie-gigantamax.png"));
                    } else {
                        String pokeTexture = resourcelocation.getPath();
                        if (!entity.getSpecialTexture().isEmpty()) {
                            pokeTexture = pokeTexture.replaceAll(entity.getSpecialTexture(), "");
                        }
                        pokeTexture = pokeTexture.contains("-") ? (PixelmonModelRegistry.gmaxModels.get((Object)entity.getSpecies()).size() == 1 ? pokeTexture.replaceAll(".png", "-gigantamax.png") : pokeTexture.replaceAll(".png", "-gigantamax.png")) : pokeTexture.replaceAll(".png", "-gigantamax.png");
                        this.bindTexture(new ResourceLocation("pixelmon", pokeTexture));
                    }
                    return true;
                }
            }
            catch (Exception pokeTexture) {
                // empty catch block
            }
            String[] splits = resourcelocation.getPath().split("/");
            String textureName = splits[splits.length - 1].replaceAll(".png", "");
            TextureResource dynTexture = ClientProxy.TEXTURE_STORE.getObject(textureName);
            if (glowing) {
                if (dynTexture != null && dynTexture.hasGlow()) {
                    dynTexture.bindTexture(true);
                } else {
                    String path = resourcelocation.getPath();
                    path = path.replaceAll("/pokemon-shiny/", "");
                    String pokeTexture = path.split("/")[2];
                    path = path.replaceAll(pokeTexture, "");
                    this.bindTexture(new ResourceLocation("pixelmon", path + "pokemon-glow/" + pokeTexture));
                }
                return true;
            }
            if (dynTexture != null) {
                dynTexture.bindTexture();
            } else {
                this.bindTexture(resourcelocation);
            }
            return true;
        }
        return false;
    }

    @Override
    protected float interpolateRotation(float par1, float par2, float par3) {
        float f3;
        for (f3 = par2 - par1; f3 < -180.0f; f3 += 360.0f) {
        }
        while (f3 >= 180.0f) {
            f3 -= 360.0f;
        }
        return par1 + par3 * f3;
    }

    @Override
    public void renderName(EntityPixelmon entity, double x, double y, double z) {
        boolean owned = ServerStorageDisplay.contains(entity.getPokemonId());
        float renderdistance = entity.getBossMode() != EnumBossMode.NotBoss ? (float)this.nameRenderDistanceBoss : (float)this.nameRenderDistanceNormal;
        float f = renderdistance;
        if (this.distance <= renderdistance || owned) {
            this.drawNameTag(entity, x, y, z, owned);
        }
    }

    static {
        optifineBreak = false;
        glowing = false;
    }
}

