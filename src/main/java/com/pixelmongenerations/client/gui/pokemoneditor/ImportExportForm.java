/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.pokemoneditor;

import com.pixelmongenerations.client.gui.pokemoneditor.FormData;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

class ImportExportForm {
    private static ImportExportForm instance;
    private Map<EnumSpecies, Map<Short, String>> speciesMap = new HashMap<EnumSpecies, Map<Short, String>>();
    private Map<String, FormData> nameMap = new HashMap<String, FormData>();

    static ImportExportForm getInstance() {
        if (instance == null) {
            instance = new ImportExportForm();
        }
        return instance;
    }

    private ImportExportForm() {
        for (EnumSpecies pokemon : EnumSpecies.formList.keySet()) {
            List<IEnumForm> list = EnumSpecies.formList.get(pokemon);
            this.addFormData(pokemon, (short)0, "");
            this.addFormData(pokemon, (short)-1, "");
            for (IEnumForm iEnumForm : list) {
                this.addFormData(pokemon, iEnumForm.getForm(), iEnumForm.getFormSuffix());
            }
        }
    }

    private void addSpeciesFormData(EnumSpecies species, String ... formNames) {
        this.addFormData(species, (short)0, "");
        this.addFormData(species, (short)-1, "");
        for (int i = 0; i < formNames.length; i = (int)((short)(i + 1))) {
            String formName = formNames[i];
            this.addFormData(species, (short)(i + 1), formName);
        }
    }

    private void addFormData(EnumSpecies species, short formIndex, String formName) {
        Map<Short, String> formMap;
        if (this.speciesMap.containsKey(species)) {
            formMap = this.speciesMap.get(species);
        } else {
            formMap = new HashMap();
            this.speciesMap.put(species, formMap);
        }
        String fullFormName = species.name;
        if (!formName.isEmpty()) {
            fullFormName = fullFormName + formName;
        }
        formMap.put(formIndex, fullFormName);
        this.nameMap.put(fullFormName, new FormData(species, formIndex));
    }

    String getFormName(EnumSpecies species, short formIndex) {
        if (this.speciesMap.isEmpty()) {
            instance = new ImportExportForm();
        }
        if (this.speciesMap.containsKey(species)) {
            Map<Short, String> formMap = this.speciesMap.get(species);
            if (formMap.containsKey(formIndex)) {
                return formMap.get(formIndex);
            }
            return formMap.get((short)0);
        }
        return species.name;
    }

    Optional<FormData> getFormData(String formName) {
        if (this.speciesMap.isEmpty()) {
            instance = new ImportExportForm();
        }
        if (this.nameMap.containsKey(formName)) {
            return Optional.of(this.nameMap.get(formName));
        }
        return Optional.empty();
    }
}

