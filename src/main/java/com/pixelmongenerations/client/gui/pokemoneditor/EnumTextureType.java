/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.pokemoneditor;

import net.minecraft.client.resources.I18n;

enum EnumTextureType {
    Normal,
    Shiny,
    Special;


    String getLocalizedName() {
        return I18n.format("gui.trainereditor." + this.name().toLowerCase(), new Object[0]);
    }
}

