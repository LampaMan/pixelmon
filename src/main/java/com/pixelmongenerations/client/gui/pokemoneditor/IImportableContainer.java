/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.pokemoneditor;

import net.minecraft.client.gui.GuiScreen;

public interface IImportableContainer {
    public String getExportText();

    public String importText(String var1);

    public GuiScreen getScreen();
}

