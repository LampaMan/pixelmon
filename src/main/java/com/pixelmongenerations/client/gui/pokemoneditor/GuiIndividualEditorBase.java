/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.pokemoneditor;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.elements.GuiButtonHoverDisable;
import com.pixelmongenerations.client.gui.elements.GuiContainerDropDown;
import com.pixelmongenerations.client.gui.elements.GuiDropDown;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiImportExport;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiPokemonEditorAdvanced;
import com.pixelmongenerations.client.gui.pokemoneditor.IImportableContainer;
import com.pixelmongenerations.client.gui.pokemoneditor.ImportExportConverter;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.IEnumSpecialTexture;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.PixelmonMovesetData;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.util.text.translation.I18n;

public abstract class GuiIndividualEditorBase
extends GuiContainerDropDown
implements IImportableContainer {
    public PixelmonData p;
    public String titleText;
    private GuiTextField tbName;
    private GuiTextField tbNickname;
    private GuiTextField tbLvl;
    private GuiTextField[] tbMoves = new GuiTextField[4];
    private String origName;
    private Attack[] attacks = new Attack[4];
    protected GuiTextField formText = null;
    private static final int BUTTON_OKAY = 1;
    private static final int BUTTON_GENDER = 3;
    private static final int BUTTON_CHANGE = 4;
    private static final int BUTTON_ADVANCED = 6;
    private static final int BUTTON_DELETE = 14;
    private static final int BUTTON_IMPORT_EXPORT = 15;
    protected List<GuiTextField> textFields = new ArrayList<GuiTextField>();
    GuiDropDown texture;

    public GuiIndividualEditorBase(PixelmonData p, String titleText) {
        this.p = p;
        this.origName = Entity1Base.getLocalizedName(this.p.name);
        this.titleText = titleText;
    }

    @Override
    public void initGui() {
        int i;
        super.initGui();
        this.buttonList.clear();
        this.buttonList.add(new GuiButton(1, this.width / 2 + 155, this.height / 2 + 90, 30, 20, I18n.translateToLocal("gui.guiItemDrops.ok")));
        this.updateTextureDropDown();
        int malePercent = Entity3HasStats.getBaseStats((String)this.p.name).get().malePercent;
        if (malePercent > 0 && malePercent < 100) {
            this.buttonList.add(new GuiButtonHoverDisable(3, this.width / 2 - 120, this.height / 2 + 30, 80, 20, this.p.gender == Gender.Male ? I18n.translateToLocal("gui.trainereditor.male") : I18n.translateToLocal("gui.trainereditor.female")));
        }
        this.addButton(new GuiButton(30, this.width / 2 + 15, this.height / 2 + 30, 30, 20, this.p.isShiny ? "Yes" : "No"));
        if (this.p.getSpecies().hasGmaxForm()) {
            this.addButton(new GuiButton(52, this.width / 2 + 15, this.height / 2, 30, 20, this.p.hasGmaxFactor ? "Yes" : "No"));
        }
        this.addDropDown(new GuiDropDown<EnumGrowth>(EnumGrowth.getProperOrder(), this.p.growth, this.width / 2 - 120, this.height / 2 + 28, 80, 100).setOnSelected(growth -> {
            this.p.growth = growth;
        }).setGetOptionString(EnumGrowth::getLocalizedName).setInactiveTop(this.height / 2 + 54));
        this.tbName = new GuiTextField(6, this.mc.fontRenderer, this.width / 2 - 120, this.height / 2 - 50, 90, 17);
        this.tbName.setText(Entity1Base.getLocalizedName(this.p.name.toLowerCase()));
        this.buttonList.add(new GuiButton(4, this.width / 2 - 20, this.height / 2 - 52, 100, 20, I18n.translateToLocal("gui.trainereditor.changepokemon")));
        this.tbLvl = new GuiTextField(7, this.mc.fontRenderer, this.width / 2 - 120, this.height / 2 - 30, 60, 17);
        this.tbLvl.setText(this.p.lvl + "");
        this.tbNickname = new GuiTextField(8, this.mc.fontRenderer, this.width / 2 - 120, this.height / 2 - 10, 90, 17);
        this.tbNickname.setText(this.p.nickname);
        for (i = 0; i < this.tbMoves.length; ++i) {
            this.tbMoves[i] = new GuiTextField(9 + i, this.mc.fontRenderer, this.width / 2 + 60, this.height / 2 + 20 * i, 120, 17);
        }
        for (i = 0; i < this.p.numMoves; ++i) {
            this.attacks[i] = new Attack(this.p.moveset[i].attackIndex);
            this.tbMoves[i].setText(this.attacks[i].getAttackBase().getLocalizedName());
        }
        this.buttonList.add(new GuiButton(6, this.width / 2 + 105, this.height / 2 - 110, 80, 20, I18n.translateToLocal("gui.trainereditor.advanced")));
        this.textFields.clear();
        this.textFields.addAll(Arrays.asList(this.tbName, this.tbLvl, this.tbNickname, this.tbMoves[0], this.tbMoves[1], this.tbMoves[2], this.tbMoves[3]));
        if (Entity3HasStats.hasForms(this.p.name)) {
            this.formText = this.createExtraTextField(13);
            this.formText.setText(Short.toString(this.p.form));
            this.textFields.add(this.formText);
        } else {
            this.formText = null;
        }
        if (this.showDeleteButton()) {
            this.buttonList.add(new GuiButton(14, this.width / 2 - 185, this.height / 2 - 110, 90, 20, I18n.translateToLocal("gui.trainereditor.deletepoke")));
        }
        this.buttonList.add(new GuiButton(15, this.width / 2 + 20, this.height / 2 + 90, 100, 20, I18n.translateToLocal("gui.pokemoneditor.importexport")));
    }

    protected GuiTextField createExtraTextField(int id) {
        return new GuiTextField(id, this.mc.fontRenderer, this.width / 2 - 120, this.height / 2 + 72, 60, 17);
    }

    private void setTextureType(IEnumSpecialTexture textureType) {
        this.p.specialTexture = (short)textureType.getId();
    }

    protected boolean showDeleteButton() {
        return true;
    }

    @Override
    protected void drawBackgroundUnderMenus(float f, int i, int j) {
        if (this.tbName == null) {
            this.initGui();
        }
        this.mc.renderEngine.bindTexture(GuiResources.cwPanel);
        GuiHelper.drawImageQuad(this.width / 2 - 200, this.height / 2 - 120, 400.0, 240.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        RenderHelper.disableStandardItemLighting();
        this.mc.fontRenderer.drawString(this.titleText, this.width / 2 - this.mc.fontRenderer.getStringWidth(this.titleText) / 2, this.height / 2 - 90, 0);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.name"), this.width / 2 - 180, this.height / 2 - 45, 0);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.lvl"), this.width / 2 - 180, this.height / 2 - 25, 0);
        this.tbName.drawTextBox();
        this.tbLvl.drawTextBox();
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.nickname"), this.width / 2 - 180, this.height / 2 - 5, 0);
        this.tbNickname.drawTextBox();
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.special"), this.width / 2 - 180, this.height / 2 + 15, 0);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.gender"), this.width / 2 - 180, this.height / 2 + 35, 0);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.shiny"), this.width / 2 - 20, this.height / 2 + 35, 0);
        if (this.p.getSpecies().hasGmaxForm()) {
            this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.gmax"), this.width / 2 - 20, this.height / 2 + 5, 0);
        }
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.growth"), this.width / 2 - 180, this.height / 2 + 55, 0);
        GuiHelper.bindPokemonSprite(this.p, this.mc);
        GlStateManager.color(1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(this.width / 2 - 157, this.height / 2 - 73 - (this.p.isGen6Sprite() ? -3 : 0), 20.0, 20.0f, 0.0, 0.0, 1.0, 1.0, 1.0f);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.screenpokechecker.moves"), this.width / 2 + 100, this.height / 2 - 15, 0);
        for (GuiTextField tbMove : this.tbMoves) {
            tbMove.drawTextBox();
        }
        this.drawExtraText(this.formText, "gui.trainereditor.form");
    }

    protected void drawExtraText(GuiTextField textField, String langKey) {
        if (textField != null) {
            textField.drawTextBox();
            this.mc.fontRenderer.drawString(I18n.translateToLocal(langKey), this.width / 2 - 180, textField.y + 3, 0);
        }
    }

    @Override
    protected void keyTyped(char key, int keyCode) throws IOException {
        this.tbLvl.textboxKeyTyped(key, keyCode);
        int lvl = 0;
        try {
            lvl = Integer.parseInt(this.tbLvl.getText());
            if (lvl > 0 && lvl <= PixelmonServerConfig.maxLevel) {
                this.p.lvl = lvl;
            }
        }
        catch (NumberFormatException numberFormatException) {
            // empty catch block
        }
        GuiHelper.textboxKeyTypedLimited(this.tbNickname, key, keyCode, 11);
        this.p.nickname = this.tbNickname.getText();
        this.tbName.textboxKeyTyped(key, keyCode);
        for (GuiTextField tbMove : this.tbMoves) {
            tbMove.textboxKeyTyped(key, keyCode);
        }
        if (this.formText != null) {
            this.formText.textboxKeyTyped(key, keyCode);
            boolean form = false;
            try {
                short newForm;
                this.p.form = newForm = Short.parseShort(this.formText.getText());
                if (EnumSpecies.getFormFromID(this.p.getSpecies(), newForm) == null || this.p.getBaseStats() == null) {
                    this.p.form = 0;
                }
            }
            catch (NumberFormatException newForm) {
                // empty catch block
            }
            boolean FormSupportsAbility = false;
            for (String s : this.p.getBaseStats().abilities) {
                if (!this.p.ability.equals(s)) continue;
                FormSupportsAbility = true;
                break;
            }
            if (!FormSupportsAbility) {
                this.p.ability = this.p.getBaseStats().abilities[0];
            }
            this.updateTextureDropDown();
        }
        GuiHelper.switchFocus(keyCode, this.textFields);
        if (keyCode == 1 || keyCode == 28) {
            this.saveFields();
        }
    }

    private void updateTextureDropDown() {
        List<IEnumSpecialTexture> shinyOptions = this.p.getSpecies().getSpecialTextures(this.p.form);
        IEnumSpecialTexture cuurentType = this.p.getSpecies().getSpecialTexture(this.p.getSpecies().getFormEnum(this.p.form), this.p.specialTexture);
        if (!shinyOptions.contains(EnumTextures.None)) {
            shinyOptions.add(EnumTextures.None);
        }
        this.removeDropDown(this.texture);
        this.texture = new GuiDropDown<IEnumSpecialTexture>(shinyOptions, cuurentType, this.width / 2 - 120, this.height / 2 + 14, 80, 50).setOnSelected(this::setTextureType).setGetOptionString(IEnumSpecialTexture::name);
        this.addDropDown(this.texture);
    }

    @Override
    protected void mouseClickedUnderMenus(int x, int y, int button) throws IOException {
        this.tbLvl.mouseClicked(x, y, button);
        this.tbNickname.mouseClicked(x, y, button);
        this.tbName.mouseClicked(x, y, button);
        for (GuiTextField tbMove : this.tbMoves) {
            tbMove.mouseClicked(x, y, button);
        }
        if (this.formText != null) {
            this.formText.mouseClicked(x, y, button);
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        if (button.enabled) {
            if (button.id == 1) {
                this.saveFields();
            } else if (button.id == 3 && Entity3HasStats.getBaseStats((String)this.p.name).get().malePercent > 0) {
                this.p.gender = this.p.gender == Gender.Male ? Gender.Female : Gender.Male;
                button.displayString = this.p.gender == Gender.Male ? I18n.translateToLocal("gui.trainereditor.male") : I18n.translateToLocal("gui.trainereditor.female");
            } else if (button.id == 4) {
                Optional<EnumSpecies> pokemon = EnumSpecies.getFromName(this.tbName.getText());
                if (pokemon.isPresent()) {
                    this.origName = this.tbName.getText();
                    this.changePokemon(pokemon.get());
                } else {
                    this.tbName.setText(this.origName);
                }
            } else if (button.id == 6) {
                if (this.checkFields()) {
                    this.mc.displayGuiScreen(new GuiPokemonEditorAdvanced(this));
                }
            } else if (button.id == 14) {
                this.deletePokemon();
            } else if (button.id == 15 && this.checkFields()) {
                this.mc.displayGuiScreen(new GuiImportExport(this, this.titleText));
            } else if (button.id == 30) {
                this.p.isShiny = !this.p.isShiny;
                button.displayString = this.p.isShiny ? "Yes" : "No";
            } else if (button.id == 52 && this.p.getSpecies().hasGmaxForm()) {
                this.p.hasGmaxFactor = !this.p.hasGmaxFactor;
                button.displayString = this.p.hasGmaxFactor ? "Yes" : "No";
            }
        }
    }

    protected abstract void changePokemon(EnumSpecies var1);

    protected abstract void deletePokemon();

    private void saveFields() {
        if (this.checkFields()) {
            this.saveAndClose();
        }
    }

    protected abstract void saveAndClose();

    protected boolean checkFields() {
        this.p.moveset[3] = null;
        this.p.moveset[2] = null;
        this.p.moveset[1] = null;
        this.p.moveset[0] = null;
        int numMoves = 0;
        for (GuiTextField tbMove : this.tbMoves) {
            String moveText = tbMove.getText();
            if (moveText.isEmpty()) continue;
            if (!Attack.hasAttack(moveText)) {
                tbMove.setText("");
                return false;
            }
            Attack a = new Attack(moveText);
            this.p.moveset[this.getNextAvailablePosition()] = PixelmonMovesetData.createPacket(a);
        }
        for (int i = 0; i < 4; ++i) {
            if (this.p.moveset[i] == null) continue;
            ++numMoves;
        }
        if (numMoves == 0) {
            return false;
        }
        this.p.numMoves = numMoves;
        return true;
    }

    public int getNextAvailablePosition() {
        for (int i = 0; i < 4; ++i) {
            if (this.p.moveset[i] != null) continue;
            return i;
        }
        return -1;
    }

    public abstract List<PixelmonData> getPokemonList();

    @Override
    public String getExportText() {
        return ImportExportConverter.getExportText(this.p);
    }

    @Override
    public String importText(String importText) {
        return ImportExportConverter.importText(importText, this.p);
    }

    @Override
    public GuiScreen getScreen() {
        return this;
    }
}

