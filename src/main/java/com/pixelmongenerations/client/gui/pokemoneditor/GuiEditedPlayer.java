/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.pokemoneditor;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.elements.GuiChatExtension;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.RequestCloseEditingPlayer;
import java.io.IOException;
import java.util.UUID;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.text.translation.I18n;

public class GuiEditedPlayer
extends GuiContainer {
    public static UUID editingPlayerUUID;
    public static String editingPlayerName;
    private GuiChatExtension chat = new GuiChatExtension(this, 35);
    private boolean forceClose;
    private static final int BUTTON_CANCEL = 0;

    public GuiEditedPlayer() {
        super(new ContainerEmpty());
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GuiHelper.drawCenteredString(I18n.translateToLocal("gui.pokemoneditor.editedplayer").replace("%s", editingPlayerName), this.width / 2, this.height / 2 - 20, 0xFFFFFF);
    }

    @Override
    public void handleKeyboardInput() {
        this.chat.handleKeyboardInput();
    }

    @Override
    public void initGui() {
        super.initGui();
        this.chat.initGui();
        this.buttonList.clear();
        this.buttonList.add(new GuiButton(0, this.width / 2 - 25, this.height / 2, 50, 20, I18n.translateToLocal("gui.pokemoneditor.cancel")));
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        this.chat.onGuiClosed();
        if (!this.forceClose) {
            Pixelmon.NETWORK.sendToServer(new RequestCloseEditingPlayer(editingPlayerUUID));
            this.forceClose = false;
        }
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        this.chat.updateScreen();
    }

    @Override
    public void drawScreen(int par1, int par2, float par3) {
        this.chat.drawScreen(par1, par2, par3);
        super.drawScreen(par1, par2, par3);
    }

    @Override
    protected void keyTyped(char par1, int par2) {
        this.chat.keyTyped(par1, par2);
    }

    @Override
    public void handleMouseInput() throws IOException {
        try {
            super.handleMouseInput();
        }
        catch (NullPointerException e) {
            if (PixelmonConfig.printErrors) {
                e.printStackTrace();
            }
            return;
        }
        this.chat.handleMouseInput();
    }

    @Override
    protected void mouseClicked(int par1, int par2, int par3) throws IOException {
        this.chat.mouseClicked(par1, par2, par3);
        super.mouseClicked(par1, par2, par3);
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        if (button.id == 0) {
            GuiHelper.closeScreen();
        }
    }

    public void markForceClose() {
        this.forceClose = true;
    }
}

