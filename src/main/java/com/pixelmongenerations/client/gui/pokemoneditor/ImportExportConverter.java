/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.pokemoneditor;

import com.pixelmongenerations.client.gui.pokemoneditor.FormData;
import com.pixelmongenerations.client.gui.pokemoneditor.ImportExportForm;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.HiddenPower;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.FriendShip;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.entity.pixelmon.stats.IVStore;
import com.pixelmongenerations.common.entity.pixelmon.stats.Stats;
import com.pixelmongenerations.common.entity.pixelmon.stats.extraStats.LakeTrioStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.extraStats.LightTrioStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.extraStats.MeloettaStats;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumTextures;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.enums.EnumNature;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.forms.EnumForms;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.PixelmonMovesetData;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ImportExportConverter {
    private static final String GENDER_TEXT = "Gender";
    private static final String ABILITY_TEXT = "Ability";
    private static final String LEVEL_TEXT = "Level";
    private static final String SHINY_TEXT = "Shiny";
    private static final String HAPPINESS_TEXT = "Happiness";
    private static final String EV_TEXT = "EVs";
    private static final String NATURE_TEXT = "Nature";
    private static final String IV_TEXT = "IVs";
    private static final String POKE_BALL_TEXT = "Poke Ball";
    private static final String GROWTH_TEXT = "Growth";
    private static final String CLONES_TEXT = "Clones";
    private static final String ENCHANTED_TEXT = "Enchants";
    private static final String WORMHOLES_TEXT = "Wormholes";
    private static final String ABUNDANT_ACTIVATIONS_TEXT = "ACTIVATIONS";
    private static final String MOVE_TEXT = "Moves";
    private static final String[] STAT_TEXT = new String[]{"HP", "Atk", "Def", "SpA", "SpD", "Spe"};
    private static final char MALE_SYMBOL = 'M';
    private static final char FEMALE_SYMBOL = 'F';
    private static final String SHINY_YES = "Yes";
    private static final String SHINY_NO = "No";
    private static final String SPECIAL_TEXTURE = "Texture";
    private static Map<String, String> importNameMap;

    private ImportExportConverter() {
    }

    public static String getExportText(PixelmonData data) {
        StringBuilder exportText = new StringBuilder();
        String exportName = ImportExportForm.getInstance().getFormName(data.getSpecies(), data.form);
        String nickname = data.getNickname();
        if (nickname.equals(exportName)) {
            exportText.append(exportName);
        } else {
            exportText.append(nickname);
            exportText.append(" (");
            exportText.append(exportName);
            exportText.append(")");
        }
        if (data.gender != Gender.None) {
            exportText.append(" (");
            exportText.append(data.gender == Gender.Male ? (char)'M' : 'F');
            exportText.append(")");
        }
        if (!data.heldItem.isEmpty()) {
            exportText.append(" @ ");
            exportText.append(data.heldItem.getDisplayName());
        }
        exportText.append("\n");
        ImportExportConverter.addColonSeparated(exportText, ABILITY_TEXT, ImportExportConverter.convertCamelCaseToWords(data.ability));
        if (data.lvl != PixelmonServerConfig.maxLevel) {
            ImportExportConverter.addColonSeparated(exportText, LEVEL_TEXT, data.lvl);
        }
        if (data.isShiny) {
            ImportExportConverter.addColonSeparated(exportText, SHINY_TEXT, data.isShiny ? SHINY_YES : SHINY_NO);
        }
        ImportExportConverter.addColonSeparated(exportText, SPECIAL_TEXTURE, data.getSpecies().getSpecialTexture(data.getSpecies().getFormEnum(data.form), data.specialTexture).name());
        if (data.friendship < FriendShip.getMaxFriendship()) {
            ImportExportConverter.addColonSeparated(exportText, HAPPINESS_TEXT, data.friendship);
        }
        ImportExportConverter.writeStats(exportText, data.evs, null, EV_TEXT, 0);
        exportText.append(data.nature.toString());
        exportText.append(" ");
        exportText.append(NATURE_TEXT);
        exportText.append("\n");
        ImportExportConverter.writeStats(exportText, data.ivs, data.caps, IV_TEXT, 31);
        if (data.pokeball != EnumPokeball.PokeBall) {
            ImportExportConverter.addColonSeparated(exportText, POKE_BALL_TEXT, data.pokeball);
        }
        ImportExportConverter.addColonSeparated(exportText, GROWTH_TEXT, (Object)data.growth);
        if (data.numClones > 0) {
            ImportExportConverter.addColonSeparated(exportText, CLONES_TEXT, data.numClones);
        }
        if (data.numEnchanted > 0) {
            ImportExportConverter.addColonSeparated(exportText, ENCHANTED_TEXT, data.numEnchanted);
        }
        if (data.numWormholes > 0) {
            ImportExportConverter.addColonSeparated(exportText, WORMHOLES_TEXT, data.numWormholes);
        }
        for (int i = 0; i < data.moveset.length; ++i) {
            Attack attack;
            if (data.moveset[i] == null || (attack = data.moveset[i].getAttack()) == null) continue;
            exportText.append("- ");
            exportText.append(attack.getAttackBase().getUnlocalizedName());
            exportText.append("\n");
        }
        return exportText.toString();
    }

    public static void addLine(StringBuilder builder, String label) {
        builder.append(label);
        builder.append("\n");
    }

    public static void addColonSeparated(StringBuilder builder, String label, Object value) {
        builder.append(label);
        builder.append(": ");
        builder.append(value.toString());
        builder.append("\n");
    }

    private static String convertCamelCaseToWords(String text) {
        if (text == null || text.length() < 2) {
            return text;
        }
        StringBuilder newText = new StringBuilder();
        int textLength = text.length();
        for (int i = 0; i < textLength; ++i) {
            char currentChar = text.charAt(i);
            if (currentChar >= 'A' && currentChar <= 'Z' && i > 0 && i < textLength) {
                newText.append(' ');
            }
            newText.append(currentChar);
        }
        return newText.toString();
    }

    private static void writeStats(StringBuilder exportText, int[] statArray, boolean[] caps, String statType, int defaultValue) {
        boolean defaultStats = true;
        for (int stat : statArray) {
            if (stat == defaultValue) continue;
            defaultStats = false;
            break;
        }
        if (!defaultStats) {
            exportText.append(statType);
            exportText.append(": ");
            boolean hasPrevious = false;
            for (int i = 0; i < statArray.length; ++i) {
                if (statArray[i] == defaultValue) continue;
                if (hasPrevious) {
                    exportText.append(" / ");
                }
                exportText.append(statArray[i]);
                if (caps != null && caps[i]) {
                    exportText.append("*");
                }
                exportText.append(" ");
                exportText.append(STAT_TEXT[i]);
                hasPrevious = true;
            }
            exportText.append("\n");
        }
    }

    public static String importText(String importText, PixelmonData data) {
        int i;
        PixelmonData tempData = new PixelmonData();
        String[] importTextSplit = importText.split("\n");
        try {
            String heldItemName;
            Item heldItem;
            BaseStats stats;
            Optional<FormData> formDataOptional;
            String formCorrection;
            int atIndex;
            int rightParenthesesIndex;
            String nextField;
            String currentLine = importTextSplit[0];
            int leftParenthesesIndex = currentLine.lastIndexOf(40);
            int genderIndex = -1;
            if (leftParenthesesIndex > -1 && (nextField = currentLine.substring(leftParenthesesIndex + 1, rightParenthesesIndex = currentLine.indexOf(41, leftParenthesesIndex)).trim()).length() == 1) {
                genderIndex = leftParenthesesIndex;
                char genderChar = nextField.charAt(0);
                if (genderChar == 'M') {
                    tempData.gender = Gender.Male;
                } else if (genderChar == 'F') {
                    tempData.gender = Gender.Female;
                } else {
                    return GENDER_TEXT;
                }
                leftParenthesesIndex = currentLine.lastIndexOf(40, leftParenthesesIndex - 1);
            }
            if (leftParenthesesIndex > -1) {
                tempData.nickname = currentLine.substring(0, leftParenthesesIndex).trim();
                rightParenthesesIndex = currentLine.indexOf(41, leftParenthesesIndex);
                tempData.name = currentLine.substring(leftParenthesesIndex + 1, rightParenthesesIndex);
                atIndex = currentLine.indexOf(64, rightParenthesesIndex);
            } else {
                atIndex = currentLine.indexOf(64);
                tempData.name = genderIndex > -1 ? currentLine.substring(0, genderIndex) : (atIndex > -1 ? currentLine.substring(0, atIndex) : currentLine);
                tempData.nickname = "";
            }
            tempData.name = tempData.name.trim();
            if (tempData.name.contains("Alola")) {
                String name = tempData.name.split("-")[0];
                formCorrection = EnumForms.Alolan.getFormSuffix();
                tempData.name = name + formCorrection;
            }
            if (tempData.name.contains("Fan") || tempData.name.contains("Frost") || tempData.name.contains("Heat") || tempData.name.contains("Mow") || tempData.name.contains("Wash")) {
                String name = tempData.name.split("-")[0];
                formCorrection = tempData.name.split("-")[1].toLowerCase();
                tempData.name = name + "-" + formCorrection;
            }
            if ((formDataOptional = ImportExportForm.getInstance().getFormData(tempData.name)).isPresent()) {
                FormData formData = formDataOptional.get();
                tempData.name = formData.species.name;
                tempData.form = formData.form;
            } else {
                tempData.name = tempData.name.trim();
                tempData.name = ImportExportConverter.convertName(tempData.name);
            }
            try {
                stats = Entity3HasStats.getBaseStats(tempData.name, (int)tempData.form).get();
            }
            catch (NoSuchElementException e) {
                return "Pok\u00e9mon";
            }
            tempData.nationalPokedexNumber = stats.nationalPokedexNumber;
            if (atIndex > -1 && (heldItem = PixelmonItems.getItemFromName(heldItemName = currentLine.substring(atIndex + 1).trim())) != null) {
                tempData.heldItem = new ItemStack(heldItem);
            }
            tempData.friendship = -1;
            for (int i2 = 0; i2 < data.evs.length; ++i2) {
                tempData.ivs[i2] = 31;
            }
            boolean setIVs = false;
            for (int currentIndex = 1; currentIndex < importTextSplit.length; ++currentIndex) {
                currentLine = importTextSplit[currentIndex];
                if (tempData.ability == null && currentLine.startsWith(ABILITY_TEXT)) {
                    String parsedAbility = ImportExportConverter.getStringAfterColon(currentLine).replace(" ", "");
                    if (parsedAbility.equals("BattleArmor") || parsedAbility.equals("ShellArmor")) {
                        parsedAbility = parsedAbility.replaceAll("o", "ou");
                    }
                    for (String ability : stats.abilities) {
                        if (!parsedAbility.equals(ability)) continue;
                        tempData.ability = parsedAbility;
                        break;
                    }
                    if (tempData.ability != null) continue;
                    return ABILITY_TEXT;
                }
                if (tempData.lvl == 0 && currentLine.startsWith(LEVEL_TEXT)) {
                    int level = Integer.parseInt(ImportExportConverter.getStringAfterColon(currentLine));
                    if (level > 0 && level <= PixelmonServerConfig.maxLevel) {
                        tempData.lvl = level;
                        continue;
                    }
                    return LEVEL_TEXT;
                }
                if (!tempData.isShiny && currentLine.startsWith(SHINY_TEXT)) {
                    String shinyText = ImportExportConverter.getStringAfterColon(currentLine);
                    if (SHINY_YES.equals(shinyText)) {
                        tempData.isShiny = true;
                        continue;
                    }
                    if (SHINY_NO.equals(shinyText)) continue;
                    return SHINY_TEXT;
                }
                if (currentLine.startsWith(SPECIAL_TEXTURE)) {
                    String texture = ImportExportConverter.getStringAfterColon(currentLine);
                    EnumSpecies pokemon = tempData.getSpecies();
                    tempData.specialTexture = (short)pokemon.getSpecialTextures(data.form).stream().filter(t -> t.name().equals(texture)).findFirst().orElse(EnumTextures.None).getId();
                    continue;
                }
                if (tempData.friendship == -1 && currentLine.startsWith(HAPPINESS_TEXT)) {
                    int friendship = Integer.parseInt(ImportExportConverter.getStringAfterColon(currentLine));
                    if (friendship >= 0 && friendship <= FriendShip.getMaxFriendship()) {
                        tempData.friendship = friendship;
                        continue;
                    }
                    return HAPPINESS_TEXT;
                }
                if (currentLine.startsWith(EV_TEXT)) {
                    ImportExportConverter.parseStats(currentLine, tempData.evs, null, (statValue, totalStats) -> {
                        int limitedStatValue = Math.max(0, Math.min(255, statValue));
                        return Math.min(limitedStatValue, 510 - totalStats);
                    });
                    continue;
                }
                if (tempData.nature == null && (currentLine.endsWith(NATURE_TEXT) || currentLine.endsWith("Nature  "))) {
                    String natureText = currentLine.substring(0, currentLine.indexOf(32));
                    tempData.nature = EnumNature.natureFromString(natureText);
                    continue;
                }
                if (currentLine.startsWith(IV_TEXT)) {
                    setIVs = true;
                    ImportExportConverter.parseStats(currentLine, tempData.ivs, tempData.caps, (statValue, totalStats) -> Math.max(0, Math.min(31, statValue)));
                    continue;
                }
                if (tempData.pokeball == null && currentLine.startsWith(POKE_BALL_TEXT)) {
                    try {
                        tempData.pokeball = EnumPokeball.valueOf(ImportExportConverter.getStringAfterColon(currentLine));
                        continue;
                    }
                    catch (IllegalArgumentException e) {
                        return POKE_BALL_TEXT;
                    }
                }
                if (tempData.growth == null && currentLine.startsWith(GROWTH_TEXT)) {
                    tempData.growth = EnumGrowth.growthFromString(ImportExportConverter.getStringAfterColon(currentLine));
                    if (tempData.growth != null) continue;
                    return GROWTH_TEXT;
                }
                if (currentLine.startsWith(CLONES_TEXT)) {
                    int numClones = Integer.parseInt(ImportExportConverter.getStringAfterColon(currentLine));
                    if (stats.pokemon == EnumSpecies.Mew && numClones >= 0 && numClones <= 3) {
                        tempData.numClones = numClones;
                        continue;
                    }
                    return CLONES_TEXT;
                }
                if (currentLine.startsWith(ENCHANTED_TEXT)) {
                    int numEnchanted = Integer.parseInt(ImportExportConverter.getStringAfterColon(currentLine));
                    if ((stats.pokemon == EnumSpecies.Azelf || stats.pokemon == EnumSpecies.Uxie || stats.pokemon == EnumSpecies.Mesprit) && numEnchanted >= 0 && numEnchanted <= LakeTrioStats.MAX_ENCHANTED) {
                        tempData.numEnchanted = numEnchanted;
                        continue;
                    }
                    return ENCHANTED_TEXT;
                }
                if (currentLine.startsWith(WORMHOLES_TEXT)) {
                    int numWormholes = Integer.parseInt(ImportExportConverter.getStringAfterColon(currentLine));
                    if ((stats.pokemon == EnumSpecies.Necrozma || stats.pokemon == EnumSpecies.Lunala || stats.pokemon == EnumSpecies.Solgaleo) && numWormholes >= 0 && numWormholes <= LightTrioStats.MAX_WORMHOLES) {
                        tempData.numWormholes = numWormholes;
                        continue;
                    }
                    return WORMHOLES_TEXT;
                }
                if (currentLine.startsWith(ABUNDANT_ACTIVATIONS_TEXT)) {
                    int numAbundantActivation = Integer.parseInt(ImportExportConverter.getStringAfterColon(currentLine));
                    if (stats.pokemon == EnumSpecies.Mew && numAbundantActivation >= 0 && numAbundantActivation <= MeloettaStats.MAX_ACTIVATIONS) {
                        tempData.numAbundantActivations = numAbundantActivation;
                        continue;
                    }
                    return ABUNDANT_ACTIVATIONS_TEXT;
                }
                if (currentLine.isEmpty() || currentLine.charAt(0) != '-' || (currentLine = currentLine.trim()).length() <= 1) continue;
                if (tempData.numMoves >= 4) {
                    return MOVE_TEXT;
                }
                String moveText = currentLine.substring(1).trim();
                if ((moveText = ImportExportConverter.convertName(moveText)).contains("Hidden Power")) {
                    if (!setIVs) {
                        String typeText = moveText.replace("Hidden Power ", "").replace("[", "").replace("]", "");
                        EnumType hiddenPowerType = EnumType.parseType(typeText);
                        if (hiddenPowerType == null) {
                            return MOVE_TEXT;
                        }
                        IVStore ivs = HiddenPower.getOptimalIVs(hiddenPowerType);
                        tempData.ivs = ivs.getArray();
                    }
                    moveText = "Hidden Power";
                }
                if (!Attack.hasAttack(moveText)) {
                    return MOVE_TEXT;
                }
                Attack move = new Attack(moveText);
                tempData.moveset[tempData.numMoves++] = PixelmonMovesetData.createPacket(move);
            }
            if (tempData.ability == null) {
                return ABILITY_TEXT;
            }
            if (tempData.numMoves == 0) {
                return MOVE_TEXT;
            }
            if (tempData.gender == null) {
                tempData.gender = Entity3HasStats.getRandomGender(stats);
            }
            if (tempData.lvl == 0) {
                tempData.lvl = PixelmonServerConfig.maxLevel;
            }
            if (tempData.friendship == -1) {
                tempData.friendship = FriendShip.getMaxFriendship();
            }
            if (tempData.nature == null) {
                tempData.nature = EnumNature.Hardy;
            }
            if (tempData.pokeball == null) {
                tempData.pokeball = EnumPokeball.PokeBall;
            }
            if (tempData.growth == null) {
                tempData.growth = EnumGrowth.Ordinary;
            }
        }
        catch (IndexOutOfBoundsException | NullPointerException | NumberFormatException | NoSuchElementException e) {
            return "";
        }
        data.nickname = tempData.nickname;
        data.name = tempData.name;
        data.nationalPokedexNumber = tempData.nationalPokedexNumber;
        data.gender = tempData.gender;
        data.heldItem = tempData.heldItem;
        data.ability = tempData.ability;
        data.lvl = tempData.lvl;
        data.isShiny = tempData.isShiny;
        data.specialTexture = tempData.specialTexture;
        data.friendship = tempData.friendship;
        for (i = 0; i < data.evs.length; ++i) {
            data.evs[i] = tempData.evs[i];
            data.ivs[i] = tempData.ivs[i];
        }
        data.nature = tempData.nature;
        data.pokeball = tempData.pokeball;
        data.growth = tempData.growth;
        data.numMoves = tempData.numMoves;
        for (i = 0; i < data.numMoves; ++i) {
            data.moveset[i] = tempData.moveset[i];
        }
        data.bossMode = EnumBossMode.NotBoss;
        data.form = tempData.form;
        data.numClones = tempData.numClones;
        data.numEnchanted = tempData.numEnchanted;
        data.numWormholes = tempData.numWormholes;
        Stats.calculateStatsForData(data);
        return null;
    }

    public static int getIntAfterColon(String string) {
        return Integer.parseInt(ImportExportConverter.getStringAfterColon(string));
    }

    public static String getStringAfterColon(String string) {
        return string.substring(string.indexOf(58) + 1).trim();
    }

    private static void parseStats(String statString, int[] statArray, boolean[] capArray, StatValidator validator) {
        String[] splitStats = ImportExportConverter.getStringAfterColon(statString).split("\\/");
        int totalStats = 0;
        for (String stat : splitStats) {
            stat = stat.trim();
            String statType = stat.substring(stat.lastIndexOf(32) + 1, stat.length());
            for (int i = 0; i < STAT_TEXT.length; ++i) {
                if (!STAT_TEXT[i].equalsIgnoreCase(statType)) continue;
                String statSubstring = stat.substring(0, stat.indexOf(32));
                if (capArray != null) {
                    capArray[i] = statSubstring.contains("*");
                    statSubstring = statSubstring.replace("*", "");
                }
                int statAmount = Integer.parseInt(statSubstring);
                statArray[i] = statAmount = validator.validateStat(statAmount, totalStats);
                totalStats += statAmount;
            }
        }
    }

    private static String convertName(String nameText) {
        importNameMap = null;
        if (importNameMap == null) {
            ImportExportConverter.initializeNameMap();
        }
        return importNameMap.containsKey(nameText) ? importNameMap.get(nameText) : nameText;
    }

    private static void initializeNameMap() {
        importNameMap = new HashMap<String, String>();
        importNameMap.put("Mime Jr.", "Mime_Jr.");
        importNameMap.put("Mr. Mime", "MrMime");
        importNameMap.put("Nidoran-F", "Nidoranfemale");
        importNameMap.put("Nidoran-M", "Nidoranmale");
        importNameMap.put("AncientPower", "Ancient Power");
        importNameMap.put("BubbleBeam", "Bubble Beam");
        importNameMap.put("DoubleSlap", "Double Slap");
        importNameMap.put("DragonBreath", "Dragon Breath");
        importNameMap.put("Dynamic Punch", "DynamicPunch");
        importNameMap.put("ExtremeSpeed", "Extreme Speed");
        importNameMap.put("FeatherDance", "Feather Dance");
        importNameMap.put("Feint Attack", "Faint Attack");
        importNameMap.put("GrassWhistle", "Grass Whistle");
        importNameMap.put("High Jump Kick", "Hi Jump Kick");
        importNameMap.put("Sand-Attack", "Sand Attack");
        importNameMap.put("Selfdestruct", "Self-Destruct");
        importNameMap.put("SmellingSalt", "Smelling Salts");
        importNameMap.put("SmokeScreen", "Smokescreen");
        importNameMap.put("Softboiled", "Soft-Boiled");
        importNameMap.put("SolarBeam", "Solar Beam");
        importNameMap.put("SonicBoom", "Sonic Boom");
        importNameMap.put("ThunderShock", "Thunder Shock");
        importNameMap.put("U-Turn", "U-turn");
        importNameMap.put("Tapu Koko", "TapuKoko");
        importNameMap.put("Tapu Lele", "TapuLele");
        importNameMap.put("Tapu Fini", "TapuFini");
        importNameMap.put("Tapu Bulu", "TapuBulu");
    }

    private static interface StatValidator {
        public int validateStat(int var1, int var2);
    }
}

