/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.pokemoneditor;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiIndividualEditorBase;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiPokemonEditorParty;
import com.pixelmongenerations.common.entity.pixelmon.stats.extraStats.LakeTrioStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.extraStats.LightTrioStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.extraStats.MeloettaStats;
import com.pixelmongenerations.common.item.ItemPokeball;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonItemsPokeballs;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.ChangePokemon;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.DeletePokemon;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.UpdatePlayerPokemon;
import java.io.IOException;
import java.util.List;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.translation.I18n;

public class GuiPokemonEditorIndividual
extends GuiIndividualEditorBase {
    private GuiTextField pokeBallText;
    private GuiTextField cloneText;
    private GuiTextField lakeText;
    private GuiTextField wormholeText;
    private GuiTextField abundantActivationText;
    private ItemStack pokeBall;

    GuiPokemonEditorIndividual(PixelmonData p, String titleText) {
        super(p, titleText);
    }

    @Override
    public void initGui() {
        super.initGui();
        this.pokeBallText = new GuiTextField(16, this.mc.fontRenderer, this.width / 2 - 120, this.height / 2 + 72, 90, 17);
        this.pokeBall = new ItemStack(this.p.pokeball.getItem());
        this.pokeBallText.setText(this.pokeBall.getDisplayName());
        this.textFields.add(this.pokeBallText);
        int formOffset = 20;
        if (this.formText != null) {
            this.formText.y += formOffset;
            this.textFields.remove(this.formText);
            this.textFields.add(this.formText);
            if (this.p.getSpecies() == EnumSpecies.Necrozma) {
                this.wormholeText = this.createExtraTextField(17);
                this.wormholeText.y += formOffset * 2;
                this.wormholeText.setText(Integer.toString(this.p.numWormholes));
                this.textFields.add(this.wormholeText);
            } else if (this.p.getSpecies() == EnumSpecies.Meloetta) {
                this.abundantActivationText = this.createExtraTextField(17);
                this.abundantActivationText.y += formOffset * 2;
                this.abundantActivationText.setText(Integer.toString(this.p.numAbundantActivations));
                this.textFields.add(this.abundantActivationText);
            }
        } else if (this.p.getSpecies() == EnumSpecies.Mew) {
            this.cloneText = this.createExtraTextField(17);
            this.cloneText.y += formOffset;
            this.cloneText.setText(Integer.toString(this.p.numClones));
            this.textFields.add(this.cloneText);
        } else if (this.p.getNationalPokedexNumber() <= 482 && this.p.getNationalPokedexNumber() >= 480) {
            this.lakeText = this.createExtraTextField(17);
            this.lakeText.y += formOffset;
            this.lakeText.setText(Integer.toString(this.p.numEnchanted));
            this.textFields.add(this.lakeText);
        } else if (this.p.getSpecies() == EnumSpecies.Lunala || this.p.getSpecies() == EnumSpecies.Solgaleo || this.p.getSpecies() == EnumSpecies.Necrozma) {
            this.wormholeText = this.createExtraTextField(17);
            this.wormholeText.y += formOffset;
            this.wormholeText.setText(Integer.toString(this.p.numWormholes));
            this.textFields.add(this.wormholeText);
        }
    }

    @Override
    protected void drawBackgroundUnderMenus(float f, int i, int j) {
        super.drawBackgroundUnderMenus(f, i, j);
        if (this.pokeBallText != null) {
            this.pokeBallText.drawTextBox();
            this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.pokemoneditor.pokeball"), this.width / 2 - 180, this.height / 2 + 75, 0);
            this.itemRender.renderItemAndEffectIntoGUI(this.pokeBall, this.width / 2 - 28, this.height / 2 + 72);
        }
        this.drawExtraText(this.cloneText, "gui.pokemoneditor.clones");
        this.drawExtraText(this.lakeText, "gui.pokemoneditor.lake_enchants");
        this.drawExtraText(this.wormholeText, "gui.pokemoneditor.num_wormholes");
        this.drawExtraText(this.abundantActivationText, "gui.pokemoneditor.num_abundant_activations");
    }

    @Override
    protected void keyTyped(char key, int keyCode) throws IOException {
        super.keyTyped(key, keyCode);
        if (this.pokeBallText != null) {
            this.pokeBallText.textboxKeyTyped(key, keyCode);
            if (this.pokeBallText.isFocused()) {
                this.updatePokeBall();
            }
        }
        if (this.cloneText != null) {
            this.cloneText.textboxKeyTyped(key, keyCode);
        }
        if (this.lakeText != null) {
            this.lakeText.textboxKeyTyped(key, keyCode);
        }
        if (this.wormholeText != null) {
            this.wormholeText.textboxKeyTyped(key, keyCode);
        }
        if (this.abundantActivationText != null) {
            this.abundantActivationText.textboxKeyTyped(key, keyCode);
        }
    }

    private void updatePokeBall() {
        Item item = PixelmonItems.getItemFromName(this.pokeBallText.getText());
        if (!(item instanceof ItemPokeball)) {
            item = PixelmonItemsPokeballs.pokeBall;
        }
        ItemPokeball newPokeBall = (ItemPokeball)item;
        this.pokeBall = new ItemStack(newPokeBall);
        this.p.pokeball = newPokeBall.type;
    }

    @Override
    protected void mouseClickedUnderMenus(int x, int y, int mouseButton) throws IOException {
        super.mouseClickedUnderMenus(x, y, mouseButton);
        for (GuiTextField textField : new GuiTextField[]{this.pokeBallText, this.cloneText, this.lakeText, this.wormholeText, this.abundantActivationText}) {
            if (textField == null) continue;
            textField.mouseClicked(x, y, mouseButton);
        }
    }

    @Override
    protected void changePokemon(EnumSpecies newPokemon) {
        Pixelmon.NETWORK.sendToServer(new ChangePokemon(this.p.order, newPokemon));
    }

    @Override
    protected void deletePokemon() {
        Pixelmon.NETWORK.sendToServer(new DeletePokemon(this.p.order));
        this.mc.displayGuiScreen(new GuiPokemonEditorParty());
    }

    @Override
    protected void saveAndClose() {
        Pixelmon.NETWORK.sendToServer(new UpdatePlayerPokemon(this.p));
        this.mc.displayGuiScreen(new GuiPokemonEditorParty());
    }

    @Override
    public List<PixelmonData> getPokemonList() {
        return ServerStorageDisplay.editedPokemon;
    }

    @Override
    protected boolean checkFields() {
        boolean valid = super.checkFields();
        if (this.cloneText != null) {
            try {
                int numClones = Integer.parseInt(this.cloneText.getText());
                if (numClones < 0) {
                    this.cloneText.setText("0");
                    valid = false;
                } else if (numClones > 3) {
                    this.cloneText.setText(Integer.toString(3));
                    valid = false;
                } else {
                    this.p.numClones = numClones;
                }
            }
            catch (NumberFormatException e) {
                this.cloneText.setText("");
                valid = false;
            }
        }
        if (this.lakeText != null) {
            try {
                int enchants = Integer.parseInt(this.lakeText.getText());
                if (enchants < 0) {
                    this.lakeText.setText("0");
                    valid = false;
                } else if (enchants > LakeTrioStats.MAX_ENCHANTED) {
                    this.lakeText.setText(Integer.toString(LakeTrioStats.MAX_ENCHANTED));
                    valid = false;
                } else {
                    this.p.numEnchanted = enchants;
                }
            }
            catch (NumberFormatException e) {
                this.lakeText.setText("");
                valid = false;
            }
        }
        if (this.wormholeText != null) {
            try {
                int wormholes = Integer.parseInt(this.wormholeText.getText());
                if (wormholes < 0) {
                    this.wormholeText.setText("0");
                    valid = false;
                } else if (wormholes > LightTrioStats.MAX_WORMHOLES) {
                    this.wormholeText.setText(Integer.toString(LightTrioStats.MAX_WORMHOLES));
                    valid = false;
                } else {
                    this.p.numWormholes = wormholes;
                }
            }
            catch (NumberFormatException e) {
                this.wormholeText.setText("");
                valid = false;
            }
        }
        if (this.abundantActivationText != null) {
            try {
                int abundantActivations = Integer.parseInt(this.abundantActivationText.getText());
                if (abundantActivations < 0) {
                    this.abundantActivationText.setText("0");
                    valid = false;
                } else if (abundantActivations > MeloettaStats.MAX_ACTIVATIONS) {
                    this.abundantActivationText.setText(Integer.toString(MeloettaStats.MAX_ACTIVATIONS));
                    valid = false;
                } else {
                    this.p.numAbundantActivations = abundantActivations;
                }
            }
            catch (NumberFormatException e) {
                this.abundantActivationText.setText("");
                valid = false;
            }
        }
        return valid;
    }
}

