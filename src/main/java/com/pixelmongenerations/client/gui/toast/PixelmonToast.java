/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.toast;

import com.pixelmongenerations.client.gui.toast.PixelmonFrameType;
import java.util.List;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.toasts.GuiToast;
import net.minecraft.client.gui.toasts.IToast;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;

public class PixelmonToast
implements IToast {
    ResourceLocation TEXTURE_TOASTS = new ResourceLocation("pixelmon:textures/gui/toasts.png");
    private String title;
    private String text;
    private ItemStack itemStack;
    private PixelmonFrameType frameType;
    private boolean hasPlayedSound = false;

    public PixelmonToast(String title, String text, ItemStack itemStack, PixelmonFrameType frameType) {
        this.title = title;
        this.text = text;
        this.itemStack = itemStack;
        this.frameType = frameType;
    }

    @Override
    public IToast.Visibility draw(GuiToast toastGui, long delta) {
        int i;
        toastGui.getMinecraft().getTextureManager().bindTexture(this.TEXTURE_TOASTS);
        GlStateManager.color(1.0f, 1.0f, 1.0f);
        toastGui.drawTexturedModalRect(0, 0, 0, this.frameType.getStartY(), 160, 32);
        List<String> list = toastGui.getMinecraft().fontRenderer.listFormattedStringToWidth(this.text, 125);
        int n = i = this.frameType == PixelmonFrameType.CHALLENGE ? 0xFF88FF : 0xFFFF00;
        if (list.size() == 1) {
            toastGui.getMinecraft().fontRenderer.drawString(this.title, 30, 7, i | 0xFF000000);
            toastGui.getMinecraft().fontRenderer.drawString(this.text, 30, 18, -1);
        } else {
            int j = 1500;
            float f = 300.0f;
            if (delta < 1500L) {
                int k = MathHelper.floor(MathHelper.clamp((float)(1500L - delta) / 300.0f, 0.0f, 1.0f) * 255.0f) << 24 | 0x4000000;
                toastGui.getMinecraft().fontRenderer.drawString(this.title, 30, 11, i | k);
            } else {
                int i1 = MathHelper.floor(MathHelper.clamp((float)(delta - 1500L) / 300.0f, 0.0f, 1.0f) * 252.0f) << 24 | 0x4000000;
                int l = 16 - list.size() * toastGui.getMinecraft().fontRenderer.FONT_HEIGHT / 2;
                for (String s : list) {
                    toastGui.getMinecraft().fontRenderer.drawString(s, 30, l, 0xFFFFFF | i1);
                    l += toastGui.getMinecraft().fontRenderer.FONT_HEIGHT;
                }
            }
        }
        if (!this.hasPlayedSound && delta > 0L) {
            this.hasPlayedSound = true;
            if (this.frameType == PixelmonFrameType.CHALLENGE) {
                toastGui.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getRecord(SoundEvents.UI_TOAST_CHALLENGE_COMPLETE, 1.0f, 1.0f));
            }
        }
        RenderHelper.enableGUIStandardItemLighting();
        toastGui.getMinecraft().getRenderItem().renderItemAndEffectIntoGUI(null, this.itemStack, 8, 8);
        return delta >= 5000L ? IToast.Visibility.HIDE : IToast.Visibility.SHOW;
    }
}

