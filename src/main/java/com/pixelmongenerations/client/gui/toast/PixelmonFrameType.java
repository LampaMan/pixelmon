/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.toast;

import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public enum PixelmonFrameType {
    TASK("task", 0, TextFormatting.GREEN, 0),
    CHALLENGE("challenge", 26, TextFormatting.DARK_PURPLE, 0),
    GOAL("goal", 52, TextFormatting.GREEN, 0),
    ANCIENT("ancient", 0, TextFormatting.GREEN, 128);

    private final String name;
    private final int icon;
    private final TextFormatting format;
    private final int startY;

    private PixelmonFrameType(String nameIn, int iconIn, TextFormatting formatIn, int startY) {
        this.name = nameIn;
        this.icon = iconIn;
        this.format = formatIn;
        this.startY = startY;
    }

    public String getName() {
        return this.name;
    }

    public static PixelmonFrameType byName(String nameIn) {
        for (PixelmonFrameType frametype : PixelmonFrameType.values()) {
            if (!frametype.name.equals(nameIn)) continue;
            return frametype;
        }
        throw new IllegalArgumentException("Unknown frame type '" + nameIn + "'");
    }

    @SideOnly(value=Side.CLIENT)
    public int getIcon() {
        return this.icon;
    }

    public TextFormatting getFormat() {
        return this.format;
    }

    public int getStartY() {
        return this.startY;
    }
}

