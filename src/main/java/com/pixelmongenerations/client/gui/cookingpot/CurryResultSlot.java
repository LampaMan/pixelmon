/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.cookingpot;

import com.pixelmongenerations.common.currydex.CurryDex;
import com.pixelmongenerations.common.item.ItemCurry;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class CurryResultSlot
extends SlotItemHandler {
    private int removeCount;

    public CurryResultSlot(IItemHandler handler, int slotIndex, int xPosition, int yPosition) {
        super(handler, slotIndex, xPosition, yPosition);
    }

    @Override
    public boolean isItemValid(ItemStack stack) {
        return false;
    }

    @Override
    public ItemStack onTake(EntityPlayer player, ItemStack stack) {
        this.onCrafting(stack);
        super.onTake(player, stack);
        return stack;
    }

    protected void onCrafting(EntityPlayer player, ItemStack stack) {
        stack.onCrafting(player.world, player, this.removeCount);
        if (!player.world.isRemote) {
            CurryDex.add(player, ItemCurry.getData(stack));
        }
        this.removeCount = 0;
    }
}

