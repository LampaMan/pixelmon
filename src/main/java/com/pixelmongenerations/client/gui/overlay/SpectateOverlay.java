/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.overlay;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.overlay.BaseOverlay;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.util.RegexPatterns;
import java.util.UUID;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.entity.player.EntityPlayer;

public class SpectateOverlay
extends BaseOverlay {
    private String messageName = null;
    private String oldMessageName = null;
    private UUID uuid;
    private UUID lastUuid;
    private int fadeInTicks = 0;
    private int fadeOutTicks = 0;

    @Override
    public void render(int screenWidth, int screenHeight, Minecraft mc, FontRenderer fontRenderer) {
        int c;
        float alpha;
        int width;
        String spectateString;
        if (this.messageName != null) {
            spectateString = RegexPatterns.$_L_VAR.matcher(RegexPatterns.$_P_VAR.matcher(I18n.format("gui.spectate.spectateMessage", new Object[0])).replaceAll(this.messageName)).replaceAll(GameSettings.getKeyDisplayString(ClientProxy.spectateKeyBind.getKeyCode()));
            width = fontRenderer.getStringWidth(spectateString);
            alpha = 1.0f - (float)this.fadeInTicks / 20.0f * 0.7f;
            c = GuiHelper.toColourValue(0.7f, 0.7f, 0.7f, alpha);
            fontRenderer.drawString(spectateString, screenWidth / 2 - width / 2, screenHeight / 2 + 30, c);
        }
        if (this.oldMessageName != null && this.fadeOutTicks > 0) {
            spectateString = RegexPatterns.$_L_VAR.matcher(RegexPatterns.$_P_VAR.matcher(I18n.format("gui.spectate.spectateMessage", new Object[0])).replaceAll(this.oldMessageName)).replaceAll(GameSettings.getKeyDisplayString(ClientProxy.spectateKeyBind.getKeyCode()));
            width = fontRenderer.getStringWidth(spectateString);
            alpha = (float)this.fadeOutTicks / 15.0f * 0.7f;
            c = GuiHelper.toColourValue(0.7f, 0.7f, 0.7f, alpha);
            fontRenderer.drawString(spectateString, screenWidth / 2 - width / 2, screenHeight / 2 + 30, c);
        }
    }

    public void showSpectateMessage(UUID playerUuid) {
        EntityPlayer player = Minecraft.getMinecraft().world.getPlayerEntityByUUID(playerUuid);
        if (player != null) {
            this.messageName = player.getDisplayNameString();
            this.uuid = playerUuid;
            this.fadeInTicks = 20;
        }
    }

    public void hideSpectateMessage(UUID playerUuid) {
        if (this.uuid == null) {
            this.fadeOutTicks = 0;
            this.messageName = null;
        } else {
            EntityPlayer player = Minecraft.getMinecraft().world.getPlayerEntityByUUID(this.uuid);
            if (player == null) {
                this.fadeOutTicks = 0;
                this.uuid = null;
                this.messageName = null;
            } else {
                this.fadeOutTicks = 15;
                this.oldMessageName = player.getDisplayNameString();
                this.lastUuid = this.uuid;
                if (this.uuid.equals(playerUuid)) {
                    this.messageName = null;
                    this.uuid = null;
                }
            }
        }
    }

    public void onPlayerTick() {
        if (this.fadeInTicks > 0) {
            --this.fadeInTicks;
        }
        if (this.fadeOutTicks > 0) {
            --this.fadeOutTicks;
        }
    }

    public UUID getCurrentSpectatingUUID() {
        if (this.uuid != null) {
            return this.uuid;
        }
        return this.lastUuid != null && this.fadeOutTicks > 0 ? this.lastUuid : null;
    }
}

