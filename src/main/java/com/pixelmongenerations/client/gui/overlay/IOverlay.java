/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.overlay;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;

public interface IOverlay {
    public boolean isActive();

    public void setActive(boolean var1);

    public boolean canUpdate();

    public void update();

    public void render(int var1, int var2, Minecraft var3, FontRenderer var4);
}

