/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.overlay;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.camera.GuiChattableCamera;
import com.pixelmongenerations.client.gui.GuiItemDrops;
import com.pixelmongenerations.client.gui.battles.rules.GuiBattleRulesPlayer;
import com.pixelmongenerations.client.gui.overlay.CameraOverlay;
import com.pixelmongenerations.client.gui.overlay.CurrentPokemonOverlay;
import com.pixelmongenerations.client.gui.overlay.IOverlay;
import com.pixelmongenerations.client.gui.overlay.IconsOverlay;
import com.pixelmongenerations.client.gui.overlay.OverlayType;
import com.pixelmongenerations.client.gui.overlay.PartyOverlay;
import com.pixelmongenerations.client.gui.overlay.PopupOverlay;
import com.pixelmongenerations.client.gui.overlay.ServerBarOverlay;
import com.pixelmongenerations.client.gui.overlay.SpectateOverlay;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiEditedPlayer;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.util.PixelmonMethods;
import java.util.HashMap;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.entity.Entity;
import net.minecraft.world.World;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class GuiPixelmonOverlay
extends Gui {
    private static HashMap<OverlayType, IOverlay> overlays;
    private FontRenderer fontRenderer;
    private static int count;
    public static boolean isVisible;

    public GuiPixelmonOverlay() {
        this.fontRenderer = Minecraft.getMinecraft().fontRenderer;
        overlays = new HashMap();
        overlays.put(OverlayType.CAMERA, new CameraOverlay());
        overlays.put(OverlayType.PARTY, new PartyOverlay());
        overlays.put(OverlayType.CURRENT_POKEMON, new CurrentPokemonOverlay());
        overlays.put(OverlayType.ICONS, new IconsOverlay());
        overlays.put(OverlayType.SPECTATE, new SpectateOverlay());
        overlays.put(OverlayType.POPUP, new PopupOverlay());
        overlays.put(OverlayType.BAR, new ServerBarOverlay());
    }

    public static IOverlay getOverlay(OverlayType type) {
        return overlays.get((Object)type);
    }

    public static void updateOverlays() {
        overlays.values().stream().filter(IOverlay::canUpdate).forEach(IOverlay::update);
    }

    @SubscribeEvent
    public void onRenderGameOverlay(RenderGameOverlayEvent.Pre event) {
        Minecraft mc = Minecraft.getMinecraft();
        if (count++ >= 100) {
            count = 0;
            this.checkEntitysInWorld(mc.world);
        }
        if (event.getType() == RenderGameOverlayEvent.ElementType.CHAT && (mc.currentScreen instanceof GuiChattableCamera || mc.currentScreen instanceof GuiEditedPlayer || mc.currentScreen instanceof GuiBattleRulesPlayer)) {
            event.setCanceled(true);
        }
        if (!(event.getType() != RenderGameOverlayEvent.ElementType.ALL || mc.currentScreen != null && !isVisible || mc.currentScreen instanceof GuiInventory || mc.gameSettings.hideGUI || mc.currentScreen instanceof GuiItemDrops)) {
            ScaledResolution scaledResolution = new ScaledResolution(mc);
            mc.entityRenderer.setupOverlayRendering();
            GlStateManager.enableBlend();
            GlStateManager.blendFunc(770, 771);
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            overlays.values().stream().filter(IOverlay::isActive).forEach(overlay -> overlay.render(scaledResolution.getScaledWidth(), scaledResolution.getScaledHeight(), mc, this.fontRenderer));
            this.fontRenderer.setUnicodeFlag(false);
            RenderHelper.disableStandardItemLighting();
            GlStateManager.disableLighting();
            GlStateManager.depthMask(true);
            GlStateManager.enableDepth();
        }
    }

    public void checkEntitysInWorld(World world) {
        PixelmonData[] party;
        PixelmonData[] var3 = party = ServerStorageDisplay.getPokemon();
        int var4 = party.length;
        for (int var5 = 0; var5 < var4; ++var5) {
            PixelmonData p = var3[var5];
            if (p == null) continue;
            p.outside = false;
        }
        for (int i = 0; i < world.loadedEntityList.size(); ++i) {
            int[] existingID;
            Entity e = world.loadedEntityList.get(i);
            if (!(e instanceof EntityPixelmon) || (existingID = ((EntityPixelmon)e).getPokemonId())[0] == -1) continue;
            PixelmonData[] var13 = party;
            int var7 = party.length;
            for (int var8 = 0; var8 < var7; ++var8) {
                PixelmonData p = var13[var8];
                if (p == null || !PixelmonMethods.isIDSame((EntityPixelmon)e, p.pokemonID)) continue;
                p.outside = true;
                p.outsideEntity = (EntityPixelmon)e;
            }
        }
    }

    static {
        count = 100;
        isVisible = true;
    }
}

