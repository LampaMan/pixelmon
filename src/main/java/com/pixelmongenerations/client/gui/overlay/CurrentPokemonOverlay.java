/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.overlay;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.overlay.BaseOverlay;
import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.client.gui.overlay.OverlayType;
import com.pixelmongenerations.client.gui.overlay.PartyOverlay;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.storage.playerData.ExternalMoveData;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;

public class CurrentPokemonOverlay
extends BaseOverlay {
    private PartyOverlay partyOverlay = (PartyOverlay)GuiPixelmonOverlay.getOverlay(OverlayType.PARTY);

    @Override
    public void render(int screenWidth, int screenHeight, Minecraft mc, FontRenderer fontRenderer) {
        PixelmonData poke = this.partyOverlay.getSelectedPokemon();
        fontRenderer.setUnicodeFlag(true);
        if (PixelmonConfig.showTarget && poke != null && poke.outside) {
            mc.renderEngine.bindTexture(GuiResources.targetArea);
            GuiHelper.drawImageQuad(screenWidth - 76, 2.0, 24.0, 24.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            fontRenderer.drawString(GameSettings.getKeyDisplayString(ClientProxy.actionKeyBind.getKeyCode()), screenWidth - 50, 0, 0xFFFFFF);
            if (poke.targetId != -1) {
                EntityLivingBase entity = (EntityLivingBase)mc.world.getEntityByID(poke.targetId);
                if (entity instanceof EntityPixelmon) {
                    EntityPixelmon pokemon = (EntityPixelmon)entity;
                    if (pokemon.baseStats == null) {
                        GuiHelper.bindPokemonSprite(1, false, mc);
                    } else {
                        GuiHelper.bindPokemonSprite(pokemon.getSpecies().getNationalPokedexInteger(), pokemon.getForm(), pokemon.getGender(), pokemon.getSpecialTextureIndex(), pokemon.isShiny(), mc);
                    }
                    GuiHelper.drawImageQuad(screenWidth - 75, 3.0, 20.0, 20.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
                } else if (entity instanceof EntityPlayer) {
                    mc.getRenderManager().renderEngine.bindTexture(((AbstractClientPlayer)entity).getLocationSkin());
                    GuiHelper.drawImageQuad(screenWidth - 73, 5.0, 18.0, 18.0f, 0.125, 0.125, 0.25, 0.25, this.zLevel);
                } else if (entity instanceof NPCTrainer) {
                    mc.getRenderManager().renderEngine.bindTexture(AbstractClientPlayer.getLocationSkin(""));
                    GuiHelper.drawImageQuad(screenWidth - 73, 5.0, 18.0, 18.0f, 0.125, 0.125, 0.25, 0.25, this.zLevel);
                }
            } else {
                mc.renderEngine.bindTexture(GuiResources.notarget);
                GuiHelper.drawImageQuad(screenWidth - 76, 2.0, 24.0, 24.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            }
            mc.renderEngine.bindTexture(GuiResources.targetAreaOver);
            GuiHelper.drawImageQuad(screenWidth - 76, 2.0, 24.0, 24.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        }
        if (poke != null && poke.outside && PixelmonConfig.allowExternalMoves) {
            ExternalMoveData[] moves = poke.getExternalMoves();
            short index = poke.selectedMoveIndex;
            if (moves != null) {
                if (moves.length > index) {
                    mc.renderEngine.bindTexture(GuiResources.targetArea);
                    GuiHelper.drawImageQuad(screenWidth - 42, 2.0, 24.0, 24.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
                    ExternalMoveData move = moves[index];
                    mc.renderEngine.bindTexture(move.getBaseExternalMove().getIcon());
                    GuiHelper.drawImageQuad(screenWidth - 42, 2.0, 24.0, 24.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
                    mc.renderEngine.bindTexture(GuiResources.targetAreaOver);
                    GuiHelper.drawImageQuad(screenWidth - 42, 2.0, 24.0, 24.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
                    double cooldown = move.getBaseExternalMove().getCooldown(poke);
                    if ((double)mc.world.getTotalWorldTime() < (double)move.timeLastUsed + cooldown) {
                        double percentLeft = mc.world.getTotalWorldTime() - move.timeLastUsed;
                        percentLeft = 1.0 - percentLeft / cooldown;
                        mc.renderEngine.bindTexture(GuiResources.cooldown);
                        int pixels = (int)(percentLeft * 24.0);
                        GuiHelper.drawImageQuad(screenWidth - 42, 26 - pixels, 24.0, pixels, 0.0, 1.0f - (float)pixels / 24.0f, 1.0, 1.0, this.zLevel);
                    }
                    fontRenderer.drawString(GameSettings.getKeyDisplayString(ClientProxy.externalKeyBind.getKeyCode()), screenWidth - 17, 0, 0xFFFFFF);
                    String nameString = "";
                    nameString = move.moveIndex == 5 ? I18n.format("externalMove.forage.name", new Object[0]) : (move.moveIndex == 99 ? I18n.format("externalMove.megaEvolution.name", new Object[0]) : poke.moveset[move.moveIndex].getAttack().baseAttack.getLocalizedName());
                    fontRenderer.drawString(nameString, screenWidth - 30 - fontRenderer.getStringWidth(nameString) / 2, 24, 0xFFFFFF);
                } else {
                    poke.selectedMoveIndex = 0;
                }
            }
        }
    }

    public void selectNextExternalMove() {
        PixelmonData current = this.partyOverlay.getSelectedPokemon();
        if (current != null) {
            short index;
            ExternalMoveData[] moves = current.getExternalMoves();
            index = moves.length > (index = current.selectedMoveIndex) + 1 ? (short)(index + 1) : (short)0;
            current.selectedMoveIndex = index;
        }
    }
}

