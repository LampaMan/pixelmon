/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.overlay;

import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.overlay.BaseOverlay;
import com.pixelmongenerations.core.proxy.ClientProxy;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.client.GuiIngameForge;

public class ServerBarOverlay
extends BaseOverlay {
    public static float progress = 0.0f;

    @Override
    public void render(int screenWidth, int screenHeight, Minecraft mc, FontRenderer fontRenderer) {
        TextureResource barTexture = ClientProxy.TEXTURE_STORE.getObject("bar_empty");
        if (barTexture != null) {
            TextureResource iconTexture;
            GuiIngameForge.renderFood = false;
            int left = screenWidth / 2 + 12;
            int top = screenHeight - 37;
            barTexture.bindTexture();
            GuiHelper.drawImageQuad(left, top, 79.0, 5.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            TextureResource barFilledTexture = ClientProxy.TEXTURE_STORE.getObject("bar_filled");
            if (barFilledTexture != null) {
                double width = MathHelper.clampedLerp(0.0, 79.0, progress);
                barFilledTexture.bindTexture();
                GuiHelper.drawImageQuad(left, top, width, 5.0f, 0.0, 0.0, progress, 1.0, this.zLevel);
            }
            if ((iconTexture = ClientProxy.TEXTURE_STORE.getObject("bar_icon")) != null) {
                iconTexture.bindTexture();
                GuiHelper.drawImageQuad(left - 7, top - 5, 12.0, 12.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            }
        }
    }
}

