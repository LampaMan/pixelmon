/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.overlay;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.overlay.BaseOverlay;
import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.client.gui.overlay.IOverlay;
import com.pixelmongenerations.client.gui.overlay.OverlayType;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.proxy.ClientProxy;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;

public class PartyOverlay
extends BaseOverlay {
    private IOverlay cameraOverlay = GuiPixelmonOverlay.getOverlay(OverlayType.CAMERA);
    private boolean minimized = false;
    private int selectedPixelmon;
    private int sideBarHeight = 200;

    @Override
    public boolean isActive() {
        return !this.cameraOverlay.isActive();
    }

    public void toggleMinimized() {
        this.minimized = !this.minimized;
    }

    @Override
    public void render(int screenWidth, int screenHeight, Minecraft mc, FontRenderer fontRenderer) {
        int topSideBar = screenHeight / 2 - this.sideBarHeight / 2;
        int topOffset = topSideBar + 5;
        float slotHeight = 30.0f;
        mc.renderEngine.bindTexture(GuiResources.dock);
        this.zLevel = -90.0f;
        GuiHelper.drawImageQuad(0.0, topSideBar, 22.0, 203.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        int leftText = 30;
        fontRenderer.setUnicodeFlag(true);
        int i = 0;
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.checkSelection();
        PixelmonData[] party = ServerStorageDisplay.getPokemon();
        for (int scaledWidth = 0; scaledWidth < party.length; ++scaledWidth) {
            PixelmonData p = party[scaledWidth];
            int offset = 0;
            int yPos = topOffset + (int)((float)i * slotHeight) + 9 + offset;
            if (p != null) {
                if (!this.minimized) {
                    mc.renderEngine.bindTexture(GuiResources.textbox);
                    GuiHelper.drawImageQuad(leftText - 28, yPos - 10, 123.0, 34.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
                    float textureX = -1.0f;
                    float textureY = -1.0f;
                    if (p.status != null) {
                        float[] texturePair = StatusType.getTexturePos(p.status);
                        textureX = texturePair[0];
                        textureY = texturePair[1];
                    }
                    if (textureX != -1.0f && p.health > 0) {
                        mc.renderEngine.bindTexture(GuiResources.status);
                        GuiHelper.drawImageQuad(leftText + 46, yPos + 1, 14.72f, 6.44f, textureX / 299.0f, textureY / 210.0f, (textureX + 147.0f) / 299.0f, (textureY + 68.0f) / 210.0f, this.zLevel);
                    }
                }
                String displayName = p.getNickname();
                i = p.order;
                if (!this.minimized) {
                    fontRenderer.drawString(displayName, leftText - 2, yPos, 0xFFFFFF);
                    GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
                    if (p.gender == Gender.Male && !p.isEgg) {
                        mc.renderEngine.bindTexture(GuiResources.male);
                        GuiHelper.drawImageQuad(fontRenderer.getStringWidth(displayName) + leftText - 1, yPos, 5.0, 8.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
                    } else if (p.gender == Gender.Female && !p.isEgg) {
                        mc.renderEngine.bindTexture(GuiResources.female);
                        GuiHelper.drawImageQuad(fontRenderer.getStringWidth(displayName) + leftText - 1, yPos, 5.0, 8.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
                    }
                    int x = 1;
                    int icons = 0;
                    if (p.hasGmaxFactor) {
                        mc.renderEngine.bindTexture(GuiResources.gmaxIcon);
                        GuiHelper.drawImageQuad(fontRenderer.getStringWidth(displayName) + leftText - 1 + 6 + x, yPos, 10.0, 10.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
                        x += 9;
                        ++icons;
                    }
                    if (p.isShiny) {
                        mc.renderEngine.bindTexture(GuiResources.shiny);
                        GuiHelper.drawImageQuad(fontRenderer.getStringWidth(displayName) + leftText - 1 + 6 + x, yPos, 10.0, 10.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
                        x += 8;
                        ++icons;
                    }
                    if (p.customTexture != null && !p.customTexture.isEmpty() || p.specialTexture != 0 && icons < 2) {
                        mc.renderEngine.bindTexture(GuiResources.st);
                        GuiHelper.drawImageQuad(fontRenderer.getStringWidth(displayName) + leftText - 1 + 6 + x, yPos, 10.0, 10.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
                    }
                }
                Minecraft.getMinecraft().renderEngine.bindTexture(GuiResources.pokeball(p.pokeball));
                GuiHelper.drawImageQuad(-3.0, yPos - 7, 32.0, 32.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
                if (i == this.selectedPixelmon) {
                    if (p.isFainted) {
                        mc.renderEngine.bindTexture(GuiResources.faintedSelected);
                    } else if (p.outside) {
                        mc.renderEngine.bindTexture(GuiResources.releasedSelected);
                    } else {
                        mc.renderEngine.bindTexture(GuiResources.selected);
                    }
                } else if (p.isFainted) {
                    mc.renderEngine.bindTexture(GuiResources.fainted);
                } else if (p.outside) {
                    mc.renderEngine.bindTexture(GuiResources.released);
                } else {
                    mc.renderEngine.bindTexture(GuiResources.normal);
                }
                GuiHelper.drawImageQuad(-3.0, yPos - 7, 32.0, 32.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
                this.drawPokemonSprite(p, yPos, mc);
                if (!p.heldItem.isEmpty()) {
                    mc.renderEngine.bindTexture(GuiResources.heldItem);
                    GuiHelper.drawImageQuad(18.0, yPos + 13, 6.0, 6.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
                }
                if (!this.minimized && !p.isEgg) {
                    String levelString = I18n.format("gui.screenpokechecker.lvl", new Object[0]) + " " + p.lvl;
                    fontRenderer.drawString(levelString, leftText - 1, yPos + 1 + fontRenderer.FONT_HEIGHT, 0xFFFFFF);
                    if (p.health <= 0) {
                        fontRenderer.drawString(I18n.format("gui.creativeinv.fainted", new Object[0]), leftText + 1 + fontRenderer.getStringWidth(levelString), yPos + 1 + fontRenderer.FONT_HEIGHT, 0xFFFFFF);
                    } else {
                        fontRenderer.drawString(I18n.format("nbt.hp.name", new Object[0]) + " " + p.health + "/" + p.hp, leftText + 2 + fontRenderer.getStringWidth(levelString), yPos + 1 + fontRenderer.FONT_HEIGHT, 0xFFFFFF);
                    }
                }
            } else {
                mc.renderEngine.bindTexture(GuiResources.available);
                GuiHelper.drawImageQuad(5.0, yPos + 1, 16.0, 16.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            }
            ++i;
        }
    }

    private void drawPokemonSprite(PixelmonData p, int yPos, Minecraft mc) {
        GuiHelper.bindPokemonSprite(p, mc);
        GuiHelper.drawImageQuad(1.0, yPos - 3 - (p.isGen6Sprite() ? 0 : 3), 24.0, 24.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
    }

    public void selectNextPixelmon() {
        if (ServerStorageDisplay.count() != 0 && ServerStorageDisplay.countNonEgg() != 0) {
            ++this.selectedPixelmon;
            if (this.selectedPixelmon >= 6) {
                this.selectedPixelmon = 0;
            }
            PixelmonData[] party = ServerStorageDisplay.getPokemon();
            while (party[this.selectedPixelmon] == null || party[this.selectedPixelmon].isEgg) {
                ++this.selectedPixelmon;
                if (this.selectedPixelmon < 6) continue;
                this.selectedPixelmon = 0;
            }
        }
    }

    public void selectPreviousPixelmon() {
        if (ServerStorageDisplay.count() != 0 && ServerStorageDisplay.countNonEgg() != 0) {
            --this.selectedPixelmon;
            if (this.selectedPixelmon < 0) {
                this.selectedPixelmon = 5;
            }
            PixelmonData[] party = ServerStorageDisplay.getPokemon();
            while (party[this.selectedPixelmon] == null || party[this.selectedPixelmon].isEgg) {
                --this.selectedPixelmon;
                if (this.selectedPixelmon >= 0) continue;
                this.selectedPixelmon = 5;
            }
        }
    }

    public void checkSelection() {
        PixelmonData current = ServerStorageDisplay.getPokemon()[this.selectedPixelmon];
        if ((current == null || current.isEgg) && !ClientProxy.battleManager.isBattling()) {
            this.selectPreviousPixelmon();
        }
    }

    public PixelmonData getSelectedPokemon() {
        return ServerStorageDisplay.getPokemon()[this.selectedPixelmon];
    }
}

