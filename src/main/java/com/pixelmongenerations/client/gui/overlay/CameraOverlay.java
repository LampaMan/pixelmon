/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.overlay;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.overlay.BaseOverlay;
import com.pixelmongenerations.common.item.ItemCamera;
import com.pixelmongenerations.core.config.PixelmonItems;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraftforge.client.GuiIngameForge;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class CameraOverlay
extends BaseOverlay {
    @Override
    public void render(int screenWidth, int screenHeight, Minecraft mc, FontRenderer fontRenderer) {
        this.checkForDisable();
        if (!this.isActive()) {
            return;
        }
        float zLevel = -90.0f;
        Minecraft.getMinecraft().renderEngine.bindTexture(GuiResources.cameraOverlay);
        GuiHelper.drawImageQuad(0.0, 0.0, screenWidth, screenHeight, 0.0, 0.0, 1.0, 1.0, zLevel);
        Minecraft.getMinecraft().renderEngine.bindTexture(GuiResources.cameraControls);
        int controlsWidth = 57;
        int controlsHeight = 80;
        GuiHelper.drawImageQuad(screenWidth / 2 - controlsWidth / 2, screenHeight / 2 - controlsHeight / 2, controlsWidth, controlsHeight, 0.0, 0.0, 1.0, 1.0, zLevel);
        EntityPlayerSP player = mc.player;
        if (player.inventory.hasItemStack(new ItemStack(PixelmonItems.filmItem))) {
            int filmInInv = 0;
            for (int i = 0; i < player.inventory.getSizeInventory(); ++i) {
                ItemStack stackInSlot = player.inventory.getStackInSlot(i);
                if (stackInSlot.isEmpty() || stackInSlot.getItem() != PixelmonItems.filmItem) continue;
                filmInInv += stackInSlot.getCount();
            }
            ScaledResolution scaledResolution = new ScaledResolution(mc);
            int x = scaledResolution.getScaledWidth() - scaledResolution.getScaledWidth() / 4;
            int y = scaledResolution.getScaledHeight() / 4;
            mc.getRenderItem().renderItemAndEffectIntoGUI(new ItemStack(PixelmonItems.filmItem), x, y);
            mc.fontRenderer.drawString("" + filmInInv, x + 16, y, 0xFFFFFF, false);
        }
    }

    @SubscribeEvent
    public void renderHandEvent(RenderHandEvent event) {
        if (!this.isActive()) {
            event.setCanceled(true);
        }
    }

    private void checkForDisable() {
        ItemStack heldItemStack = Minecraft.getMinecraft().player.getHeldItem(EnumHand.MAIN_HAND);
        if (heldItemStack.isEmpty() || !(heldItemStack.getItem() instanceof ItemCamera)) {
            this.setActive(false);
        }
        this.updateForgeGui(this.isActive());
    }

    private void updateForgeGui(boolean showUi) {
        GuiIngameForge.renderHotbar = showUi;
        GuiIngameForge.renderCrosshairs = showUi;
        GuiIngameForge.renderExperiance = showUi;
        GuiIngameForge.renderArmor = showUi;
    }
}

