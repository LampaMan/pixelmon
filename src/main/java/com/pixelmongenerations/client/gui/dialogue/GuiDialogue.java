/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.client.gui.dialogue;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.dialogue.Dialogue;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.elements.GuiRoundButton;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.dialogue.DialogueChoiceMade;
import com.pixelmongenerations.core.network.packetHandlers.dialogue.DialogueClosure;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;

public class GuiDialogue
extends GuiContainer {
    private static volatile List<Dialogue> dialogues;
    private List<GuiRoundButton> choiceButtons = new ArrayList<GuiRoundButton>();
    private int buttonWidth;
    private Dialogue currentDialogue = dialogues.get(0);
    private boolean pause = false;

    public GuiDialogue() {
        super(new ContainerEmpty());
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        if (this.currentDialogue == null) {
            return;
        }
        if (this.choiceButtons.isEmpty() && !this.currentDialogue.choices.isEmpty()) {
            this.loadButtons();
        }
        GuiHelper.drawChatBox(this, this.currentDialogue.name, Lists.newArrayList(this.currentDialogue.text), 0.0f);
        for (int i = 0; i < this.choiceButtons.size(); ++i) {
            this.choiceButtons.get(i).drawButton(this.getButtonX(i), this.getButtonY(i), mouseX, mouseY, 0.0f);
        }
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        if (this.pause) {
            return;
        }
        if (this.currentDialogue.choices.isEmpty()) {
            GuiDialogue.removeImmediateDialogue();
            this.next();
            if (this.currentDialogue != null) {
                this.loadButtons();
            }
            return;
        }
        for (int i = 0; i < this.choiceButtons.size(); ++i) {
            if (!this.choiceButtons.get(i).isMouseOver(this.getButtonX(i), this.getButtonY(i), mouseX, mouseY)) continue;
            this.pause = true;
            Pixelmon.NETWORK.sendToServer(new DialogueChoiceMade(this.currentDialogue.choices.get(i)));
            break;
        }
    }

    public void close() {
        Minecraft.getMinecraft().player.closeScreen();
        Pixelmon.NETWORK.sendToServer(new DialogueClosure());
    }

    public void next() {
        this.pause = false;
        if (dialogues.isEmpty()) {
            this.close();
        } else {
            this.currentDialogue = dialogues.get(0);
            this.choiceButtons.clear();
        }
    }

    public static void removeImmediateDialogue() {
        if (!dialogues.isEmpty()) {
            dialogues.remove(0);
        }
    }

    public static void addDialogues(List<Dialogue> dialogues) {
        GuiDialogue.dialogues.addAll(dialogues);
    }

    public static void insertDialogues(List<Dialogue> dialogues) {
        dialogues.addAll(GuiDialogue.dialogues);
        GuiDialogue.dialogues = dialogues;
    }

    public static void setDialogues(List<Dialogue> dialogues) {
        GuiDialogue.dialogues = dialogues;
    }

    private void loadButtons() {
        this.choiceButtons.clear();
        this.buttonWidth = this.getLargestWidth() + 20;
        for (int i = 0; i < this.currentDialogue.choices.size(); ++i) {
            this.choiceButtons.add(new GuiRoundButton(0, 65, this.currentDialogue.choices.get((int)i).text, this.buttonWidth, 20));
        }
    }

    private int getLargestWidth() {
        int max = 0;
        for (int i = 0; i < this.currentDialogue.choices.size(); ++i) {
            if (this.mc.fontRenderer.getStringWidth(this.currentDialogue.choices.get((int)i).text) <= max) continue;
            max = this.mc.fontRenderer.getStringWidth(this.currentDialogue.choices.get((int)i).text);
        }
        return max;
    }

    private int getButtonX(int buttonNum) {
        return this.width / 2 - (this.buttonWidth * this.currentDialogue.column + 4 * (this.currentDialogue.column - 1)) / 2 + (this.buttonWidth + 4) * (buttonNum % this.currentDialogue.column);
    }

    private int getButtonY(int buttonNum) {
        return this.guiTop + 24 * (buttonNum / this.currentDialogue.column);
    }
}

