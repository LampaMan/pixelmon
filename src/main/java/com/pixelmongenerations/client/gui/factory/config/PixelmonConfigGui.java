/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.client.gui.factory.config;

import com.google.common.collect.Lists;
import com.pixelmongenerations.client.gui.factory.config.ConfigurateNodeElement;
import com.pixelmongenerations.core.config.PixelmonConfig;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.client.config.DummyConfigElement;
import net.minecraftforge.fml.client.config.GuiConfig;
import net.minecraftforge.fml.client.config.GuiConfigEntries;
import net.minecraftforge.fml.client.config.IConfigElement;

public class PixelmonConfigGui
extends GuiConfig {
    public PixelmonConfigGui(GuiScreen parentScreen) {
        super(parentScreen, PixelmonConfigGui.getConfigElements(), "pixelmon", false, false, I18n.translateToLocal("pixelmon.config.title"));
    }

    private static List<IConfigElement> getConfigElements() {
        ArrayList<IConfigElement> list = new ArrayList<IConfigElement>();
        list.add(new DummyConfigElement.DummyCategoryElement("General", "pixelmon.config.general", GeneralEntry.class));
        list.add(new DummyConfigElement.DummyCategoryElement("AFK Handler", "pixelmon.config.afkhandler", AFKEntry.class));
        list.add(new DummyConfigElement.DummyCategoryElement("Starter", "pixelmon.config.starter", StarterEntry.class));
        list.add(new DummyConfigElement.DummyCategoryElement("Breeding", "pixelmon.config.breeding", BreedingEntry.class));
        list.add(new DummyConfigElement.DummyCategoryElement("Elevator", "pixelmon.config.elevator", ElevatorEntry.class));
        list.add(new DummyConfigElement.DummyCategoryElement("External Moves", "pixelmon.config.externalmoves", ExternalMovesEntry.class));
        list.add(new DummyConfigElement.DummyCategoryElement("Graphics", "pixelmon.config.graphics", GraphicsEntry.class));
        list.add(new DummyConfigElement.DummyCategoryElement("Pixel Utilities", "pixelmon.config.pixelutilities", PixelUtilsEntry.class));
        list.add(new DummyConfigElement.DummyCategoryElement("PokeLoot", "pixelmon.config.pokeloot", PokeLootEntry.class));
        list.add(new DummyConfigElement.DummyCategoryElement("Spawning", "pixelmon.config.spawning", SpawningEntry.class));
        list.add(new DummyConfigElement.DummyCategoryElement("Better Spawning", "pixelmon.config.betterspawning", BetterSpawningEntry.class));
        list.add(new DummyConfigElement.DummyCategoryElement("Economy", "pixelmon.config.economy", EconomyEntry.class));
        list.add(new DummyConfigElement.DummyCategoryElement("Performance", "pixelmon.config.performance", PerformanceEntry.class));
        list.add(new DummyConfigElement.DummyCategoryElement("Shiny Rates", "pixelmon.config.shinyrates", ShinyRatesEntry.class));
        list.add(new DummyConfigElement.DummyCategoryElement("Shrine", "pixelmon.config.shrine", ShrineEntry.class));
        list.add(new DummyConfigElement.DummyCategoryElement("Storage", "pixelmon.config.storage", StorageEntry.class));
        list.add(new DummyConfigElement.DummyCategoryElement("Timing", "pixelmon.config.timing", TimingEntry.class));
        list.add(new DummyConfigElement.DummyCategoryElement("World Gen", "pixelmon.config.worldgen", WorldGenEntry.class));
        return list;
    }

    public static class WorldGenEntry
    extends PixelmonCategoryEntry {
        public WorldGenEntry(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
            super(owningScreen, owningEntryList, configElement);
        }

        @Override
        protected GuiScreen buildChildScreen() {
            ArrayList configElements = Lists.newArrayList();
            CommentedConfigurationNode parentNode = PixelmonConfig.getConfig().getNode("World Gen");
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("pokeChestBiomeBlackList"), Collections.singletonList("oceans")));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("pokeChestBiomeWhiteList"), Collections.singletonList("all")));
            return new GuiConfig((GuiScreen)this.owningScreen, (List<IConfigElement>)configElements, this.owningScreen.modID, "Performance", this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart, this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart, I18n.translateToLocal("pixelmon.config.worldgen.tooltip"));
        }
    }

    public static class TimingEntry
    extends PixelmonCategoryEntry {
        public TimingEntry(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
            super(owningScreen, owningEntryList, configElement);
        }

        @Override
        protected GuiScreen buildChildScreen() {
            ArrayList configElements = Lists.newArrayList();
            CommentedConfigurationNode parentNode = PixelmonConfig.getConfig().getNode("Timing");
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("furRegrowth"), 600000).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("hoopaUnbound"), 600000).setNonNegative());
            return new GuiConfig((GuiScreen)this.owningScreen, (List<IConfigElement>)configElements, this.owningScreen.modID, "Performance", this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart, this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart, I18n.translateToLocal("pixelmon.config.timing.tooltip"));
        }
    }

    public static class StorageEntry
    extends PixelmonCategoryEntry {
        public StorageEntry(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
            super(owningScreen, owningEntryList, configElement);
        }

        @Override
        protected GuiScreen buildChildScreen() {
            ArrayList configElements = Lists.newArrayList();
            CommentedConfigurationNode parentNode = PixelmonConfig.getConfig().getNode("Storage");
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("asyncInterval"), 60).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("useAsyncSaving"), false));
            return new GuiConfig((GuiScreen)this.owningScreen, (List<IConfigElement>)configElements, this.owningScreen.modID, "Performance", this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart, this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart, I18n.translateToLocal("pixelmon.config.storage.tooltip"));
        }
    }

    public static class ShrineEntry
    extends PixelmonCategoryEntry {
        public ShrineEntry(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
            super(owningScreen, owningEntryList, configElement);
        }

        @Override
        protected GuiScreen buildChildScreen() {
            ArrayList configElements = Lists.newArrayList();
            CommentedConfigurationNode parentNode = PixelmonConfig.getConfig().getNode("Shrine");
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("limitHoOh"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("limitMew"), 3).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("limitRayquaza"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("limitRegigigas"), false));
            return new GuiConfig((GuiScreen)this.owningScreen, (List<IConfigElement>)configElements, this.owningScreen.modID, "Performance", this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart, this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart, I18n.translateToLocal("pixelmon.config.shrine.tooltip"));
        }
    }

    public static class ShinyRatesEntry
    extends PixelmonCategoryEntry {
        public ShinyRatesEntry(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
            super(owningScreen, owningEntryList, configElement);
        }

        @Override
        protected GuiScreen buildChildScreen() {
            ArrayList configElements = Lists.newArrayList();
            CommentedConfigurationNode parentNode = PixelmonConfig.getConfig().getNode("Shiny Rates");
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("catchComboTier1"), 1728).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("catchComboTier1Charm"), 1296).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("catchComboTier2"), 1296).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("catchComboTier2Charm"), 972).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("catchComboTier3"), 972).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("catchComboTier3Charm"), 729).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("enableCatchComboShinyLock"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("enableCatchCombos"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("shinyRateWithCharm"), 3072).setNonNegative());
            return new GuiConfig((GuiScreen)this.owningScreen, (List<IConfigElement>)configElements, this.owningScreen.modID, "Performance", this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart, this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart, I18n.translateToLocal("pixelmon.config.shinyrates.tooltip"));
        }
    }

    public static class PerformanceEntry
    extends PixelmonCategoryEntry {
        public PerformanceEntry(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
            super(owningScreen, owningEntryList, configElement);
        }

        @Override
        protected GuiScreen buildChildScreen() {
            ArrayList configElements = Lists.newArrayList();
            CommentedConfigurationNode parentNode = PixelmonConfig.getConfig().getNode("Performance");
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("tickRate24Radius"), 1).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("tickRate48Radius"), 2).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("tickRateHigherRadius"), 3).setNonNegative());
            return new GuiConfig((GuiScreen)this.owningScreen, (List<IConfigElement>)configElements, this.owningScreen.modID, "Performance", this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart, this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart, I18n.translateToLocal("pixelmon.config.performance.tooltip"));
        }
    }

    public static class EconomyEntry
    extends PixelmonCategoryEntry {
        public EconomyEntry(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
            super(owningScreen, owningEntryList, configElement);
        }

        @Override
        protected GuiScreen buildChildScreen() {
            ArrayList configElements = Lists.newArrayList();
            CommentedConfigurationNode parentNode = PixelmonConfig.getConfig().getNode("Economy");
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("amuletCoinMultiplier"), Float.valueOf(2.0f)));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("happyHourMultiplier"), Float.valueOf(2.0f)));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("payDayMultiplier"), Float.valueOf(5.0f)));
            return new GuiConfig((GuiScreen)this.owningScreen, (List<IConfigElement>)configElements, this.owningScreen.modID, "Economy", this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart, this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart, I18n.translateToLocal("pixelmon.config.economy.tooltip"));
        }
    }

    public static class BetterSpawningEntry
    extends PixelmonCategoryEntry {
        public BetterSpawningEntry(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
            super(owningScreen, owningEntryList, configElement);
        }

        @Override
        protected GuiScreen buildChildScreen() {
            ArrayList configElements = Lists.newArrayList();
            CommentedConfigurationNode parentNode = PixelmonConfig.getConfig().getNode("Better Spawning");
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("spawnSetFolder"), "default"));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("entitiesPerPlayer"), 40).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("spawnsPerPass"), 2).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("spawnFrequency"), 60).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("minimumDistanceBetweenSpawns"), 8).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("minimumDistanceFromCentre"), 8).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("maximumDistanceFromCentre"), 64).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("horizontalTrackFactor"), Float.valueOf(100.0f)).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("verticalTrackFactor"), Float.valueOf(0.0f)).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("horizontalSliceRadius"), 16).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("verticalSliceRadius"), 16).setNonNegative());
            return new GuiConfig((GuiScreen)this.owningScreen, (List<IConfigElement>)configElements, this.owningScreen.modID, "Better Spawning", this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart, this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart, I18n.translateToLocal("pixelmon.config.betterspawning.tooltip"));
        }
    }

    public static class ExternalMovesEntry
    extends PixelmonCategoryEntry {
        public ExternalMovesEntry(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
            super(owningScreen, owningEntryList, configElement);
        }

        @Override
        protected GuiScreen buildChildScreen() {
            ArrayList configElements = Lists.newArrayList();
            CommentedConfigurationNode parentNode = PixelmonConfig.getConfig().getNode("ExternalMoves");
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("allowExternalMoves"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("allowDestructiveExternalMoves"), true));
            return new GuiConfig((GuiScreen)this.owningScreen, (List<IConfigElement>)configElements, this.owningScreen.modID, "External Moves", this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart, this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart, I18n.translateToLocal("pixelmon.config.externalmoves.tooltip"));
        }
    }

    public static class ElevatorEntry
    extends PixelmonCategoryEntry {
        public ElevatorEntry(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
            super(owningScreen, owningEntryList, configElement);
        }

        @Override
        protected GuiScreen buildChildScreen() {
            ArrayList configElements = Lists.newArrayList();
            CommentedConfigurationNode parentNode = PixelmonConfig.getConfig().getNode("Elevator");
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("elevatorSearchRange"), 10).setBounds(1, 255).setSlider());
            return new GuiConfig((GuiScreen)this.owningScreen, (List<IConfigElement>)configElements, this.owningScreen.modID, "Elevator", this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart, this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart, I18n.translateToLocal("pixelmon.config.elevator.tooltip"));
        }
    }

    public static class StarterEntry
    extends PixelmonCategoryEntry {
        public StarterEntry(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
            super(owningScreen, owningEntryList, configElement);
        }

        @Override
        protected GuiScreen buildChildScreen() {
            ArrayList configElements = Lists.newArrayList();
            CommentedConfigurationNode parentNode = PixelmonConfig.getConfig().getNode("Starters");
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("level"), 5).setBounds(1, 100).setSlider());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("shiny"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("starterList"), Lists.newArrayList((Object[])new String[]{"Bulbasaur", "Squirtle", "Charmander", "Chikorita", "Totodile", "Cyndaquil", "Treecko", "Mudkip", "Torchic", "Turtwig", "Piplup", "Chimchar", "Snivy", "Oshawott", "Tepig", "Chespin", "Froakie", "Fennekin"})));
            return new GuiConfig((GuiScreen)this.owningScreen, (List<IConfigElement>)configElements, this.owningScreen.modID, "Starters", this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart, this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart, I18n.translateToLocal("pixelmon.config.starter.tooltip"));
        }
    }

    public static class BreedingEntry
    extends PixelmonCategoryEntry {
        public BreedingEntry(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
            super(owningScreen, owningEntryList, configElement);
        }

        @Override
        protected GuiScreen buildChildScreen() {
            ArrayList configElements = Lists.newArrayList();
            CommentedConfigurationNode parentNode = PixelmonConfig.getConfig().getNode("Breeding");
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("allowBreeding"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("allowDittoDittoBreeding"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("allowRandomBreedingEggsToBeLegendary"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("allowRandomSpawnedEggsToBeLegendary"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("useBreedingEnvironment"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("breedingEnvironmentCheckSeconds"), 500).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("stepsPerEggCycle"), 255).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("numBreedingStages"), 5).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("breedingTicks"), 18000).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("allowRanchExpansion"), true).setRequiresRestart());
            return new GuiConfig((GuiScreen)this.owningScreen, (List<IConfigElement>)configElements, this.owningScreen.modID, "Breeding", this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart, this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart, I18n.translateToLocal("pixelmon.config.breeding.tooltip"));
        }
    }

    public static class AFKEntry
    extends PixelmonCategoryEntry {
        public AFKEntry(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
            super(owningScreen, owningEntryList, configElement);
        }

        @Override
        protected GuiScreen buildChildScreen() {
            ArrayList configElements = Lists.newArrayList();
            CommentedConfigurationNode parentNode = PixelmonConfig.getConfig().getNode("AFKHandler");
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("enableAFKHandler"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("afkActivateSeconds"), 90).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("afkHandlerTurnSeconds"), 15).setNonNegative());
            return new GuiConfig((GuiScreen)this.owningScreen, (List<IConfigElement>)configElements, this.owningScreen.modID, "AFKHandler", this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart, this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart, I18n.translateToLocal("pixelmon.config.afkhandler.tooltip"));
        }
    }

    public static class GeneralEntry
    extends PixelmonCategoryEntry {
        public GeneralEntry(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
            super(owningScreen, owningEntryList, configElement);
        }

        @Override
        protected GuiScreen buildChildScreen() {
            ArrayList configElements = Lists.newArrayList();
            CommentedConfigurationNode parentNode = PixelmonConfig.getConfig().getNode("General");
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("awardPhotos"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("spawnStructures"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("allowPokemonNicknames"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("allowAnvilAutoreloading"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("allowVanillaMobs"), false).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("allowCaptureOutsideBattle"), true).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("namePlateRange"), 1).setBounds(1, 3).setSlider());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("showWildNames"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("scalePokemonModels"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("growthScaleModifier"), 1.0).setBounds(0.0, 2.0).setSlider());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("pokemonDropsEnabled"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("printErrors"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("allowRiding"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("allowPlanting"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("maximumPlants"), 32).setLowerBound(0));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("allowPvPExperience"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("allowTrainerExperience"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("returnHeldItems"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("forceEndBattleResult"), 0).setBounds(0, 2).setSlider());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("cloningMachineEnabled"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("lakeTrioMaxEnchants"), 3).setNonNegative().setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("engagePlayerByPokeBall"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("computerBoxes"), 30).setBounds(1, 256).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("enableWildAggression"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("allowTMReuse"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("writeEntitiesToWorld"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("spawnBirdShrines"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("reusableBirdShrines"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("spawnersOpOnly"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("needHMToRide"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("tradersReusable"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("allowEventMoveTutors"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("starterOnJoin"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("enablePointToSteer"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("useSystemTimeForWorldTime"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("systemTimeSyncInterval"), 30).setLowerBound(2));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("useExternalJSONFilesDrops"), false).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("useExternalJSONFilesNPCs"), false).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("useExternalJSONFilesRules"), false).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("useExternalJSONFilesSpawning"), false).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("useExternalJSONFilesStructures"), false).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("useExternalJSONFilesBreedingConditions"), false).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("useExternalJSONFilesMoves"), false).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("useExternalJSONFilesPokemon"), false).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("useExternalJSONFilesTrades"), false).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("chunkSpawnRadius"), 8).setBounds(2, 16).setSlider());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("battleAIWild"), 1).setBounds(1, 4).setSlider());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("battleAIBoss"), 2).setBounds(1, 4).setSlider());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("battleAITrainer"), 3).setBounds(1, 4).setSlider());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("expModifier"), Float.valueOf(1.0f)).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("multiplePhotosOfSamePokemon"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("allowPayDayMoney"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("pickupRate"), 10).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("bedsHealPokemon"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("allowPokemonEditors"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("dataSaveOnWorldSave"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("maxLevel"), 100));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("useDropGUI"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("ridingSpeedMultiplier"), 1.0).setBounds(0.0, 1.0));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("berryTreeGrowthMultiplier"), 1.0).setBounds(0.1, 1000.0));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("allowVanillaMusic"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("alwaysHaveMegaRing"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("canPokemonBeHit"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("curryTriggersLevelEvent"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("despawnRadius"), 80).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("enableRichPresence"), true).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("levelCandyTriggersLevelEvent"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("lightTrioMaxWormholes"), 3).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("rareCandyTriggersLevelEvent"), true));
            return new GuiConfig((GuiScreen)this.owningScreen, (List<IConfigElement>)configElements, this.owningScreen.modID, "General", this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart, this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart, I18n.translateToLocal("pixelmon.config.general.tooltip"));
        }
    }

    public static class GraphicsEntry
    extends PixelmonCategoryEntry {
        public GraphicsEntry(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
            super(owningScreen, owningEntryList, configElement);
        }

        @Override
        protected GuiScreen buildChildScreen() {
            ArrayList configElements = Lists.newArrayList();
            CommentedConfigurationNode parentNode = PixelmonConfig.getConfig().getNode("Graphics");
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("renderDistanceWeight"), 2.0).setLowerBound(0.0));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("lowResTextures"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("useSmoothShadingOnPokeBalls"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("useSmoothShadingOnPokemon"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("useOriginalPokemonTexturesForStatues"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("showCurrentAttackTarget"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("drawHealthBars"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("useBattleCamera"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("playerControlCamera"), true));
            return new GuiConfig((GuiScreen)this.owningScreen, (List<IConfigElement>)configElements, this.owningScreen.modID, "Graphics", this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart, this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart, I18n.translateToLocal("pixelmon.config.graphics.tooltip"));
        }
    }

    public static class PixelUtilsEntry
    extends PixelmonCategoryEntry {
        public PixelUtilsEntry(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
            super(owningScreen, owningEntryList, configElement);
        }

        @Override
        protected GuiScreen buildChildScreen() {
            ArrayList configElements = Lists.newArrayList();
            CommentedConfigurationNode parentNode = PixelmonConfig.getConfig().getNode("PixelUtilities");
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("scaleGrassBattles"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("blocksHaveLegendaries"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("pokeGiftReusable"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("pokeGiftHaveEvents"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("eventPokeGiftLoad"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("eventHasLegendaries"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("eventHasShinies"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("eventMaxPokemon"), 1).setBounds(1, 10).setSlider());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("eventShinyRate"), 10).setBounds(1, 100).setSlider());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("eventTime"), "D/M"));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("eventCoords"), Collections.singletonList("notConfigured")));
            return new GuiConfig((GuiScreen)this.owningScreen, (List<IConfigElement>)configElements, this.owningScreen.modID, "PixelUtils", this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart, this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart, I18n.translateToLocal("pixelmon.config.pixelutilities.tooltip"));
        }
    }

    public static class PokeLootEntry
    extends PixelmonCategoryEntry {
        public PokeLootEntry(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
            super(owningScreen, owningEntryList, configElement);
        }

        @Override
        protected GuiScreen buildChildScreen() {
            ArrayList configElements = Lists.newArrayList();
            CommentedConfigurationNode parentNode = PixelmonConfig.getConfig().getNode("PokeLoot");
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("spawnNormal"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("spawnHidden"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("spawnGrotto"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("spawnRate"), 1).setBounds(0, 3).setSlider());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("spawnMode"), 0).setBounds(0, 3).setSlider());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("timedLootReuseHours"), 24).setNonNegative());
            return new GuiConfig((GuiScreen)this.owningScreen, (List<IConfigElement>)configElements, this.owningScreen.modID, "PokeLoot", this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart, this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart, I18n.translateToLocal("pixelmon.config.pokeloot.tooltip"));
        }
    }

    public static class SpawningGensEntry
    extends PixelmonCategoryEntry {
        public SpawningGensEntry(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
            super(owningScreen, owningEntryList, configElement);
        }

        @Override
        protected GuiScreen buildChildScreen() {
            ArrayList configElements = Lists.newArrayList();
            CommentedConfigurationNode parentNode = PixelmonConfig.getConfig().getNode("Spawning").getNode("Gens");
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("Gen1"), true).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("Gen2"), true).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("Gen3"), true).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("Gen4"), true).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("Gen5"), true).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("Gen6"), true).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("Gen7"), true).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("Gen8"), true).setRequiresRestart());
            return new GuiConfig((GuiScreen)this.owningScreen, (List<IConfigElement>)configElements, this.owningScreen.modID, "Spawning Gens", this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart, this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart, I18n.translateToLocal("pixelmon.config.gens.tooltip"));
        }
    }

    public static class SpawningEntry
    extends PixelmonCategoryEntry {
        public SpawningEntry(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
            super(owningScreen, owningEntryList, configElement);
        }

        @Override
        protected GuiScreen buildChildScreen() {
            ArrayList configElements = Lists.newArrayList();
            CommentedConfigurationNode parentNode = PixelmonConfig.getConfig().getNode("Spawning");
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("spawnDimensions"), Collections.singletonList(0)));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("shinySpawnRate"), Float.valueOf(4096.0f)).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("bossSpawnTicks"), 1200).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("spawnTickRate"), 60).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("hiddenAbilitySpawnRate"), 150).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("spawnLevelsByDistance"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("maxLevelByDistance"), 60).setBounds(5, 100));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("distancePerLevel"), 30).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("displayLegendaryGlobalMessage"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("replaceMCVillagers"), true).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("spawnPokeMarts"), true).setRequiresRestart());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("spawnGyms"), true));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("useRecentLevelMoves"), false));
            configElements.add(new DummyConfigElement.DummyCategoryElement("Gens", "pixelmon.config.gens", SpawningGensEntry.class));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("maxSpawnsPerTick"), 100).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("maxLandPokemon"), 40).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("maxUndergroundPokemon"), 20).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("maxWaterPokemon"), 20).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("maxFlyingPokemon"), 2).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("maxNumNPCs"), 4).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("maxNumBosses"), 1).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("despawnOnFleeOrLoss"), false));
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("maxNumTotems"), 1).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("totemSpawnTicks"), 1200).setNonNegative());
            configElements.add(new ConfigurateNodeElement(parentNode.getNode("ultraShinySpawnRate"), 16).setNonNegative());
            return new GuiConfig((GuiScreen)this.owningScreen, (List<IConfigElement>)configElements, this.owningScreen.modID, "Spawning", this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart, this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart, I18n.translateToLocal("pixelmon.config.spawning.tooltip"));
        }
    }

    public static class PixelmonCategoryEntry
    extends GuiConfigEntries.CategoryEntry {
        public PixelmonCategoryEntry(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
            super(owningScreen, owningEntryList, configElement);
        }
    }
}

