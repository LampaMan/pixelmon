/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.client.gui.factory.config;

import com.google.common.collect.Lists;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.client.config.ConfigGuiType;
import net.minecraftforge.fml.client.config.GuiConfigEntries;
import net.minecraftforge.fml.client.config.GuiEditArrayEntries;
import net.minecraftforge.fml.client.config.IConfigElement;

public class ConfigurateNodeElement
implements IConfigElement {
    String humanName;
    ConfigurationNode configNode;
    Optional<Number> lowerBound = Optional.empty();
    Optional<Number> upperBound = Optional.empty();
    Object defaultValue;
    boolean useSlider = false;
    boolean requiresRestart = false;

    public ConfigurateNodeElement(String humanName, ConfigurationNode configNode, Object defaultValue) {
        this.humanName = humanName;
        this.configNode = configNode;
        this.defaultValue = defaultValue;
        if (configNode.isVirtual()) {
            Pixelmon.LOGGER.error("WARNING: Passed virtual config node for " + configNode.getKey() + "!");
        }
    }

    public ConfigurateNodeElement(ConfigurationNode configNode, Object defaultValue) {
        this(I18n.translateToLocal("pixelmon.config." + configNode.getKey()), configNode, defaultValue);
    }

    public ConfigurateNodeElement setBounds(Number lowerBound, Number upperBound) {
        this.lowerBound = Optional.ofNullable(lowerBound);
        this.upperBound = Optional.ofNullable(upperBound);
        return this;
    }

    public ConfigurateNodeElement setLowerBound(Number lowerBound) {
        this.lowerBound = Optional.ofNullable(lowerBound);
        return this;
    }

    public ConfigurateNodeElement setNonNegative() {
        this.setLowerBound(0);
        return this;
    }

    public ConfigurateNodeElement setSlider() {
        this.useSlider = true;
        return this;
    }

    public ConfigurateNodeElement setRequiresRestart() {
        this.requiresRestart = true;
        return this;
    }

    @Override
    public boolean isProperty() {
        return true;
    }

    @Override
    public Class<? extends GuiConfigEntries.IConfigEntry> getConfigEntryClass() {
        if (this.useSlider) {
            return GuiConfigEntries.NumberSliderEntry.class;
        }
        return null;
    }

    @Override
    public Class<? extends GuiEditArrayEntries.IArrayEntry> getArrayEntryClass() {
        return null;
    }

    @Override
    public String getName() {
        return this.humanName;
    }

    @Override
    public String getQualifiedName() {
        return null;
    }

    @Override
    public String getLanguageKey() {
        return "";
    }

    @Override
    public String getComment() {
        Optional<String> commentOptional;
        if (this.configNode instanceof CommentedConfigurationNode && (commentOptional = ((CommentedConfigurationNode)this.configNode).getComment()).isPresent()) {
            return commentOptional.get();
        }
        return "No comment?";
    }

    @Override
    public List<IConfigElement> getChildElements() {
        return null;
    }

    @Override
    public ConfigGuiType getType() {
        return this.getType(this.defaultValue);
    }

    public ConfigGuiType getType(Object value) {
        List list;
        if (value instanceof Boolean) {
            return ConfigGuiType.BOOLEAN;
        }
        if (value instanceof Integer) {
            return ConfigGuiType.INTEGER;
        }
        if (value instanceof String) {
            return ConfigGuiType.STRING;
        }
        if (value instanceof Double || value instanceof Float) {
            return ConfigGuiType.DOUBLE;
        }
        if (value instanceof List && !(list = (List)value).isEmpty()) {
            return this.getType(list.get(0));
        }
        Pixelmon.LOGGER.error("TODO getType " + value.getClass());
        return ConfigGuiType.STRING;
    }

    @Override
    public boolean isList() {
        return this.configNode.hasListChildren();
    }

    @Override
    public boolean isListLengthFixed() {
        return false;
    }

    @Override
    public int getMaxListLength() {
        return 0;
    }

    @Override
    public boolean isDefault() {
        return true;
    }

    @Override
    public Object getDefault() {
        return this.defaultValue;
    }

    @Override
    public Object[] getDefaults() {
        if (this.defaultValue instanceof List) {
            return ((List)this.defaultValue).toArray();
        }
        return null;
    }

    @Override
    public void setToDefault() {
        System.out.println("Is this even called?");
    }

    @Override
    public boolean requiresWorldRestart() {
        return false;
    }

    @Override
    public boolean showInGui() {
        return true;
    }

    @Override
    public boolean requiresMcRestart() {
        return this.requiresRestart;
    }

    @Override
    public Object get() {
        return this.configNode.getValue();
    }

    @Override
    public Object[] getList() {
        return ((ArrayList)this.configNode.getValue()).toArray();
    }

    @Override
    public void set(Object value) {
        this.configNode.setValue(value);
        PixelmonConfig.saveConfig();
        try {
            PixelmonConfig.reload(false);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void set(Object[] aVal) {
        try {
            if (aVal.length >= 1) {
                Object type = aVal[0];
                if (type instanceof String) {
                    this.configNode.setValue(Lists.newArrayList((Object[])Arrays.copyOf(aVal, aVal.length, String[].class)));
                } else if (type instanceof Integer) {
                    this.configNode.setValue(Lists.newArrayList((Object[])Arrays.copyOf(aVal, aVal.length, Integer[].class)));
                }
            } else {
                this.configNode.setValue(null);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        PixelmonConfig.saveConfig();
        try {
            PixelmonConfig.reload(false);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String[] getValidValues() {
        return new String[0];
    }

    @Override
    public Object getMinValue() {
        if (this.lowerBound.isPresent()) {
            return this.lowerBound.get();
        }
        return Integer.MIN_VALUE;
    }

    @Override
    public Object getMaxValue() {
        if (this.upperBound.isPresent()) {
            return this.upperBound.get();
        }
        return Integer.MAX_VALUE;
    }

    @Override
    public Pattern getValidationPattern() {
        return null;
    }
}

