/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.chooseMoveset;

import com.pixelmongenerations.common.battle.attacks.Attack;
import java.util.ArrayList;

public interface IMoveClicked {
    public void elementClicked(ArrayList<Attack> var1, int var2);
}

