/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;

public class GuiDoctor
extends GuiContainer {
    String s;
    private EnumGuiDoctorMode mode = EnumGuiDoctorMode.Before;
    int flashCount = 0;

    public GuiDoctor() {
        super(new ContainerEmpty());
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float f, int i, int j) {
        this.drawMessageScreen();
    }

    private void drawMessageScreen() {
        int guiWidth = 300;
        int guiHeight = 60;
        this.mc.renderEngine.bindTexture(GuiResources.battleGui3);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(this.width / 2 - guiWidth / 2, this.height - guiHeight, guiWidth, guiHeight, 0.0, 0.0, 1.0, 0.30416667461395264, this.zLevel);
        if (this.mode == EnumGuiDoctorMode.Before) {
            this.s = I18n.format("gui.guiDoctor.healPokemon", new Object[0]);
            this.drawCenteredString(this.mc.fontRenderer, this.s, this.width / 2, this.height - 35, 0xFFFFFF);
            ++this.flashCount;
            if (this.flashCount > 30) {
                this.mc.renderEngine.bindTexture(GuiResources.battleGui3);
                GuiHelper.drawImageQuad(this.width / 2 + 130, this.height - 15, 10.0, 6.0f, 0.9546874761581421, 0.31041666865348816, 0.98125f, 0.33125f, this.zLevel);
                if (this.flashCount > 60) {
                    this.flashCount = 0;
                }
            }
        } else {
            this.s = I18n.format("gui.guiDoctor.waiting", new Object[0]);
            ++this.flashCount;
            if (this.flashCount >= 160) {
                this.flashCount = 0;
            }
            if (this.flashCount < 40) {
                this.drawCenteredString(this.mc.fontRenderer, this.s, this.width / 2, this.height - 35, 0xFFFFFF);
            } else if (this.flashCount < 80) {
                this.drawCenteredString(this.mc.fontRenderer, this.s + ".", this.width / 2, this.height - 35, 0xFFFFFF);
            } else if (this.flashCount < 120) {
                this.drawCenteredString(this.mc.fontRenderer, this.s + "..", this.width / 2, this.height - 35, 0xFFFFFF);
            } else if (this.flashCount < 160) {
                this.drawCenteredString(this.mc.fontRenderer, this.s + "...", this.width / 2, this.height - 35, 0xFFFFFF);
            }
        }
    }

    static enum EnumGuiDoctorMode {
        Before,
        Healing,
        After;

    }
}

