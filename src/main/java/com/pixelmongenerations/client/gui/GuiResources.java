/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.base.Throwables
 *  org.lwjgl.LWJGLException
 *  org.lwjgl.input.Cursor
 *  org.lwjgl.input.Mouse
 */
package com.pixelmongenerations.client.gui;

import com.google.common.base.Throwables;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import java.awt.image.BufferedImage;
import java.nio.IntBuffer;
import java.util.HashMap;
import javax.imageio.ImageIO;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Cursor;
import org.lwjgl.input.Mouse;

public class GuiResources {
    public static ResourceLocation prefix;
    public static ResourceLocation lastBall;
    public static ResourceLocation tradeGui;
    public static ResourceLocation heldItem;
    public static ResourceLocation overlaySimple;
    public static ResourceLocation overlayExtended;
    public static ResourceLocation pokemonInfoP1;
    public static ResourceLocation pokemonInfoP2;
    public static ResourceLocation levelUpPopup;
    public static ResourceLocation battleGui1;
    public static ResourceLocation battleGui2;
    public static ResourceLocation battleGui3;
    public static ResourceLocation battleGui1B;
    public static ResourceLocation battleGui3B;
    public static ResourceLocation yesNo;
    public static ResourceLocation chooseMove;
    public static ResourceLocation chooseMove2;
    public static ResourceLocation choosePokemon;
    public static ResourceLocation selectCurrentPokemon;
    public static ResourceLocation itemGui2;
    public static ResourceLocation itemGui1;
    public static ResourceLocation pokecheckerPopup;
    public static ResourceLocation pixelmonCreativeInventory;
    public static ResourceLocation pixelmonCreativeInventoryNew;
    public static ResourceLocation pixelmonOverlay;
    public static ResourceLocation pixelmonOverlayExtended2;
    public static ResourceLocation pixelmonOverlayExtended2New;
    public static ResourceLocation mcInventory;
    public static ResourceLocation pcPartyBox;
    public static ResourceLocation pcBox;
    public static ResourceLocation pcBackgrounds;
    public static ResourceLocation partyPCPanel;
    public static ResourceLocation boxFist;
    public static ResourceLocation boxGrab;
    public static ResourceLocation boxPoint;
    public static ResourceLocation pcSelectBackground;
    public static ResourceLocation pcSelectBackgroundLocked;
    public static ResourceLocation pcBanner;
    public static ResourceLocation pcBanner2;
    public static ResourceLocation summarySummary;
    public static ResourceLocation rename;
    public static ResourceLocation summaryMoves;
    public static ResourceLocation summaryStats;
    public static ResourceLocation pokedex;
    public static ResourceLocation types;
    public static ResourceLocation status;
    public static ResourceLocation shiny;
    public static ResourceLocation st;
    public static ResourceLocation caught;
    public static ResourceLocation eggNormal1;
    public static ResourceLocation eggNormal2;
    public static ResourceLocation eggNormal3;
    public static ResourceLocation eggTogepi1;
    public static ResourceLocation eggTogepi2;
    public static ResourceLocation eggTogepi3;
    public static ResourceLocation eggManaphy1;
    public static ResourceLocation eggManaphy2;
    public static ResourceLocation eggManaphy3;
    public static ResourceLocation available;
    public static ResourceLocation dock;
    public static ResourceLocation textbox;
    public static ResourceLocation fainted;
    public static ResourceLocation faintedSelected;
    public static ResourceLocation normal;
    public static ResourceLocation released;
    public static ResourceLocation releasedSelected;
    public static ResourceLocation selected;
    public static ResourceLocation targetArea;
    public static ResourceLocation targetAreaOver;
    public static ResourceLocation targetBox;
    public static ResourceLocation cooldown;
    public static ResourceLocation notarget;
    public static ResourceLocation pokedexItemIcon;
    public static ResourceLocation wikiItemIcon;
    public static ResourceLocation cosmeticKeyIcon;
    public static ResourceLocation shadow;
    public static final ResourceLocation buttonTexture;
    public static final ResourceLocation mouseOverTexture;
    public static final ResourceLocation questionMark;
    public static ResourceLocation starterBackground;
    public static ResourceLocation starterButton;
    public static ResourceLocation starterButtonHover;
    public static ResourceLocation starterBorders;
    public static ResourceLocation cwPanel;
    public static ResourceLocation evo;
    public static ResourceLocation button;
    public static ResourceLocation buttonOver;
    public static ResourceLocation background;
    public static ResourceLocation itemSlot;
    public static ResourceLocation itemSlotOver;
    public static ResourceLocation yesAndNo;
    public static ResourceLocation male;
    public static ResourceLocation female;
    public static ResourceLocation padlock;
    public static ResourceLocation pokedollar;
    public static ResourceLocation shopkeeper;
    public static ResourceLocation cameraOverlay;
    public static ResourceLocation cameraControls;
    public static ResourceLocation keyStoneSprite;
    public static ResourceLocation shinyCharmSprite;
    public static ResourceLocation keyStoneSpriteGray;
    public static ResourceLocation shinyCharmSpriteGray;
    public static ResourceLocation equipmentSprite;
    public static ResourceLocation equipmentSpriteHover;
    public static ResourceLocation megaBraceletORAS;
    public static ResourceLocation noItem;
    public static ResourceLocation dropDownIcon;
    public static ResourceLocation roundedButton;
    public static ResourceLocation roundedButtonOver;
    public static ResourceLocation megaBraceletUnlock;
    public static ResourceLocation shinyCharmUnlock;
    public static ResourceLocation megaBraceletEquip;
    public static ResourceLocation shinyCharmEquip;
    public static ResourceLocation buttonEquip;
    public static ResourceLocation buttonUnequip;
    public static ResourceLocation pokedexHomeBackground;
    public static ResourceLocation pokedexInfoBackground;
    public static ResourceLocation pokedexTypeBug;
    public static ResourceLocation pokedexTypeDark;
    public static ResourceLocation pokedexTypeDragon;
    public static ResourceLocation pokedexTypeElectric;
    public static ResourceLocation pokedexTypeFairy;
    public static ResourceLocation pokedexTypeFighting;
    public static ResourceLocation pokedexTypeFire;
    public static ResourceLocation pokedexTypeFlying;
    public static ResourceLocation pokedexTypeGhost;
    public static ResourceLocation pokedexTypeGrass;
    public static ResourceLocation pokedexTypeGround;
    public static ResourceLocation pokedexTypeIce;
    public static ResourceLocation pokedexTypeNormal;
    public static ResourceLocation pokedexTypePoison;
    public static ResourceLocation pokedexTypePsychic;
    public static ResourceLocation pokedexTypeRock;
    public static ResourceLocation pokedexTypeSteel;
    public static ResourceLocation pokedexTypeWater;
    public static ResourceLocation spawnerBackground;
    public static ResourceLocation rotomCatalogBackground;
    public static ResourceLocation rotomCatalogButton;
    public static ResourceLocation curryDex;
    public static ResourceLocation selection;
    public static ResourceLocation dynamax;
    public static ResourceLocation dynamaxHover;
    public static ResourceLocation pokeRusInfected;
    public static ResourceLocation pokeRusCured;
    public static ResourceLocation gmaxIcon;
    public static ResourceLocation checkBox;
    public static ResourceLocation cosmeticsBackgroundBlue;
    public static ResourceLocation pauseIcon;
    public static ResourceLocation playIcon;
    private static HashMap<EnumPokeball, ResourceLocation> pokeballs;
    public static final Cursor defaultCursor;
    public static final Cursor fistCursor;
    public static final Cursor pointCursor;
    public static final Cursor grabCursor;

    public static ResourceLocation background(String background) {
        return new ResourceLocation(prefix + "gui/pc/" + background + ".png");
    }

    public static ResourceLocation shinySprite(String numString) {
        return new ResourceLocation(prefix + "sprites/shinypokemon/" + numString + ".png");
    }

    public static ResourceLocation sprite(String numString) {
        return new ResourceLocation(prefix + "sprites/pokemon/" + numString + ".png");
    }

    public static ResourceLocation pokeball(EnumPokeball pokeball) {
        return pokeballs.get(pokeball);
    }

    public static void setCursor(Cursor cursor) {
        try {
            Mouse.setNativeCursor((Cursor)cursor);
        }
        catch (LWJGLException e) {
            Throwables.propagate((Throwable)e);
        }
    }

    static {
        BufferedImage image;
        prefix = new ResourceLocation("pixelmon:textures/");
        lastBall = new ResourceLocation(prefix + "gui/lastball.png");
        tradeGui = new ResourceLocation(prefix + "gui/tradeGui.png");
        heldItem = new ResourceLocation(prefix + "helditem.png");
        overlaySimple = new ResourceLocation(prefix + "gui/pixelmonOverlaySimple.png");
        overlayExtended = new ResourceLocation(prefix + "gui/pixelmonOverlay.png");
        pokemonInfoP1 = new ResourceLocation(prefix + "gui/pokemonInfoP1.png");
        pokemonInfoP2 = new ResourceLocation(prefix + "gui/pokemonInforP2.png");
        levelUpPopup = new ResourceLocation(prefix + "gui/levelUpPopUp.png");
        battleGui1 = new ResourceLocation(prefix + "gui/battleGui1.png");
        battleGui2 = new ResourceLocation(prefix + "gui/battleGui2.png");
        battleGui3 = new ResourceLocation(prefix + "gui/battleGui3.png");
        battleGui1B = new ResourceLocation(prefix + "gui/battleGui1B.png");
        battleGui3B = new ResourceLocation(prefix + "gui/battleGui3B.png");
        yesNo = new ResourceLocation(prefix + "gui/yesNo.png");
        chooseMove = new ResourceLocation(prefix + "gui/chooseMove.png");
        chooseMove2 = new ResourceLocation(prefix + "gui/chooseMove2.png");
        choosePokemon = new ResourceLocation(prefix + "gui/choosePokemon.png");
        selectCurrentPokemon = new ResourceLocation(prefix + "gui/selectCurrentPokemon.png");
        itemGui2 = new ResourceLocation(prefix + "gui/itemGui2.png");
        itemGui1 = new ResourceLocation(prefix + "gui/itemGui1_Test.png");
        pokecheckerPopup = new ResourceLocation(prefix + "gui/pokecheckerPopup.png");
        pixelmonCreativeInventory = new ResourceLocation(prefix + "gui/PixelmonCreativeInventory.png");
        pixelmonCreativeInventoryNew = new ResourceLocation(prefix + "gui/PixelmonCreativeInventory_new.png");
        pixelmonOverlay = new ResourceLocation(prefix + "gui/pixelmonOverlay.png");
        pixelmonOverlayExtended2 = new ResourceLocation(prefix + "gui/pixelmonOverlayExtended2.png");
        pixelmonOverlayExtended2New = new ResourceLocation(prefix + "gui/pixelmonOverlayExtended2_new.png");
        mcInventory = new ResourceLocation("minecraft:textures/gui/container/inventory.png");
        pcPartyBox = new ResourceLocation(prefix + "gui/pcPartyBox.png");
        pcBox = new ResourceLocation(prefix + "gui/pcBox.png");
        pcBackgrounds = new ResourceLocation(prefix + "gui/pc/backgrounds.png");
        partyPCPanel = new ResourceLocation(prefix + "gui/pc/boxpartytab.png");
        boxFist = new ResourceLocation(prefix + "gui/pc/boxfist.png");
        boxGrab = new ResourceLocation(prefix + "gui/pc/boxgrab.png");
        boxPoint = new ResourceLocation(prefix + "gui/pc/boxpoint.png");
        pcSelectBackground = new ResourceLocation(prefix + "gui/pc/selectbackground.png");
        pcSelectBackgroundLocked = new ResourceLocation(prefix + "gui/pc/selectbackgroundlocked.png");
        pcBanner = new ResourceLocation(prefix + "gui/banner_preset.png");
        pcBanner2 = new ResourceLocation(prefix + "gui/banner_preset_2.png");
        summarySummary = new ResourceLocation(prefix + "gui/summarySummary.png");
        rename = new ResourceLocation(prefix + "gui/rename.png");
        summaryMoves = new ResourceLocation(prefix + "gui/summaryMoves.png");
        summaryStats = new ResourceLocation(prefix + "gui/summaryStats.png");
        pokedex = new ResourceLocation(prefix + "gui/pokedex.png");
        types = new ResourceLocation(prefix + "gui/types.png");
        status = new ResourceLocation(prefix + "gui/status.png");
        shiny = new ResourceLocation(prefix + "sprites/shinypokemon/star.png");
        st = new ResourceLocation(prefix + "sprites/st.png");
        caught = new ResourceLocation(prefix + "sprites/pokemon/pokeball.png");
        eggNormal1 = new ResourceLocation(prefix + "sprites/eggs/egg1.png");
        eggNormal2 = new ResourceLocation(prefix + "sprites/eggs/egg2.png");
        eggNormal3 = new ResourceLocation(prefix + "sprites/eggs/egg3.png");
        eggTogepi1 = new ResourceLocation(prefix + "sprites/eggs/togepi1.png");
        eggTogepi2 = new ResourceLocation(prefix + "sprites/eggs/togepi2.png");
        eggTogepi3 = new ResourceLocation(prefix + "sprites/eggs/togepi3.png");
        eggManaphy1 = new ResourceLocation(prefix + "sprites/eggs/manaphy1.png");
        eggManaphy2 = new ResourceLocation(prefix + "sprites/eggs/manaphy2.png");
        eggManaphy3 = new ResourceLocation(prefix + "sprites/eggs/manaphy3.png");
        available = new ResourceLocation(prefix + "gui/overlay/available.png");
        dock = new ResourceLocation(prefix + "gui/overlay/dock.png");
        textbox = new ResourceLocation(prefix + "gui/overlay/ui.png");
        fainted = new ResourceLocation(prefix + "gui/overlay/fainted.png");
        faintedSelected = new ResourceLocation(prefix + "gui/overlay/selected-fainted.png");
        normal = new ResourceLocation(prefix + "gui/overlay/normal.png");
        released = new ResourceLocation(prefix + "gui/overlay/released.png");
        releasedSelected = new ResourceLocation(prefix + "gui/overlay/selected-released.png");
        selected = new ResourceLocation(prefix + "gui/overlay/selected.png");
        targetArea = new ResourceLocation(prefix + "gui/overlay/targetArea.png");
        targetAreaOver = new ResourceLocation(prefix + "gui/overlay/targetAreaOverlay.png");
        targetBox = new ResourceLocation(prefix + "gui/overlay/targetBox.png");
        cooldown = new ResourceLocation(prefix + "gui/overlay/cooldown.png");
        notarget = new ResourceLocation(prefix + "gui/overlay/notarget.png");
        pokedexItemIcon = new ResourceLocation(prefix + "items/pokedex.png");
        wikiItemIcon = new ResourceLocation(prefix + "gui/wikiIcon.png");
        cosmeticKeyIcon = new ResourceLocation(prefix + "gui/cosmeticsicon.png");
        shadow = new ResourceLocation(prefix + "gui/starter/ShadowLarge.png");
        buttonTexture = new ResourceLocation(prefix + "gui/starter/starterHolder.png");
        mouseOverTexture = new ResourceLocation(prefix + "gui/starter/moStarter.png");
        questionMark = new ResourceLocation(prefix + "gui/starter/questionmark.png");
        starterBackground = new ResourceLocation(prefix + "gui/starter/background.png");
        starterButton = new ResourceLocation(prefix + "gui/starter/button.png");
        starterButtonHover = new ResourceLocation(prefix + "gui/starter/button_hover.png");
        starterBorders = new ResourceLocation(prefix + "gui/starter/button_hover.png");
        cwPanel = new ResourceLocation(prefix + "gui/starter/cwpanel.png");
        evo = new ResourceLocation(prefix + "gui/evolution/Evolution.png");
        button = new ResourceLocation(prefix + "gui/evolution/Button.png");
        buttonOver = new ResourceLocation(prefix + "gui/evolution/ButtonOver.png");
        background = new ResourceLocation(prefix + "gui/drops/Drops1.png");
        itemSlot = new ResourceLocation(prefix + "gui/drops/Drops2.png");
        itemSlotOver = new ResourceLocation(prefix + "gui/drops/Drops2Over.png");
        yesAndNo = new ResourceLocation(prefix + "gui/yesAndNo.png");
        male = new ResourceLocation(prefix + "gui/male.png");
        female = new ResourceLocation(prefix + "gui/female.png");
        padlock = new ResourceLocation(prefix + "gui/padlock.png");
        pokedollar = new ResourceLocation(prefix + "gui/pokedollar.png");
        shopkeeper = new ResourceLocation(prefix + "gui/shopkeeper.png");
        cameraOverlay = new ResourceLocation(prefix + "gui/cameraOverlay.png");
        cameraControls = new ResourceLocation(prefix + "gui/cameraControls.png");
        keyStoneSprite = new ResourceLocation(prefix + "gui/megaItems/keystone.png");
        shinyCharmSprite = new ResourceLocation(prefix + "gui/shinyitems/shinycharm.png");
        keyStoneSpriteGray = new ResourceLocation(prefix + "gui/megaItems/keystone_gray.png");
        shinyCharmSpriteGray = new ResourceLocation(prefix + "gui/shinyitems/shinycharm_gray.png");
        equipmentSprite = new ResourceLocation(prefix + "gui/equipment.png");
        equipmentSpriteHover = new ResourceLocation(prefix + "gui/equipmenthover.png");
        megaBraceletORAS = new ResourceLocation(prefix + "gui/megaitems/megabraceletoras.png");
        noItem = new ResourceLocation(prefix + "gui/megaItems/noitem.png");
        dropDownIcon = new ResourceLocation(prefix + "gui/dropdown.png");
        roundedButton = new ResourceLocation("pixelmon:textures/gui/acceptDeny/button.png");
        roundedButtonOver = new ResourceLocation("pixelmon:textures/gui/acceptDeny/buttonOver.png");
        megaBraceletUnlock = new ResourceLocation("pixelmon:textures/gui/unlocks/unlock_megabracelet.png");
        shinyCharmUnlock = new ResourceLocation("pixelmon:textures/gui/unlocks/unlock_shinycharm.png");
        megaBraceletEquip = new ResourceLocation("pixelmon:textures/gui/unlocks/equip_megabracelet.png");
        shinyCharmEquip = new ResourceLocation("pixelmon:textures/gui/unlocks/equip_shinycharm.png");
        buttonEquip = new ResourceLocation("pixelmon:textures/gui/unlocks/button_equip.png");
        buttonUnequip = new ResourceLocation("pixelmon:textures/gui/unlocks/button_unequip.png");
        pokedexHomeBackground = new ResourceLocation("pixelmon:textures/gui/pokedex/gui_home_bg.png");
        pokedexInfoBackground = new ResourceLocation("pixelmon:textures/gui/pokedex/gui_info_bg.png");
        pokedexTypeBug = new ResourceLocation("pixelmon:textures/gui/pokedex/type_bug.png");
        pokedexTypeDark = new ResourceLocation("pixelmon:textures/gui/pokedex/type_dark.png");
        pokedexTypeDragon = new ResourceLocation("pixelmon:textures/gui/pokedex/type_dragon.png");
        pokedexTypeElectric = new ResourceLocation("pixelmon:textures/gui/pokedex/type_electric.png");
        pokedexTypeFairy = new ResourceLocation("pixelmon:textures/gui/pokedex/type_fairy.png");
        pokedexTypeFighting = new ResourceLocation("pixelmon:textures/gui/pokedex/type_fighting.png");
        pokedexTypeFire = new ResourceLocation("pixelmon:textures/gui/pokedex/type_fire.png");
        pokedexTypeFlying = new ResourceLocation("pixelmon:textures/gui/pokedex/type_flying.png");
        pokedexTypeGhost = new ResourceLocation("pixelmon:textures/gui/pokedex/type_ghost.png");
        pokedexTypeGrass = new ResourceLocation("pixelmon:textures/gui/pokedex/type_grass.png");
        pokedexTypeGround = new ResourceLocation("pixelmon:textures/gui/pokedex/type_ground.png");
        pokedexTypeIce = new ResourceLocation("pixelmon:textures/gui/pokedex/type_ice.png");
        pokedexTypeNormal = new ResourceLocation("pixelmon:textures/gui/pokedex/type_normal.png");
        pokedexTypePoison = new ResourceLocation("pixelmon:textures/gui/pokedex/type_poison.png");
        pokedexTypePsychic = new ResourceLocation("pixelmon:textures/gui/pokedex/type_psychic.png");
        pokedexTypeRock = new ResourceLocation("pixelmon:textures/gui/pokedex/type_rock.png");
        pokedexTypeSteel = new ResourceLocation("pixelmon:textures/gui/pokedex/type_steel.png");
        pokedexTypeWater = new ResourceLocation("pixelmon:textures/gui/pokedex/type_water.png");
        spawnerBackground = new ResourceLocation("pixelmon:textures/gui/spawner_bg.png");
        rotomCatalogBackground = new ResourceLocation("pixelmon:textures/gui/rotom/rotom_catalog_bg.png");
        rotomCatalogButton = new ResourceLocation("pixelmon:textures/gui/rotom/rotom_button_base.png");
        curryDex = new ResourceLocation("pixelmon:textures/gui/currydex/curry_dex.png");
        selection = new ResourceLocation("pixelmon:textures/gui/currydex/selection.png");
        dynamax = new ResourceLocation("pixelmon:textures/gui/battle/dynamax.png");
        dynamaxHover = new ResourceLocation("pixelmon:textures/gui/battle/dynamaxhover.png");
        pokeRusInfected = new ResourceLocation("pixelmon:textures/gui/pokerus_infected.png");
        pokeRusCured = new ResourceLocation("pixelmon:textures/gui/pokerus_cured.png");
        gmaxIcon = new ResourceLocation("pixelmon:textures/gui/gigamax_icon.png");
        checkBox = new ResourceLocation(prefix + "gui/button/check_box.png");
        cosmeticsBackgroundBlue = new ResourceLocation("pixelmon:textures/gui/cosmetics/background_blue.png");
        pauseIcon = new ResourceLocation("pixelmon:textures/gui/cosmetics/pause_icon.png");
        playIcon = new ResourceLocation("pixelmon:textures/gui/cosmetics/play_icon.png");
        pokeballs = new HashMap();
        for (EnumPokeball pokeball : EnumPokeball.values()) {
            pokeballs.put(pokeball, new ResourceLocation(prefix + "gui/overlay/" + pokeball.getFilenamePrefix().replaceAll("_ball", ".png")));
        }
        defaultCursor = Mouse.getNativeCursor();
        try {
            image = ImageIO.read(Minecraft.getMinecraft().getResourceManager().getResource(boxFist).getInputStream());
            fistCursor = new Cursor(image.getWidth(), image.getHeight(), image.getWidth() / 2, image.getHeight() / 2, 1, IntBuffer.wrap(image.getRGB(0, 0, image.getWidth(), image.getHeight(), null, 0, image.getWidth())), null);
        }
        catch (Exception e) {
            throw Throwables.propagate((Throwable)e);
        }
        try {
            image = ImageIO.read(Minecraft.getMinecraft().getResourceManager().getResource(boxPoint).getInputStream());
            pointCursor = new Cursor(image.getWidth(), image.getHeight(), image.getWidth() / 2, image.getHeight() / 2, 1, IntBuffer.wrap(image.getRGB(0, 0, image.getWidth(), image.getHeight(), null, 0, image.getWidth())), null);
        }
        catch (Exception e) {
            throw Throwables.propagate((Throwable)e);
        }
        try {
            image = ImageIO.read(Minecraft.getMinecraft().getResourceManager().getResource(boxGrab).getInputStream());
            grabCursor = new Cursor(image.getWidth(), image.getHeight(), image.getWidth() / 2, image.getHeight() / 2, 1, IntBuffer.wrap(image.getRGB(0, 0, image.getWidth(), image.getHeight(), null, 0, image.getWidth())), null);
        }
        catch (Exception e) {
            throw Throwables.propagate((Throwable)e);
        }
    }
}

