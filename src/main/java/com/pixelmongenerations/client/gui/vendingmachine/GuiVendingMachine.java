/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.vendingmachine;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.npc.ClientShopItem;
import com.pixelmongenerations.client.gui.npc.GuiShopContainer;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.vendingMachine.VendingMachinePacket;
import java.io.IOException;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.translation.I18n;

public class GuiVendingMachine
extends GuiShopContainer {
    BlockPos vendingMachineLocation = null;

    public GuiVendingMachine(BlockPos pos) {
        this.vendingMachineLocation = pos;
        this.allowMultiple = false;
    }

    @Override
    public void handleMouseInput() throws IOException {
        super.handleMouseInput();
        this.handleMouseScroll();
    }

    @Override
    public void keyTyped(char key, int i) {
        if (i == 1) {
            GuiHelper.closeScreen();
        }
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        this.clickBuyScreen(mouseX, mouseY);
    }

    @Override
    protected void sendBuyPacket() {
        Pixelmon.NETWORK.sendToServer(new VendingMachinePacket(this.vendingMachineLocation, ((ClientShopItem)buyItems.get(this.selectedItem)).getItemID()));
    }

    @Override
    public void drawGuiContainerBackgroundLayer(float f, int mouseX, int mouseY) {
        this.renderBuyScreen(mouseX, mouseY);
        int colour = 0xFFFFFF;
        String buyLabel = I18n.translateToLocal("gui.shopkeeper.buy");
        this.drawString(this.mc.fontRenderer, buyLabel, this.width / 2 - 2 - this.mc.fontRenderer.getStringWidth(buyLabel) / 2, this.height / 2 - 82, colour);
        colour = 0xFFFFFF;
        if (mouseX > this.width / 2 + 28 && mouseX < this.width / 2 + 28 + 58 && mouseY > this.height / 2 - 93 && mouseY < this.height / 2 - 93 + 30) {
            colour = 0xCCCCCC;
        }
    }
}

