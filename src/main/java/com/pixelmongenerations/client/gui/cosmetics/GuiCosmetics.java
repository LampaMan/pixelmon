/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Mouse
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.gui.cosmetics;

import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.client.event.ClientRenderHooks;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.pokechecker.GuiTextFieldTransparent;
import com.pixelmongenerations.client.util.ClientUtils;
import com.pixelmongenerations.common.cosmetic.CosmeticCategory;
import com.pixelmongenerations.common.cosmetic.CosmeticData;
import com.pixelmongenerations.common.cosmetic.CosmeticEntry;
import com.pixelmongenerations.common.cosmetic.LocalCosmeticCache;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.cosmetic.ChangeCosmetic;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.awt.Rectangle;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

public class GuiCosmetics
extends GuiContainer {
    private int guiWidth = 433;
    private int guiHeight = 250;
    private boolean spinning = true;
    private boolean dragging = false;
    private int lastXPosition = 0;
    private float rotate = 0.0f;
    protected GuiTextFieldTransparent searchText;
    private List<CosmeticEntry> entries;
    private boolean searching = false;
    private List<CosmeticEntry> searchResults;
    private ResourceLocation MISSING = new ResourceLocation("pixelmon:textures/gui/cosmetics/icons/missing.png");
    private HashMap<String, ResourceLocation> cache = new HashMap();
    private CosmeticCategory category;
    private int page = 1;

    public GuiCosmetics() {
        super(new ContainerEmpty());
    }

    @Override
    public void initGui() {
        super.initGui();
        int guiTopLeft = this.width / 2 - this.guiWidth / 2;
        int guiTopY = this.height / 2 - this.guiHeight / 2;
        this.searching = false;
        this.searchResults = new ArrayList<CosmeticEntry>();
        this.searchText = new GuiTextFieldTransparent(this.fontRenderer, (int)((double)guiTopLeft + (double)this.guiWidth * 0.315), guiTopY + 33, 127, 10);
        this.searchText.setMaxStringLength(100);
        this.searchText.setFocused(false);
        this.category = CosmeticCategory.Head;
        this.entries = LocalCosmeticCache.getCosmeticData(this.mc.player.getUniqueID()).getCosmeticsFromCategory(this.category);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        if (this.page > this.getMaxPages()) {
            this.page = 1;
        }
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        int guiTopLeft = this.width / 2 - this.guiWidth / 2;
        int guiTopY = this.height / 2 - this.guiHeight / 2;
        this.mc.getTextureManager().bindTexture(GuiResources.cosmeticsBackgroundBlue);
        ClientUtils.drawImageQuad(guiTopLeft, guiTopY, this.guiWidth, this.guiHeight, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        this.mc.getTextureManager().bindTexture(new ResourceLocation("pixelmon:textures/gui/cosmetics/pgicon.png"));
        ClientUtils.drawImageQuad((double)guiTopLeft + (double)this.guiWidth * 0.315, guiTopY + 3, 22.0, 22.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        GL11.glPushMatrix();
        float textScale = 1.3f;
        GL11.glScalef((float)textScale, (float)textScale, (float)textScale);
        this.fontRenderer.drawString("Generations Cosmetics", (float)(this.width / 2 - this.fontRenderer.getStringWidth("Generations Cosmetics") / 2) / textScale, (float)(guiTopY + 9) / textScale, 16704021, false);
        GL11.glScalef((float)1.0f, (float)1.0f, (float)1.0f);
        GL11.glPopMatrix();
        if (!new Rectangle(guiTopLeft + 15, guiTopY + 55, 16, 16).contains(mouseX, mouseY)) {
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        }
        this.mc.getTextureManager().bindTexture(this.spinning ? GuiResources.pauseIcon : GuiResources.playIcon);
        ClientUtils.drawImageQuad(guiTopLeft + 15, guiTopY + 55, 16.0, 16.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        this.fontRenderer.drawString("Preview Cosmetics", guiTopLeft + 17, guiTopY + 37, 0xFFFFFF, false);
        this.fontRenderer.drawString(this.category.name() + " Cosmetic's Page: " + this.page + "/" + this.getMaxPages(), guiTopLeft + 275, guiTopY + 34, 0xFFFFFF, false);
        this.drawEntityOnScreen(guiTopLeft + 60, guiTopY + (int)((double)this.guiHeight * 0.9), 70, this.mc.player);
        this.searchText.drawTextBox();
        int gap = this.fontRenderer.FONT_HEIGHT + 3;
        int index = this.page * 8 - 8;
        for (int i = 1; i < 9; ++i) {
            int posIndex = i - 1;
            if (index + i > (this.searching ? this.searchResults.size() : this.entries.size())) continue;
            CosmeticEntry entry = this.searching ? this.searchResults.get(index + posIndex) : this.entries.get(index + posIndex);
            int stringWidth = this.fontRenderer.getStringWidth(entry.getName()) / 2;
            int textColor = 0xFFFFFF;
            if (new Rectangle(guiTopLeft + 121 + posIndex * 66 - (posIndex > 3 ? 264 : 0), guiTopY + 50 + (posIndex > 3 ? 99 : 0), 56, 91).contains(mouseX, mouseY)) {
                textColor = 16572537;
            } else {
                CosmeticData data = LocalCosmeticCache.getCosmeticData(Minecraft.getMinecraft().player.getUniqueID());
                if (data == null) break;
                CosmeticEntry cosEntry = data.getCosmeticInSlot(this.category);
                if (cosEntry != null && cosEntry.hashCode() == entry.hashCode()) {
                    textColor = 16566079;
                }
            }
            if (entry.getName().contains(" ")) {
                String[] lines = entry.getName().split(" ");
                int lineCount = 0;
                for (String line : lines) {
                    stringWidth = this.fontRenderer.getStringWidth(line) / 2;
                    this.fontRenderer.drawString(line, (float)(guiTopLeft + 149 + posIndex * 66 - stringWidth - (int)(posIndex < 5 ? (double)posIndex * 0.35 : (double)posIndex * 0.5)) - (posIndex > 3 ? 262.0f : 0.0f), guiTopY + 53 + lineCount * gap + (posIndex > 3 ? 99 : 0), textColor, false);
                    ++lineCount;
                }
            } else {
                this.fontRenderer.drawString(entry.getName(), (float)(guiTopLeft + 149 + posIndex * 66 - stringWidth - (int)(posIndex < 5 ? (double)posIndex * 0.35 : (double)posIndex * 0.5)) - (posIndex > 3 ? 262.0f : 0.0f), guiTopY + 53 + (posIndex > 3 ? 99 : 0), textColor, false);
            }
            GL11.glColor3f((float)1.0f, (float)1.0f, (float)1.0f);
            String previewString = "preview-" + entry.getModelName() + "-" + entry.getTextureName();
            TextureResource textureResource = ClientProxy.TEXTURE_STORE.getObject(previewString);
            if (textureResource == null) {
                ResourceLocation resource;
                previewString = entry.getModelName() + "-" + entry.getTextureName();
                if (this.cache.containsKey(previewString)) {
                    resource = this.cache.get(previewString);
                } else {
                    resource = new ResourceLocation("pixelmon:textures/gui/cosmetics/icons/" + previewString + ".png");
                    this.cache.put(previewString, resource);
                }
                try {
                    Minecraft.getMinecraft().getResourceManager().getResource(resource);
                    this.mc.getTextureManager().bindTexture(resource);
                }
                catch (Throwable throwable) {
                    this.mc.getTextureManager().bindTexture(this.MISSING);
                }
            } else {
                textureResource.bindTexture();
            }
            ClientUtils.drawImageQuad((double)(guiTopLeft + 124) + (double)posIndex * 65.5 - (posIndex > 3 ? 262.0 : 0.0), guiTopY + 87 + (posIndex > 3 ? 99 : 0), 50.0, 50.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        }
        GL11.glPushMatrix();
        textScale = 1.02f;
        GL11.glScalef((float)textScale, (float)textScale, (float)textScale);
        this.fontRenderer.drawString("Categories", (float)(guiTopLeft + 403 - this.fontRenderer.getStringWidth("Categories") / 2) / textScale, (float)(guiTopY + 54) / textScale, 16704021, false);
        GL11.glScalef((float)1.0f, (float)1.0f, (float)1.0f);
        GL11.glPopMatrix();
        int startingY = guiTopY + 71;
        int count = 0;
        for (CosmeticCategory cc : CosmeticCategory.values()) {
            int stringWidth = this.fontRenderer.getStringWidth(cc.name());
            int textColor = 0xFFFFFF;
            int textPosX = guiTopLeft + 404 - stringWidth / 2;
            int textPosY = startingY + (this.fontRenderer.FONT_HEIGHT + 10) * count;
            int selectionPadding = 2;
            if (this.category == cc) {
                textColor = 16566079;
            }
            if (new Rectangle(textPosX - selectionPadding, textPosY - selectionPadding, stringWidth + selectionPadding, this.fontRenderer.FONT_HEIGHT + selectionPadding).contains(mouseX, mouseY)) {
                textColor = 16572537;
            }
            this.fontRenderer.drawString(cc.name(), textPosX, textPosY, textColor, false);
            ++count;
        }
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        if (this.searchText.isFocused()) {
            this.searchText.textboxKeyTyped(typedChar, keyCode);
            this.searching = !this.searchText.getText().isEmpty();
            this.updateSearchResults();
            this.page = 1;
            return;
        }
        super.keyTyped(typedChar, keyCode);
    }

    private void updateSearchResults() {
        this.searchResults.clear();
        if (this.searching) {
            String search = this.searchText.getText().toLowerCase();
            this.searchResults.addAll(this.entries.stream().filter(entry -> {
                String name = entry.getName().toLowerCase();
                return name.startsWith(search) || name.contains(search);
            }).collect(Collectors.toList()));
        }
    }

    public void drawEntityOnScreen(int posX, int posY, int scale, EntityLivingBase ent) {
        GlStateManager.enableColorMaterial();
        GlStateManager.pushMatrix();
        GlStateManager.translate(posX, posY, 50.0f);
        GlStateManager.scale(-scale, scale, scale);
        GlStateManager.rotate(180.0f, 0.0f, 0.0f, 1.0f);
        float f = ent.renderYawOffset;
        float f1 = ent.rotationYaw;
        float f2 = ent.rotationPitch;
        float f3 = ent.prevRotationYawHead;
        float f4 = ent.rotationYawHead;
        GlStateManager.rotate(135.0f, 0.0f, 1.0f, 0.0f);
        RenderHelper.enableStandardItemLighting();
        GlStateManager.rotate(-135.0f, 0.0f, 1.0f, 0.0f);
        GlStateManager.rotate(this.rotate, 0.0f, 1.0f, 0.0f);
        ent.rotationYawHead = ent.rotationYaw;
        ent.prevRotationYawHead = ent.rotationYaw;
        GlStateManager.translate(0.0f, 0.0f, 0.0f);
        RenderManager rendermanager = Minecraft.getMinecraft().getRenderManager();
        rendermanager.setPlayerViewY(180.0f);
        rendermanager.setRenderShadow(false);
        rendermanager.renderEntity(ent, 0.0, 0.0, 0.0, 0.0f, 1.0f, false);
        rendermanager.setRenderShadow(true);
        ent.renderYawOffset = f;
        ent.rotationYaw = f1;
        ent.rotationPitch = f2;
        ent.prevRotationYawHead = f3;
        ent.rotationYawHead = f4;
        GlStateManager.popMatrix();
        RenderHelper.disableStandardItemLighting();
        GlStateManager.disableRescaleNormal();
        GlStateManager.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        GlStateManager.disableTexture2D();
        GlStateManager.setActiveTexture(OpenGlHelper.defaultTexUnit);
    }

    @Override
    public void updateScreen() {
        if (this.spinning) {
            this.rotate += ClientRenderHooks.partialTicks * -1.0f;
            if (this.rotate < -360.0f || this.rotate > 360.0f) {
                this.rotate = 0.0f;
            }
        }
        super.updateScreen();
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int button) throws IOException {
        this.searchText.mouseClicked(mouseX, mouseY, button);
        int guiTopLeft = this.width / 2 - this.guiWidth / 2;
        int guiTopY = this.height / 2 - this.guiHeight / 2;
        if (new Rectangle(guiTopLeft + 15, guiTopY + 55, 16, 16).contains(mouseX, mouseY)) {
            this.spinning = !this.spinning;
        } else if (new Rectangle(guiTopLeft + 10, guiTopY + 51, 102, 190).contains(mouseX, mouseY)) {
            this.spinning = false;
            this.dragging = true;
            this.lastXPosition = mouseX;
        }
        int index = this.page * 8 - 8;
        for (int i = 1; i < 9; ++i) {
            int posIndex = i - 1;
            if (index + i > (this.searching ? this.searchResults.size() : this.entries.size()) || !new Rectangle(guiTopLeft + 121 + posIndex * 66 - (posIndex > 3 ? 264 : 0), guiTopY + 50 + (posIndex > 3 ? 99 : 0), 56, 91).contains(mouseX, mouseY)) continue;
            CosmeticEntry entry = this.searching ? this.searchResults.get(index + posIndex) : this.entries.get(index + posIndex);
            Pixelmon.NETWORK.sendToServer(new ChangeCosmetic(entry.hashCode()));
        }
        int startingY = guiTopY + 71;
        int count = 0;
        for (CosmeticCategory cc : CosmeticCategory.values()) {
            int textPosY;
            int selectionPadding;
            int stringWidth = this.fontRenderer.getStringWidth(cc.name());
            int textPosX = guiTopLeft + 404 - stringWidth / 2;
            if (new Rectangle(textPosX - (selectionPadding = 2), (textPosY = startingY + (this.fontRenderer.FONT_HEIGHT + 10) * count) - selectionPadding, stringWidth + selectionPadding, this.fontRenderer.FONT_HEIGHT + selectionPadding).contains(mouseX, mouseY)) {
                this.searching = false;
                this.searchResults.clear();
                this.searchText.setText("");
                this.searchText.setFocused(false);
                this.category = cc;
                this.entries = LocalCosmeticCache.getCosmeticData(this.mc.player.getUniqueID()).getCosmeticsFromCategory(this.category);
                this.page = 1;
                return;
            }
            ++count;
        }
    }

    @Override
    protected void mouseClickMove(int mouseX, int mouseY, int clickedMouseButton, long timeSinceLastClick) {
        super.mouseClickMove(mouseX, mouseY, clickedMouseButton, timeSinceLastClick);
        if (this.dragging) {
            if (this.lastXPosition != mouseX) {
                int difference = this.lastXPosition - mouseX;
                this.rotate -= (float)(difference * 2);
                if (this.rotate < -360.0f || this.rotate > 360.0f) {
                    this.rotate = 0.0f;
                }
            }
            this.lastXPosition = mouseX;
        }
    }

    @Override
    protected void mouseReleased(int mouseX, int mouseY, int state) {
        if (this.dragging) {
            this.dragging = false;
        }
        super.mouseReleased(mouseX, mouseY, state);
    }

    @Override
    public void handleMouseInput() throws IOException {
        this.handleMouseScroll();
        super.handleMouseInput();
    }

    protected void handleMouseScroll() {
        int j = Mouse.getEventDWheel();
        if (j != 0) {
            int futurePage = this.page -= (j = j > 0 ? 1 : -1);
            this.page = futurePage > this.getMaxPages() ? 1 : (futurePage < 1 ? this.getMaxPages() : futurePage);
        }
    }

    public int getMaxPages() {
        if (this.searching && this.searchResults.isEmpty() || this.entries.isEmpty()) {
            return 1;
        }
        return (int)Math.ceil((double)(this.searching ? this.searchResults.size() : this.entries.size()) / 8.0);
    }
}

