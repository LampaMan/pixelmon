/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui;

import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.network.PixelmonMovesetData;
import net.minecraft.client.resources.I18n;

public class GuiItemMoveSlot {
    private PixelmonMovesetData attack;
    private String name;
    String s;
    private int attackIndex;

    public GuiItemMoveSlot(PixelmonMovesetData[] moveSet, int a) {
        this.attack = moveSet[a];
        this.attackIndex = a;
        this.name = DatabaseMoves.getAttack(this.attack.attackIndex).getAttackBase().getLocalizedName();
        this.s = I18n.format("attack." + this.name + ".name", new Object[0]);
    }

    public int getAttackIndex() {
        return this.attackIndex;
    }

    public String getDisplay() {
        return this.s + " " + this.attack.pp + "/" + this.attack.ppBase;
    }

    public String getName() {
        return this.name;
    }

    public PixelmonMovesetData getAttack() {
        return this.attack;
    }
}

