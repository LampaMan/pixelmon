/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.npc;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCChatting;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;

public class GuiChattingNPC
extends GuiContainer {
    public static ArrayList<String> chatPages;
    public static String name;
    int currentPage = 0;
    int currentTrainerID;

    public GuiChattingNPC(int trainerId) {
        super(new ContainerEmpty());
        Optional<NPCChatting> entityNPCOptional = EntityNPC.locateNPCClient(Minecraft.getMinecraft().world, trainerId, NPCChatting.class);
        if (!entityNPCOptional.isPresent()) {
            Minecraft.getMinecraft().player.closeScreen();
        } else {
            entityNPCOptional.get();
            this.currentTrainerID = trainerId;
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float f, int i, int j) {
        ArrayList<String> chatText = new ArrayList<String>();
        if (this.currentPage < chatPages.size()) {
            chatText.add(chatPages.get(this.currentPage));
        }
        GuiHelper.drawChatBox(this, name, chatText, this.zLevel);
    }

    @Override
    protected void keyTyped(char key, int par2) throws IOException {
        super.keyTyped(key, par2);
        if (par2 == 28) {
            this.continueDialogue();
        }
    }

    @Override
    protected void mouseClicked(int x, int y, int z) throws IOException {
        super.mouseClicked(x, y, z);
        this.continueDialogue();
    }

    private void continueDialogue() {
        ++this.currentPage;
        if (this.currentPage > chatPages.size() - 1) {
            Minecraft.getMinecraft().player.closeScreen();
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
    }
}

