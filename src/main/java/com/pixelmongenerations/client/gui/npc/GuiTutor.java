/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.npc;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.chooseMoveset.GuiMoveList;
import com.pixelmongenerations.client.gui.chooseMoveset.IMoveClicked;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCTutor;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.PixelmonMovesetData;
import com.pixelmongenerations.core.network.packetHandlers.LearnMove;
import java.util.ArrayList;
import java.util.Optional;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.translation.I18n;

public class GuiTutor
extends GuiContainer
implements IMoveClicked {
    NPCTutor tutor;
    public static ArrayList<Attack> attackList;
    public static ArrayList<ArrayList<ItemStack>> costs;
    public char[] able;
    ArrayList<ItemStack> cost;
    int listTop;
    int listLeft;
    int listHeight;
    int listWidth;
    GuiMoveList attackListGui;
    int selectedMove = -1;
    PixelmonData pokemon;

    public GuiTutor(PixelmonData data) {
        super(new ContainerEmpty());
        Optional<NPCTutor> entityNPCOptional = EntityNPC.locateNPCClient(Minecraft.getMinecraft().world, ServerStorageDisplay.NPCInteractId, NPCTutor.class);
        if (!entityNPCOptional.isPresent()) {
            GuiHelper.closeScreen();
            return;
        }
        this.tutor = entityNPCOptional.get();
        this.pokemon = data;
        this.listHeight = 150;
        this.listWidth = 90;
    }

    @Override
    public void initGui() {
        super.initGui();
        if (attackList.isEmpty()) {
            this.mc.player.closeScreen();
            return;
        }
        this.listTop = this.height / 2 - 62;
        this.listLeft = this.width / 2 - 40;
        this.attackListGui = new GuiMoveList(this, attackList, this.listWidth, this.listHeight, this.listTop, this.listLeft, this.mc);
        this.buttonList.add(new GuiButton(1, this.width / 2 + 155, this.height / 2 + 90, 50, 20, I18n.translateToLocal("gui.cancel.text")));
        this.able = new char[attackList.size()];
        for (int i = 0; i < attackList.size(); ++i) {
            for (PixelmonMovesetData move : this.pokemon.moveset) {
                if (move == null || !move.getAttack().equals(attackList.get(i))) continue;
                this.able[i] = 97;
                break;
            }
            if (this.able[i] == 'a') continue;
            this.able[i] = DatabaseMoves.CanLearnAttack(DatabaseMoves.getPokemonID(this.pokemon), attackList.get(i).getAttackBase().getUnlocalizedName()) ? (char)121 : (char)110;
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float mFloat, int mouseX, int mouseY) {
        this.mc.renderEngine.bindTexture(GuiResources.cwPanel);
        GuiHelper.drawImageQuad(0.0, 0.0, this.width, this.height, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        RenderHelper.disableStandardItemLighting();
        String text = I18n.translateToLocal("pixelmon.npc.tutorname");
        this.mc.fontRenderer.drawString(text, this.width / 2 - this.fontRenderer.getStringWidth(text) / 2, this.height / 2 - 110, 0);
        text = I18n.translateToLocal("gui.choosemoveset.choosemove");
        this.mc.fontRenderer.drawString(text, this.width / 2 - this.fontRenderer.getStringWidth(text) / 2, this.height / 2 - 92, 0);
        GuiHelper.bindPokemonSprite(this.pokemon, this.mc);
        GlStateManager.color(1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(this.width / 2 - 211, this.height / 2 - 98 - (this.pokemon.isGen6Sprite() ? 3 : 0), 84.0, 84.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
        this.attackListGui.drawScreen(mouseX, mouseY, mFloat);
        GuiHelper.drawAttackInfoBox(this.zLevel, this.width, this.height);
        text = I18n.translateToLocal("gui.choosemoveset.cost");
        int costTextWidth = this.fontRenderer.getStringWidth(text);
        this.mc.fontRenderer.drawString(text, this.width / 2 + 125 - costTextWidth, this.height / 2 + 60, 0);
        for (int i = 0; i < attackList.size(); ++i) {
            if (!this.attackListGui.isMouseOver(i, mouseX, mouseY)) continue;
            GuiHelper.drawAttackInfoList(attackList.get(i), this.width, this.height);
            this.cost = costs.get(i);
            if (this.cost != null && !this.cost.isEmpty()) {
                int j = 0;
                for (ItemStack item : this.cost) {
                    this.itemRender.renderItemAndEffectIntoGUI(item, this.width / 2 + 125 + j * 20, this.height / 2 + 55);
                    this.itemRender.renderItemOverlayIntoGUI(this.mc.fontRenderer, item, this.width / 2 + 125 + j * 20, this.height / 2 + 55, null);
                    ++j;
                }
            }
            text = this.able[i] == 'y' ? I18n.translateToLocal("gui.tutor.canlearn") : (this.able[i] == 'a' ? I18n.translateToLocal("gui.tutor.already") : I18n.translateToLocal("gui.tutor.cantlearn"));
            this.mc.fontRenderer.drawString(text, this.width / 2 + 125 - costTextWidth, this.height / 2 + 75, 0);
        }
        this.cost = null;
    }

    @Override
    public void elementClicked(ArrayList<Attack> list, int index) {
        if (this.able[index] == 'y') {
            Attack a = attackList.get(index);
            this.mc.player.closeScreen();
            Pixelmon.NETWORK.sendToServer(new LearnMove(this.pokemon.pokemonID, a.getAttackBase().attackIndex, true, this.tutor.getId()));
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        if (button.id == 1) {
            this.mc.player.closeScreen();
        }
    }
}

