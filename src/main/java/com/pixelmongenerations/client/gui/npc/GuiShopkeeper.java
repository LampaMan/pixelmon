/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.npc;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.npc.ClientShopItem;
import com.pixelmongenerations.client.gui.npc.GuiShopContainer;
import com.pixelmongenerations.client.gui.npc.GuiShopkeeperScreens;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCShopkeeper;
import com.pixelmongenerations.common.entity.npcs.registry.EnumBuySell;
import com.pixelmongenerations.common.entity.npcs.registry.ShopkeeperChat;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.npc.ShopKeeperPacket;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.translation.I18n;

public class GuiShopkeeper
extends GuiShopContainer {
    public static String name;
    public static ShopkeeperChat chat;
    GuiShopkeeperScreens screen = GuiShopkeeperScreens.Hello;
    private int shopkeeperID;
    private int mousePressed;

    public GuiShopkeeper(int trainerId) {
        Minecraft mc = Minecraft.getMinecraft();
        Optional<NPCShopkeeper> entityNPCOptional = EntityNPC.locateNPCClient(mc.world, trainerId, NPCShopkeeper.class);
        if (!entityNPCOptional.isPresent()) {
            mc.player.closeScreen();
            return;
        }
        entityNPCOptional.get();
        this.shopkeeperID = trainerId;
    }

    @Override
    public void handleMouseInput() throws IOException {
        super.handleMouseInput();
        if (this.screen != GuiShopkeeperScreens.Buy) {
            return;
        }
        this.handleMouseScroll();
    }

    @Override
    protected void keyTyped(char key, int keyCode) throws IOException {
        if (this.screen == GuiShopkeeperScreens.Buy && keyCode == 1) {
            this.closeScreen();
        } else {
            super.keyTyped(key, keyCode);
        }
        if (keyCode == 28) {
            this.continueDialogue();
        }
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        this.mousePressed = 1;
        this.continueDialogue();
        if (this.screen == GuiShopkeeperScreens.Buy) {
            this.clickBuyScreen(mouseX, mouseY);
            if (this.currentTab == EnumBuySell.Buy && mouseX > this.width / 2 + 28 && mouseX < this.width / 2 + 28 + 58 && mouseY > this.height / 2 - 93 && mouseY < this.height / 2 - 93 + 30) {
                this.currentTab = EnumBuySell.Sell;
                this.selectedItem = -1;
            } else if (this.currentTab == EnumBuySell.Sell && mouseX > this.width / 2 - 31 && mouseX < this.width / 2 - 31 + 58 && mouseY > this.height / 2 - 93 && mouseY < this.height / 2 - 93 + 30) {
                this.currentTab = EnumBuySell.Buy;
                this.selectedItem = -1;
            }
        }
    }

    @Override
    protected void mouseReleased(int mouseX, int mouseY, int state) {
        this.mousePressed = 0;
    }

    private void continueDialogue() {
        if (this.screen == GuiShopkeeperScreens.Hello) {
            this.screen = GuiShopkeeperScreens.Buy;
            return;
        }
        if (this.screen == GuiShopkeeperScreens.Goodbye) {
            Minecraft.getMinecraft().player.closeScreen();
        }
    }

    @Override
    protected void closeScreen() {
        this.screen = GuiShopkeeperScreens.Goodbye;
    }

    @Override
    protected void sendBuyPacket() {
        Pixelmon.NETWORK.sendToServer(new ShopKeeperPacket(EnumBuySell.Buy, this.shopkeeperID, ((ClientShopItem)buyItems.get(this.selectedItem)).getItemID(), this.quantity));
    }

    @Override
    protected void sendSellPacket() {
        Pixelmon.NETWORK.sendToServer(new ShopKeeperPacket(EnumBuySell.Sell, this.shopkeeperID, ((ClientShopItem)sellItems.get(this.selectedItem)).getItemID(), this.quantity));
    }

    @Override
    public void drawGuiContainerBackgroundLayer(float f, int mouseX, int mouseY) {
        NonNullList<ItemStack> inv = Minecraft.getMinecraft().player.inventory.mainInventory;
        for (int i = 0; i < sellItems.size(); ++i) {
            ItemStack item = ((ClientShopItem)sellItems.get(i)).getItemStack();
            ((ClientShopItem)GuiShopkeeper.sellItems.get((int)i)).amount = 0;
            for (int j = 0; j < inv.size(); ++j) {
                if (inv.get(j) == null || !ItemStack.areItemsEqual(item, inv.get(j)) || !ItemStack.areItemStackTagsEqual(item, inv.get(j)) || item.getItemDamage() != inv.get(j).getItemDamage()) continue;
                ((ClientShopItem)GuiShopkeeper.sellItems.get((int)i)).amount += inv.get(j).getCount();
            }
        }
        if (this.screen == GuiShopkeeperScreens.Hello || this.screen == GuiShopkeeperScreens.Goodbye) {
            ArrayList<String> chatText = new ArrayList<String>();
            if (this.screen == GuiShopkeeperScreens.Hello) {
                chatText.add(GuiShopkeeper.chat.hello);
            } else {
                chatText.add(GuiShopkeeper.chat.goodbye);
            }
            GuiHelper.drawChatBox(this, name, chatText, this.zLevel);
        } else {
            this.renderBuyScreen(mouseX, mouseY);
            int colour = 0xFFFFFF;
            if (this.currentTab == EnumBuySell.Sell && mouseX > this.width / 2 - 31 && mouseX < this.width / 2 - 31 + 58 && mouseY > this.height / 2 - 93 && mouseY < this.height / 2 - 93 + 30) {
                colour = 0xCCCCCC;
            }
            String buyLabel = I18n.translateToLocal("gui.shopkeeper.buy");
            this.drawString(this.mc.fontRenderer, buyLabel, this.width / 2 - 2 - this.mc.fontRenderer.getStringWidth(buyLabel) / 2, this.height / 2 - 82, colour);
            colour = 0xFFFFFF;
            if (this.currentTab == EnumBuySell.Buy && mouseX > this.width / 2 + 28 && mouseX < this.width / 2 + 28 + 58 && mouseY > this.height / 2 - 93 && mouseY < this.height / 2 - 93 + 30) {
                colour = 0xCCCCCC;
            }
            String sellLabel = I18n.translateToLocal("gui.shopkeeper.sell");
            this.drawString(this.mc.fontRenderer, sellLabel, this.width / 2 + 57 - this.mc.fontRenderer.getStringWidth(sellLabel) / 2, this.height / 2 - 82, colour);
        }
        if (this.mousePressed > 0 && ++this.mousePressed > 5) {
            this.clickBuyMiniScreen(mouseX, mouseY, false);
        }
    }
}

