/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.npc;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.npc.GuiTradeYesNo;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCTrader;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.database.DatabaseStats;
import com.pixelmongenerations.core.network.PixelmonData;
import java.io.IOException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;

public class GuiNPCTrader
extends GuiContainer {
    private NPCTrader trader;
    private NPCTrader.TradePair pair;
    private int offerSlot;
    private int left;
    private int top;

    public GuiNPCTrader(NPCTrader trader) {
        super(new ContainerEmpty());
        this.trader = trader;
        if (this.trader == null) {
            return;
        }
        this.trader.updateTradePair();
        this.pair = this.trader.getTrade();
    }

    public GuiNPCTrader(int traderId) {
        this((NPCTrader)EntityNPC.locateNPCClient(Minecraft.getMinecraft().world, traderId, NPCTrader.class).orElse(null));
    }

    @Override
    public void initGui() {
        if (this.trader == null) {
            return;
        }
        super.initGui();
        this.left = (this.width - this.xSize) / 2;
        this.top = (this.height - this.ySize) / 2;
        this.buttonList.add(new GuiButton(1, this.width / 2 + 155, this.height / 2 + 90, 30, 20, I18n.translateToLocal("gui.guiItemDrops.ok")));
        this.offerSlot = GuiNPCTrader.getAvailableSlot(this.pair.exchangefor.toString());
        if (this.offerSlot != -1) {
            this.buttonList.add(new GuiButton(2, this.width / 2 - 25, this.height / 2 - 50, 60, 20, I18n.translateToLocal("gui.trading.trade")));
        }
    }

    public static int getAvailableSlot(String exchange) {
        PixelmonData[] pokemon = ServerStorageDisplay.getPokemon();
        int pokemonLength = pokemon.length;
        for (int i = 0; i < pokemonLength; ++i) {
            PixelmonData data = pokemon[i];
            if (data == null || !data.name.equalsIgnoreCase(exchange)) continue;
            return i;
        }
        return -1;
    }

    @Override
    public void updateScreen() {
        if (this.trader == null) {
            this.mc.player.closeScreen();
        } else {
            super.updateScreen();
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        if (this.trader == null) {
            return;
        }
        this.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3) {
        if (this.trader == null) {
            return;
        }
        int offer = DatabaseStats.getBaseStats((String)this.pair.offer.name).get().nationalPokedexNumber;
        int exchange = DatabaseStats.getBaseStats((String)this.pair.exchangefor.name).get().nationalPokedexNumber;
        Minecraft mc = Minecraft.getMinecraft();
        GuiHelper.bindPokemonSprite(offer, this.trader.getForm(), Gender.Male, this.trader.getIsShiny(), mc);
        GuiHelper.drawImageQuad(this.width / 2 - 120, this.height / 2 - 100 + (offer <= 649 ? 0 : 12), 86.0, 86.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        GuiHelper.bindPokemonSprite(exchange, 0, Gender.Male, false, mc);
        GuiHelper.drawImageQuad(this.width / 2 + 40, this.height / 2 - 100 + (exchange <= 649 ? 0 : 12), 86.0, 86.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.npctrader.yours"), this.left + 145, this.top, 0xFFFFFF);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.npctrader.their"), this.left - 20, this.top, 0xFFFFFF);
        String s3 = "pixelmon." + this.pair.offer.name.toLowerCase() + ".name";
        if (this.trader.getIsShiny()) {
            this.mc.fontRenderer.drawString((Object)((Object)TextFormatting.YELLOW) + I18n.translateToLocal(s3), this.left - 10, this.top + 70, 0xFFFFFF);
        } else {
            this.mc.fontRenderer.drawString(I18n.translateToLocal(s3), this.left - 10, this.top + 70, 0xFFFFFF);
        }
        this.mc.fontRenderer.drawString(I18n.translateToLocal("pixelmon." + this.pair.exchangefor.name.toLowerCase() + ".name"), this.left + 155, this.top + 70, 0xFFFFFF);
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        if (this.trader == null) {
            return;
        }
        if (button.id == 1) {
            this.mc.player.closeScreen();
            this.mc.setRenderViewEntity(Minecraft.getMinecraft().player);
        } else if (button.id == 2) {
            this.mc.displayGuiScreen(new GuiTradeYesNo(this.offerSlot, this.trader));
        } else {
            super.actionPerformed(button);
        }
    }
}

