/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.npc;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.npc.GuiNPCTrader;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCTrader;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.AcceptNPCTradePacket;
import com.pixelmongenerations.core.util.RegexPatterns;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.text.translation.I18n;

public class GuiTradeYesNo
extends GuiScreen {
    private NPCTrader trader;
    private NPCTrader.TradePair pair;
    private int offerSlot;

    public GuiTradeYesNo(int offerSlot, NPCTrader trader) {
        this.trader = trader;
        this.offerSlot = offerSlot;
        if (this.trader == null) {
            return;
        }
        this.trader.updateTradePair();
        this.pair = trader.getTrade();
        if (this.offerSlot == -1) {
            this.offerSlot = GuiNPCTrader.getAvailableSlot(this.pair.exchangefor.toString());
        }
    }

    public GuiTradeYesNo(int traderId) {
        this(-1, EntityNPC.locateNPCClient(Minecraft.getMinecraft().world, traderId, NPCTrader.class).orElse(null));
    }

    @Override
    public void updateScreen() {
        if (this.trader == null) {
            this.mc.player.closeScreen();
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        if (this.trader == null) {
            return;
        }
        this.mc.renderEngine.bindTexture(GuiResources.yesNo);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(this.width / 2 - 128, this.height / 2 - 50, 256.0, 100.0f, 0.0, 0.0, 1.0, 0.78125, this.zLevel);
        String offer = I18n.translateToLocal("pixelmon." + this.pair.offer.name.toLowerCase() + ".name");
        String e = I18n.translateToLocal("pixelmon." + this.pair.exchangefor.name.toLowerCase() + ".name");
        String s = RegexPatterns.$_O_VAR.matcher(RegexPatterns.$_E_VAR.matcher(I18n.translateToLocal("gui.yesno.trade")).replaceAll(e)).replaceAll(offer);
        float textAreaWidth = 170.0f;
        float textWidth = this.mc.fontRenderer.getStringWidth(s);
        int numLines = (int)(textWidth / textAreaWidth) + 1;
        this.mc.fontRenderer.drawSplitString(s, this.width / 2 - 109, this.height / 2 + 1 - numLines * 10 / 2, (int)textAreaWidth, 0);
        this.mc.fontRenderer.drawSplitString(s, this.width / 2 - 110, this.height / 2 - numLines * 10 / 2, (int)textAreaWidth, 0xFFFFFF);
        this.mc.renderEngine.bindTexture(GuiResources.yesNo);
        if (mouseX > this.width / 2 + 63 && mouseX < this.width / 2 + 108 && mouseY > this.height / 2 - 33 && mouseY < this.height / 2 - 7) {
            GuiHelper.drawImageQuad(this.width / 2 + 63, this.height / 2 - 33, 45.0, 26.0f, 0.6015625, 0.7890625, 0.77734375, 0.9921875, this.zLevel);
        }
        if (mouseX > this.width / 2 + 63 && mouseX < this.width / 2 + 108 && mouseY > this.height / 2 + 5 && mouseY < this.height / 2 + 31) {
            GuiHelper.drawImageQuad(this.width / 2 + 63, this.height / 2 + 5, 45.0, 26.0f, 0.6015625, 0.7890625, 0.77734375, 0.9921875, this.zLevel);
        }
        this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.yesno.yes"), this.width / 2 + 76, this.height / 2 - 23, 0xFFFFFF);
        this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.yesno.no"), this.width / 2 + 80, this.height / 2 + 15, 0xFFFFFF);
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        if (this.trader == null) {
            return;
        }
        if (mouseX > this.width / 2 + 63 && mouseX < this.width / 2 + 108 && mouseY > this.height / 2 - 33 && mouseY < this.height / 2 - 7) {
            Pixelmon.NETWORK.sendToServer(new AcceptNPCTradePacket(this.offerSlot, this.trader.getId()));
            this.mc.player.closeScreen();
        }
        if (mouseX > this.width / 2 + 63 && mouseX < this.width / 2 + 108 && mouseY > this.height / 2 + 5 && mouseY < this.height / 2 + 31) {
            this.mc.player.closeScreen();
        }
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }
}

