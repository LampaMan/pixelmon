/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.ranchblock;

import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.ranch.EnumRanchServerPacketMode;
import com.pixelmongenerations.core.network.packetHandlers.ranch.RanchBlockServerPacket;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.text.translation.I18n;

public class GuiExtendRanch
extends GuiContainer {
    public static int[] pos;
    public static boolean[] extendDirections;

    public GuiExtendRanch() {
        super(new ContainerEmpty());
    }

    @Override
    public void initGui() {
        super.initGui();
        if (extendDirections[0]) {
            this.buttonList.add(new GuiButton(0, this.width / 2 - 60, this.height / 2 - 10, 40, 20, I18n.translateToLocal("gui.ranch.extendplusz")));
        }
        if (extendDirections[1]) {
            this.buttonList.add(new GuiButton(1, this.width / 2 + 20, this.height / 2 - 50, 40, 20, I18n.translateToLocal("gui.ranch.extendminusx")));
        }
        if (extendDirections[2]) {
            this.buttonList.add(new GuiButton(2, this.width / 2 + 20, this.height / 2 - 10, 40, 20, I18n.translateToLocal("gui.ranch.extendminusz")));
        }
        if (extendDirections[3]) {
            this.buttonList.add(new GuiButton(3, this.width / 2 - 60, this.height / 2 - 50, 40, 20, I18n.translateToLocal("gui.ranch.extendplusx")));
        }
        this.buttonList.add(new GuiButton(4, this.width / 2 - 30, this.height / 2 + 30, 60, 20, I18n.translateToLocal("gui.cancel.text")));
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
        String s = I18n.translateToLocal("gui.ranch.extendTitle");
        this.mc.fontRenderer.drawString(s, this.width / 2 - this.mc.fontRenderer.getStringWidth(s) / 2, this.height / 2 - 80, 0xFFFFFF);
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        if (button.id == 0) {
            Pixelmon.NETWORK.sendToServer(new RanchBlockServerPacket(pos[0], pos[1], pos[2], EnumRanchServerPacketMode.ExtendRanch, new boolean[]{true, false, false, false}));
        } else if (button.id == 1) {
            Pixelmon.NETWORK.sendToServer(new RanchBlockServerPacket(pos[0], pos[1], pos[2], EnumRanchServerPacketMode.ExtendRanch, new boolean[]{false, true, false, false}));
        } else if (button.id == 2) {
            Pixelmon.NETWORK.sendToServer(new RanchBlockServerPacket(pos[0], pos[1], pos[2], EnumRanchServerPacketMode.ExtendRanch, new boolean[]{false, false, true, false}));
        } else if (button.id == 3) {
            Pixelmon.NETWORK.sendToServer(new RanchBlockServerPacket(pos[0], pos[1], pos[2], EnumRanchServerPacketMode.ExtendRanch, new boolean[]{false, false, false, true}));
        }
        this.mc.player.closeScreen();
    }
}

