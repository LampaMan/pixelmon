/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.gui;

import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.proxy.ClientProxy;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.gui.inventory.GuiContainer;
import org.lwjgl.opengl.GL11;

public class GuiBigImage
extends GuiContainer {
    private String texture;
    private int width;
    private int height;

    public GuiBigImage(String texture, int width, int height) {
        super(new ContainerEmpty());
        this.texture = texture;
        this.width = width;
        this.height = height;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        if (this.texture == null) {
            return;
        }
        ScaledResolution scaledRes = new ScaledResolution(this.mc);
        GL11.glColor3f((float)1.0f, (float)1.0f, (float)1.0f);
        TextureResource texture = ClientProxy.TEXTURE_STORE.getObject(this.texture);
        if (texture != null) {
            texture.bindTexture();
            this.width = scaledRes.getScaledWidth() / 2;
            this.height = (int)((double)scaledRes.getScaledHeight() / 1.3);
            int x = scaledRes.getScaledWidth() / 2 - this.width / 2;
            int y = scaledRes.getScaledHeight() / 2 - this.height / 2 + -14;
            GuiHelper.drawImageQuad(x, y, this.width, this.height, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            this.fontRenderer.setUnicodeFlag(false);
            GuiHelper.drawString(this.fontRenderer, "Press 'Esc.' to close", (int)((double)(scaledRes.getScaledWidth() / 2) - (double)(this.fontRenderer.getStringWidth("Press 'Esc.' to close") / 2) * 1.3), y + this.height - 2, 1.3f, 16771202, false);
        }
    }
}

