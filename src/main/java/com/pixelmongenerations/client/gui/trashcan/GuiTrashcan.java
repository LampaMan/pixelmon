/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.trashcan;

import com.pixelmongenerations.client.gui.trashcan.ContainerTrashcan;
import com.pixelmongenerations.common.block.tileEntities.TileEntityTrashcan;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiTrashcan
extends GuiContainer {
    private static final ResourceLocation TEXTURES = new ResourceLocation("pixelmon:textures/gui/trashcan/trashcan.png");
    private final InventoryPlayer player;
    private final TileEntityTrashcan tileentity;

    public GuiTrashcan(InventoryPlayer player, TileEntityTrashcan tileentity) {
        super(new ContainerTrashcan(player));
        this.player = player;
        this.tileentity = tileentity;
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        String tileName = this.tileentity.getDisplayName().getUnformattedText();
        this.fontRenderer.drawString(tileName, this.xSize / 2 - this.fontRenderer.getStringWidth(tileName) / 2 + 3, 8, 0x404040);
        this.fontRenderer.drawString(this.player.getDisplayName().getUnformattedText(), 122, this.ySize - 96 + 2, 0x404040);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.mc.getTextureManager().bindTexture(TEXTURES);
        this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);
    }
}

