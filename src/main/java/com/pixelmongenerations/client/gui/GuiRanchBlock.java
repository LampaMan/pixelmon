/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;

public class GuiRanchBlock
extends GuiContainer {
    public GuiRanchBlock() {
        super(new ContainerEmpty());
    }

    @Override
    public void initGui() {
        super.initGui();
        this.buttonList.add(new GuiButton(0, this.width / 2 - 140, this.height / 3, 120, 20, I18n.format("gui.guiRanch.selectPokemon", new Object[0])));
        this.buttonList.add(new GuiButton(1, this.width / 2 + 20, this.height / 3, 120, 20, I18n.format("gui.guiRanch.selectPokemon", new Object[0])));
        this.buttonList.add(new GuiButton(2, this.width / 2 - 60, this.height * 2 / 3, 120, 20, I18n.format("gui.guiRanch.claimEgg", new Object[0])));
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float par3, int par1, int par2) {
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.mc.renderEngine.bindTexture(GuiResources.cwPanel);
        GuiHelper.drawImageQuad(0.0, 0.0, this.width, this.height, 0.0, 0.0, 1.0, 1.0, this.zLevel);
    }
}

