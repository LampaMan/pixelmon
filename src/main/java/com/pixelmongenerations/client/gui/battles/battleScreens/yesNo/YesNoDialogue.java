/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.battleScreens.yesNo;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.battleScreens.BattleScreen;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.text.translation.I18n;

public abstract class YesNoDialogue
extends BattleScreen {
    public YesNoDialogue(GuiBattle parent, BattleMode mode) {
        super(parent, mode);
    }

    @Override
    public void drawScreen(int width, int height, int mouseX, int mouseY) {
        this.mc.renderEngine.bindTexture(GuiResources.yesNo);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(width / 2 - 128, height / 2 - 50, 256.0, 100.0f, 0.0, 0.0, 1.0, 0.78125, this.zLevel);
        this.drawConfirmText(width, height);
        this.mc.renderEngine.bindTexture(GuiResources.yesNo);
        if (mouseX > width / 2 + 63 && mouseX < width / 2 + 108 && mouseY > height / 2 - 33 && mouseY < height / 2 - 7) {
            GuiHelper.drawImageQuad(width / 2 + 63, height / 2 - 33, 45.0, 26.0f, 0.6015625, 0.7890625, 0.77734375, 0.9921875, this.zLevel);
        }
        if (mouseX > width / 2 + 63 && mouseX < width / 2 + 108 && mouseY > height / 2 + 5 && mouseY < height / 2 + 31) {
            GuiHelper.drawImageQuad(width / 2 + 63, height / 2 + 5, 45.0, 26.0f, 0.6015625, 0.7890625, 0.77734375, 0.9921875, this.zLevel);
        }
        this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.yesno.yes"), width / 2 + 76, height / 2 - 23, 0xFFFFFF);
        this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.yesno.no"), width / 2 + 80, height / 2 + 15, 0xFFFFFF);
    }

    protected abstract void drawConfirmText(int var1, int var2);

    @Override
    public void click(int width, int height, int mouseX, int mouseY) {
        if (mouseX > width / 2 + 63 && mouseX < width / 2 + 108) {
            if (mouseY > height / 2 - 33 && mouseY < height / 2 - 7) {
                this.confirm();
            } else if (mouseY > height / 2 + 5 && mouseY < height / 2 + 31) {
                this.bm.mode = this.bm.oldMode;
            }
        }
    }

    protected abstract void confirm();
}

