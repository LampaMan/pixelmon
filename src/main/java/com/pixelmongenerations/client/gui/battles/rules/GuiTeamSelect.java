/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.rules;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.battles.ClientBattleManager;
import com.pixelmongenerations.client.gui.battles.GuiVersus;
import com.pixelmongenerations.client.gui.battles.rules.TeamSelectPokemonIcon;
import com.pixelmongenerations.client.gui.elements.GuiRoundButton;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClause;
import com.pixelmongenerations.common.battle.rules.teamselection.TeamSelectPokemon;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.battles.rules.ConfirmTeamSelect;
import com.pixelmongenerations.core.network.packetHandlers.battles.rules.ShowTeamSelect;
import com.pixelmongenerations.core.network.packetHandlers.battles.rules.UnconfirmTeamSelect;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.text.translation.I18n;

public class GuiTeamSelect
extends GuiVersus {
    public static ShowTeamSelect teamSelectPacket;
    private NPCTrainer trainer;
    private GuiRoundButton confirmButton = new GuiRoundButton(85, 152, I18n.translateToLocal("gui.battlerules.selectteam"));
    private boolean confirmed;
    private boolean timeExpired;
    private ClientBattleManager bm = ClientProxy.battleManager;
    private List<TeamSelectPokemonIcon> icons = new ArrayList<TeamSelectPokemonIcon>();
    private int numSelected;
    public String rejectClause = "";
    private static final String SELECT_TEAM = "gui.battlerules.selectteam";

    public GuiTeamSelect() {
        Optional<NPCTrainer> entityNPCOptional;
        this.bm.setTeamSelectTime();
        if (GuiTeamSelect.teamSelectPacket.npcID != -1 && (entityNPCOptional = EntityNPC.locateNPCClient(Minecraft.getMinecraft().world, GuiTeamSelect.teamSelectPacket.npcID, NPCTrainer.class)).isPresent()) {
            this.trainer = entityNPCOptional.get();
            this.isNPC = true;
        }
    }

    @Override
    public void initGui() {
        int i;
        super.initGui();
        if (this.icons.isEmpty()) {
            i = 0;
            for (PixelmonData data : ServerStorageDisplay.getPokemon()) {
                TeamSelectPokemon pokemon = null;
                if (data != null) {
                    pokemon = new TeamSelectPokemon(data);
                }
                this.icons.add(new TeamSelectPokemonIcon(pokemon, GuiTeamSelect.teamSelectPacket.disabled[i]));
                ++i;
            }
            if (this.bm.rules.teamPreview) {
                for (TeamSelectPokemon pokemon : GuiTeamSelect.teamSelectPacket.opponentTeam) {
                    this.icons.add(new TeamSelectPokemonIcon(pokemon, "n"));
                }
            }
        }
        i = 0;
        for (TeamSelectPokemonIcon icon : this.icons) {
            if (i < 6) {
                icon.setPosition(this.playerPartyX + i * 12, this.playerPartyY);
            } else {
                icon.setPosition(this.opponentPartyX + (i - 6) * 12, this.opponentPartyY);
            }
            ++i;
        }
    }

    @Override
    protected void keyTyped(char key, int keyCode) {
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        super.drawGuiContainerBackgroundLayer(partialTicks, mouseX, mouseY);
        if (this.ticks == 0) {
            boolean canHighlight = this.numSelected > 0;
            int currentX = canHighlight ? mouseX : 0;
            int currentY = canHighlight ? mouseY : 0;
            this.confirmButton.drawButton(this.leftX, this.topY, currentX, currentY, this.zLevel, this.confirmed);
        }
        int i = 0;
        for (TeamSelectPokemonIcon icon : this.icons) {
            icon.setTickOffset(i++ < 6 ? this.offset1 : this.offset2);
            icon.drawIcon(mouseX, mouseY, this.zLevel, this.isFullSelect());
        }
        if (!this.bm.rules.teamPreview) {
            this.drawOpponentPokeBalls(GuiTeamSelect.teamSelectPacket.opponentSize);
        }
        if (this.bm.afkOn) {
            GuiHelper.drawBattleTimer(this, this.bm.afkTime);
            if (this.bm.afkTime <= 0 && !this.timeExpired) {
                this.confirmed = true;
                this.timeExpired = true;
                Pixelmon.NETWORK.sendToServer(new ConfirmTeamSelect(GuiTeamSelect.teamSelectPacket.teamSelectID, this.getSelectionOrder(), true));
            }
        }
        String centerMessage = "";
        if (!this.rejectClause.isEmpty()) {
            centerMessage = I18n.translateToLocal("gui.battlerules.teamviolated") + " " + BattleClause.getLocalizedName(this.rejectClause);
            this.confirmed = false;
        } else if (this.confirmed) {
            centerMessage = I18n.translateToLocal("gui.battlerules.waitselect");
        }
        if (!centerMessage.isEmpty()) {
            GuiHelper.renderTooltip(this.leftX + 140, this.topY + 60, this.mc.fontRenderer.listFormattedStringToWidth(centerMessage, 100), Color.BLUE.getRGB(), Color.BLACK.getRGB(), 200, true, true);
        }
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int par3) {
        if (this.timeExpired) {
            return;
        }
        if (!this.confirmed) {
            for (TeamSelectPokemonIcon icon : this.icons) {
                if (!icon.isMouseOver(mouseX, mouseY) || icon.isDisabled()) continue;
                if (icon.selectIndex == -1) {
                    if (this.isFullSelect()) break;
                    icon.selectIndex = this.numSelected++;
                    if (this.numSelected == 1) {
                        this.confirmButton.setText(I18n.translateToLocal("gui.battlerules.confirm"));
                    }
                } else {
                    for (TeamSelectPokemonIcon icon2 : this.icons) {
                        if (icon2.selectIndex <= icon.selectIndex) continue;
                        --icon2.selectIndex;
                    }
                    icon.selectIndex = -1;
                    if (--this.numSelected == 0) {
                        this.confirmButton.setText(I18n.translateToLocal(SELECT_TEAM));
                    }
                }
                this.rejectClause = "";
                break;
            }
        }
        if (this.numSelected > 0 && this.confirmButton.isMouseOver(this.leftX, this.topY, mouseX, mouseY)) {
            this.confirmed = !this.confirmed;
            boolean bl = this.confirmed;
            if (this.confirmed) {
                Pixelmon.NETWORK.sendToServer(new ConfirmTeamSelect(GuiTeamSelect.teamSelectPacket.teamSelectID, this.getSelectionOrder(), false));
            } else {
                Pixelmon.NETWORK.sendToServer(new UnconfirmTeamSelect(GuiTeamSelect.teamSelectPacket.teamSelectID));
            }
        }
    }

    private boolean isFullSelect() {
        return this.numSelected >= this.bm.rules.numPokemon;
    }

    @Override
    protected EntityLivingBase getOpponent() {
        if (GuiTeamSelect.teamSelectPacket.opponentUUID == null) {
            return this.trainer;
        }
        return this.mc.world.getPlayerEntityByUUID(GuiTeamSelect.teamSelectPacket.opponentUUID);
    }

    private int[] getSelectionOrder() {
        int[] selection = new int[6];
        for (int i = 0; i < selection.length; ++i) {
            selection[i] = this.icons.get((int)i).selectIndex;
        }
        return selection;
    }
}

