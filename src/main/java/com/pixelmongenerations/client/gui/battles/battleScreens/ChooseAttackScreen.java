/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.apache.commons.lang3.text.WordUtils
 */
package com.pixelmongenerations.client.gui.battles.battleScreens;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.client.SoundHelper;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.battleScreens.BattleScreen;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.MaxAttackHelper;
import com.pixelmongenerations.common.battle.attacks.TargetingInfo;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.Judgment;
import com.pixelmongenerations.common.item.heldItems.ZCrystal;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.event.EntityPlayerExtension;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.PixelmonMovesetData;
import com.pixelmongenerations.core.network.packetHandlers.battles.ChooseAttack;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.client.config.GuiUtils;
import org.apache.commons.lang3.text.WordUtils;

public class ChooseAttackScreen
extends BattleScreen {
    private static final int MEGA_BUTTON = 4;
    private static final int BUTTON_WIDTH = 87;
    private static final int BUTTON_HEIGHT = 20;
    private int megaAnimation;
    private int blink = 0;
    public boolean usedZMove = false;
    private boolean zMoveActivated = false;
    public static ResourceLocation prefix = new ResourceLocation("pixelmon:textures/");
    public static ResourceLocation sparkleButton = new ResourceLocation(prefix + "gui/battlegui2-sparkle.png");
    public static ResourceLocation zIndicator = new ResourceLocation(prefix + "gui/battle/z-indicator.png");
    public static ResourceLocation megeEvolution = new ResourceLocation(prefix + "gui/battle/evolution.png");
    public static ResourceLocation ultraBurst = new ResourceLocation(prefix + "gui/battle/ultraburst.png");
    public static ResourceLocation dynamax = new ResourceLocation(prefix + "gui/battle/dynamax.png");
    public static ResourceLocation dynamaxHover = new ResourceLocation(prefix + "gui/battle/dynamaxhover.png");

    public ChooseAttackScreen(GuiBattle parent) {
        super(parent, BattleMode.ChooseAttack);
    }

    @Override
    public void drawScreen(int width, int height, int mouseX, int mouseY) {
        this.mc.renderEngine.bindTexture(GuiResources.battleGui2);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        int guiWidth = this.parent.getGuiWidth();
        int guiHeight = this.parent.getGuiHeight();
        PixelmonData pokemon = this.bm.getUserPokemonPacket();
        GuiHelper.drawImageQuad(width / 2 - guiWidth / 2, height - guiHeight, guiWidth, guiHeight, 0.0, 0.0, 1.0, 0.30416667461395264, this.zLevel);
        if (pokemon != null) {
            int top;
            int left;
            PixelmonMovesetData[] moveset = pokemon.moveset;
            int numMoves = pokemon.numMoves;
            if (moveset[0] == null || moveset[0].getAttack() == null || moveset[0].getAttack().getAttackBase() == null) {
                numMoves = 0;
            }

            boolean hasZMove = false;
            int strongestZMoveIndex = 0;
            int strongestBasePower = 0;
            boolean[] isZMove = new boolean[4];
            if (pokemon.heldItem != null && pokemon.heldItem.getItem() instanceof ZCrystal) {
                ZCrystal crystal = (ZCrystal)pokemon.heldItem.getItem();
                for (int i = 0; i < moveset.length; ++i) {
                    PixelmonMovesetData move = moveset[i];
                    if (move != null && move.getAttack() != null && crystal.canEffectMove((new PokemonSpec(pokemon.name)).fromData(pokemon), move.getAttack())) {
                        hasZMove = true;
                        isZMove[i] = true;
                        if (crystal.getUsableAttack(move.getAttack()).movePower > strongestBasePower) {
                            strongestBasePower = crystal.getUsableAttack(move.getAttack()).movePower;
                            strongestZMoveIndex = i;
                        }
                    }
                }
            }

            int baseHeight = height - guiHeight + 9;
            int baseWidth = width / 2 - 50;

            for (int i = 0; i < numMoves; ++i) {
                int x = baseWidth + (i % 2 == 1 ? 0 : -91);
                int y = baseHeight + (i / 2 == 0 ? 0 : 24);
                if (!this.bm.dynamaxing && (!Arrays.equals(pokemon.pokemonID, this.bm.dynamax) || this.bm.hasDynamaxed)) {
                    if (pokemon.heldItem != null && pokemon.heldItem.getItem() instanceof ZCrystal && isZMove[i] && this.zMoveActivated && !this.usedZMove) {
                        this.mc.renderEngine.bindTexture(strongestZMoveIndex == i && this.blink <= 25 ? sparkleButton : GuiResources.battleGui2);
                        ZCrystal crystal = (ZCrystal) pokemon.heldItem.getItem();
                        this.parent.drawButton(BattleMode.ChooseAttack, x, y, BUTTON_WIDTH, BUTTON_HEIGHT, crystal.getUsableAttack(moveset[i].getAttack()).getAttackBase().getLocalizedName(), moveset[i].disabled, mouseX, mouseY, i);
                        this.mc.renderEngine.bindTexture(zIndicator);
                        GuiHelper.drawImageQuad((double) x, (double) y, 8.0D, 8.0F, 0.0D, 0.0D, 1.0D, 1.0D, this.zLevel);
                    } else {
                        this.mc.renderEngine.bindTexture(GuiResources.battleGui2);
                        this.parent.drawButton(BattleMode.ChooseAttack, x, y, BUTTON_WIDTH, BUTTON_HEIGHT, moveset[i].getAttack().getAttackBase().getLocalizedName(), moveset[i].disabled, mouseX, mouseY, i);
                        if (isZMove[i] && !this.usedZMove) {
                            this.mc.renderEngine.bindTexture(zIndicator);
                            GuiHelper.drawImageQuad((double) x, (double) y, 8.0D, 8.0F, 0.0D, 0.0D, 1.0D, 1.0D, this.zLevel);
                        }
                    }
                } else {
                    this.mc.renderEngine.bindTexture(GuiResources.battleGui2);
                    Attack attack = MaxAttackHelper.getMaxMove(pokemon, moveset[i].getAttack());
                    this.parent.drawButton(BattleMode.ChooseAttack, x, y, BUTTON_WIDTH, BUTTON_HEIGHT, attack.getAttackBase().getLocalizedName(), false, mouseX, mouseY, i);
                }
            }

            if (mouseX > width / 2 + 42 && mouseX < width / 2 + 52 && mouseY > height - 11 && mouseY < height - 1) {
                this.mc.renderEngine.bindTexture(GuiResources.battleGui2);
                GuiHelper.drawImageQuad(width / 2 + 42, height - 12, 11.0, 11.0f, 0.957812488079071, 0.31458333134651184, 0.9921875, 0.35625f, this.zLevel);
            }

            if (hasZMove && !this.usedZMove) {
                left = (width - 287) / 2;
                top = height - guiHeight - 20;
                this.mc.renderEngine.bindTexture(GuiResources.battleGui2);
                this.parent.drawButton(BattleMode.ChooseAttack, left, top, BUTTON_WIDTH, BUTTON_HEIGHT, I18n.translateToLocal("gui.mainmenu.zmove"), this.zMoveActivated, mouseX, mouseY, 5);
                if (this.zMoveActivated) {
                    int numColors = 10;
                    int posOffset = BUTTON_WIDTH / numColors;
                    int colorLeft = left;
                    int colorRight = left + posOffset;
                    int alpha = 100;
                    float colorOffset = (float)this.megaAnimation / 255.0f;
                    for (int i = 0; i < numColors; ++i) {
                        int alphaMask = alpha << 24 | 0xFFFFFF;
                        float startHue = (float)i / (float)numColors + colorOffset;
                        startHue -= (float)((int)startHue);
                        float endHue = (float)(i + 1) / (float)numColors + colorOffset;
                        endHue -= (float)((int)endHue);
                        int startColor = Color.HSBtoRGB(startHue, 1.0f, 1.0f) & alphaMask;
                        int endColor = Color.HSBtoRGB(endHue, 1.0f, 1.0f) & alphaMask;
                        if (i == numColors - 1) {
                            colorRight = left + BUTTON_WIDTH;
                        }
                        GuiHelper.drawGradientRect(colorLeft, top, this.zLevel, colorRight, top + 20, startColor, endColor, true);
                        colorLeft += posOffset;
                        colorRight += posOffset;
                    }
                }
            }
            if (pokemon.canMegaEvolve() && EntityPlayerExtension.getPlayerMegaItem(Minecraft.getMinecraft().player).canEvolve()) {
                this.mc.renderEngine.bindTexture(GuiResources.battleGui2);
                left = (width - BUTTON_WIDTH) / 2;
                top = height - guiHeight - BUTTON_HEIGHT;
                this.parent.drawButton(BattleMode.ChooseAttack, left, top, BUTTON_WIDTH, BUTTON_HEIGHT, I18n.translateToLocal("gui.mainmenu.megaevolution"), !this.bm.canMegaEvolve(), mouseX, mouseY, MEGA_BUTTON);
                if (this.bm.evolving) {
                    float colorOffset = (float)this.megaAnimation / 255.0f;
                    this.mc.renderEngine.bindTexture(megeEvolution);
                    GuiHelper.drawImageQuad(left, top, BUTTON_WIDTH, BUTTON_HEIGHT, 0.0f + colorOffset, 0.0, 1.0f + colorOffset, 1.0, this.zLevel);
                }
            }

            if (this.bm.canDynamax() && !this.bm.hasDynamaxed) {
                left = (width + 110) / 2;
                top = height - guiHeight - 2;
                if (!this.bm.dynamaxing) {
                    this.mc.renderEngine.bindTexture(dynamaxHover);
                } else {
                    this.mc.renderEngine.bindTexture(dynamax);
                }
                GuiHelper.drawImageQuad(left, top, 64.0, 64.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            }

            if (pokemon.canUltraBurst()) {
                this.mc.renderEngine.bindTexture(GuiResources.battleGui2);
                left = (width - BUTTON_WIDTH) / 2;
                top = height - guiHeight - 20;
                this.parent.drawButton(BattleMode.ChooseAttack, left, top, BUTTON_WIDTH, BUTTON_HEIGHT, I18n.translateToLocal("gui.mainmenu.ultraburst"), !this.bm.canUltraBurst(), mouseX, mouseY, MEGA_BUTTON);
                if (this.bm.evolving) {
                    float colorOffset = (float)this.megaAnimation / 255.0f;
                    this.mc.renderEngine.bindTexture(ultraBurst);
                    GuiHelper.drawImageQuad(left, top, BUTTON_WIDTH, BUTTON_HEIGHT, 0.0f + colorOffset, 0.0, 1.0f + colorOffset, 1.0, this.zLevel);
                }
            }

            this.megaAnimation = (this.megaAnimation + 2) % 255;
            ++this.blink;
            if (this.blink > 50) {
                this.blink = 0;
            }

            if (this.parent.mouseOverButton < moveset.length && moveset[this.parent.mouseOverButton] != null) {
                PixelmonMovesetData moveData = pokemon.moveset[this.parent.mouseOverButton];
                Attack tmp = !this.bm.dynamaxing && (!Arrays.equals(pokemon.pokemonID, this.bm.dynamax) || this.bm.hasDynamaxed) ? moveset[this.parent.mouseOverButton].getAttack() : MaxAttackHelper.getMaxMove(pokemon, moveset[this.parent.mouseOverButton].getAttack());
                if (!GuiScreen.isShiftKeyDown()) {
                    ArrayList<String> tooltip = new ArrayList<>();
                    tooltip.add(I18n.translateToLocal("gui.battle.power") + ": " + tmp.movePower);
                    tooltip.add(I18n.translateToLocal("nbt.pp.name") + " " + moveData.pp + "/" + moveData.ppBase);
                    if (pokemon.getSpecies() == EnumSpecies.Arceus && pokemon.heldItem != null && pokemon.form != 0 && moveset[this.parent.mouseOverButton].getAttack().isAttack("Judgment")) {
                        Judgment j = new Judgment();
                        tooltip.add(I18n.translateToLocal("gui.battle.type") + " " + j.getTypeFromForm(pokemon.form));
                    } else {
                        tooltip.add(I18n.translateToLocal("gui.battle.type") + " " + I18n.translateToLocal("type." + tmp.getType().toString().toLowerCase()));
                    }

                    tooltip.add(I18n.translateToLocal("gui.battle.category") + ": " + tmp.getAttackCategory().getLocalizedName());
                    tooltip.add(" ");
                    tooltip.add("Hold shift for more info.");
                    GuiUtils.drawHoveringText(tooltip, mouseX, mouseY, width, height, -1, this.fontRenderer);
                    GlStateManager.disableRescaleNormal();
                    RenderHelper.disableStandardItemLighting();
                    GlStateManager.disableLighting();
                    GlStateManager.disableDepth();
                    int i = 0;
                    for (String s : tooltip) {
                        int j = this.fontRenderer.getStringWidth(s);
                        if (j <= i) continue;
                        i = j;
                    }
                    int l1 = mouseX + 12;
                    int i2 = mouseY - 12;
                    int k = 8;
                    if (tooltip.size() > 1) {
                        k += 2 + (tooltip.size() - 1) * 10;
                    }

                    if (l1 + i > this.width) {
                        l1 -= 28 + i;
                    }
                    if (i2 + k + 6 > this.height) {
                        i2 = this.height - k - 6;
                    }
                    this.zLevel = 300.0f;
                    this.itemRender.zLevel = 300.0f;
                    int l = -267386864;
                    this.drawGradientRect(l1 - 3, i2 - 4, l1 + i + 3, i2 - 3, -267386864, -267386864);
                    this.drawGradientRect(l1 - 3, i2 + k + 3, l1 + i + 3, i2 + k + 4, -267386864, -267386864);
                    this.drawGradientRect(l1 - 3, i2 - 3, l1 + i + 3, i2 + k + 3, -267386864, -267386864);
                    this.drawGradientRect(l1 - 4, i2 - 3, l1 - 3, i2 + k + 3, -267386864, -267386864);
                    this.drawGradientRect(l1 + i + 3, i2 - 3, l1 + i + 4, i2 + k + 3, -267386864, -267386864);
                    int i1 = 0x505000FF;
                    int j1 = 1344798847;
                    this.drawGradientRect(l1 - 3, i2 - 3 + 1, l1 - 3 + 1, i2 + k + 3 - 1, 0x505000FF, 1344798847);
                    this.drawGradientRect(l1 + i + 2, i2 - 3 + 1, l1 + i + 3, i2 + k + 3 - 1, 0x505000FF, 1344798847);
                    this.drawGradientRect(l1 - 3, i2 - 3, l1 + i + 3, i2 - 3 + 1, 0x505000FF, 0x505000FF);
                    this.drawGradientRect(l1 - 3, i2 + k + 2, l1 + i + 3, i2 + k + 3, 1344798847, 1344798847);
                    for (int k1 = 0; k1 < tooltip.size(); ++k1) {
                        String s1 = tooltip.get(k1);
                        this.fontRenderer.drawStringWithShadow(s1, l1, i2, -1);
                        if (k1 == 0) {
                            i2 += 2;
                        }
                        i2 += 10;
                    }

                    this.zLevel = 0.0f;
                    this.itemRender.zLevel = 0.0f;
                    GlStateManager.enableLighting();
                    GlStateManager.enableDepth();
                    RenderHelper.enableStandardItemLighting();
                    GlStateManager.enableRescaleNormal();
                } else if (GuiScreen.isShiftKeyDown()) {
                    String text = this.parent.mouseOverButton < 4 ? tmp.getAttackBase().getLocalizedDescription() : (this.parent.mouseOverButton == MEGA_BUTTON ? I18n.translateToLocal("gui.battle.mega.description") : I18n.translateToLocal("gui.battle.zmove.description"));
                    ArrayList<String> tooltip = wrapString(text, 50);
                    tooltip.add(0, "Description: ");
                    GuiUtils.drawHoveringText(tooltip, mouseX, mouseY, width, height, -1, this.fontRenderer);
                    GlStateManager.disableRescaleNormal();
                    RenderHelper.disableStandardItemLighting();
                    GlStateManager.disableLighting();
                    GlStateManager.disableDepth();
                    int i = 0;
                    for (String s : tooltip) {
                        int j = this.fontRenderer.getStringWidth(s);
                        if (j <= i) continue;
                        i = j;
                    }
                    int l1 = mouseX + 12;
                    int i2 = mouseY - 12;
                    int k = 8;
                    if (tooltip.size() > 1) {
                        k += 2 + (tooltip.size() - 1) * 10;
                    }
                    if (l1 + i > this.width) {
                        l1 -= 28 + i;
                    }
                    if (i2 + k + 6 > this.height) {
                        i2 = this.height - k - 6;
                    }
                    this.zLevel = 300.0f;
                    this.itemRender.zLevel = 300.0f;
                    int l = -267386864;
                    this.drawGradientRect(l1 - 3, i2 - 4, l1 + i + 3, i2 - 3, -267386864, -267386864);
                    this.drawGradientRect(l1 - 3, i2 + k + 3, l1 + i + 3, i2 + k + 4, -267386864, -267386864);
                    this.drawGradientRect(l1 - 3, i2 - 3, l1 + i + 3, i2 + k + 3, -267386864, -267386864);
                    this.drawGradientRect(l1 - 4, i2 - 3, l1 - 3, i2 + k + 3, -267386864, -267386864);
                    this.drawGradientRect(l1 + i + 3, i2 - 3, l1 + i + 4, i2 + k + 3, -267386864, -267386864);
                    int i1 = 0x505000FF;
                    int j1 = 1344798847;
                    this.drawGradientRect(l1 - 3, i2 - 3 + 1, l1 - 3 + 1, i2 + k + 3 - 1, 0x505000FF, 1344798847);
                    this.drawGradientRect(l1 + i + 2, i2 - 3 + 1, l1 + i + 3, i2 + k + 3 - 1, 0x505000FF, 1344798847);
                    this.drawGradientRect(l1 - 3, i2 - 3, l1 + i + 3, i2 - 3 + 1, 0x505000FF, 0x505000FF);
                    this.drawGradientRect(l1 - 3, i2 + k + 2, l1 + i + 3, i2 + k + 3, 1344798847, 1344798847);
                    for (int k1 = 0; k1 < tooltip.size(); ++k1) {
                        String s1 = tooltip.get(k1);
                        this.fontRenderer.drawStringWithShadow(s1, l1, i2, -1);
                        if (k1 == 0) {
                            i2 += 2;
                        }

                        i2 += 10;
                    }
                    this.zLevel = 0.0f;
                    this.itemRender.zLevel = 0.0f;
                    GlStateManager.enableLighting();
                    GlStateManager.enableDepth();
                    RenderHelper.enableStandardItemLighting();
                    GlStateManager.enableRescaleNormal();
                }

                if (pokemon.heldItem != null && pokemon.heldItem.getItem() instanceof ZCrystal && isZMove[this.parent.mouseOverButton] && this.zMoveActivated && !this.usedZMove) {
                    this.parent.setTargetting(((ZCrystal) pokemon.heldItem.getItem()).getUsableAttack(moveset[this.parent.mouseOverButton].getAttack()), -1, -1);
                } else if (!this.bm.dynamaxing && !Arrays.equals(pokemon.pokemonID, this.bm.dynamax)) {
                    this.parent.setTargetting(moveset[this.parent.mouseOverButton].getAttack(), -1, -1);
                } else {
                    Attack attack = MaxAttackHelper.getMaxMove(pokemon, moveset[this.parent.mouseOverButton].getAttack());
                    this.parent.setTargetting(attack, -1, -1);
                }
            }
        }
    }

    public static ArrayList<String> wrapString(String string, int stringSize) {
        ArrayList<String> lines = new ArrayList<String>();
        for (String line : WordUtils.wrap((String)string, (int)stringSize).split("\n")) {
            lines.add(line.trim());
        }
        return lines;
    }

    @Override
    public void click(int width, int height, int mouseX, int mouseY) {
        int guiHeight = this.parent.getGuiHeight();
        int dynamaxX = (width + 110) / 2;
        int dynamaxY = height - guiHeight - 2;
        if (mouseX >= dynamaxX && mouseX <= dynamaxX + 64 && mouseY >= dynamaxY && mouseY <= dynamaxY + 64 && this.bm.canDynamax() && !this.bm.hasDynamaxed) {
            this.bm.dynamaxing = !this.bm.dynamaxing;
            SoundHelper.playSound(SoundEvents.ITEM_FIRECHARGE_USE);
        }
        if (mouseX > width / 2 + 42 && mouseX < width / 2 + 52 && mouseY > height - 11 && mouseY < height - 1) {
            this.bm.mode = BattleMode.MainMenu;
        } else {
            int x1 = width / 2 - 141;
            int x2 = width / 2 - 50;
            int y1 = height - guiHeight + 9;
            int y2 = height - guiHeight + 33;
            int w = BUTTON_WIDTH;
            int h = BUTTON_HEIGHT;
            PixelmonData poke = this.bm.getUserPokemonPacket();
            PixelmonMovesetData[] moveset = poke.moveset;
            int numMoves = poke.numMoves;
            if (mouseX > x1 && mouseX < x1 + w && mouseY > y1 && mouseY < y1 + h && numMoves > 0 && moveset[0].pp > 0 && (!moveset[0].disabled || this.bm.dynamaxing)) {
                this.checkAttack(0);
            } else if (mouseX > x2 && mouseX < x2 + w && mouseY > y1 && mouseY < y1 + h && numMoves > 1 && moveset[1].pp > 0 && (!moveset[1].disabled || this.bm.dynamaxing)) {
                this.checkAttack(1);
            } else if (mouseX > x1 && mouseX < x1 + w && mouseY > y2 && mouseY < y2 + h && numMoves > 2 && moveset[2].pp > 0 && (!moveset[2].disabled || this.bm.dynamaxing)) {
                this.checkAttack(2);
            } else if (mouseX > x2 && mouseX < x2 + w && mouseY > y2 && mouseY < y2 + h && numMoves > 3 && moveset[3].pp > 0 && (!moveset[3].disabled || this.bm.dynamaxing)) {
                this.checkAttack(3);
            } else if (mouseX > (width - w) / 2 && mouseX < (width + w) / 2 && mouseY > height - guiHeight - 20 && mouseY < height - guiHeight) {
                if (this.bm.canMegaEvolve() && EntityPlayerExtension.getPlayerMegaItem(Minecraft.getMinecraft().player).canEvolve()) {
                    boolean bl = this.bm.evolving = !this.bm.evolving;
                    if (this.bm.evolving) {
                        SoundHelper.playSound(SoundEvents.ITEM_FIRECHARGE_USE);
                    }
                } else if (this.bm.canUltraBurst()) {
                    boolean bl = this.bm.evolving = !this.bm.evolving;
                    if (this.bm.evolving) {
                        SoundHelper.playSound(SoundEvents.ITEM_FIRECHARGE_USE);
                    }
                }
            } else if (mouseX > (width - w - 200) / 2 && mouseX < (width + w - 200) / 2 && mouseY > height - guiHeight - 20 && mouseY < height - guiHeight) {
                this.zMoveActivated = !this.zMoveActivated;
            }
        }
    }

    private void checkAttack(int index) {
        boolean shouldChooseTargets = false;
        PixelmonData poke = this.bm.getUserPokemonPacket();
        PixelmonMovesetData moveset = poke.moveset[index];
        if (this.canSelectTarget(moveset.getAttack()) && !this.zMoveActivated && !this.bm.dynamaxing && !Arrays.equals(poke.pokemonID, this.bm.dynamax)) {
            this.bm.selectedAttack = index;
            this.bm.mode = BattleMode.ChooseTargets;
        } else {
            if (poke.heldItem != null && poke.heldItem.getItem() instanceof ZCrystal) {
                ZCrystal crystal = (ZCrystal)poke.heldItem.getItem();
                if (crystal.canEffectMove(new PokemonSpec(poke.name).fromData(poke), moveset.getAttack())) {
                    if (this.zMoveActivated && !this.usedZMove) {
                        this.zMoveActivated = false;
                        if (this.canSelectTarget(crystal.getUsableAttack(moveset.getAttack()))) {
                            this.bm.selectedAttack = index + 4;
                            this.bm.mode = BattleMode.ChooseTargets;
                            shouldChooseTargets = true;
                        } else {
                            this.chooseAttack(index + 4);
                        }
                    } else {
                        this.chooseAttack(index);
                    }
                } else {
                    this.chooseAttack(index);
                }
            } else if (this.bm.dynamaxing || Arrays.equals(poke.pokemonID, this.bm.dynamax)) {
                Attack maxAttack = MaxAttackHelper.getMaxMove(poke, moveset.getAttack());
                if (this.canSelectTarget(maxAttack)) {
                    this.bm.selectedAttack = index + 4;
                    this.bm.mode = BattleMode.ChooseTargets;
                    shouldChooseTargets = true;
                } else if (maxAttack.isAttack("Max Knuckle", "Max Mindstorm") && this.bm.displayedEnemyPokemon.length > 1) {
                    maxAttack.baseAttack.targetingInfo.hitsAll = false;
                    maxAttack.baseAttack.targetingInfo.hitsOppositeFoe = true;
                    maxAttack.baseAttack.targetingInfo.hitsAdjacentFoe = true;
                    maxAttack.baseAttack.targetingInfo.hitsExtendedFoe = false;
                    maxAttack.baseAttack.targetingInfo.hitsSelf = false;
                    maxAttack.baseAttack.targetingInfo.hitsAdjacentAlly = false;
                    maxAttack.baseAttack.targetingInfo.hitsExtendedAlly = false;
                    this.bm.selectedAttack = index + 4;
                    this.bm.mode = BattleMode.ChooseTargets;
                    shouldChooseTargets = true;
                } else {
                    this.chooseAttack(index + 4);
                }
            } else {
                this.chooseAttack(index);
            }
            if (!shouldChooseTargets) {
                this.bm.selectedMove();
            }
        }
    }

    private void chooseAttack(int index) {
        this.bm.selectedActions.add(new ChooseAttack(this.bm.getUserPokemonPacket().pokemonID, ChooseAttackScreen.clone2D(this.bm.targetted), index, this.bm.battleControllerIndex, this.bm.evolving, this.bm.dynamaxing));
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private boolean canSelectTarget(Attack attack) {
        TargetingInfo info = attack.getAttackBase().targetingInfo;
        if (info.hitsOppositeFoe && info.hitsAdjacentFoe && !info.hitsAdjacentAlly) {
            if (!attack.isAttack("Me First")) return false;
        }
        if (info.hitsSelf && info.hitsAdjacentAlly && !info.hitsAll) {
            if (this.bm.teamPokemon.length > 1) return true;
        }
        if (!info.hitsSelf && !info.hitsAdjacentFoe) {
            if (this.bm.teamPokemon.length == 1) return false;
        }
        if (attack.isAttack("Curse")) {
            if (!this.bm.getUserPokemonPacket().hasType(EnumType.Ghost)) return false;
        }
        if (info.hitsAll) return false;
        if (info.hitsSelf) return false;
        if (this.bm.teamPokemon.length > 1) return true;
        if (this.bm.displayedEnemyPokemon.length <= 1) return false;
        return true;
    }

    static boolean[][] clone2D(boolean[][] array) {
        boolean[][] clonedArray = new boolean[array.length][];
        for (int i = 0; i < array.length; ++i) {
            clonedArray[i] = (boolean[])array[i].clone();
        }
        return clonedArray;
    }
}

