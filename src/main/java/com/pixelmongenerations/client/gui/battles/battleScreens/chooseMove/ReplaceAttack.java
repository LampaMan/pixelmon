/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.battleScreens.chooseMove;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.battles.AttackData;
import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.battleScreens.BattleScreen;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.ReplaceMove;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.text.translation.I18n;

public class ReplaceAttack
extends BattleScreen {
    public ReplaceAttack(GuiBattle parent) {
        super(parent, BattleMode.ReplaceAttack);
    }

    @Override
    public void drawScreen(int width, int height, int mouseX, int mouseY) {
        this.mc.renderEngine.bindTexture(GuiResources.chooseMove);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(width / 2 - 128, height / 2 - 102, 256.0, 205.0f, 0.0, 0.0, 1.0, 0.80078125, this.zLevel);
        if (this.bm.newAttackList.isEmpty()) {
            return;
        }
        PixelmonData pokemonToLearnAttack = ServerStorageDisplay.get(this.bm.newAttackList.get((int)0).pokemonID);
        if (pokemonToLearnAttack == null) {
            this.bm.newAttackList.clear();
            this.bm.mode = this.bm.oldMode;
            return;
        }
        GuiHelper.drawMoveset(pokemonToLearnAttack, width, height, this.zLevel);
        for (int i = 0; i < pokemonToLearnAttack.numMoves; ++i) {
            if (!this.mouseOverMove(i, width, height, mouseX, mouseY)) continue;
            this.mc.renderEngine.bindTexture(GuiResources.chooseMove);
            GuiHelper.drawImageQuad(width / 2 - 30, height / 2 - 94 + 22 * i, 152.0, 24.0f, 0.37890625, 0.81640625, 0.97265625, 0.9140625, this.zLevel);
            GuiHelper.drawAttackInfoMoveset(pokemonToLearnAttack.moveset[i].getAttack(), height / 2 + 53, width, height);
        }
        Attack newAttack = this.bm.newAttackList.get((int)0).attack;
        this.drawString(this.mc.fontRenderer, newAttack.getAttackBase().getLocalizedName(), width / 2 + 11, height / 2 - 78 + 88, 0xFFFFFF);
        this.drawString(this.mc.fontRenderer, newAttack.pp + "/" + newAttack.ppBase, width / 2 + 90, height / 2 - 76 + 88, 0xFFFFFF);
        float x3 = newAttack.getAttackBase().attackType.textureX;
        float y3 = newAttack.getAttackBase().attackType.textureY;
        this.mc.renderEngine.bindTexture(GuiResources.types);
        GuiHelper.drawImageQuad(width / 2 - 23, height / 2 + 3, 21.0, 21.0f, x3 / 495.0f, y3 / 392.0f, (x3 + 98.0f) / 495.0f, (y3 + 98.0f) / 392.0f, this.zLevel);
        if (mouseX > width / 2 - 30 && mouseX < width / 2 + 120 && mouseY > height / 2 + 3 && mouseY < height / 2 + 25) {
            this.mc.renderEngine.bindTexture(GuiResources.chooseMove);
            GuiHelper.drawImageQuad(width / 2 - 30, height / 2 + 1, 152.0, 24.0f, 0.37890625, 0.81640625, 0.97265625, 0.9140625, this.zLevel);
            GuiHelper.drawAttackInfoMoveset(newAttack, height / 2 + 53, width, height);
        }
        this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.replaceattack.effect"), width / 2 - 96, height / 2 + 38, 0xFFFFFF);
        this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.description.text"), width / 2 - 20, height / 2 + 38, 0xFFFFFF);
        GuiHelper.drawPokemonInfoChooseMove(pokemonToLearnAttack, width, height, this.zLevel);
    }

    @Override
    public void click(int width, int height, int mouseX, int mouseY) {
        if (this.bm.newAttackList.isEmpty()) {
            try {
                Thread.sleep(100L);
            }
            catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
        if (this.bm.newAttackList.isEmpty()) {
            return;
        }
        AttackData newAttackData = this.bm.newAttackList.get(0);
        Attack newAttack = newAttackData.attack;
        PixelmonData pokemonToLearnAttack = ServerStorageDisplay.get(newAttackData.pokemonID);
        if (pokemonToLearnAttack == null) {
            return;
        }
        for (int i = 0; i < pokemonToLearnAttack.numMoves; ++i) {
            if (!this.mouseOverMove(i, width, height, mouseX, mouseY)) continue;
            this.showConfirm(pokemonToLearnAttack, newAttack, i);
            return;
        }
        if (mouseX > width / 2 - 30 && mouseX < width / 2 + 120 && mouseY > height / 2 + 3 && mouseY < height / 2 + 25) {
            this.showConfirm(pokemonToLearnAttack, newAttack, -1);
        }
    }

    private void showConfirm(PixelmonData pokemonToLearnAttack, Attack newAttack, int attackIndex) {
        boolean checkEvo = this.bm.newAttackList.get((int)0).checkEvo;
        this.bm.sendPacket = attackIndex >= 0 || checkEvo ? new ReplaceMove(pokemonToLearnAttack.pokemonID, newAttack.getAttackBase().attackIndex, attackIndex, checkEvo) : null;
        this.bm.yesNoOrigin = this.bm.mode;
        this.bm.mode = BattleMode.YesNoReplaceMove;
        this.bm.selectedAttack = attackIndex;
    }

    private boolean mouseOverMove(int i, int width, int height, int mouseX, int mouseY) {
        return mouseX > width / 2 - 30 && mouseX < width / 2 + 120 && mouseY > height / 2 - 94 + 22 * i && mouseY < height / 2 - 94 + 22 * (i + 1);
    }

    @Override
    public boolean isScreen() {
        return this.bm.mode == BattleMode.Waiting && this.bm.hasNewAttacks() || this.bm.mode == BattleMode.ReplaceAttack;
    }
}

