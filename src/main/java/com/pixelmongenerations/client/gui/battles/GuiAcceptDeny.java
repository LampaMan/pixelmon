/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.battles.GuiVersus;
import com.pixelmongenerations.client.gui.elements.GuiRoundButton;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.battles.AcceptDeclineBattle;
import com.pixelmongenerations.core.network.packetHandlers.battles.BattleQueryPacket;
import com.pixelmongenerations.core.network.packetHandlers.battles.EnumBattleQueryResponse;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.text.translation.I18n;

public class GuiAcceptDeny
extends GuiVersus {
    public static BattleQueryPacket opponent;
    boolean accepted = false;
    boolean changeRules = false;
    int battleQueryID;
    private GuiRoundButton acceptButton;
    private GuiRoundButton denyButton;
    private GuiRoundButton rulesButton;
    private boolean responded;

    public GuiAcceptDeny(int battleQueryID) {
        this.battleQueryID = battleQueryID;
        int acceptDenyHeight = 152;
        this.acceptButton = new GuiRoundButton(25, acceptDenyHeight, I18n.translateToLocal("gui.acceptdeny.accept"));
        this.denyButton = new GuiRoundButton(145, acceptDenyHeight, I18n.translateToLocal("gui.acceptdeny.deny"));
        this.rulesButton = new GuiRoundButton(85, 182, I18n.translateToLocal("gui.battlerules.title"));
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float f, int mouseX, int mouseY) {
        super.drawGuiContainerBackgroundLayer(f, mouseX, mouseY);
        if (opponent == null) {
            this.mc.player.closeScreen();
            return;
        }
        EntityPlayer opponentEntity = this.mc.world.getPlayerEntityByUUID(GuiAcceptDeny.opponent.opponentUUID);
        if (opponentEntity == null) {
            this.mc.player.closeScreen();
            return;
        }
        int[] pokeballs1 = new int[6];
        PixelmonData[] pokemon = ServerStorageDisplay.getPokemon();
        for (int p = 0; p < 6; ++p) {
            PixelmonData current = pokemon[p];
            if (current != null) {
                pokeballs1[p] = current.pokeball.getIndex();
                if (!current.isFainted) continue;
                pokeballs1[p] = pokeballs1[p] * -1 - 1;
                continue;
            }
            pokeballs1[p] = -999;
        }
        this.drawPokeBalls(pokeballs1, this.playerPartyX, this.playerPartyY, this.offset1);
        int numOpponentPokeBalls = 0;
        for (int pokeBall : GuiAcceptDeny.opponent.pokeballs) {
            if (pokeBall <= -1) continue;
            ++numOpponentPokeBalls;
        }
        this.drawOpponentPokeBalls(numOpponentPokeBalls);
        GlStateManager.disableLighting();
        GlStateManager.enableBlend();
        if (this.ticks == 0) {
            this.acceptButton.drawButton(this.leftX, this.topY, mouseX, mouseY, this.zLevel, this.accepted);
            this.denyButton.drawButton(this.leftX, this.topY, mouseX, mouseY, this.zLevel);
            this.rulesButton.drawButton(this.leftX, this.topY, mouseX, mouseY, this.zLevel, this.changeRules);
        }
        GlStateManager.disableBlend();
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int par3) {
        EnumBattleQueryResponse response = null;
        if (!this.accepted && this.acceptButton.isMouseOver(this.leftX, this.topY, mouseX, mouseY)) {
            this.accepted = true;
            this.changeRules = false;
            response = EnumBattleQueryResponse.Accept;
        } else if (this.denyButton.isMouseOver(this.leftX, this.topY, mouseX, mouseY)) {
            this.mc.player.closeScreen();
            response = EnumBattleQueryResponse.Decline;
        } else if (!this.changeRules && this.rulesButton.isMouseOver(this.leftX, this.topY, mouseX, mouseY)) {
            this.accepted = false;
            this.changeRules = true;
            response = EnumBattleQueryResponse.Rules;
        }
        if (response != null) {
            this.responded = true;
            Pixelmon.NETWORK.sendToServer(new AcceptDeclineBattle(this.battleQueryID, response));
        }
    }

    @Override
    public void onGuiClosed() {
        if (!this.responded) {
            Pixelmon.NETWORK.sendToServer(new AcceptDeclineBattle(this.battleQueryID, EnumBattleQueryResponse.Decline));
        }
    }

    @Override
    protected EntityLivingBase getOpponent() {
        return this.mc.world.getPlayerEntityByUUID(GuiAcceptDeny.opponent.opponentUUID);
    }
}

