/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.client.gui.battles;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.NoStatus;
import com.pixelmongenerations.common.battle.status.StatusPersist;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Illusion;
import com.pixelmongenerations.common.entity.pixelmon.stats.Level;
import com.pixelmongenerations.core.network.PixelmonData;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class PixelmonInGui {
    public int[] pokemonID;
    public float health;
    public int maxHealth;
    public String nickname;
    public String pokemonName;
    public int status = -1;
    public int level;
    public int bossLevel;
    public short gender;
    public boolean shiny;
    public int position = 0;
    public short form = 0;
    public short specialTexture = 0;
    public float expFraction = 0.0f;
    public int xPos = 0;
    public boolean isSwitching;
    public boolean totem;
    public String customTexture;

    public PixelmonInGui() {
        this.nickname = "";
        this.pokemonName = "";
    }

    public PixelmonInGui(PixelmonData poke) {
        this.set(poke);
    }

    public PixelmonInGui(PixelmonWrapper pixelmon) {
        this.pokemonID = pixelmon.getPokemonID();
        this.health = pixelmon.getHealth();
        this.maxHealth = pixelmon.getMaxHealth();
        AbilityBase ability = pixelmon.getBattleAbility();
        boolean inIllusion = false;
        if (ability instanceof Illusion) {
            Illusion illusion = (Illusion)ability;
            if (illusion.disguisedGender != null) {
                this.nickname = illusion.disguisedNickname;
                this.pokemonName = illusion.disguisedPokemon.name;
                this.gender = (short)illusion.disguisedGender.ordinal();
                this.form = (short)illusion.disguisedForm;
                inIllusion = true;
            }
        }
        if (!inIllusion) {
            this.nickname = pixelmon.getNickname();
            this.pokemonName = pixelmon.getPokemonName();
            this.gender = (short)pixelmon.getGender().ordinal();
            this.form = (short)pixelmon.pokemon.getFormIncludeTransformed();
        }
        Level levelContainer = pixelmon.getLevel();
        this.level = levelContainer.getLevel();
        this.bossLevel = pixelmon.pokemon.getBossMode().index;
        this.shiny = pixelmon.pokemon.isShiny();
        this.specialTexture = (short)pixelmon.pokemon.getSpecialTextureIndex();
        StatusPersist primaryStatus = pixelmon.getPrimaryStatus();
        this.status = primaryStatus != NoStatus.noStatus ? primaryStatus.type.ordinal() : -1;
        this.expFraction = levelContainer.getExpFraction();
        this.totem = pixelmon.totem;
        this.customTexture = pixelmon.getCustomTexture();
    }

    public void set(PixelmonData poke) {
        this.pokemonID = poke.pokemonID;
        this.health = poke.health;
        this.maxHealth = poke.hp;
        this.nickname = poke.getNickname();
        this.pokemonName = poke.name;
        this.status = poke.status == null ? -1 : poke.status.ordinal();
        this.level = poke.lvl;
        this.gender = (short)poke.gender.ordinal();
        this.shiny = poke.isShiny;
        this.expFraction = poke.getExpFraction();
        this.form = poke.form;
        this.specialTexture = poke.specialTexture;
        this.totem = poke.totem;
        this.customTexture = poke.customTexture;
    }

    public static PixelmonInGui[] convertToGUI(ArrayList<PixelmonWrapper> pokemon) {
        PixelmonInGui[] data = new PixelmonInGui[pokemon.size()];
        for (int i = 0; i < pokemon.size(); ++i) {
            data[i] = new PixelmonInGui(pokemon.get(i));
        }
        return data;
    }

    public void decodeInto(ByteBuf buffer) {
        this.pokemonID = new int[]{buffer.readInt(), buffer.readInt()};
        this.health = buffer.readFloat();
        this.maxHealth = buffer.readInt();
        this.nickname = ByteBufUtils.readUTF8String(buffer);
        this.pokemonName = ByteBufUtils.readUTF8String(buffer);
        this.level = buffer.readInt();
        this.bossLevel = buffer.readInt();
        this.gender = buffer.readShort();
        this.shiny = buffer.readBoolean();
        this.status = buffer.readInt();
        this.expFraction = buffer.readFloat();
        this.form = buffer.readShort();
        this.specialTexture = buffer.readShort();
        this.totem = buffer.readBoolean();
        this.customTexture = ByteBufUtils.readUTF8String(buffer);
        if (this.customTexture.equals("null")) {
            this.customTexture = null;
        }
    }

    public void encodeInto(ByteBuf buffer) {
        if (this.nickname == null) {
            this.nickname = this.pokemonName;
        }
        for (int i : this.pokemonID) {
            buffer.writeInt(i);
        }
        buffer.writeFloat(this.health);
        buffer.writeInt(this.maxHealth);
        ByteBufUtils.writeUTF8String(buffer, this.nickname);
        ByteBufUtils.writeUTF8String(buffer, this.pokemonName);
        buffer.writeInt(this.level);
        buffer.writeInt(this.bossLevel);
        buffer.writeShort((int)this.gender);
        buffer.writeBoolean(this.shiny);
        buffer.writeInt(this.status);
        buffer.writeFloat(this.expFraction);
        buffer.writeShort((int)this.form);
        buffer.writeShort((int)this.specialTexture);
        buffer.writeBoolean(this.totem);
        ByteBufUtils.writeUTF8String(buffer, this.customTexture == null ? "null" : this.customTexture);
    }

    public String getDisplayName() {
        String localizedName = Entity1Base.getLocalizedName(this.pokemonName);
        if (this.nickname.isEmpty() || this.nickname.equals(this.pokemonName)) {
            return localizedName;
        }
        return this.nickname;
    }

    public int getDexNumber() {
        return Entity3HasStats.getPokedexNumber(this.pokemonName);
    }
}

