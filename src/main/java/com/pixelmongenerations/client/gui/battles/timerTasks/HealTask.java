/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.timerTasks;

import com.pixelmongenerations.client.gui.battles.timerTasks.HPTask;
import com.pixelmongenerations.common.battle.attacks.IBattleMessage;

public class HealTask
extends HPTask {
    public HealTask(IBattleMessage message, float healthDifference, int[] id) {
        super(message, healthDifference, id);
    }

    @Override
    protected void boundsCheck() {
        if (this.currentHealth + this.healthDifference > (float)this.pokemon.maxHealth) {
            this.healthDifference = (float)this.pokemon.maxHealth - this.currentHealth;
        }
    }

    @Override
    protected boolean isDone() {
        return this.currentHealth >= this.originalHealth + this.healthDifference;
    }
}

