/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles;

public enum BattleDirection {
    BattleMessage,
    SetOpponentType,
    SetOpponent,
    SetBattlingPokemon,
    EnforcedSwitch,
    BackToMainMenu,
    ExitBattle,
    BattleSwitch;

}

