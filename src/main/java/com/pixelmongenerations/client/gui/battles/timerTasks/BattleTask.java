/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.timerTasks;

import com.pixelmongenerations.common.battle.attacks.IBattleMessage;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.util.TimerTask;

public abstract class BattleTask
extends TimerTask {
    IBattleMessage message;

    public BattleTask(IBattleMessage message) {
        this.message = message;
    }

    @Override
    public boolean cancel() {
        ClientProxy.battleManager.removeBattleMessage(this.message);
        return super.cancel();
    }
}

