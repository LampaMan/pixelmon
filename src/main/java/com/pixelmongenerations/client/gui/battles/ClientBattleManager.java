/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.camera.CameraMode;
import com.pixelmongenerations.client.camera.CameraTargetEntity;
import com.pixelmongenerations.client.camera.EntityCamera;
import com.pixelmongenerations.client.camera.ICameraTarget;
import com.pixelmongenerations.client.gui.GuiEvolve;
import com.pixelmongenerations.client.gui.battles.AttackData;
import com.pixelmongenerations.client.gui.battles.EvoInfo;
import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.PixelmonInGui;
import com.pixelmongenerations.client.gui.battles.battleScreens.choosePokemon.EnforcedSwitch;
import com.pixelmongenerations.client.gui.battles.timerTasks.AFKTask;
import com.pixelmongenerations.common.battle.attacks.IBattleMessage;
import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.rules.BattleRules;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.ItemData;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.battle.BagSection;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.enums.battle.EnumBattleMusic;
import com.pixelmongenerations.core.enums.battle.EnumBattleType;
import com.pixelmongenerations.core.event.EntityPlayerExtension;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.LevelUp;
import com.pixelmongenerations.core.network.packetHandlers.battles.ChooseAttack;
import com.pixelmongenerations.core.network.packetHandlers.battles.Flee;
import com.pixelmongenerations.core.network.packetHandlers.battles.ParticipantReady;
import com.pixelmongenerations.core.network.packetHandlers.battles.SwitchPokemon;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.util.PixelmonMethods;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.client.GuiIngameForge;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class ClientBattleManager {
    public int battleControllerIndex = -1;
    public BattleMode mode;
    public BagSection bagSection;
    public boolean battleEnded = true;
    public boolean isSpectating = false;
    public EnumBattleMusic battleSong = EnumBattleMusic.None;
    private ArrayList<String> messageList = new ArrayList();
    public int[][] teamPokemon;
    public PixelmonInGui[] displayedEnemyPokemon;
    public PixelmonInGui[] displayedOurPokemon;
    public PixelmonInGui[] displayedAllyPokemon;
    public ArrayList<IBattleMessage> battleMessages = new ArrayList();
    public ArrayList<ItemData> bagStore = new ArrayList();
    public ArrayList<LevelUp> levelUpList = new ArrayList(6);
    public ArrayList<AttackData> newAttackList = new ArrayList();
    public boolean goBackToMainMenu;
    public int startIndex = 0;
    public ItemData itemToUse = null;
    public IMessage sendPacket;
    public BattleMode oldMode;
    public BattleMode yesNoOrigin;
    public List<EvoInfo> evolveList = new ArrayList<EvoInfo>(6);
    public boolean isHealing = false;
    public int healAmount = 0;
    public PixelmonData pixelmonToHeal = null;
    public int selectedAttack = -1;
    public int currentPokemon = -1;
    public boolean[][] targetted = new boolean[2][];
    public boolean choosingPokemon;
    public ArrayList<IMessage> selectedActions = new ArrayList();
    public EnumBattleType battleType;
    public Queue<com.pixelmongenerations.core.network.packetHandlers.battles.EnforcedSwitch> enforcedSwitches = new LinkedList<com.pixelmongenerations.core.network.packetHandlers.battles.EnforcedSwitch>();
    public boolean afkOn;
    public boolean afkActive;
    public int afkActivate;
    public int afkTurn;
    public int afkTime;
    private Timer afkTimer;
    public boolean waitingText;
    public boolean evolving;
    public boolean dynamaxing;
    public boolean hasDynamaxed;
    public int[] megaEvolution;
    public int[] ultraBurst;
    public int[] dynamax;
    public BattleRules rules = new BattleRules();
    public boolean canSwitch = true;
    public boolean canFlee = true;
    public ParticipantType[][] battleSetup;
    public boolean healFinished = false;
    int ticksSincePicked = 0;
    public ArrayList<int[]> pokemonToChoose;
    public UUID spectatingUUID;
    private EntityPlayer spectating;
    private int thirdP = 0;

    public boolean isBattling() {
        return this.battleControllerIndex != -1 && !this.battleEnded && !this.isSpectating;
    }

    public void addMessage(String s) {
        this.messageList.add(s);
    }

    public String getNextMessage() {
        if (this.hasMoreMessages()) {
            return this.messageList.get(0);
        }
        return "";
    }

    public void removeMessage() {
        if (this.hasMoreMessages()) {
            this.messageList.remove(0);
        }
    }

    public boolean hasMoreMessages() {
        return !this.messageList.isEmpty();
    }

    public PixelmonData getUserPokemonPacket() {
        if (this.currentPokemon != -1) {
            if (this.teamPokemon != null) {
                if (this.currentPokemon < this.teamPokemon.length) {
                    return ServerStorageDisplay.get(this.teamPokemon[this.currentPokemon]);
                }
            } else {
                return ServerStorageDisplay.getPokemon()[this.currentPokemon];
            }
        }
        return null;
    }

    public EntityPixelmon getUserPokemon() {
        return this.getUserPokemon(CameraMode.Battle);
    }

    public EntityPixelmon getUserPokemon(CameraMode mode) {
        if (mode == CameraMode.Battle) {
            if ((this.currentPokemon != -1 || this.isSpectating) && this.teamPokemon != null && this.teamPokemon.length > 0) {
                if (this.currentPokemon == -1) {
                    this.currentPokemon = 0;
                }
                int pokemonIndex = Math.min(this.currentPokemon, this.teamPokemon.length - 1);
                return this.getEntity(this.teamPokemon[pokemonIndex]);
            }
        } else if (mode == CameraMode.Evolution) {
            return GuiEvolve.currentPokemon;
        }
        return null;
    }

    public EntityPixelmon getEntity(int[] id) {
        Minecraft mc = Minecraft.getMinecraft();
        if (mc.world != null) {
            List loadedEntityList = mc.world.loadedEntityList;
            for (int i = 0; i < loadedEntityList.size(); ++i) {
                EntityPixelmon pokemon;
                Entity e = (Entity)loadedEntityList.get(i);
                if (!(e instanceof EntityPixelmon) || !PixelmonMethods.isIDSame(pokemon = (EntityPixelmon)e, id)) continue;
                return pokemon;
            }
        }
        return null;
    }

    public void clearMessages() {
        this.messageList.clear();
    }

    public boolean hasLevelUps() {
        return !this.levelUpList.isEmpty();
    }

    public boolean hasNewAttacks() {
        return !this.newAttackList.isEmpty();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void startBattle(int battleControllerIndex, ParticipantType[][] battleSetup, int afkActivate, int afkTurn, BattleRules rules) {
        ArrayList<IBattleMessage> arrayList;
        this.battleControllerIndex = battleControllerIndex;
        this.battleSetup = battleSetup;
        this.rules = rules;
        this.displayedOurPokemon = null;
        this.displayedEnemyPokemon = null;
        this.displayedAllyPokemon = null;
        this.teamPokemon = null;
        this.targetted = new boolean[2][];
        this.mode = BattleMode.Waiting;
        this.clearMessages();
        this.canSwitch = true;
        this.canFlee = true;
        this.battleEnded = false;
        this.megaEvolution = null;
        this.ultraBurst = null;
        if (afkActivate == -1) {
            this.afkOn = false;
        } else {
            this.afkOn = true;
            this.afkActivate = afkActivate;
            this.afkTurn = afkTurn;
            this.afkActive = false;
            this.resetAFKTime();
        }
        ServerStorageDisplay.clearBattle();
        ArrayList<IBattleMessage> arrayList2 = arrayList = this.battleMessages;
        synchronized (arrayList2) {
            this.battleMessages.clear();
        }
        this.dynamax = null;
        this.dynamaxing = false;
        this.hasDynamaxed = false;
    }

    public void startSpectate(EnumBattleType battleType) {
        ClientProxy.battleManager.isSpectating = true;
        ClientProxy.battleManager.battleType = battleType;
        this.spectating = null;
        Minecraft mc = Minecraft.getMinecraft();
        mc.player.openGui(Pixelmon.INSTANCE, EnumGui.Battle.ordinal(), mc.world, 0, 0, 0);
    }

    public void endSpectate() {
        if (ClientProxy.battleManager.isSpectating) {
            ClientProxy.battleManager.isSpectating = false;
            ClientProxy.battleManager.battleEnded = true;
            this.spectating = null;
            this.spectatingUUID = null;
            this.resetViewEntity();
        }
    }

    @SubscribeEvent
    public void onTick(TickEvent.ClientTickEvent event) {
        if (event.phase == TickEvent.Phase.END && !this.battleEnded) {
            this.tick();
        }
    }

    private void tick() {
        ICameraTarget tar;
        Minecraft minecraft = Minecraft.getMinecraft();
        if (!(minecraft.currentScreen instanceof GuiBattle) && minecraft.player != null) {
            minecraft.player.openGui(Pixelmon.INSTANCE, EnumGui.Battle.ordinal(), minecraft.world, 0, 0, 0);
        }
        if (this.ticksSincePicked > 0) {
            --this.ticksSincePicked;
        }
        boolean doCameraTick = true;
        if (ClientProxy.camera != null && (tar = ClientProxy.camera.getTarget()) != null) {
            EntityPixelmon userPokemon = this.getUserPokemon();
            if (tar.getTargetData() == userPokemon && (userPokemon == null || userPokemon.isDead || userPokemon.getHealth() <= 0.0f)) {
                this.setCameraToPlayer();
                doCameraTick = false;
            }
        }
        if (PixelmonConfig.useBattleCamera && doCameraTick && this.ticksSincePicked <= 0 && this.getUserPokemon() != null) {
            Entity viewEntity = minecraft.getRenderViewEntity();
            if (viewEntity instanceof EntityCamera && viewEntity != ClientProxy.camera) {
                minecraft.setRenderViewEntity(ClientProxy.camera);
            }
            this.setCameraToPixelmon();
        }
    }

    public void restoreSettingsAndClose() {
        this.battleEnded = true;
        if (this.afkTimer != null) {
            this.afkTimer.cancel();
        }
        this.resetViewEntity();
        this.spectating = null;
        this.spectatingUUID = null;
        this.selectedActions.clear();
        this.enforcedSwitches.clear();
        EnforcedSwitch.failFlee = false;
        this.megaEvolution = null;
        this.ultraBurst = null;
    }

    public boolean canCatchOpponent() {
        for (int i = 0; i < this.battleSetup[1].length; ++i) {
            ParticipantType t = this.battleSetup[1][i];
            if (t != ParticipantType.WildPokemon || this.displayedEnemyPokemon[i].bossLevel != EnumBossMode.NotBoss.index || this.displayedEnemyPokemon[i].totem) continue;
            return true;
        }
        return false;
    }

    public boolean canRunFromBattle() {
        for (int i = 0; i < this.battleSetup[1].length; ++i) {
            ParticipantType t = this.battleSetup[1][i];
            if (t != ParticipantType.WildPokemon) continue;
            return this.canFlee;
        }
        return !this.rules.hasClause("forfeit");
    }

    public boolean canForfeit() {
        for (int i = 0; i < this.battleSetup[1].length; ++i) {
            ParticipantType t = this.battleSetup[1][i];
            if (t == ParticipantType.WildPokemon) continue;
            return true;
        }
        return false;
    }

    public EntityPlayer getViewPlayer() {
        Minecraft minecraft = Minecraft.getMinecraft();
        if (this.isSpectating && this.spectatingUUID != null) {
            if (this.spectating == null) {
                this.spectating = minecraft.world.getPlayerEntityByUUID(this.spectatingUUID);
            }
            if (this.spectating != null) {
                return this.spectating;
            }
        }
        return minecraft.player;
    }

    public void startPicking(boolean canSwitch, boolean canFlee, ArrayList<int[]> pokemonToChoose) {
        this.setCameraToPlayer();
        this.ticksSincePicked = 50;
        if (!this.isSpectating) {
            this.canSwitch = canSwitch;
            this.canFlee = canFlee;
            this.oldMode = BattleMode.MainMenu;
            this.mode = BattleMode.MainMenu;
            this.pokemonToChoose = pokemonToChoose;
            for (int i = 0; i < this.teamPokemon.length; ++i) {
                int[] id = this.teamPokemon[i];
                if (!ServerStorageDisplay.has(id) || !this.hasTurn(id)) continue;
                this.currentPokemon = i;
                break;
            }
        }
    }

    private boolean hasTurn(int[] id) {
        for (int[] pid : this.pokemonToChoose) {
            if (!PixelmonMethods.isIDSame(pid, id)) continue;
            return true;
        }
        return false;
    }

    public void selectedMove() {
        this.selectedMove(false);
    }

    public void selectedMove(boolean isEnforcedSwitch) {
        this.afkActive = false;
        this.evolving = false;
        this.dynamaxing = false;
        if (!this.enforcedSwitches.isEmpty()) {
            this.enforcedSwitches.poll().processMessage();
            return;
        }
        if (!isEnforcedSwitch && this.teamPokemon.length > this.currentPokemon + 1 && this.teamPokemon.length > this.selectedActions.size()) {
            int prevCurrentPokemon = this.currentPokemon;
            for (int i = this.currentPokemon + 1; i < this.teamPokemon.length; ++i) {
                int[] id = this.teamPokemon[i];
                if (ServerStorageDisplay.has(id)) {
                    if (!this.hasTurn(id)) continue;
                    this.currentPokemon = i;
                    break;
                }
                this.finishSelection();
                return;
            }
            if (prevCurrentPokemon == this.currentPokemon) {
                this.finishSelection();
                return;
            }
            this.mode = BattleMode.MainMenu;
        } else {
            this.finishSelection();
        }
    }

    public void afkSelectMove() {
        if (this.mode != BattleMode.Waiting) {
            if (this.mode == BattleMode.EnforcedSwitch) {
                ArrayList<PixelmonData> inBattle = new ArrayList<PixelmonData>(3);
                if (this.teamPokemon != null) {
                    for (int[] id : this.teamPokemon) {
                        if (!ServerStorageDisplay.has(id)) continue;
                        inBattle.add(ServerStorageDisplay.get(id));
                    }
                    PixelmonData currentData = (PixelmonData)inBattle.get(this.currentPokemon % inBattle.size());
                    this.selectedActions.add(new SwitchPokemon(-1, this.battleControllerIndex, currentData.pokemonID, true));
                }
            } else {
                for (int i = this.currentPokemon; i < this.teamPokemon.length; ++i) {
                    int[] id = this.teamPokemon[i];
                    if (!ServerStorageDisplay.has(id) || !this.hasTurn(id)) continue;
                    this.currentPokemon = i;
                    PixelmonData currentData = this.getUserPokemonPacket();
                    IMessage action = currentData.health <= 0 ? new SwitchPokemon(-1, this.battleControllerIndex, currentData.pokemonID, false) : new ChooseAttack(this.getUserPokemonPacket().pokemonID, new boolean[0][0], -1, this.battleControllerIndex, this.evolving, this.dynamaxing);
                    this.evolving = false;
                    this.dynamaxing = false;
                    this.selectedActions.add(action);
                }
            }
            this.finishSelection();
        }
    }

    public void finishSelection() {
        this.mode = BattleMode.Waiting;
        this.selectedActions.stream().filter(action -> action != null).forEach(Pixelmon.NETWORK::sendToServer);
        this.selectedActions.clear();
        for (PixelmonData data : ServerStorageDisplay.getPokemon()) {
            if (data == null) continue;
            data.selected = false;
        }
    }

    public void setTeamPokemon(int[][] pokemon) {
        this.teamPokemon = pokemon;
        this.targetted[0] = new boolean[pokemon.length];
        if (this.displayedOurPokemon == null) {
            ArrayList<PixelmonInGui> ourPokemon = new ArrayList<PixelmonInGui>();
            for (int[] id : pokemon) {
                if (ServerStorageDisplay.get(id) == null) continue;
                ourPokemon.add(new PixelmonInGui(ServerStorageDisplay.get(id)));
            }
            for (PixelmonInGui pig : this.displayedOurPokemon = ourPokemon.toArray(new PixelmonInGui[ourPokemon.size()])) {
                pig.xPos = 120;
            }
        }
    }

    public void setTeamPokemon(PixelmonInGui[] data) {
        if (this.displayedOurPokemon != null && data != null) {
            for (PixelmonInGui replacement : data) {
                for (PixelmonInGui current : this.displayedOurPokemon) {
                    if (!PixelmonMethods.isIDSame(replacement.pokemonID, current.pokemonID)) continue;
                    replacement.health = current.health;
                }
            }
        }
        this.displayedOurPokemon = data;
        this.targetted[0] = new boolean[this.displayedOurPokemon.length];
        for (PixelmonInGui pig : this.displayedOurPokemon) {
            pig.xPos = 120;
        }
    }

    public void setOpponents(PixelmonInGui[] data) {
        if (this.displayedEnemyPokemon != null && data != null) {
            for (PixelmonInGui replacement : data) {
                for (PixelmonInGui current : this.displayedEnemyPokemon) {
                    if (!PixelmonMethods.isIDSame(replacement.pokemonID, current.pokemonID)) continue;
                    replacement.health = current.health;
                }
            }
        }
        this.displayedEnemyPokemon = data;
        this.targetted[1] = new boolean[this.displayedEnemyPokemon.length];
    }

    public void setTeamData(PixelmonInGui[] data) {
        if (this.displayedAllyPokemon != null && data != null) {
            for (PixelmonInGui replacement : data) {
                for (PixelmonInGui current : this.displayedAllyPokemon) {
                    if (!PixelmonMethods.isIDSame(replacement.pokemonID, current.pokemonID)) continue;
                    replacement.health = current.health;
                }
            }
        }
        this.displayedAllyPokemon = data;
        for (PixelmonInGui pig : this.displayedAllyPokemon) {
            pig.xPos = 120;
        }
    }

    public PixelmonInGui getUncontrolledTeamPokemon(int[] id) {
        if (this.displayedAllyPokemon == null) {
            return null;
        }
        for (PixelmonInGui data : this.displayedAllyPokemon) {
            if (!PixelmonMethods.isIDSame(id, data.pokemonID)) continue;
            return data;
        }
        return null;
    }

    public PixelmonInGui getPokemon(int[] id) {
        if (this.displayedOurPokemon != null) {
            for (PixelmonInGui pig : this.displayedOurPokemon) {
                if (!PixelmonMethods.isIDSame(id, pig.pokemonID)) continue;
                return pig;
            }
        }
        if (this.displayedEnemyPokemon != null) {
            for (PixelmonInGui pig : this.displayedEnemyPokemon) {
                if (!PixelmonMethods.isIDSame(id, pig.pokemonID)) continue;
                return pig;
            }
        }
        if (this.displayedAllyPokemon != null) {
            for (PixelmonInGui pig : this.displayedAllyPokemon) {
                if (!PixelmonMethods.isIDSame(id, pig.pokemonID)) continue;
                return pig;
            }
        }
        return null;
    }

    public boolean isEnemyPokemon(PixelmonInGui pokemon) {
        for (PixelmonInGui p : this.displayedEnemyPokemon) {
            if (p != pokemon) continue;
            return true;
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addBattleMessage(IBattleMessage message) {
        ArrayList<IBattleMessage> arrayList;
        ArrayList<IBattleMessage> arrayList2 = arrayList = this.battleMessages;
        synchronized (arrayList2) {
            this.battleMessages.add(message);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeBattleMessage(IBattleMessage message) {
        ArrayList<IBattleMessage> arrayList;
        ArrayList<IBattleMessage> arrayList2 = arrayList = this.battleMessages;
        synchronized (arrayList2) {
            this.battleMessages.remove(message);
            if (this.battleMessages.isEmpty()) {
                this.onClearedMessages();
            }
        }
    }

    public void onClearedMessages() {
        if (!this.isSpectating && !this.hasMoreMessages() && this.battleMessages.isEmpty()) {
            Pixelmon.NETWORK.sendToServer(new ParticipantReady(this.battleControllerIndex, Minecraft.getMinecraft().player.getUniqueID().toString()));
        }
    }

    public void setCameraToPixelmon() {
        if (ClientProxy.camera != null) {
            ICameraTarget tar = ClientProxy.camera.getTarget();
            if (tar != null) {
                EntityPixelmon userPokemon = this.getUserPokemon();
                if (tar.getTargetData() != userPokemon) {
                    if (tar instanceof CameraTargetEntity) {
                        tar.setTargetData(userPokemon);
                    } else if (!PixelmonConfig.playerControlCamera) {
                        ClientProxy.camera.setTarget(new CameraTargetEntity(userPokemon));
                    }
                }
            } else if (!PixelmonConfig.playerControlCamera) {
                ClientProxy.camera.setTarget(new CameraTargetEntity(Minecraft.getMinecraft().player));
            }
        }
    }

    public void setCameraToPlayer() {
        if (ClientProxy.camera != null) {
            ICameraTarget tar = ClientProxy.camera.getTarget();
            if (tar != null) {
                if (tar.getTargetData() != this.getViewPlayer()) {
                    if (tar instanceof CameraTargetEntity) {
                        tar.setTargetData(this.getViewPlayer());
                    } else if (!PixelmonConfig.playerControlCamera) {
                        ClientProxy.camera.setTarget(new CameraTargetEntity(this.getViewPlayer()));
                    }
                }
            } else if (ClientProxy.camera == null || this.getViewPlayer() == null) {
                Pixelmon.LOGGER.warn("Problem finding battle camera.");
            } else if (!PixelmonConfig.playerControlCamera) {
                ClientProxy.camera.setTarget(new CameraTargetEntity(this.getViewPlayer()));
            }
        }
    }

    public void setViewEntity(Entity entity) {
        Minecraft mc = Minecraft.getMinecraft();
        mc.setRenderViewEntity(entity);
        mc.gameSettings.hideGUI = true;
        this.thirdP = mc.gameSettings.thirdPersonView;
        mc.gameSettings.thirdPersonView = 0;
        GuiIngameForge.renderHotbar = false;
        GuiIngameForge.renderCrosshairs = false;
        GuiIngameForge.renderExperiance = false;
        GuiIngameForge.renderAir = false;
        GuiIngameForge.renderHealth = false;
        GuiIngameForge.renderFood = false;
        GuiIngameForge.renderArmor = false;
    }

    public Entity getViewEntity() {
        return Minecraft.getMinecraft().getRenderViewEntity();
    }

    public void resetViewEntity() {
        Minecraft mc = Minecraft.getMinecraft();
        mc.setRenderViewEntity(mc.player);
        GuiIngameForge.renderHotbar = true;
        GuiIngameForge.renderCrosshairs = true;
        GuiIngameForge.renderExperiance = true;
        GuiIngameForge.renderAir = true;
        GuiIngameForge.renderHealth = true;
        GuiIngameForge.renderFood = true;
        GuiIngameForge.renderArmor = true;
        mc.gameSettings.hideGUI = false;
        mc.gameSettings.thirdPersonView = this.thirdP;
        if (ClientProxy.camera != null) {
            ClientProxy.camera.setDead();
        }
        ClientProxy.camera = null;
    }

    public void resetAFKTime() {
        this.setAFKTimer(this.afkActive ? this.afkTurn : this.afkActivate);
    }

    private void setAFKTimer(int time) {
        this.afkTime = time;
        if (this.afkTimer != null) {
            this.afkTimer.cancel();
        }
        this.afkTimer = new Timer();
        this.afkTimer.scheduleAtFixedRate((TimerTask)new AFKTask(this), 1000L, 1000L);
    }

    public void setTeamSelectTime() {
        if (this.rules.teamSelectTime > 0) {
            this.afkOn = true;
            this.setAFKTimer(this.rules.teamSelectTime);
        } else {
            this.afkOn = false;
        }
    }

    public void selectRunAction(int[] pokemonID) {
        if (this.canForfeit()) {
            this.oldMode = this.mode;
            this.mode = BattleMode.YesNoForfeit;
        } else {
            Pixelmon.NETWORK.sendToServer(new Flee(pokemonID));
            this.mode = BattleMode.Waiting;
        }
    }

    public void checkClearedMessages() {
        if (!(this.hasMoreMessages() || this.mode != BattleMode.Waiting && this.mode != BattleMode.MainMenu || this.choosingPokemon || this.hasLevelUps())) {
            this.onClearedMessages();
            if (this.afkOn) {
                this.resetAFKTime();
            }
        }
    }

    public boolean canUltraBurst() {
        if (this.ultraBurst != null) {
            return false;
        }
        for (IMessage message : this.selectedActions) {
            if (!(message instanceof ChooseAttack)) continue;
            ChooseAttack attackMessage = (ChooseAttack)message;
            if (!attackMessage.evolving) continue;
            return false;
        }
        return true;
    }

    public boolean canMegaEvolve() {
        if (this.megaEvolution != null) {
            return false;
        }
        for (IMessage message : this.selectedActions) {
            if (!(message instanceof ChooseAttack)) continue;
            ChooseAttack attackMessage = (ChooseAttack)message;
            if (!attackMessage.evolving) continue;
            return false;
        }
        return true;
    }

    public boolean canDynamax() {
        if (!EntityPlayerExtension.getPlayerDynamaxItem(Minecraft.getMinecraft().player).canDynamax()) {
            return false;
        }
        if (this.getUserPokemonPacket().getSpecies().equals((Object)EnumSpecies.Groudon) && this.getUserPokemonPacket().heldItem.getItem() == PixelmonItemsHeld.redOrb || this.getUserPokemonPacket().getSpecies().equals((Object)EnumSpecies.Kyogre) && this.getUserPokemonPacket().heldItem.getItem() == PixelmonItemsHeld.blueOrb) {
            return false;
        }
        if (this.getUserPokemonPacket().isMegaEvolved()) {
            return false;
        }
        if (this.getUserPokemonPacket().getSpecies().equals((Object)EnumSpecies.Zacian) || this.getUserPokemonPacket().getSpecies().equals((Object)EnumSpecies.Zamazenta) || this.getUserPokemonPacket().getSpecies().equals((Object)EnumSpecies.Eternatus)) {
            return false;
        }
        if (this.dynamax != null) {
            return false;
        }
        for (IMessage message : this.selectedActions) {
            if (!(message instanceof ChooseAttack)) continue;
            ChooseAttack attackMessage = (ChooseAttack)message;
            if (!attackMessage.dynamaxing) continue;
            return false;
        }
        return true;
    }
}

