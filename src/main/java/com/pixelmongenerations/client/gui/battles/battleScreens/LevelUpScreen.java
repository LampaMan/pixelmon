/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.battleScreens;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.battleScreens.BattleScreen;
import com.pixelmongenerations.client.gui.battles.battleScreens.EnumLevelStage;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.PixelmonStatsData;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.util.RegexPatterns;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.text.translation.I18n;

public class LevelUpScreen
extends BattleScreen {
    private EnumLevelStage drawLevelStage = EnumLevelStage.First;
    int flashCount = 0;

    public LevelUpScreen(GuiBattle parent) {
        super(parent, BattleMode.LevelUp);
    }

    @Override
    public void drawScreen(int width, int height, int mouseX, int mouseY) {
        Minecraft mc = Minecraft.getMinecraft();
        if (ClientProxy.camera != null) {
            this.parent.bm.setCameraToPlayer();
        }
        mc.renderEngine.bindTexture(GuiResources.levelUpPopup);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(width / 2 - 52, height / 2 - 66, 104.0, 113.0f, 0.0, 0.0, 0.40625, 0.44140625, this.zLevel);
        if (!this.bm.levelUpList.isEmpty()) {
            this.drawString(mc.fontRenderer, I18n.translateToLocal("nbt.hp2.name"), width / 2 - 43, height / 2 - 54, 0xFFFFFF);
            this.drawString(mc.fontRenderer, I18n.translateToLocal("nbt.attack2.name"), width / 2 - 43, height / 2 - 38, 0xFFFFFF);
            this.drawString(mc.fontRenderer, I18n.translateToLocal("nbt.defense2.name"), width / 2 - 43, height / 2 - 22, 0xFFFFFF);
            this.drawString(mc.fontRenderer, I18n.translateToLocal("nbt.spattack2.name"), width / 2 - 43, height / 2 - 6, 0xFFFFFF);
            this.drawString(mc.fontRenderer, I18n.translateToLocal("nbt.spdefense2.name"), width / 2 - 43, height / 2 + 10, 0xFFFFFF);
            this.drawString(mc.fontRenderer, I18n.translateToLocal("nbt.speed2.name"), width / 2 - 43, height / 2 + 26, 0xFFFFFF);
            if (this.drawLevelStage == EnumLevelStage.First) {
                this.drawString(mc.fontRenderer, "+" + (this.bm.levelUpList.get((int)0).statsLevel2.HP - this.bm.levelUpList.get((int)0).statsLevel1.HP), width / 2 + 25, height / 2 - 54, 0xFFFFFF);
                this.drawString(mc.fontRenderer, "+" + (this.bm.levelUpList.get((int)0).statsLevel2.Attack - this.bm.levelUpList.get((int)0).statsLevel1.Attack), width / 2 + 25, height / 2 - 38, 0xFFFFFF);
                this.drawString(mc.fontRenderer, "+" + (this.bm.levelUpList.get((int)0).statsLevel2.Defence - this.bm.levelUpList.get((int)0).statsLevel1.Defence), width / 2 + 25, height / 2 - 22, 0xFFFFFF);
                this.drawString(mc.fontRenderer, "+" + (this.bm.levelUpList.get((int)0).statsLevel2.SpecialAttack - this.bm.levelUpList.get((int)0).statsLevel1.SpecialAttack), width / 2 + 25, height / 2 - 6, 0xFFFFFF);
                this.drawString(mc.fontRenderer, "+" + (this.bm.levelUpList.get((int)0).statsLevel2.SpecialDefence - this.bm.levelUpList.get((int)0).statsLevel1.SpecialDefence), width / 2 + 25, height / 2 + 10, 0xFFFFFF);
                this.drawString(mc.fontRenderer, "+" + (this.bm.levelUpList.get((int)0).statsLevel2.Speed - this.bm.levelUpList.get((int)0).statsLevel1.Speed), width / 2 + 25, height / 2 + 26, 0xFFFFFF);
            } else if (this.drawLevelStage == EnumLevelStage.Second) {
                PixelmonStatsData stats = this.bm.levelUpList.get((int)0).statsLevel2;
                this.drawString(mc.fontRenderer, "" + stats.HP, stats.HP < 100 ? width / 2 + 28 : width / 2 + 22, height / 2 - 54, 0xFFFFFF);
                this.drawString(mc.fontRenderer, "" + stats.Attack, stats.Attack < 100 ? width / 2 + 28 : width / 2 + 22, height / 2 - 38, 0xFFFFFF);
                this.drawString(mc.fontRenderer, "" + stats.Defence, stats.Defence < 100 ? width / 2 + 28 : width / 2 + 22, height / 2 - 22, 0xFFFFFF);
                this.drawString(mc.fontRenderer, "" + stats.SpecialAttack, stats.SpecialAttack < 100 ? width / 2 + 28 : width / 2 + 22, height / 2 - 6, 0xFFFFFF);
                this.drawString(mc.fontRenderer, "" + stats.SpecialDefence, stats.SpecialDefence < 100 ? width / 2 + 28 : width / 2 + 22, height / 2 + 10, 0xFFFFFF);
                this.drawString(mc.fontRenderer, "" + stats.Speed, stats.Speed < 100 ? width / 2 + 28 : width / 2 + 22, height / 2 + 26, 0xFFFFFF);
            }
            mc.renderEngine.bindTexture(GuiResources.battleGui3);
            PixelmonData selected = ServerStorageDisplay.get(this.bm.levelUpList.get((int)0).pokemonID);
            if (selected == null) {
                this.parent.restoreSettingsAndClose();
                return;
            }
            String name = selected.getEscapedNickname();
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            GuiHelper.drawImageQuad(width / 2 - this.parent.getGuiWidth() / 2, height - this.parent.getGuiHeight(), this.parent.getGuiWidth(), this.parent.getGuiHeight(), 0.0, 0.0, 1.0, 0.30416667461395264, this.zLevel);
            this.drawCenteredString(mc.fontRenderer, RegexPatterns.$_L_VAR.matcher(RegexPatterns.$_P_VAR.matcher(I18n.translateToLocal("gui.levelupscreen.lvlup")).replaceAll(name)).replaceAll(Integer.toString(this.bm.levelUpList.get((int)0).level)), width / 2, height - 35, 0xFFFFFF);
        }
        ++this.flashCount;
        if (this.flashCount > 30) {
            mc.renderEngine.bindTexture(GuiResources.battleGui3);
            GuiHelper.drawImageQuad(width / 2 + 130, height - 15, 10.0, 6.0f, 0.9546874761581421, 0.31041666865348816, 0.98125f, 0.33125f, this.zLevel);
            if (this.flashCount > 60) {
                this.flashCount = 0;
            }
        }
    }

    @Override
    public void click(int width, int height, int mouseX, int mouseY) {
        if (this.drawLevelStage == EnumLevelStage.First) {
            this.drawLevelStage = EnumLevelStage.Second;
        } else {
            try {
                this.bm.levelUpList.remove(0);
            }
            catch (Exception e) {
                this.returnToBattleScreen();
                return;
            }
            if (this.bm.hasLevelUps()) {
                this.drawLevelStage = EnumLevelStage.First;
            } else if (this.bm.battleControllerIndex == -1 && !this.bm.hasNewAttacks()) {
                this.parent.restoreSettingsAndClose();
            } else {
                this.returnToBattleScreen();
            }
        }
    }

    private void returnToBattleScreen() {
        this.bm.mode = this.bm.oldMode;
        this.bm.checkClearedMessages();
    }

    @Override
    public boolean isScreen() {
        return (this.bm.mode == BattleMode.Waiting || this.bm.mode == BattleMode.LevelUp) && this.bm.hasLevelUps();
    }
}

