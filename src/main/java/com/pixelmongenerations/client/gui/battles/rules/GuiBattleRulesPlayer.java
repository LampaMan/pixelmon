/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.rules;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.battles.GuiAcceptDeny;
import com.pixelmongenerations.client.gui.battles.rules.EnumRulesGuiState;
import com.pixelmongenerations.client.gui.battles.rules.GuiBattleRulesBase;
import com.pixelmongenerations.client.gui.elements.GuiChatExtension;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.battles.AcceptDeclineBattle;
import com.pixelmongenerations.core.network.packetHandlers.battles.EnumBattleQueryResponse;
import com.pixelmongenerations.core.network.packetHandlers.battles.rules.ProposeBattleRules;
import com.pixelmongenerations.core.util.RegexPatterns;
import java.io.IOException;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.text.translation.I18n;

public class GuiBattleRulesPlayer
extends GuiBattleRulesBase {
    private GuiChatExtension chat;
    private String opponentName = "";
    private int battleQueryID;
    private EnumRulesGuiState state;
    private GuiButton proposeButton;
    private GuiButton acceptButton;
    private GuiButton changeButton;
    private GuiButton declineButton;
    private int flashCounter;
    private static final int FLASH_DURATION = 30;

    public GuiBattleRulesPlayer(int battleQueryID, boolean isProposing) {
        EntityPlayer opponentEntity;
        this.battleQueryID = battleQueryID;
        this.state = isProposing ? EnumRulesGuiState.Propose : EnumRulesGuiState.WaitPropose;
        this.chat = new GuiChatExtension(this, 20);
        if (GuiAcceptDeny.opponent != null && (opponentEntity = this.mc.world.getPlayerEntityByUUID(GuiAcceptDeny.opponent.opponentUUID)) != null) {
            this.opponentName = opponentEntity.getDisplayNameString();
        }
        this.yChange = -15;
        this.clauseListHeight = 50;
    }

    @Override
    public void initGui() {
        super.initGui();
        this.chat.initGui();
        int buttonWidth = 60;
        int idCounter = 100;
        this.proposeButton = new GuiButton(idCounter++, this.centerX - buttonWidth / 2, this.centerY + 60, buttonWidth, 20, I18n.translateToLocal("gui.battlerules.propose"));
        this.acceptButton = new GuiButton(idCounter++, this.centerX - buttonWidth, this.centerY + 60, buttonWidth, 20, I18n.translateToLocal("gui.acceptdeny.accept"));
        this.changeButton = new GuiButton(idCounter++, this.centerX, this.centerY + 60, buttonWidth, 20, I18n.translateToLocal("gui.battlerules.change"));
        this.declineButton = new GuiButton(idCounter++, this.centerX + 200 - buttonWidth, this.centerY + 60, buttonWidth, 20, I18n.translateToLocal("gui.acceptdeny.deny"));
        this.buttonList.add(this.declineButton);
        this.changeState(this.state);
    }

    public void changeState(EnumRulesGuiState state) {
        this.state = state;
        switch (state) {
            case Propose: {
                this.buttonList.remove(this.acceptButton);
                this.buttonList.remove(this.changeButton);
                this.enableButton(this.proposeButton);
                this.editingEnabled = true;
                break;
            }
            case WaitPropose: 
            case WaitChange: 
            case WaitAccept: {
                this.buttonList.remove(this.proposeButton);
                this.buttonList.remove(this.acceptButton);
                this.buttonList.remove(this.changeButton);
                this.editingEnabled = false;
                break;
            }
            case Accept: {
                this.buttonList.remove(this.proposeButton);
                this.enableButton(this.acceptButton);
                this.enableButton(this.changeButton);
                this.editingEnabled = false;
            }
        }
    }

    private void enableButton(GuiButton button) {
        if (!this.buttonList.contains(button)) {
            this.buttonList.add(button);
        }
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        this.chat.updateScreen();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.chat.drawScreen(mouseX, mouseY, partialTicks);
        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        super.drawGuiContainerBackgroundLayer(partialTicks, mouseX, mouseY);
        if (this.state != EnumRulesGuiState.Propose) {
            boolean isWaiting;
            this.dimScreen();
            String langKey = "";
            int textColor = 0;
            switch (this.state) {
                case WaitAccept: {
                    langKey = "gui.battlerules.waitaccept";
                    break;
                }
                case WaitPropose: {
                    langKey = "gui.battlerules.waitpropose";
                    break;
                }
                case WaitChange: {
                    langKey = "gui.battlerules.waitchange";
                    break;
                }
            }
            int n = this.flashCounter++;
            if ((double)n >= 45.0) {
                this.flashCounter = 0;
            }
            if (!(isWaiting = this.state.isWaiting())) {
                this.highlightButtons(80, 35);
            }
            if (this.flashCounter < 30 || !isWaiting) {
                GuiHelper.drawCenteredString(RegexPatterns.$_P_VAR.matcher(I18n.translateToLocal(langKey)).replaceAll(this.opponentName), this.centerX, this.rectBottom - 30, textColor);
            }
        }
    }

    @Override
    protected int getBackgroundHeight() {
        return 200;
    }

    @Override
    public void drawWorldBackground(int tint) {
    }

    @Override
    public void handleKeyboardInput() throws IOException {
        super.handleKeyboardInput();
        this.chat.handleKeyboardInput();
    }

    @Override
    public void handleMouseInput() throws IOException {
        super.handleMouseInput();
        this.chat.handleMouseInput();
    }

    @Override
    protected void mouseClickedUnderMenus(int x, int y, int mouseButton) throws IOException {
        super.mouseClickedUnderMenus(x, y, mouseButton);
        this.chat.mouseClicked(x, y, mouseButton);
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        if (!button.enabled) {
            return;
        }
        if (button == this.proposeButton) {
            this.registerRules();
            Pixelmon.NETWORK.sendToServer(new ProposeBattleRules(this.battleQueryID, this.rules));
        } else if (button == this.acceptButton) {
            Pixelmon.NETWORK.sendToServer(new AcceptDeclineBattle(this.battleQueryID, EnumBattleQueryResponse.Accept));
        } else if (button == this.changeButton) {
            Pixelmon.NETWORK.sendToServer(new AcceptDeclineBattle(this.battleQueryID, EnumBattleQueryResponse.Change));
        } else if (button == this.declineButton) {
            Pixelmon.NETWORK.sendToServer(new AcceptDeclineBattle(this.battleQueryID, EnumBattleQueryResponse.Decline));
            GuiHelper.closeScreen();
        }
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        this.chat.onGuiClosed();
    }
}

