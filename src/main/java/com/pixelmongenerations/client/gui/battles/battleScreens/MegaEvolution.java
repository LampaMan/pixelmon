/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.battleScreens;

import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.battleScreens.BattleScreen;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.network.packetHandlers.evolution.EvolutionStage;
import com.pixelmongenerations.core.proxy.ClientProxy;

public class MegaEvolution
extends BattleScreen {
    private static EntityPixelmon evolvingPokemon;
    int ticks = 0;
    int fadeCount = 0;
    int stage = 0;

    public MegaEvolution(GuiBattle parent) {
        super(parent, BattleMode.MegaEvolution);
    }

    @Override
    public void drawScreen(int width, int height, int mouseX, int mouseY) {
    }

    @Override
    public void click(int width, int height, int mouseX, int mouseY) {
    }

    public static void selectEntity() {
        evolvingPokemon = ClientProxy.battleManager.getEntity(ClientProxy.battleManager.megaEvolution);
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        if (evolvingPokemon == null || MegaEvolution.evolvingPokemon.evoStage == null || MegaEvolution.evolvingPokemon.evoStage == EvolutionStage.End) {
            this.parent.selectScreenImmediate(BattleMode.ChooseAttack);
            this.bm.mode = BattleMode.Waiting;
        }
    }
}

