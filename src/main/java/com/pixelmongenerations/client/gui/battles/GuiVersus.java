/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.battles.rules.GuiTeamSelect;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.common.item.ItemPokeball;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import java.util.Arrays;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public abstract class GuiVersus
extends GuiContainer {
    private static final ResourceLocation TOP_LEFT = new ResourceLocation("pixelmon:textures/gui/acceptDeny/Opponent1.png");
    private static final ResourceLocation MIDDLE = new ResourceLocation("pixelmon:textures/gui/acceptDeny/Opponent2.png");
    private static final ResourceLocation BOTTOM_RIGHT = new ResourceLocation("pixelmon:textures/gui/acceptDeny/Opponent3.png");
    private static final ResourceLocation VS = new ResourceLocation("pixelmon:textures/gui/acceptDeny/Opponent8.png");
    private static final ResourceLocation PLAYER_1_FRAME = new ResourceLocation("pixelmon:textures/gui/acceptDeny/player1Frame.png");
    private static final ResourceLocation PLAYER_2_FRAME = new ResourceLocation("pixelmon:textures/gui/acceptDeny/player2Frame.png");
    private static final ResourceLocation PLAYER_1_NAME = new ResourceLocation("pixelmon:textures/gui/acceptDeny/player1Name.png");
    private static final ResourceLocation PLAYER_2_NAME = new ResourceLocation("pixelmon:textures/gui/acceptDeny/player2Name.png");
    private static final ResourceLocation POKE_BALL_HOLDER = new ResourceLocation("pixelmon:textures/gui/acceptDeny/pokeballHolder.png");
    protected int leftX;
    protected int topY;
    protected int ticks = 5;
    protected int offset1;
    protected int offset2;
    protected int playerPartyX;
    protected int playerPartyY;
    protected int opponentPartyX;
    protected int opponentPartyY;
    protected boolean isNPC;
    private ResourceLocation npcSkin;
    protected static final int GUI_WIDTH = 280;
    protected static final int GUI_HEIGHT = 182;
    public static final int PARTY_SEPARATOR = 12;

    protected GuiVersus() {
        super(new ContainerEmpty());
        this.mc = Minecraft.getMinecraft();
    }

    @Override
    public void initGui() {
        super.initGui();
        this.updateAnchors();
    }

    @Override
    public void drawBackground(int par1) {
    }

    @Override
    public void drawDefaultBackground() {
    }

    @Override
    public void updateScreen() {
        try {
            super.updateScreen();
        }
        catch (NullPointerException nullPointerException) {
            // empty catch block
        }
        if (this.ticks > 0) {
            --this.ticks;
        }
    }

    private void updateAnchors() {
        this.leftX = (this.width - 280) / 2;
        this.topY = (this.height - 182) / 2;
        this.offset1 = -this.ticks * 50;
        this.offset2 = -this.offset1;
        this.playerPartyX = this.leftX + 84;
        this.playerPartyY = this.topY + 27;
        this.opponentPartyX = this.leftX + 120;
        this.opponentPartyY = this.topY + 121;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float f, int mouseX, int mouseY) {
        this.updateAnchors();
        this.drawRectangle();
        this.drawEntity(PLAYER_1_NAME, PLAYER_1_FRAME, this.mc.player, this.mc.player.getDisplayNameString(), this.offset1, 76, 9, 19, 11, 16, 9, 82, 27, 92, 15, false);
        EntityLivingBase opponent = this.getOpponent();
        if (opponent == null) {
            return;
        }
        String opponentName = this.isNPC ? GuiTeamSelect.teamSelectPacket.npcName : opponent.getDisplayName().getFormattedText();
        this.drawEntity(PLAYER_2_NAME, PLAYER_2_FRAME, opponent, opponentName, this.offset2, 56, 103, 202, 76, 199, 74, 118, 121, 188 - this.mc.fontRenderer.getStringWidth(opponentName), 108, this.isNPC);
        GlStateManager.disableBlend();
    }

    protected abstract EntityLivingBase getOpponent();

    private void drawRectangle() {
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GlStateManager.enableBlend();
        this.mc.renderEngine.bindTexture(TOP_LEFT);
        GuiHelper.drawImageQuad(this.leftX + this.offset1, this.topY, 280.0, 182.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        this.mc.renderEngine.bindTexture(MIDDLE);
        GuiHelper.drawImageQuad(this.leftX + this.offset2, this.topY, 280.0, 182.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        this.mc.renderEngine.bindTexture(BOTTOM_RIGHT);
        GuiHelper.drawImageQuad(this.leftX, this.topY, 280.0, 182.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        if (this.ticks == 0) {
            this.mc.renderEngine.bindTexture(VS);
            GuiHelper.drawImageQuad(this.leftX, this.topY + 2, 280.0, 226.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        }
    }

    protected void drawEntity(ResourceLocation name, ResourceLocation frame, EntityLivingBase entity, String entityName, int tickOffset, int nameBoxX, int nameBoxY, int headX, int headY, int frameX, int frameY, int holderX, int holderY, int nameX, int nameY, boolean isCurrentNPC) {
        this.mc.renderEngine.bindTexture(name);
        GuiHelper.drawImageQuad(this.leftX + nameBoxX + tickOffset, this.topY + nameBoxY, 145.0, 17.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        this.drawEntity(entity, this.leftX + headX + tickOffset, this.topY + headY, isCurrentNPC);
        this.mc.renderEngine.bindTexture(frame);
        GuiHelper.drawImageQuad(this.leftX + frameX + tickOffset, this.topY + frameY, 65.0, 65.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        this.mc.renderEngine.bindTexture(POKE_BALL_HOLDER);
        GuiHelper.drawImageQuad(this.leftX + holderX + tickOffset, this.topY + holderY, 80.0, 17.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        this.mc.fontRenderer.drawString(entityName, this.leftX + nameX + tickOffset, this.topY + nameY, 0xFFFFFF);
    }

    protected void drawPokeBalls(int[] pokeBalls, int startX, int startY, int tickOffset) {
        int pos = 0;
        for (int pid : pokeBalls) {
            if (pid != -999) {
                GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
                if (pid < 0) {
                    pid *= -1;
                    --pid;
                    GlStateManager.color(0.4f, 0.4f, 0.4f, 1.0f);
                }
                ItemPokeball pball = EnumPokeball.getFromIndex(pid).getItem();
                this.itemRender.renderItemIntoGUI(new ItemStack(pball), startX + pos * 12 + tickOffset, startY);
            }
            ++pos;
        }
    }

    protected void drawOpponentPokeBalls(int numPokeBalls) {
        int[] opponentPokeBalls = new int[numPokeBalls];
        Arrays.fill(opponentPokeBalls, 0);
        this.drawPokeBalls(opponentPokeBalls, this.opponentPartyX, this.opponentPartyY, this.offset2);
    }

    protected void drawEntity(EntityLivingBase entity, int x, int y, boolean isCurrentNPC) {
        ResourceLocation skin = null;
        if (isCurrentNPC && entity instanceof EntityNPC) {
            EntityNPC npc = (EntityNPC)entity;
            if (!npc.bindTexture() && this.npcSkin == null) {
                this.npcSkin = new ResourceLocation(npc.getTexture());
            }
            skin = this.npcSkin;
        } else {
            skin = ((AbstractClientPlayer)entity).getLocationSkin();
            this.mc.renderEngine.bindTexture(skin);
        }
        if (skin != null) {
            this.mc.renderEngine.bindTexture(skin);
        }
        GuiHelper.drawImageQuad(x, y, 60.0, 60.0f, 0.125, 0.125, 0.25, 0.25, this.zLevel);
    }
}

