/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.battleScreens.chooseMove;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.battleScreens.BattleScreen;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.network.PixelmonData;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.text.translation.I18n;

public abstract class ChooseMove
extends BattleScreen {
    public ChooseMove(GuiBattle parent, BattleMode mode) {
        super(parent, mode);
    }

    @Override
    public void drawScreen(int width, int height, int mouseX, int mouseY) {
        PixelmonData pokemon;
        this.mc.renderEngine.bindTexture(GuiResources.chooseMove2);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(width / 2 - 128, height / 2 - 102, 256.0, 205.0f, 0.0, 0.0, 1.0, 0.80078125, this.zLevel);
        if (this.mouseOverBack(width, height, mouseX, mouseY)) {
            GuiHelper.drawImageQuad(width / 2 + 109, height / 2 + 89, 12.0, 10.0f, 0.3125, 0.8171206116676331, 0.359375, 0.85546875, this.zLevel);
        }
        if ((pokemon = this.bm.getUserPokemonPacket()) == null) {
            GuiHelper.closeScreen();
            return;
        }
        GuiHelper.drawMoveset(pokemon, width, height, this.zLevel);
        for (int i = 0; i < pokemon.numMoves; ++i) {
            if (!this.mouseOverMove(i, width, height, mouseX, mouseY)) continue;
            this.mc.renderEngine.bindTexture(GuiResources.chooseMove2);
            GuiHelper.drawImageQuad(width / 2 - 30, height / 2 - 94 + 22 * i, 152.0, 24.0f, 0.37890625, 0.81640625, 0.97265625, 0.9140625, this.zLevel);
            GuiHelper.drawAttackInfoMoveset(pokemon.moveset[i].getAttack(), height / 2 + 37, width, height);
        }
        this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.replaceattack.effect"), width / 2 - 96, height / 2 + 38, 0xFFFFFF);
        this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.description.text"), width / 2 - 20, height / 2 + 22, 0xFFFFFF);
        GuiHelper.drawPokemonInfoChooseMove(pokemon, width, height, this.zLevel);
    }

    @Override
    public void click(int width, int height, int mouseX, int mouseY) {
        if (this.mouseOverBack(width, height, mouseX, mouseY)) {
            this.closeScreen();
            return;
        }
        PixelmonData pokemon = this.bm.getUserPokemonPacket();
        for (int i = 0; i < pokemon.numMoves; ++i) {
            if (!this.mouseOverMove(i, width, height, mouseX, mouseY)) continue;
            this.clickMove(i);
        }
    }

    private boolean mouseOverMove(int i, int width, int height, int mouseX, int mouseY) {
        return mouseX > width / 2 - 30 && mouseX < width / 2 + 120 && mouseY > height / 2 - 94 + 22 * i && mouseY < height / 2 - 94 + 22 * (i + 1);
    }

    private boolean mouseOverBack(int width, int height, int mouseX, int mouseY) {
        return mouseX >= width / 2 + 109 && mouseX <= width / 2 + 121 && mouseY >= height / 2 + 89 && mouseY <= height / 2 + 99;
    }

    protected abstract void clickMove(int var1);

    protected void closeScreen() {
        if (this.bm.battleEnded) {
            this.bm.choosingPokemon = false;
        } else {
            this.bm.mode = BattleMode.MainMenu;
        }
    }
}

