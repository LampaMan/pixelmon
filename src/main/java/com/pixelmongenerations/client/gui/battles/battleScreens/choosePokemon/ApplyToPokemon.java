/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.battleScreens.choosePokemon;

import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.battleScreens.choosePokemon.ChoosePokemon;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.battles.BagPacket;
import net.minecraft.item.Item;

public class ApplyToPokemon
extends ChoosePokemon {
    public ApplyToPokemon(GuiBattle parent) {
        super(parent, BattleMode.ApplyToPokemon);
    }

    @Override
    public void click(int width, int height, int mouseX, int mouseY) {
        PixelmonData pdata;
        if (this.bm.isHealing) {
            return;
        }
        if (mouseX > width / 2 + 63 && mouseX < width / 2 + 63 + 48 && mouseY > height - 27 && mouseY < height - 27 + 17) {
            this.bm.mode = BattleMode.UseBag;
            return;
        }
        int pokemonToApplyTo = -1;
        int chosenSlot = this.choosePokemonSlot(width, height, mouseX, mouseY);
        PixelmonData[] party = this.getParty();
        Item item = Item.getItemById(this.bm.itemToUse.id);
        if (chosenSlot != -1 && (pdata = party[chosenSlot]) != null) {
            if (item == PixelmonItems.revive || item == PixelmonItems.maxRevive || item == PixelmonItems.revivalHerb) {
                if (pdata.isFainted) {
                    pokemonToApplyTo = chosenSlot;
                }
            } else if (!pdata.isFainted) {
                pokemonToApplyTo = chosenSlot;
            }
        }
        if (pokemonToApplyTo != -1) {
            if (item == PixelmonItems.ether || item == PixelmonItems.maxEther || item == PixelmonItemsHeld.leppaBerry) {
                this.bm.mode = BattleMode.ChooseEther;
            } else {
                this.bm.selectedActions.add(new BagPacket(this.bm.getUserPokemonPacket().pokemonID, party[chosenSlot].pokemonID, this.bm.itemToUse.id, this.bm.battleControllerIndex));
                this.bm.selectedMove();
            }
        }
    }
}

