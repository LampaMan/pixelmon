/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.rules;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClause;
import com.pixelmongenerations.common.battle.rules.teamselection.TeamSelectPokemon;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import java.awt.Color;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;

public class TeamSelectPokemonIcon {
    private TeamSelectPokemon pokemon;
    private String disabled;
    private int x;
    private int y;
    private int tickOffset;
    public int selectIndex = -1;
    private static final int SIZE = 16;

    public TeamSelectPokemonIcon() {
    }

    public TeamSelectPokemonIcon(TeamSelectPokemon pokemon, String disabled) {
        this.pokemon = pokemon;
        this.disabled = disabled;
    }

    public TeamSelectPokemonIcon setPosition(int x, int y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public void setTickOffset(int tickOffset) {
        this.tickOffset = tickOffset;
    }

    public void drawIcon(int mouseX, int mouseY, float zLevel, boolean fullSelected) {
        if (this.pokemon == null) {
            return;
        }
        String name = this.pokemon.pokemon.pokemon.name;
        Minecraft mc = Minecraft.getMinecraft();
        int dexNumber = this.pokemon.pokemon.pokemon.getNationalPokedexInteger();
        int currentX = this.x + this.tickOffset;
        int currentY = this.y + (dexNumber > 649 ? 1 : -1);
        boolean allowed = this.disabled.isEmpty();
        boolean mouseOver = this.isMouseOver(mouseX, mouseY);
        boolean isOpponent = "n".equals(this.disabled);
        boolean unselected = this.selectIndex == -1;
        ResourceLocation circle = null;
        if (!allowed) {
            if (!isOpponent) {
                circle = GuiResources.fainted;
            }
        } else if (mouseOver) {
            circle = GuiResources.selected;
        } else if (!unselected) {
            circle = GuiResources.released;
        }
        if (circle != null) {
            mc.renderEngine.bindTexture(circle);
            int circleDiff = 4;
            int circleSize = 16 + circleDiff;
            GuiHelper.drawImageQuad(currentX - circleDiff / 2, this.y - circleDiff / 2, circleSize, circleSize, 0.0, 0.0, 1.0, 1.0, zLevel);
        }
        GuiHelper.bindPokemonSprite(dexNumber, this.pokemon.pokemon.form, this.pokemon.pokemon.gender, this.pokemon.specialTexture, this.pokemon.customTexture, this.pokemon.isShiny, this.pokemon.isEgg, this.pokemon.eggCycles, mc);
        double us = isOpponent ? 0.0 : 1.0;
        double ue = isOpponent ? 1.0 : 0.0;
        GuiHelper.drawImageQuad(currentX, currentY, 16.0, 16.0f, us, 0.0, ue, 1.0, zLevel);
        if (!unselected) {
            mc.fontRenderer.drawString(Integer.toString(this.selectIndex + 1), currentX + 8 - 3, this.y + 16 + 1, 0);
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        }
        if (mouseOver && (!allowed || unselected && fullSelected)) {
            String reason = "";
            if (isOpponent) {
                reason = Entity1Base.getLocalizedName(this.pokemon.pokemon.pokemon);
            } else if (this.disabled.equals("e")) {
                reason = I18n.translateToLocal("gui.battlerules.egg");
            } else if (this.disabled.equals("f")) {
                reason = I18n.translateToLocal("gui.battlerules.fainted");
            } else if (!this.disabled.isEmpty() && !this.disabled.equals("n")) {
                reason = I18n.translateToLocal("gui.battlerules.clauseviolated") + " " + BattleClause.getLocalizedName(this.disabled);
            } else if (fullSelected) {
                reason = I18n.translateToLocal("gui.battlerules.fullselect");
            }
            if (!reason.isEmpty()) {
                GuiHelper.renderTooltip(mouseX + 10, mouseY, mc.fontRenderer.listFormattedStringToWidth(reason, 100), Color.BLUE.getRGB(), Color.BLACK.getRGB(), 100, false, false);
            }
        }
    }

    public boolean isMouseOver(int mouseX, int mouseY) {
        if (this.tickOffset != 0 || this.pokemon == null) {
            return false;
        }
        int selectOffset = 2;
        return mouseX >= this.x + selectOffset + 1 && mouseX <= this.x + 12 + selectOffset && mouseY >= this.y && mouseY <= this.y + 16;
    }

    public boolean isDisabled() {
        return !this.disabled.isEmpty();
    }
}

