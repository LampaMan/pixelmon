/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.gui.battles.battleScreens;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.battleScreens.BattleScreen;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.PixelmonMovesetData;
import com.pixelmongenerations.core.network.packetHandlers.battles.BagPacket;
import com.pixelmongenerations.core.network.packetHandlers.battles.SetStruggle;
import com.pixelmongenerations.core.util.RegexPatterns;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.opengl.GL11;

public class MainMenu
extends BattleScreen {
    public static Integer lastId;

    public MainMenu(GuiBattle parent) {
        super(parent, BattleMode.MainMenu);
    }

    @Override
    public void drawScreen(int width, int height, int mouseX, int mouseY) {
        PixelmonData data = this.bm.getUserPokemonPacket();
        if (data != null && data.health <= 0) {
            this.bm.selectedMove();
        } else {
            if (!this.bm.selectedActions.isEmpty()) {
                this.mc.renderEngine.bindTexture(GuiResources.battleGui1B);
            } else {
                this.mc.renderEngine.bindTexture(GuiResources.battleGui1);
            }
            int guiHeight = this.parent.getGuiHeight();
            int guiWidth = this.parent.getGuiWidth();
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            GuiHelper.drawImageQuad(width / 2 - guiWidth / 2, height - guiHeight, guiWidth, guiHeight, 0.0, 0.0, 1.0, 0.30416667461395264, this.zLevel);
            this.parent.drawButton(BattleMode.MainMenu, width / 2 + 31, height - guiHeight + 9, 48, 16, I18n.translateToLocal("gui.mainmenu.fight"), mouseX, mouseY, 0);
            this.mc.renderEngine.bindTexture(GuiResources.battleGui1);
            this.parent.drawButton(BattleMode.MainMenu, width / 2 + 31, height - guiHeight + 35, 48, 16, I18n.translateToLocal("gui.mainmenu.bag"), this.bm.rules.hasClause("bag"), mouseX, mouseY, 1);
            this.mc.renderEngine.bindTexture(GuiResources.battleGui1);
            this.parent.drawButton(BattleMode.MainMenu, width / 2 + 90, height - guiHeight + 9, 48, 16, I18n.translateToLocal("gui.mainmenu.pokemon"), mouseX, mouseY, 2);
            this.mc.renderEngine.bindTexture(GuiResources.battleGui1);
            this.parent.drawButton(BattleMode.MainMenu, width / 2 + 90, height - guiHeight + 35, 48, 16, I18n.translateToLocal("gui.mainmenu.run"), this.bm.rules.hasClause("forfeit"), mouseX, mouseY, 3);
            if (lastId != null) {
                Item item = Item.getItemById(lastId);
                this.itemRender.renderItemIntoGUI(new ItemStack(item), width / 2 + 156, height - guiHeight + 37);
                int bX = width / 2 + 152;
                int bY = height - guiHeight + 33;
                int bWidth = 24;
                int bHeight = 24;
                this.mc.renderEngine.bindTexture(GuiResources.lastBall);
                GL11.glColor3f((float)0.5f, (float)0.5f, (float)0.5f);
                if (this.bm.canCatchOpponent()) {
                    for (ItemStack i : Minecraft.getMinecraft().player.inventory.mainInventory) {
                        if (i == null || Item.getIdFromItem(i.getItem()) != lastId) continue;
                        if (mouseX > bX && mouseX < bX + bWidth && mouseY > bY && mouseY < bY + bHeight) {
                            GL11.glColor3f((float)0.95f, (float)0.95f, (float)0.95f);
                            break;
                        }
                        GL11.glColor3f((float)0.8f, (float)0.8f, (float)0.8f);
                        break;
                    }
                }
                GuiHelper.drawImageQuad(bX, bY, bWidth, bHeight, 0.0, 0.0, 1.0, 1.0, this.zLevel);
                GL11.glColor3f((float)1.0f, (float)1.0f, (float)1.0f);
            }
            if (data != null) {
                String nickname = data.getEscapedNickname();
                this.drawString(this.mc.fontRenderer, RegexPatterns.$_P_VAR.matcher(I18n.translateToLocal("gui.mainmenu.whatdo")).replaceAll(nickname), width / 2 - 130, height - 35, 0xFFFFFF);
            }
            if (!this.bm.selectedActions.isEmpty() && mouseX < width / 2 - 137 && mouseX > width / 2 - 148 && mouseY > height - 11 && mouseY < height - 1) {
                this.mc.renderEngine.bindTexture(GuiResources.battleGui1B);
                GuiHelper.drawImageQuad(width / 2 - 148, height - 11, 11.0, 10.0f, 0.0078125, 0.31458333134651184, 0.0421875f, 0.35625f, this.zLevel);
            }
        }
    }

    @Override
    public void click(int width, int height, int mouseX, int mouseY) {
        int guiHeight = this.parent.getGuiHeight();
        int guiWidth = this.parent.getGuiWidth();
        int bX = width / 2 + 152;
        int bY = height - guiHeight + 33;
        int bWidth = 24;
        int bHeight = 24;
        if (lastId != null && mouseX > bX && mouseX < bX + bWidth && mouseY > bY && mouseY < bY + bHeight) {
            if (this.bm.canCatchOpponent()) {
                for (ItemStack i : Minecraft.getMinecraft().player.inventory.mainInventory) {
                    if (i == null || Item.getIdFromItem(i.getItem()) != lastId) continue;
                    Pixelmon.NETWORK.sendToServer(new BagPacket(this.bm.getUserPokemonPacket().pokemonID, lastId, this.bm.battleControllerIndex, 0));
                    this.bm.mode = BattleMode.Waiting;
                    break;
                }
            } else {
                this.bm.addMessage(I18n.translateToLocal("gui.choosebag.nopokeballs"));
            }
        }
        if (!this.bm.selectedActions.isEmpty() && mouseX < width / 2 - 137 && mouseX > width / 2 - 148 && mouseY > height - 11 && mouseY < height - 1) {
            if (this.bm.currentPokemon > 0) {
                --this.bm.currentPokemon;
                this.bm.selectedActions.remove(this.bm.selectedActions.size() - 1);
                int y1 = ServerStorageDisplay.getPokemon().length;
                for (int x2 = 0; x2 < y1; ++x2) {
                    PixelmonData data = ServerStorageDisplay.getPokemon()[x2];
                    if (data == null) continue;
                    data.selected = false;
                }
            }
            this.bm.mode = BattleMode.MainMenu;
        } else {
            int x1 = width / 2 + 31;
            int y1 = height - this.parent.getGuiHeight() + 9;
            int x2 = width / 2 + 90;
            int y2 = height - this.parent.getGuiHeight() + 35;
            int w = 48;
            int h = 16;
            if (mouseX > x1 && mouseX < x1 + w && mouseY > y1 && mouseY < y1 + h) {
                PixelmonData poke = this.bm.getUserPokemonPacket();
                boolean canAttack = false;
                for (int i = 0; i < poke.moveset.length; ++i) {
                    PixelmonMovesetData a = poke.moveset[i];
                    if (a == null || a.pp <= 0 || a.disabled) continue;
                    canAttack = true;
                }
                if (!canAttack) {
                    this.parent.setTargetting(DatabaseMoves.getAttack("Struggle"), -1, -1);
                    this.bm.selectedActions.add(new SetStruggle(this.bm.getUserPokemonPacket().pokemonID, this.bm.targetted, this.bm.battleControllerIndex));
                    this.bm.selectedMove();
                    return;
                }
                this.bm.mode = BattleMode.ChooseAttack;
                this.bm.evolving = false;
            } else if (mouseX > x2 && mouseX < x2 + w && mouseY > y1 && mouseY < y1 + h) {
                if (!this.bm.canSwitch) {
                    this.bm.addMessage(I18n.translateToLocal("gui.mainmenu.cantswitch"));
                } else {
                    this.bm.mode = BattleMode.ChoosePokemon;
                }
            } else if (mouseX > x1 && mouseX < x1 + w && mouseY > y2 && mouseY < y2 + h) {
                if (!this.bm.rules.hasClause("bag")) {
                    this.bm.mode = BattleMode.ChooseBag;
                }
            } else if (mouseX > x2 && mouseX < x2 + w && mouseY > y2 && mouseY < y2 + h) {
                if (this.bm.canRunFromBattle()) {
                    if (this.bm.selectedActions.isEmpty()) {
                        this.bm.selectRunAction(this.bm.getUserPokemonPacket().pokemonID);
                    } else {
                        this.bm.addMessage(I18n.translateToLocal("gui.mainmenu.already"));
                    }
                } else if (!this.bm.canFlee && this.bm.getUserPokemon() != null) {
                    this.bm.addMessage(RegexPatterns.$_P_VAR.matcher(I18n.translateToLocal("gui.mainmenu.traped")).replaceAll(this.bm.getUserPokemon().getEscapedNickname()));
                } else {
                    this.bm.addMessage(I18n.translateToLocal("gui.mainmenu.trainer"));
                }
            }
        }
    }
}

