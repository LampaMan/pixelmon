/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.rules;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.battles.rules.GuiBattleRulesBase;
import com.pixelmongenerations.client.gui.elements.GuiSlotBase;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClause;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;

public class GuiClauseList
extends GuiSlotBase {
    private GuiBattleRulesBase parent;
    private List<BattleClause> clauses;

    public GuiClauseList(GuiBattleRulesBase parent, List<BattleClause> clauses, int top, int left, int width, int height) {
        super(top, left, width, height, true);
        this.parent = parent;
        this.clauses = clauses;
    }

    @Override
    protected int getSize() {
        return this.clauses.size();
    }

    @Override
    protected void elementClicked(int index, boolean doubleClicked) {
        this.parent.clauseListSelected(this.clauses, index);
    }

    @Override
    protected boolean isSelected(int element) {
        return this.parent.isClauseSelected(this.clauses, element);
    }

    @Override
    protected void drawSlot(int index, int x, int yTop, int yMiddle, Tessellator tessellator) {
        if (index < 0 || index >= this.clauses.size()) {
            return;
        }
        Minecraft.getMinecraft().fontRenderer.drawString(GuiHelper.getLimitedString(this.clauses.get(index).getLocalizedName(), this.width), x + 2, yTop - 1, 0);
    }

    @Override
    protected float[] get1Color() {
        return new float[]{1.0f, 1.0f, 1.0f};
    }

    @Override
    protected int[] getSelectionColor() {
        return new int[]{128, 128, 128};
    }

    public BattleClause getElement(int index) {
        return this.clauses.get(index);
    }
}

