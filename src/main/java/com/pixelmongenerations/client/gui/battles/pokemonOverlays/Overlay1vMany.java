/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.pokemonOverlays;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.PixelmonInGui;
import com.pixelmongenerations.client.gui.battles.pokemonOverlays.OverlayBase;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.network.PixelmonData;
import java.util.ArrayList;
import net.minecraft.client.renderer.GlStateManager;

public class Overlay1vMany
extends OverlayBase {
    public Overlay1vMany(GuiBattle parent) {
        super(parent);
    }

    @Override
    public void draw(int width, int height, int guiWidth, int guiHeight) {
        this.displayMessage();
        ArrayList<PixelmonInGui> drawnPokemon = new ArrayList<PixelmonInGui>(this.bm.teamPokemon.length);
        for (int i = 0; i < this.bm.teamPokemon.length; ++i) {
            int xPos;
            PixelmonData userPokemon;
            boolean isYours = true;
            PixelmonInGui pokemon = null;
            if (!this.bm.isSpectating && (userPokemon = ServerStorageDisplay.get(this.bm.teamPokemon[i])) == null && (pokemon = this.bm.getUncontrolledTeamPokemon(this.bm.teamPokemon[i])) == null) {
                isYours = false;
            }
            if (pokemon == null && (pokemon = this.bm.getPokemon(this.bm.teamPokemon[i])) == null) continue;
            if (!isYours) {
                if ((this.bm.mode == BattleMode.ChooseTargets || this.bm.mode == BattleMode.ChooseAttack) && this.bm.targetted != null && this.bm.targetted[0][i] && pokemon.health > 0.0f) {
                    GlStateManager.color(1.0f, 0.0f, 0.0f, 0.8f);
                    this.mc.renderEngine.bindTexture(GuiResources.pokemonInfoP2);
                    xPos = width - pokemon.xPos;
                    GuiHelper.drawImageQuad(width - xPos, height - (guiHeight + 37) + (i - 1) * 47, 123.0, 38.0f, 0.0, 0.0, 0.9296875, 0.53125, this.zLevel);
                }
                this.drawOwnedPokemon(pokemon, i, width, height, guiWidth, guiHeight);
                continue;
            }
            if ((this.bm.mode == BattleMode.ChooseTargets || this.bm.mode == BattleMode.ChooseAttack) && this.bm.targetted != null && this.bm.targetted[0][i] && pokemon.health > 0.0f) {
                GlStateManager.color(1.0f, 0.0f, 0.0f, 0.8f);
                this.mc.renderEngine.bindTexture(GuiResources.pokemonInfoP1);
                xPos = width - pokemon.xPos;
                int yPos = height - (guiHeight + 37) + (pokemon.position - 1) * 47;
                GuiHelper.drawImageQuad(xPos - 2, yPos - 2, 123.0, 38.0f, 0.0, 0.0, 0.9296875, 0.53125, this.zLevel);
            }
            this.drawOwnedPokemon(pokemon, i, width, height, guiWidth, guiHeight);
        }
        this.drawSwitchingPokemon(this.bm.displayedOurPokemon, drawnPokemon, width, height, guiWidth, guiHeight);
        this.drawSwitchingPokemon(this.bm.displayedAllyPokemon, drawnPokemon, width, height, guiWidth, guiHeight);
        this.mc.renderEngine.bindTexture(GuiResources.pokemonInfoP2);
        PixelmonInGui[] targetPokemonList = this.bm.displayedEnemyPokemon;
        if (targetPokemonList != null) {
            int i;
            for (i = 0; i < targetPokemonList.length; ++i) {
                PixelmonInGui targetPokemon = targetPokemonList[i];
                if (targetPokemon == null || this.bm.mode != BattleMode.ChooseTargets && this.bm.mode != BattleMode.ChooseAttack || this.bm.targetted == null || !this.bm.targetted[1][i] || targetPokemon.health <= 0.0f) continue;
                GlStateManager.color(1.0f, 0.0f, 0.0f, 0.8f);
                this.mc.renderEngine.bindTexture(GuiResources.pokemonInfoP2);
                GuiHelper.drawImageQuad(targetPokemon.xPos, i * 37 - 2, 121.0, 38.0f, 0.0, 0.0, 0.9296875, 0.53125, this.zLevel);
            }
            for (i = 0; i < targetPokemonList.length; ++i) {
                PixelmonInGui targetPokemon = targetPokemonList[i];
                if (targetPokemon == null) {
                    System.out.println("Found null");
                    continue;
                }
                this.drawOpponentPokemon(targetPokemon, i, width, height, guiWidth, guiHeight);
            }
        }
    }

    private void drawSwitchingPokemon(PixelmonInGui[] activePokemon, ArrayList<PixelmonInGui> drawnPokemon, int width, int height, int guiWidth, int guiHeight) {
        if (activePokemon != null) {
            for (PixelmonInGui pokemon : activePokemon) {
                if (drawnPokemon.contains(pokemon)) continue;
                this.drawOwnedPokemon(pokemon, pokemon.position, width, height, guiWidth, guiHeight);
            }
        }
    }

    @Override
    protected void drawOpponentPokemon(PixelmonInGui targetPokemon, int position, int width, int height, int guiWidth, int guiHeight) {
        this.checkPosition(targetPokemon, position, this.bm.displayedEnemyPokemon);
        super.drawOpponentPokemon(targetPokemon, targetPokemon.position, width, height, guiWidth, guiHeight);
    }

    @Override
    protected void drawOwnedPokemon(PixelmonInGui userPokemon, int position, int width, int height, int guiWidth, int guiHeight) {
        this.checkPosition(userPokemon, position, this.bm.displayedOurPokemon);
        super.drawOwnedPokemon(userPokemon, userPokemon.position, width, height, guiWidth, guiHeight);
    }

    private void checkPosition(PixelmonInGui checkPokemon, int position, PixelmonInGui[] pokemonList) {
        if (checkPokemon.position != position && pokemonList != null) {
            boolean hasPosition = false;
            for (PixelmonInGui pokemon : pokemonList) {
                if (pokemon.position != position) continue;
                hasPosition = true;
            }
            if (!hasPosition) {
                checkPokemon.position = position;
            }
        }
    }

    @Override
    public int mouseOverEnemyPokemon(int guiWidth, int guiHeight, int mouseX, int mouseY) {
        PixelmonInGui[] targetPokemonList = this.bm.displayedEnemyPokemon;
        for (int i = 0; i < targetPokemonList.length; ++i) {
            PixelmonInGui targetPokemon = targetPokemonList[i];
            if (targetPokemon == null || targetPokemon.health <= 0.0f || mouseX < 0 || mouseX >= 121 || mouseY <= i * 37 - 2 || mouseY >= i * 37 - 2 + 38) continue;
            return i;
        }
        return -1;
    }

    @Override
    public int mouseOverUserPokemon(int width, int height, int guiWidth, int guiHeight, int mouseX, int mouseY) {
        for (int i = 0; i < this.bm.teamPokemon.length; ++i) {
            PixelmonInGui ally;
            PixelmonData userPokemon = ServerStorageDisplay.get(this.bm.teamPokemon[i]);
            boolean hasAlly = false;
            if (userPokemon == null && (ally = this.bm.getUncontrolledTeamPokemon(this.bm.teamPokemon[i])) != null && ally.health > 0.0f) {
                hasAlly = true;
            }
            if ((userPokemon == null || userPokemon.isFainted) && !hasAlly) continue;
            int xPos = width - 120;
            int yPos = height - (guiHeight + 37) + (i - 1) * 47;
            if (mouseX <= xPos - 2 || mouseX >= xPos - 2 + 123 || mouseY <= yPos - 2 || mouseY >= yPos - 2 + 38) continue;
            return i;
        }
        return -1;
    }
}

