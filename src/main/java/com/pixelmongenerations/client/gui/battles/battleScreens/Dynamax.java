/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.battleScreens;

import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.battleScreens.BattleScreen;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.network.packetHandlers.evolution.EvolutionStage;
import com.pixelmongenerations.core.proxy.ClientProxy;

public class Dynamax
extends BattleScreen {
    private static EntityPixelmon dynamaxingPokemon;
    int ticks = 0;
    int fadeCount = 0;
    int stage = 0;

    public Dynamax(GuiBattle parent) {
        super(parent, BattleMode.Dynamax);
    }

    @Override
    public void drawScreen(int width, int height, int mouseX, int mouseY) {
    }

    @Override
    public void click(int width, int height, int mouseX, int mouseY) {
    }

    public static void selectEntity() {
        dynamaxingPokemon = ClientProxy.battleManager.getEntity(ClientProxy.battleManager.dynamax);
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        if (dynamaxingPokemon == null || Dynamax.dynamaxingPokemon.evoStage == null || Dynamax.dynamaxingPokemon.evoStage == EvolutionStage.End) {
            this.parent.selectScreenImmediate(BattleMode.ChooseAttack);
            this.bm.mode = BattleMode.Waiting;
        }
    }
}

