/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.pokemonOverlays;

import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.PixelmonInGui;
import com.pixelmongenerations.client.gui.battles.pokemonOverlays.OverlayBase;

public class Overlay1v1
extends OverlayBase {
    public Overlay1v1(GuiBattle parent) {
        super(parent);
    }

    @Override
    public void draw(int width, int height, int guiWidth, int guiHeight) {
        this.displayMessage();
        if (this.bm.displayedOurPokemon == null || this.bm.displayedOurPokemon.length == 0) {
            return;
        }
        PixelmonInGui userPokemon = this.bm.displayedOurPokemon[0];
        if (this.bm.getUserPokemonPacket() == null && !this.bm.isSpectating) {
            return;
        }
        this.drawOwnedPokemon(userPokemon, 1, width, height, guiWidth, guiHeight);
        PixelmonInGui targetPokemon = this.bm.displayedEnemyPokemon.length > 0 ? this.bm.displayedEnemyPokemon[0] : null;
        if (targetPokemon != null) {
            this.drawOpponentPokemon(targetPokemon, 0, width, height, guiWidth, guiHeight);
        }
    }

    @Override
    public int mouseOverEnemyPokemon(int guiWidth, int guiHeight, int mouseX, int mouseY) {
        return -1;
    }

    @Override
    public int mouseOverUserPokemon(int width, int height, int guiWidth, int guiHeight, int mouseX, int mouseY) {
        return -1;
    }
}

