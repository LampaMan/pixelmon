/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.battleScreens.yesNo;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.battleScreens.yesNo.YesNoDialogue;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.util.RegexPatterns;
import net.minecraft.util.text.translation.I18n;

public class YesNoReplaceMove
extends YesNoDialogue {
    public YesNoReplaceMove(GuiBattle parent) {
        super(parent, BattleMode.YesNoReplaceMove);
    }

    @Override
    protected void drawConfirmText(int width, int height) {
        Attack newAttack;
        if (this.bm.newAttackList.isEmpty()) {
            this.parent.restoreSettingsAndClose();
            this.bm.mode = this.bm.oldMode;
            return;
        }
        PixelmonData pokemonToLearnAttack = ServerStorageDisplay.get(this.bm.newAttackList.get((int)0).pokemonID);
        try {
            newAttack = this.bm.newAttackList.get((int)0).attack;
        }
        catch (Exception e) {
            Pixelmon.LOGGER.warn("User is clicking too fast.");
            Pixelmon.LOGGER.warn("New attack list is " + e.getMessage());
            this.bm.mode = this.bm.oldMode;
            return;
        }
        float textAreaWidth = 170.0f;
        if (this.bm.selectedAttack == -1) {
            String attackName = newAttack.getAttackBase().getLocalizedName();
            float textWidth = this.mc.fontRenderer.getStringWidth(RegexPatterns.$_M_VAR.matcher(I18n.translateToLocal("gui.yesno.yousure")).replaceAll(attackName));
            int numLines = (int)(textWidth / textAreaWidth) + 1;
            this.mc.fontRenderer.drawSplitString(RegexPatterns.$_M_VAR.matcher(I18n.translateToLocal("gui.yesno.yousure")).replaceAll(attackName), width / 2 - 109, height / 2 + 1 - numLines * 10 / 2, (int)textAreaWidth, 0);
            this.mc.fontRenderer.drawSplitString(RegexPatterns.$_M_VAR.matcher(I18n.translateToLocal("gui.yesno.yousure")).replaceAll(attackName), width / 2 - 110, height / 2 - numLines * 10 / 2, (int)textAreaWidth, 0xFFFFFF);
        } else {
            if (pokemonToLearnAttack == null) {
                return;
            }
            String text = RegexPatterns.$_M_VAR.matcher(I18n.translateToLocal("gui.yesno.replace")).replaceAll(pokemonToLearnAttack.moveset[this.bm.selectedAttack].getAttack().getAttackBase().getLocalizedName());
            text = RegexPatterns.$_NM_VAR.matcher(text).replaceAll(newAttack.getAttackBase().getLocalizedName());
            float textWidth = this.mc.fontRenderer.getStringWidth(text);
            int numLines = (int)(textWidth / textAreaWidth) + 1;
            this.mc.fontRenderer.drawSplitString(text, width / 2 - 109, height / 2 + 1 - numLines * 10 / 2, (int)textAreaWidth, 0);
            this.mc.fontRenderer.drawSplitString(text, width / 2 - 110, height / 2 - numLines * 10 / 2, (int)textAreaWidth, 0xFFFFFF);
        }
    }

    @Override
    protected void confirm() {
        if (this.bm.newAttackList.isEmpty()) {
            this.closeBattleScreen();
            return;
        }
        this.bm.newAttackList.remove(0);
        if (this.bm.sendPacket != null) {
            Pixelmon.NETWORK.sendToServer(this.bm.sendPacket);
        }
        if (this.bm.battleControllerIndex != -1) {
            if (this.bm.hasNewAttacks() || this.bm.hasLevelUps()) {
                this.bm.mode = this.bm.yesNoOrigin;
            } else {
                if (this.bm.battleEnded) {
                    this.closeBattleScreen();
                    return;
                }
                this.bm.mode = BattleMode.MainMenu;
                this.bm.checkClearedMessages();
            }
        } else {
            this.closeBattleScreen();
            return;
        }
    }

    private void closeBattleScreen() {
        this.parent.restoreSettingsAndClose();
        this.bm.mode = this.bm.oldMode;
    }
}

