/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.battleScreens.chooseMove;

import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.battleScreens.chooseMove.ChooseMove;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.network.packetHandlers.battles.UsePPUp;

public class ChoosePPUp
extends ChooseMove {
    public ChoosePPUp(GuiBattle parent) {
        super(parent, BattleMode.ChoosePPUp);
    }

    @Override
    protected void clickMove(int moveIndex) {
        int[] pokemonID = this.bm.getUserPokemonPacket().pokemonID;
        Pixelmon.NETWORK.sendToServer(new UsePPUp(moveIndex, pokemonID));
        this.closeScreen();
    }
}

