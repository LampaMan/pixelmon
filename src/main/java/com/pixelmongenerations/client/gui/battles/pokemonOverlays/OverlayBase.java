/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.pokemonOverlays;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.battles.ClientBattleManager;
import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.PixelmonInGui;
import com.pixelmongenerations.client.gui.pokedex.ClientPokedexManager;
import com.pixelmongenerations.common.battle.attacks.IBattleMessage;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.pokedex.Pokedex;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.util.PixelmonMethods;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.text.translation.I18n;

public abstract class OverlayBase
extends GuiScreen {
    GuiBattle parent;
    ClientBattleManager bm;

    public OverlayBase(GuiBattle parent) {
        this.parent = parent;
        this.mc = Minecraft.getMinecraft();
        this.bm = ClientProxy.battleManager;
        this.bm.getUserPokemonPacket();
    }

    public abstract void draw(int var1, int var2, int var3, int var4);

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void displayMessage() {
        ArrayList<IBattleMessage> arrayList;
        ArrayList<IBattleMessage> arrayList2 = arrayList = this.bm.battleMessages;
        synchronized (arrayList2) {
            if (!this.bm.battleMessages.isEmpty()) {
                try {
                    ArrayList<IBattleMessage> ibms = new ArrayList<IBattleMessage>();
                    ibms.add(this.bm.battleMessages.get(0));
                    for (int i = 1; i < this.bm.battleMessages.size(); ++i) {
                        IBattleMessage ibm = this.bm.battleMessages.get(i);
                        boolean conflicts = false;
                        for (IBattleMessage ibmMatch : ibms) {
                            if (ibm.messageType == ibmMatch.messageType && !PixelmonMethods.isIDSame(ibm.pokemonID, ibmMatch.pokemonID)) continue;
                            conflicts = true;
                            break;
                        }
                        if (conflicts) break;
                        ibms.add(ibm);
                    }
                    for (IBattleMessage ibm : ibms) {
                        if (ibm == null || ibm.viewed) continue;
                        ibm.viewed = true;
                        ibm.process();
                    }
                }
                catch (IndexOutOfBoundsException indexOutOfBoundsException) {
                    // empty catch block
                }
            }
        }
    }

    protected void drawExpBar(int x, int y, int width, int height, PixelmonInGui p) {
        GlStateManager.enableRescaleNormal();
        GlStateManager.enableColorMaterial();
        GlStateManager.pushMatrix();
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder worldRenderer = tessellator.getBuffer();
        GlStateManager.disableTexture2D();
        int barWidth = (int)(p.expFraction * ((float)width - 6.0f));
        worldRenderer.begin(7, DefaultVertexFormats.POSITION_COLOR);
        worldRenderer.pos(x, y, 0.0).color(0.0f, 0.0f, 0.4f, 1.0f).endVertex();
        worldRenderer.pos(x, y + height, 0.0).color(0.0f, 0.0f, 0.4f, 1.0f).endVertex();
        worldRenderer.pos(x + width, y + height, 0.0).color(0.0f, 0.0f, 0.4f, 1.0f).endVertex();
        worldRenderer.pos(x + width, y, 0.0).color(0.0f, 0.0f, 0.4f, 1.0f).endVertex();
        worldRenderer.pos(x, y, 0.0).color(0.3f, 1.0f, 1.0f, 1.0f).endVertex();
        worldRenderer.pos(x, y + height, 0.0).color(0.3f, 1.0f, 1.0f, 1.0f).endVertex();
        worldRenderer.pos(x + barWidth, y + height, 0.0).color(0.3f, 1.0f, 1.0f, 1.0f).endVertex();
        worldRenderer.pos(x + barWidth, y, 0.0).color(0.3f, 1.0f, 1.0f, 1.0f).endVertex();
        tessellator.draw();
        GlStateManager.popMatrix();
        GlStateManager.enableTexture2D();
        GlStateManager.disableRescaleNormal();
        GlStateManager.disableColorMaterial();
    }

    protected boolean hasCaught(PixelmonInGui targetPokemon) {
        Pokedex pokedex = ClientPokedexManager.pokedex;
        if (pokedex == null) {
            return false;
        }
        try {
            int nationalPokedexNumber = Entity3HasStats.getBaseStats((String)targetPokemon.pokemonName).get().nationalPokedexNumber;
            return pokedex.hasCaught(nationalPokedexNumber);
        }
        catch (NoSuchElementException e) {
            return false;
        }
    }

    protected void drawOpponentPokemon(PixelmonInGui targetPokemon, int position, int width, int height, int guiWidth, int guiHeight) {
        String targetName = targetPokemon.getDisplayName();
        this.mc.renderEngine.bindTexture(GuiResources.pokemonInfoP2);
        int yPos = position * 37;
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        targetPokemon.xPos = Math.min(0, targetPokemon.xPos);
        GuiHelper.drawImageQuad(targetPokemon.xPos, yPos, 119.0, 34.0f, 0.0, 0.0, 0.9296875, 0.53125, this.zLevel);
        this.drawString(this.mc.fontRenderer, targetName, targetPokemon.xPos + 8, yPos + 8, 0xFFFFFF);
        if (targetPokemon.status != -1 && StatusType.getEffect(targetPokemon.status) != null) {
            float[] texturePair2 = StatusType.getTexturePos(StatusType.getEffect(targetPokemon.status));
            this.mc.renderEngine.bindTexture(GuiResources.status);
            GuiHelper.drawImageQuad(targetPokemon.xPos + 70, yPos + 18, 24.0, 10.5f, texturePair2[0] / 299.0f, texturePair2[1] / 210.0f, (texturePair2[0] + 147.0f) / 299.0f, (texturePair2[1] + 68.0f) / 210.0f, this.zLevel);
        }
        this.mc.renderEngine.bindTexture(GuiResources.pokemonInfoP2);
        GuiBattle.drawHealthBar(targetPokemon.xPos + 18, yPos + 19, 56, 6, targetPokemon.health, targetPokemon.maxHealth);
        GuiHelper.drawImageQuad(targetPokemon.xPos + 8, yPos + 18, 62.0, 9.0f, 0.0078125, 0.671875, 0.4921875, 0.828125, this.zLevel);
        boolean caught = this.hasCaught(targetPokemon);
        if (targetPokemon.gender == Gender.Male.ordinal()) {
            GuiHelper.drawImageQuad(targetPokemon.xPos + 8 + this.mc.fontRenderer.getStringWidth(targetName), yPos + 6, 7.0, 10.0f, 0.5625, 0.65625, 0.6171875, 0.8125, this.zLevel);
        } else if (targetPokemon.gender == Gender.Female.ordinal()) {
            GuiHelper.drawImageQuad(targetPokemon.xPos + 8 + this.mc.fontRenderer.getStringWidth(targetName), yPos + 6, 7.0, 10.0f, 0.5, 0.65625, 0.5546875, 0.8125, this.zLevel);
        }
        int iconSize = 13;
        int iconX = 106 - iconSize;
        if (targetPokemon.shiny) {
            int iconY = yPos + 18;
            int yOffset = 0;
            if (caught) {
                yOffset = -3;
                GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
                this.mc.renderEngine.bindTexture(GuiResources.caught);
                GuiHelper.drawImageQuad(targetPokemon.xPos + iconX, iconY - yOffset, 10.0, 10.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            }
            this.mc.renderEngine.bindTexture(GuiResources.pokemonInfoP2);
            this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.overlay1v1.lv") + targetPokemon.level, targetPokemon.xPos + 111 - this.mc.fontRenderer.getStringWidth(I18n.translateToLocal("gui.overlay1v1.lv") + targetPokemon.level), yPos + 8, 0xFFFFFF);
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            this.mc.renderEngine.bindTexture(GuiResources.shiny);
            GuiHelper.drawImageQuad(targetPokemon.xPos + iconX, iconY + yOffset, 10.0, 10.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        } else {
            if (caught) {
                GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
                this.mc.renderEngine.bindTexture(GuiResources.caught);
                GuiHelper.drawImageQuad(targetPokemon.xPos + iconX, yPos + 18, 10.0, 10.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            }
            this.mc.renderEngine.bindTexture(GuiResources.pokemonInfoP2);
            this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.overlay1v1.lv") + targetPokemon.level, targetPokemon.xPos + 111 - this.mc.fontRenderer.getStringWidth(I18n.translateToLocal("gui.overlay1v1.lv") + targetPokemon.level), yPos + 8, 0xFFFFFF);
        }
        GuiHelper.bindPokemonSprite(targetPokemon.getDexNumber(), targetPokemon.form, Gender.getGender(targetPokemon.gender), targetPokemon.specialTexture, targetPokemon.customTexture, targetPokemon.shiny, this.mc);
        GuiHelper.drawImageQuad(targetPokemon.xPos + 112 - iconSize, yPos + 16, iconSize, iconSize, 1.0, 0.0, 0.0, 1.0, this.zLevel);
    }

    protected void drawOwnedPokemon(PixelmonInGui userPokemon, int position, int width, int height, int guiWidth, int guiHeight) {
        userPokemon.xPos = Math.min(120, userPokemon.xPos);
        int xPos = width - userPokemon.xPos;
        int yPos = height - (guiHeight + 37) + (position - 1) * 47;
        this.mc.renderEngine.bindTexture(GuiResources.pokemonInfoP1);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(xPos, yPos - 8, 120.0, 45.0f, 0.0, 0.0, 0.9375, 0.703125, this.zLevel);
        String name = userPokemon.getDisplayName();
        this.drawString(this.mc.fontRenderer, name, xPos + 7, yPos, 0xFFFFFF);
        this.mc.renderEngine.bindTexture(GuiResources.pokemonInfoP1);
        PixelmonData userData = ServerStorageDisplay.get(userPokemon.pokemonID);
        if (userData != null) {
            if (!userPokemon.isSwitching) {
                userPokemon.expFraction = userData.getExpFraction();
            }
            this.drawExpBar(xPos + 6, yPos + 29, 105, 4, userPokemon);
        }
        GuiHelper.drawImageQuad(xPos + 5, yPos + 26, 109.0, 7.0f, 0.0078125, 0.875, 0.859375, 0.96875, this.zLevel);
        GuiBattle.drawHealthBar(xPos + 19, yPos + 11, 97, 6, userPokemon.health, userPokemon.maxHealth);
        GuiHelper.drawImageQuad(xPos + 9, yPos + 9, 103.0, 9.0f, 0.0078125, 0.703125, 0.8125, 0.859375, this.zLevel);
        if (!this.bm.isSpectating) {
            this.drawString(this.mc.fontRenderer, "" + Math.round(userPokemon.health) + "/" + userPokemon.maxHealth, xPos + 110 - this.mc.fontRenderer.getStringWidth("" + Math.round(userPokemon.health) + "/" + userPokemon.maxHealth), yPos + 19, 0xFFFFFF);
        }
        if (userPokemon.status != -1) {
            float[] texturePair = StatusType.getTexturePos(StatusType.getEffect(userPokemon.status));
            this.mc.renderEngine.bindTexture(GuiResources.status);
            GuiHelper.drawImageQuad(width - userPokemon.xPos + 15, yPos + 18, 20.0, 8.75f, texturePair[0] / 299.0f, texturePair[1] / 210.0f, (texturePair[0] + 147.0f) / 299.0f, (texturePair[1] + 68.0f) / 210.0f, this.zLevel);
        }
        this.mc.renderEngine.bindTexture(GuiResources.pokemonInfoP1);
        if (userPokemon.gender == Gender.Male.ordinal()) {
            GuiHelper.drawImageQuad(xPos + 7 + this.mc.fontRenderer.getStringWidth(name), yPos - 2, 7.0, 10.0f, 0.9296875, 0.8125, 0.984375, 0.96875, this.zLevel);
        } else if (userPokemon.gender == Gender.Female.ordinal()) {
            GuiHelper.drawImageQuad(xPos + 7 + this.mc.fontRenderer.getStringWidth(name), yPos - 2, 7.0, 10.0f, 0.8671875, 0.8125, 0.921875, 0.96875, this.zLevel);
        }
        this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.overlay1v1.lv") + userPokemon.level, xPos + 110 - this.mc.fontRenderer.getStringWidth(I18n.translateToLocal("gui.overlay1v1.lv") + userPokemon.level), yPos, 0xFFFFFF);
        GuiHelper.bindPokemonSprite(userPokemon.getDexNumber(), userPokemon.form, Gender.getGender(userPokemon.gender), userPokemon.specialTexture, userPokemon.customTexture, userPokemon.shiny, this.mc);
        int iconSize = 10;
        if (userPokemon.shiny) {
            this.mc.renderEngine.bindTexture(GuiResources.shiny);
            GuiHelper.drawImageQuad(xPos + 14, yPos + 18, iconSize, iconSize, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            GuiHelper.bindPokemonSprite(userPokemon.getDexNumber(), userPokemon.form, Gender.getGender(userPokemon.gender), userPokemon.specialTexture, userPokemon.shiny, this.mc);
        }
        GuiHelper.drawImageQuad(xPos + 6, yPos + 17, iconSize, iconSize, 0.0, 0.0, 1.0, 1.0, this.zLevel);
    }

    public abstract int mouseOverEnemyPokemon(int var1, int var2, int var3, int var4);

    public abstract int mouseOverUserPokemon(int var1, int var2, int var3, int var4, int var5, int var6);
}

