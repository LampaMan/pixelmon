/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.battleScreens.choosePokemon;

import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.battleScreens.choosePokemon.ChoosePokemon;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.battles.SwitchPokemon;

public class EnforcedSwitch
extends ChoosePokemon {
    public static boolean failFlee;

    public EnforcedSwitch(GuiBattle parent) {
        super(parent, BattleMode.EnforcedSwitch);
    }

    @Override
    protected String getBackText() {
        if (failFlee || this.getSwitchingPokemon().health > 0) {
            return "";
        }
        return "gui.fainterChoice.run";
    }

    @Override
    protected void addSwitch(int position) {
        failFlee = false;
        this.bm.selectedActions.add(new SwitchPokemon(position, this.bm.battleControllerIndex, this.getSwitchingPokemon().pokemonID, true));
        if (this.bm.afkOn) {
            this.bm.resetAFKTime();
        }
    }

    @Override
    protected void clickBackButton() {
        if (!"".equals(this.backText)) {
            this.bm.selectRunAction(this.getSwitchingPokemon().pokemonID);
        }
    }

    private PixelmonData getSwitchingPokemon() {
        if (this.bm.currentPokemon < 0) {
            this.bm.currentPokemon = 0;
        }
        if (this.inBattle.isEmpty()) {
            return new PixelmonData();
        }
        return (PixelmonData)this.inBattle.get(this.bm.currentPokemon % this.inBattle.size());
    }

    @Override
    protected void selectedMove() {
        this.bm.selectedMove(true);
    }
}

