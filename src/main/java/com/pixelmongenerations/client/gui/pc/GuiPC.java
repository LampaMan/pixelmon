/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Mouse
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.gui.pc;

import com.pixelmongenerations.api.pc.SearchSystem;
import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.elements.GuiButtonHoverDisable;
import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.client.gui.overlay.OverlayType;
import com.pixelmongenerations.client.gui.overlay.PartyOverlay;
import com.pixelmongenerations.client.gui.pc.GuiReleaseWarning;
import com.pixelmongenerations.client.gui.pc.GuiRenameBox;
import com.pixelmongenerations.client.gui.pc.GuiSelectBackground;
import com.pixelmongenerations.client.gui.pokechecker.GuiPokeCheckerTabs;
import com.pixelmongenerations.client.gui.ranchblock.GuiPCRanch;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.storage.PCClient;
import com.pixelmongenerations.core.storage.PCClientStorage;
import com.pixelmongenerations.core.storage.PCPos;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.awt.Rectangle;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

public class GuiPC
extends GuiContainer {
    protected int boxNumber = PCClientStorage.getLastBoxOpen();
    protected int trashX;
    protected int trashY;
    protected int pcLeft;
    protected int pcRight;
    protected int pcTop;
    protected int pcBottom;
    protected int partyLeft;
    protected int partyRight;
    protected int partyTop;
    protected int partyBottom;
    protected int slotWidth = 30;
    protected int slotHeight = 28;
    protected int pcNumWidth = 6;
    protected int partyNumWidth = 6;
    protected int pcNumHeight = 5;
    protected PCClient pcClient = new PCClient();
    GuiButton pMenuButtonSumm;
    GuiButton pMenuButtonMove;
    GuiButton pMenuButtonStat;
    Rectangle buttonBounds;
    Rectangle buttonBoundsMoves;
    Rectangle buttonBoundsStat;
    protected boolean pixelmonMenuOpen = false;
    public static PixelmonData selected = null;
    protected int menuX;
    protected int menuY;
    private int menuLeft;
    private int menuTop;
    private static final int MENU_WIDTH = 67;
    private static final int MENU_HEIGHT = 73;
    public PixelmonData mouseHeldPokemon = null;
    protected boolean goingToPokeChecker = false;
    protected GuiTextField searchText;
    protected GuiTextField pcBox;
    protected final String sortName = "BYNAME";
    protected final String sortDexNumber = "BYDEXNUMBER";
    protected String currentSearchType = null;
    protected int sortX;
    protected int sortY;
    private SearchSystem searchSystem;
    private long clickedTime;
    public boolean startClick = false;

    public GuiPC() {
        super(new ContainerEmpty());
        this.pcClient.unselectAll();
    }

    public GuiPC(PixelmonData targetPacket, int box) {
        this();
        this.mouseHeldPokemon = targetPacket;
        this.boxNumber = box;
    }

    @Override
    public void initGui() {
        super.initGui();
        this.searchSystem = new SearchSystem();
        this.buttonList.clear();
        this.trashX = this.width / 2 - 91 + 202;
        this.trashY = this.height / 6 + 110;
        this.pcLeft = this.width / 2 - 90;
        this.pcRight = this.pcLeft + this.pcNumWidth * this.slotWidth;
        this.pcTop = this.height / 6 - 5;
        this.pcBottom = this.pcTop + this.pcNumHeight * this.slotHeight;
        this.partyLeft = this.width / 2 - 90;
        this.partyRight = this.partyLeft + this.partyNumWidth * this.slotWidth;
        this.partyTop = this.height / 6 + 147;
        this.partyBottom = this.partyTop + this.slotHeight;
        this.searchText = new GuiTextField(2, this.fontRenderer, this.width / 2 - 90, this.partyBottom + 0, this.partyRight - this.partyLeft, 20);
        this.searchText.setMaxStringLength(100);
        this.searchText.setFocused(true);
        this.pcBox = new GuiTextField(3, this.fontRenderer, this.width / 2 - 91 + 205, this.height / 6 - 30, 20, 20);
        this.pcBox.setMaxStringLength(2);
        this.pcBox.setText(String.valueOf(this.boxNumber + 1));
        this.sortX = this.width / 2 - 91 + 144;
        this.sortY = this.height / 6 - 27;
        GuiResources.setCursor(GuiResources.pointCursor);
        PixelmonData data = this.pcClient.getSelected();
        if (data != null) {
            data.selected = false;
        }
        this.startClick = false;
    }

    public PixelmonData getSlotAt(int x, int y) {
        PixelmonData pkt = null;
        pkt = this.getPCAt(x, y);
        return pkt != null ? pkt : this.getPartyAt(x, y);
    }

    public PCPos getPosAt(int x, int y) {
        PCPos pos = null;
        pos = this.getPCPosAt(x, y);
        return pos != null ? pos : this.getPartyPosAt(x, y);
    }

    public PlayerStorage getStorage(UUID uuid) {
        return PixelmonStorage.pokeBallManager.getPlayerStorageFromUUID(uuid).get();
    }

    protected PCPos getPartyPosAt(int x, int y) {
        int xSizeBox = 226;
        int ySizeBox = 207;
        int xSizeParty = 103;
        int ySizeParty = 207;
        int pcBoxTopLeft = this.width / 2 - xSizeBox / 2 + xSizeParty / 2;
        int pcBoxTop = (int)((double)this.height * 0.45 - (double)(ySizeParty / 2));
        for (int i = 0; i < 6; ++i) {
            int xPos = i % 2 == 0 ? pcBoxTopLeft - xSizeParty + 11 : pcBoxTopLeft - xSizeParty / 2 + 5;
            int yPos = (int)((double)this.height * 0.45 - (double)(ySizeParty / 2));
            yPos += 3;
            if (i % 2 == 0) {
                yPos += i * 19;
            } else {
                yPos -= 10;
                yPos += i * 19;
            }
            if (i == 2) {
                --yPos;
            }
            if (i == 4) {
                --yPos;
            }
            if (i == 3) {
                --yPos;
            }
            if (i == 5) {
                --yPos;
            }
            if (x < xPos || y < yPos || x >= xPos + 30 || y >= yPos + 30) continue;
            return new PCPos(-1, i);
        }
        return null;
    }

    protected PCPos getPCPosAt(int x, int y) {
        if (x >= this.pcLeft && x < this.pcRight && y >= this.pcTop + 5 && y <= this.pcBottom + 5) {
            double xInd = ((double)x - (double)this.pcLeft) / (double)this.slotWidth;
            double yInd = ((double)y - ((double)this.pcTop + 5.0)) / (double)this.slotHeight;
            int ind = (int)Math.floor(yInd) * 6 + (int)Math.floor(xInd);
            return new PCPos(this.boxNumber, ind > 29 ? 29 : ind);
        }
        return null;
    }

    protected PixelmonData getPartyAt(int x, int y) {
        int xSizeBox = 226;
        int ySizeBox = 207;
        int xSizeParty = 103;
        int ySizeParty = 207;
        int pcBoxTopLeft = this.width / 2 - xSizeBox / 2 + xSizeParty / 2;
        int pcBoxTop = (int)((double)this.height * 0.45 - (double)(ySizeParty / 2));
        for (int i = 0; i < 6; ++i) {
            int xPos = i % 2 == 0 ? pcBoxTopLeft - xSizeParty + 11 : pcBoxTopLeft - xSizeParty / 2 + 5;
            int yPos = (int)((double)this.height * 0.45 - (double)(ySizeParty / 2));
            yPos += 3;
            if (i % 2 == 0) {
                yPos += i * 19;
            } else {
                yPos -= 10;
                yPos += i * 19;
            }
            if (i == 2) {
                --yPos;
            }
            if (i == 4) {
                --yPos;
            }
            if (i == 3) {
                --yPos;
            }
            if (i == 5) {
                --yPos;
            }
            if (x < xPos || y < yPos || x >= xPos + 30 || y >= yPos + 30) continue;
            return this.getPartyAt(i);
        }
        return null;
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        if (this.searchText.isFocused()) {
            if (this.searchText.getText().isEmpty()) {
                this.boxNumber = 0;
                this.pcBox.setText("1");
            }
            this.searchText.textboxKeyTyped(typedChar, keyCode);
            if (keyCode == 1) {
                super.keyTyped(typedChar, keyCode);
            }
            if (this.searchSystem != null) {
                this.searchSystem.updateSearch(this.searchText.getText().split(" "));
            }
            return;
        }
        if (this.pcBox.isFocused()) {
            this.pcBox.textboxKeyTyped(typedChar, keyCode);
            try {
                int box = Integer.parseInt(this.pcBox.getText());
                if (box > 0 && box <= PlayerComputerStorage.boxCount) {
                    this.boxNumber = box - 1;
                    this.pcBox.setText(String.valueOf(this.boxNumber + 1));
                }
            }
            catch (Exception exception) {
                // empty catch block
            }
        }
        super.keyTyped(typedChar, keyCode);
    }

    protected PixelmonData getPartyAt(double index) {
        return this.pcClient.getPokemonAtPos(-1, (int)Math.floor(index));
    }

    protected PixelmonData getPCAt(int x, int y) {
        if (x >= this.pcLeft && x < this.pcRight && y >= this.pcTop + 5 && y <= this.pcBottom + 5) {
            double xInd = ((double)x - (double)this.pcLeft) / (double)this.slotWidth;
            double yInd = ((double)y - ((double)this.pcTop + 5.0)) / (double)this.slotHeight;
            int ind = (int)Math.floor(yInd) * 6 + (int)Math.floor(xInd);
            return ind > 29 ? null : this.pcClient.getPokemonAtPos(this.boxNumber, ind);
        }
        return null;
    }

    public boolean checkIfLast() {
        return this.pcClient.hasOneInParty();
    }

    public int numSelected() {
        return this.pcClient.numSelected();
    }

    private boolean isMouseInMenu(int x, int y) {
        return this.pixelmonMenuOpen && x >= this.menuLeft && x <= this.menuLeft + 67 && y >= this.menuTop && y <= this.menuTop + 73;
    }

    @Override
    protected void mouseClicked(int x, int y, int par3) throws IOException {
        this.startClick = true;
        boolean clickInMenu = this.isMouseInMenu(x, y);
        if (!clickInMenu) {
            super.mouseClicked(x, y, par3);
        }
        int xSizeBox = 226;
        int ySizeBox = 207;
        int xSizeParty = 103;
        int pcBoxTopLeft = this.width / 2 - xSizeBox / 2 + xSizeParty / 2;
        int ySizeParty = 207;
        int pcBoxTop = (int)((double)this.height * 0.45 - (double)(ySizeParty / 2));
        if (new Rectangle(pcBoxTopLeft + 33, pcBoxTop + 1, 160, 30).contains(x, y)) {
            this.mc.displayGuiScreen(new GuiRenameBox(this, this.boxNumber));
        }
        this.searchText.mouseClicked(x, y, par3);
        this.pcBox.mouseClicked(x, y, par3);
        if (!(this instanceof GuiPCRanch)) {
            if (par3 == 0 && this.pixelmonMenuOpen) {
                EnumGui pokeCheckerGUI = null;
                if (this.buttonBounds.contains(x, y)) {
                    pokeCheckerGUI = EnumGui.PokeChecker;
                } else if (this.buttonBoundsMoves.contains(x, y)) {
                    pokeCheckerGUI = EnumGui.PokeCheckerMoves;
                } else if (this.buttonBoundsStat.contains(x, y)) {
                    pokeCheckerGUI = EnumGui.PokeCheckerStats;
                }
                if (pokeCheckerGUI != null) {
                    this.mc.player.openGui(Pixelmon.INSTANCE, pokeCheckerGUI.getIndex(), this.mc.world, -1, this.boxNumber, 0);
                    return;
                }
                if (!clickInMenu) {
                    this.closePokeCheckerMenu();
                }
            }
            if (!clickInMenu) {
                List<PixelmonData> matches;
                PixelmonData p = this.getSlotAt(x, y);
                if (!this.searchText.getText().isEmpty()) {
                    matches = this.searchPokemon(this.searchText.getText());
                    double xInd = ((double)x - (double)this.pcLeft) / (double)this.slotWidth;
                    double yInd = ((double)y - ((double)this.pcTop + 5.0)) / (double)this.slotHeight;
                    int ind = this.boxNumber * PlayerComputerStorage.boxCount + (int)Math.floor(yInd) * 6 + (int)Math.floor(xInd);
                    p = ind < matches.size() && ind >= 0 && yInd < 5.0 && xInd < 6.0 ? matches.get(ind) : null;
                } else if (this.currentSearchType != null) {
                    matches = this.sortPokemon(this.currentSearchType);
                    double xInd = ((double)x - (double)this.pcLeft) / (double)this.slotWidth;
                    double yInd = ((double)y - ((double)this.pcTop + 5.0)) / (double)this.slotHeight;
                    int ind = this.boxNumber * PlayerComputerStorage.boxCount + (int)Math.floor(yInd) * 6 + (int)Math.floor(xInd);
                    PixelmonData pixelmonData = p = ind < matches.size() && ind >= 0 && yInd < 5.0 && xInd < 6.0 ? matches.get(ind) : null;
                }
                if (p != null && par3 == 1) {
                    if (this.pixelmonMenuOpen) {
                        this.closePokeCheckerMenu();
                    }
                    this.menuX = x;
                    this.menuY = y;
                    if (y >= this.partyTop && y <= this.partyBottom) {
                        this.menuY -= 50;
                    }
                    this.pMenuButtonSumm = new GuiPokeCheckerTabs(6, 3, x - 63, this.menuY + 5, 47, 13, I18n.translateToLocal("gui.screenpokechecker.summary"));
                    this.pMenuButtonMove = new GuiPokeCheckerTabs(6, 4, x - 63, this.menuY + 24, 47, 13, I18n.translateToLocal("gui.screenpokechecker.moves"));
                    this.pMenuButtonStat = new GuiPokeCheckerTabs(6, 5, x - 63, this.menuY + 43, 47, 13, I18n.translateToLocal("gui.screenpokechecker.stats"));
                    this.buttonBounds = new Rectangle(x - 63, this.menuY + 5, 47, 13);
                    this.buttonBoundsMoves = new Rectangle(x - 63, this.menuY + 24, 47, 13);
                    this.buttonBoundsStat = new Rectangle(x - 63, this.menuY + 43, 47, 13);
                    this.pixelmonMenuOpen = true;
                    selected = p;
                    this.startClick = false;
                } else {
                    if (p != null && this.numSelected() < 1 && !p.isInRanch) {
                        p.selected = true;
                        this.clickedTime = System.currentTimeMillis();
                    }
                    if (new Rectangle(pcBoxTopLeft - 35, pcBoxTop + ySizeParty - 36, 32, 32).contains(x, y)) {
                        PixelmonData selected = this.pcClient.getSelected();
                        if (this.numSelected() == 1 && (selected != null && selected.isEgg || ServerStorageDisplay.countNonEgg() > 1 || this.pcClient.getPos((PixelmonData)selected).box >= 0)) {
                            this.goingToPokeChecker = true;
                            this.mc.displayGuiScreen(new GuiReleaseWarning(this));
                        }
                    } else if (new Rectangle(pcBoxTopLeft - 70, pcBoxTop + ySizeParty - 30, 28, 19).contains(x, y)) {
                        this.mc.displayGuiScreen(new GuiSelectBackground(this, this.boxNumber));
                    } else if (new Rectangle(pcBoxTopLeft - 90, pcBoxTop + ySizeParty - 71, 78, 30).contains(x, y)) {
                        this.mc.player.closeScreen();
                    }
                    if (new Rectangle(pcBoxTopLeft + 2, pcBoxTop, 23, 32).contains(x, y)) {
                        this.boxNumber = this.boxNumber == 0 ? PlayerComputerStorage.boxCount - 1 : (this.boxNumber = this.boxNumber - 1);
                        this.mc.fontRenderer.drawString(PCClientStorage.getBoxName(this.boxNumber), this.width / 2 - 33, this.height / 6 - 20, 0xFFFFFF);
                    } else if (new Rectangle(pcBoxTopLeft + 201, pcBoxTop, 23, 32).contains(x, y)) {
                        this.boxNumber = this.boxNumber == PlayerComputerStorage.boxCount - 1 ? 0 : (this.boxNumber = this.boxNumber + 1);
                        this.mc.fontRenderer.drawString(PCClientStorage.getBoxName(this.boxNumber), this.width / 2 - 33, this.height / 6 - 20, 0xFFFFFF);
                    }
                }
            }
        }
    }

    @Override
    protected void mouseReleased(int mouseX, int mouseY, int state) {
        if (!this.startClick) {
            return;
        }
        this.startClick = false;
        int xSizeBox = 226;
        int ySizeBox = 207;
        int xSizeParty = 103;
        int pcBoxTopLeft = this.width / 2 - xSizeBox / 2 + xSizeParty / 2;
        int ySizeParty = 207;
        int pcBoxTop = (int)((double)this.height * 0.45 - (double)(ySizeParty / 2));
        if (new Rectangle(pcBoxTopLeft + 2, pcBoxTop, 23, 32).contains(mouseX, mouseY)) {
            return;
        }
        if (new Rectangle(pcBoxTopLeft + 201, pcBoxTop, 23, 32).contains(mouseX, mouseY)) {
            return;
        }
        long timeSince = System.currentTimeMillis() - this.clickedTime;
        PixelmonData p = this.getSlotAt(mouseX, mouseY);
        if (p != null && !p.isInRanch && timeSince > 150L) {
            this.pcClient.swapPokemonWithSelected(p);
        } else if (p == null && this.pcClient.getSelected() != null && (ServerStorageDisplay.count() > 1 || this.pcClient.getPos((PixelmonData)this.pcClient.getSelected()).box >= 0)) {
            PCPos pos = this.getPosAt(mouseX, mouseY);
            if (new Rectangle(pcBoxTopLeft - 35, pcBoxTop + ySizeParty - 36, 32, 32).contains(mouseX, mouseY)) {
                PixelmonData selected = this.pcClient.getSelected();
                if (this.numSelected() == 1 && (selected != null && selected.isEgg || ServerStorageDisplay.countNonEgg() > 1 || this.pcClient.getPos((PixelmonData)selected).box >= 0)) {
                    this.goingToPokeChecker = true;
                    this.mc.displayGuiScreen(new GuiReleaseWarning(this));
                }
                return;
            }
            if (pos == null) {
                this.pcClient.getSelected().selected = false;
                return;
            }
            this.pcClient.swapPositionWithSelected(pos);
        }
        super.mouseReleased(mouseX, mouseY, state);
    }

    protected List<PixelmonData> searchPokemon(String text) {
        text = this.searchSystem.getRemainingString();
        ArrayList<PixelmonData> matches = new ArrayList<PixelmonData>();
        for (int box = 0; box < PlayerComputerStorage.boxCount; ++box) {
            for (int pos = 0; pos < 30; ++pos) {
                boolean containsName;
                PixelmonData pokemon = this.pcClient.getPokemonAtPos(box, pos);
                if (pokemon == null) continue;
                boolean bl = containsName = pokemon.getSpecies().name.toLowerCase().startsWith(text) || pokemon.getNickname().toLowerCase().startsWith(text);
                if (!text.isEmpty() && !containsName || !this.searchSystem.match(pokemon)) continue;
                matches.add(pokemon);
            }
        }
        return matches;
    }

    protected List<PixelmonData> sortPokemon(String sortBy) {
        ArrayList<PixelmonData> matches = new ArrayList<PixelmonData>();
        for (int box = 0; box < PlayerComputerStorage.boxCount; ++box) {
            for (int pos = 0; pos < 30; ++pos) {
                PixelmonData pokemon = this.pcClient.getPokemonAtPos(box, pos);
                if (pokemon == null) continue;
                matches.add(pokemon);
            }
        }
        if (sortBy.equalsIgnoreCase("BYNAME")) {
            matches.sort((pixelmonData, secondData) -> pixelmonData.name.compareToIgnoreCase(secondData.name));
        } else if (sortBy.equalsIgnoreCase("BYDEXNUMBER")) {
            matches.sort(Comparator.comparing(pixelmonData -> pixelmonData.nationalPokedexNumber));
        }
        return matches;
    }

    void releasePokemon() {
        this.pcClient.deletePokemon();
    }

    private void closePokeCheckerMenu() {
        this.pMenuButtonSumm = null;
        this.pMenuButtonMove = null;
        this.pMenuButtonStat = null;
        this.pixelmonMenuOpen = false;
        selected = null;
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        if (!this.goingToPokeChecker) {
            if (this.pcClient.getSelected() != null) {
                this.pcClient.getSelected().selected = false;
            }
            PartyOverlay overlay = (PartyOverlay)GuiPixelmonOverlay.getOverlay(OverlayType.PARTY);
            overlay.checkSelection();
        }
        GuiResources.setCursor(GuiResources.defaultCursor);
    }

    @Override
    public void actionPerformed(GuiButton button) {
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawPC(partialTicks, mouseX, mouseY);
        GuiButtonHoverDisable.setHoverDisabledScreen(this.buttonList, this.isMouseInMenu(mouseX, mouseY));
        this.searchText.drawTextBox();
        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        int ind;
        double yInd;
        double xInd;
        super.drawGuiContainerForegroundLayer(mouseX, mouseY);
        GlStateManager.pushMatrix();
        GlStateManager.translate(-this.guiLeft, -this.guiTop, 0.0f);
        PixelmonData slot = this.getSlotAt(mouseX, mouseY);
        if (this.currentSearchType != null) {
            List<PixelmonData> matches = this.sortPokemon(this.currentSearchType);
            xInd = ((double)mouseX - (double)this.pcLeft) / (double)this.slotWidth;
            yInd = ((double)mouseY - ((double)this.pcTop + 5.0)) / (double)this.slotHeight;
            ind = this.boxNumber * PlayerComputerStorage.boxCount + (int)Math.floor(yInd) * 6 + (int)Math.floor(xInd);
            PixelmonData pixelmonData = slot = ind >= matches.size() || ind < 0 ? null : matches.get(ind);
        }
        if (!this.searchText.getText().isEmpty()) {
            List<PixelmonData> list;
            xInd = ((double)mouseX - (double)this.pcLeft) / (double)this.slotWidth;
            yInd = ((double)mouseY - ((double)this.pcTop + 5.0)) / (double)this.slotHeight;
            ind = this.boxNumber * PlayerComputerStorage.boxCount + (int)Math.floor(yInd) * 6 + (int)Math.floor(xInd);
            PixelmonData pixelmonData = slot = ind >= (list = this.searchPokemon(this.searchText.getText())).size() || ind < 0 ? null : list.get(ind);
        }
        if (!Mouse.getNativeCursor().equals((Object)GuiResources.fistCursor)) {
            if (slot != null && !(this instanceof GuiPCRanch)) {
                GuiResources.setCursor(GuiResources.grabCursor);
            } else {
                GuiResources.setCursor(GuiResources.pointCursor);
            }
        }
        if (slot != null && !slot.selected) {
            GuiHelper.drawPokemonInfoPC(mouseX, mouseY, slot, this.zLevel);
        }
        if (this.pixelmonMenuOpen) {
            this.drawButtonContainer();
        }
        for (GuiButton button : new GuiButton[]{this.pMenuButtonMove, this.pMenuButtonStat, this.pMenuButtonSumm}) {
            if (button == null) continue;
            button.drawButton(this.mc, mouseX, mouseY, 0.0f);
        }
        GlStateManager.popMatrix();
    }

    protected void drawPC(float partialTicks, int mouseX, int mouseY) {
        int yPos;
        int xPos;
        int pokemonIndex;
        int index;
        int x2;
        int x;
        String background = PCClientStorage.getBoxBackground(this.boxNumber);
        TextureResource texture = ClientProxy.TEXTURE_STORE.getObject(background);
        if (texture != null) {
            texture.bindTexture();
        } else {
            this.mc.renderEngine.bindTexture(GuiResources.background(background));
        }
        int xSizeBox = 226;
        int ySizeBox = 207;
        int xSizeParty = 103;
        int ySizeParty = 207;
        int pcBoxTopLeft = this.width / 2 - xSizeBox / 2 + xSizeParty / 2;
        int pcBoxTop = (int)((double)this.height * 0.45 - (double)(ySizeParty / 2));
        GuiHelper.drawImageQuad(pcBoxTopLeft, pcBoxTop, xSizeBox, ySizeBox, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        this.searchText.x = this.width / 2 - this.searchText.width / 2;
        this.searchText.y = pcBoxTop + ySizeBox + 3;
        this.slotWidth = 35;
        this.slotHeight = 33;
        this.pcLeft = this.width / 2 - 55;
        this.pcRight = this.pcLeft + this.pcNumWidth * this.slotWidth;
        this.pcTop = pcBoxTop + this.slotHeight;
        this.pcBottom = this.pcTop + this.pcNumHeight * this.slotHeight;
        this.mc.renderEngine.bindTexture(GuiResources.partyPCPanel);
        GuiHelper.drawImageQuad(pcBoxTopLeft - xSizeParty, (double)this.height * 0.45 - (double)(ySizeParty / 2), xSizeParty, ySizeParty, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        boolean selected = false;
        this.mc.renderEngine.bindTexture(GuiResources.pcBox);
        this.drawTexturedModalRect(pcBoxTopLeft - 35, pcBoxTop + ySizeParty - 36, 0, 224, 32, 32);
        this.mc.renderEngine.bindTexture(GuiResources.pcBackgrounds);
        GuiHelper.drawImageQuad(pcBoxTopLeft - 70, pcBoxTop + ySizeParty - 30, 28.0, 19.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        if (this.searchText.getText().isEmpty()) {
            if (this.currentSearchType == null) {
                for (x = 0; x < this.pcNumWidth; ++x) {
                    for (int y = 0; y < this.pcNumHeight; ++y) {
                        PixelmonData pkt = this.pcClient.getPokemonAtPos(this.boxNumber, y * this.pcNumWidth + x);
                        if (pkt == null) continue;
                        GuiHelper.bindPokemonSprite(pkt, this.mc);
                        int xPos2 = this.pcLeft + x * this.slotWidth;
                        int yPos2 = this.pcTop + y * this.slotHeight;
                        if (pkt.selected) {
                            xPos2 = mouseX - this.slotWidth / 2;
                            yPos2 = mouseY - (this.slotHeight / 2 + 5);
                        }
                        GuiHelper.drawImageQuad(xPos2 + 2, yPos2 + 3 - (pkt.isGen6Sprite() ? -3 : 0), 30.0, 30.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
                        if (!pkt.heldItem.isEmpty()) {
                            this.mc.renderEngine.bindTexture(GuiResources.heldItem);
                            GuiHelper.drawImageQuad(xPos2 + 22, yPos2 + 22, 8.0, 8.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
                        }
                        if (pkt.selected) {
                            selected = true;
                        }
                        if (!pkt.isInRanch) continue;
                        this.mc.renderEngine.bindTexture(GuiResources.padlock);
                        GuiHelper.drawImageQuad(xPos2 + 2, yPos2 + 22, 8.0, 8.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
                    }
                }
            } else {
                List<PixelmonData> matches = this.sortPokemon(this.currentSearchType);
                x2 = 0;
                int y = 0;
                for (index = 0; index < matches.size(); ++index) {
                    PixelmonData pokemon;
                    pokemonIndex = this.boxNumber * PlayerComputerStorage.boxCount + index;
                    if (pokemonIndex >= matches.size() || (pokemon = matches.get(pokemonIndex)) == null) continue;
                    GuiHelper.bindPokemonSprite(pokemon, this.mc);
                    xPos = this.pcLeft + x2 * this.slotWidth;
                    yPos = this.pcTop + y * this.slotHeight;
                    if (pokemon.selected) {
                        xPos = mouseX - this.slotWidth / 2;
                        yPos = mouseY - (this.slotHeight / 2 + 5);
                    }
                    GuiHelper.drawImageQuad(xPos + 2, yPos + 3 - (pokemon.isGen6Sprite() ? -3 : 0), 30.0, 30.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
                    if (!pokemon.heldItem.isEmpty()) {
                        this.mc.renderEngine.bindTexture(GuiResources.heldItem);
                        GuiHelper.drawImageQuad(xPos + 22, yPos + 22, 8.0, 8.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
                    }
                    if (pokemon.selected) {
                        selected = true;
                    }
                    if (pokemon.isInRanch) {
                        this.mc.renderEngine.bindTexture(GuiResources.padlock);
                        GuiHelper.drawImageQuad(xPos + 2, yPos + 22, 8.0, 8.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
                    }
                    if (++x2 != 6) continue;
                    if (++y != 5) {
                        x2 = 0;
                        continue;
                    }
                    break;
                }
            }
        } else {
            List<PixelmonData> matches = this.searchPokemon(this.searchText.getText());
            x2 = 0;
            int y = 0;
            for (index = 0; index < matches.size(); ++index) {
                PixelmonData pokemon;
                pokemonIndex = this.boxNumber * PlayerComputerStorage.boxCount + index;
                if (pokemonIndex >= matches.size() || (pokemon = matches.get(pokemonIndex)) == null) continue;
                GuiHelper.bindPokemonSprite(pokemon, this.mc);
                xPos = this.pcLeft + x2 * this.slotWidth;
                yPos = this.pcTop + y * this.slotHeight;
                if (pokemon.selected) {
                    xPos = mouseX - this.slotWidth / 2;
                    yPos = mouseY - (this.slotHeight / 2 + 5);
                }
                GuiHelper.drawImageQuad(xPos + 2, yPos + 3 - (pokemon.isGen6Sprite() ? -3 : 0), 30.0, 30.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
                if (!pokemon.heldItem.isEmpty()) {
                    this.mc.renderEngine.bindTexture(GuiResources.heldItem);
                    GuiHelper.drawImageQuad(xPos + 22, yPos + 22, 8.0, 8.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
                }
                if (pokemon.selected) {
                    this.mc.renderEngine.bindTexture(GuiResources.pcBox);
                    GuiHelper.drawImageQuad(xPos, yPos + 4, this.slotWidth, this.slotHeight + 1, 0.0, 0.55859375, 0.12109375, 0.68359375, 0.0f);
                    selected = true;
                }
                if (pokemon.isInRanch) {
                    this.mc.renderEngine.bindTexture(GuiResources.padlock);
                    GuiHelper.drawImageQuad(xPos + 2, yPos + 22, 8.0, 8.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
                }
                if (++x2 != 6) continue;
                if (++y != 5) {
                    x2 = 0;
                    continue;
                }
                break;
            }
        }
        if (!(this instanceof GuiPCRanch)) {
            if (selected) {
                GuiResources.setCursor(GuiResources.fistCursor);
            } else if (Mouse.getNativeCursor().equals((Object)GuiResources.fistCursor)) {
                PixelmonData data = PCClientStorage.getSelected();
                if (data != null) {
                    GuiHelper.bindPokemonSprite(data, this.mc);
                    int xPos3 = mouseX - this.slotWidth / 2;
                    int yPos3 = mouseY - (this.slotHeight / 2 + 5);
                    GuiHelper.drawImageQuad(xPos3 + 2, yPos3 + 3 - (data.isGen6Sprite() ? -3 : 0), 30.0, 30.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
                    if (!data.heldItem.isEmpty()) {
                        this.mc.renderEngine.bindTexture(GuiResources.heldItem);
                        GuiHelper.drawImageQuad(xPos3 + 22, yPos3 + 22, 8.0, 8.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
                    }
                } else {
                    GuiResources.setCursor(GuiResources.pointCursor);
                }
            }
        }
        for (x = 0; x < this.getPartySize(); ++x) {
            PixelmonData pkt = this.getPartyPokemon(x);
            if (pkt == null) continue;
            int xPos4 = x % 2 == 0 ? pcBoxTopLeft - xSizeParty + 11 : pcBoxTopLeft - xSizeParty / 2 + 5;
            int yPos4 = (int)((double)this.height * 0.45 - (double)(ySizeParty / 2));
            yPos4 += 3;
            if (x % 2 == 0) {
                yPos4 += x * 19;
            } else {
                yPos4 -= 10;
                yPos4 += x * 19;
            }
            if (x == 2) {
                --yPos4;
            }
            if (x == 4) {
                --yPos4;
            }
            if (x == 3) {
                --yPos4;
            }
            if (x == 5) {
                --yPos4;
            }
            if (pkt.selected) {
                xPos4 = mouseX - this.slotWidth / 2;
                yPos4 = mouseY - (this.slotHeight / 2 + 5);
            }
            GuiHelper.bindPokemonSprite(pkt, this.mc);
            GuiHelper.drawImageQuad(xPos4 + 2, yPos4 + 3 - (pkt.getNationalPokedexNumber() <= 649 ? 0 : -3), 30.0, 30.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
            if (!pkt.heldItem.isEmpty()) {
                this.mc.renderEngine.bindTexture(GuiResources.heldItem);
                GuiHelper.drawImageQuad(xPos4 + 18, yPos4 + 22, 8.0, 8.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
            }
            if (pkt.selected) continue;
        }
        float scale = 1.2f;
        String boxName = PCClientStorage.getBoxName(this.boxNumber);
        int textWidthHalf = (int)((float)(this.mc.fontRenderer.getStringWidth(boxName) / 2) * scale);
        GL11.glPushMatrix();
        GuiHelper.drawString(this.mc.fontRenderer, boxName, pcBoxTopLeft + xSizeBox / 2 - textWidthHalf, pcBoxTop + 12, scale, 0xFFFFFF, false);
        GL11.glPopMatrix();
    }

    protected int getPartySize() {
        return this.partyNumWidth;
    }

    protected PixelmonData getPartyPokemon(int pos) {
        return this.pcClient.getPokemonAtPos(-1, pos);
    }

    public void drawButtonContainer() {
        if (this.pixelmonMenuOpen) {
            this.mc.renderEngine.bindTexture(GuiResources.pokecheckerPopup);
            this.menuLeft = this.menuX - 73;
            this.menuTop = this.menuY - 10;
            GuiHelper.drawImageQuad(this.menuLeft, this.menuTop, 67.0, 73.0f, 0.0, 0.0, 0.26171875, 0.29f, -1.0f);
        }
    }
}

