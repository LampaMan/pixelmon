/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.pc;

import com.pixelmongenerations.client.gui.GuiWarning;
import com.pixelmongenerations.client.gui.pc.GuiPC;
import com.pixelmongenerations.core.network.PixelmonData;
import net.minecraft.util.text.translation.I18n;

public class GuiReleaseWarning
extends GuiWarning {
    private GuiPC previousScreenPC;

    GuiReleaseWarning(GuiPC previousScreen) {
        super(previousScreen);
        this.previousScreenPC = previousScreen;
    }

    @Override
    protected void confirmAction() {
        this.previousScreenPC.releasePokemon();
    }

    @Override
    protected void drawWarningText() {
        PixelmonData selectedPokemon = this.previousScreenPC.pcClient.getSelected();
        String selectedName = "";
        if (selectedPokemon != null) {
            selectedName = selectedPokemon.getNickname();
        }
        this.drawCenteredSplitText(I18n.translateToLocal("gui.pc.releasewarning").replace("%s", selectedName));
    }

    @Override
    public void closeScreen() {
        PixelmonData data = this.previousScreenPC.pcClient.getSelected();
        if (data != null) {
            data.selected = false;
            this.previousScreenPC.startClick = false;
        }
        super.closeScreen();
    }
}

