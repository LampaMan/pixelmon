/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.pc;

import com.pixelmongenerations.core.network.PixelmonData;
import java.awt.Rectangle;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

public class SlotPC {
    public PixelmonData pokemonData;
    public int x;
    public int y;
    public int swidth;

    public SlotPC(int x, int y, PixelmonData pokemon) {
        this.pokemonData = pokemon;
        this.x = x;
        this.y = y;
        this.swidth = 30;
    }

    public void bindTexture() {
        if (this.pokemonData == null) {
            return;
        }
        String pokeNum = "";
        pokeNum = this.pokemonData.getNationalPokedexNumber() < 10 ? "00" + this.pokemonData.getNationalPokedexNumber() : (this.pokemonData.getNationalPokedexNumber() < 100 ? "0" + this.pokemonData.getNationalPokedexNumber() : "" + this.pokemonData.getNationalPokedexNumber());
        Minecraft.getMinecraft().renderEngine.bindTexture(new ResourceLocation("/pixelmon/sprites/" + pokeNum));
    }

    public void clearPokemon() {
        this.pokemonData = null;
    }

    public void setPokemon(PixelmonData p) {
        this.pokemonData = p;
    }

    public void setXandY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Rectangle getBounds() {
        return new Rectangle(this.x, this.y, this.swidth, this.swidth);
    }
}

