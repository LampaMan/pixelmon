/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Keyboard
 */
package com.pixelmongenerations.client.gui.spawner;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.spawner.GuiPokemonList;
import com.pixelmongenerations.common.block.enums.EnumSpawnerAggression;
import com.pixelmongenerations.common.block.machines.PokemonRarity;
import com.pixelmongenerations.common.block.spawning.TileEntityPixelmonSpawner;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.PixelmonSpawnerData;
import com.pixelmongenerations.core.network.packetHandlers.UpdateSpawner;
import java.io.IOException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.input.Keyboard;

public class GuiPixelmonSpawner
extends GuiContainer {
    TileEntityPixelmonSpawner ps;
    int listTop;
    int listLeft;
    int listHeight;
    int listWidth;
    GuiTextField pokemonNameBox;
    GuiTextField rarityBox;
    GuiTextField formBox;
    GuiTextField textureBox;
    GuiTextField shinyBox;
    GuiTextField spawnTickBox;
    GuiTextField spawnRadiusBox;
    GuiTextField maxSpawnsBox;
    GuiTextField levelMinBox;
    GuiTextField levelMaxBox;
    GuiTextField bossRatioBox;
    GuiTextField activateRangeBox;
    GuiPokemonList list;

    public GuiPixelmonSpawner(int x, int y, int z) {
        super(new ContainerEmpty());
        Keyboard.enableRepeatEvents((boolean)true);
        this.ps = (TileEntityPixelmonSpawner)Minecraft.getMinecraft().world.getTileEntity(new BlockPos(x, y, z));
        this.listTop = 58;
        this.listLeft = 30;
        this.listHeight = 100;
        this.listWidth = 100;
    }

    @Override
    public void initGui() {
        super.initGui();
        this.list = new GuiPokemonList(this, 200, this.listHeight, this.listTop - 40, this.mc);
        this.buttonList.add(new GuiButton(0, 60, 210, 30, 20, I18n.translateToLocal("gui.trainereditor.add")));
        this.buttonList.add(new GuiButton(1, 100, 210, 30, 20, I18n.translateToLocal("gui.guiItemDrops.ok")));
        this.buttonList.add(new GuiButton(2, 260, 15, 120, 20, this.ps.fireOnTick ? I18n.translateToLocal("gui.pixelmonspawner.fireontick") : I18n.translateToLocal("gui.pixelmonspawner.fireonredstone")));
        this.buttonList.add(new GuiButton(3, 301, 203, 80, 20, this.ps.aggression.getLocalizedName()));
        this.buttonList.add(new GuiButton(4, 301, 225, 80, 20, this.ps.spawnLocation.getLocalizedName()));
        this.pokemonNameBox = new GuiTextField(5, this.mc.fontRenderer, 34, this.listTop + (int)((double)this.listHeight / 1.45) + 11, 80, 20);
        this.pokemonNameBox.setText("");
        this.rarityBox = new GuiTextField(6, this.mc.fontRenderer, this.listLeft + 90, this.listTop + (int)((double)this.listHeight / 1.45) + 11, 40, 20);
        this.rarityBox.setText("");
        this.textureBox = new GuiTextField(6, this.mc.fontRenderer, 76, this.listTop + this.listHeight + 20, 40, 20);
        this.textureBox.setText("0");
        this.formBox = new GuiTextField(13, this.mc.fontRenderer, 122, this.listTop + this.listHeight + 20, 40, 20);
        this.formBox.setText("-1");
        this.shinyBox = new GuiTextField(14, this.mc.fontRenderer, 30, this.listTop + this.listHeight + 20, 40, 20);
        this.shinyBox.setText("4096");
        this.spawnTickBox = new GuiTextField(7, this.mc.fontRenderer, 340, this.listTop - 19, 40, 20);
        this.spawnTickBox.setText("" + this.ps.spawnTick);
        this.spawnRadiusBox = new GuiTextField(8, this.mc.fontRenderer, 340, this.listTop + 4, 40, 20);
        this.spawnRadiusBox.setText("" + this.ps.spawnRadius);
        this.maxSpawnsBox = new GuiTextField(9, this.mc.fontRenderer, 340, this.listTop + 27, 40, 20);
        this.maxSpawnsBox.setText("" + this.ps.maxSpawns);
        this.levelMinBox = new GuiTextField(10, this.mc.fontRenderer, 340, this.listTop + 50, 40, 20);
        this.levelMinBox.setText("" + this.ps.levelMin);
        this.levelMaxBox = new GuiTextField(11, this.mc.fontRenderer, 340, this.listTop + 73, 40, 20);
        this.levelMaxBox.setText("" + this.ps.levelMax);
        this.bossRatioBox = new GuiTextField(12, this.mc.fontRenderer, 340, this.listTop + 96, 40, 20);
        this.bossRatioBox.setText("" + this.ps.bossRatio);
        this.activateRangeBox = new GuiTextField(8, this.mc.fontRenderer, 340, this.listTop + 119, 40, 20);
        this.activateRangeBox.setText("" + this.ps.activationRange);
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        this.pokemonNameBox.textboxKeyTyped(typedChar, keyCode);
        this.rarityBox.textboxKeyTyped(typedChar, keyCode);
        this.formBox.textboxKeyTyped(typedChar, keyCode);
        this.spawnTickBox.textboxKeyTyped(typedChar, keyCode);
        this.spawnRadiusBox.textboxKeyTyped(typedChar, keyCode);
        this.maxSpawnsBox.textboxKeyTyped(typedChar, keyCode);
        this.levelMinBox.textboxKeyTyped(typedChar, keyCode);
        this.levelMaxBox.textboxKeyTyped(typedChar, keyCode);
        this.bossRatioBox.textboxKeyTyped(typedChar, keyCode);
        this.textureBox.textboxKeyTyped(typedChar, keyCode);
        this.shinyBox.textboxKeyTyped(typedChar, keyCode);
        this.activateRangeBox.textboxKeyTyped(typedChar, keyCode);
        GuiHelper.switchFocus(keyCode, this.spawnTickBox, this.spawnRadiusBox, this.maxSpawnsBox, this.levelMinBox, this.levelMaxBox, this.bossRatioBox, this.pokemonNameBox, this.rarityBox, this.formBox);
        if (keyCode == 1 || keyCode == 28) {
            this.saveFields();
        }
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        this.pokemonNameBox.mouseClicked(mouseX, mouseY, mouseButton);
        this.rarityBox.mouseClicked(mouseX, mouseY, mouseButton);
        this.formBox.mouseClicked(mouseX, mouseY, mouseButton);
        this.spawnTickBox.mouseClicked(mouseX, mouseY, mouseButton);
        this.spawnRadiusBox.mouseClicked(mouseX, mouseY, mouseButton);
        this.maxSpawnsBox.mouseClicked(mouseX, mouseY, mouseButton);
        this.levelMinBox.mouseClicked(mouseX, mouseY, mouseButton);
        this.levelMaxBox.mouseClicked(mouseX, mouseY, mouseButton);
        this.bossRatioBox.mouseClicked(mouseX, mouseY, mouseButton);
        this.textureBox.mouseClicked(mouseX, mouseY, mouseButton);
        this.shinyBox.mouseClicked(mouseX, mouseY, mouseButton);
        this.activateRangeBox.mouseClicked(mouseX, mouseY, mouseButton);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float mfloat, int mouseX, int mouseY) {
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.mc.renderEngine.bindTexture(GuiResources.spawnerBackground);
        GuiHelper.drawImageQuad(0.0, 0.0, this.width, this.height, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        RenderHelper.disableStandardItemLighting();
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.pixelmonspawner.pcms"), 250, 5, 13094);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.pixelmonspawner.pokemonlist"), 50, 5, 13094);
        this.list.drawScreen(mouseX, mouseY, mfloat);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.pixelmonspawner.name"), 39, this.listTop + (int)((double)this.listHeight / 1.45), 13094);
        this.pokemonNameBox.drawTextBox();
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.pixelmonspawner.rarity"), this.listLeft + 95, this.listTop + (int)((double)this.listHeight / 1.45), 13094);
        this.rarityBox.drawTextBox();
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.pixelmonspawner.form"), this.listLeft + 100, this.listTop + this.listHeight + 9, 13094);
        this.formBox.drawTextBox();
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.pixelmonspawner.texture"), this.listLeft + 46, this.listTop + this.listHeight + 9, 13094);
        this.textureBox.drawTextBox();
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.pixelmonspawner.shinyChance"), this.listLeft + 8, this.listTop + this.listHeight + 9, 13094);
        this.shinyBox.drawTextBox();
        this.renderSpawnInfoText("gui.pixelmonspawner.spawntick", this.spawnTickBox);
        this.renderSpawnInfoText("gui.pixelmonspawner.radius", this.spawnRadiusBox);
        this.renderSpawnInfoText("gui.pixelmonspawner.maxspawns", this.maxSpawnsBox);
        this.renderSpawnInfoText("gui.pixelmonspawner.minlevel", this.levelMinBox);
        this.renderSpawnInfoText("gui.pixelmonspawner.maxlevel", this.levelMaxBox);
        this.renderSpawnInfoText("gui.pixelmonspawner.bossratio", this.bossRatioBox);
        this.renderSpawnInfoText("gui.pixelmonspawner.activaterange", this.activateRangeBox);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.pixelmonspawner.aggression"), 235, this.listTop + 151, 13094);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.pixelmonspawner.spawnlocation"), 216, this.listTop + 173, 13094);
    }

    public void renderSpawnInfoText(String text, GuiTextField field) {
        GuiHelper.drawStringRightAligned(I18n.translateToLocal(text), field.x - 5, field.y + 6, 13094);
        field.drawTextBox();
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        if (button.enabled) {
            switch (button.id) {
                case 0: {
                    int shinyChance;
                    int texture;
                    int form;
                    int rarity;
                    EnumSpecies pokemon = EnumSpecies.getFromNameAnyCase(this.pokemonNameBox.getText());
                    try {
                        rarity = Integer.parseInt(this.rarityBox.getText());
                        form = Integer.parseInt(this.formBox.getText());
                        texture = Integer.parseInt(this.textureBox.getText());
                        shinyChance = Integer.parseInt(this.shinyBox.getText());
                    }
                    catch (NumberFormatException e) {
                        return;
                    }
                    if (pokemon == null || rarity <= 0) break;
                    this.ps.pokemonList.add(new PokemonRarity(pokemon, rarity, form, texture, shinyChance));
                    break;
                }
                case 1: {
                    this.saveFields();
                    break;
                }
                case 2: {
                    this.ps.fireOnTick = !this.ps.fireOnTick;
                    boolean bl = this.ps.fireOnTick;
                    button.displayString = this.ps.fireOnTick ? I18n.translateToLocal("gui.pixelmonspawner.fireontick") : I18n.translateToLocal("gui.pixelmonspawner.fireonredstone");
                    break;
                }
                case 3: {
                    this.ps.aggression = EnumSpawnerAggression.nextAggression(this.ps.aggression);
                    button.displayString = this.ps.aggression.getLocalizedName();
                    break;
                }
                case 4: {
                    this.ps.spawnLocation = SpawnLocation.nextLocation(this.ps.spawnLocation);
                    button.displayString = this.ps.spawnLocation.getLocalizedName();
                }
            }
        }
    }

    private void saveFields() {
        if (this.checkFields()) {
            GuiHelper.closeScreen();
        }
    }

    private boolean checkFields() {
        try {
            int spawnTicks = Integer.parseInt(this.spawnTickBox.getText());
            int spawnRadius = Integer.parseInt(this.spawnRadiusBox.getText());
            int maxSpawns = Integer.parseInt(this.maxSpawnsBox.getText());
            int levelMin = Integer.parseInt(this.levelMinBox.getText());
            int levelMax = Integer.parseInt(this.levelMaxBox.getText());
            int bossRatio = Integer.parseInt(this.bossRatioBox.getText());
            int activationRange = Integer.parseInt(this.activateRangeBox.getText());
            if (spawnTicks > 0 && spawnTicks < 1000 && spawnRadius > 0 && spawnRadius <= 100 && maxSpawns > 0 && maxSpawns < 50 && levelMin > 0 && levelMin <= PixelmonServerConfig.maxLevel && levelMax >= levelMin && levelMax <= PixelmonServerConfig.maxLevel) {
                this.ps.spawnTick = spawnTicks;
                this.ps.spawnRadius = spawnRadius;
                this.ps.maxSpawns = maxSpawns;
                this.ps.levelMin = levelMin;
                this.ps.levelMax = levelMax;
                this.ps.bossRatio = bossRatio;
                this.ps.activationRange = activationRange;
                NBTTagCompound nbt = new NBTTagCompound();
                this.ps.writeToNBT(nbt);
                PixelmonSpawnerData p = new PixelmonSpawnerData(this.ps.getPos(), nbt);
                Pixelmon.NETWORK.sendToServer(new UpdateSpawner(p));
                return true;
            }
        }
        catch (Exception exception) {
            // empty catch block
        }
        return false;
    }

    public PokemonRarity getPokemonListEntry(int ind) {
        if (ind < this.ps.pokemonList.size() && ind >= 0) {
            return this.ps.pokemonList.get(ind);
        }
        return null;
    }

    public int getPokemonListCount() {
        return this.ps.pokemonList.size();
    }

    public void removeFromList(int var1) {
        this.ps.pokemonList.remove(var1);
    }

    public static Minecraft getMinecraft(GuiPixelmonSpawner gui) {
        return gui.mc;
    }
}

