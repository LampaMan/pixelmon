/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.pokechecker;

import com.pixelmongenerations.client.gui.GuiWarning;
import com.pixelmongenerations.client.gui.pokechecker.GuiScreenPokeCheckerMoves;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.DeleteMove;
import com.pixelmongenerations.core.util.RegexPatterns;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.text.translation.I18n;

class GuiScreenPokeCheckerWarningDeleteMove extends GuiWarning {

    private PixelmonData targetPacket;
    private int moveToDelete;
    private Attack attack;

    GuiScreenPokeCheckerWarningDeleteMove(GuiContainer previousScreen, PixelmonData targetPacket, int moveToDelete) {
        super(previousScreen);
        this.targetPacket = targetPacket;
        this.moveToDelete = moveToDelete;
    }

    @Override
    protected void confirmAction() {
        Pixelmon.NETWORK.sendToServer(new DeleteMove(this.targetPacket.pokemonID, this.moveToDelete));
        if (this.previousScreen instanceof GuiScreenPokeCheckerMoves) {
            GuiScreenPokeCheckerMoves moveScreen = (GuiScreenPokeCheckerMoves)this.previousScreen;
            moveScreen.reloadMoves();
        }
    }

    @Override
    protected void drawWarningText() {
        this.drawCenteredString(this.mc.fontRenderer, I18n.translateToLocal("gui.screenpokechecker.forget1"), 60, 68, 0xFFFFFF);
        this.drawCenteredString(this.mc.fontRenderer, RegexPatterns.$_P_VAR.matcher(I18n.translateToLocal("gui.screenpokechecker.forget2")).replaceAll(this.targetPacket.getEscapedNickname()), 60, 78, 0xFFFFFF);
        if (this.attack == null) {
            this.attack = DatabaseMoves.getAttack(this.targetPacket.moveset[this.moveToDelete].attackIndex);
        }
        this.drawCenteredString(this.mc.fontRenderer, this.attack.getAttackBase().getLocalizedName() + "?", 60, 88, 0xFFFFFF);
    }
}

