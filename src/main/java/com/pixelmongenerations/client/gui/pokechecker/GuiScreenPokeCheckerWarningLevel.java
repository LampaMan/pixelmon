/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.pokechecker;

import com.pixelmongenerations.client.gui.GuiWarning;
import com.pixelmongenerations.client.gui.pokechecker.GuiScreenPokeChecker;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.StopStartLevelling;
import com.pixelmongenerations.core.util.RegexPatterns;
import net.minecraft.util.text.translation.I18n;

public class GuiScreenPokeCheckerWarningLevel
extends GuiWarning {
    private PixelmonData targetPacket;

    public GuiScreenPokeCheckerWarningLevel(GuiScreenPokeChecker previousScreen, PixelmonData targetPacket) {
        super(previousScreen);
        this.targetPacket = targetPacket;
    }

    @Override
    protected void confirmAction() {
        Pixelmon.NETWORK.sendToServer(new StopStartLevelling(this.targetPacket.pokemonID));
    }

    @Override
    protected void drawWarningText() {
        String langString = "gui.screenpokechecker." + (this.targetPacket.doesLevel ? "disable" : "enable");
        String text = I18n.translateToLocal(langString);
        this.drawCenteredString(this.mc.fontRenderer, I18n.translateToLocal("gui.screenpokechecker.forget1"), 60, 73, 0xFFFFFF);
        this.drawCenteredString(this.mc.fontRenderer, RegexPatterns.$_A_VAR.matcher(I18n.translateToLocal("gui.screenpokechecker.move")).replaceAll(text), 60, 83, 0xFFFFFF);
    }
}

