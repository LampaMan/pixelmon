/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.gui.pokechecker;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.pc.GuiPC;
import com.pixelmongenerations.client.gui.pokechecker.GuiPokeCheckerTabs;
import com.pixelmongenerations.client.gui.pokechecker.GuiRenamePokemon;
import com.pixelmongenerations.client.gui.pokechecker.GuiScreenPokeChecker;
import com.pixelmongenerations.client.gui.pokechecker.GuiScreenPokeCheckerStats;
import com.pixelmongenerations.client.gui.pokechecker.GuiScreenPokeCheckerWarningDeleteMove;
import com.pixelmongenerations.client.gui.pokechecker.GuiScreenPokeCheckerWarningLevel;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.PixelmonMovesetData;
import com.pixelmongenerations.core.network.packetHandlers.SwapMove;
import java.io.IOException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.opengl.GL11;

public class GuiScreenPokeCheckerMoves
extends GuiScreenPokeChecker {
    GuiButton nameButton;
    boolean renameButton;
    PixelmonData firstDrawData = null;
    static int selectedNumber = -1;
    static int attackClicked = -1;
    static boolean move1 = true;
    static boolean move2 = false;
    static boolean move3 = false;
    static boolean move4 = false;
    static boolean isPC;
    int box;
    private Attack[] attacks = new Attack[4];

    public GuiScreenPokeCheckerMoves(PixelmonData PixelmonData2, boolean b) {
        super(PixelmonData2, b);
        this.targetPacket = PixelmonData2;
        isPC = b;
    }

    public GuiScreenPokeCheckerMoves(PixelmonData selected, boolean b, int box) {
        this(selected, b);
        this.box = box;
    }

    @Override
    public boolean doesGuiPauseGame() {
        return true;
    }

    @Override
    public void initGui() {
        super.initGui();
        this.buttonList.clear();
        this.buttonList.add(new GuiPokeCheckerTabs(3, 0, this.width / 2 + 93, this.height / 2 + 79, 33, 16, ""));
        this.buttonList.add(new GuiPokeCheckerTabs(0, 1, this.width / 2 - 126, this.height / 2 + 79, 92, 16, I18n.translateToLocal("gui.screenpokechecker.summary")));
        this.buttonList.add(new GuiPokeCheckerTabs(2, 2, this.width / 2 + 30, this.height / 2 + 79, 62, 16, I18n.translateToLocal("gui.screenpokechecker.stats")));
        this.buttonList.add(new GuiPokeCheckerTabs(4, 4, this.width / 2 - 43, this.height / 2 - 91, 9, 9, "", this.targetPacket));
        this.buttonList.add(new GuiPokeCheckerTabs(7, 5, this.width / 2 - 43, this.height / 2 - 14, 9, 8, "", this.targetPacket));
    }

    @Override
    public void actionPerformed(GuiButton button) {
        switch (button.id) {
            case 0: {
                if (!isPC) {
                    this.mc.player.closeScreen();
                    break;
                }
                GuiPC gui = new GuiPC(this.targetPacket, this.box);
                this.mc.displayGuiScreen(gui);
                break;
            }
            case 1: {
                this.mc.displayGuiScreen(new GuiScreenPokeChecker(this.targetPacket, isPC, this.box));
                break;
            }
            case 2: {
                this.mc.displayGuiScreen(new GuiScreenPokeCheckerStats(this.targetPacket, isPC, this.box));
                break;
            }
            case 3: {
                if (!PixelmonConfig.allowNicknames || this.targetPacket.isEgg) break;
                this.mc.displayGuiScreen(new GuiRenamePokemon(this.targetPacket, this));
                break;
            }
            case 4: {
                this.mc.displayGuiScreen(new GuiScreenPokeCheckerWarningLevel(this, this.targetPacket));
                break;
            }
            case 5: {
                if (!PixelmonConfig.allowNicknames || this.targetPacket.isEgg) break;
                this.mc.displayGuiScreen(new GuiRenamePokemon(this.targetPacket, this));
            }
        }
    }

    public void resetAll() {
        move1 = false;
        move2 = false;
        move3 = false;
        move4 = false;
    }

    void reloadMoves() {
    }

    @Override
    public void drawGuiContainerForegroundLayer(int i, int i1) {
        GL11.glNormal3f((float)0.0f, (float)-1.0f, (float)0.0f);
        if (!this.targetPacket.isEgg) {
            this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.screenpokechecker.lvl") + " " + this.targetPacket.lvl, 58, -19, 0xFFFFFF);
            String dexText = I18n.translateToLocal("gui.screenpokechecker.number") + " " + String.valueOf(this.targetPacket.getNationalPokedexNumber());
            this.drawString(this.mc.fontRenderer, dexText, 58, -4, 0xFFFFFF);
            this.drawString(this.mc.fontRenderer, this.targetPacket.getSpecies().getPokemonName(), 58 + this.mc.fontRenderer.getStringWidth(dexText) + 20, -4, 0xFFFFFF);
            if (this.targetPacket.pokeball != null) {
                this.itemRender.renderItemIntoGUI(new ItemStack(this.targetPacket.pokeball.getItem()), -39, -24);
            }
            for (int i2 = 0; i2 < this.targetPacket.numMoves; ++i2) {
                this.attacks[i2] = DatabaseMoves.getAttack(this.targetPacket.moveset[i2].attackIndex);
                if (this.attacks[i2] == null) continue;
                this.drawCenteredStringWithoutShadow(this.attacks[i2].getAttackBase().getLocalizedName(), 135, 16 + i2 * 22, 5461844);
                this.drawCenteredStringWithoutShadow(this.targetPacket.moveset[i2].pp + "/" + this.targetPacket.moveset[i2].ppBase, 198, 16 + i2 * 22, 0x595959);
                this.mc.renderEngine.bindTexture(GuiResources.types);
                EnumType type = this.attacks[i2].getAttackBase().attackType;
                String move = this.attacks[i2].baseAttack.getUnlocalizedName();
                if (move.equals("Judgment") || move.equals("Multi-Attack")) {
                    type = this.targetPacket.getTypeFromItem();
                }
                float x = type.textureX;
                float y = type.textureY;
                GL11.glColor3f((float)1.0f, (float)1.0f, (float)1.0f);
                GuiHelper.drawImageQuad(63.0, (double)(22 * i2) + 7.5, 21.0, 21.0f, x / 495.0f, y / 392.0f, (x + 98.0f) / 495.0f, (y + 96.0f) / 392.0f, this.zLevel);
            }
        } else {
            this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.screenpokechecker.lvl") + " ???", 58, -19, 0xFFFFFF);
            this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.screenpokechecker.number") + " ???", 58, -4, 0xFFFFFF);
            this.drawCenteredString(this.mc.fontRenderer, "???", 140, -4, 0xDDDDDD);
            this.mc.fontRenderer.drawString("???", -40 + (46 - this.mc.fontRenderer.getStringWidth("???") / 2), 132, 0x595959);
        }
        this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.battle.description"), 107, 98, 0xFFFFFF);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.screenpokechecker.moves"), 71, 166, 0xFFFFFF);
        this.drawSelection(i, i1);
        this.drawSelectedRectBin(i, i1);
        this.drawMoveDescription();
        this.drawBasePokemonInfo();
    }

    public void drawMoveDescription() {
        if (!this.targetPacket.isEgg) {
            if (move1 && this.targetPacket.numMoves > 0) {
                this.drawMoveInfo(this.attacks[0]);
            }
            if (move2 && this.targetPacket.numMoves > 1) {
                this.drawMoveInfo(this.attacks[1]);
            }
            if (move3 && this.targetPacket.numMoves > 2) {
                this.drawMoveInfo(this.attacks[2]);
            }
            if (move4 && this.targetPacket.numMoves > 3) {
                this.drawMoveInfo(this.attacks[3]);
            }
        }
    }

    public void switchMoves(int moveToChange2) {
        Pixelmon.NETWORK.sendToServer(new SwapMove(this.targetPacket.pokemonID, selectedNumber, moveToChange2));
        Attack[] switched = new Attack[]{this.attacks[selectedNumber], this.attacks[moveToChange2]};
        this.attacks[GuiScreenPokeCheckerMoves.selectedNumber] = switched[1];
        this.attacks[moveToChange2] = switched[0];
        PixelmonMovesetData[] switched2 = new PixelmonMovesetData[]{this.targetPacket.moveset[selectedNumber], this.targetPacket.moveset[moveToChange2]};
        this.targetPacket.moveset[GuiScreenPokeCheckerMoves.selectedNumber] = switched2[1];
        this.targetPacket.moveset[moveToChange2] = switched2[0];
        selectedNumber = -1;
    }

    private void drawMoveInfo(Attack attack) {
        if (attack == null) {
            return;
        }
        String powerText = I18n.translateToLocal("gui.battle.power");
        this.mc.fontRenderer.drawString(powerText, -40 + (46 - this.mc.fontRenderer.getStringWidth(powerText) / 2), 84, 0x353535);
        String accuracyText = I18n.translateToLocal("gui.battle.accuracy");
        this.mc.fontRenderer.drawString(accuracyText, -40 + (46 - this.mc.fontRenderer.getStringWidth(accuracyText) / 2), 116, 0x353535);
        int bpExtra = 0;
        int acExtra = 0;
        if (attack.getAttackBase().basePower >= 100) {
            bpExtra = this.mc.fontRenderer.getCharWidth('0');
        }
        if (attack.getAttackBase().accuracy >= 100) {
            acExtra = this.mc.fontRenderer.getCharWidth('0');
        }
        if (attack.getAttackBase().basePower > 0) {
            this.mc.fontRenderer.drawString("" + attack.getAttackBase().basePower, -40 + (46 - this.mc.fontRenderer.getStringWidth("" + attack.getAttackBase().basePower) / 2), 100, 0x595959);
        } else {
            this.mc.fontRenderer.drawString("--", -40 + (46 - this.mc.fontRenderer.getStringWidth("--") / 2), 100, 0x595959);
        }
        if (attack.getAttackBase().accuracy <= 0) {
            this.mc.fontRenderer.drawString("--", -40 + (46 - this.mc.fontRenderer.getStringWidth("--") / 2), 132, 0x595959);
        } else {
            this.mc.fontRenderer.drawString("" + attack.getAttackBase().accuracy, -40 + (46 - this.mc.fontRenderer.getStringWidth("" + attack.getAttackBase().accuracy) / 2), 132, 0x595959);
        }
        String categoryText = attack.getAttackCategory().getLocalizedName();
        int textColor = attack.getAttackCategory() == AttackCategory.Physical ? 0xFF4400 : (attack.getAttackCategory() == AttackCategory.Special ? 0x2266CC : 0x999999);
        this.mc.fontRenderer.drawString(categoryText, -40 + (46 - this.mc.fontRenderer.getStringWidth(categoryText) / 2), 148, textColor);
        this.mc.fontRenderer.drawSplitString(attack.getAttackBase().getLocalizedDescription(), 60, 112, 145, 0x595959);
    }

    public void drawSelection(int i, int i1) {
        this.mc.renderEngine.bindTexture(GuiResources.summaryMoves);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        if (this.targetPacket.numMoves > 0 && i > this.width / 2 - 31 && i < this.width / 2 + 123 && i1 > this.height / 2 - 80 && i1 < this.height / 2 - 56 || move1) {
            this.drawTexturedModalRect(55, 6, 1, 231, 160, 23);
            this.resetAll();
            move1 = true;
        }
        if (this.targetPacket.numMoves > 1 && i > this.width / 2 - 31 && i < this.width / 2 + 123 && i1 > this.height / 2 - 55 && i1 < this.height / 2 - 35 || move2) {
            this.drawTexturedModalRect(55, 28, 1, 231, 160, 23);
            this.resetAll();
            move2 = true;
        }
        if (this.targetPacket.numMoves > 2 && i > this.width / 2 - 31 && i < this.width / 2 + 123 && i1 > this.height / 2 - 34 && i1 < this.height / 2 - 14 || move3) {
            this.drawTexturedModalRect(55, 50, 1, 231, 160, 23);
            this.resetAll();
            move3 = true;
        }
        if (this.targetPacket.numMoves > 3 && i > this.width / 2 - 31 && i < this.width / 2 + 123 && i1 > this.height / 2 - 13 && i1 < this.height / 2 - -11 || move4) {
            this.drawTexturedModalRect(55, 72, 1, 231, 160, 23);
            this.resetAll();
            move4 = true;
        }
        this.drawSelectedRect();
    }

    protected void drawSelectedRect() {
        this.mc.renderEngine.bindTexture(GuiResources.summaryMoves);
        GlStateManager.color(0.0f, 1.0f, 0.0f);
        if (selectedNumber == 0) {
            this.drawTexturedModalRect(55, 6, 1, 231, 160, 23);
        } else if (selectedNumber == 1) {
            this.drawTexturedModalRect(55, 28, 1, 231, 160, 23);
        } else if (selectedNumber == 2) {
            this.drawTexturedModalRect(55, 50, 1, 231, 160, 23);
        } else if (selectedNumber == 3) {
            this.drawTexturedModalRect(55, 72, 1, 231, 160, 23);
        }
        GlStateManager.color(1.0f, 1.0f, 1.0f);
    }

    protected void drawSelectedRectBin(int i, int i1) {
        this.mc.renderEngine.bindTexture(GuiResources.summaryMoves);
        GlStateManager.color(1.0f, 0.0f, 0.0f);
        if (selectedNumber >= this.targetPacket.numMoves && i > this.width / 2 + 130 && i < this.width / 2 + 158 && i1 > this.height / 2 - 25 && i1 < this.height / 2 + 9) {
            this.drawTexturedModalRect(220, 60, 230, 225, 26, 31);
        }
        GlStateManager.color(1.0f, 1.0f, 1.0f);
    }

    protected int moveClicked(int i, int i1) {
        if (this.targetPacket.isEgg) {
            return -1;
        }
        if (this.targetPacket.numMoves > 0 && i > this.width / 2 - 31 && i < this.width / 2 + 123 && i1 > this.height / 2 - 80 && i1 < this.height / 2 - 56) {
            return 0;
        }
        if (this.targetPacket.numMoves > 1 && i > this.width / 2 - 31 && i < this.width / 2 + 123 && i1 > this.height / 2 - 55 && i1 < this.height / 2 - 35) {
            return 1;
        }
        if (this.targetPacket.numMoves > 2 && i > this.width / 2 - 31 && i < this.width / 2 + 123 && i1 > this.height / 2 - 34 && i1 < this.height / 2 - 14) {
            return 2;
        }
        if (this.targetPacket.numMoves > 3 && i > this.width / 2 - 31 && i < this.width / 2 + 123 && i1 > this.height / 2 - 13 && i1 < this.height / 2 - -11) {
            return 3;
        }
        return -1;
    }

    protected void attackClicked(int i, int i1) {
        if (this.targetPacket.numMoves > 0 && i > this.width / 2 - 31 && i < this.width / 2 + 123 && i1 > this.height / 2 - 80 && i1 < this.height / 2 - 56) {
            attackClicked = this.attacks[0].getAttackBase().attackIndex;
        } else if (this.targetPacket.numMoves > 1 && i > this.width / 2 - 31 && i < this.width / 2 + 123 && i1 > this.height / 2 - 55 && i1 < this.height / 2 - 35) {
            attackClicked = this.attacks[1].getAttackBase().attackIndex;
        } else if (this.targetPacket.numMoves > 2 && i > this.width / 2 - 31 && i < this.width / 2 + 123 && i1 > this.height / 2 - 34 && i1 < this.height / 2 - 14) {
            attackClicked = this.attacks[2].getAttackBase().attackIndex;
        } else if (this.targetPacket.numMoves > 3 && i > this.width / 2 - 31 && i < this.width / 2 + 123 && i1 > this.height / 2 - 13 && i1 < this.height / 2 - -11) {
            attackClicked = this.attacks[3].getAttackBase().attackIndex;
        }
    }

    protected void selectMove(int i, int i1) {
        if (selectedNumber != this.moveClicked(i, i1)) {
            if (selectedNumber == -1) {
                selectedNumber = this.moveClicked(i, i1);
            } else if (selectedNumber != -1 && this.moveClicked(i, i1) != -1) {
                this.switchMoves(this.moveClicked(i, i1));
            }
        } else {
            selectedNumber = -1;
        }
    }

    @Override
    protected void mouseClicked(int x, int y, int par3) throws IOException {
        ScaledResolution var5 = new ScaledResolution(Minecraft.getMinecraft());
        int var6 = var5.getScaledWidth();
        int var7 = var5.getScaledHeight();
        super.mouseClicked(x, y, par3);
        if (this.canDeleteMove() && !isPC && selectedNumber >= 0 && x > var6 / 2 + 130 && x < var6 / 2 + 158 && y > var7 / 2 - 25 && y < var7 / 2 + 9) {
            this.mc.displayGuiScreen(new GuiScreenPokeCheckerWarningDeleteMove(this, this.targetPacket, selectedNumber));
        }
        if (!isPC && !this.targetPacket.isEgg) {
            this.attackClicked(x, y);
            this.selectMove(x, y);
        }
        if (x > var6 / 2 - 125 && x < var6 / 2 - 40 && y > var7 / 2 - 15 && y < var7 / 2 + 5) {
            if (par3 == 1 && !this.renameButton) {
                this.nameButton = new GuiButton(3, x, y, 50, 20, I18n.translateToLocal("gui.screenpokechecker.rename"));
                this.buttonList.add(this.nameButton);
                this.renameButton = true;
            } else if (par3 != 1 && this.renameButton) {
                this.buttonList.remove(this.nameButton);
                this.renameButton = false;
            } else if (par3 == 1 && this.renameButton) {
                this.buttonList.remove(this.nameButton);
                this.nameButton = new GuiButton(3, x, y, 50, 20, I18n.translateToLocal("gui.screenpokechecker.rename"));
                this.buttonList.add(this.nameButton);
            }
        }
    }

    private boolean canDeleteMove() {
        int count = 0;
        for (int i = 0; i < this.targetPacket.moveset.length; ++i) {
            if (this.targetPacket.moveset[i] == null) continue;
            ++count;
        }
        return count > 1;
    }

    @Override
    public void drawGuiContainerBackgroundLayer(float f, int i, int i1) {
        ScaledResolution var5 = new ScaledResolution(Minecraft.getMinecraft());
        var5.getScaledWidth();
        var5.getScaledHeight();
        this.mc.renderEngine.bindTexture(GuiResources.summaryMoves);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.drawTexturedModalRect((this.width - this.xSize) / 2 - 40, (this.height - this.ySize) / 2 - 25, 0, 0, 256, 205);
        if (!isPC && selectedNumber >= 0 && this.canDeleteMove()) {
            this.drawTexturedModalRect((this.width - this.xSize) / 2 + 220, (this.height - this.ySize) / 2 + 60, 203, 225, 26, 31);
        }
        this.mc.renderEngine.bindTexture(GuiResources.summarySummary);
        this.drawTexturedModalRect(this.width / 2 - 33, this.height / 2 + 79, 95, 205, 62, 16);
        float xpBarFill = this.targetPacket.getExpFraction() * 10.0f;
        this.drawTexturedModalRect(this.width / 2 + 25, this.height / 2 - 105, 162, 222, 49, 12);
        if (xpBarFill >= 1.0f) {
            this.drawTexturedModalRect(this.width / 2 + 25, this.height / 2 - 105, 162, 237, (int)xpBarFill * 5, 12);
        }
        this.drawPokemonName();
        this.drawArrows(i, i1);
    }

    @Override
    public void drawCenteredStringWithoutShadow(String par2Str, int par3, int par4, int par5) {
        this.mc.fontRenderer.drawString(par2Str, par3 - this.mc.fontRenderer.getStringWidth(par2Str) / 2, par4, par5);
    }
}

