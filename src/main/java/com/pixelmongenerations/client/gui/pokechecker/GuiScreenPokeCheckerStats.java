/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.gui.pokechecker;

import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.pc.GuiPC;
import com.pixelmongenerations.client.gui.pokechecker.GuiPokeCheckerTabs;
import com.pixelmongenerations.client.gui.pokechecker.GuiRenamePokemon;
import com.pixelmongenerations.client.gui.pokechecker.GuiScreenPokeChecker;
import com.pixelmongenerations.client.gui.pokechecker.GuiScreenPokeCheckerMoves;
import com.pixelmongenerations.client.gui.pokechecker.GuiScreenPokeCheckerWarningLevel;
import com.pixelmongenerations.common.entity.pixelmon.textures.IEnumSpecialTexture;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.network.PixelmonData;
import java.io.IOException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.opengl.GL11;

public class GuiScreenPokeCheckerStats extends GuiScreenPokeChecker {

    GuiButton nameButton;
    boolean renameButton;
    boolean isPC;
    int box;
    int hexWhite = 0xFFFFFF;
    int hexDecrease = 0xFF3030;
    int hexIncrease = 65280;

    public GuiScreenPokeCheckerStats(PixelmonData PixelmonData2, boolean b) {
        super(PixelmonData2, b);
        this.targetPacket = PixelmonData2;
        this.isPC = b;
    }

    public GuiScreenPokeCheckerStats(PixelmonData selected, boolean b, int box) {
        this(selected, b);
        this.box = box;
    }

    @Override
    public boolean doesGuiPauseGame() {
        return true;
    }

    @Override
    public void initGui() {
        super.initGui();
        this.buttonList.clear();
        this.buttonList.add(new GuiPokeCheckerTabs(3, 0, this.width / 2 + 93, this.height / 2 + 79, 33, 16, ""));
        this.buttonList.add(new GuiPokeCheckerTabs(0, 1, this.width / 2 - 126, this.height / 2 + 79, 92, 16, I18n.translateToLocal("gui.screenpokechecker.summary")));
        this.buttonList.add(new GuiPokeCheckerTabs(1, 2, this.width / 2 - 33, this.height / 2 + 79, 62, 16, I18n.translateToLocal("gui.screenpokechecker.moves")));
        this.buttonList.add(new GuiPokeCheckerTabs(4, 4, this.width / 2 - 43, this.height / 2 - 91, 9, 9, "", this.targetPacket));
        this.buttonList.add(new GuiPokeCheckerTabs(7, 5, this.width / 2 - 43, this.height / 2 - 14, 9, 8, "", this.targetPacket));
    }

    @Override
    public void actionPerformed(GuiButton button) {
        switch (button.id) {
            case 0: {
                if (!this.isPC) {
                    this.mc.player.closeScreen();
                    break;
                }
                GuiPC gui = new GuiPC(this.targetPacket, this.box);
                this.mc.displayGuiScreen(gui);
                break;
            }
            case 1: {
                this.mc.displayGuiScreen(new GuiScreenPokeChecker(this.targetPacket, this.isPC, this.box));
                break;
            }
            case 2: {
                this.mc.displayGuiScreen(new GuiScreenPokeCheckerMoves(this.targetPacket, this.isPC, this.box));
                break;
            }
            case 3: {
                if (!PixelmonConfig.allowNicknames || this.targetPacket.isEgg) break;
                this.mc.displayGuiScreen(new GuiRenamePokemon(this.targetPacket, this));
                break;
            }
            case 4: {
                this.mc.displayGuiScreen(new GuiScreenPokeCheckerWarningLevel(this, this.targetPacket));
                break;
            }
            case 5: {
                if (!PixelmonConfig.allowNicknames || this.targetPacket.isEgg) break;
                this.mc.displayGuiScreen(new GuiRenamePokemon(this.targetPacket, this));
            }
        }
    }

    @Override
    protected void mouseClicked(int x, int y, int par3) throws IOException {
        ScaledResolution var5 = new ScaledResolution(Minecraft.getMinecraft());
        int var6 = var5.getScaledWidth();
        int var7 = var5.getScaledHeight();
        super.mouseClicked(x, y, par3);
        if (x > var6 / 2 - 125 && x < var6 / 2 - 40 && y > var7 / 2 - 15 && y < var7 / 2 + 5) {
            if (par3 == 1 && !this.renameButton) {
                this.nameButton = new GuiButton(3, x, y, 50, 20, I18n.translateToLocal("gui.screenpokechecker.rename"));
                this.buttonList.add(this.nameButton);
                this.renameButton = true;
            } else if (par3 != 1 && this.renameButton) {
                this.buttonList.remove(this.nameButton);
                this.renameButton = false;
            } else if (par3 == 1 && this.renameButton) {
                this.buttonList.remove(this.nameButton);
                this.nameButton = new GuiButton(3, x, y, 50, 20, I18n.translateToLocal("gui.screenpokechecker.rename"));
                this.buttonList.add(this.nameButton);
            }
        }
    }

    @Override
    public void drawGuiContainerForegroundLayer(int par1, int par2) {
        int hexColor = this.hexWhite;
        GL11.glNormal3f((float)0.0f, (float)-1.0f, (float)0.0f);
        String otText = I18n.translateToLocal("gui.screenpokechecker.ot");
        this.mc.fontRenderer.drawString(otText, -40 + (46 - this.mc.fontRenderer.getStringWidth(otText) / 2), 84, 0x353535);
        String specialTextureText = "Special Texture";
        this.mc.fontRenderer.drawString(specialTextureText, -40 + (46 - this.mc.fontRenderer.getStringWidth(specialTextureText) / 2), 116, 0x353535);
        String happinessText = I18n.translateToLocal("gui.screenpokechecker.happiness");
        this.mc.fontRenderer.drawString(happinessText, 55 + (31 - this.mc.fontRenderer.getStringWidth(happinessText) / 2), 36, 0x353535);
        String growthText = I18n.translateToLocal("gui.screenpokechecker.growth");
        this.mc.fontRenderer.drawString(growthText, 55 + (31 - this.mc.fontRenderer.getStringWidth(growthText) / 2), 52, 0x353535);
        if (!this.targetPacket.isEgg) {
            this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.screenpokechecker.lvl") + " " + this.targetPacket.lvl, 58, -19, 0xFFFFFF);
            String dexText = I18n.translateToLocal("gui.screenpokechecker.number") + " " + String.valueOf(this.targetPacket.getNationalPokedexNumber());
            this.drawString(this.mc.fontRenderer, dexText, 58, -4, 0xFFFFFF);
            this.drawString(this.mc.fontRenderer, this.targetPacket.getSpecies().getPokemonName(), 58 + this.mc.fontRenderer.getStringWidth(dexText) + 20, -4, 0xFFFFFF);
            if (this.targetPacket.pokeball != null) {
                this.itemRender.renderItemIntoGUI(new ItemStack(this.targetPacket.pokeball.getItem()), -39, -24);
            }
            String otName = this.targetPacket.OT;
            this.mc.fontRenderer.drawString(otName, -40 + (46 - this.mc.fontRenderer.getStringWidth(otName) / 2), 100, 0x595959);
            IEnumSpecialTexture currentType = this.targetPacket.getSpecies().getSpecialTexture(this.targetPacket.getSpecies().getFormEnum(this.targetPacket.form), this.targetPacket.specialTexture);
            String texture = !currentType.getProperName().isEmpty() ? currentType.getProperName() : "None";
            this.mc.fontRenderer.drawString(texture, -40 + (46 - this.mc.fontRenderer.getStringWidth(texture) / 2), 132, 0x595959);
            String shiny = this.targetPacket.isShiny ? "Shiny" : "Not Shiny";
            this.mc.fontRenderer.drawString(shiny, -40 + (46 - this.mc.fontRenderer.getStringWidth(shiny) / 2), 148, this.targetPacket.isShiny ? 16768644 : 0xFFFFFF);
            String growth = this.targetPacket.growth.getLocalizedName();
            this.mc.fontRenderer.drawString(growth, 122, 52, 0x595959);
            this.mc.fontRenderer.drawString(Integer.toString(this.targetPacket.friendship), 122, 36, 0x595959);
        } else {
            this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.screenpokechecker.lvl") + " ???", 58, -19, 0xFFFFFF);
            this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.screenpokechecker.number") + " ???", 58, -4, 0xFFFFFF);
            this.drawCenteredString(this.mc.fontRenderer, "???", 140, -4, 0xDDDDDD);
            this.mc.fontRenderer.drawString("???", -40 + (46 - this.mc.fontRenderer.getStringWidth("???") / 2), 100, 0x595959);
            this.mc.fontRenderer.drawString("???", -40 + (46 - this.mc.fontRenderer.getStringWidth("???") / 2), 132, 0x595959);
            this.mc.fontRenderer.drawString("???", 122, 52, 0x595959);
            this.mc.fontRenderer.drawString("???", 122, 36, 0x595959);
        }
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.screenpokechecker.stats"), 136, 166, hexColor);
        this.drawBasePokemonInfo();
    }

    @Override
    public void drawGuiContainerBackgroundLayer(float f, int i, int i1) {
        if (this.targetPacket == null) {
            this.mc.player.closeScreen();
        }
        ScaledResolution var5 = new ScaledResolution(Minecraft.getMinecraft());
        var5.getScaledWidth();
        var5.getScaledHeight();
        this.mc.renderEngine.bindTexture(GuiResources.summaryStats);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.drawTexturedModalRect((this.width - this.xSize) / 2 - 40, (this.height - this.ySize) / 2 - 25, 0, 0, 256, 205);
        this.drawPokemonName();
        this.drawArrows(i, i1);
        this.mc.renderEngine.bindTexture(GuiResources.summarySummary);
        this.drawTexturedModalRect(this.width / 2 + 30, this.height / 2 + 79, 95, 205, 62, 16);
        float xpBarFill = this.targetPacket.getExpFraction() * 10.0f;
        this.drawTexturedModalRect(this.width / 2 + 25, this.height / 2 - 105, 162, 222, 49, 12);
        if (xpBarFill >= 1.0f) {
            this.drawTexturedModalRect(this.width / 2 + 25, this.height / 2 - 105, 162, 237, (int)xpBarFill * 5, 12);
        }
    }

    @Override
    public void drawCenteredStringWithoutShadow(String par2Str, int par3, int par4, int par5) {
        this.mc.fontRenderer.drawString(par2Str, par3 - this.mc.fontRenderer.getStringWidth(par2Str) / 2, par4, par5);
    }
}

