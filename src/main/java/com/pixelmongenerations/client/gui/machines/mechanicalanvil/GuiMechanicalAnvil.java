/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.machines.mechanicalanvil;

import com.pixelmongenerations.client.gui.machines.mechanicalanvil.ContainerMechanicalAnvil;
import com.pixelmongenerations.common.block.tileEntities.TileEntityMechanicalAnvil;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiMechanicalAnvil
extends GuiContainer {
    public static final ResourceLocation furnaceGuiTextures = new ResourceLocation("textures/gui/container/furnace.png");
    private TileEntityMechanicalAnvil tileMechanicalAnvil;

    public GuiMechanicalAnvil(InventoryPlayer inventoryPlayer, TileEntityMechanicalAnvil tileEntityFurnace) {
        super(new ContainerMechanicalAnvil(inventoryPlayer, tileEntityFurnace));
        this.tileMechanicalAnvil = tileEntityFurnace;
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
        this.renderHoveredToolTip(mouseX, mouseY);
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        this.fontRenderer.drawString("Mechanical Anvil", this.xSize / 2 - this.fontRenderer.getStringWidth("Mechanical Anvil") / 2, 6, 0x404040);
        this.fontRenderer.drawString(I18n.format("container.inventory", new Object[0]), 8, this.ySize - 96 + 2, 0x404040);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.mc.getTextureManager().bindTexture(furnaceGuiTextures);
        int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
        if (this.tileMechanicalAnvil.isRunning()) {
            int i1 = this.tileMechanicalAnvil.getBurnTimeRemainingScaled(13);
            this.drawTexturedModalRect(k + 56, l + 36 + 12 - i1, 176, 12 - i1, 14, i1 + 1);
            i1 = this.tileMechanicalAnvil.getHammerProgressScaled(24);
            this.drawTexturedModalRect(k + 79, l + 34, 176, 14, i1 + 1, 16);
        }
    }
}

