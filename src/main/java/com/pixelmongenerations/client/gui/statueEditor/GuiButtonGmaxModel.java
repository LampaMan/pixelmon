/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.statueEditor;

import com.pixelmongenerations.client.gui.elements.GuiButtonToggle;
import java.util.function.Consumer;

public class GuiButtonGmaxModel
extends GuiButtonToggle {
    public GuiButtonGmaxModel(int buttonID, int x, int y, int width, int height, boolean on, Consumer<Boolean> consumer) {
        super(buttonID, x, y, width, height, "gui.statue.gmax", "gui.statue.normal", on, consumer);
    }
}

