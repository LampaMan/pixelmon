/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.camera.CameraMode;
import com.pixelmongenerations.client.camera.CameraTargetEntity;
import com.pixelmongenerations.client.camera.GuiCamera;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.battles.EvoInfo;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.network.packetHandlers.battles.BattleGuiClosed;
import com.pixelmongenerations.core.network.packetHandlers.evolution.EvolutionResponse;
import com.pixelmongenerations.core.network.packetHandlers.evolution.EvolutionStage;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.storage.ClientData;
import com.pixelmongenerations.core.util.RegexPatterns;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.resources.I18n;

public class GuiEvolve
extends GuiCamera {
    public static EntityPixelmon currentPokemon;
    boolean createdEntity = false;
    String newPokemon;
    String oldNickname;
    boolean cancelled = false;
    EvoInfo evoInfo = null;
    int ticks = 0;
    int fadeCount = 0;

    public GuiEvolve() {
        super(CameraMode.Evolution);
        if (this.mc == null) {
            this.mc = Minecraft.getMinecraft();
        }
        if (ClientProxy.battleManager.evolveList.isEmpty()) {
            this.mc.player.closeScreen();
            return;
        }
        this.checkForPokemon();
        if (currentPokemon == null) {
            Pixelmon.NETWORK.sendToServer(new EvolutionResponse(this.evoInfo.pokemonID));
        }
    }

    private void checkForPokemon() {
        if (this.evoInfo == null) {
            this.evoInfo = ClientProxy.battleManager.evolveList.get(0);
            ClientProxy.battleManager.evolveList.remove(0);
        }
        int[] pokemonID = this.evoInfo.pokemonID;
        this.newPokemon = this.evoInfo.evolveInto;
        currentPokemon = GuiHelper.getEntity(pokemonID);
        Minecraft mc = Minecraft.getMinecraft();
        if (currentPokemon == null) {
            return;
        }
        GuiEvolve.currentPokemon.tasks.taskEntries.clear();
        if (ClientProxy.camera != null && PixelmonConfig.useBattleCamera) {
            ClientProxy.camera.isDead = false;
            mc.setRenderViewEntity(ClientProxy.camera);
            ClientProxy.camera.setTargetRandomPosition(new CameraTargetEntity(currentPokemon));
        }
        this.oldNickname = currentPokemon.getNickname();
        this.calcSizeDifference();
    }

    private void calcSizeDifference() {
        BaseStats bs = Entity3HasStats.getBaseStats(this.newPokemon).get();
        BaseStats currentStats = currentPokemon.initializeBaseStatsIfNull();
        if (currentStats == null) {
            return;
        }
        GuiEvolve.currentPokemon.heightDiff = bs.height - currentStats.height;
        GuiEvolve.currentPokemon.widthDiff = bs.width - currentStats.width;
        GuiEvolve.currentPokemon.lengthDiff = bs.length - currentStats.length;
    }

    @Override
    public void drawBackground(int par1) {
    }

    @Override
    public void drawDefaultBackground() {
    }

    @Override
    public void initGui() {
        super.initGui();
        this.buttonList.clear();
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        if (currentPokemon != null && GuiEvolve.currentPokemon.evoStage == EvolutionStage.Choice) {
            ++this.ticks;
            if (this.ticks >= 80) {
                Pixelmon.NETWORK.sendToServer(new EvolutionResponse(currentPokemon.getPokemonId(), true));
                GuiEvolve.currentPokemon.evolvingVal = 0;
                GuiEvolve.currentPokemon.evoAnimTicks = 0;
                this.ticks = 0;
            }
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float f, int mouseX, int mouseY) {
        String s;
        if (this.mc == null) {
            this.mc = Minecraft.getMinecraft();
        }
        this.mc.renderEngine.bindTexture(GuiResources.evo);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        if (currentPokemon == null) {
            this.checkForPokemon();
            return;
        }
        if (GuiEvolve.currentPokemon.evoStage != EvolutionStage.PreAnimation && GuiEvolve.currentPokemon.evoStage != EvolutionStage.PostAnimation) {
            GuiHelper.drawImageQuad(this.width / 2 - 120, this.height / 4 - 40, 240.0, 40.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        }
        if (GuiEvolve.currentPokemon.evoStage == EvolutionStage.PreChoice) {
            s = I18n.format("gui.guiEvolve.huh", new Object[0]);
            this.mc.fontRenderer.drawString(s, this.width / 2 - this.mc.fontRenderer.getStringWidth(s) / 2, this.height / 4 - 30, 0xFFFFFF);
        }
        if (GuiEvolve.currentPokemon.evoStage == EvolutionStage.Choice) {
            this.oldNickname = currentPokemon.getEscapedNickname();
            s = RegexPatterns.$_P_VAR.matcher(I18n.format("gui.guiEvolve.evolve", new Object[0])).replaceAll(this.oldNickname);
            this.mc.fontRenderer.drawString(s, this.width / 2 - this.mc.fontRenderer.getStringWidth(s) / 2, this.height / 4 - 30, 0xFFFFFF);
            int xPos = this.width / 2 - 30;
            int yPos = this.height / 4 - 15;
            if (mouseX >= xPos && mouseX <= xPos + 60 && mouseY >= yPos && mouseY <= yPos + 17) {
                this.mc.renderEngine.bindTexture(GuiResources.buttonOver);
            } else {
                this.mc.renderEngine.bindTexture(GuiResources.button);
            }
            GuiHelper.drawImageQuad(xPos, yPos, 60.0, 17.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            s = I18n.format("gui.cancel.text", new Object[0]);
            this.mc.fontRenderer.drawString(s, this.width / 2 - this.mc.fontRenderer.getStringWidth(s) / 2, this.height / 4 - 11, 0xFFFFFF);
        }
        if (this.cancelled) {
            s = I18n.format("gui.guiEvolve.cancel", new Object[0]);
            this.mc.fontRenderer.drawString(s, this.width / 2 - this.mc.fontRenderer.getStringWidth(s) / 2, this.height / 4 - 30, 0xFFFFFF);
        } else if (GuiEvolve.currentPokemon.evoStage == EvolutionStage.End || GuiEvolve.currentPokemon.evoStage == null) {
            s = RegexPatterns.$_P_VAR.matcher(I18n.format("gui.guiEvolve.done", new Object[0])).replaceAll(this.oldNickname);
            s = RegexPatterns.$_N_VAR.matcher(s).replaceAll(currentPokemon.getLocalizedName());
            this.mc.fontRenderer.drawString(s, this.width / 2 - this.mc.fontRenderer.getStringWidth(s) / 2, this.height / 4 - 30, 0xFFFFFF);
        }
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int par3) {
        if (currentPokemon == null) {
            return;
        }
        if (GuiEvolve.currentPokemon.evoStage == EvolutionStage.Choice) {
            int xPos = this.width / 2 - 30;
            int yPos = this.height / 4 - 15;
            if (mouseX >= xPos && mouseX <= xPos + 60 && mouseY >= yPos && mouseY <= yPos + 17) {
                Pixelmon.NETWORK.sendToServer(new EvolutionResponse(currentPokemon.getPokemonId(), false));
                GuiEvolve.currentPokemon.evoStage = null;
                this.cancelled = true;
            }
        } else if (GuiEvolve.currentPokemon.evoStage == null || this.cancelled) {
            Minecraft minecraft = Minecraft.getMinecraft();
            minecraft.player.closeScreen();
            if (!ClientProxy.battleManager.evolveList.isEmpty()) {
                minecraft.player.openGui(Pixelmon.INSTANCE, EnumGui.Evolution.getIndex(), minecraft.world, 0, 0, 0);
            } else if (ServerStorageDisplay.bossDrops != null) {
                minecraft.player.openGui(Pixelmon.INSTANCE, EnumGui.ItemDrops.getIndex(), minecraft.world, 0, 0, 0);
            } else if (!ClientProxy.battleManager.newAttackList.isEmpty()) {
                minecraft.player.openGui(Pixelmon.INSTANCE, EnumGui.LearnMove.getIndex(), minecraft.world, 0, 0, 0);
            } else if (ClientData.openMegaItemGui) {
                minecraft.player.openGui(Pixelmon.INSTANCE, EnumGui.MegaItem.getIndex(), minecraft.world, 1, 0, 0);
            } else {
                Pixelmon.NETWORK.sendToServer(new BattleGuiClosed());
            }
            minecraft.setRenderViewEntity(minecraft.player);
        }
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
    }

    @Override
    public void drawScreen(int par1, int par2, float par3) {
        this.drawGuiContainerBackgroundLayer(par3, par1, par2);
        GlStateManager.disableRescaleNormal();
        RenderHelper.disableStandardItemLighting();
        GlStateManager.disableLighting();
        GlStateManager.disableDepth();
    }
}

