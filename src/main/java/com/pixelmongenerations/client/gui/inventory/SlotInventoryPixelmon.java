/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.inventory;

import com.pixelmongenerations.client.gui.pc.SlotPC;
import com.pixelmongenerations.core.network.PixelmonData;
import java.awt.Rectangle;

public class SlotInventoryPixelmon
extends SlotPC {
    public int heldItemX;
    public int heldItemY;
    private static final int HELD_X_OFFSET = 19;

    public SlotInventoryPixelmon(int x, int y, PixelmonData pokemon) {
        super(x, y, pokemon);
        this.swidth = 16;
        this.heldItemX = x + 19;
        this.heldItemY = y;
    }

    public Rectangle getHeldItemBounds() {
        return new Rectangle(this.heldItemX, this.heldItemY, this.swidth, this.swidth);
    }

    void setX(int newX) {
        this.x = newX;
        this.heldItemX = this.x + 19;
    }
}

