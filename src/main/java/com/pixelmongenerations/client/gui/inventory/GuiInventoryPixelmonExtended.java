/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Ordering
 */
package com.pixelmongenerations.client.gui.inventory;

import com.google.common.collect.Ordering;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.inventory.IInventoryPixelmon;
import com.pixelmongenerations.client.gui.inventory.InventoryPixelmon;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;

public class GuiInventoryPixelmonExtended
extends GuiInventory
implements IInventoryPixelmon {
    private InventoryPixelmon inventory = new InventoryPixelmon<GuiInventoryPixelmonExtended>(this, 42);

    public GuiInventoryPixelmonExtended(EntityPlayer par1EntityPlayer) {
        super(par1EntityPlayer);
    }

    public void drawActivePotionEffects() {
        if (this.inventory.isPokeInfoOpen()) {
            return;
        }
        int i = this.guiLeft - 161;
        int j = this.guiTop;
        int k = 166;
        Collection<PotionEffect> collection = this.mc.player.getActivePotionEffects();
        if (!collection.isEmpty()) {
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            GlStateManager.disableLighting();
            int l = 33;
            if (collection.size() > 5) {
                l = 132 / (collection.size() - 1);
            }
            for (PotionEffect potioneffect : Ordering.natural().sortedCopy(collection)) {
                Potion potion = potioneffect.getPotion();
                if (!potion.shouldRender(potioneffect)) continue;
                GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
                this.mc.getTextureManager().bindTexture(INVENTORY_BACKGROUND);
                this.drawTexturedModalRect(i, j, 0, 166, 140, 32);
                if (potion.hasStatusIcon()) {
                    int i1 = potion.getStatusIconIndex();
                    this.drawTexturedModalRect(i + 6, j + 7, 0 + i1 % 8 * 18, 198 + i1 / 8 * 18, 18, 18);
                }
                potion.renderInventoryEffect(i, j, potioneffect, this.mc);
                if (!potion.shouldRenderInvText(potioneffect)) {
                    j += l;
                    continue;
                }
                String s1 = I18n.format(potion.getName(), new Object[0]);
                if (potioneffect.getAmplifier() == 1) {
                    s1 = s1 + " " + I18n.format("enchantment.level.2", new Object[0]);
                } else if (potioneffect.getAmplifier() == 2) {
                    s1 = s1 + " " + I18n.format("enchantment.level.3", new Object[0]);
                } else if (potioneffect.getAmplifier() == 3) {
                    s1 = s1 + " " + I18n.format("enchantment.level.4", new Object[0]);
                }
                this.fontRenderer.drawStringWithShadow(s1, i + 10 + 18, j + 6, 0xFFFFFF);
                String s = Potion.getPotionDurationString(potioneffect, 1.0f);
                this.fontRenderer.drawStringWithShadow(s, i + 10 + 18, j + 6 + 10, 0x7F7F7F);
                j += l;
            }
        }
    }

    @Override
    public void initGui() {
        super.initGui();
        this.inventory.initGui();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.inventory.drawScreen(mouseX, mouseY, partialTicks);
    }

    @Override
    public void superDrawScreen(int mouseX, int mouseY, float partialTicks) {
        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    @Override
    public float getZLevel() {
        return this.zLevel;
    }

    @Override
    public int getGUILeft() {
        return this.guiLeft;
    }

    @Override
    public void offsetGUILeft(int offset) {
        this.guiLeft += offset;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3) {
        super.drawGuiContainerBackgroundLayer(par1, par2, par3);
        GlStateManager.enableRescaleNormal();
        if (!this.func_194310_f().isVisible()) {
            this.mc.renderEngine.bindTexture(GuiResources.pixelmonOverlayExtended2New);
            this.drawTexturedModalRect(this.guiLeft - 42, this.height / 2 - 83, 0, 0, 46, 167);
            this.inventory.drawGuiContainerBackgroundLayer(par1, par2, par3);
        }
    }

    @Override
    public void subDrawGradientRect(int left, int top, int right, int bottom, int startColor, int endColor) {
        super.drawGradientRect(left, top, right, bottom, startColor, endColor);
    }

    @Override
    public List<GuiButton> getButtonList() {
        return this.buttonList;
    }

    @Override
    protected void mouseClicked(int x, int y, int mouseButton) throws IOException {
        if (this.func_194310_f().isVisible() || this.inventory.mouseClicked(x, y, mouseButton) && !this.inParty(x, y)) {
            try {
                super.mouseClicked(x, y, mouseButton);
            }
            catch (IndexOutOfBoundsException indexOutOfBoundsException) {
                // empty catch block
            }
        }
    }

    @Override
    protected void mouseReleased(int mouseX, int mouseY, int state) {
        if (!this.inParty(mouseX, mouseY)) {
            super.mouseReleased(mouseX, mouseY, state);
        }
    }

    private boolean inParty(int x, int y) {
        return x > this.guiLeft - 42 && x < this.guiLeft + 4 && y > this.height / 2 - 83 && y < this.height / 2 + 83;
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        this.inventory.tick();
    }
}

