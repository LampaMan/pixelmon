/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.inventory;

import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.inventory.IInventoryPixelmon;
import com.pixelmongenerations.client.gui.inventory.InventoryPixelmon;
import java.io.IOException;
import java.util.List;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainerCreative;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;

public class GuiCreativeInventoryExtended
extends GuiContainerCreative
implements IInventoryPixelmon {
    private InventoryPixelmon inventory = new InventoryPixelmon<GuiCreativeInventoryExtended>(this, 53);

    public GuiCreativeInventoryExtended(EntityPlayer par1EntityPlayer) {
        super(par1EntityPlayer);
    }

    @Override
    public void initGui() {
        super.initGui();
        this.inventory.initGui();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.inventory.drawScreen(mouseX, mouseY, partialTicks);
    }

    @Override
    public void superDrawScreen(int mouseX, int mouseY, float partialTicks) {
        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    @Override
    public float getZLevel() {
        return this.zLevel;
    }

    @Override
    public int getGUILeft() {
        return this.guiLeft;
    }

    @Override
    public void offsetGUILeft(int offset) {
        this.guiLeft += offset;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3) {
        super.drawGuiContainerBackgroundLayer(par1, par2, par3);
        GlStateManager.enableRescaleNormal();
        this.mc.renderEngine.bindTexture(GuiResources.pixelmonCreativeInventoryNew);
        this.drawTexturedModalRect(this.guiLeft - 53, this.height / 2 - 83, 0, 0, 54, 167);
        this.inventory.drawGuiContainerBackgroundLayer(par1, par2, par3);
    }

    @Override
    public void subDrawGradientRect(int left, int top, int right, int bottom, int startColor, int endColor) {
        super.drawGradientRect(left, top, right, bottom, startColor, endColor);
    }

    @Override
    public List<GuiButton> getButtonList() {
        return this.buttonList;
    }

    @Override
    protected void mouseClicked(int x, int y, int mouseButton) throws IOException {
        if (this.inventory.mouseClicked(x, y, mouseButton) && !this.inParty(x, y)) {
            try {
                super.mouseClicked(x, y, mouseButton);
            }
            catch (IndexOutOfBoundsException indexOutOfBoundsException) {
                // empty catch block
            }
        }
    }

    @Override
    protected void mouseReleased(int mouseX, int mouseY, int state) {
        if (!this.inParty(mouseX, mouseY)) {
            super.mouseReleased(mouseX, mouseY, state);
        }
    }

    private boolean inParty(int x, int y) {
        return x > this.guiLeft - 33 && x < this.guiLeft + 13 && y > this.height / 2 - 83 && y < this.height / 2 + 83;
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        this.inventory.tick();
    }
}

