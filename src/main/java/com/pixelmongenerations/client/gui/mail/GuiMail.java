/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Keyboard
 */
package com.pixelmongenerations.client.gui.mail;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.mail.MailPacket;
import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ChatAllowedCharacters;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Keyboard;

@SideOnly(value=Side.CLIENT)
public class GuiMail extends GuiContainer {

    private boolean editable = true;
    private ResourceLocation backgroundTexture;
    private final ResourceLocation sealButtonTexture = new ResourceLocation(GuiResources.prefix + "gui/mail/sealButton.png");
    private final ResourceLocation abandonButtonTexture = new ResourceLocation(GuiResources.prefix + "gui/mail/closeButton.png");
    private GuiButton sealButton;
    private final int sealX = 60;
    private final int sealY = 185;
    private GuiButton exitButton;
    private final int exitX = 100;
    private final int exitY = 185;
    private String bookContents = "";
    private String author = "";
    private EntityPlayer player;

    public GuiMail(EntityPlayer player, ItemStack item) {
        super(new ContainerEmpty());
        this.player = player;
        String mailType = item.getTranslationKey().split("_")[1];
        this.backgroundTexture = new ResourceLocation(GuiResources.prefix + "gui/mail/" + mailType + "mail.png");
        if (item.hasTagCompound()) {
            NBTTagCompound nbtdata = item.getTagCompound();
            this.editable = nbtdata.getBoolean("editable");
            this.author = nbtdata.getString("author");
            this.bookContents = nbtdata.getString("contents");
        }
    }

    private void writeLetterData(boolean shouldSeal) {
        MailPacket packetMail = new MailPacket(shouldSeal, this.player.getDisplayName().getFormattedText(), this.bookContents, this.player);
        Pixelmon.NETWORK.sendToServer(packetMail);
        this.mc.player.closeScreen();
    }

    @Override
    public void initGui() {
        Keyboard.enableRepeatEvents((boolean)true);
        int buttonID = 0;
        int xOffset = (this.width - 252) / 2;
        int yOffset = 2;
        int n = buttonID++;
        this.getClass();
        this.getClass();
        this.sealButton = new GuiButton(n, xOffset + 60, yOffset + 185, 30, 20, I18n.translateToLocal("gui.mail.seal"));
        this.getClass();
        this.getClass();
        this.exitButton = new GuiButton(buttonID, xOffset + 100, yOffset + 185, 30, 20, I18n.translateToLocal("gui.mail.close"));
        if (!this.editable) {
            this.sealButton.enabled = false;
        }
        this.buttonList.add(this.sealButton);
        this.buttonList.add(this.exitButton);
        super.initGui();
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        switch (button.id) {
            case 0: {
                this.writeLetterData(true);
                break;
            }
            case 1: {
                if (this.editable) {
                    this.writeLetterData(false);
                    break;
                }
                this.mc.player.closeScreen();
            }
        }
    }

    @Override
    protected void keyTyped(char keyChar, int keyCode) throws IOException {
        if (!this.editable) {
            return;
        }
        block0 : switch (keyChar) {
            case '\u0016': {
                for (char character : GuiScreen.getClipboardString().toCharArray()) {
                    this.addNormalCharacter(character);
                }
                break;
            }
            default: {
                switch (keyCode) {
                    case 14: {
                        if (this.bookContents.isEmpty()) break block0;
                        this.bookContents = this.bookContents.substring(0, this.bookContents.length() - 1);
                        break block0;
                    }
                    case 28: 
                    case 156: {
                        String[] strings2 = this.bookContents.split("\n");
                        if (strings2.length == 14) break block0;
                        this.bookContents = this.bookContents + "\n";
                        break block0;
                    }
                    default: {
                        this.addNormalCharacter(keyChar);
                    }
                }
            }
        }
    }

    private void addNormalCharacter(char keyChar) {
        int widthInPixels;
        if (!ChatAllowedCharacters.isAllowedCharacter(keyChar)) {
            return;
        }
        String[] strings = this.bookContents.split("\n");
        if (strings.length > 0 && (widthInPixels = this.fontRenderer.getStringWidth(strings[strings.length - 1])) >= 200) {
            if (strings.length == 14) {
                return;
            }
            this.bookContents = this.bookContents + "\n";
        }
        this.bookContents = this.bookContents + keyChar;
    }

    @Override
    public void onGuiClosed() {
        Keyboard.enableRepeatEvents((boolean)false);
        super.onGuiClosed();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float wut) {
        int guiWidth = 252;
        int guiHeight = 188;
        this.mc.renderEngine.bindTexture(this.backgroundTexture);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        int xOffset = (this.width - 252) / 2;
        int yOffset = 2;
        Gui.drawModalRectWithCustomSizedTexture(xOffset, yOffset, 0.0f, 0.0f, 252, 188, 252.0f, 188.0f);
        int buttonWidth = 32;
        int buttonHeight = 32;
        if (this.editable) {
            this.mc.renderEngine.bindTexture(this.sealButtonTexture);
            Gui.drawModalRectWithCustomSizedTexture(xOffset + 60, yOffset + 185, 0.0f, 0.0f, buttonWidth, buttonHeight, buttonWidth, buttonHeight);
        }
        this.mc.renderEngine.bindTexture(this.abandonButtonTexture);
        Gui.drawModalRectWithCustomSizedTexture(xOffset + 100, yOffset + 185, 0.0f, 0.0f, buttonWidth, buttonHeight, buttonWidth, buttonHeight);
        String[] lines = this.bookContents.split("\n");
        for (int lineNumber = 0; lineNumber < lines.length; ++lineNumber) {
            this.fontRenderer.drawString(lines[lineNumber], xOffset + 27, yOffset + 32 + lineNumber * 9 - this.fontRenderer.FONT_HEIGHT, 0);
        }
        if (this.backgroundTexture.toString().contains("harbor")) {
            this.fontRenderer.drawString(this.author, xOffset + 164, yOffset + 168 - this.fontRenderer.FONT_HEIGHT, 0);
        } else {
            this.fontRenderer.drawString(this.author, xOffset + 164, yOffset + 175 - this.fontRenderer.FONT_HEIGHT, 0);
        }
        this.drawButtonTooltips(mouseX, mouseY);
    }

    private void drawButtonTooltips(int mouseX, int mouseY) {
        if (this.mouseOverButton(mouseX, mouseY, this.sealButton)) {
            ArrayList<String> tooltipData = new ArrayList<String>(1);
            tooltipData.add(I18n.translateToLocal("gui.mail.seal"));
            GuiHelper.renderTooltip(mouseX, mouseY, tooltipData, Color.BLUE.getRGB(), Color.BLACK.getRGB(), 100, false, false);
        } else if (this.mouseOverButton(mouseX, mouseY, this.exitButton)) {
            ArrayList<String> tooltipData = new ArrayList<String>(1);
            tooltipData.add(I18n.translateToLocal("gui.mail.close"));
            GuiHelper.renderTooltip(mouseX, mouseY, tooltipData, Color.BLUE.getRGB(), Color.BLACK.getRGB(), 100, false, false);
        }
    }

    private boolean mouseOverButton(int mouseX, int mouseY, GuiButton button) {
        return button.enabled && mouseX >= button.x && mouseX <= button.x + button.width && mouseY >= button.y && mouseY <= button.y + button.width;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        super.drawGuiContainerForegroundLayer(mouseX, mouseY);
    }
}

