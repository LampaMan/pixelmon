/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.pokedex;

import com.pixelmongenerations.client.SoundHelper;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.pokedex.ClientPokedexManager;
import com.pixelmongenerations.client.gui.pokedex.GuiPokedexHome;
import com.pixelmongenerations.client.render.RenderPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.common.pokedex.Pokedex;
import com.pixelmongenerations.common.pokedex.PokedexEntry;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.network.packetHandlers.RequestSpawnInfo;
import com.pixelmongenerations.core.util.PixelSounds;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.text.translation.I18n;

public class GuiPokedexInfo
extends GuiContainer {
    private EnumSpecies pokemon;
    private final String loadingText;
    private String prevSearchText;
    public static String serverBiomes;
    public static String serverTimes;
    private float spinCount = 0.0f;
    private float spinSpeed = 0.8f;
    private boolean playedSound = false;
    private boolean hoveringShinyOn = false;
    public boolean shinyOn = false;

    public GuiPokedexInfo(EnumSpecies pokemon) {
        this(pokemon, false);
    }

    public GuiPokedexInfo(EnumSpecies pokemon, boolean shinyOn) {
        super(new ContainerEmpty());
        this.pokemon = pokemon;
        this.loadingText = "Undiscovered";
        this.shinyOn = shinyOn;
        int dex = pokemon.getNationalPokedexInteger();
        if (ClientPokedexManager.pokedex.hasSeen(dex)) {
            Pixelmon.NETWORK.sendToServer(new RequestSpawnInfo(pokemon.getNationalPokedexInteger()));
        } else {
            while (!ClientPokedexManager.pokedex.hasSeen(dex)) {
                Optional<EnumSpecies> epOpt;
                if (++dex > 898) {
                    dex = 1;
                }
                if (!(epOpt = EnumSpecies.getFromDexUnsafe(dex)).isPresent()) continue;
                this.pokemon = epOpt.get();
                Pixelmon.NETWORK.sendToServer(new RequestSpawnInfo(this.pokemon.getNationalPokedexInteger()));
            }
        }
    }

    @Override
    public void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        this.mc.renderEngine.bindTexture(GuiResources.pokedexInfoBackground);
        GuiHelper.drawImageQuad(0.0, 0.0, this.width, this.height, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        PokedexEntry entry = ClientPokedexManager.pokedex.getEntry(this.pokemon.getNationalPokedexInteger());
        boolean hasSeenPokemon = (ClientPokedexManager.pokedex.hasSeen(entry.natPokedexNum) || this.mc.player.capabilities.isCreativeMode) && !Pokedex.isEntryEmpty(entry.natPokedexNum);
        EntityPixelmon ep = entry.getRenderTarget(this.mc.world);
        if (ep != null && ep.getModel() != null) {
            if (hasSeenPokemon) {
                ep.setShiny(this.shinyOn);
            }
            this.drawEntityToScreen(this.toInt((double)this.width / 1.76), this.toInt((double)this.height - (double)this.height / 2.96), this.toInt((double)this.width / 2.9), this.toInt(this.width / 4), ep, partialTicks, true);
        }
        if (hasSeenPokemon && !this.playedSound) {
            Optional<SoundEvent> pokemonSoundOpt = PixelSounds.getSoundFor(ep);
            if (pokemonSoundOpt.isPresent()) {
                SoundHelper.playSound(pokemonSoundOpt.get());
            }
            this.playedSound = true;
        }
        int xPos = this.toInt(this.width / 18);
        int yPos = this.toInt(this.height / 12);
        int textGap = 15;
        GlStateManager.pushMatrix();
        String title = I18n.translateToLocal("gui.pokedex.infotitle") + String.format(" %03d", this.pokemon.getNationalPokedexInteger());
        float textScale = 1.5f;
        int textHeight = this.fontRenderer.FONT_HEIGHT * 2 + 15;
        int textHeightY = this.toInt((float)textHeight * textScale);
        while ((double)textHeightY > (double)Minecraft.getMinecraft().displayHeight / 21.1) {
            textHeightY = this.toInt((float)textHeight * (textScale -= 0.05f));
            textGap = this.toInt(textHeightY / 3);
        }
        int textWidthX = this.toInt((float)this.fontRenderer.getStringWidth(title) * textScale);
        while ((double)textWidthX > (double)Minecraft.getMinecraft().displayWidth / 8.2) {
            textWidthX = this.toInt((float)this.fontRenderer.getStringWidth(title) * (textScale -= 0.05f));
        }
        GlStateManager.scale(textScale, textScale, textScale);
        this.fontRenderer.drawString(title, this.toInt((float)xPos / textScale), this.toInt((float)yPos / textScale), 0x363636);
        this.fontRenderer.drawString(this.pokemon.name, this.toInt((float)xPos / textScale), this.toInt((float)(yPos + textGap) / textScale), 0x363636);
        if (ep.isShiny()) {
            this.fontRenderer.drawString("\u2605", this.toInt((float)xPos / textScale + (float)this.fontRenderer.getStringWidth(this.pokemon.name)) + 3, this.toInt((float)(yPos + textGap) / textScale), 16774625);
        }
        this.fontRenderer.drawString("Generation " + EntityPixelmon.getGenerationFrom(this.pokemon), this.toInt((float)xPos / textScale), this.toInt(((double)yPos + (double)textGap * 2.8) / (double)textScale), 0x363636);
        this.fontRenderer.drawString("Spawn Times", this.toInt((double)this.width / 1.265 / (double)textScale), this.toInt((float)yPos / textScale), 0x363636);
        int textWidth = this.toInt((double)this.width / 6.8);
        this.fontRenderer.drawSplitString(serverTimes != null ? serverTimes : this.loadingText, this.toInt((double)this.width / 1.265 / (double)textScale), this.toInt(((double)yPos + (double)textGap * 1.6) / (double)textScale), textWidth, 0x363636);
        int shinyYPos = yPos + this.toInt((double)this.height / 6.6);
        this.fontRenderer.drawString("Shiny Preview", this.toInt((double)this.width / 1.265 / (double)textScale), this.toInt((float)shinyYPos / textScale), 0x363636);
        int bubble = this.toInt(20.0f * textScale);
        int shinyStatusX = this.toInt((double)this.width / 1.265) - bubble / 2;
        int shinyStatusY = this.toInt((double)shinyYPos + (double)textGap * 1.6) - bubble / 2;
        int shinyStatusWidth = shinyStatusX + this.toInt((float)this.fontRenderer.getStringWidth("Off") * textScale) + bubble;
        int shinyStatusHeight = shinyStatusY + this.toInt((float)this.fontRenderer.FONT_HEIGHT * textScale) + bubble;
        this.hoveringShinyOn = mouseX >= shinyStatusX && mouseY >= shinyStatusY && mouseX <= shinyStatusWidth && mouseY <= shinyStatusHeight;
        this.fontRenderer.drawString(this.shinyOn ? "On" : "Off", this.toInt((double)this.width / 1.265 / (double)textScale), this.toInt(((double)shinyYPos + (double)textGap * 1.6) / (double)textScale), this.hoveringShinyOn ? 16558919 : (this.shinyOn ? 7995220 : 16738388));
        GlStateManager.color(1.0f, 1.0f, 1.0f);
        GlStateManager.popMatrix();
        int typeWidth = 44;
        int typeHeight = 15;
        int gap = 5;
        int count = 1;
        for (EnumType type : ep.type) {
            ResourceLocation typeTexture;
            if (type == null || (typeTexture = GuiPokedexInfo.typeToImage(type)) == null || type == null) continue;
            this.mc.renderEngine.bindTexture(typeTexture);
            int typeX = xPos - 4 - typeWidth + typeWidth * count;
            if (count > 1) {
                typeX += gap * (count - 1);
            }
            GuiHelper.drawImageQuad(typeX, this.toInt((double)yPos + (double)textGap * 3.7), typeWidth, typeHeight, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            ++count;
        }
        GlStateManager.pushMatrix();
        String langString = null;
        if (hasSeenPokemon) {
            langString = entry.name.toLowerCase();
        } else if (this.pokemon.name.equals("\u2014\u2014\u2014")) {
            langString = "unimplemented";
        }
        langString = langString != null ? I18n.translateToLocal("pixelmon." + langString + ".description") : this.loadingText;
        float textScale2 = 1.5f;
        int textXPos = this.toInt(this.width / 24);
        int textWidth2 = this.toInt((float)(this.width / 5 * 2) / textScale2);
        int textHeight2 = this.toInt((float)this.fontRenderer.getWordWrappedHeight(langString, textWidth2) * textScale2);
        while ((double)textHeight2 > (double)this.height / 6.1) {
            textWidth2 = this.toInt((float)(this.width / 5 * 2) / (textScale2 -= 0.1f));
            textHeight2 = this.toInt((float)this.fontRenderer.getWordWrappedHeight(langString, textWidth2) * textScale2);
        }
        if (langString != null) {
            GlStateManager.scale(textScale2, textScale2, textScale2);
            this.fontRenderer.drawString("About", this.toInt((float)textXPos / textScale2), this.toInt((double)(this.height / 4) * 2.98 / (double)textScale2), 0x363636);
            this.fontRenderer.drawSplitString(langString, this.toInt((float)textXPos / textScale2), this.toInt((float)(this.height / 5 * 4) / textScale2), textWidth2, 0x363636);
        }
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix();
        float biomeScale = 1.5f;
        int biomeWidth = this.toInt((float)(this.width / 5 * 2) / biomeScale);
        int textHeight3 = this.toInt((float)this.fontRenderer.getWordWrappedHeight(serverBiomes != null ? serverBiomes : this.loadingText, biomeWidth) * biomeScale);
        while ((double)textHeight3 > (double)this.height / 6.1) {
            biomeWidth = this.toInt((float)(this.width / 5 * 2) / (biomeScale -= 0.1f));
            textHeight3 = this.toInt((float)this.fontRenderer.getWordWrappedHeight(serverBiomes != null ? serverBiomes : this.loadingText, biomeWidth) * biomeScale);
        }
        GlStateManager.scale(biomeScale, biomeScale, biomeScale);
        this.fontRenderer.drawString("Biomes", this.toInt((double)(this.width / 4) * 2.25 / (double)biomeScale), this.toInt((double)(this.height / 4) * 2.98 / (double)biomeScale), 0x363636);
        this.fontRenderer.drawSplitString(serverBiomes != null ? serverBiomes : this.loadingText, this.toInt((double)(this.width / 4) * 2.25 / (double)biomeScale), this.toInt((float)(this.height / 5 * 4) / biomeScale), biomeWidth, 0x363636);
        GlStateManager.popMatrix();
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        if (this.hoveringShinyOn) {
            this.shinyOn = !this.shinyOn;
        }
        super.mouseClicked(mouseX, mouseY, mouseButton);
    }

    @Override
    public void onGuiClosed() {
        serverBiomes = null;
        serverTimes = null;
        super.onGuiClosed();
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        if (keyCode == 1) {
            GuiPokedexHome pokedexHome = this.prevSearchText != null ? new GuiPokedexHome(this.prevSearchText) : new GuiPokedexHome(this.pokemon.getNationalPokedexInteger());
            Minecraft.getMinecraft().displayGuiScreen(pokedexHome);
            return;
        }
        if (Arrays.asList(203, 205, 208, 200, 17, 32, 30, 31).contains(keyCode)) {
            int amount = Arrays.asList(203, 208, 30, 31).contains(keyCode) ? -1 : 1;
            int dex = this.pokemon.getNationalPokedexInteger();
            dex = dex + amount > 898 ? 1 : (dex + amount < 1 ? 898 : (dex += amount));
            Optional<EnumSpecies> pokemonOpt = EnumSpecies.getFromDexUnsafe(dex);
            while (!pokemonOpt.isPresent()) {
                dex = dex + amount > 898 ? 1 : (dex + amount < 1 ? 898 : (dex += amount));
                pokemonOpt = EnumSpecies.getFromDexUnsafe(dex);
            }
            Minecraft.getMinecraft().displayGuiScreen(new GuiPokedexInfo(EnumSpecies.getFromDexUnsafe(dex).get(), this.shinyOn));
        }
    }

    public void drawEntityToScreen(int x, int y, int w, int l, EntityPixelmon e, float pt, boolean spin) {
        float scalar;
        GlStateManager.pushMatrix();
        GlStateManager.enableColorMaterial();
        GlStateManager.enableDepth();
        GlStateManager.translate(x, y, 400.0f);
        float width = this.getCorrectedWidth(e.getSpecies()).orElse(Float.valueOf(0.8f)).floatValue();
        float eHeight = (float)l / 1.4f / 4.0f;
        float eWidth = (float)l / width / 4.0f;
        float f = scalar = eHeight > eWidth ? eHeight : eWidth;
        if (e.getSpecies() == EnumSpecies.Electrode) {
            GlStateManager.translate(0.0f, -1.0f * scalar, 0.0f);
        }
        GlStateManager.scale(scalar, scalar, scalar);
        GlStateManager.rotate(180.0f, 0.0f, 0.0f, 1.0f);
        if (spin) {
            GlStateManager.rotate(this.spinCount += 1.5f * pt, 0.0f, 1.0f, 0.0f);
        }
        RenderHelper.enableStandardItemLighting();
        try {
            RenderManager renderManager = Minecraft.getMinecraft().getRenderManager();
            Render entityClassRenderObject = renderManager.getEntityClassRenderObject(EntityPixelmon.class);
            RenderPixelmon rp = (RenderPixelmon)entityClassRenderObject;
            rp.renderPixelmon(e, 0.0, e.getYOffset(), 0.0, 0.0f, pt, true);
            renderManager.playerViewY = 180.0f;
        }
        catch (Exception exception) {
            // empty catch block
        }
        RenderHelper.disableStandardItemLighting();
        GlStateManager.disableDepth();
        GlStateManager.disableColorMaterial();
        GlStateManager.popMatrix();
    }

    public Optional<Float> getCorrectedWidth(EnumSpecies species) {
        Float width = null;
        if (Arrays.asList(new EnumSpecies[]{EnumSpecies.Lugia, EnumSpecies.Flygon, EnumSpecies.Rayquaza, EnumSpecies.Dialga, EnumSpecies.Regigigas, EnumSpecies.Giratina, EnumSpecies.Tyrantrum}).contains((Object)species)) {
            width = Float.valueOf(1.3f);
        } else if (Arrays.asList(new EnumSpecies[]{EnumSpecies.Lunatone, EnumSpecies.Solrock, EnumSpecies.Tropius, EnumSpecies.Groudon, EnumSpecies.Torterra, EnumSpecies.Rhyperior, EnumSpecies.Palkia, EnumSpecies.Arceus, EnumSpecies.Chandelure, EnumSpecies.Reshiram}).contains((Object)species)) {
            width = Float.valueOf(1.0f);
        } else if (species == EnumSpecies.Hooh || species == EnumSpecies.Kyogre || species == EnumSpecies.Dhelmise) {
            width = Float.valueOf(1.4f);
        } else if (species == EnumSpecies.Wailord) {
            width = Float.valueOf(2.5f);
        }
        return width != null ? Optional.of(width) : Optional.empty();
    }

    public static ResourceLocation typeToImage(EnumType type) {
        switch (type) {
            case Bug: {
                return GuiResources.pokedexTypeBug;
            }
            case Dark: {
                return GuiResources.pokedexTypeDark;
            }
            case Dragon: {
                return GuiResources.pokedexTypeDragon;
            }
            case Electric: {
                return GuiResources.pokedexTypeElectric;
            }
            case Fairy: {
                return GuiResources.pokedexTypeFairy;
            }
            case Fighting: {
                return GuiResources.pokedexTypeFighting;
            }
            case Fire: {
                return GuiResources.pokedexTypeFire;
            }
            case Flying: {
                return GuiResources.pokedexTypeFlying;
            }
            case Ghost: {
                return GuiResources.pokedexTypeGhost;
            }
            case Grass: {
                return GuiResources.pokedexTypeGrass;
            }
            case Ground: {
                return GuiResources.pokedexTypeGround;
            }
            case Ice: {
                return GuiResources.pokedexTypeIce;
            }
            case Normal: {
                return GuiResources.pokedexTypeNormal;
            }
            case Poison: {
                return GuiResources.pokedexTypePoison;
            }
            case Psychic: {
                return GuiResources.pokedexTypePsychic;
            }
            case Rock: {
                return GuiResources.pokedexTypeRock;
            }
            case Steel: {
                return GuiResources.pokedexTypeSteel;
            }
            case Water: {
                return GuiResources.pokedexTypeWater;
            }
        }
        return null;
    }

    private int toInt(double value) {
        return (int)value;
    }

    public void setPrevSearchText(String search) {
        this.prevSearchText = search;
    }
}

