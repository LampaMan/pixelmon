/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;

public class FontScaler {
    public static void scaleFont(FontRenderer f, String s, int x, int y, int color, double scale) {
        GlStateManager.scale(scale, scale, scale);
        f.drawString(s, x, y, color);
        double fixScale = 1.0 / scale;
        GlStateManager.scale(fixScale, fixScale, fixScale);
    }
}

