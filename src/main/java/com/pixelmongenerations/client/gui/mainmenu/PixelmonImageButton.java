/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.gui.mainmenu;

import com.pixelmongenerations.client.gui.GuiHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class PixelmonImageButton
extends GuiButton {
    private ResourceLocation resource;
    private float textScale;
    private int textX;
    private int textY;
    private int color;
    private boolean shadow;

    public PixelmonImageButton(int buttonId, int x, int y, int widthIn, int heightIn, ResourceLocation resource) {
        super(buttonId, x, y, widthIn, heightIn, "");
        this.resource = resource;
    }

    public PixelmonImageButton(int buttonId, int x, int y, int widthIn, int heightIn, ResourceLocation resource, String buttonText, float textScale, int color, boolean shadow) {
        super(buttonId, x, y, widthIn, heightIn, buttonText);
        this.resource = resource;
        this.textScale = textScale;
        this.color = color;
        this.shadow = shadow;
        FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
        this.textX = (this.x + (this.x + this.width)) / 2 - (int)((float)fontRenderer.getStringWidth(this.displayString) * textScale / 2.0f);
        this.textY = (this.y + (this.y + this.height)) / 2 - (int)((float)fontRenderer.FONT_HEIGHT * textScale / 2.0f);
    }

    @Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        this.hovered = mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;
        GL11.glPushMatrix();
        if (this.hovered) {
            GL11.glColor3f((float)0.85f, (float)0.85f, (float)0.85f);
        }
        mc.getTextureManager().bindTexture(this.resource);
        GuiHelper.drawImageQuad(this.x, this.y, this.width, this.height, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        GL11.glColor3f((float)1.0f, (float)1.0f, (float)1.0f);
        if (!this.displayString.isEmpty()) {
            GuiHelper.drawString(mc.fontRenderer, this.displayString, this.textX, this.textY, this.textScale, this.hovered ? 16770439 : this.color, this.shadow);
        }
        GL11.glColor3f((float)1.0f, (float)1.0f, (float)1.0f);
        GL11.glPopMatrix();
    }
}

