/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.apache.logging.log4j.LogManager
 *  org.apache.logging.log4j.Logger
 */
package com.pixelmongenerations.client.gui.mainmenu;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.mainmenu.PixelmonImageButton;
import java.io.File;
import java.net.URI;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiMultiplayer;
import net.minecraft.client.gui.GuiOptions;
import net.minecraft.client.gui.GuiWorldSelection;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.util.ResourceLocation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GuiPixelmonMainMenu
extends GuiMainMenu {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final ResourceLocation LOGO = new ResourceLocation("pixelmon:textures/gui/title/logo.png");
    private static final ResourceLocation BACKGROUND = new ResourceLocation("pixelmon:textures/gui/title/background.png");
    private static final ResourceLocation[] ICON_BUTTON_BACKGROUNDS = new ResourceLocation[]{new ResourceLocation("pixelmon:textures/gui/title/discord.png"), new ResourceLocation("pixelmon:textures/gui/title/settings.png"), new ResourceLocation("pixelmon:textures/gui/title/logs.png")};
    private static final ResourceLocation[] MAIN_BUTTON_BACKGROUNDS = new ResourceLocation[]{new ResourceLocation("pixelmon:textures/gui/title/singleplayer_button.png"), new ResourceLocation("pixelmon:textures/gui/title/multiplayer_button.png"), new ResourceLocation("pixelmon:textures/gui/title/website_button.png")};

    @Override
    public void initGui() {
        this.buttonList.clear();
        int logoSizeWidth = this.width / 4;
        int logoSizeHeight = this.height / 5;
        int buttonSizeWidth = 200;
        int buttonSizeHeight = 40;
        int buttonGap = this.height / 20;
        float textScale = 2.0f;
        for (int i = 0; i < 3; ++i) {
            String buttonText = i == 0 ? "Singleplayer" : (i == 1 ? "Multiplayer" : "Website");
            this.buttonList.add(new PixelmonImageButton(i, this.width / 2 - 100, buttonGap * 2 + logoSizeHeight + (40 + buttonGap) * i, 200, 40, MAIN_BUTTON_BACKGROUNDS[i], buttonText, 2.0f, 0xFFFFFF, true));
        }
        int iconButtonGap = 40;
        int mainButtonStartY = buttonGap * 2 + logoSizeHeight + (40 + buttonGap) * 0;
        int mainButtonEndY = buttonGap * 2 + logoSizeHeight + (40 + buttonGap) * 2 + 20;
        int iconStartY = (mainButtonStartY + mainButtonEndY) / 2 - iconButtonGap - 3;
        for (int i = 0; i < 3; ++i) {
            int multiplier = 2 - i;
            this.buttonList.add(new PixelmonImageButton(3 + i, this.width / 2 - 100 - (iconButtonGap + 7), iconStartY + i * iconButtonGap, 34, 28, ICON_BUTTON_BACKGROUNDS[i]));
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.mc.getTextureManager().bindTexture(BACKGROUND);
        GuiHelper.drawImageQuad(0.0, 0.0, this.width, this.height, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        int logoSizeWidth = this.width / 4;
        int logoSizeHeight = this.height / 5;
        GlStateManager.enableBlend();
        GlStateManager.blendFunc(770, 771);
        this.mc.getTextureManager().bindTexture(LOGO);
        GuiHelper.drawImageQuad(this.width / 2 - logoSizeWidth / 2, 5.0, logoSizeWidth, logoSizeHeight, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        GlStateManager.depthMask(true);
        GlStateManager.enableDepth();
        this.buttonList.forEach(button -> button.drawButton(this.mc, mouseX, mouseY, partialTicks));
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        switch (button.id) {
            case 0: {
                this.mc.displayGuiScreen(new GuiWorldSelection(this));
                break;
            }
            case 1: {
                this.mc.displayGuiScreen(new GuiMultiplayer(this));
                break;
            }
            case 2: {
                try {
                    Class<?> oclass = Class.forName("java.awt.Desktop");
                    Object object = oclass.getMethod("getDesktop", new Class[0]).invoke(null, new Object[0]);
                    oclass.getMethod("browse", URI.class).invoke(object, new URI("https://pixelmongenerations.com/"));
                }
                catch (Throwable throwable) {
                    LOGGER.error("Couldn't open link", throwable);
                }
                break;
            }
            case 3: {
                try {
                    Class<?> oclass = Class.forName("java.awt.Desktop");
                    Object object = oclass.getMethod("getDesktop", new Class[0]).invoke(null, new Object[0]);
                    oclass.getMethod("browse", URI.class).invoke(object, new URI("https://discord.gg/XzuPWrZHSG"));
                }
                catch (Throwable throwable) {
                    LOGGER.error("Couldn't open link", throwable);
                }
                break;
            }
            case 4: {
                this.mc.displayGuiScreen(new GuiOptions(this, this.mc.gameSettings));
                break;
            }
            case 5: {
                File file1 = new File(this.mc.gameDir, "/logs/");
                OpenGlHelper.openFile(file1);
                break;
            }
        }
    }
}

