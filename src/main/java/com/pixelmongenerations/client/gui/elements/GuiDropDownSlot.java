/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.elements;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.elements.GuiDropDown;
import com.pixelmongenerations.client.gui.elements.GuiSlotBase;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.Tessellator;

class GuiDropDownSlot<T>
extends GuiSlotBase {
    private GuiDropDown<T> dropDown;

    public GuiDropDownSlot(GuiDropDown<T> dropDown, int top, int left, int width, int height) {
        super(top, left, width, height, true);
        this.dropDown = dropDown;
    }

    @Override
    protected int getSize() {
        return this.dropDown.options.size();
    }

    @Override
    protected void elementClicked(int index, boolean doubleClicked) {
        this.dropDown.elementClicked(index);
    }

    @Override
    protected boolean isSelected(int element) {
        return element == this.dropDown.selectedIndex;
    }

    @Override
    protected void drawSlot(int index, int x, int yTop, int yMiddle, Tessellator tessellator) {
        this.drawOptionString(index, yTop);
    }

    void drawOptionString(int index, int y) {
        if (index < 0 || index >= this.dropDown.options.size()) {
            return;
        }
        this.drawOptionString(this.dropDown.options.get(index), y);
    }

    void drawOptionString(T option, int y) {
        if (option == null) {
            return;
        }
        String optionString = this.dropDown.toOptionString(option);
        optionString = GuiHelper.getLimitedString(optionString, this.width);
        int color = 0;
        switch (this.dropDown.align) {
            case Left: {
                Minecraft.getMinecraft().fontRenderer.drawString(optionString, this.left + 2, y, color);
                break;
            }
            case Center: {
                GuiHelper.drawCenteredString(optionString, this.left + this.width / 2, y, color);
                break;
            }
            case Right: {
                GuiHelper.drawStringRightAligned(optionString, this.left + this.width, y, color);
            }
        }
    }

    @Override
    protected void drawBackground() {
        this.drawBackgroundRect(this.top + 3, this.top + this.height, -5000269);
    }

    void drawBackgroundRect(int top, int bottom, int color) {
        Gui.drawRect(this.left, top, this.left + this.width, bottom, color);
    }

    @Override
    protected float[] get1Color() {
        return new float[]{1.0f, 1.0f, 1.0f};
    }

    @Override
    protected int[] getSelectionColor() {
        return new int[]{128, 128, 128};
    }

    @Override
    protected int[] get255Color() {
        return null;
    }
}

