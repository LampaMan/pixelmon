/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.elements;

import com.pixelmongenerations.client.gui.elements.GuiSlotBase;
import com.pixelmongenerations.client.gui.elements.GuiTextFieldMultipleLine;
import net.minecraft.client.renderer.Tessellator;

class GuiTextScrollBar
extends GuiSlotBase {
    private GuiTextFieldMultipleLine textField;

    GuiTextScrollBar(GuiTextFieldMultipleLine textField) {
        super(textField.y - 2, textField.x, textField.width, textField.height, true);
        this.textField = textField;
        this.slotHeight = textField.getLineHeight();
    }

    @Override
    protected int getSize() {
        return this.textField.splitString.size();
    }

    @Override
    protected void elementClicked(int index, boolean doubleClicked) {
    }

    @Override
    protected boolean isSelected(int element) {
        return false;
    }

    @Override
    protected void drawSlot(int index, int x, int yTop, int yMiddle, Tessellator tessellator) {
    }

    @Override
    protected float[] get1Color() {
        return new float[]{0.0f, 0.0f, 0.0f};
    }

    @Override
    protected int[] getSelectionColor() {
        return null;
    }

    @Override
    protected int[] get255Color() {
        return null;
    }
}

