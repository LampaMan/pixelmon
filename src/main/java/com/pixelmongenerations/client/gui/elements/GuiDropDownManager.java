/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Mouse
 */
package com.pixelmongenerations.client.gui.elements;

import com.pixelmongenerations.client.gui.elements.GuiDropDown;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import org.lwjgl.input.Mouse;

public class GuiDropDownManager {
    private final List<GuiDropDown> dropDowns = new CopyOnWriteArrayList<GuiDropDown>();
    private boolean lastMouse;

    public void drawDropDowns(float partialTicks, int mouseX, int mouseY) {
        GuiDropDown active = null;
        int tempMouseX = mouseX;
        int tempMouseY = mouseY;
        for (GuiDropDown dropDown : this.dropDowns) {
            if (!dropDown.active) continue;
            active = dropDown;
            if (!active.isMouseOver(mouseX, mouseY)) break;
            mouseY = -1;
            mouseX = -1;
            break;
        }
        for (GuiDropDown down : this.dropDowns) {
            if (down.active) continue;
            down.drawScreen(mouseX, mouseY, partialTicks);
        }
        if (active != null) {
            active.drawScreen(tempMouseX, tempMouseY, partialTicks);
        }
        this.lastMouse = Mouse.isButtonDown((int)0);
    }

    public boolean mouseClicked(int mouseX, int mouseY, int mouseButton) {
        boolean selected = false;
        for (GuiDropDown dropDown : this.dropDowns) {
            if (dropDown.isMouseOver(mouseX, mouseY)) {
                selected = true;
            } else {
                dropDown.active = false;
            }
            selected = selected || dropDown.getLastSelected();
        }
        return selected;
    }

    public void addDropDown(GuiDropDown dropDown) {
        dropDown.setManager(this);
        this.dropDowns.add(dropDown);
    }

    public void removeDropDown(GuiDropDown dropDown) {
        this.dropDowns.remove(dropDown);
    }

    public void clearDropDowns() {
        this.dropDowns.clear();
    }

    public boolean isMouseOver(int mouseX, int mouseY) {
        return this.dropDowns.stream().anyMatch(dropDown -> dropDown.isMouseOver(mouseX, mouseY));
    }

    boolean getLastMouse() {
        return this.lastMouse;
    }
}

