/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.elements;

import com.pixelmongenerations.client.gui.elements.GuiDropDown;
import com.pixelmongenerations.client.gui.elements.GuiDropDownManager;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import java.io.IOException;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;

public abstract class GuiContainerDropDown
extends GuiContainer {
    private final GuiDropDownManager dropDownManager = new GuiDropDownManager();

    protected GuiContainerDropDown() {
        super(new ContainerEmpty());
    }

    @Override
    public void initGui() {
        super.initGui();
        this.dropDownManager.clearDropDowns();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        super.drawGuiContainerForegroundLayer(mouseX, mouseY);
        if (this.disableMenus()) {
            mouseY = -1;
            mouseX = -1;
        }
        GlStateManager.pushMatrix();
        GlStateManager.translate(-this.guiLeft, -this.guiTop, 0.0f);
        this.dropDownManager.drawDropDowns(0.0f, mouseX, mouseY);
        GlStateManager.popMatrix();
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        if (this.dropDownManager.isMouseOver(mouseX, mouseY)) {
            mouseY = -1;
            mouseX = -1;
        }
        this.drawBackgroundUnderMenus(partialTicks, mouseX, mouseY);
    }

    protected abstract void drawBackgroundUnderMenus(float var1, int var2, int var3);

    @Override
    protected final void mouseClicked(int mouseX, int mouseY, int button) throws IOException {
        if (this.dropDownManager.mouseClicked(mouseX, mouseY, button)) {
            return;
        }
        super.mouseClicked(mouseX, mouseY, button);
        this.mouseClickedUnderMenus(mouseX, mouseY, button);
    }

    protected void mouseClickedUnderMenus(int mouseX, int mouseY, int button) throws IOException {
    }

    public void addDropDown(GuiDropDown dropDown) {
        this.dropDownManager.addDropDown(dropDown);
    }

    public void removeDropDown(GuiDropDown dropDown) {
        this.dropDownManager.removeDropDown(dropDown);
    }

    protected boolean disableMenus() {
        return false;
    }
}

