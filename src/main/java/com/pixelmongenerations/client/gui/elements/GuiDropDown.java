/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Mouse
 */
package com.pixelmongenerations.client.gui.elements;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.elements.EnumTextAlign;
import com.pixelmongenerations.client.gui.elements.GuiDropDownManager;
import com.pixelmongenerations.client.gui.elements.GuiDropDownSlot;
import java.awt.Rectangle;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import org.lwjgl.input.Mouse;

public class GuiDropDown<T> {
    List<T> options;
    int selectedIndex = -1;
    T selectedOption;
    Consumer<T> onSelected;
    Consumer<Integer> onSelectedIndex;
    Function<T, String> getOptionString;
    private GuiDropDownSlot slot;
    boolean active;
    private boolean lastSelected;
    private int top;
    private Rectangle rect;
    private Rectangle iconRect;
    EnumTextAlign align = EnumTextAlign.Left;
    private GuiDropDownManager manager;
    private static final int ICON_LENGTH = 11;
    public static final int OPTION_HEIGHT = 10;

    public GuiDropDown(List<T> options, int selected, int left, int top, int width, int height) {
        this(options, left, top, width, height);
        setOptions(options, selected);
    }

    public GuiDropDown(List<T> options, T selected, int left, int top, int width, int height) {
        this(options, left, top, width, height);
        setOptions(options, selected);
    }

    private GuiDropDown(List<T> options, int left, int top, int width, int height) {
        height = Math.min(height, 10 * options.size() + 4);
        this.top = top;
        this.slot = new GuiDropDownSlot(this, top - 3, left, width, height);
        int rectTop = top + 1;
        this.rect = new Rectangle(left, rectTop, width, 10);
        this.iconRect = new Rectangle(left + width, rectTop, 11, 11);
    }

    void setManager(GuiDropDownManager manager) {
        this.manager = manager;
    }

    private void setOptions(List<T> options, T selected) {
        this.options = options;
        this.setSelected(selected);
    }

    private void setOptions(List<T> options, int selected) {
        this.options = options;
        this.setSelected(selected);
    }

    public void setSelected(T selected) {
        this.selectedOption = selected;
        for (int i = 0; i < this.options.size(); ++i) {
            if (!this.options.get(i).equals(selected)) continue;
            this.selectedIndex = i;
            this.slot.scrollTo(i);
            break;
        }
    }

    public void setSelected(int selected) {
        this.selectedIndex = selected;
        this.slot.scrollTo(selected);
        this.selectedOption = this.selectedIndex >= 0 && this.selectedIndex < this.options.size() ? this.options.get(selected) : null;
    }

    public GuiDropDown<T> setInactiveTop(int top) {
        int rectTop;
        this.top = top;
        this.rect.y = rectTop = top + 1;
        this.iconRect.y = rectTop;
        return this;
    }

    public GuiDropDown<T> setTextAlign(EnumTextAlign align) {
        this.align = align;
        return this;
    }

    public GuiDropDown<T> setGetOptionString(Function<T, String> getOptionString) {
        this.getOptionString = getOptionString;
        return this;
    }

    public GuiDropDown<T> setOnSelected(Consumer<T> onSelected) {
        this.onSelected = onSelected;
        return this;
    }

    public GuiDropDown<T> setOnSelectedIndex(Consumer<Integer> onSelectedIndex) {
        this.onSelectedIndex = onSelectedIndex;
        return this;
    }

    public GuiDropDown<T> setOrdered() {
        this.options.sort(Comparator.comparing(this::toOptionString));
        if (this.selectedIndex >= 0) {
            for (int i = 0; i < this.options.size(); ++i) {
                if (this.options.get(i) != this.selectedOption) continue;
                this.setSelected(i);
                break;
            }
        }
        return this;
    }

    public T getSelected() {
        return this.selectedOption;
    }

    void mouseClicked(int mouseX, int mouseY) {
    }

    void elementClicked(int index) {
        if (this.manager.getLastMouse()) {
            return;
        }
        if (this.active) {
            this.lastSelected = true;
            this.selectedIndex = index;
            this.selectedOption = this.options.get(index);
            if (this.onSelected != null) {
                this.onSelected.accept(this.selectedOption);
            }
            if (this.onSelectedIndex != null) {
                this.onSelectedIndex.accept(index);
            }
            this.active = false;
        } else {
            this.active = true;
        }
    }

    String toOptionString(T option) {
        return this.getOptionString == null ? option.toString() : this.getOptionString.apply(option);
    }

    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        GlStateManager.pushMatrix();
        GlStateManager.disableLighting();
        float gray = this.active ? 0.6f : 1.0f;
        GlStateManager.color(gray, gray, gray, 1.0f);
        Minecraft.getMinecraft().renderEngine.bindTexture(GuiResources.dropDownIcon);
        GuiHelper.drawImageQuad(this.slot.left + this.slot.width, this.top, 11.0, 11.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
        GlStateManager.popMatrix();
        boolean mouseDown = Mouse.isButtonDown((int)0);
        boolean lastMouse = this.manager.getLastMouse();
        boolean mouseFirstPressed = mouseDown && !lastMouse;
        boolean bl = this.lastSelected = this.lastSelected && lastMouse;
        if (this.active) {
            this.slot.drawScreen(mouseX, mouseY, partialTicks);
            if (mouseFirstPressed && this.iconRect.contains(mouseX, mouseY) && (!this.slot.hasScrollBar() || !this.slot.inBoundsScroll(mouseX, mouseY))) {
                this.active = false;
            }
        } else {
            int slotTop = this.top + 1;
            this.slot.drawBackgroundRect(this.top, slotTop + 10, -1644826);
            if (this.isMouseOver(mouseX, mouseY)) {
                this.slot.drawSelection(0, 0, 6, this.top + 1);
                if (mouseFirstPressed) {
                    this.active = true;
                }
            }
            this.slot.drawOptionString(this.selectedOption, slotTop);
        }
    }

    public boolean isMouseOver(int mouseX, int mouseY) {
        if (this.iconRect.contains(mouseX, mouseY)) {
            return true;
        }
        if (this.active) {
            return this.slot.hasScrollBar() ? this.slot.inBoundsScroll(mouseX, mouseY) : this.slot.inBounds(mouseX, mouseY);
        }
        return this.rect.contains(mouseX, mouseY);
    }

    public boolean getLastSelected() {
        return this.lastSelected;
    }

    public int getTop() {
        return this.top;
    }

    public int getSelectedIndex() {
        return this.selectedIndex;
    }
}

