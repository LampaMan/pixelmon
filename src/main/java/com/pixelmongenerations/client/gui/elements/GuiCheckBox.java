/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.elements;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;

public class GuiCheckBox
extends GuiButton {
    protected ResourceLocation resourceLocation = GuiResources.checkBox;
    protected boolean stateTriggered;
    protected double xTexStart = 0.0;
    protected double yTexStart = 0.0;
    protected double xDiffTex = 0.5;
    protected double yDiffTex = 0.5;

    public GuiCheckBox(int buttonId, int xIn, int yIn, int widthIn, int heightIn, boolean buttonText) {
        super(buttonId, xIn, yIn, widthIn, heightIn, "");
        this.stateTriggered = buttonText;
    }

    public void setStateTriggered(boolean p_191753_1_) {
        this.stateTriggered = p_191753_1_;
    }

    public boolean isStateTriggered() {
        return this.stateTriggered;
    }

    public void setPosition(int p_191752_1_, int p_191752_2_) {
        this.x = p_191752_1_;
        this.y = p_191752_2_;
    }

    @Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        if (this.visible) {
            this.hovered = mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;
            mc.getTextureManager().bindTexture(this.resourceLocation);
            GlStateManager.disableDepth();
            double i = this.xTexStart;
            double j = this.yTexStart;
            if (this.stateTriggered) {
                i += this.xDiffTex;
            }
            if (this.hovered) {
                j += this.yDiffTex;
            }
            GuiHelper.drawImageQuad(this.x, this.y, this.width, this.height, i, j, i + 0.5, j + 0.5, this.zLevel);
            GlStateManager.enableDepth();
        }
    }
}

