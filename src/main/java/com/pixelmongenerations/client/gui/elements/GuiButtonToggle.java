/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.elements;

import java.util.function.Consumer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.text.translation.I18n;

public class GuiButtonToggle
extends GuiButton {
    private final String trueText;
    private final String falseText;
    private boolean on;
    private final Consumer<Boolean> callback;

    public GuiButtonToggle(int buttonID, int x, int y, int width, int height, String trueText, String falseText, boolean on) {
        this(buttonID, x, y, width, height, trueText, falseText, on, a -> {});
    }

    public GuiButtonToggle(int buttonID, int x, int y, int width, int height, String trueText, String falseText, boolean on, Consumer<Boolean> callback) {
        super(buttonID, x, y, width, height, "");
        this.trueText = trueText;
        this.falseText = falseText;
        this.on = on;
        this.callback = callback;
        this.updateText();
    }

    public boolean toggle() {
        this.setOn(!this.on);
        return this.on;
    }

    private void updateText() {
        this.displayString = I18n.translateToLocal(this.on ? this.trueText : this.falseText);
    }

    public boolean isOn() {
        return this.on;
    }

    public void setOn(boolean on) {
        this.on = on;
        this.callback.accept(on);
        this.updateText();
    }
}

