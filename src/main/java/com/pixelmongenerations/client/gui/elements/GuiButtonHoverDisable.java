/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.elements;

import java.util.List;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;

public class GuiButtonHoverDisable
extends GuiButton {
    private boolean hoverDisabled;

    public GuiButtonHoverDisable(int buttonId, int x, int y, String buttonText) {
        super(buttonId, x, y, buttonText);
    }

    public GuiButtonHoverDisable(int buttonId, int x, int y, int widthIn, int heightIn, String buttonText) {
        super(buttonId, x, y, widthIn, heightIn, buttonText);
    }

    public void setHoverDisabled(boolean hoverDisabled) {
        this.hoverDisabled = hoverDisabled;
    }

    @Override
    protected int getHoverState(boolean mouseOver) {
        int hoverState = super.getHoverState(mouseOver);
        if (hoverState == 2 && this.hoverDisabled) {
            hoverState = 1;
        }
        return hoverState;
    }

    @Override
    public void drawCenteredString(FontRenderer fontRendererIn, String text, int x, int y, int color) {
        if (this.hoverDisabled) {
            color = 0xE0E0E0;
            if (this.packedFGColour != 0) {
                color = this.packedFGColour;
            } else if (!this.enabled) {
                color = 0xA0A0A0;
            }
        }
        super.drawCenteredString(fontRendererIn, text, x, y, color);
    }

    public static void setHoverDisabledScreen(List<GuiButton> buttonList, boolean hoverDisabled) {
        for (GuiButton button : buttonList) {
            if (!(button instanceof GuiButtonHoverDisable)) continue;
            ((GuiButtonHoverDisable)button).setHoverDisabled(hoverDisabled);
        }
    }
}

