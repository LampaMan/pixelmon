/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 *  org.lwjgl.input.Keyboard
 *  org.lwjgl.input.Mouse
 */
package com.pixelmongenerations.client.gui.elements;

import com.google.common.collect.Lists;
import java.io.IOException;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ChatLine;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiNewChat;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.play.client.CPacketTabComplete;
import net.minecraft.util.StringUtils;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.client.ClientCommandHandler;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

public class GuiChatExtension {
    private int bPosX = 0;
    private int bPosY = 0;
    private int bWidth = 0;
    private int yOffset;
    public static boolean chatOpen = false;
    private int sentHistoryCursor = -1;
    protected GuiTextField inputField;
    private boolean field_73897_d;
    private int field_73903_n;
    private ArrayList<String> onlineNames = Lists.newArrayList();
    private String field_73898_b;
    private Minecraft mc = Minecraft.getMinecraft();
    private GuiScreen screen;

    public GuiChatExtension(GuiScreen screen, int yOffset) {
        this.screen = screen;
        this.yOffset = yOffset;
    }

    public void handleKeyboardInput() {
        if (Keyboard.getEventKeyState()) {
            int i = Keyboard.getEventKey();
            char c0 = Keyboard.getEventCharacter();
            this.keyTyped(c0, i);
        }
    }

    public void initGui() {
        this.bPosX = 2;
        this.bPosY = this.screen.height - this.yOffset;
        this.bWidth = this.screen.width - 124;
        chatOpen = false;
        Keyboard.enableRepeatEvents((boolean)true);
        this.sentHistoryCursor = this.mc.ingameGUI.getChatGUI().getSentMessages().size();
        this.inputField = new GuiTextField(0, this.mc.fontRenderer, this.bPosX + 2, this.bPosY + 2, this.bWidth - 2, this.screen.height - 64);
        this.inputField.setMaxStringLength(100);
        this.inputField.setEnableBackgroundDrawing(false);
        this.inputField.setFocused(true);
        this.inputField.setText("");
        this.inputField.setCanLoseFocus(false);
        this.inputField.setVisible(false);
    }

    public void onGuiClosed() {
        Keyboard.enableRepeatEvents((boolean)false);
        this.mc.ingameGUI.getChatGUI().resetScroll();
        chatOpen = false;
    }

    public void updateScreen() {
        if (this.inputField != null) {
            this.inputField.updateCursorCounter();
        }
    }

    public void drawScreen(int par1, int par2, float par3) {
        if (chatOpen) {
            Gui.drawRect(2, this.screen.height - this.yOffset, this.screen.width - 122, this.screen.height - this.yOffset + 12, Integer.MIN_VALUE);
            this.inputField.drawTextBox();
        }
        if (this.mc == null || this.mc.ingameGUI == null) {
            return;
        }
        GuiNewChat guiNewChat = this.mc.ingameGUI.getChatGUI();
        int j = this.getLineCount();
        int k = 0;
        int l = guiNewChat.drawnChatLines.size();
        float f = this.mc.gameSettings.chatOpacity * 0.9f + 0.1f;
        int updateCounter = this.mc.ingameGUI.getUpdateCounter();
        if (l > 0) {
            int l1;
            int k1;
            int j1;
            float f1 = this.getChatScale();
            int i1 = MathHelper.ceil((float)this.getChatWidth() / f1);
            GlStateManager.pushMatrix();
            if (chatOpen) {
                GlStateManager.translate(2.0f, this.bPosY - 3, 0.0f);
            } else {
                GlStateManager.translate(2.0f, this.bPosY + 10, 0.0f);
            }
            GlStateManager.scale(f1, f1, 1.0f);
            for (j1 = 0; j1 + guiNewChat.scrollPos < guiNewChat.drawnChatLines.size() && j1 < j; ++j1) {
                ChatLine chatline = guiNewChat.drawnChatLines.get(j1 + guiNewChat.scrollPos);
                if (chatline == null || (k1 = updateCounter - chatline.getUpdatedCounter()) >= 200 && !chatOpen) continue;
                double d0 = (double)k1 / 200.0;
                d0 = 1.0 - d0;
                d0 *= 10.0;
                if (d0 < 0.0) {
                    d0 = 0.0;
                }
                if (d0 > 1.0) {
                    d0 = 1.0;
                }
                d0 *= d0;
                l1 = (int)(255.0 * d0);
                if (chatOpen) {
                    l1 = 255;
                }
                l1 = (int)((float)l1 * f);
                ++k;
                if (l1 <= 3) continue;
                int b0 = 0;
                int i2 = -j1 * 9;
                Gui.drawRect(b0, i2 - 9, b0 + i1 + 4, i2, l1 / 2 << 24);
                String s = chatline.getChatComponent().getFormattedText();
                if (!this.mc.gameSettings.chatColours) {
                    s = StringUtils.stripControlCodes(s);
                }
                GlStateManager.enableBlend();
                this.mc.fontRenderer.drawStringWithShadow(s, b0, i2 - 8, 0xFFFFFF + (l1 << 24));
                GlStateManager.disableAlpha();
                GlStateManager.disableBlend();
            }
            if (chatOpen) {
                j1 = this.mc.fontRenderer.FONT_HEIGHT;
                GlStateManager.translate(-3.0f, 0.0f, 0.0f);
                int j2 = l * j1 + l;
                k1 = k * j1 + k;
                int k2 = guiNewChat.scrollPos * k1 / l;
                int l2 = k1 * k1 / j2;
                if (j2 != k1) {
                    l1 = k2 > 0 ? 170 : 96;
                    int i3 = guiNewChat.isScrolled ? 0xCC3333 : 0x3333AA;
                    Gui.drawRect(0, -k2, 2, -k2 - l2, i3 + (l1 << 24));
                    Gui.drawRect(2, -k2, 1, -k2 - l2, 0xCCCCCC + (l1 << 24));
                }
            }
            GlStateManager.popMatrix();
        }
    }

    private void chatOpened(String start) {
        if (this.mc.gameSettings.chatVisibility == EntityPlayer.EnumChatVisibility.HIDDEN || this.inputField == null) {
            return;
        }
        if (start != null) {
            this.inputField.setText(start);
        } else {
            this.inputField.setText("");
        }
        chatOpen = true;
        this.inputField.setVisible(true);
    }

    private void chatClosed() {
        chatOpen = false;
        this.inputField.setVisible(false);
        this.inputField.setText("");
    }

    public void keyTyped(char par1, int par2) {
        if (!(par2 != this.mc.gameSettings.keyBindChat.getKeyCode() && par2 != 53 || chatOpen)) {
            this.chatOpened(par2 == 53 ? "/" : null);
            return;
        }
        if (par2 == 1 && chatOpen) {
            this.chatClosed();
            return;
        }
        if (!chatOpen) {
            return;
        }
        if (par2 == 15) {
            this.completePlayerName();
        } else {
            this.field_73897_d = false;
        }
        if (par2 != 28 && par2 != 156) {
            if (par2 == 200) {
                this.getSentHistory(-1);
            } else if (par2 == 208) {
                this.getSentHistory(1);
            } else if (par2 == 201) {
                this.mc.ingameGUI.getChatGUI().scroll(1);
            } else if (par2 == 209) {
                this.mc.ingameGUI.getChatGUI().scroll(-1);
            } else {
                this.inputField.textboxKeyTyped(par1, par2);
            }
        } else {
            String s = this.inputField.getText().trim();
            if (!s.isEmpty()) {
                this.mc.ingameGUI.getChatGUI().addToSentMessages(s);
                if (ClientCommandHandler.instance.executeCommand(this.mc.player, s) != 1) {
                    this.mc.player.sendChatMessage(s);
                }
            }
            this.inputField.setText("");
            this.chatClosed();
        }
    }

    public void handleMouseInput() throws IOException {
        int i = Mouse.getEventDWheel();
        if (i != 0) {
            if (i > 1) {
                i = 1;
            }
            if (i < -1) {
                i = -1;
            }
            if (!GuiScreen.isShiftKeyDown()) {
                i *= 7;
            }
            this.mc.ingameGUI.getChatGUI().scroll(i);
        }
    }

    private void completePlayerName() {
        if (this.field_73897_d) {
            this.inputField.deleteFromCursor(this.inputField.getNthWordFromPosWS(-1, this.inputField.getCursorPosition(), false) - this.inputField.getCursorPosition());
            if (this.field_73903_n >= this.onlineNames.size()) {
                this.field_73903_n = 0;
            }
        } else {
            int i = this.inputField.getNthWordFromPosWS(-1, this.inputField.getCursorPosition(), false);
            this.onlineNames.clear();
            this.field_73903_n = 0;
            String s = this.inputField.getText().substring(0, this.inputField.getCursorPosition());
            this.autoComplete(s);
            if (this.onlineNames.isEmpty()) {
                return;
            }
            this.field_73897_d = true;
            this.inputField.deleteFromCursor(i - this.inputField.getCursorPosition());
        }
        if (this.onlineNames.size() > 1) {
            StringBuilder stringBuilder = new StringBuilder();
            for (String s : this.onlineNames) {
                if (stringBuilder.length() > 0) {
                    stringBuilder.append(", ");
                }
                stringBuilder.append(s);
            }
            this.mc.ingameGUI.getChatGUI().printChatMessageWithOptionalDeletion(new TextComponentString(stringBuilder.toString()), 1);
        }
        this.inputField.writeText(TextFormatting.getTextWithoutFormattingCodes(this.onlineNames.get(this.field_73903_n++)));
    }

    private void autoComplete(String par1Str) {
        if (par1Str.length() >= 1) {
            ClientCommandHandler.instance.autoComplete(par1Str);
            this.mc.player.connection.sendPacket(new CPacketTabComplete(par1Str, null, false));
        }
    }

    private void getSentHistory(int par1) {
        int j = this.sentHistoryCursor + par1;
        int k = this.mc.ingameGUI.getChatGUI().getSentMessages().size();
        if (j < 0) {
            j = 0;
        }
        if (j > k) {
            j = k;
        }
        if (j != this.sentHistoryCursor) {
            try {
                if (j == k) {
                    this.sentHistoryCursor = k;
                    this.inputField.setText(this.field_73898_b);
                } else {
                    if (this.sentHistoryCursor == k) {
                        this.field_73898_b = this.inputField.getText();
                    }
                    this.inputField.setText(this.mc.ingameGUI.getChatGUI().getSentMessages().get(j));
                    this.sentHistoryCursor = j;
                }
            }
            catch (NullPointerException nullPointerException) {
                // empty catch block
            }
        }
    }

    public void mouseClicked(int par1, int par2, int par3) throws IOException {
        this.inputField.mouseClicked(par1, par2, par3);
    }

    private int getChatWidth() {
        return GuiChatExtension.calculateChatboxWidth(this.mc.gameSettings.chatWidth);
    }

    private int getChatHeight() {
        return GuiChatExtension.calculateChatboxHeight(this.mc.gameSettings.chatHeightFocused);
    }

    private float getChatScale() {
        return this.mc.gameSettings.chatScale;
    }

    private static int calculateChatboxWidth(float p_146233_0_) {
        int short1 = 320;
        int b0 = 40;
        return MathHelper.floor(p_146233_0_ * (float)(short1 - b0) + (float)b0);
    }

    private static int calculateChatboxHeight(float p_146243_0_) {
        int short1 = 180;
        int b0 = 20;
        return MathHelper.floor(p_146243_0_ * (float)(short1 - b0) + (float)b0);
    }

    private int getLineCount() {
        return this.getChatHeight() / 9;
    }
}

