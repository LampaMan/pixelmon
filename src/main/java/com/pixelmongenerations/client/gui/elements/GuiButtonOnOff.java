/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.elements;

import com.pixelmongenerations.client.gui.elements.GuiButtonToggle;

public class GuiButtonOnOff
extends GuiButtonToggle {
    public GuiButtonOnOff(int buttonID, int x, int y, int width, int height, boolean on) {
        super(buttonID, x, y, width, height, "gui.battlerules.on", "gui.battlerules.off", on);
    }
}

