/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Keyboard
 */
package com.pixelmongenerations.client.gui.npcEditor;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.elements.GuiContainerDropDown;
import com.pixelmongenerations.client.gui.npcEditor.TextureEditorNPC;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCChatting;
import com.pixelmongenerations.common.entity.npcs.registry.ClientNPCData;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.npc.DeleteNPC;
import com.pixelmongenerations.core.network.packetHandlers.npc.EnumNPCServerPacketType;
import com.pixelmongenerations.core.network.packetHandlers.npc.NPCServerPacket;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.input.Keyboard;

public class GuiChattingNPCEditor
extends GuiContainerDropDown {
    public static int currentNPCID;
    public static ArrayList<String> chatPages;
    public static boolean chatChanged;
    public static String name;
    NPCChatting npc;
    GuiTextField nameBox;
    GuiTextField page1Box;
    GuiTextField page2Box;
    GuiTextField page3Box;
    GuiTextField page4Box;
    public static List<ClientNPCData> npcData;
    String oldName;
    private TextureEditorNPC textureEditor;

    public GuiChattingNPCEditor(int npcID) {
        Keyboard.enableRepeatEvents((boolean)true);
        Optional<NPCChatting> npcChattingOptional = EntityNPC.locateNPCClient(Minecraft.getMinecraft().world, npcID, NPCChatting.class);
        if (!npcChattingOptional.isPresent()) {
            GuiHelper.closeScreen();
        } else {
            this.npc = npcChattingOptional.get();
            currentNPCID = npcID;
            if (this.npc == null) {
                GuiHelper.closeScreen();
            }
        }
    }

    @Override
    public void initGui() {
        super.initGui();
        this.buttonList.add(new GuiButton(1, this.width / 2 + 155, this.height / 2 + 90, 30, 20, I18n.translateToLocal("gui.guiItemDrops.ok")));
        this.buttonList.add(new GuiButton(6, this.width / 2 + 100, this.height / 2 - 120, 80, 20, I18n.translateToLocal("gui.npceditor.despawn")));
        this.nameBox = new GuiTextField(12, this.mc.fontRenderer, this.width / 2 - 20, this.height / 2 - 96, 180, 20);
        this.nameBox.setText(name);
        this.textureEditor = new TextureEditorNPC(this, this.npc, this.width / 2 - 190, this.height / 2 + 78, 130, -23);
        this.page1Box = new GuiTextField(2, this.mc.fontRenderer, this.width / 2 - 80, this.height / 2 - 50, 260, 20);
        this.page1Box.setMaxStringLength(128);
        if (!chatPages.isEmpty()) {
            this.page1Box.setText(chatPages.get(0));
        }
        this.page2Box = new GuiTextField(3, this.mc.fontRenderer, this.width / 2 - 80, this.height / 2 - 15, 260, 20);
        this.page2Box.setMaxStringLength(128);
        if (chatPages.size() > 1) {
            this.page2Box.setText(chatPages.get(1));
        }
        this.page3Box = new GuiTextField(4, this.mc.fontRenderer, this.width / 2 - 80, this.height / 2 + 20, 260, 20);
        this.page3Box.setMaxStringLength(128);
        if (chatPages.size() > 2) {
            this.page3Box.setText(chatPages.get(2));
        }
        this.page4Box = new GuiTextField(5, this.mc.fontRenderer, this.width / 2 - 80, this.height / 2 + 55, 260, 20);
        this.page4Box.setMaxStringLength(128);
        if (chatPages.size() > 3) {
            this.page4Box.setText(chatPages.get(3));
        }
        chatChanged = false;
        this.oldName = name;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        if (!this.oldName.equals(name)) {
            this.oldName = name;
            this.nameBox.setText(name);
        }
        if (chatChanged) {
            chatChanged = false;
            if (!chatPages.isEmpty()) {
                this.page1Box.setText(chatPages.get(0));
            }
            if (chatPages.size() > 1) {
                this.page2Box.setText(chatPages.get(1));
            }
            if (chatPages.size() > 2) {
                this.page3Box.setText(chatPages.get(2));
            }
            if (chatPages.size() > 3) {
                this.page4Box.setText(chatPages.get(3));
            }
        }
        this.mc.renderEngine.bindTexture(GuiResources.cwPanel);
        GuiHelper.drawImageQuad(this.width / 2 - 200, this.height / 2 - 120, 400.0, 280.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        RenderHelper.disableStandardItemLighting();
        GuiHelper.drawEntity(this.npc, this.width / 2 - 140, this.height / 2 + 50, 60.0f, 0.0f, 0.0f);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.name"), this.width / 2 - 60, this.height / 2 - 90, 0);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.npceditor.page1"), this.width / 2 - 80, this.height / 2 - 61, 0);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.npceditor.page2"), this.width / 2 - 80, this.height / 2 - 26, 0);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.npceditor.page3"), this.width / 2 - 80, this.height / 2 + 9, 0);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.npceditor.page4"), this.width / 2 - 80, this.height / 2 + 44, 0);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.model"), this.width / 2 - 180, this.height / 2 + 65, 0);
        this.nameBox.drawTextBox();
        this.page1Box.drawTextBox();
        this.page2Box.drawTextBox();
        this.page3Box.drawTextBox();
        this.page4Box.drawTextBox();
        this.textureEditor.drawCustomTextBox();
    }

    @Override
    protected void drawBackgroundUnderMenus(float f, int i, int j) {
    }

    @Override
    protected void keyTyped(char key, int par2) {
        this.nameBox.textboxKeyTyped(key, par2);
        this.page1Box.textboxKeyTyped(key, par2);
        this.page2Box.textboxKeyTyped(key, par2);
        this.page3Box.textboxKeyTyped(key, par2);
        this.page4Box.textboxKeyTyped(key, par2);
        GuiHelper.switchFocus(par2, this.nameBox, this.page1Box, this.page2Box, this.page3Box, this.page4Box);
        if (par2 == 1 || par2 == 28) {
            this.saveFields();
        }
        this.textureEditor.keyTyped(key, par2, this.nameBox, this.page1Box, this.page2Box, this.page3Box, this.page4Box);
    }

    @Override
    protected void mouseClickedUnderMenus(int x, int y, int z) throws IOException {
        this.nameBox.mouseClicked(x, y, z);
        this.page1Box.mouseClicked(x, y, z);
        this.page2Box.mouseClicked(x, y, z);
        this.page3Box.mouseClicked(x, y, z);
        this.page4Box.mouseClicked(x, y, z);
        this.textureEditor.mouseClicked(x, y, z);
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        if (button.enabled) {
            if (button.id == 1) {
                this.saveFields();
            } else if (button.id == 6) {
                Pixelmon.NETWORK.sendToServer(new DeleteNPC(currentNPCID));
                GuiHelper.closeScreen();
            }
        }
    }

    private void saveFields() {
        this.textureEditor.saveCustomTexture();
        if (this.checkFields()) {
            GuiHelper.closeScreen();
        }
    }

    private boolean checkFields() {
        if (this.nameBox.getText().equals("")) {
            return false;
        }
        if (currentNPCID <= 0) {
            currentNPCID = this.npc.getId();
        }
        if (!this.nameBox.getText().equals(name)) {
            Pixelmon.NETWORK.sendToServer(new NPCServerPacket(currentNPCID, EnumNPCServerPacketType.Name, this.nameBox.getText()));
        }
        ArrayList<String> pages = new ArrayList<String>();
        if (!this.page1Box.getText().equals("")) {
            pages.add(this.page1Box.getText());
        }
        if (!this.page2Box.getText().equals("")) {
            pages.add(this.page2Box.getText());
        }
        if (!this.page3Box.getText().equals("")) {
            pages.add(this.page3Box.getText());
        }
        if (!this.page4Box.getText().equals("")) {
            pages.add(this.page4Box.getText());
        }
        if (!this.compareChat(pages)) {
            Pixelmon.NETWORK.sendToServer(new NPCServerPacket(currentNPCID, pages));
        }
        return true;
    }

    private boolean compareChat(ArrayList<String> pages) {
        if (pages.size() != chatPages.size()) {
            return false;
        }
        for (int i = 0; i < chatPages.size(); ++i) {
            String oldPage;
            String page = pages.get(i);
            if (page.equals(oldPage = chatPages.get(i))) continue;
            return false;
        }
        return true;
    }

    static {
        chatChanged = false;
        npcData = new ArrayList<ClientNPCData>();
    }
}

