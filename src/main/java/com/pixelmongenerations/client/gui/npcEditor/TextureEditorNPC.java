/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.npcEditor;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.elements.GuiContainerDropDown;
import com.pixelmongenerations.client.gui.elements.GuiDropDown;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.registry.BaseTrainer;
import com.pixelmongenerations.common.entity.npcs.registry.NPCRegistryTrainers;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.npc.EnumNPCServerPacketType;
import com.pixelmongenerations.core.network.packetHandlers.npc.NPCServerPacket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(value=Side.CLIENT)
class TextureEditorNPC {
    private GuiContainerDropDown gui;
    private EntityNPC npc;
    private BaseTrainer model;
    private int x;
    private int y;
    private int width;
    private int yOffset;
    private GuiTextField customTextureBox;
    private GuiDropDown<String> modelTextureDropDown;

    TextureEditorNPC(GuiContainerDropDown gui, EntityNPC npc, int x, int y, int width) {
        this(gui, npc, x, y, width, 0);
    }

    TextureEditorNPC(GuiContainerDropDown gui, EntityNPC npc, int x, int y, int width, int yOffset) {
        this.gui = gui;
        this.npc = npc;
        this.model = npc.getBaseTrainer();
        this.x = x;
        this.y = y;
        this.width = width;
        this.yOffset = yOffset;
        gui.addDropDown(new GuiDropDown<BaseTrainer>(ServerNPCRegistry.trainers.getTypes(), this.model, x, y + yOffset, width, 92).setGetOptionString(trainer -> I18n.translateToLocal("trainer." + trainer.name)).setOnSelected(this::selectTrainerType).setInactiveTop(y).setOrdered());
        if (this.model.textures.size() > 1) {
            this.displayTextureMenu();
        }
        this.customTextureBox = new GuiTextField(11, Minecraft.getMinecraft().fontRenderer, x, y + 38, width, 20);
        this.customTextureBox.setText(this.npc.getCustomSteveTexture());
    }

    private void displayTextureMenu() {
        int modelDistance = 16;
        this.modelTextureDropDown = new GuiDropDown<>(this.model.textures, this.npc.getTextureIndex(), this.x, this.y + Math.max(this.yOffset, (this.model.textures.size() - 1) * -10) + modelDistance, this.width, 70).setGetOptionString(texture -> I18n.translateToLocal("trainer.model." + texture)).setOnSelectedIndex(this::selectTexture).setInactiveTop(this.y + modelDistance);
        this.gui.addDropDown(this.modelTextureDropDown);
    }

    private void selectTrainerType(BaseTrainer model) {
        this.model = model;
        this.npc.getDataManager().set(EntityNPC.dwModel, this.model.id);
        Pixelmon.NETWORK.sendToServer(new NPCServerPacket(this.npc.getId(), this.model.id));
        this.npc.setTextureIndex(0);
        this.gui.removeDropDown(this.modelTextureDropDown);
        if (this.model.textures.size() > 1) {
            this.npc.getDataManager().set(EntityNPC.dwNickname, "Steve");
            this.displayTextureMenu();
        }
    }

    private void selectTexture(int textureIndex) {
        if (!this.model.textures.isEmpty()) {
            Pixelmon.NETWORK.sendToServer(new NPCServerPacket(this.npc.getId(), EnumNPCServerPacketType.TextureIndex, textureIndex));
        }
    }

    void drawCustomTextBox() {
        if (this.isCustomTexture()) {
            this.customTextureBox.drawTextBox();
            this.customTextureBox.setVisible(true);
        } else {
            this.customTextureBox.setVisible(false);
        }
    }

    void keyTyped(char key, int keyCode, GuiTextField ... otherFields) {
        this.customTextureBox.textboxKeyTyped(key, keyCode);
        if (this.customTextureBox.getVisible()) {
            ArrayList<GuiTextField> fields = new ArrayList<GuiTextField>(Arrays.asList(otherFields));
            fields.add(this.customTextureBox);
            GuiHelper.switchFocus(keyCode, fields);
        } else {
            GuiHelper.switchFocus(keyCode, otherFields);
        }
    }

    void mouseClicked(int x, int y, int mouseButton) {
        this.customTextureBox.mouseClicked(x, y, mouseButton);
    }

    void saveCustomTexture() {
        if (this.isCustomTexture()) {
            Pixelmon.NETWORK.sendToServer(new NPCServerPacket(this.npc.getId(), EnumNPCServerPacketType.CustomSteveTexture, this.customTextureBox.getText()));
        }
    }

    private boolean isCustomTexture() {
        if (this.model != NPCRegistryTrainers.Steve && this.model != NPCRegistryTrainers.Alex) {
            return false;
        }
        try {
            if (this.npc.getTextureIndex() == -1) {
                this.npc.setTextureIndex(0);
            }
        }
        catch (Exception e) {
            System.out.println(this.npc.getTextureIndex());
            this.npc.setTextureIndex(0);
        }
        String texture = this.model.textures.get(this.npc.getTextureIndex());
        return texture.equals("Custom_RP") || texture.equals("Custom_PN");
    }
}

