/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Keyboard
 */
package com.pixelmongenerations.client.gui.npcEditor;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.elements.GuiContainerDropDown;
import com.pixelmongenerations.client.gui.elements.GuiDropDown;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCShopkeeper;
import com.pixelmongenerations.common.entity.npcs.registry.ClientShopkeeperData;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.npc.DeleteNPC;
import com.pixelmongenerations.core.network.packetHandlers.npc.EnumNPCServerPacketType;
import com.pixelmongenerations.core.network.packetHandlers.npc.NPCServerPacket;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.input.Keyboard;

public class GuiShopkeeperEditor
extends GuiContainerDropDown {
    public static int shopkeeperID;
    public static String json;
    public static String name;
    public static List<ClientShopkeeperData> shopkeeperData;
    NPCShopkeeper npc;
    private GuiDropDown jsonDropDown;
    private GuiDropDown nameDropDown;
    private GuiDropDown textureDropDown;

    public GuiShopkeeperEditor(int shopkeeperID) {
        Keyboard.enableRepeatEvents((boolean)true);
        Optional<NPCShopkeeper> npcShopkeeperOptional = EntityNPC.locateNPCClient(Minecraft.getMinecraft().world, shopkeeperID, NPCShopkeeper.class);
        if (!npcShopkeeperOptional.isPresent()) {
            GuiHelper.closeScreen();
            return;
        }
        this.npc = npcShopkeeperOptional.get();
        GuiShopkeeperEditor.shopkeeperID = shopkeeperID;
        if (this.npc == null) {
            GuiHelper.closeScreen();
        }
    }

    @Override
    public void initGui() {
        super.initGui();
        this.buttonList.add(new GuiButton(1, this.width / 2 + 155, this.height / 2 + 90, 30, 20, I18n.translateToLocal("gui.guiItemDrops.ok")));
        this.buttonList.add(new GuiButton(6, this.width / 2 + 100, this.height / 2 - 120, 80, 20, I18n.translateToLocal("gui.npceditor.despawn")));
        this.jsonDropDown = new GuiDropDown<ClientShopkeeperData>(shopkeeperData, new ClientShopkeeperData(json), this.width / 2 - 10, this.height / 2 - 81, 140, 100).setGetOptionString(shopkeeper -> I18n.translateToLocal("npc.shopkeeper." + shopkeeper.getID())).setOnSelected(shopkeeper -> Pixelmon.NETWORK.sendToServer(new NPCServerPacket(shopkeeperID, EnumNPCServerPacketType.CycleJson, shopkeeper.getID()))).setOrdered();
        this.addDropDown(this.jsonDropDown);
        ClientShopkeeperData currentShopkeeper = null;
        for (ClientShopkeeperData data : shopkeeperData) {
            if (!data.getID().equals(name)) continue;
            currentShopkeeper = data;
            break;
        }
        if (currentShopkeeper == null) {
            if (shopkeeperData.isEmpty()) {
                GuiHelper.closeScreen();
                return;
            }
            currentShopkeeper = shopkeeperData.get(0);
        }
        this.resetDropDowns(currentShopkeeper, this.npc.getCustomSteveTexture());
        this.buttonList.add(new GuiButton(5, this.width / 2 - 10, this.height / 2 + 5, 140, 20, I18n.translateToLocal("gui.shopkeepereditor.refreshItems")));
    }

    public void updateShopkeeper(String texture) {
        int selectedIndex;
        if (this.jsonDropDown != null && (selectedIndex = this.jsonDropDown.getSelectedIndex()) >= 0) {
            this.resetDropDowns(shopkeeperData.get(selectedIndex), texture);
        }
    }

    private void resetDropDowns(ClientShopkeeperData currentShopkeeper, String newTexture) {
        this.removeDropDown(this.nameDropDown);
        this.nameDropDown = new GuiDropDown<String>(currentShopkeeper.getNames(), name, this.width / 2 - 10, this.height / 2 - 51, 140, 100).setOnSelectedIndex(index -> Pixelmon.NETWORK.sendToServer(new NPCServerPacket(shopkeeperID, EnumNPCServerPacketType.CycleName, (int)index)));
        this.addDropDown(this.nameDropDown);
        this.removeDropDown(this.textureDropDown);
        this.textureDropDown = new GuiDropDown<String>(currentShopkeeper.getTextures(), newTexture, this.width / 2 - 10, this.height / 2 - 21, 140, 100).setGetOptionString(texture -> I18n.translateToLocal("npc.model." + texture.replace(".png", ""))).setOnSelected(texture -> Pixelmon.NETWORK.sendToServer(new NPCServerPacket(shopkeeperID, EnumNPCServerPacketType.CustomSteveTexture, (String)texture)));
        this.addDropDown(this.textureDropDown);
    }

    @Override
    protected void drawBackgroundUnderMenus(float f, int i, int j) {
        this.mc.renderEngine.bindTexture(GuiResources.cwPanel);
        GuiHelper.drawImageQuad(this.width / 2 - 200, this.height / 2 - 120, 400.0, 240.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        RenderHelper.disableStandardItemLighting();
        GuiHelper.drawEntity(this.npc, this.width / 2 - 140, this.height / 2 + 50, 60.0f, 0.0f, 0.0f);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.shopkeeper.json"), this.width / 2 - 60, this.height / 2 - 80, 0);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.name"), this.width / 2 - 60, this.height / 2 - 50, 0);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.model"), this.width / 2 - 60, this.height / 2 - 20, 0);
    }

    @Override
    protected void keyTyped(char key, int par2) {
        if (par2 == 1 || par2 == 28) {
            GuiHelper.closeScreen();
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        if (button.enabled) {
            if (button.id == 1) {
                GuiHelper.closeScreen();
            } else if (button.id == 5) {
                Pixelmon.NETWORK.sendToServer(new NPCServerPacket(shopkeeperID, EnumNPCServerPacketType.RefreshItems));
            } else if (button.id == 6) {
                Pixelmon.NETWORK.sendToServer(new DeleteNPC(shopkeeperID));
                GuiHelper.closeScreen();
            }
        }
    }

    static {
        shopkeeperData = new ArrayList<ClientShopkeeperData>();
    }
}

