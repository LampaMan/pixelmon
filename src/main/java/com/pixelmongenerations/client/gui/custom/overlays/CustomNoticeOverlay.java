/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.custom.overlays;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.custom.overlays.GraphicPlacement;
import com.pixelmongenerations.client.gui.custom.overlays.GraphicType;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.enums.EnumGrowth;
import java.util.Collection;
import java.util.Optional;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;

public class CustomNoticeOverlay {
    private static boolean enabled = false;
    private static Collection<String> noticeLines;
    private static GraphicType graphicType;
    private static GraphicPlacement graphicDisplayType;
    private static PokemonSpec pixelmonSpec;
    private static EntityPixelmon pokemonToRender;
    private static ItemStack itemStackToRender;
    private static ResourceLocation itemSpriteToRender;
    private static int borderColor;
    private static int backgroundColor;
    private static int squareSize;

    public static void resetNotice() {
        enabled = false;
        noticeLines = null;
        graphicType = null;
        graphicDisplayType = null;
        pokemonToRender = null;
        itemStackToRender = null;
        itemSpriteToRender = null;
        borderColor = 255;
        backgroundColor = 8684191;
    }

    public static void draw(ScaledResolution scaledResolution) {
        Optional<int[]> coordsOptional = GuiHelper.renderTooltip(scaledResolution.getScaledWidth() / 2, 15, noticeLines, borderColor, backgroundColor, 100, true, true);
        if (coordsOptional.isPresent()) {
            int[] coords = coordsOptional.get();
            if (graphicType != null) {
                switch (graphicType) {
                    case PokemonSprite: {
                        CustomNoticeOverlay.drawPokemonSprites(coords);
                        break;
                    }
                    case Pokemon3D: {
                        CustomNoticeOverlay.drawPokemon3Ds(coords);
                        break;
                    }
                    case ItemSprite: {
                        CustomNoticeOverlay.drawItemSprites(coords);
                        break;
                    }
                    case Item3D: {
                        CustomNoticeOverlay.drawItem3Ds(coords);
                    }
                }
            }
        }
    }

    public static void setPokemon3D(NBTTagCompound pokemon, GraphicPlacement displayType) {
        graphicType = GraphicType.Pokemon3D;
        graphicDisplayType = displayType;
        pokemonToRender = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(pokemon, Minecraft.getMinecraft().world);
        pokemonToRender.setGrowth(EnumGrowth.Ordinary);
    }

    public static void setPokemonSprite(NBTTagCompound pokemon, GraphicPlacement displayType) {
        pixelmonSpec = new PokemonSpec(pokemon);
        graphicType = GraphicType.PokemonSprite;
        graphicDisplayType = displayType;
    }

    public static void setItemSprite(ItemStack itemStack, GraphicPlacement displayType) {
        if (!itemStack.isEmpty()) {
            graphicType = GraphicType.ItemSprite;
            graphicDisplayType = displayType;
            itemStackToRender = itemStack;
            String[] split = Minecraft.getMinecraft().getRenderItem().getItemModelMesher().getItemModel(itemStackToRender).getParticleTexture().getIconName().split(":");
            itemSpriteToRender = new ResourceLocation(split[0], "textures/" + split[1] + ".png");
        }
    }

    public static void setItem3D(ItemStack itemStack, GraphicPlacement displayType) {
        if (!itemStack.isEmpty()) {
            graphicType = GraphicType.Item3D;
            graphicDisplayType = displayType;
            itemStackToRender = itemStack;
        }
    }

    public static void setBorderColor(int color) {
        borderColor = color;
    }

    public static void setBackgroundColor(int color) {
        backgroundColor = color;
    }

    public static void populate(Collection<String> noticeLines) {
        CustomNoticeOverlay.noticeLines = noticeLines;
    }

    public static void setEnabled(boolean enabled) {
        CustomNoticeOverlay.enabled = enabled;
    }

    public static boolean isEnabled() {
        return enabled;
    }

    private static void drawItem3Ds(int[] coords) {
        RenderHelper.enableStandardItemLighting();
        switch (graphicDisplayType) {
            case LEFT: {
                Minecraft.getMinecraft().getRenderItem().renderItemIntoGUI(itemStackToRender, coords[0] - 20, 15);
                break;
            }
            case RIGHT: {
                Minecraft.getMinecraft().getRenderItem().renderItemIntoGUI(itemStackToRender, coords[2] + 5, 15);
                break;
            }
            case BOTH_SIDES: {
                Minecraft.getMinecraft().getRenderItem().renderItemIntoGUI(itemStackToRender, coords[0] - 20, 15);
                Minecraft.getMinecraft().getRenderItem().renderItemIntoGUI(itemStackToRender, coords[2] + 5, 15);
            }
        }
    }

    private static void drawItemSprites(int[] coords) {
        squareSize = 16;
        Minecraft.getMinecraft().getTextureManager().bindTexture(itemSpriteToRender);
        switch (graphicDisplayType) {
            case LEFT: {
                GuiHelper.drawImageQuad(coords[0] - squareSize, squareSize, squareSize, squareSize, 1.0, 0.0, 0.0, 1.0, 300.0f);
                break;
            }
            case RIGHT: {
                GuiHelper.drawImageQuad(coords[2], squareSize, squareSize, squareSize, 1.0, 0.0, 0.0, 1.0, 300.0f);
                break;
            }
            case BOTH_SIDES: {
                GuiHelper.drawImageQuad(coords[0] - (squareSize + 2), squareSize, squareSize, squareSize, 0.0, 0.0, 1.0, 1.0, 300.0f);
                GuiHelper.drawImageQuad(coords[2] + 2, squareSize, squareSize, squareSize, 0.0, 0.0, 1.0, 1.0, 300.0f);
            }
        }
    }

    private static void drawPokemon3Ds(int[] coords) {
        float scale = CustomNoticeOverlay.pokemonToRender.baseStats.giScale * 10.0f;
        GuiHelper.drawEntity(Minecraft.getMinecraft().player, 0, 0, scale, 0.0f, 0.0f);
        switch (graphicDisplayType) {
            case LEFT: {
                GuiHelper.drawEntity(pokemonToRender, coords[0] - 15, squareSize - 5, scale, 0.0f, 0.0f);
                break;
            }
            case RIGHT: {
                GuiHelper.drawEntity(pokemonToRender, coords[2] + 10, squareSize - 5, scale, 0.0f, 0.0f);
                break;
            }
            case BOTH_SIDES: {
                GuiHelper.drawEntity(pokemonToRender, coords[0] - 15, squareSize - 5, scale, 0.0f, 0.0f);
                GuiHelper.drawEntity(pokemonToRender, coords[2] + 10, squareSize - 5, scale, 0.0f, 0.0f);
            }
        }
    }

    private static void drawPokemonSprites(int[] coords) {
        squareSize = 32;
        GuiHelper.bindPokemonSprite(pixelmonSpec, Minecraft.getMinecraft());
        switch (graphicDisplayType) {
            case LEFT: {
                GuiHelper.drawImageQuad(coords[0] - squareSize, 0.0, squareSize, squareSize, 1.0, 0.0, 0.0, 1.0, 300.0f);
                break;
            }
            case RIGHT: {
                GuiHelper.drawImageQuad(coords[2], 0.0, squareSize, squareSize, 1.0, 0.0, 0.0, 1.0, 300.0f);
                break;
            }
            case BOTH_SIDES: {
                GuiHelper.drawImageQuad(coords[0] - squareSize, 0.0, squareSize, squareSize, 1.0, 0.0, 0.0, 1.0, 300.0f);
                GuiHelper.drawImageQuad(coords[2], 0.0, squareSize, squareSize, 0.0, 0.0, 1.0, 1.0, 300.0f);
            }
        }
    }

    static {
        squareSize = 16;
    }
}

