/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.custom.overlays;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.custom.overlays.ScoreboardLocation;
import java.util.Collection;
import net.minecraft.client.gui.ScaledResolution;

public class CustomScoreboardOverlay {
    private static boolean enabled = false;
    private static String title;
    private static Collection<String> scoreboardLines;
    private static Collection<String> scores;
    private static ScoreboardLocation location;

    public static void draw(ScaledResolution scaledResolution) {
        if (location == ScoreboardLocation.RIGHT_TOP) {
            GuiHelper.drawScoreboard(0, scaledResolution.getScaledWidth(), 0x64000000, title, scoreboardLines, scores);
        } else if (location == ScoreboardLocation.RIGHT_MIDDLE) {
            GuiHelper.drawScoreboard(scaledResolution.getScaledHeight() / 2 - (scoreboardLines.size() + 1) * 10 / 2, scaledResolution.getScaledWidth(), 0x64000000, title, scoreboardLines, scores);
        } else if (location == ScoreboardLocation.RIGHT_BOTTOM) {
            GuiHelper.drawScoreboard(scaledResolution.getScaledHeight() - (scoreboardLines.size() + 1) * 10, scaledResolution.getScaledWidth(), 0x64000000, title, scoreboardLines, scores);
        }
    }

    public static void populate(ScoreboardLocation location, String title, Collection<String> scoreboardLines, Collection<String> scores) {
        CustomScoreboardOverlay.location = location;
        CustomScoreboardOverlay.title = title;
        CustomScoreboardOverlay.scoreboardLines = scoreboardLines;
        CustomScoreboardOverlay.scores = scores;
    }

    public static void setLocation(ScoreboardLocation location) {
        CustomScoreboardOverlay.location = location;
    }

    public static void setEnabled(boolean enabled) {
        CustomScoreboardOverlay.enabled = enabled;
    }

    public static boolean isEnabled() {
        return enabled;
    }

    public static void resetBoard() {
        enabled = false;
        title = null;
        scoreboardLines = null;
        scores = null;
        location = null;
    }
}

