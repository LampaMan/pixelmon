/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.custom.overlays;

public enum GraphicType {
    PokemonSprite,
    Pokemon3D,
    ItemSprite,
    Item3D;

}

