/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.materials;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import net.minecraft.client.renderer.GlStateManager;
import org.lwjgl.opengl.GL11;

public class Lighting {
    private static final ByteBuffer buffer = ByteBuffer.allocateDirect(16).order(ByteOrder.nativeOrder());

    public static void testLight(float x, float y, float z) {
        GlStateManager.enableLighting();
        GL11.glEnable((int)16385);
        float[] lightAmbient = new float[]{1.0f, 0.0f, 1.0f, 1.0f};
        float[] lightDiffuse = new float[]{1.0f, 0.0f, 1.0f, 1.0f};
        float[] lightPosition = new float[]{x, y, z, 1.0f};
        GL11.glLight((int)16385, (int)4611, (FloatBuffer)((FloatBuffer)buffer.asFloatBuffer().put(lightPosition).flip()));
        GL11.glLight((int)16385, (int)4608, (FloatBuffer)((FloatBuffer)buffer.asFloatBuffer().put(lightAmbient).flip()));
        GL11.glLight((int)16385, (int)4609, (FloatBuffer)((FloatBuffer)buffer.asFloatBuffer().put(lightDiffuse).flip()));
    }

    public static void attempt(float x, float y, float z) {
        GL11.glClearColor((float)0.0f, (float)0.0f, (float)0.0f, (float)0.0f);
        GlStateManager.shadeModel(7425);
        float[] light_position = new float[]{x, y, z, 1.0f};
        float[] lightAmbient = new float[]{1.0f, 1.0f, 1.0f, 1.0f};
        float[] lightDiffuse = new float[]{1.0f, 1.0f, 1.0f, 1.0f};
        GlStateManager.enableColorMaterial();
        GL11.glColorMaterial((int)1032, (int)5634);
        GL11.glLight((int)16384, (int)4611, (FloatBuffer)((FloatBuffer)buffer.asFloatBuffer().put(light_position).flip()));
        GL11.glLight((int)16384, (int)4608, (FloatBuffer)((FloatBuffer)buffer.asFloatBuffer().put(lightAmbient).flip()));
        GL11.glLight((int)16384, (int)4609, (FloatBuffer)((FloatBuffer)buffer.asFloatBuffer().put(lightDiffuse).flip()));
        GlStateManager.enableLighting();
        GL11.glEnable((int)16384);
    }

    public static void wat() {
    }
}

