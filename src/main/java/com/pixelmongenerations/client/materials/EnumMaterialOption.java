/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.materials;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import org.lwjgl.opengl.GL11;

public enum EnumMaterialOption {
    NO_LIGHTING{

        @Override
        public void begin(Object ... params) {
            FS.start_unlit();
        }

        @Override
        public void end(Object ... params) {
            FS.end_unlit();
        }
    }
    ,
    WIREFRAME{

        @Override
        public void begin(Object ... params) {
            FS.start_wire();
        }

        @Override
        public void end(Object ... params) {
            GL11.glPopAttrib();
        }
    }
    ,
    TRANSPARENCY{

        @Override
        public void begin(Object ... params) {
            FS.start_transparency(params);
        }

        @Override
        public void end(Object ... params) {
            FS.end_transparency();
        }
    }
    ,
    NOCULL{

        @Override
        public void begin(Object ... params) {
            FS.enable_nocull();
        }

        @Override
        public void end(Object ... params) {
            FS.disable_nocull();
        }
    };

    public static int cubemapID;

    public abstract void begin(Object ... var1);

    public abstract void end(Object ... var1);

    static {
        cubemapID = 33986;
    }

    private static final class FS {
        private FS() {
        }

        public static void start_wire() {
            GL11.glPushAttrib((int)2880);
            GL11.glPolygonMode((int)1032, (int)6913);
        }

        public static void start_unlit() {
            GlStateManager.disableLighting();
            Minecraft.getMinecraft().entityRenderer.disableLightmap();
        }

        public static void end_unlit() {
            GlStateManager.enableLighting();
            Minecraft.getMinecraft().entityRenderer.enableLightmap();
        }

        public static void start_transparency(Object[] actuallyAFloat) {
            GlStateManager.enableBlend();
            GlStateManager.blendFunc(770, 771);
            if (actuallyAFloat != null && actuallyAFloat.length > 0 && actuallyAFloat[0] instanceof Float) {
                float alpha = ((Float)actuallyAFloat[0]).floatValue();
                GlStateManager.color(1.0f, 1.0f, 1.0f, alpha);
            }
        }

        public static void end_transparency() {
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            GlStateManager.disableBlend();
        }

        public static void enable_nocull() {
            GlStateManager.enableCull();
        }

        public static void disable_nocull() {
            GlStateManager.disableCull();
        }
    }
}

