/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.obj.parser.mtl;

import com.pixelmongenerations.client.models.obj.Material;
import com.pixelmongenerations.client.models.obj.WavefrontObject;
import com.pixelmongenerations.client.models.obj.parser.LineParser;
import com.pixelmongenerations.client.models.obj.parser.mtl.MtlLineParserFactory;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

public class MaterialFileParser
extends LineParser {
    HashMap<String, Material> materials = new HashMap();
    private WavefrontObject object;
    private MtlLineParserFactory parserFactory = null;

    public MaterialFileParser(WavefrontObject object) {
        this.object = object;
        this.parserFactory = new MtlLineParserFactory(object);
    }

    @Override
    public void incoporateResults(WavefrontObject wavefrontObject) {
    }

    @Override
    public void parse() {
        String pathToMTL = this.words[1];
        InputStream fileInput = this.getClass().getResourceAsStream(pathToMTL);
        if (fileInput == null) {
            try {
                File file = new File(pathToMTL);
                if (file.exists()) {
                    fileInput = new FileInputStream(file);
                }
            }
            catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        String currentLine = null;
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(fileInput));
            while ((currentLine = in.readLine()) != null) {
                LineParser parser = this.parserFactory.getLineParser(currentLine);
                parser.parse();
                parser.incoporateResults(this.object);
            }
            in.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on line:" + currentLine);
            throw new RuntimeException("Error parsing :'" + pathToMTL + "'");
        }
    }
}

