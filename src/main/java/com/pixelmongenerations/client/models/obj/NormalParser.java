/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.obj;

import com.pixelmongenerations.client.models.obj.Vertex;
import com.pixelmongenerations.client.models.obj.WavefrontObject;
import com.pixelmongenerations.client.models.obj.parser.LineParser;

public class NormalParser
extends LineParser {
    Vertex vertex = null;

    @Override
    public void parse() {
        this.vertex = new Vertex();
        try {
            this.vertex.setX(Float.parseFloat(this.words[1]));
            this.vertex.setY(Float.parseFloat(this.words[2]));
            this.vertex.setZ(Float.parseFloat(this.words[3]));
        }
        catch (Exception e) {
            throw new RuntimeException("NormalParser Error");
        }
    }

    @Override
    public void incoporateResults(WavefrontObject wavefrontObject) {
        wavefrontObject.getNormals().add(this.vertex);
    }
}

