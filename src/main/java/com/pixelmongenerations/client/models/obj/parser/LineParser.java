/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.obj.parser;

import com.pixelmongenerations.client.models.obj.WavefrontObject;

public abstract class LineParser {
    protected String[] words = null;

    public void setWords(String[] words) {
        this.words = words;
    }

    public abstract void parse();

    public abstract void incoporateResults(WavefrontObject var1);
}

