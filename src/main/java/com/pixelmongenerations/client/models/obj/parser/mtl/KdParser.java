/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.obj.parser.mtl;

import com.pixelmongenerations.client.models.obj.Material;
import com.pixelmongenerations.client.models.obj.Vertex;
import com.pixelmongenerations.client.models.obj.WavefrontObject;
import com.pixelmongenerations.client.models.obj.parser.LineParser;

public class KdParser
extends LineParser {
    Vertex kd = null;

    @Override
    public void incoporateResults(WavefrontObject wavefrontObject) {
        Material currentMaterial = wavefrontObject.getCurrentMaterial();
        currentMaterial.setKd(this.kd);
    }

    @Override
    public void parse() {
        this.kd = new Vertex();
        try {
            this.kd.setX(Float.parseFloat(this.words[1]));
            this.kd.setY(Float.parseFloat(this.words[2]));
            this.kd.setZ(Float.parseFloat(this.words[3]));
        }
        catch (Exception e) {
            throw new RuntimeException("VertexParser Error");
        }
    }
}

