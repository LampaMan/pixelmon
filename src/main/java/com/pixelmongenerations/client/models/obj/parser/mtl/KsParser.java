/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.obj.parser.mtl;

import com.pixelmongenerations.client.models.obj.Material;
import com.pixelmongenerations.client.models.obj.Vertex;
import com.pixelmongenerations.client.models.obj.WavefrontObject;
import com.pixelmongenerations.client.models.obj.parser.LineParser;

public class KsParser
extends LineParser {
    Vertex ks = null;

    @Override
    public void incoporateResults(WavefrontObject wavefrontObject) {
        Material currentMaterial = wavefrontObject.getCurrentMaterial();
        currentMaterial.setKs(this.ks);
    }

    @Override
    public void parse() {
        this.ks = new Vertex();
        try {
            this.ks.setX(Float.parseFloat(this.words[1]));
            this.ks.setY(Float.parseFloat(this.words[2]));
            this.ks.setZ(Float.parseFloat(this.words[3]));
        }
        catch (Exception e) {
            throw new RuntimeException("VertexParser Error");
        }
    }
}

