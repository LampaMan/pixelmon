/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.obj.parser.mtl;

import com.pixelmongenerations.client.models.obj.Material;
import com.pixelmongenerations.client.models.obj.WavefrontObject;
import com.pixelmongenerations.client.models.obj.parser.LineParser;

public class NsParser
extends LineParser {
    float ns;

    @Override
    public void incoporateResults(WavefrontObject wavefrontObject) {
        Material currentMaterial = wavefrontObject.getCurrentMaterial();
        currentMaterial.setShininess(this.ns);
    }

    @Override
    public void parse() {
        try {
            this.ns = Float.parseFloat(this.words[1]);
        }
        catch (Exception e) {
            throw new RuntimeException("VertexParser Error");
        }
    }
}

