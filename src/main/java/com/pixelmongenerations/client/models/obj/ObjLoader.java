/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.obj;

import com.pixelmongenerations.client.models.obj.WavefrontObject;
import java.io.IOException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.IResource;
import net.minecraft.util.ResourceLocation;

public class ObjLoader {
    public static WavefrontObject loadModel(ResourceLocation resource) {
        try {
            IResource res = Minecraft.getMinecraft().getResourceManager().getResource(resource);
            return new WavefrontObject(res.getInputStream());
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean accepts(ResourceLocation modelLocation) {
        return modelLocation.getPath().endsWith(".obj");
    }
}

