/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.obj.parser.mtl;

import com.pixelmongenerations.client.models.obj.Material;
import com.pixelmongenerations.client.models.obj.WavefrontObject;
import com.pixelmongenerations.client.models.obj.parser.LineParser;

public class MaterialParser
extends LineParser {
    String materialName = "";

    @Override
    public void incoporateResults(WavefrontObject wavefrontObject) {
        Material newMaterial = new Material(this.materialName);
        wavefrontObject.getMaterials().put(this.materialName, newMaterial);
        wavefrontObject.setCurrentMaterial(newMaterial);
    }

    @Override
    public void parse() {
        this.materialName = this.words[1];
    }
}

