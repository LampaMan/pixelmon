/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.obj;

import com.pixelmongenerations.client.models.obj.WavefrontObject;
import com.pixelmongenerations.client.models.obj.parser.DefaultParser;
import com.pixelmongenerations.client.models.obj.parser.LineParser;
import java.util.HashMap;
import java.util.regex.Pattern;

public abstract class LineParserFactory {
    private static final Pattern COMPILE = Pattern.compile("  ");
    private static final Pattern PATTERN = Pattern.compile("\t");
    protected HashMap<String, LineParser> parsers = new HashMap();
    protected WavefrontObject object = null;

    public LineParser getLineParser(String line) {
        if (line == null) {
            return null;
        }
        line = COMPILE.matcher(line).replaceAll(" ");
        String[] lineWords = (line = PATTERN.matcher(line).replaceAll("")).split(" ");
        if (lineWords.length < 1) {
            return new DefaultParser();
        }
        String lineType = lineWords[0];
        LineParser parser = this.parsers.get(lineType);
        if (parser == null) {
            parser = new DefaultParser();
        }
        parser.setWords(lineWords);
        return parser;
    }
}

