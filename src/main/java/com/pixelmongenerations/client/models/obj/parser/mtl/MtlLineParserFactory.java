/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.obj.parser.mtl;

import com.pixelmongenerations.client.models.obj.LineParserFactory;
import com.pixelmongenerations.client.models.obj.WavefrontObject;
import com.pixelmongenerations.client.models.obj.parser.CommentParser;
import com.pixelmongenerations.client.models.obj.parser.mtl.KaParser;
import com.pixelmongenerations.client.models.obj.parser.mtl.KdMapParser;
import com.pixelmongenerations.client.models.obj.parser.mtl.KdParser;
import com.pixelmongenerations.client.models.obj.parser.mtl.KsParser;
import com.pixelmongenerations.client.models.obj.parser.mtl.MaterialParser;
import com.pixelmongenerations.client.models.obj.parser.mtl.NsParser;

public class MtlLineParserFactory
extends LineParserFactory {
    public MtlLineParserFactory(WavefrontObject object) {
        this.object = object;
        this.parsers.put("newmtl", new MaterialParser());
        this.parsers.put("Ka", new KaParser());
        this.parsers.put("Kd", new KdParser());
        this.parsers.put("Ks", new KsParser());
        this.parsers.put("Ns", new NsParser());
        this.parsers.put("map_Kd", new KdMapParser(object));
        this.parsers.put("#", new CommentParser());
    }
}

