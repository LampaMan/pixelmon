/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.obj.parser.mtl;

import com.pixelmongenerations.client.models.obj.Material;
import com.pixelmongenerations.client.models.obj.Texture;
import com.pixelmongenerations.client.models.obj.TextureLoader;
import com.pixelmongenerations.client.models.obj.WavefrontObject;
import com.pixelmongenerations.client.models.obj.parser.LineParser;

public class KdMapParser
extends LineParser {
    private Texture texture = null;
    private String texName;

    public KdMapParser(WavefrontObject object) {
    }

    @Override
    public void incoporateResults(WavefrontObject wavefrontObject) {
        if (this.texture != null) {
            Material currentMaterial = wavefrontObject.getCurrentMaterial();
            currentMaterial.texName = this.texName;
            currentMaterial.setTexture(this.texture);
        }
    }

    @Override
    public void parse() {
        String textureFileName;
        this.texName = textureFileName = this.words[this.words.length - 1];
        String pathToTextureBinary = textureFileName;
        this.texture = TextureLoader.instance().loadTexture(pathToTextureBinary);
    }
}

