/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.obj.parser.obj;

import com.pixelmongenerations.client.models.obj.WavefrontObject;
import com.pixelmongenerations.client.models.obj.parser.LineParser;

public class MaterialParser
extends LineParser {
    String materialName = "";

    @Override
    public void parse() {
        this.materialName = this.words[1];
    }

    @Override
    public void incoporateResults(WavefrontObject wavefrontObject) {
    }
}

