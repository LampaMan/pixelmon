/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.obj.parser.obj;

import com.pixelmongenerations.client.models.obj.Group;
import com.pixelmongenerations.client.models.obj.WavefrontObject;
import com.pixelmongenerations.client.models.obj.parser.LineParser;

public class GroupParser
extends LineParser {
    Group newGroup = null;

    @Override
    public void incoporateResults(WavefrontObject wavefrontObject) {
        if (wavefrontObject.getCurrentGroup() != null) {
            wavefrontObject.getCurrentGroup().pack();
        }
        wavefrontObject.getGroups().add(this.newGroup);
        wavefrontObject.getGroupsDirectAccess().put(this.newGroup.getName(), this.newGroup);
        wavefrontObject.setCurrentGroup(this.newGroup);
    }

    @Override
    public void parse() {
        String groupName = this.words[1];
        this.newGroup = new Group(groupName);
    }
}

