/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.obj;

import com.pixelmongenerations.client.models.obj.Face;
import com.pixelmongenerations.client.models.obj.Material;
import com.pixelmongenerations.client.models.obj.TextureCoordinate;
import com.pixelmongenerations.client.models.obj.Vertex;
import java.util.ArrayList;
import java.util.Iterator;

public class Group {
    private String name;
    private Vertex min = null;
    private Material material;
    private ArrayList<Face> faces = new ArrayList();
    public ArrayList<Integer> indices = new ArrayList();
    public ArrayList<Vertex> vertices = new ArrayList();
    public ArrayList<Vertex> normals = new ArrayList();
    public ArrayList<TextureCoordinate> texcoords = new ArrayList();
    public int indexCount = 0;

    public Group(String name) {
        this.name = name;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public void addFace(Face face) {
        this.faces.add(face);
    }

    public void pack() {
        float minX = 0.0f;
        float minY = 0.0f;
        float minZ = 0.0f;
        Face currentFace = null;
        Vertex currentVertex = null;
        Iterator<Face> iterator = this.faces.iterator();
        while (iterator.hasNext()) {
            Face face;
            currentFace = face = iterator.next();
            for (int j = 0; j < currentFace.getVertices().length; ++j) {
                currentVertex = currentFace.getVertices()[j];
                if (Math.abs(currentVertex.getX()) > minX) {
                    minX = Math.abs(currentVertex.getX());
                }
                if (Math.abs(currentVertex.getY()) > minY) {
                    minY = Math.abs(currentVertex.getY());
                }
                if (Math.abs(currentVertex.getZ()) <= minZ) continue;
                minZ = Math.abs(currentVertex.getZ());
            }
        }
        this.min = new Vertex(minX, minY, minZ);
    }

    public String getName() {
        return this.name;
    }

    public Material getMaterial() {
        return this.material;
    }

    public ArrayList<Face> getFaces() {
        return this.faces;
    }

    public Vertex getMin() {
        return this.min;
    }
}

