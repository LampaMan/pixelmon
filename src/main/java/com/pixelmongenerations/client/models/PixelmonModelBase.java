/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models;

import com.pixelmongenerations.client.models.animations.SkeletonBase;
import com.pixelmongenerations.common.entity.pixelmon.Entity2HasModel;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.helpers.animation.IncrementingVariable;
import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;

public abstract class PixelmonModelBase
extends ModelBase {
    public float scale = 1.0f;
    protected SkeletonBase skeleton;
    public float movementThreshold = 0.3f;

    @Override
    public void render(Entity var1, float f, float f1, float f2, float f3, float f4, float f5) {
        if (var1 instanceof EntityPixelmon && this.skeleton != null) {
            this.doAnimation(var1, f, f1, f2, f3, f4, f5);
        }
    }

    public void doAnimation(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        Entity3HasStats pixelmon = (Entity3HasStats)entity;
        if (entity.isInWater()) {
            this.skeleton.swim(pixelmon, f, f1, f2, f3, f4);
        } else if (entity.isAirBorne || pixelmon.baseStats.doesHover) {
            this.skeleton.fly(pixelmon, f, f1, f2, f3, f4);
        } else {
            this.skeleton.walk(pixelmon, f, f1, f2, f3, f4);
        }
    }

    public float getScale() {
        return this.scale;
    }

    protected void setInt(int id, int value, Entity2HasModel pixelmon) {
        pixelmon.getAnimationVariables().setInt(id, value);
    }

    protected int getInt(int id, Entity2HasModel pixelmon) {
        return pixelmon.getAnimationVariables().getInt(id);
    }

    protected IncrementingVariable setCounter(int id, int limit, int increment, Entity2HasModel pixelmon) {
        pixelmon.getAnimationVariables().setCounter(id, limit, increment);
        return this.getCounter(id, pixelmon);
    }

    protected IncrementingVariable getCounter(int id, Entity2HasModel pixelmon) {
        return pixelmon.getAnimationVariables().getCounter(id);
    }

    protected void registerAnimationCounters() {
    }

    protected boolean hasInt(int id, Entity2HasModel pixelmon) {
        return pixelmon.getAnimationVariables().hasInt(id);
    }
}

