/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.blocks;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

public class ModelMechanicalAnvil
extends GenericSmdModel {
    public ModelMechanicalAnvil() {
        super("models/blocks/mech_anvil", "mech_anvil.pqc");
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        float scale = 0.2654421f;
        GlStateManager.scale(scale, scale, scale);
        GlStateManager.rotate(180.0f, 0.0f, 1.0f, 0.0f);
        GlStateManager.rotate(-90.0f, 1.0f, 0.0f, 0.0f);
        super.render(entity, f, f1, f2, f3, f4, f5);
    }
}

