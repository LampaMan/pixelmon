/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.blocks;

import com.pixelmongenerations.common.block.tileEntities.TileEntityAnvil;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelAnvil
extends ModelBase {
    ModelRenderer AngledTB;
    ModelRenderer anvil3;
    ModelRenderer Anvil1;
    ModelRenderer AngledTF;
    ModelRenderer Anvil4;
    ModelRenderer AngledTR;
    ModelRenderer Base;
    ModelRenderer AngledBF;
    ModelRenderer AngledBR;
    ModelRenderer AngledBL;
    ModelRenderer AngledBB;
    ModelRenderer Ar6;
    ModelRenderer stufffs;
    ModelRenderer AngledTL;
    ModelRenderer anvil2;
    ModelRenderer Top;
    ModelRenderer Ar8;
    ModelRenderer Ar7;
    ModelRenderer Ar5;
    ModelRenderer BaseTop;
    ModelRenderer AR1;
    ModelRenderer Ar4;
    ModelRenderer Ar3;
    ModelRenderer Ar2;

    public ModelAnvil() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.AngledTB = new ModelRenderer(this, 0, 12);
        this.AngledTB.addBox(-3.0f, -2.0f, 1.0f, 6, 2, 1);
        this.AngledTB.setRotationPoint(1.0f, 16.0f, 0.5f);
        this.AngledTB.setTextureSize(64, 32);
        this.AngledTB.mirror = true;
        this.setRotation(this.AngledTB, -0.7853982f, 0.0f, 0.0f);
        this.anvil3 = new ModelRenderer(this, 46, 4);
        this.anvil3.addBox(-0.5f, 0.0f, -0.5f, 1, 1, 1);
        this.anvil3.setRotationPoint(6.5f, 13.2f, 0.0f);
        this.anvil3.setTextureSize(64, 32);
        this.anvil3.mirror = true;
        this.setRotation(this.anvil3, 0.0f, 0.0f, 0.0f);
        this.Anvil1 = new ModelRenderer(this, 38, 0);
        this.Anvil1.addBox(1.0f, -2.0f, -1.0f, 1, 3, 2);
        this.Anvil1.setRotationPoint(4.5f, 14.0f, 0.0f);
        this.Anvil1.setTextureSize(64, 32);
        this.Anvil1.mirror = true;
        this.setRotation(this.Anvil1, 0.0f, 0.0f, 0.9599311f);
        this.AngledTF = new ModelRenderer(this, 0, 12);
        this.AngledTF.addBox(-3.0f, -2.0f, -2.0f, 6, 2, 1);
        this.AngledTF.setRotationPoint(1.0f, 16.0f, -0.5f);
        this.AngledTF.setTextureSize(64, 32);
        this.AngledTF.mirror = true;
        this.setRotation(this.AngledTF, 0.7853982f, 0.0f, 0.0f);
        this.Anvil4 = new ModelRenderer(this, 27, 24);
        this.Anvil4.addBox(-3.0f, 0.0f, -2.0f, 6, 4, 4);
        this.Anvil4.setRotationPoint(1.0f, 16.0f, 0.0f);
        this.Anvil4.setTextureSize(64, 32);
        this.Anvil4.mirror = true;
        this.setRotation(this.Anvil4, 0.0f, 0.0f, 0.0f);
        this.AngledTR = new ModelRenderer(this, 26, 2);
        this.AngledTR.addBox(-2.0f, -2.0f, -2.0f, 1, 2, 4);
        this.AngledTR.setRotationPoint(-0.5f, 16.0f, 0.0f);
        this.AngledTR.setTextureSize(64, 32);
        this.AngledTR.mirror = true;
        this.setRotation(this.AngledTR, 0.0f, 0.0f, -0.7853982f);
        this.Base = new ModelRenderer(this, 23, 8);
        this.Base.addBox(-4.0f, 0.0f, -4.0f, 10, 2, 8);
        this.Base.setRotationPoint(-0.2f, 22.0f, 0.0f);
        this.Base.setTextureSize(64, 32);
        this.Base.mirror = true;
        this.setRotation(this.Base, 0.0f, 0.0f, 0.0f);
        this.AngledBF = new ModelRenderer(this, 0, 12);
        this.AngledBF.addBox(-3.0f, 0.0f, -2.0f, 6, 2, 1);
        this.AngledBF.setRotationPoint(1.0f, 20.0f, -0.5f);
        this.AngledBF.setTextureSize(64, 32);
        this.AngledBF.mirror = true;
        this.setRotation(this.AngledBF, -0.7853982f, 0.0f, 0.0f);
        this.AngledBR = new ModelRenderer(this, 26, 2);
        this.AngledBR.addBox(-2.0f, 0.0f, -2.0f, 1, 2, 4);
        this.AngledBR.setRotationPoint(-0.5f, 20.0f, 0.0f);
        this.AngledBR.setTextureSize(64, 32);
        this.AngledBR.mirror = true;
        this.setRotation(this.AngledBR, 0.0f, 0.0f, 0.7853982f);
        this.AngledBL = new ModelRenderer(this, 26, 2);
        this.AngledBL.addBox(1.0f, 0.0f, -2.0f, 1, 2, 4);
        this.AngledBL.setRotationPoint(2.5f, 20.0f, 0.0f);
        this.AngledBL.setTextureSize(64, 32);
        this.AngledBL.mirror = true;
        this.setRotation(this.AngledBL, 0.0f, 0.0f, -0.7853982f);
        this.AngledBB = new ModelRenderer(this, 0, 12);
        this.AngledBB.addBox(-3.0f, 0.0f, 1.0f, 6, 2, 1);
        this.AngledBB.setRotationPoint(1.0f, 20.0f, 0.5f);
        this.AngledBB.setTextureSize(64, 32);
        this.AngledBB.mirror = true;
        this.setRotation(this.AngledBB, 0.7853982f, 0.0f, 0.0f);
        this.Ar6 = new ModelRenderer(this, 0, 6);
        this.Ar6.addBox(-6.0f, 0.0f, -0.5f, 5, 1, 1);
        this.Ar6.setRotationPoint(-2.0f, 14.6f, -3.0f);
        this.Ar6.setTextureSize(64, 32);
        this.Ar6.mirror = true;
        this.setRotation(this.Ar6, 0.0f, 0.5061455f, 0.0698132f);
        this.stufffs = new ModelRenderer(this, 54, 0);
        this.stufffs.addBox(-1.0f, 0.0f, -1.0f, 2, 1, 2);
        this.stufffs.setRotationPoint(4.5f, 14.1f, 0.0f);
        this.stufffs.setTextureSize(64, 32);
        this.stufffs.mirror = true;
        this.setRotation(this.stufffs, 0.0f, 0.0f, 0.0f);
        this.AngledTL = new ModelRenderer(this, 26, 2);
        this.AngledTL.addBox(1.0f, -2.0f, -2.0f, 1, 2, 4);
        this.AngledTL.setRotationPoint(2.5f, 16.0f, 0.0f);
        this.AngledTL.setTextureSize(64, 32);
        this.AngledTL.mirror = true;
        this.setRotation(this.AngledTL, 0.0f, 0.0f, 0.7853982f);
        this.anvil2 = new ModelRenderer(this, 45, 0);
        this.anvil2.addBox(-1.0f, 0.0f, -1.0f, 2, 1, 2);
        this.anvil2.setRotationPoint(6.5f, 13.5f, 0.0f);
        this.anvil2.setTextureSize(64, 32);
        this.anvil2.mirror = true;
        this.setRotation(this.anvil2, 0.0f, 0.0f, 0.0f);
        this.Top = new ModelRenderer(this, 0, 16);
        this.Top.addBox(-3.5f, 0.0f, -3.5f, 9, 2, 7);
        this.Top.setRotationPoint(0.0f, 14.0f, 0.0f);
        this.Top.setTextureSize(64, 32);
        this.Top.mirror = true;
        this.setRotation(this.Top, 0.0f, 0.0f, 0.0f);
        this.Ar8 = new ModelRenderer(this, 27, 19);
        this.Ar8.addBox(-5.0f, 0.0f, -1.5f, 5, 1, 3);
        this.Ar8.setRotationPoint(-0.6f, 14.8f, 0.0f);
        this.Ar8.setTextureSize(64, 32);
        this.Ar8.mirror = true;
        this.setRotation(this.Ar8, 0.0f, 0.0f, 0.1047198f);
        this.Ar7 = new ModelRenderer(this, 0, 6);
        this.Ar7.addBox(-6.0f, 0.0f, -0.5f, 6, 1, 1);
        this.Ar7.setRotationPoint(-2.0f, 14.6f, 3.0f);
        this.Ar7.setTextureSize(64, 32);
        this.Ar7.mirror = true;
        this.setRotation(this.Ar7, 0.0f, -0.5061455f, 0.0698132f);
        this.Ar5 = new ModelRenderer(this, 0, 9);
        this.Ar5.addBox(-7.0f, 0.0f, -0.5f, 7, 1, 1);
        this.Ar5.setRotationPoint(-0.5f, 15.2f, 0.0f);
        this.Ar5.setTextureSize(64, 32);
        this.Ar5.mirror = true;
        this.setRotation(this.Ar5, 0.0f, 0.0f, 0.1396263f);
        this.BaseTop = new ModelRenderer(this, 0, 23);
        this.BaseTop.addBox(-4.5f, 0.0f, -3.5f, 9, 2, 7);
        this.BaseTop.setRotationPoint(1.0f, 20.0f, 0.0f);
        this.BaseTop.setTextureSize(64, 32);
        this.BaseTop.mirror = true;
        this.setRotation(this.BaseTop, 0.0f, 0.0f, 0.0f);
        this.AR1 = new ModelRenderer(this, 0, 6);
        this.AR1.addBox(-6.0f, 0.0f, -0.5f, 5, 1, 1);
        this.AR1.setRotationPoint(-2.0f, 14.1f, -3.0f);
        this.AR1.setTextureSize(64, 32);
        this.AR1.mirror = true;
        this.setRotation(this.AR1, 0.0f, 0.5061455f, 0.0f);
        this.Ar4 = new ModelRenderer(this, 0, 6);
        this.Ar4.addBox(-6.0f, 0.0f, -0.5f, 6, 1, 1);
        this.Ar4.setRotationPoint(-2.0f, 14.1f, 3.0f);
        this.Ar4.setTextureSize(64, 32);
        this.Ar4.mirror = true;
        this.setRotation(this.Ar4, 0.0f, -0.5061455f, 0.0f);
        this.Ar3 = new ModelRenderer(this, 0, 9);
        this.Ar3.addBox(-7.0f, 0.0f, -0.5f, 7, 1, 1);
        this.Ar3.setRotationPoint(-0.5f, 14.1f, 0.0f);
        this.Ar3.setTextureSize(64, 32);
        this.Ar3.mirror = true;
        this.setRotation(this.Ar3, 0.0f, 0.0f, 0.0f);
        this.Ar2 = new ModelRenderer(this, 27, 19);
        this.Ar2.addBox(-5.0f, 0.0f, -1.5f, 5, 1, 3);
        this.Ar2.setRotationPoint(-0.6f, 14.1f, 0.0f);
        this.Ar2.setTextureSize(64, 32);
        this.Ar2.mirror = true;
        this.setRotation(this.Ar2, 0.0f, 0.0f, 0.0f);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.AngledTB.render(f5);
        this.anvil3.render(f5);
        this.Anvil1.render(f5);
        this.AngledTF.render(f5);
        this.Anvil4.render(f5);
        this.AngledTR.render(f5);
        this.Base.render(f5);
        this.AngledBF.render(f5);
        this.AngledBR.render(f5);
        this.AngledBL.render(f5);
        this.AngledBB.render(f5);
        this.Ar6.render(f5);
        this.stufffs.render(f5);
        this.AngledTL.render(f5);
        this.anvil2.render(f5);
        this.Top.render(f5);
        this.Ar8.render(f5);
        this.Ar7.render(f5);
        this.Ar5.render(f5);
        this.BaseTop.render(f5);
        this.AR1.render(f5);
        this.Ar4.render(f5);
        this.Ar3.render(f5);
        this.Ar2.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    @Override
    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
        super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    }

    public void renderModel(TileEntityAnvil tile, float f5) {
        this.AngledTB.render(f5);
        this.anvil3.render(f5);
        this.Anvil1.render(f5);
        this.AngledTF.render(f5);
        this.Anvil4.render(f5);
        this.AngledTR.render(f5);
        this.Base.render(f5);
        this.AngledBF.render(f5);
        this.AngledBR.render(f5);
        this.AngledBL.render(f5);
        this.AngledBB.render(f5);
        this.Ar6.render(f5);
        this.stufffs.render(f5);
        this.AngledTL.render(f5);
        this.anvil2.render(f5);
        this.Top.render(f5);
        this.Ar8.render(f5);
        this.Ar7.render(f5);
        this.Ar5.render(f5);
        this.BaseTop.render(f5);
        this.AR1.render(f5);
        this.Ar4.render(f5);
        this.Ar3.render(f5);
        this.Ar2.render(f5);
    }
}

