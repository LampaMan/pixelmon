/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nonnull
 *  javax.vecmath.Vector3f
 */
package com.pixelmongenerations.client.models.blocks;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.IFrameCounter;
import com.pixelmongenerations.common.spawning.spawners.EnumWorldState;
import com.pixelmongenerations.common.spawning.spawners.SpawnerBase;
import javax.annotation.Nonnull;
import javax.vecmath.Vector3f;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class TemporalSmdRenderer<T extends TileEntity>
extends TileEntityRenderer<T> {
    private BlockModelHolder<GenericSmdModel> holderDay;
    ResourceLocation textureDay;
    private BlockModelHolder<GenericSmdModel> holderNight;
    ResourceLocation textureNight;
    private float angle = 0.0f;
    private Vector3f axis = new Vector3f(1.0f, 0.0f, 0.0f);

    public TemporalSmdRenderer(@Nonnull BlockModelHolder<GenericSmdModel> holderDay, @Nonnull ResourceLocation textureDay, @Nonnull BlockModelHolder<GenericSmdModel> holderNight, @Nonnull ResourceLocation textureNight) {
        this.holderDay = holderDay;
        this.textureDay = textureDay;
        this.holderNight = holderNight;
        this.textureNight = textureNight;
    }

    public TemporalSmdRenderer(ResourceLocation modelDay, ResourceLocation textureDay, ResourceLocation modelNight, ResourceLocation textureNight) {
        this(new BlockModelHolder<GenericSmdModel>(modelDay), textureDay, new BlockModelHolder<GenericSmdModel>(modelNight), textureNight);
    }

    public TemporalSmdRenderer(String pqcDayPath, String textureDay, String pqcNightPath, String textureNight) {
        this(new ResourceLocation("pixelmon", "models/" + pqcDayPath), textureDay == null ? null : new ResourceLocation("pixelmon:textures/blocks/" + textureDay), new ResourceLocation("pixelmon", "models/" + pqcNightPath), textureNight == null ? null : new ResourceLocation("pixelmon:textures/blocks/" + textureNight));
    }

    @Override
    public void renderTileEntity(T te, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        BlockModelHolder<GenericSmdModel> model;
        EnumWorldState worldState = SpawnerBase.getWorldState(Minecraft.getMinecraft().world);
        boolean isDayTime = worldState == EnumWorldState.dawn || worldState == EnumWorldState.day;
        this.bindTexture(isDayTime ? this.textureDay : this.textureNight);
        BlockModelHolder<GenericSmdModel> blockModelHolder = model = isDayTime ? this.holderDay : this.holderNight;
        if (te instanceof IFrameCounter && model.getModel() instanceof GenericSmdModel) {
            ((GenericSmdModel)model.getModel()).setFrame(((IFrameCounter)te).getFrame());
        }
        GlStateManager.pushMatrix();
        GlStateManager.rotate(this.angle, this.axis.x, this.axis.y, this.axis.z);
        GlStateManager.disableNormalize();
        GlStateManager.shadeModel(7425);
        GlStateManager.disableCull();
        model.render();
        GlStateManager.popMatrix();
    }

    public TemporalSmdRenderer<T> enableBlend() {
        this.blend = true;
        return this;
    }

    public TemporalSmdRenderer<T> disableCulling() {
        this.disableCulling = true;
        return this;
    }

    public TemporalSmdRenderer<T> disableLighting() {
        this.disableLighting = true;
        return this;
    }

    public TemporalSmdRenderer<T> setCorrectionAngles(int correctionAngles) {
        this.correctionAngles = correctionAngles;
        return this;
    }

    public TemporalSmdRenderer<T> setYOffset(float yOffset) {
        this.yOffset = yOffset;
        return this;
    }

    public TemporalSmdRenderer<T> rotate(float d, float axisX, float axisY, float axisZ) {
        this.angle = d;
        this.axis = new Vector3f(axisX, axisY, axisZ);
        return this;
    }

    public TemporalSmdRenderer<T> scale(float scale) {
        this.scale = scale;
        return this;
    }
}

