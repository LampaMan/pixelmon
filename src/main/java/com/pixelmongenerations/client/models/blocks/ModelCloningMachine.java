/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.blocks;

import com.pixelmongenerations.client.models.ModelCustomWrapper;
import com.pixelmongenerations.client.models.PixelmonModelRenderer;
import com.pixelmongenerations.client.models.blocks.ModelEntityBlock;
import com.pixelmongenerations.client.models.obj.ObjLoader;
import com.pixelmongenerations.common.block.tileEntities.TileEntityCloningMachine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityDecorativeBase;
import net.minecraft.util.ResourceLocation;

public class ModelCloningMachine
extends ModelEntityBlock {
    ResourceLocation base = new ResourceLocation("pixelmon:models/blocks/cloning_machine/cloner_base.obj");
    ResourceLocation glass1 = new ResourceLocation("pixelmon:models/blocks/cloning_machine/cloner_glass1.obj");
    ResourceLocation glass2 = new ResourceLocation("pixelmon:models/blocks/cloning_machine/cloner_glass2.obj");
    ResourceLocation laser = new ResourceLocation("pixelmon:models/blocks/cloning_machine/cloner_laser.obj");
    ResourceLocation balls = new ResourceLocation("pixelmon:models/blocks/cloning_machine/cloner_laserballs.obj");
    PixelmonModelRenderer machine = new PixelmonModelRenderer(this);
    PixelmonModelRenderer glass;
    PixelmonModelRenderer las;
    PixelmonModelRenderer ball;
    boolean travDown = true;

    public ModelCloningMachine() {
        this.machine.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(this.base)));
        this.las = new PixelmonModelRenderer(this);
        this.las.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(this.laser)));
        this.las.setTransparent(1.0f);
        this.ball = new PixelmonModelRenderer(this);
        this.ball.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(this.balls)));
        this.glass = new PixelmonModelRenderer(this);
        this.glass.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(this.glass1)));
        this.glass.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(this.glass2)));
        this.glass.setTransparent(1.0f);
    }

    @Override
    public void renderTileEntity(TileEntityDecorativeBase tileEnt, float scale) {
        TileEntityCloningMachine te = (TileEntityCloningMachine)tileEnt;
        this.machine.render(0.0625f);
        this.ball.rotationPointY = te.lasPos;
        this.ball.render(0.0625f);
    }

    public void renderGlass(TileEntityCloningMachine tileEntity, float f) {
        this.las.rotationPointY = this.ball.rotationPointY;
        this.las.render(0.0625f);
        this.glass.render(0.0625f);
    }
}

