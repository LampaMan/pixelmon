/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.blocks;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelTV
extends ModelBase {
    ModelRenderer FrontLeft;
    ModelRenderer SideRightTop;
    ModelRenderer SideLeft;
    ModelRenderer Front;
    ModelRenderer FrontRight;
    ModelRenderer Top1;
    ModelRenderer BaseRight;
    ModelRenderer Back1;
    ModelRenderer FrontTop;
    ModelRenderer Top2;
    ModelRenderer BaseLeft;
    ModelRenderer SideLeftTop;
    ModelRenderer Screen;
    ModelRenderer SideRight;
    ModelRenderer Back2;

    public ModelTV() {
        this.textureWidth = 64;
        this.textureHeight = 64;
        this.FrontLeft = new ModelRenderer(this, 32, 2);
        this.FrontLeft.addBox(0.0f, 0.0f, 0.0f, 3, 7, 6);
        this.FrontLeft.setRotationPoint(3.5f, 17.0f, -6.0f);
        this.FrontLeft.setTextureSize(64, 64);
        this.FrontLeft.mirror = true;
        this.setRotation(this.FrontLeft, 0.0f, -1.003822f, 0.0f);
        this.SideRightTop = new ModelRenderer(this, 0, 0);
        this.SideRightTop.addBox(0.0f, -2.0f, 0.0f, 2, 2, 9);
        this.SideRightTop.setRotationPoint(-5.7f, 7.0f, -3.7f);
        this.SideRightTop.setTextureSize(64, 64);
        this.SideRightTop.mirror = true;
        this.setRotation(this.SideRightTop, 0.0f, 0.0f, 0.7243116f);
        this.SideLeft = new ModelRenderer(this, 0, 0);
        this.SideLeft.addBox(-2.0f, 0.0f, 0.0f, 2, 7, 9);
        this.SideLeft.setRotationPoint(5.7f, 7.0f, -3.7f);
        this.SideLeft.setTextureSize(64, 64);
        this.SideLeft.mirror = true;
        this.setRotation(this.SideLeft, 0.0f, 0.0f, 0.0f);
        this.Front = new ModelRenderer(this, 38, 19);
        this.Front.addBox(0.0f, 0.0f, 0.0f, 7, 7, 4);
        this.Front.setRotationPoint(-3.5f, 17.0f, -6.0f);
        this.Front.setTextureSize(64, 64);
        this.Front.mirror = true;
        this.setRotation(this.Front, 0.0f, 0.0f, 0.0f);
        this.FrontRight = new ModelRenderer(this, 34, 2);
        this.FrontRight.addBox(-3.0f, 0.0f, 0.0f, 3, 7, 6);
        this.FrontRight.setRotationPoint(-3.5f, 17.0f, -6.0f);
        this.FrontRight.setTextureSize(64, 64);
        this.FrontRight.mirror = true;
        this.setRotation(this.FrontRight, 0.0f, 1.003822f, 0.0f);
        this.Top1 = new ModelRenderer(this, 0, 0);
        this.Top1.addBox(0.0f, -2.0f, 0.0f, 4, 2, 9);
        this.Top1.setRotationPoint(-4.4f, 7.5f, -3.7f);
        this.Top1.setTextureSize(64, 64);
        this.Top1.mirror = true;
        this.setRotation(this.Top1, 0.0f, 0.0f, 0.0f);
        this.BaseRight = new ModelRenderer(this, 0, 45);
        this.BaseRight.addBox(-4.0f, 0.0f, 0.0f, 11, 10, 9);
        this.BaseRight.setRotationPoint(-1.7f, 14.0f, -3.7f);
        this.BaseRight.setTextureSize(64, 64);
        this.BaseRight.mirror = true;
        this.setRotation(this.BaseRight, 0.0f, 0.0f, 0.0f);
        this.Back1 = new ModelRenderer(this, 0, 0);
        this.Back1.addBox(-8.0f, 0.0f, 0.0f, 8, 8, 1);
        this.Back1.setRotationPoint(3.7f, 7.0f, 5.5f);
        this.Back1.setTextureSize(64, 64);
        this.Back1.mirror = true;
        this.setRotation(this.Back1, 0.0f, 0.0f, 0.0f);
        this.FrontTop = new ModelRenderer(this, 0, 0);
        this.FrontTop.addBox(-3.5f, 0.0f, -2.0f, 7, 2, 3);
        this.FrontTop.setRotationPoint(0.0f, 16.0f, -3.5f);
        this.FrontTop.setTextureSize(64, 64);
        this.FrontTop.mirror = true;
        this.setRotation(this.FrontTop, 0.5948606f, 0.0f, 0.0f);
        this.Top2 = new ModelRenderer(this, 0, 0);
        this.Top2.addBox(-5.0f, -2.0f, 0.0f, 5, 2, 9);
        this.Top2.setRotationPoint(4.4f, 7.5f, -3.7f);
        this.Top2.setTextureSize(64, 64);
        this.Top2.mirror = true;
        this.setRotation(this.Top2, 0.0f, 0.0f, 0.0f);
        this.BaseLeft = new ModelRenderer(this, 0, 45);
        this.BaseLeft.addBox(-4.0f, 0.0f, 0.0f, 11, 10, 9);
        this.BaseLeft.setRotationPoint(-1.3f, 14.0f, -3.7f);
        this.BaseLeft.setTextureSize(64, 64);
        this.BaseLeft.mirror = true;
        this.setRotation(this.BaseLeft, 0.0f, 0.0f, 0.0f);
        this.SideLeftTop = new ModelRenderer(this, 0, 0);
        this.SideLeftTop.addBox(-2.0f, -2.0f, 0.0f, 2, 2, 9);
        this.SideLeftTop.setRotationPoint(5.7f, 7.0f, -3.7f);
        this.SideLeftTop.setTextureSize(64, 64);
        this.SideLeftTop.mirror = true;
        this.setRotation(this.SideLeftTop, 0.0f, 0.0f, -0.7243116f);
        this.Screen = new ModelRenderer(this, 0, 23);
        this.Screen.addBox(-8.0f, 0.0f, 0.0f, 8, 8, 9);
        this.Screen.setRotationPoint(3.7f, 7.0f, -3.5f);
        this.Screen.setTextureSize(64, 64);
        this.Screen.mirror = true;
        this.setRotation(this.Screen, 0.0f, 0.0f, 0.0f);
        this.SideRight = new ModelRenderer(this, 0, 0);
        this.SideRight.addBox(-2.0f, 0.0f, 0.0f, 2, 7, 9);
        this.SideRight.setRotationPoint(-3.7f, 7.0f, -3.7f);
        this.SideRight.setTextureSize(64, 64);
        this.SideRight.mirror = true;
        this.setRotation(this.SideRight, 0.0f, 0.0f, 0.0f);
        this.Back2 = new ModelRenderer(this, 0, 0);
        this.Back2.addBox(-8.0f, 0.0f, 0.0f, 7, 7, 2);
        this.Back2.setRotationPoint(4.2f, 7.5f, 5.5f);
        this.Back2.setTextureSize(64, 64);
        this.Back2.mirror = true;
        this.setRotation(this.Back2, 0.0f, 0.0f, 0.0f);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.FrontLeft.render(f5);
        this.SideRightTop.render(f5);
        this.SideLeft.render(f5);
        this.Front.render(f5);
        this.FrontRight.render(f5);
        this.Top1.render(f5);
        this.BaseRight.render(f5);
        this.Back1.render(f5);
        this.FrontTop.render(f5);
        this.Top2.render(f5);
        this.BaseLeft.render(f5);
        this.SideLeftTop.render(f5);
        this.Screen.render(f5);
        this.SideRight.render(f5);
        this.Back2.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    @Override
    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
        super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    }
}

