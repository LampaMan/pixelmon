/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nonnull
 *  javax.vecmath.Vector3f
 */
package com.pixelmongenerations.client.models.blocks;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import javax.annotation.Nonnull;
import javax.vecmath.Vector3f;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class TransparentSmdRenderer<T extends TileEntity>
extends TileEntityRenderer<T> {
    private BlockModelHolder<GenericSmdModel> mainModel;
    private BlockModelHolder<GenericSmdModel> transparentModel;
    private ResourceLocation texture;
    private float angle = 0.0f;
    private Vector3f axis = new Vector3f(1.0f, 0.0f, 0.0f);

    public TransparentSmdRenderer(@Nonnull BlockModelHolder<GenericSmdModel> mainModel, @Nonnull BlockModelHolder<GenericSmdModel> transparentModel, @Nonnull ResourceLocation texture) {
        this.mainModel = mainModel;
        this.transparentModel = transparentModel;
        this.texture = texture;
    }

    public TransparentSmdRenderer(ResourceLocation mainModel, ResourceLocation transparentModel, ResourceLocation texture) {
        this(new BlockModelHolder<GenericSmdModel>(mainModel), new BlockModelHolder<GenericSmdModel>(transparentModel), texture);
    }

    @Override
    public void renderTileEntity(T te, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        if (te instanceof ISpecialTexture) {
            this.bindTexture(((ISpecialTexture)te).getTexture());
        } else {
            this.bindTexture(this.texture);
        }
        GlStateManager.pushMatrix();
        GlStateManager.rotate(this.angle, this.axis.x, this.axis.y, this.axis.z);
        GlStateManager.disableNormalize();
        GlStateManager.shadeModel(7425);
        GlStateManager.disableCull();
        this.mainModel.render();
        this.renderTransparent();
        GlStateManager.popMatrix();
    }

    public void renderTransparent() {
        GlStateManager.enableBlend();
        GlStateManager.blendFunc(770, 771);
        GlStateManager.depthMask(false);
        this.transparentModel.render();
        GlStateManager.disableBlend();
        GlStateManager.depthMask(true);
    }

    public TransparentSmdRenderer<T> enableBlend() {
        this.blend = true;
        return this;
    }

    public TransparentSmdRenderer<T> disableCulling() {
        this.disableCulling = true;
        return this;
    }

    public TransparentSmdRenderer<T> disableLighting() {
        this.disableLighting = true;
        return this;
    }

    public TransparentSmdRenderer<T> setCorrectionAngles(int correctionAngles) {
        this.correctionAngles = correctionAngles;
        return this;
    }

    public TransparentSmdRenderer<T> setYOffset(float yOffset) {
        this.yOffset = yOffset;
        return this;
    }

    public TransparentSmdRenderer<T> rotate(float d, float axisX, float axisY, float axisZ) {
        this.angle = d;
        this.axis = new Vector3f(axisX, axisY, axisZ);
        return this;
    }

    public TransparentSmdRenderer<T> scale(float scale) {
        this.scale = scale;
        return this;
    }
}

