/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.blocks;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelFoldingChair
extends ModelBase {
    ModelRenderer SeatSide1;
    ModelRenderer Leg1;
    ModelRenderer Leg2;
    ModelRenderer Arm1;
    ModelRenderer Leg3;
    ModelRenderer Arm2;
    ModelRenderer MainSeat;
    ModelRenderer SeatSide2;
    ModelRenderer Leg4;

    public ModelFoldingChair() {
        this.textureWidth = 64;
        this.textureHeight = 64;
        this.SeatSide1 = new ModelRenderer(this, 0, 18);
        this.SeatSide1.addBox(0.0f, 0.0f, 0.0f, 4, 2, 16);
        this.SeatSide1.setRotationPoint(8.0f, 9.0f, -8.0f);
        this.SeatSide1.setTextureSize(64, 64);
        this.SeatSide1.mirror = true;
        this.setRotation(this.SeatSide1, 0.0f, 0.0f, 1.003822f);
        this.Leg1 = new ModelRenderer(this, 0, 36);
        this.Leg1.addBox(0.0f, -20.0f, 0.0f, 2, 20, 2);
        this.Leg1.setRotationPoint(-8.0f, 23.0f, 6.0f);
        this.Leg1.setTextureSize(64, 64);
        this.Leg1.mirror = true;
        this.setRotation(this.Leg1, 0.0f, 0.0f, 0.8179294f);
        this.Leg2 = new ModelRenderer(this, 0, 36);
        this.Leg2.addBox(-2.0f, -20.0f, 0.0f, 2, 20, 2);
        this.Leg2.setRotationPoint(8.0f, 23.0f, -8.0f);
        this.Leg2.setTextureSize(64, 64);
        this.Leg2.mirror = true;
        this.setRotation(this.Leg2, 0.0f, 0.0f, -0.8179294f);
        this.Arm1 = new ModelRenderer(this, 9, 37);
        this.Arm1.addBox(0.0f, 0.0f, 0.0f, 2, 2, 16);
        this.Arm1.setRotationPoint(6.0f, 22.0f, -8.0f);
        this.Arm1.setTextureSize(64, 64);
        this.Arm1.mirror = true;
        this.setRotation(this.Arm1, 0.0f, 0.0f, 0.0f);
        this.Leg3 = new ModelRenderer(this, 0, 36);
        this.Leg3.addBox(-2.0f, -20.0f, 0.0f, 2, 20, 2);
        this.Leg3.setRotationPoint(8.0f, 23.0f, 6.0f);
        this.Leg3.setTextureSize(64, 64);
        this.Leg3.mirror = true;
        this.setRotation(this.Leg3, 0.0f, 0.0f, -0.8179294f);
        this.Arm2 = new ModelRenderer(this, 9, 37);
        this.Arm2.addBox(0.0f, 0.0f, 0.0f, 2, 2, 16);
        this.Arm2.setRotationPoint(-8.0f, 22.0f, -8.0f);
        this.Arm2.setTextureSize(64, 64);
        this.Arm2.mirror = true;
        this.setRotation(this.Arm2, 0.0f, 0.0f, 0.0f);
        this.MainSeat = new ModelRenderer(this, 0, 0);
        this.MainSeat.addBox(0.0f, 0.0f, 0.0f, 16, 2, 16);
        this.MainSeat.setRotationPoint(-8.0f, 9.0f, -8.0f);
        this.MainSeat.setTextureSize(64, 64);
        this.MainSeat.mirror = true;
        this.setRotation(this.MainSeat, 0.0f, 0.0f, 0.0f);
        this.SeatSide2 = new ModelRenderer(this, 0, 18);
        this.SeatSide2.addBox(-4.0f, 0.0f, 0.0f, 4, 2, 16);
        this.SeatSide2.setRotationPoint(-8.0f, 9.0f, -8.0f);
        this.SeatSide2.setTextureSize(64, 64);
        this.SeatSide2.mirror = true;
        this.setRotation(this.SeatSide2, 0.0f, 0.0f, -1.003822f);
        this.Leg4 = new ModelRenderer(this, 0, 36);
        this.Leg4.addBox(0.0f, -20.0f, 0.0f, 2, 20, 2);
        this.Leg4.setRotationPoint(-8.0f, 23.0f, -8.0f);
        this.Leg4.setTextureSize(64, 64);
        this.Leg4.mirror = true;
        this.setRotation(this.Leg4, 0.0f, 0.0f, 0.8179294f);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.SeatSide1.render(f5);
        this.Leg1.render(f5);
        this.Leg2.render(f5);
        this.Arm1.render(f5);
        this.Leg3.render(f5);
        this.Arm2.render(f5);
        this.MainSeat.render(f5);
        this.SeatSide2.render(f5);
        this.Leg4.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }
}

