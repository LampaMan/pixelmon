/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.blocks;

import com.pixelmongenerations.client.models.blocks.ModelPillar;

public class ModelHauntedPillar
extends ModelPillar {
    public ModelHauntedPillar() {
        super("haunted");
    }
}

