/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.blocks;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;

public class AltarHolder
extends BlockModelHolder<GenericSmdModel> {
    private BlockModelHolder<GenericSmdModel> secondary;
    private final ResourceLocation glass;

    public AltarHolder(ResourceLocation model, ResourceLocation secondary, ResourceLocation glass) {
        super(model);
        this.secondary = new BlockModelHolder(secondary);
        this.glass = glass;
    }

    @Override
    public void render() {
        GlStateManager.rotate(180.0f, 0.0f, 0.0f, 1.0f);
        ((GenericSmdModel)this.getModel()).render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, 1.0f);
        this.renderGlass(1.0f);
    }

    @Override
    public void render(float scale) {
        ((GenericSmdModel)this.getModel()).render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, scale);
        this.renderGlass(scale);
    }

    public void renderGlass(float scale) {
        GlStateManager.enableBlend();
        GlStateManager.blendFunc(770, 771);
        GlStateManager.depthMask(false);
        Minecraft.getMinecraft().getTextureManager().bindTexture(this.glass);
        ((GenericSmdModel)this.secondary.getModel()).render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, scale);
        GlStateManager.disableBlend();
        GlStateManager.depthMask(true);
    }
}

