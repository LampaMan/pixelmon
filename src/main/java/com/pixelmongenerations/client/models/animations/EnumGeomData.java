/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.animations;

public enum EnumGeomData {
    xloc,
    yloc,
    zloc,
    xrot,
    yrot,
    zrot;


    public boolean isRotation() {
        return this == xrot || this == yrot || this == zrot;
    }
}

