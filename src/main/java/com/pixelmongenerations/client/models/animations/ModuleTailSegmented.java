/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.animations;

import com.pixelmongenerations.client.models.animations.ModuleTailBasic;
import com.pixelmongenerations.common.entity.pixelmon.Entity2HasModel;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;

public class ModuleTailSegmented
extends ModuleTailBasic {
    public ModelRenderer[] tailParts;
    public int[] lengths;
    float phaseOffset;
    float zeroPoint;
    public float initialRotation;
    float tailRotationLimit;
    int tailType;
    float currentAngleY;

    public ModuleTailSegmented(ModelRenderer tail, int tailType, float zeroPoint, float initialRotation, float tailRotationLimit, float TailSpeed, float phaseOffset, ModelRenderer ... tailArgs) {
        super(tail, 0.0f, 0.0f, TailSpeed);
        this.tailType = tailType;
        this.tailParts = tailArgs;
        this.phaseOffset = phaseOffset;
        this.zeroPoint = zeroPoint;
        this.initialRotation = initialRotation;
        this.tailRotationLimit = tailRotationLimit;
        this.TailSpeed = TailSpeed;
        this.lengths = new int[this.tailParts.length];
    }

    @Override
    public void walk(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
        if (this.tailType == 0) {
            for (int i = 0; i < this.tailParts.length; ++i) {
                this.tailParts[i].rotateAngleX = i == 0 ? MathHelper.sin(f2 * this.TailSpeed) * MathHelper.sin(f2 * this.TailSpeed) * this.tailRotationLimit / (float)this.tailParts.length + this.initialRotation : MathHelper.sin(f2 * this.TailSpeed) * MathHelper.sin(f2 * this.TailSpeed) * this.tailRotationLimit / (float)this.tailParts.length;
            }
        } else {
            for (int i = 0; i < this.tailParts.length; ++i) {
                if (i == 0) {
                    this.tailParts[i].rotateAngleX = MathHelper.sin(f2 * this.TailSpeed) * this.tailRotationLimit / (float)this.tailParts.length + this.initialRotation;
                    this.currentAngleY = this.tailParts[i].rotateAngleY = this.tailRotationLimit / 3.0f * MathHelper.cos(f2 * this.TailSpeed);
                    continue;
                }
                this.tailParts[i].rotateAngleX = MathHelper.sin(f2 * this.TailSpeed) * this.tailRotationLimit / (float)this.tailParts.length;
                this.tailParts[i].rotateAngleY = (float)Math.exp(-0.1 * (double)i) * this.tailRotationLimit / 2.0f * MathHelper.cos(f2 * this.TailSpeed - (float)i) - this.currentAngleY;
                this.currentAngleY = this.tailParts[i].rotateAngleY + this.currentAngleY;
            }
        }
    }

    @Override
    public void fly(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
        this.walk(entity, f, f1, f2, f3, f4);
        if (this.tailType == 0) {
            for (int i = 0; i < this.tailParts.length; ++i) {
                this.tailParts[i].rotateAngleX = i == 0 ? MathHelper.sin(f2 * this.TailSpeed) * MathHelper.sin(f2 * this.TailSpeed) * this.tailRotationLimit / (float)this.tailParts.length + this.initialRotation : MathHelper.sin(f2 * this.TailSpeed) * MathHelper.sin(f2 * this.TailSpeed) * this.tailRotationLimit / (float)this.tailParts.length;
            }
        }
    }
}

