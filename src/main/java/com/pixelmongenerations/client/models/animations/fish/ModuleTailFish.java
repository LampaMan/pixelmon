/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.animations.fish;

import com.pixelmongenerations.client.models.animations.EnumGeomData;
import com.pixelmongenerations.client.models.animations.IModulized;
import com.pixelmongenerations.client.models.animations.Module;
import com.pixelmongenerations.client.models.animations.ModulizedRenderWrapper;
import com.pixelmongenerations.common.entity.pixelmon.Entity2HasModel;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;

public class ModuleTailFish
extends Module {
    IModulized tail;
    float TailRotationLimit;
    float TailSpeed;
    float TailInitY;
    float TailInitZ;
    float TailOrientation;
    float TurningSpeed;

    public ModuleTailFish(IModulized tail, float TailOrientation, float TailRotationLimit, float TailSpeed) {
        this.tail = tail;
        this.TailSpeed = TailSpeed;
        this.TailRotationLimit = TailRotationLimit;
        this.TailOrientation = TailOrientation;
        this.TailInitY = tail.getValue(EnumGeomData.yrot);
        this.TailInitZ = tail.getValue(EnumGeomData.zrot);
    }

    public ModuleTailFish(ModelRenderer tail, float TailOrientation, float TailRotationLimit, float TailSpeed) {
        this(new ModulizedRenderWrapper(tail), TailOrientation, TailRotationLimit, TailSpeed);
    }

    @Override
    public void walk(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
        this.yrD = MathHelper.cos((float)Math.toRadians(this.TailOrientation)) * MathHelper.cos(f * this.TailSpeed) * (float)Math.PI * f1 * this.TailRotationLimit;
        this.xrD = MathHelper.sin((float)Math.toRadians(this.TailOrientation)) * MathHelper.cos(f * this.TailSpeed) * (float)Math.PI * f1 * this.TailRotationLimit;
        this.tail.setValue(this.yrD + this.TailInitY, EnumGeomData.yrot);
        this.tail.setValue(this.xrD + this.TailInitY, EnumGeomData.xrot);
    }

    @Override
    public void fly(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
    }
}

