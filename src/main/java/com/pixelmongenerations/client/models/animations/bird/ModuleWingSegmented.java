/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.animations.bird;

import com.pixelmongenerations.client.models.animations.bird.EnumWing;
import com.pixelmongenerations.client.models.animations.bird.ModuleWing;
import com.pixelmongenerations.common.entity.pixelmon.Entity2HasModel;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;

public class ModuleWingSegmented
extends ModuleWing {
    public ModelRenderer[] wingParts;
    public int[] lengths;
    float phaseOffset;
    float zeroPoint;
    float initialRotation;

    public ModuleWingSegmented(ModelRenderer wing, EnumWing WingVariable, float WingOrientation, float zeroPoint, float initialRotation, float WingRotationLimit, float WingSpeed, float phaseOffset, ModelRenderer ... wingArgs) {
        super(wing, WingVariable, WingOrientation, WingRotationLimit, WingSpeed);
        this.wingParts = wingArgs;
        this.phaseOffset = phaseOffset;
        this.zeroPoint = zeroPoint;
        this.initialRotation = initialRotation;
        this.lengths = new int[this.wingParts.length];
    }

    @Override
    public void walk(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
        for (int i = 0; i < this.wingParts.length; ++i) {
            if (i == 0) {
                this.wingParts[i].rotateAngleY = MathHelper.cos((float)Math.toRadians(this.WingOrientation)) * this.WingDirection * ((float)Math.pow(MathHelper.cos(f2 * this.WingSpeed - (float)i / this.phaseOffset), 2.0) * (float)Math.PI - this.zeroPoint) * this.WingRotationLimit / (float)this.wingParts.length + this.WingDirection * (float)((double)MathHelper.cos((float)Math.toRadians(this.WingOrientation)) * Math.toRadians(this.initialRotation));
                this.wingParts[i].rotateAngleZ = MathHelper.sin((float)Math.toRadians(this.WingOrientation)) * this.WingDirection * ((float)Math.pow(MathHelper.cos(f2 * this.WingSpeed - (float)i / this.phaseOffset), 2.0) * (float)Math.PI - this.zeroPoint) * this.WingRotationLimit / (float)this.wingParts.length + this.WingDirection * (float)((double)MathHelper.sin((float)Math.toRadians(this.WingOrientation)) * Math.toRadians(this.initialRotation));
                continue;
            }
            this.wingParts[i].rotateAngleY = MathHelper.cos((float)Math.toRadians(this.WingOrientation)) * this.WingDirection * ((float)Math.pow(MathHelper.cos(f2 * this.WingSpeed - (float)i / this.phaseOffset), 2.0) * (float)Math.PI - this.zeroPoint) * this.WingRotationLimit / (float)(this.wingParts.length * i);
            this.wingParts[i].rotateAngleZ = MathHelper.sin((float)Math.toRadians(this.WingOrientation)) * this.WingDirection * ((float)Math.pow(MathHelper.cos(f2 * this.WingSpeed - (float)i / this.phaseOffset), 2.0) * (float)Math.PI - this.zeroPoint) * this.WingRotationLimit / (float)(this.wingParts.length * i);
        }
    }

    @Override
    public void fly(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
        this.walk(entity, f, f1, f2, f3, f4);
    }
}

