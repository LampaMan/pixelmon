/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.animations.serpent;

import com.pixelmongenerations.client.models.animations.ModuleHead;
import com.pixelmongenerations.client.models.animations.SkeletonBase;
import com.pixelmongenerations.common.entity.pixelmon.Entity2HasModel;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;

public class SkeletonSerpent
extends SkeletonBase {
    public ModelRenderer[] bodyParts;
    public int[] lengths;
    float animationAngle;
    float animationSpeed;
    float currentAngleY;
    float currentAngleX;
    float topAngle;
    float dampeningFactor;
    float phaseoffset;

    public SkeletonSerpent(ModelRenderer body, ModuleHead headModule, float animationAngle, float topAngle, float dampeningFactor, float animationSpeed, float phaseoffset, ModelRenderer ... bodyArgs) {
        super(body);
        this.modules.add(headModule);
        this.animationAngle = animationAngle;
        this.topAngle = topAngle;
        this.dampeningFactor = dampeningFactor;
        this.animationSpeed = animationSpeed;
        this.phaseoffset = phaseoffset;
        this.bodyParts = bodyArgs;
        this.lengths = new int[this.bodyParts.length];
    }

    @Override
    public void walk(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
        for (int i = 0; i < this.bodyParts.length; ++i) {
            if (i == 0) {
                this.currentAngleY = this.bodyParts[i].rotateAngleY = MathHelper.cos((float)Math.toRadians(this.animationAngle)) * this.topAngle * MathHelper.cos(f * this.animationSpeed);
                this.currentAngleX = this.bodyParts[i].rotateAngleX = MathHelper.sin((float)Math.toRadians(this.animationAngle)) * this.topAngle * MathHelper.cos(f * this.animationSpeed);
                continue;
            }
            this.bodyParts[i].rotateAngleY = MathHelper.cos((float)Math.toRadians(this.animationAngle)) * ((float)Math.exp(this.dampeningFactor * (float)i) * this.topAngle * MathHelper.cos(f * this.animationSpeed + (float)i * this.phaseoffset) - this.currentAngleY);
            this.bodyParts[i].rotateAngleX = MathHelper.sin((float)Math.toRadians(this.animationAngle)) * ((float)Math.exp(this.dampeningFactor * (float)i) * this.topAngle * MathHelper.cos(f * this.animationSpeed + (float)i * this.phaseoffset) - this.currentAngleX);
            this.currentAngleY = this.bodyParts[i].rotateAngleY + this.currentAngleY;
            this.currentAngleX = this.bodyParts[i].rotateAngleX + this.currentAngleX;
        }
    }

    @Override
    public void fly(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
        for (int i = 0; i < this.bodyParts.length; ++i) {
            if (i == 0) {
                this.currentAngleY = this.bodyParts[i].rotateAngleY = MathHelper.cos((float)Math.toRadians(this.animationAngle)) * this.topAngle * MathHelper.cos(f * this.animationSpeed);
                this.currentAngleX = this.bodyParts[i].rotateAngleX = MathHelper.sin((float)Math.toRadians(this.animationAngle)) * this.topAngle * MathHelper.cos(f * this.animationSpeed);
                continue;
            }
            this.bodyParts[i].rotateAngleY = MathHelper.cos((float)Math.toRadians(this.animationAngle)) * ((float)Math.exp(this.dampeningFactor * (float)i) * this.topAngle * MathHelper.cos(f * this.animationSpeed + (float)i * this.phaseoffset) - this.currentAngleY);
            this.bodyParts[i].rotateAngleX = MathHelper.sin((float)Math.toRadians(this.animationAngle)) * ((float)Math.exp(this.dampeningFactor * (float)i) * this.topAngle * MathHelper.cos(f * this.animationSpeed + (float)i * this.phaseoffset) - this.currentAngleX);
            this.currentAngleY = this.bodyParts[i].rotateAngleY + this.currentAngleY;
            this.currentAngleX = this.bodyParts[i].rotateAngleX + this.currentAngleX;
        }
    }
}

