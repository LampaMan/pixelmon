/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.animations;

import com.pixelmongenerations.client.models.animations.EnumGeomData;
import com.pixelmongenerations.client.models.animations.IModulized;
import net.minecraft.client.model.ModelRenderer;

public class ModulizedRenderWrapper
implements IModulized {
    ModelRenderer renderer;
    float x;
    float y;
    float z;
    float xr;
    float yr;
    float zr;
    float x0;
    float y0;
    float z0;
    float xr0;
    float yr0;
    float zr0;

    public ModulizedRenderWrapper(ModelRenderer renderer) {
        this.renderer = renderer;
    }

    @Override
    public float getValue(EnumGeomData d) {
        switch (d) {
            case xloc: {
                return -this.renderer.rotationPointX;
            }
            case yloc: {
                return -this.renderer.rotationPointY;
            }
            case zloc: {
                return -this.renderer.rotationPointZ;
            }
            case xrot: {
                return this.renderer.rotateAngleX;
            }
            case yrot: {
                return this.renderer.rotateAngleY;
            }
            case zrot: {
                return this.renderer.rotateAngleZ;
            }
        }
        return -1.0f;
    }

    @Override
    public float setValue(float value, EnumGeomData d) {
        switch (d) {
            case xloc: {
                this.renderer.rotationPointX = -value;
                break;
            }
            case yloc: {
                this.renderer.rotationPointY = -value;
                break;
            }
            case zloc: {
                this.renderer.rotationPointY = -value;
                break;
            }
            case xrot: {
                this.renderer.rotateAngleX = value;
                break;
            }
            case yrot: {
                this.renderer.rotateAngleY = value;
                break;
            }
            case zrot: {
                this.renderer.rotateAngleZ = value;
            }
        }
        return value;
    }
}

