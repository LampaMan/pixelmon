/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.animations;

import com.pixelmongenerations.client.models.animations.EnumGeomData;
import com.pixelmongenerations.client.models.animations.IModulized;
import com.pixelmongenerations.client.models.animations.Module;
import com.pixelmongenerations.client.models.animations.ModulizedRenderWrapper;
import com.pixelmongenerations.common.entity.pixelmon.Entity2HasModel;
import net.minecraft.client.model.ModelRenderer;

public class ModuleHead
extends Module {
    IModulized head;
    float headStartAngleX;
    float headStartAngleY;
    public float ymaximum;
    public float xmaximum;
    public float xoffset;
    public float yoffset;
    public int headAxis;
    public int ydirection;

    public ModuleHead(IModulized head) {
        this.head = head;
        this.headStartAngleX = this.head.getValue(EnumGeomData.xrot);
        this.headStartAngleY = this.head.getValue(EnumGeomData.yrot);
    }

    public ModuleHead(ModelRenderer head) {
        this(new ModulizedRenderWrapper(head));
    }

    @Override
    public void walk(Entity2HasModel entity, float f, float f1, float f2, float rotateAnglePitch, float rotateAngleYaw) {
        this.xrD = rotateAngleYaw * ((float)Math.PI / 180);
        this.yrD = rotateAnglePitch * ((float)Math.PI / 180);
        this.zrD = rotateAnglePitch * ((float)Math.PI / 180);
        float xrotation = -this.xoffset + this.xrD + this.headStartAngleX;
        if (this.xmaximum != 0.0f) {
            if (xrotation > this.xmaximum * ((float)Math.PI / 180)) {
                xrotation = this.xmaximum * ((float)Math.PI / 180);
            }
            if (xrotation < -this.xmaximum * ((float)Math.PI / 180)) {
                xrotation = -this.xmaximum * ((float)Math.PI / 180);
            }
        }
        this.head.setValue(xrotation, EnumGeomData.xrot);
        float yrotation = -this.yoffset + this.yrD + this.headStartAngleY;
        if (this.ymaximum != 0.0f) {
            if (yrotation > this.ymaximum * ((float)Math.PI / 180)) {
                yrotation = this.ymaximum * ((float)Math.PI / 180);
            }
            if (yrotation < -this.ymaximum * ((float)Math.PI / 180)) {
                yrotation = -this.ymaximum * ((float)Math.PI / 180);
            }
        }
        if (this.headAxis == 1) {
            if (this.ydirection == 1) {
                this.head.setValue(yrotation, EnumGeomData.zrot);
            } else {
                this.head.setValue(-1.0f * yrotation, EnumGeomData.zrot);
                this.head.setValue(0.0f, EnumGeomData.yrot);
            }
        } else {
            if (this.ydirection == 1) {
                this.head.setValue(yrotation, EnumGeomData.yrot);
            } else {
                this.head.setValue(-1.0f * yrotation, EnumGeomData.yrot);
            }
            this.head.setValue(0.0f, EnumGeomData.zrot);
        }
        for (Module m : this.modules) {
            m.walk(entity, f, f1, f2, rotateAnglePitch, rotateAngleYaw);
        }
    }

    @Override
    public void fly(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
        this.walk(entity, f, f1, f2, f3, f4);
    }
}

