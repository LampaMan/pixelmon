/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.animations;

import com.pixelmongenerations.client.models.animations.EnumRotation;
import com.pixelmongenerations.client.models.animations.IModulized;
import com.pixelmongenerations.client.models.animations.ModulizedRenderWrapper;
import com.pixelmongenerations.common.entity.pixelmon.Entity2HasModel;
import java.util.ArrayList;
import net.minecraft.client.model.ModelRenderer;

public abstract class Module {
    public static final float toDegrees = 57.29578f;
    public static final float toRadians = (float)Math.PI / 180;
    protected ArrayList<Module> modules = new ArrayList();
    protected float xrD;
    protected float yrD;
    protected float zrD;

    public abstract void walk(Entity2HasModel var1, float var2, float var3, float var4, float var5, float var6);

    public abstract void fly(Entity2HasModel var1, float var2, float var3, float var4, float var5, float var6);

    public float getDelta(EnumRotation rot) {
        switch (rot) {
            case x: {
                return this.xrD;
            }
            case y: {
                return this.yrD;
            }
            case z: {
                return this.zrD;
            }
        }
        return 0.0f;
    }

    public static IModulized getModulizableFrom(Object obj) {
        if (obj instanceof ModelRenderer) {
            return new ModulizedRenderWrapper((ModelRenderer)obj);
        }
        if (obj instanceof IModulized) {
            return (IModulized)obj;
        }
        throw new IllegalArgumentException("The first variable passed-in to the Module contstructor must either be a ModelRenderer, or an instance of IModulizable. The class of the passed-in arm variable was " + obj.getClass().getSimpleName());
    }

    public Module addModule(Module m) {
        this.modules.add(m);
        return this;
    }
}

