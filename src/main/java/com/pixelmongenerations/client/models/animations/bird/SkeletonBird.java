/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.animations.bird;

import com.pixelmongenerations.client.models.animations.ModuleHead;
import com.pixelmongenerations.client.models.animations.ModuleTailBasic;
import com.pixelmongenerations.client.models.animations.SkeletonBase;
import com.pixelmongenerations.client.models.animations.bird.ModuleWing;
import net.minecraft.client.model.ModelRenderer;

public class SkeletonBird
extends SkeletonBase {
    public SkeletonBird(ModelRenderer body, ModuleHead head, ModuleWing leftWing, ModuleWing rightWing, ModelRenderer leftLeg, ModelRenderer rightLeg) {
        super(body);
        if (head != null) {
            this.modules.add(head);
        }
        if (leftWing != null) {
            this.modules.add(leftWing);
        }
        if (rightWing != null) {
            this.modules.add(rightWing);
        }
    }

    public SkeletonBird(ModelRenderer body, ModuleHead head, ModuleWing leftWing, ModuleWing rightWing, ModelRenderer leftLeg, ModelRenderer rightLeg, ModuleTailBasic tail) {
        super(body);
        if (head != null) {
            this.modules.add(head);
        }
        if (leftWing != null) {
            this.modules.add(leftWing);
        }
        if (rightWing != null) {
            this.modules.add(rightWing);
        }
        if (tail != null) {
            this.modules.add(tail);
        }
    }
}

