/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.animations.fish;

import com.pixelmongenerations.client.models.animations.EnumArm;
import com.pixelmongenerations.client.models.animations.EnumGeomData;
import com.pixelmongenerations.client.models.animations.IModulized;
import com.pixelmongenerations.client.models.animations.Module;
import com.pixelmongenerations.client.models.animations.ModulizedRenderWrapper;
import com.pixelmongenerations.common.entity.pixelmon.Entity2HasModel;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;

public class ModuleFin
extends Module {
    IModulized Fin;
    float FinRotationLimit;
    float FinInitY;
    float FinInitZ;
    float FinSpeed;
    float FinDirection;
    float FinOrientation;
    EnumArm FinVariable;

    public ModuleFin(IModulized Fin, EnumArm FinVariable, float FinOrientation, float FinRotationLimit, float FinSpeed) {
        this.Fin = Fin;
        this.FinRotationLimit = FinRotationLimit;
        this.FinOrientation = FinOrientation;
        this.FinSpeed = FinSpeed;
        this.FinInitY = Fin.getValue(EnumGeomData.yrot);
        this.FinInitZ = Fin.getValue(EnumGeomData.zrot);
        this.FinDirection = FinVariable == EnumArm.Right ? 1.0f : -1.0f;
    }

    public ModuleFin(ModelRenderer Fin, EnumArm FinVariable, float FinOrientation, float FinRotationLimit, float FinSpeed) {
        this(new ModulizedRenderWrapper(Fin), FinVariable, FinOrientation, FinRotationLimit, FinSpeed);
    }

    @Override
    public void walk(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
        this.yrD = MathHelper.cos((float)Math.toRadians(this.FinOrientation)) * this.FinDirection * MathHelper.cos(f * this.FinSpeed) * this.FinRotationLimit * f1;
        this.zrD = MathHelper.sin((float)Math.toRadians(this.FinOrientation)) * this.FinDirection * MathHelper.cos(f * this.FinSpeed) * this.FinRotationLimit * f1;
        this.Fin.setValue(this.yrD + this.FinInitY, EnumGeomData.yrot);
        this.Fin.setValue(this.zrD + this.FinInitZ, EnumGeomData.zrot);
    }

    @Override
    public void fly(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
    }
}

