/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.animations.bird;

import com.pixelmongenerations.client.models.animations.EnumGeomData;
import com.pixelmongenerations.client.models.animations.IModulized;
import com.pixelmongenerations.client.models.animations.bird.EnumWing;
import com.pixelmongenerations.client.models.animations.bird.ModuleWing;
import com.pixelmongenerations.common.entity.pixelmon.Entity2HasModel;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.helpers.animation.AnimationVariables;
import com.pixelmongenerations.common.entity.pixelmon.helpers.animation.IncrementingVariable;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;

public class ModuleWingComplex
extends ModuleWing {
    AnimationVariables animationVariables;

    public ModuleWingComplex(ModelRenderer wing, EnumWing WingVariable, float WingOrientation, float WingRotationLimit) {
        super(wing, WingVariable, WingOrientation, WingRotationLimit, 0.0f);
        this.registerAnimationCounters();
    }

    public ModuleWingComplex(IModulized wing, EnumWing WingVariable, float WingOrientation, float WingRotationLimit) {
        super(wing, WingVariable, WingOrientation, WingRotationLimit, 0.0f);
        this.registerAnimationCounters();
    }

    @Override
    public void walk(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
        IncrementingVariable counter = this.getCounter(0, entity);
        if (counter == null) {
            this.setCounter(0, 90, 6, entity);
            counter = this.getCounter(0, entity);
        }
        if (this.WingVariable == EnumWing.Left) {
            if (counter.value < 45.0f) {
                this.yrD = MathHelper.cos((float)Math.toRadians(this.WingOrientation)) * ((float)((double)1.0091f - (double)1.009f * Math.exp(-Math.toRadians(counter.value) * 6.0)) * (float)Math.PI * 2.0f * this.WingRotationLimit - (float)Math.PI * this.WingRotationLimit);
                this.zrD = MathHelper.sin((float)Math.toRadians(this.WingOrientation)) * ((float)((double)1.0091f - (double)1.009f * Math.exp(-Math.toRadians(counter.value) * 6.0)) * (float)Math.PI * 2.0f * this.WingRotationLimit - (float)Math.PI * this.WingRotationLimit);
            } else {
                this.yrD = MathHelper.cos((float)Math.toRadians(this.WingOrientation)) * (float)(0.5 - 0.5 * Math.cos(Math.toRadians(counter.value) * 4.0) * 3.1415927410125732 * 2.0 * (double)this.WingRotationLimit - (double)((float)Math.PI * this.WingRotationLimit));
                this.zrD = MathHelper.sin((float)Math.toRadians(this.WingOrientation)) * (float)(0.5 - 0.5 * Math.cos(Math.toRadians(counter.value) * 4.0) * 3.1415927410125732 * 2.0 * (double)this.WingRotationLimit - (double)((float)Math.PI * this.WingRotationLimit));
            }
        } else if (counter.value < 45.0f) {
            this.yrD = MathHelper.cos((float)Math.toRadians(this.WingOrientation)) * ((float)Math.exp(-Math.toRadians(counter.value) * 6.0) * (float)Math.PI * 2.0f * this.WingRotationLimit - (float)Math.PI * this.WingRotationLimit);
            this.zrD = MathHelper.sin((float)Math.toRadians(this.WingOrientation)) * ((float)Math.exp(-Math.toRadians(counter.value) * 6.0) * (float)Math.PI * 2.0f * this.WingRotationLimit - (float)Math.PI * this.WingRotationLimit);
        } else {
            this.yrD = MathHelper.cos((float)Math.toRadians(this.WingOrientation)) * (float)((0.5 * Math.cos(Math.toRadians(counter.value) * 4.0) - 0.5) * 3.1415927410125732 * 2.0 * (double)this.WingRotationLimit + (double)((float)Math.PI * this.WingRotationLimit));
            this.zrD = MathHelper.sin((float)Math.toRadians(this.WingOrientation)) * ((float)(0.5 * Math.cos(Math.toRadians(counter.value) * 4.0) - 0.5) * (float)Math.PI * 2.0f * this.WingRotationLimit + (float)Math.PI * this.WingRotationLimit);
        }
        this.wing.setValue(this.yrD, EnumGeomData.yrot);
        this.wing.setValue(this.zrD, EnumGeomData.zrot);
    }

    @Override
    public void fly(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
    }

    protected void registerAnimationCounters() {
    }

    protected void setInt(int id, int value, EntityPixelmon pixelmon) {
        pixelmon.getAnimationVariables().setInt(id, value);
    }

    protected int getInt(int id, EntityPixelmon pixelmon) {
        return pixelmon.getAnimationVariables().getInt(id);
    }

    protected void setCounter(int id, int limit, int increment, Entity2HasModel pixelmon) {
        pixelmon.getAnimationVariables().setCounter(id, limit, increment);
    }

    protected IncrementingVariable getCounter(int id, Entity2HasModel pixelmon) {
        return pixelmon.getAnimationVariables().getCounter(id);
    }
}

