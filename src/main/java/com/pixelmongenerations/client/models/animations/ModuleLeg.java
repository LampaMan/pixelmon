/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.animations;

import com.pixelmongenerations.client.models.animations.EnumGeomData;
import com.pixelmongenerations.client.models.animations.EnumLeg;
import com.pixelmongenerations.client.models.animations.EnumPhase;
import com.pixelmongenerations.client.models.animations.EnumRotation;
import com.pixelmongenerations.client.models.animations.IModulized;
import com.pixelmongenerations.client.models.animations.Module;
import com.pixelmongenerations.client.models.animations.ModulizedRenderWrapper;
import com.pixelmongenerations.common.entity.pixelmon.Entity2HasModel;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;

public class ModuleLeg
extends Module {
    IModulized leg;
    float WalkOffset;
    float LegRotationLimit;
    float LegInitX;
    float LegInitY;
    float LegInitZ;
    float legSpeed;
    EnumRotation rotationAxis;
    EnumPhase phaseVariable;
    EnumLeg legVariable;

    public ModuleLeg(IModulized leg, EnumLeg legVariable, EnumPhase phaseVariable, EnumRotation rotationAxis, float LegRotationLimit, float legSpeed) {
        this.leg = leg;
        this.LegRotationLimit = LegRotationLimit;
        this.legSpeed = legSpeed;
        this.phaseVariable = phaseVariable;
        this.legVariable = legVariable;
        this.rotationAxis = rotationAxis;
        this.LegInitX = this.leg.getValue(EnumGeomData.xrot);
        this.LegInitY = this.leg.getValue(EnumGeomData.yrot);
        this.LegInitZ = this.leg.getValue(EnumGeomData.zrot);
        this.WalkOffset = phaseVariable == EnumPhase.InPhase ? (legVariable == EnumLeg.FrontLeft ? (float)Math.PI : (legVariable == EnumLeg.FrontRight ? 0.0f : (legVariable == EnumLeg.BackLeft ? (float)Math.PI : 0.0f))) : (legVariable == EnumLeg.FrontLeft ? (float)Math.PI : (legVariable == EnumLeg.FrontRight ? 0.0f : (legVariable == EnumLeg.BackLeft ? 0.0f : (float)Math.PI)));
    }

    public ModuleLeg(ModelRenderer leg, EnumLeg legVariable, EnumPhase phaseVariable, EnumRotation rotationAxis, float LegRotationLimit, float legSpeed) {
        this(new ModulizedRenderWrapper(leg), legVariable, phaseVariable, rotationAxis, LegRotationLimit, legSpeed);
    }

    @Override
    public void walk(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
        switch (this.rotationAxis) {
            case x: {
                this.xrD = MathHelper.cos(f * this.legSpeed + this.WalkOffset) * this.LegRotationLimit * f1;
                this.leg.setValue(this.xrD + this.LegInitX, EnumGeomData.xrot);
                break;
            }
            case y: {
                this.yrD = MathHelper.cos(f * this.legSpeed + this.WalkOffset) * this.LegRotationLimit * f1;
                this.leg.setValue(this.yrD + this.LegInitY, EnumGeomData.yrot);
                break;
            }
            case z: {
                this.zrD = MathHelper.cos(f * this.legSpeed + this.WalkOffset) * this.LegRotationLimit * f1;
                this.leg.setValue(this.zrD + this.LegInitZ, EnumGeomData.zrot);
            }
        }
        for (Module m : this.modules) {
            m.walk(entity, f, f1, f2, f3, f4);
        }
    }

    @Override
    public void fly(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
    }
}

