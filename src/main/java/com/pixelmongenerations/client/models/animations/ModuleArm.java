/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.animations;

import com.pixelmongenerations.client.models.animations.EnumArm;
import com.pixelmongenerations.client.models.animations.EnumGeomData;
import com.pixelmongenerations.client.models.animations.EnumRotation;
import com.pixelmongenerations.client.models.animations.IModulized;
import com.pixelmongenerations.client.models.animations.Module;
import com.pixelmongenerations.client.models.animations.ModulizedRenderWrapper;
import com.pixelmongenerations.common.entity.pixelmon.Entity2HasModel;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;

public class ModuleArm
extends Module {
    IModulized arm;
    float ArmRotationLimit;
    float ArmSpeed;
    float ArmInitX;
    float ArmInitY;
    float ArmInitZ;
    float ArmDirection;
    EnumArm ArmVariable;
    EnumRotation rotationAxis;

    public ModuleArm(IModulized arm, EnumArm ArmVariable, EnumRotation rotationAxis, float ArmRotationLimit, float ArmSpeed) {
        this.arm = arm;
        this.ArmSpeed = ArmSpeed;
        this.ArmRotationLimit = ArmRotationLimit;
        this.ArmInitY = this.arm.getValue(EnumGeomData.zrot);
        this.ArmInitZ = this.arm.getValue(EnumGeomData.zrot);
        this.ArmInitX = this.arm.getValue(EnumGeomData.xrot);
        this.ArmVariable = ArmVariable;
        this.rotationAxis = rotationAxis;
        this.ArmDirection = ArmVariable == EnumArm.Right ? 1.0f : -1.0f;
    }

    public ModuleArm(ModelRenderer arm, EnumArm ArmVariable, EnumRotation rotationAxis, float ArmRotationLimit, float ArmSpeed) {
        this(new ModulizedRenderWrapper(arm), ArmVariable, rotationAxis, ArmRotationLimit, ArmSpeed);
    }

    @Override
    public void walk(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
        switch (this.rotationAxis) {
            case x: {
                this.xrD = MathHelper.cos(f * this.ArmSpeed + (float)Math.PI) * this.ArmRotationLimit * f1 * this.ArmDirection;
                this.arm.setValue(this.xrD + this.ArmInitX, EnumGeomData.xrot);
                break;
            }
            case y: {
                this.yrD = MathHelper.cos(f * this.ArmSpeed + (float)Math.PI) * this.ArmRotationLimit * f1 * this.ArmDirection;
                this.arm.setValue(this.yrD + this.ArmInitY, EnumGeomData.yrot);
                break;
            }
            case z: {
                this.zrD = MathHelper.cos(f * this.ArmSpeed + (float)Math.PI) * this.ArmRotationLimit * f1 * this.ArmDirection;
                this.arm.setValue(this.zrD + this.ArmInitZ, EnumGeomData.zrot);
            }
        }
        for (Module m : this.modules) {
            m.walk(entity, f, f1, f2, f3, f4);
        }
    }

    @Override
    public void fly(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
    }
}

