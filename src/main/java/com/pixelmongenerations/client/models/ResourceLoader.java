/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Sets
 */
package com.pixelmongenerations.client.models;

import com.google.common.collect.Sets;
import com.pixelmongenerations.client.models.ModelHolder;
import com.pixelmongenerations.client.models.PixelmonModelBase;
import java.time.Instant;
import java.util.HashSet;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import net.minecraft.client.Minecraft;

public class ResourceLoader {
    public static final PixelmonModelBase DUMMY = new PixelmonModelBase(){};
    private static final int cleanupTime;
    private static final ScheduledExecutorService executor;

    public static <T> Future<T> addTask(Callable<T> callable) {
        return executor.submit(callable);
    }

    private static void cleanupModels() {
        long now = Instant.now().getEpochSecond();
        HashSet<ModelHolder> holders = Sets.newHashSet(ModelHolder.loadedHolders);
        holders.removeIf(holder -> now - holder.lastAccess < (long)cleanupTime);
        Minecraft.getMinecraft().addScheduledTask(() -> holders.forEach(ModelHolder::clear));
    }

    static {
        executor = Executors.newSingleThreadScheduledExecutor(r -> new Thread(r, "Pixelmon resources thread"));
        cleanupTime = Runtime.getRuntime().maxMemory() / 1024L / 1024L <= 1024L ? 120 : 240;
        executor.scheduleAtFixedRate(ResourceLoader::cleanupModels, cleanupTime, cleanupTime, TimeUnit.SECONDS);
    }
}

