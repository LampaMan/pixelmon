/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.discs;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;

public class ModelDiscStage2
extends ModelBase {
    ModelRenderer RedTip;
    ModelRenderer RedTop;
    ModelRenderer RedFront;
    ModelRenderer RedBottom;
    ModelRenderer RedRight;
    ModelRenderer RedLeft;
    ModelRenderer RedBack;

    public ModelDiscStage2() {
        this.textureWidth = 32;
        this.textureHeight = 32;
        this.RedTip = new ModelRenderer(this, 18, 0);
        this.RedTip.addBox(-1.0f, -1.2f, -1.0f, 2, 1, 2);
        this.RedTip.setRotationPoint(6.5f, 13.0f, 0.0f);
        this.RedTip.setTextureSize(32, 32);
        this.RedTip.mirror = true;
        this.setRotation(this.RedTip, 0.0f, 0.0f, 0.0f);
        this.RedTop = new ModelRenderer(this, 0, 0);
        this.RedTop.addBox(-1.5f, -0.8f, -1.5f, 3, 1, 3);
        this.RedTop.setRotationPoint(6.5f, 13.0f, 0.0f);
        this.RedTop.setTextureSize(32, 32);
        this.RedTop.mirror = true;
        this.setRotation(this.RedTop, 0.0f, 0.0f, 0.0f);
        this.RedFront = new ModelRenderer(this, 12, 0);
        this.RedFront.addBox(-1.0f, -0.6f, -1.9f, 2, 1, 1);
        this.RedFront.setRotationPoint(6.5f, 13.0f, 0.0f);
        this.RedFront.setTextureSize(32, 32);
        this.RedFront.mirror = true;
        this.setRotation(this.RedFront, 0.0f, 0.0f, 0.0f);
        this.RedBottom = new ModelRenderer(this, 0, 4);
        this.RedBottom.addBox(-1.5f, -0.8f, -1.5f, 3, 1, 3);
        this.RedBottom.setRotationPoint(6.5f, 13.0f, 0.0f);
        this.RedBottom.setTextureSize(32, 32);
        this.RedBottom.mirror = true;
        this.setRotation(this.RedBottom, 0.0f, 0.0f, 0.0f);
        this.RedRight = new ModelRenderer(this, 0, 8);
        this.RedRight.addBox(-1.9f, -0.6f, -1.0f, 1, 1, 2);
        this.RedRight.setRotationPoint(6.5f, 13.0f, 0.0f);
        this.RedRight.setTextureSize(32, 32);
        this.RedRight.mirror = true;
        this.setRotation(this.RedRight, 0.0f, 0.0f, 0.0f);
        this.RedLeft = new ModelRenderer(this, 6, 8);
        this.RedLeft.addBox(0.9f, -0.6f, -1.0f, 1, 1, 2);
        this.RedLeft.setRotationPoint(6.5f, 13.0f, 0.0f);
        this.RedLeft.setTextureSize(32, 32);
        this.RedLeft.mirror = true;
        this.setRotation(this.RedLeft, 0.0f, 0.0f, 0.0f);
        this.RedBack = new ModelRenderer(this, 12, 2);
        this.RedBack.addBox(-1.0f, -0.6f, 0.9f, 2, 1, 1);
        this.RedBack.setRotationPoint(6.5f, 13.0f, 0.0f);
        this.RedBack.setTextureSize(32, 32);
        this.RedBack.mirror = true;
        this.setRotation(this.RedBack, 0.0f, 0.0f, 0.0f);
    }

    public void renderModel(float f5) {
        this.RedTip.render(f5);
        this.RedTop.render(f5);
        this.RedFront.render(f5);
        this.RedBottom.render(f5);
        this.RedRight.render(f5);
        this.RedLeft.render(f5);
        this.RedBack.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }
}

