/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models;

import com.pixelmongenerations.client.models.ModelCustomWrapper;
import com.pixelmongenerations.client.models.PixelmonModelRenderer;
import com.pixelmongenerations.client.models.PixelmonModelSmd;
import com.pixelmongenerations.client.models.PixelmonSmdFactory;
import com.pixelmongenerations.client.models.animations.SkeletonBase;
import com.pixelmongenerations.client.models.smd.SmdAnimation;
import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import com.pixelmongenerations.common.entity.pixelmon.Entity2HasModel;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.core.enums.EnumPokemonModel;
import net.minecraft.entity.Entity;

public class DualModelFactory
extends PixelmonSmdFactory {
    protected EnumPokemonModel model2;
    protected float transparency;

    public DualModelFactory(EnumPokemonModel model, EnumPokemonModel model2) {
        super(model);
        this.model2 = model2;
    }

    public DualModelFactory setModel2Transparency(float transparency) {
        this.transparency = transparency;
        return this;
    }

    @Override
    public PixelmonModelSmd createModel() {
        TransparentImpl impl = new TransparentImpl((ValveStudioModel)this.model.loadModel(), (ValveStudioModel)this.model2.loadModel());
        impl.body.setRotationPoint(this.xRotation, this.yRotation, this.zRotation);
        impl.body.rotateAngleX = this.rotateAngleX;
        impl.body.rotateAngleY = this.rotateAngleY;
        impl.body2.setRotationPoint(this.xRotation, this.yRotation, this.zRotation);
        impl.body2.rotateAngleX = this.rotateAngleX;
        impl.body2.rotateAngleY = this.rotateAngleY;
        impl.body2.setTransparent(this.transparency);
        impl.movementThreshold = this.movementThreshold;
        impl.animationIncrement = this.animationIncrement;
        return impl;
    }

    public static class TransparentImpl
    extends PixelmonModelSmd {
        PixelmonModelRenderer body;
        PixelmonModelRenderer body2;
        ValveStudioModel model2;

        TransparentImpl(ValveStudioModel model, ValveStudioModel model2) {
            this.theModel = model;
            this.model2 = model2;
            this.body = new PixelmonModelRenderer(this, "body");
            this.body.addCustomModel(new ModelCustomWrapper(model));
            this.body2 = new PixelmonModelRenderer(this, "body");
            this.body2.addCustomModel(new ModelCustomWrapper(model2));
            this.skeleton = new SkeletonBase(this.body);
        }

        @Override
        public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
            super.render(entity, f, f1, f2, f3, f4, f5);
            this.body.render(f5);
            this.body2.render(f5);
        }

        @Override
        protected void tickAnimation(Entity3HasStats pixelmon) {
            SmdAnimation animation = this.theModel.currentAnimation;
            int frame = (int)Math.floor(this.getCounter((int)-1, (Entity2HasModel)pixelmon).value % (float)animation.totalFrames);
            animation.setCurrentFrame(frame);
            animation = this.model2.currentAnimation;
            frame = (int)Math.floor(this.getCounter((int)-1, (Entity2HasModel)pixelmon).value % (float)animation.totalFrames);
            animation.setCurrentFrame(frame);
            pixelmon.world.profiler.startSection("pixelmon_animate");
            this.theModel.animate();
            this.model2.animate();
            pixelmon.world.profiler.endSection();
        }

        @Override
        protected void setAnimation(String string) {
            this.theModel.setAnimation(string);
            this.model2.setAnimation(string);
        }
    }
}

