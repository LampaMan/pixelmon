/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableMap
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.client.models.items;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.block.model.ItemOverrideList;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.model.IModel;
import net.minecraftforge.client.model.ItemLayerModel;
import net.minecraftforge.common.model.TRSRTransformation;

public class ItemPixelmonSpriteModel
implements IBakedModel {
    public static final ModelResourceLocation modelResourceLocation = new ModelResourceLocation("pixelmon:pixelmon_sprite", "inventory");
    IBakedModel iBakedModel;

    public ItemPixelmonSpriteModel(IBakedModel iBakedModel) {
        this.iBakedModel = iBakedModel;
    }

    @Override
    public List<BakedQuad> getQuads(IBlockState state, EnumFacing side, long rand) {
        return this.iBakedModel.getQuads(state, side, rand);
    }

    @Override
    public boolean isAmbientOcclusion() {
        return this.iBakedModel.isAmbientOcclusion();
    }

    @Override
    public boolean isGui3d() {
        return false;
    }

    @Override
    public boolean isBuiltInRenderer() {
        return false;
    }

    @Override
    public TextureAtlasSprite getParticleTexture() {
        return this.iBakedModel.getParticleTexture();
    }

    @Override
    public ItemCameraTransforms getItemCameraTransforms() {
        return this.iBakedModel.getItemCameraTransforms();
    }

    @Override
    public ItemOverrideList getOverrides() {
        return new OverrideList();
    }

    class OverrideList
    extends ItemOverrideList {
        public OverrideList() {
            super(Lists.newArrayList());
        }

        @Override
        public IBakedModel handleItemState(IBakedModel originalModel, ItemStack stack, World world, EntityLivingBase entity) {
            if (stack.hasTagCompound() && stack.getTagCompound().hasKey("SpriteName")) {
                Function<ResourceLocation, TextureAtlasSprite> textureGetter = location -> Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(location.toString());
                ImmutableMap texMap = ImmutableMap.of((Object)"layer0", (Object)stack.getTagCompound().getString("SpriteName"));
                IModel iModel = ItemLayerModel.INSTANCE.retexture(texMap);
                Optional<TRSRTransformation> trsrTransformation = TRSRTransformation.identity().apply(Optional.empty());
                ItemPixelmonSpriteModel.this.iBakedModel = iModel.bake(trsrTransformation.get(), DefaultVertexFormats.ITEM, textureGetter);
            }
            return ItemPixelmonSpriteModel.this.iBakedModel;
        }
    }
}

