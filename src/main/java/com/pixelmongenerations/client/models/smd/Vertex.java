/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.smd;

public class Vertex {
    public float x;
    public float y;
    public float z;

    public Vertex(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}

