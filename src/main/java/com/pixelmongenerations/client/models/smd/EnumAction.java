/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.smd;

public enum EnumAction {
    IDLE,
    WALK,
    RUN,
    FLY,
    SWIM,
    ATTACK1,
    ATTACK2,
    JOY,
    EAT,
    CUSTOM0,
    CUSTOM1,
    CUSTOM2,
    CUSTOM3,
    CUSTOM4,
    CUSTOM5,
    CUSTOM6,
    CUSTOM7,
    CUSTOM8,
    CUSTOM9;

}

