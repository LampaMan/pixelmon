/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.models.smd;

import com.pixelmongenerations.client.materials.Cubemap;
import com.pixelmongenerations.client.materials.EnumMaterialOption;
import com.pixelmongenerations.core.util.RegexPatterns;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.texture.SimpleTexture;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class Material {
    private boolean translucent = false;
    private boolean wireframe = false;
    private boolean unlit = false;
    private boolean noCull = false;
    private boolean depthMask = true;
    private SimpleTexture texture;
    private Cubemap cubemap;
    private File directory;
    private String resourceLocationDirectory;

    public Material(File materialFile) throws IOException {
        this.directory = materialFile.getParentFile();
        String path = this.directory.getAbsolutePath();
        this.resourceLocationDirectory = path.substring(path.indexOf("pixelmon\\") + 9);
        BufferedReader reader = new BufferedReader(new FileReader(materialFile));
        String line = null;
        while ((line = reader.readLine()) != null) {
            ResourceLocation resloc;
            String resLocString;
            String[] params = RegexPatterns.MULTIPLE_WHITESPACE.split(line);
            if (params[0].equalsIgnoreCase("$texture")) {
                resLocString = this.resourceLocationDirectory.endsWith("/") ? this.resourceLocationDirectory + params[1] : this.resourceLocationDirectory + "/" + params[1];
                resloc = new ResourceLocation("pixelmon", resLocString);
                System.out.println("resloc = " + resloc);
                Minecraft.getMinecraft().getTextureManager().bindTexture(resloc);
                this.texture = (SimpleTexture)Minecraft.getMinecraft().getTextureManager().getTexture(resloc);
                continue;
            }
            if (params[0].equalsIgnoreCase("$cubemap")) {
                resLocString = path.endsWith("/") ? path + params[1] : path + "/" + params[1];
                resloc = new ResourceLocation(resLocString);
                this.cubemap = new Cubemap(resloc);
                continue;
            }
            if (params[0].equalsIgnoreCase("$nocull")) {
                this.noCull = Boolean.parseBoolean(params[1]);
                continue;
            }
            if (params[0].equalsIgnoreCase("$translucent")) {
                this.translucent = Boolean.parseBoolean(params[1]);
                continue;
            }
            if (params[0].equalsIgnoreCase("$wireframe")) {
                this.wireframe = Boolean.parseBoolean(params[1]);
                continue;
            }
            if (params[0].equalsIgnoreCase("$unlit")) {
                this.unlit = Boolean.parseBoolean(params[1]);
                continue;
            }
            if (!params[0].equalsIgnoreCase("$depthmask")) continue;
            this.depthMask = Boolean.parseBoolean(params[1]);
        }
        reader.close();
    }

    public void pre() {
        GL11.glBindTexture((int)3553, (int)this.texture.getGlTextureId());
        if (this.translucent) {
            EnumMaterialOption.TRANSPARENCY.begin(new Object[0]);
        }
        if (this.wireframe) {
            EnumMaterialOption.WIREFRAME.begin(new Object[0]);
        }
        if (this.unlit) {
            EnumMaterialOption.NO_LIGHTING.begin(new Object[0]);
        }
        if (this.noCull) {
            EnumMaterialOption.NOCULL.begin(new Object[0]);
        } else {
            EnumMaterialOption.NOCULL.end(new Object[0]);
        }
        if (!this.depthMask) {
            GlStateManager.depthMask(false);
        }
        if (this.cubemap != null) {
            this.cubemap.start();
        }
    }

    public void post() {
        if (this.translucent) {
            EnumMaterialOption.TRANSPARENCY.end(new Object[0]);
        }
        if (this.wireframe) {
            EnumMaterialOption.WIREFRAME.end(new Object[0]);
        }
        if (this.unlit) {
            EnumMaterialOption.NO_LIGHTING.end(new Object[0]);
        }
        if (!this.noCull) {
            EnumMaterialOption.NOCULL.end(new Object[0]);
        }
        if (!this.depthMask) {
            GlStateManager.depthMask(true);
        }
        if (this.cubemap != null) {
            this.cubemap.end();
        }
    }
}

