/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.smd;

import com.pixelmongenerations.client.models.smd.SmdAnimation;
import com.pixelmongenerations.client.models.smd.SmdModel;
import java.util.ArrayList;

public class BodyGroup {
    public ArrayList<SmdModel> models = new ArrayList();
    protected int currentModel;

    public void setAnimation(SmdAnimation anim) {
        for (SmdModel model : this.models) {
            model.setAnimation(anim);
        }
    }

    public void render(boolean hasChanged) {
        if (this.currentModel >= 0) {
            this.models.get(this.currentModel).render(hasChanged);
        }
    }

    public void setActiveModel(int i) {
        this.currentModel = i >= this.models.size() ? -1 : i;
    }

    public SmdModel getActiveModel() {
        return this.currentModel < 0 || this.currentModel >= this.models.size() ? null : this.models.get(this.currentModel);
    }
}

