/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.smd;

public enum AnimationType {
    IDLE,
    WALK,
    FLY,
    SWIM,
    IDLE_SWIM;


    public String toString() {
        return this.name().toLowerCase().replace("_", "");
    }

    public static AnimationType getTypeFor(String animation) {
        switch (animation) {
            case "idle": {
                return IDLE;
            }
            case "walk": {
                return WALK;
            }
            case "fly": {
                return FLY;
            }
            case "swim": {
                return SWIM;
            }
            case "idleswim": {
                return IDLE_SWIM;
            }
        }
        return IDLE;
    }
}

