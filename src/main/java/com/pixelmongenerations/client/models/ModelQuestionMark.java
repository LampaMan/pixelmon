/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelQuestionMark
extends ModelBase {
    ModelRenderer MiddleDot;
    ModelRenderer MiddleBase;
    ModelRenderer MiddleOuter;
    ModelRenderer MiddleTop;
    ModelRenderer MiddleHook;

    public ModelQuestionMark() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.MiddleDot = new ModelRenderer(this, 0, 0);
        this.MiddleDot.addBox(-2.0f, -2.0f, -2.0f, 4, 4, 4);
        this.MiddleDot.setRotationPoint(0.0f, 20.0f, 0.0f);
        this.MiddleDot.setTextureSize(64, 32);
        this.MiddleDot.mirror = true;
        this.setRotation(this.MiddleDot, 0.0f, 0.0f, 0.0f);
        this.MiddleBase = new ModelRenderer(this, 0, 0);
        this.MiddleBase.addBox(-2.0f, -3.0f, -2.0f, 4, 6, 4);
        this.MiddleBase.setRotationPoint(0.0f, 14.0f, 0.0f);
        this.MiddleBase.setTextureSize(64, 32);
        this.MiddleBase.mirror = true;
        this.setRotation(this.MiddleBase, 0.0f, 0.0f, 0.0f);
        this.MiddleOuter = new ModelRenderer(this, 0, 0);
        this.MiddleOuter.addBox(-2.0f, -4.0f, -2.0f, 4, 8, 4);
        this.MiddleOuter.setRotationPoint(4.0f, 7.0f, 0.0f);
        this.MiddleOuter.setTextureSize(64, 32);
        this.MiddleOuter.mirror = true;
        this.setRotation(this.MiddleOuter, 0.0f, 0.0f, 0.0f);
        this.MiddleTop = new ModelRenderer(this, 0, 0);
        this.MiddleTop.addBox(-2.0f, -2.0f, -2.0f, 4, 4, 4);
        this.MiddleTop.setRotationPoint(0.0f, 1.0f, 0.0f);
        this.MiddleTop.setTextureSize(64, 32);
        this.MiddleTop.mirror = true;
        this.setRotation(this.MiddleTop, 0.0f, 0.0f, 0.0f);
        this.MiddleHook = new ModelRenderer(this, 0, 0);
        this.MiddleHook.addBox(-2.0f, -2.0f, -2.0f, 4, 4, 4);
        this.MiddleHook.setRotationPoint(-4.0f, 5.0f, 0.0f);
        this.MiddleHook.setTextureSize(64, 32);
        this.MiddleHook.mirror = true;
        this.setRotation(this.MiddleHook, 0.0f, 0.0f, 0.0f);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.MiddleDot.render(f5);
        this.MiddleBase.render(f5);
        this.MiddleOuter.render(f5);
        this.MiddleTop.render(f5);
        this.MiddleHook.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }
}

