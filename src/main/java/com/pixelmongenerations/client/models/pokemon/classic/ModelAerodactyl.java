/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.pokemon.classic;

import com.pixelmongenerations.client.models.PixelmonModelBase;
import com.pixelmongenerations.client.models.PixelmonModelRenderer;
import com.pixelmongenerations.client.models.animations.ModuleHead;
import com.pixelmongenerations.client.models.animations.bird.EnumWing;
import com.pixelmongenerations.client.models.animations.bird.ModuleWingComplex;
import com.pixelmongenerations.client.models.animations.bird.SkeletonBird;
import net.minecraft.entity.Entity;

public class ModelAerodactyl
extends PixelmonModelBase {
    PixelmonModelRenderer BODY;

    public ModelAerodactyl() {
        this.textureWidth = 128;
        this.textureHeight = 64;
        this.BODY = new PixelmonModelRenderer(this, "BODY");
        this.BODY.setRotationPoint(0.0f, 4.0f, 2.0f);
        PixelmonModelRenderer body_1 = new PixelmonModelRenderer(this, 0, 0);
        body_1.addBox(-4.0f, -1.4f, -3.0f, 8, 10, 10);
        body_1.setTextureSize(128, 64);
        body_1.mirror = true;
        this.setRotation(body_1, -0.3665191f, 0.0f, 0.0f);
        PixelmonModelRenderer body_2 = new PixelmonModelRenderer(this, 0, 0);
        body_2.addBox(-4.0f, 0.2f, 2.0f, 8, 8, 6);
        body_2.setTextureSize(128, 64);
        body_2.mirror = true;
        this.setRotation(body_2, -0.1570796f, 0.0f, 0.0f);
        PixelmonModelRenderer chest = new PixelmonModelRenderer(this, 0, 0);
        chest.addBox(-3.5f, -0.5333334f, -3.6f, 7, 9, 4);
        chest.setTextureSize(128, 64);
        chest.mirror = true;
        this.setRotation(chest, -0.5934119f, 0.0f, 0.0f);
        PixelmonModelRenderer back_hump_thing_ = new PixelmonModelRenderer(this, 0, 0);
        back_hump_thing_.addBox(-1.0f, -2.4f, -3.733333f, 2, 4, 6);
        back_hump_thing_.setTextureSize(128, 64);
        back_hump_thing_.mirror = true;
        this.setRotation(back_hump_thing_, -0.8726646f, 0.0f, 0.0f);
        PixelmonModelRenderer neck_1 = new PixelmonModelRenderer(this, 0, 0);
        neck_1.addBox(-2.5f, -3.333333f, -2.6f, 5, 7, 4);
        neck_1.setRotationPoint(0.0f, 1.0f, -5.0f);
        neck_1.setTextureSize(128, 64);
        neck_1.mirror = true;
        this.setRotation(neck_1, -0.4363323f, 0.0f, 0.0f);
        PixelmonModelRenderer neck_2 = new PixelmonModelRenderer(this, 0, 0);
        neck_2.addBox(-2.0f, -2.8f, -3.866667f, 4, 5, 4);
        neck_2.setRotationPoint(0.0f, 0.0f, -7.0f);
        neck_2.setTextureSize(128, 64);
        neck_2.mirror = true;
        this.setRotation(neck_2, -0.3665191f, 0.0f, 0.0f);
        PixelmonModelRenderer neck_3 = new PixelmonModelRenderer(this, 0, 0);
        neck_3.addBox(-2.0f, -1.8f, -5.866667f, 4, 4, 6);
        neck_3.setRotationPoint(0.0f, -2.0f, -10.0f);
        neck_3.setTextureSize(128, 64);
        neck_3.mirror = true;
        this.setRotation(neck_3, -0.2617994f, 0.0f, 0.0f);
        this.BODY.addChild(body_1);
        this.BODY.addChild(body_2);
        this.BODY.addChild(chest);
        this.BODY.addChild(back_hump_thing_);
        this.BODY.addChild(neck_1);
        this.BODY.addChild(neck_2);
        this.BODY.addChild(neck_3);
        PixelmonModelRenderer TAIL = new PixelmonModelRenderer(this, "");
        TAIL.setRotationPoint(0.0f, 5.0f, 7.0f);
        PixelmonModelRenderer tail_1 = new PixelmonModelRenderer(this, 0, 0);
        tail_1.addBox(-3.0f, -3.0f, -0.3333333f, 6, 7, 5);
        tail_1.setTextureSize(128, 64);
        tail_1.mirror = true;
        this.setRotation(tail_1, -0.3141593f, 0.0f, 0.0f);
        PixelmonModelRenderer tail_2 = new PixelmonModelRenderer(this, 0, 0);
        tail_2.addBox(-2.5f, -3.0f, -1.4f, 5, 6, 4);
        tail_2.setRotationPoint(0.0f, 2.0f, 5.0f);
        tail_2.setTextureSize(128, 64);
        tail_2.mirror = true;
        this.setRotation(tail_2, -0.3839724f, 0.0f, 0.0f);
        PixelmonModelRenderer tail_3 = new PixelmonModelRenderer(this, 0, 0);
        tail_3.addBox(-2.0f, -2.266667f, -1.4f, 4, 5, 5);
        tail_3.setRotationPoint(0.0f, 3.0f, 8.0f);
        tail_3.setTextureSize(128, 64);
        tail_3.mirror = true;
        this.setRotation(tail_3, -0.4886922f, 0.0f, 0.0f);
        PixelmonModelRenderer tail_4 = new PixelmonModelRenderer(this, 0, 0);
        tail_4.addBox(-1.5f, -1.866667f, -1.4f, 3, 4, 6);
        tail_4.setRotationPoint(0.0f, 5.0f, 12.0f);
        tail_4.setTextureSize(128, 64);
        tail_4.mirror = true;
        this.setRotation(tail_4, -0.4712389f, 0.0f, 0.0f);
        PixelmonModelRenderer tail_5 = new PixelmonModelRenderer(this, 0, 0);
        tail_5.addBox(-1.0f, -1.133333f, -1.4f, 2, 3, 7);
        tail_5.setRotationPoint(0.0f, 7.0f, 17.0f);
        tail_5.setTextureSize(128, 64);
        tail_5.mirror = true;
        this.setRotation(tail_5, -0.3141593f, 0.0f, 0.0f);
        PixelmonModelRenderer tail_6 = new PixelmonModelRenderer(this, 0, 0);
        tail_6.addBox(-0.5f, -1.2f, -1.4f, 1, 2, 7);
        tail_6.setRotationPoint(0.0f, 9.0f, 23.0f);
        tail_6.setTextureSize(128, 64);
        tail_6.mirror = true;
        this.setRotation(tail_6, -0.1570796f, 0.0f, 0.0f);
        PixelmonModelRenderer tail_tip = new PixelmonModelRenderer(this, -7, 55);
        tail_tip.addBox(-4.5f, -0.2666667f, 2.333333f, 9, 1, 8);
        tail_tip.setRotationPoint(0.0f, 9.0f, 23.0f);
        tail_tip.setTextureSize(128, 64);
        tail_tip.mirror = true;
        this.setRotation(tail_tip, -0.1570796f, 0.0f, 0.0f);
        TAIL.addChild(tail_1);
        TAIL.addChild(tail_2);
        TAIL.addChild(tail_3);
        TAIL.addChild(tail_4);
        TAIL.addChild(tail_5);
        TAIL.addChild(tail_6);
        TAIL.addChild(tail_tip);
        this.BODY.addChild(TAIL);
        PixelmonModelRenderer HEAD = new PixelmonModelRenderer(this, "HEAD");
        HEAD.setRotationPoint(0.0f, -4.0f, -16.0f);
        PixelmonModelRenderer head_main = new PixelmonModelRenderer(this, 0, 0);
        head_main.addBox(-2.5f, -3.866667f, -1.933333f, 5, 3, 5);
        head_main.setTextureSize(128, 64);
        head_main.mirror = true;
        this.setRotation(head_main, 0.2268928f, 0.0f, 0.0f);
        PixelmonModelRenderer head_L = new PixelmonModelRenderer(this, 0, 0);
        head_L.addBox(0.9f, -2.0f, -2.4f, 3, 4, 5);
        head_L.setTextureSize(128, 64);
        head_L.mirror = true;
        this.setRotation(head_L, 0.1919862f, 0.6632251f, 0.0523599f);
        PixelmonModelRenderer head_R = new PixelmonModelRenderer(this, 0, 0);
        head_R.addBox(-3.9f, -2.0f, -2.4f, 3, 4, 5);
        head_R.setTextureSize(128, 64);
        head_R.mirror = true;
        this.setRotation(head_R, 0.1919862f, -0.6632251f, -0.0872665f);
        PixelmonModelRenderer head_front = new PixelmonModelRenderer(this, 0, 0);
        head_front.addBox(-1.5f, -2.4f, -4.866667f, 3, 3, 4);
        head_front.setTextureSize(128, 64);
        head_front.mirror = true;
        this.setRotation(head_front, 0.1919862f, 0.0f, 0.0f);
        PixelmonModelRenderer head_L_back = new PixelmonModelRenderer(this, 0, 0);
        head_L_back.addBox(1.0f, -2.0f, -2.066667f, 3, 5, 4);
        head_L_back.setTextureSize(128, 64);
        head_L_back.mirror = true;
        this.setRotation(head_L_back, 0.1919862f, -0.4014257f, -0.122173f);
        PixelmonModelRenderer head_R_back = new PixelmonModelRenderer(this, 0, 0);
        head_R_back.addBox(-4.0f, -2.0f, -2.066667f, 3, 5, 4);
        head_R_back.setTextureSize(128, 64);
        head_R_back.mirror = true;
        this.setRotation(head_R_back, 0.1919862f, 0.4014257f, 0.122173f);
        PixelmonModelRenderer upper_jaw_tip_2 = new PixelmonModelRenderer(this, 0, 0);
        upper_jaw_tip_2.addBox(-0.5f, 4.366667f, -10.0f, 1, 2, 1);
        upper_jaw_tip_2.setTextureSize(128, 64);
        upper_jaw_tip_2.mirror = true;
        this.setRotation(upper_jaw_tip_2, -0.0523599f, 0.0f, 0.0f);
        PixelmonModelRenderer upper_jaw_tip = new PixelmonModelRenderer(this, 0, 0);
        upper_jaw_tip.addBox(-0.5f, -0.5f, -11.86667f, 1, 2, 1);
        upper_jaw_tip.setTextureSize(128, 64);
        upper_jaw_tip.mirror = true;
        this.setRotation(upper_jaw_tip, 0.4363323f, 0.0f, 0.0f);
        PixelmonModelRenderer nose_bottom = new PixelmonModelRenderer(this, 0, 0);
        nose_bottom.addBox(-1.0f, -1.566667f, -11.93333f, 2, 2, 3);
        nose_bottom.setTextureSize(128, 64);
        nose_bottom.mirror = true;
        this.setRotation(nose_bottom, 0.4886922f, 0.0f, 0.0f);
        PixelmonModelRenderer nose_top = new PixelmonModelRenderer(this, 0, 0);
        nose_top.addBox(-1.0f, 1.966667f, -10.8f, 2, 1, 3);
        nose_top.setTextureSize(128, 64);
        nose_top.mirror = true;
        this.setRotation(nose_top, 0.0174533f, 0.0f, 0.0f);
        PixelmonModelRenderer nose_ = new PixelmonModelRenderer(this, 13, 49);
        nose_.addBox(-1.5f, 4.366667f, -10.2f, 3, 2, 3);
        nose_.setTextureSize(128, 64);
        nose_.mirror = true;
        this.setRotation(nose_, -0.2094395f, 0.0f, 0.0f);
        PixelmonModelRenderer head_top_2 = new PixelmonModelRenderer(this, 0, 0);
        head_top_2.addBox(-1.5f, -4.4f, -3.266667f, 3, 3, 3);
        head_top_2.setTextureSize(128, 64);
        head_top_2.mirror = true;
        this.setRotation(head_top_2, 0.6457718f, 0.0f, 0.0f);
        PixelmonModelRenderer eye_ridge_R = new PixelmonModelRenderer(this, 51, 50);
        eye_ridge_R.addBox(-4.0f, -3.3f, -3.333333f, 2, 4, 5);
        eye_ridge_R.setTextureSize(128, 64);
        eye_ridge_R.mirror = true;
        this.setRotation(eye_ridge_R, 0.4363323f, -0.4712389f, -0.0523599f);
        PixelmonModelRenderer eye_ridge_L = new PixelmonModelRenderer(this, 51, 50);
        eye_ridge_L.addBox(2.0f, -3.266667f, -3.266667f, 2, 4, 5);
        eye_ridge_L.setTextureSize(128, 64);
        eye_ridge_L.mirror = true;
        this.setRotation(eye_ridge_L, 0.4363323f, 0.4712389f, 0.0523599f);
        PixelmonModelRenderer horn_2_L = new PixelmonModelRenderer(this, 0, 0);
        horn_2_L.addBox(0.9666666f, -4.866667f, -0.2666667f, 1, 3, 2);
        horn_2_L.setTextureSize(128, 64);
        horn_2_L.mirror = true;
        this.setRotation(horn_2_L, -0.1047198f, 0.4014257f, 0.296706f);
        PixelmonModelRenderer horn_1_L = new PixelmonModelRenderer(this, 0, 0);
        horn_1_L.addBox(2.566667f, -4.4f, -0.2666667f, 2, 3, 2);
        horn_1_L.setTextureSize(128, 64);
        horn_1_L.mirror = true;
        this.setRotation(horn_1_L, -0.1047198f, 0.4014257f, -0.0523599f);
        PixelmonModelRenderer horn_3_L = new PixelmonModelRenderer(this, 0, 0);
        horn_3_L.addBox(1.766667f, -7.066667f, 0.2f, 1, 3, 1);
        horn_3_L.setTextureSize(128, 64);
        horn_3_L.mirror = true;
        this.setRotation(horn_3_L, -0.1047198f, 0.4014257f, 0.1919862f);
        PixelmonModelRenderer horn_4_L = new PixelmonModelRenderer(this, 0, 0);
        horn_4_L.addBox(3.3f, -6.6f, 0.2f, 1, 3, 1);
        horn_4_L.setTextureSize(128, 64);
        horn_4_L.mirror = true;
        this.setRotation(horn_4_L, -0.1047198f, 0.4014257f, -0.0174533f);
        PixelmonModelRenderer face_L_1 = new PixelmonModelRenderer(this, 0, 0);
        face_L_1.addBox(1.766667f, -2.4f, -0.3333333f, 3, 3, 2);
        face_L_1.setTextureSize(128, 64);
        face_L_1.mirror = true;
        this.setRotation(face_L_1, -0.0349066f, 0.4014257f, -0.1570796f);
        PixelmonModelRenderer upper_jaw_R_3_2 = new PixelmonModelRenderer(this, 0, 0);
        upper_jaw_R_3_2.addBox(-4.8f, 0.0f, -1.933333f, 2, 4, 1);
        upper_jaw_R_3_2.setTextureSize(128, 64);
        upper_jaw_R_3_2.mirror = true;
        this.setRotation(upper_jaw_R_3_2, 1.256637f, -0.4014257f, -0.1047198f);
        PixelmonModelRenderer upper_jaw_R_3 = new PixelmonModelRenderer(this, 0, 0);
        upper_jaw_R_3.addBox(-4.8f, 0.0f, -0.5333334f, 2, 4, 2);
        upper_jaw_R_3.setTextureSize(128, 64);
        upper_jaw_R_3.mirror = true;
        this.setRotation(upper_jaw_R_3, 0.8901179f, -0.4014257f, -0.1047198f);
        PixelmonModelRenderer upper_jaw_R_2 = new PixelmonModelRenderer(this, 0, 0);
        upper_jaw_R_2.addBox(-4.8f, -0.7333333f, -3.8f, 2, 3, 5);
        upper_jaw_R_2.setTextureSize(128, 64);
        upper_jaw_R_2.mirror = true;
        this.setRotation(upper_jaw_R_2, 0.296706f, -0.4014257f, -0.1047198f);
        PixelmonModelRenderer upper_jaw_L_3_2 = new PixelmonModelRenderer(this, 0, 0);
        upper_jaw_L_3_2.addBox(2.766667f, 0.0f, -1.933333f, 2, 4, 1);
        upper_jaw_L_3_2.setTextureSize(128, 64);
        upper_jaw_L_3_2.mirror = true;
        this.setRotation(upper_jaw_L_3_2, 1.256637f, 0.4014257f, 0.1047198f);
        PixelmonModelRenderer upper_jaw_L_3 = new PixelmonModelRenderer(this, 0, 0);
        upper_jaw_L_3.addBox(2.766667f, 0.0f, -0.5333334f, 2, 4, 2);
        upper_jaw_L_3.setTextureSize(128, 64);
        upper_jaw_L_3.mirror = true;
        this.setRotation(upper_jaw_L_3, 0.8901179f, 0.4014257f, 0.1047198f);
        PixelmonModelRenderer upper_jaw_L_2 = new PixelmonModelRenderer(this, 0, 0);
        upper_jaw_L_2.addBox(2.766667f, -0.7333333f, -3.8f, 2, 3, 5);
        upper_jaw_L_2.setTextureSize(128, 64);
        upper_jaw_L_2.mirror = true;
        this.setRotation(upper_jaw_L_2, 0.296706f, 0.4014257f, 0.1047198f);
        PixelmonModelRenderer head_back = new PixelmonModelRenderer(this, 0, 0);
        head_back.addBox(-2.5f, -2.133333f, 1.8f, 5, 3, 2);
        head_back.setTextureSize(128, 64);
        head_back.mirror = true;
        this.setRotation(head_back, 0.2268928f, 0.0f, 0.0f);
        PixelmonModelRenderer upper_jaw_L_1 = new PixelmonModelRenderer(this, 0, 0);
        upper_jaw_L_1.addBox(2.1f, -0.7333333f, -10.26667f, 2, 3, 7);
        upper_jaw_L_1.setTextureSize(128, 64);
        upper_jaw_L_1.mirror = true;
        this.setRotation(upper_jaw_L_1, 0.296706f, 0.2443461f, 0.0698132f);
        PixelmonModelRenderer upper_jaw_R_1 = new PixelmonModelRenderer(this, 0, 0);
        upper_jaw_R_1.addBox(-4.1f, -0.7f, -10.26667f, 2, 3, 7);
        upper_jaw_R_1.setTextureSize(128, 64);
        upper_jaw_R_1.mirror = true;
        this.setRotation(upper_jaw_R_1, 0.296706f, -0.2443461f, -0.0698132f);
        PixelmonModelRenderer upper_jaw_main = new PixelmonModelRenderer(this, 0, 0);
        upper_jaw_main.addBox(-1.5f, -0.7f, -10.2f, 3, 3, 7);
        upper_jaw_main.setTextureSize(128, 64);
        upper_jaw_main.mirror = true;
        this.setRotation(upper_jaw_main, 0.296706f, 0.0f, 0.0f);
        PixelmonModelRenderer face_R_1 = new PixelmonModelRenderer(this, 0, 0);
        face_R_1.addBox(-4.8f, -2.4f, -0.3333333f, 3, 3, 2);
        face_R_1.setTextureSize(128, 64);
        face_R_1.mirror = true;
        this.setRotation(face_R_1, -0.0349066f, -0.4014257f, 0.1570796f);
        PixelmonModelRenderer horn_1_R = new PixelmonModelRenderer(this, 0, 0);
        horn_1_R.addBox(-4.6f, -4.4f, -0.2666667f, 2, 3, 2);
        horn_1_R.setTextureSize(128, 64);
        horn_1_R.mirror = true;
        this.setRotation(horn_1_R, -0.1047198f, -0.4014257f, 0.0523599f);
        PixelmonModelRenderer horn_2_R = new PixelmonModelRenderer(this, 0, 0);
        horn_2_R.addBox(-2.033333f, -4.866667f, -0.2666667f, 1, 3, 2);
        horn_2_R.setTextureSize(128, 64);
        horn_2_R.mirror = true;
        this.setRotation(horn_2_R, -0.1047198f, -0.4014257f, -0.296706f);
        PixelmonModelRenderer horn_4_R = new PixelmonModelRenderer(this, 0, 0);
        horn_4_R.addBox(-4.3f, -6.6f, 0.2f, 1, 3, 1);
        horn_4_R.setTextureSize(128, 64);
        horn_4_R.mirror = true;
        this.setRotation(horn_4_R, -0.1047198f, -0.4014257f, 0.0174533f);
        PixelmonModelRenderer horn_3_R = new PixelmonModelRenderer(this, 0, 0);
        horn_3_R.addBox(-2.8f, -7.066667f, 0.2f, 1, 3, 1);
        horn_3_R.setTextureSize(128, 64);
        horn_3_R.mirror = true;
        this.setRotation(horn_3_R, -0.1047198f, -0.4014257f, -0.1919862f);
        PixelmonModelRenderer upper_jaw_inside = new PixelmonModelRenderer(this, 0, 35);
        upper_jaw_inside.addBox(-3.5f, 1.4f, -10.86667f, 7, 1, 10);
        upper_jaw_inside.setTextureSize(128, 64);
        upper_jaw_inside.mirror = true;
        this.setRotation(upper_jaw_inside, 0.296706f, 0.0f, 0.0f);
        PixelmonModelRenderer bottom_jaw_main = new PixelmonModelRenderer(this, 0, 0);
        bottom_jaw_main.addBox(-1.5f, 1.566667f, -12.66667f, 3, 2, 12);
        bottom_jaw_main.setTextureSize(128, 64);
        bottom_jaw_main.mirror = true;
        this.setRotation(bottom_jaw_main, 0.7330383f, 0.0f, 0.0f);
        PixelmonModelRenderer bottom_jaw_L = new PixelmonModelRenderer(this, 0, 0);
        bottom_jaw_L.addBox(1.833333f, 1.566667f, -11.73333f, 3, 2, 12);
        bottom_jaw_L.setTextureSize(128, 64);
        bottom_jaw_L.mirror = true;
        this.setRotation(bottom_jaw_L, 0.7330383f, 0.3141593f, 0.0872665f);
        PixelmonModelRenderer bottom_jaw_R = new PixelmonModelRenderer(this, 0, 0);
        bottom_jaw_R.addBox(-4.8f, 1.566667f, -11.73333f, 3, 2, 12);
        bottom_jaw_R.setTextureSize(128, 64);
        bottom_jaw_R.mirror = true;
        this.setRotation(bottom_jaw_R, 0.7330383f, -0.3141593f, -0.0872665f);
        PixelmonModelRenderer bottom_jaw_end_1 = new PixelmonModelRenderer(this, 0, 0);
        bottom_jaw_end_1.addBox(-1.5f, -5.833333f, -11.8f, 3, 2, 2);
        bottom_jaw_end_1.setTextureSize(128, 64);
        bottom_jaw_end_1.mirror = true;
        this.setRotation(bottom_jaw_end_1, 1.48353f, 0.0f, 0.0f);
        PixelmonModelRenderer bottom_jaw_end_2 = new PixelmonModelRenderer(this, 0, 0);
        bottom_jaw_end_2.addBox(-1.5f, -1.766667f, -12.4f, 3, 2, 4);
        bottom_jaw_end_2.setTextureSize(128, 64);
        bottom_jaw_end_2.mirror = true;
        this.setRotation(bottom_jaw_end_2, 1.151917f, 0.0f, 0.0f);
        PixelmonModelRenderer bottom_jaw_inside = new PixelmonModelRenderer(this, 40, 17);
        bottom_jaw_inside.addBox(-3.5f, 1.133333f, -12.46667f, 7, 1, 11);
        bottom_jaw_inside.setTextureSize(128, 64);
        bottom_jaw_inside.mirror = true;
        this.setRotation(bottom_jaw_inside, 0.7504916f, 0.0f, 0.0f);
        PixelmonModelRenderer teeth_L = new PixelmonModelRenderer(this, 0, 25);
        teeth_L.addBox(3.433333f, 1.3f, -11.73333f, 1, 2, 7);
        teeth_L.setTextureSize(128, 64);
        teeth_L.mirror = true;
        this.setRotation(teeth_L, 0.6283185f, 0.3141593f, 0.0872665f);
        PixelmonModelRenderer teeth_R = new PixelmonModelRenderer(this, 0, 25);
        teeth_R.addBox(-5.4f, 1.3f, -11.73333f, 1, 2, 7);
        teeth_R.setTextureSize(128, 64);
        teeth_R.mirror = true;
        this.setRotation(teeth_R, 0.6283185f, -0.3141593f, -0.0872665f);
        HEAD.addChild(head_main);
        HEAD.addChild(head_L);
        HEAD.addChild(head_R);
        HEAD.addChild(head_front);
        HEAD.addChild(head_L_back);
        HEAD.addChild(head_R_back);
        HEAD.addChild(upper_jaw_tip_2);
        HEAD.addChild(upper_jaw_tip);
        HEAD.addChild(nose_bottom);
        HEAD.addChild(nose_top);
        HEAD.addChild(nose_);
        HEAD.addChild(head_top_2);
        HEAD.addChild(eye_ridge_R);
        HEAD.addChild(eye_ridge_L);
        HEAD.addChild(horn_2_L);
        HEAD.addChild(horn_1_L);
        HEAD.addChild(horn_3_L);
        HEAD.addChild(horn_4_L);
        HEAD.addChild(face_L_1);
        HEAD.addChild(upper_jaw_R_3_2);
        HEAD.addChild(upper_jaw_R_3);
        HEAD.addChild(upper_jaw_R_2);
        HEAD.addChild(upper_jaw_L_3_2);
        HEAD.addChild(upper_jaw_L_3);
        HEAD.addChild(upper_jaw_L_2);
        HEAD.addChild(head_back);
        HEAD.addChild(upper_jaw_L_1);
        HEAD.addChild(upper_jaw_R_1);
        HEAD.addChild(upper_jaw_main);
        HEAD.addChild(face_R_1);
        HEAD.addChild(horn_1_R);
        HEAD.addChild(horn_2_R);
        HEAD.addChild(horn_4_R);
        HEAD.addChild(horn_3_R);
        HEAD.addChild(upper_jaw_inside);
        HEAD.addChild(bottom_jaw_main);
        HEAD.addChild(bottom_jaw_L);
        HEAD.addChild(bottom_jaw_R);
        HEAD.addChild(bottom_jaw_end_1);
        HEAD.addChild(bottom_jaw_end_2);
        HEAD.addChild(bottom_jaw_inside);
        HEAD.addChild(teeth_L);
        HEAD.addChild(teeth_R);
        this.BODY.addChild(HEAD);
        PixelmonModelRenderer LEGR = new PixelmonModelRenderer(this, "");
        LEGR.setRotationPoint(-4.0f, 5.0f, 5.0f);
        PixelmonModelRenderer leg_1_R = new PixelmonModelRenderer(this, 0, 0);
        leg_1_R.addBox(-1.0f, -0.9333333f, -4.333333f, 3, 9, 5);
        leg_1_R.setTextureSize(128, 64);
        leg_1_R.mirror = true;
        this.setRotation(leg_1_R, -0.2268928f, 0.5410521f, 0.2094395f);
        PixelmonModelRenderer leg_2_R = new PixelmonModelRenderer(this, 0, 0);
        leg_2_R.addBox(-0.5f, 6.933333f, -2.066667f, 2, 2, 8);
        leg_2_R.setTextureSize(128, 64);
        leg_2_R.mirror = true;
        this.setRotation(leg_2_R, -0.4537856f, 0.5410521f, 0.2094395f);
        PixelmonModelRenderer leg_3_R = new PixelmonModelRenderer(this, 0, 0);
        leg_3_R.addBox(-0.5f, 4.5f, 0.8f, 2, 2, 7);
        leg_3_R.setTextureSize(128, 64);
        leg_3_R.mirror = true;
        this.setRotation(leg_3_R, -0.7504916f, 0.5410521f, 0.2094395f);
        PixelmonModelRenderer foot_R = new PixelmonModelRenderer(this, 0, 0);
        foot_R.addBox(-0.5f, 7.466667f, 3.6f, 2, 3, 1);
        foot_R.setTextureSize(128, 64);
        foot_R.mirror = true;
        this.setRotation(foot_R, -0.2443461f, 0.5410521f, 0.2094395f);
        PixelmonModelRenderer toe_back_R = new PixelmonModelRenderer(this, 0, 0);
        toe_back_R.addBox(0.0f, 3.8f, 4.533333f, 1, 3, 1);
        toe_back_R.setTextureSize(128, 64);
        toe_back_R.mirror = true;
        this.setRotation(toe_back_R, -0.3839724f, 0.5410521f, 0.2094395f);
        PixelmonModelRenderer toe_R_2 = new PixelmonModelRenderer(this, 0, 0);
        toe_R_2.addBox(2.5f, 8.266666f, 1.933333f, 1, 4, 1);
        toe_R_2.setTextureSize(128, 64);
        toe_R_2.mirror = true;
        this.setRotation(toe_R_2, -0.0872665f, 0.5235988f, 0.4886922f);
        PixelmonModelRenderer toe_R_1 = new PixelmonModelRenderer(this, 0, 0);
        toe_R_1.addBox(-1.5f, 8.933333f, 1.933333f, 1, 4, 1);
        toe_R_1.setTextureSize(128, 64);
        toe_R_1.mirror = true;
        this.setRotation(toe_R_1, -0.0872665f, 0.5410521f, 0.0f);
        LEGR.addChild(leg_1_R);
        LEGR.addChild(leg_2_R);
        LEGR.addChild(leg_3_R);
        LEGR.addChild(foot_R);
        LEGR.addChild(toe_back_R);
        LEGR.addChild(toe_R_2);
        LEGR.addChild(toe_R_1);
        this.BODY.addChild(LEGR);
        PixelmonModelRenderer LEGL = new PixelmonModelRenderer(this, "");
        LEGL.setRotationPoint(4.0f, 5.0f, 5.0f);
        PixelmonModelRenderer leg_1_L = new PixelmonModelRenderer(this, 0, 0);
        leg_1_L.addBox(-2.0f, -0.9333333f, -4.333333f, 3, 9, 5);
        leg_1_L.setTextureSize(128, 64);
        leg_1_L.mirror = true;
        this.setRotation(leg_1_L, -0.2268928f, -0.5410521f, -0.2094395f);
        PixelmonModelRenderer leg_2_L = new PixelmonModelRenderer(this, 0, 0);
        leg_2_L.addBox(-1.5f, 6.933333f, -2.066667f, 2, 2, 8);
        leg_2_L.setTextureSize(128, 64);
        leg_2_L.mirror = true;
        this.setRotation(leg_2_L, -0.4537856f, -0.5410521f, -0.2094395f);
        PixelmonModelRenderer leg_3_L = new PixelmonModelRenderer(this, 0, 0);
        leg_3_L.addBox(-1.5f, 4.8f, 0.8f, 2, 2, 7);
        leg_3_L.setTextureSize(128, 64);
        leg_3_L.mirror = true;
        this.setRotation(leg_3_L, -0.7504916f, -0.5410521f, -0.2094395f);
        PixelmonModelRenderer foot_L = new PixelmonModelRenderer(this, 0, 0);
        foot_L.addBox(-1.5f, 7.466667f, 3.6f, 2, 3, 1);
        foot_L.setTextureSize(128, 64);
        foot_L.mirror = true;
        this.setRotation(foot_L, -0.2443461f, -0.5410521f, -0.2094395f);
        PixelmonModelRenderer toe_back_L = new PixelmonModelRenderer(this, 0, 0);
        toe_back_L.addBox(-1.0f, 3.8f, 4.533333f, 1, 3, 1);
        toe_back_L.setTextureSize(128, 64);
        toe_back_L.mirror = true;
        this.setRotation(toe_back_L, -0.3839724f, -0.5410521f, -0.2094395f);
        PixelmonModelRenderer toe_L_1 = new PixelmonModelRenderer(this, 0, 0);
        toe_L_1.addBox(0.5f, 8.933333f, 1.933333f, 1, 4, 1);
        toe_L_1.setTextureSize(128, 64);
        toe_L_1.mirror = true;
        this.setRotation(toe_L_1, -0.0872665f, -0.5410521f, 0.0f);
        PixelmonModelRenderer toe_L_2 = new PixelmonModelRenderer(this, 0, 0);
        toe_L_2.addBox(-3.5f, 8.266666f, 1.933333f, 1, 4, 1);
        toe_L_2.setTextureSize(128, 64);
        toe_L_2.mirror = true;
        this.setRotation(toe_L_2, -0.0872665f, -0.5235988f, -0.4886922f);
        LEGL.addChild(leg_1_L);
        LEGL.addChild(leg_2_L);
        LEGL.addChild(leg_3_L);
        LEGL.addChild(foot_L);
        LEGL.addChild(toe_back_L);
        LEGL.addChild(toe_L_1);
        LEGL.addChild(toe_L_2);
        this.BODY.addChild(LEGL);
        PixelmonModelRenderer WINGL = new PixelmonModelRenderer(this, "");
        WINGL.setRotationPoint(3.0f, -1.0f, -3.0f);
        PixelmonModelRenderer wing_membrane_2_L = new PixelmonModelRenderer(this, 40, 2);
        wing_membrane_2_L.addBox(0.0f, -0.5f, -3.0f, 31, 1, 26);
        wing_membrane_2_L.setRotationPoint(12.0f, 0.0f, 1.0f);
        wing_membrane_2_L.setTextureSize(128, 64);
        wing_membrane_2_L.mirror = true;
        this.setRotation(wing_membrane_2_L, -0.2268928f, -0.0872665f, -0.0349066f);
        PixelmonModelRenderer wing_membrane_1_L = new PixelmonModelRenderer(this, 10, 47);
        wing_membrane_1_L.addBox(0.0f, 0.5f, 0.0f, 12, 1, 16);
        wing_membrane_1_L.setTextureSize(128, 64);
        wing_membrane_1_L.mirror = true;
        this.setRotation(wing_membrane_1_L, -0.2268928f, -0.0872665f, -0.0872665f);
        PixelmonModelRenderer wing_3_L = new PixelmonModelRenderer(this, 0, 0);
        wing_3_L.addBox(0.0f, -1.0f, -1.0f, 22, 1, 1);
        wing_3_L.setRotationPoint(25.0f, -1.0f, -1.0f);
        wing_3_L.setTextureSize(128, 64);
        wing_3_L.mirror = true;
        this.setRotation(wing_3_L, 0.0f, -0.715585f, 0.1047198f);
        PixelmonModelRenderer wing_2_L = new PixelmonModelRenderer(this, 0, 0);
        wing_2_L.addBox(0.0f, -1.0f, -1.0f, 14, 2, 2);
        wing_2_L.setRotationPoint(12.0f, 0.0f, 1.0f);
        wing_2_L.setTextureSize(128, 64);
        wing_2_L.mirror = true;
        this.setRotation(wing_2_L, 0.0f, 0.1570796f, -0.0872665f);
        PixelmonModelRenderer wing_1_L = new PixelmonModelRenderer(this, 0, 0);
        wing_1_L.addBox(0.0f, 0.0f, -1.0f, 12, 2, 2);
        wing_1_L.setTextureSize(128, 64);
        wing_1_L.mirror = true;
        this.setRotation(wing_1_L, 0.0f, -0.0872665f, -0.0872665f);
        PixelmonModelRenderer finger_1_L = new PixelmonModelRenderer(this, 0, 0);
        finger_1_L.addBox(-3.0f, -1.0f, -1.0f, 3, 1, 1);
        finger_1_L.setRotationPoint(25.0f, -1.0f, -1.0f);
        finger_1_L.setTextureSize(128, 64);
        finger_1_L.mirror = true;
        this.setRotation(finger_1_L, 0.0f, -0.9599311f, 0.0f);
        PixelmonModelRenderer finger_2_L = new PixelmonModelRenderer(this, 0, 0);
        finger_2_L.addBox(-3.0f, -1.0f, -0.2666667f, 3, 1, 1);
        finger_2_L.setRotationPoint(25.0f, -1.0f, -1.0f);
        finger_2_L.setTextureSize(128, 64);
        finger_2_L.mirror = true;
        this.setRotation(finger_2_L, 0.0f, -1.884956f, -0.0872665f);
        PixelmonModelRenderer finger_3_L = new PixelmonModelRenderer(this, 0, 0);
        finger_3_L.addBox(-3.0f, -1.0f, -0.2666667f, 3, 1, 1);
        finger_3_L.setRotationPoint(25.0f, -1.0f, -1.0f);
        finger_3_L.setTextureSize(128, 64);
        finger_3_L.mirror = true;
        this.setRotation(finger_3_L, 0.0f, -2.478368f, -0.2094395f);
        WINGL.addChild(wing_membrane_2_L);
        WINGL.addChild(wing_membrane_1_L);
        WINGL.addChild(wing_3_L);
        WINGL.addChild(wing_2_L);
        WINGL.addChild(wing_1_L);
        WINGL.addChild(finger_1_L);
        WINGL.addChild(finger_2_L);
        WINGL.addChild(finger_3_L);
        this.BODY.addChild(WINGL);
        PixelmonModelRenderer WINGR = new PixelmonModelRenderer(this, "");
        WINGR.setRotationPoint(-3.0f, -1.0f, -3.0f);
        PixelmonModelRenderer wing_membrane_2_R = new PixelmonModelRenderer(this, 40, 37);
        wing_membrane_2_R.addBox(-31.0f, -0.5f, -3.0f, 31, 1, 26);
        wing_membrane_2_R.setRotationPoint(-12.0f, 0.0f, 1.0f);
        wing_membrane_2_R.setTextureSize(128, 64);
        wing_membrane_2_R.mirror = true;
        this.setRotation(wing_membrane_2_R, -0.2268928f, 0.0872665f, 0.0349066f);
        PixelmonModelRenderer wing_membrane_1_R = new PixelmonModelRenderer(this, 10, 29);
        wing_membrane_1_R.addBox(-12.0f, 0.5f, 0.0f, 12, 1, 16);
        wing_membrane_1_R.setTextureSize(128, 64);
        wing_membrane_1_R.mirror = true;
        this.setRotation(wing_membrane_1_R, -0.2268928f, 0.0872665f, 0.0872665f);
        PixelmonModelRenderer wing_3_R = new PixelmonModelRenderer(this, 0, 0);
        wing_3_R.addBox(-21.0f, -1.0f, -1.0f, 22, 1, 1);
        wing_3_R.setRotationPoint(-25.0f, -1.0f, -1.0f);
        wing_3_R.setTextureSize(128, 64);
        wing_3_R.mirror = true;
        this.setRotation(wing_3_R, 0.0f, 0.715585f, -0.1047198f);
        PixelmonModelRenderer wing_2_R = new PixelmonModelRenderer(this, 0, 0);
        wing_2_R.addBox(-14.0f, -1.0f, -1.0f, 14, 2, 2);
        wing_2_R.setRotationPoint(-12.0f, 0.0f, 1.0f);
        wing_2_R.setTextureSize(128, 64);
        wing_2_R.mirror = true;
        this.setRotation(wing_2_R, 0.0f, -0.1570796f, 0.0872665f);
        PixelmonModelRenderer wing_1_R = new PixelmonModelRenderer(this, 0, 0);
        wing_1_R.addBox(-12.0f, 0.0f, -1.0f, 12, 2, 2);
        wing_1_R.setTextureSize(128, 64);
        wing_1_R.mirror = true;
        this.setRotation(wing_1_R, 0.0f, 0.0872665f, 0.0872665f);
        PixelmonModelRenderer finger_2_R = new PixelmonModelRenderer(this, 0, 0);
        finger_2_R.addBox(0.0f, -1.0f, -0.2666667f, 3, 1, 1);
        finger_2_R.setRotationPoint(-25.0f, -1.0f, -1.0f);
        finger_2_R.setTextureSize(128, 64);
        finger_2_R.mirror = true;
        this.setRotation(finger_2_R, 0.0f, 1.884956f, 0.0872665f);
        PixelmonModelRenderer finger_1_R = new PixelmonModelRenderer(this, 0, 0);
        finger_1_R.addBox(0.0f, -1.0f, -1.0f, 3, 1, 1);
        finger_1_R.setRotationPoint(-25.0f, -1.0f, -1.0f);
        finger_1_R.setTextureSize(128, 64);
        finger_1_R.mirror = true;
        this.setRotation(finger_1_R, 0.0f, 0.9599311f, 0.0f);
        PixelmonModelRenderer finger_3_R = new PixelmonModelRenderer(this, 0, 0);
        finger_3_R.addBox(0.0f, -1.0f, -0.2666667f, 3, 1, 1);
        finger_3_R.setRotationPoint(-25.0f, -1.0f, -1.0f);
        finger_3_R.setTextureSize(128, 64);
        finger_3_R.mirror = true;
        this.setRotation(finger_3_R, 0.0f, 2.478368f, 0.2094395f);
        WINGR.addChild(wing_membrane_2_R);
        WINGR.addChild(wing_membrane_1_R);
        WINGR.addChild(wing_3_R);
        WINGR.addChild(wing_2_R);
        WINGR.addChild(wing_1_R);
        WINGR.addChild(finger_2_R);
        WINGR.addChild(finger_1_R);
        WINGR.addChild(finger_3_R);
        this.BODY.addChild(WINGR);
        ModuleHead headModule = new ModuleHead(HEAD);
        ModuleWingComplex rightWingModule = new ModuleWingComplex(WINGR, EnumWing.Right, 90.0f, 0.25f);
        ModuleWingComplex leftWingModule = new ModuleWingComplex(WINGL, EnumWing.Left, 90.0f, 0.25f);
        this.skeleton = new SkeletonBird(this.BODY, headModule, leftWingModule, rightWingModule, LEGL, LEGR);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5);
        this.BODY.render(f5);
    }

    private void setRotation(PixelmonModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5) {
    }
}

