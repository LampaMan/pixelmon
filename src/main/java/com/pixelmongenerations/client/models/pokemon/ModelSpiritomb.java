/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.pokemon;

import com.pixelmongenerations.client.models.ModelCustomWrapper;
import com.pixelmongenerations.client.models.PixelmonModelRenderer;
import com.pixelmongenerations.client.models.PixelmonModelSmd;
import com.pixelmongenerations.client.models.animations.biped.SkeletonBiped;
import com.pixelmongenerations.client.models.smd.SmdAnimation;
import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import com.pixelmongenerations.common.entity.pixelmon.Entity2HasModel;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.core.enums.EnumPokemonModel;
import net.minecraft.entity.Entity;

public class ModelSpiritomb
extends PixelmonModelSmd {
    PixelmonModelRenderer body = new PixelmonModelRenderer(this, "body");
    PixelmonModelRenderer bodyTransparent;
    PixelmonModelRenderer moreTransparent;
    ValveStudioModel smallSpheres;
    ValveStudioModel spheres;
    ValveStudioModel spectre;

    public ModelSpiritomb() {
        ValveStudioModel bodyAndFace;
        this.body.setRotationPoint(0.0f, 24.0f, 0.0f);
        this.body.rotateAngleX = -1.5707964f;
        this.theModel = bodyAndFace = (ValveStudioModel)EnumPokemonModel.Spiritomb.loadModel();
        this.body.addCustomModel(new ModelCustomWrapper(bodyAndFace));
        this.bodyTransparent = new PixelmonModelRenderer(this, "body");
        this.smallSpheres = (ValveStudioModel)EnumPokemonModel.SpiritombSmallSpheres.loadModel();
        this.spheres = (ValveStudioModel)EnumPokemonModel.SpiritombSpheres.loadModel();
        this.spectre = (ValveStudioModel)EnumPokemonModel.SpiritombSpectre.loadModel();
        this.bodyTransparent.addCustomModel(new ModelCustomWrapper(this.spectre));
        this.bodyTransparent.setTransparent(0.5f);
        this.bodyTransparent.setRotationPoint(0.0f, 24.0f, 0.0f);
        this.bodyTransparent.rotateAngleX = -1.5707964f;
        this.moreTransparent = new PixelmonModelRenderer(this, "body");
        this.moreTransparent.addCustomModel(new ModelCustomWrapper(this.smallSpheres));
        this.moreTransparent.addCustomModel(new ModelCustomWrapper(this.spheres));
        this.moreTransparent.setTransparent(0.2f);
        this.moreTransparent.setRotationPoint(0.0f, 24.0f, 0.0f);
        this.moreTransparent.rotateAngleX = -1.5707964f;
        this.skeleton = new SkeletonBiped(this.body, null, null, null, null, null, null);
    }

    @Override
    public void render(Entity var1, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(var1, f, f1, f2, f3, f4, f5);
        this.body.render(f5);
        this.bodyTransparent.render(f5);
        this.moreTransparent.render(f5);
    }

    @Override
    protected void tickAnimation(Entity3HasStats pixelmon) {
        super.tickAnimation(pixelmon);
        this.handleAnimation(pixelmon, this.smallSpheres);
        this.handleAnimation(pixelmon, this.spheres);
        this.handleAnimation(pixelmon, this.spectre);
    }

    private void handleAnimation(Entity3HasStats pixelmon, ValveStudioModel model) {
        SmdAnimation theAnim = model.currentAnimation;
        int frame = (int)Math.floor(this.getCounter((int)-1, (Entity2HasModel)pixelmon).value % (float)theAnim.totalFrames);
        theAnim.setCurrentFrame(frame);
        model.animate();
    }

    @Override
    protected void setAnimation(String string) {
        this.theModel.setAnimation(string);
        this.smallSpheres.setAnimation(string);
        this.spheres.setAnimation(string);
        this.spectre.setAnimation(string);
    }
}

