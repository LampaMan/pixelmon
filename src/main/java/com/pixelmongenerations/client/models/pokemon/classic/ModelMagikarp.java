/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.pokemon.classic;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

public class ModelMagikarp
extends ModelBase {
    ModelRenderer Face;
    ModelRenderer rightwhisker;
    ModelRenderer leftwhisker;
    ModelRenderer Body;
    ModelRenderer BodyFront;
    ModelRenderer BodyBack;
    ModelRenderer rightfin;
    ModelRenderer leftfin;
    ModelRenderer topfin;
    ModelRenderer bottomfin;
    ModelRenderer Rear;
    ModelRenderer Rearfin;
    ModelRenderer BodyBase;
    ModelRenderer FacePiece;
    ModelRenderer BackPiece;

    public ModelMagikarp() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.BodyBase = new ModelRenderer(this, "BodyBase");
        this.BodyBase.setRotationPoint(0.0f, 17.5f, -0.5f);
        this.setRotation(this.BodyBase, 0.0f, 0.0f, 0.0f);
        this.BodyBase.mirror = true;
        this.Body = new ModelRenderer(this, 34, 18);
        this.Body.addBox(-1.5f, -3.0f, -2.0f, 3, 7, 3);
        this.Body.setRotationPoint(0.0f, -0.5f, 0.5f);
        this.Body.setTextureSize(64, 32);
        this.Body.mirror = true;
        this.setRotation(this.Body, 0.0f, 0.0f, 0.0f);
        this.topfin = new ModelRenderer(this, 32, -5);
        this.topfin.addBox(0.0f, -6.0f, -2.5f, 0, 3, 5);
        this.topfin.setRotationPoint(0.0f, -0.5f, 0.0f);
        this.topfin.setTextureSize(64, 32);
        this.topfin.mirror = true;
        this.setRotation(this.topfin, 0.0f, 0.0f, 0.0f);
        this.bottomfin = new ModelRenderer(this, 32, -2);
        this.bottomfin.addBox(0.0f, 4.0f, -2.5f, 0, 3, 5);
        this.bottomfin.setRotationPoint(0.0f, -0.5f, 0.0f);
        this.bottomfin.setTextureSize(64, 32);
        this.bottomfin.mirror = true;
        this.setRotation(this.bottomfin, 0.0f, 0.0f, 0.0f);
        this.BodyBase.addChild(this.Body);
        this.BodyBase.addChild(this.topfin);
        this.BodyBase.addChild(this.bottomfin);
        this.FacePiece = new ModelRenderer(this, "FacePiece");
        this.FacePiece.setRotationPoint(0.0f, 0.0f, -1.5f);
        this.setRotation(this.FacePiece, 0.0f, 0.0f, 0.0f);
        this.FacePiece.mirror = true;
        this.BodyFront = new ModelRenderer(this, 22, 18);
        this.BodyFront.addBox(-1.5f, -3.0f, -2.0f, 3, 7, 3);
        this.BodyFront.setRotationPoint(0.0f, -0.5f, -1.0f);
        this.BodyFront.setTextureSize(64, 32);
        this.BodyFront.mirror = true;
        this.setRotation(this.BodyFront, 0.0f, 0.0f, 0.0f);
        this.Face = new ModelRenderer(this, 0, 15);
        this.Face.addBox(-1.0f, -2.0f, -3.0f, 2, 5, 1);
        this.Face.setRotationPoint(0.0f, -0.5f, -1.0f);
        this.Face.setTextureSize(64, 32);
        this.Face.mirror = true;
        this.setRotation(this.Face, 0.0f, 0.0f, 0.0f);
        this.rightwhisker = new ModelRenderer(this, 0, 22);
        this.rightwhisker.addBox(-4.5f, 2.0f, -2.5f, 4, 5, 0);
        this.rightwhisker.setRotationPoint(0.0f, -0.5f, -1.0f);
        this.rightwhisker.setTextureSize(64, 32);
        this.rightwhisker.mirror = true;
        this.setRotation(this.rightwhisker, 0.0f, 0.2094395f, 0.0f);
        this.leftwhisker = new ModelRenderer(this, 9, 22);
        this.leftwhisker.addBox(0.5f, 2.0f, -2.5f, 4, 5, 0);
        this.leftwhisker.setRotationPoint(0.0f, -0.5f, -1.0f);
        this.leftwhisker.setTextureSize(64, 32);
        this.leftwhisker.mirror = true;
        this.setRotation(this.leftwhisker, 0.0f, -0.2094395f, 0.0f);
        this.leftfin = new ModelRenderer(this, 22, 3);
        this.leftfin.addBox(0.0f, -3.0f, 0.0f, 5, 3, 0);
        this.leftfin.setRotationPoint(-1.5f, 2.5f, -2.0f);
        this.leftfin.setTextureSize(64, 32);
        this.leftfin.mirror = true;
        this.setRotation(this.leftfin, -0.1919862f, -2.356194f, 0.2268928f);
        this.rightfin = new ModelRenderer(this, 22, 0);
        this.rightfin.addBox(0.0f, -3.0f, 0.0f, 5, 3, 0);
        this.rightfin.setRotationPoint(1.5f, 2.5f, -2.0f);
        this.rightfin.setTextureSize(64, 32);
        this.rightfin.mirror = true;
        this.setRotation(this.rightfin, 0.1919862f, -0.7853982f, 0.2268928f);
        this.FacePiece.addChild(this.BodyFront);
        this.FacePiece.addChild(this.Face);
        this.FacePiece.addChild(this.rightwhisker);
        this.FacePiece.addChild(this.leftwhisker);
        this.FacePiece.addChild(this.rightfin);
        this.FacePiece.addChild(this.leftfin);
        this.BodyBase.addChild(this.FacePiece);
        this.BackPiece = new ModelRenderer(this, "BackPiece");
        this.BackPiece.setRotationPoint(0.0f, 0.0f, 1.5f);
        this.setRotation(this.BackPiece, 0.0f, 0.0f, 0.0f);
        this.BackPiece.mirror = true;
        this.BodyBack = new ModelRenderer(this, 46, 19);
        this.BodyBack.addBox(-1.5f, -3.0f, -2.0f, 3, 7, 2);
        this.BodyBack.setRotationPoint(0.0f, -0.5f, 2.0f);
        this.BodyBack.setTextureSize(64, 32);
        this.BodyBack.mirror = true;
        this.setRotation(this.BodyBack, 0.0f, 0.0f, 0.0f);
        this.Rear = new ModelRenderer(this, 6, 15);
        this.Rear.addBox(-1.0f, -1.5f, 0.0f, 2, 4, 1);
        this.Rear.setRotationPoint(0.0f, -0.5f, 2.0f);
        this.Rear.setTextureSize(64, 32);
        this.Rear.mirror = true;
        this.setRotation(this.Rear, 0.0f, 0.0f, 0.0f);
        this.Rearfin = new ModelRenderer(this, 22, 1);
        this.Rearfin.addBox(0.0f, -4.0f, 0.0f, 0, 8, 5);
        this.Rearfin.setRotationPoint(0.0f, 0.0f, 3.0f);
        this.Rearfin.setTextureSize(64, 32);
        this.Rearfin.mirror = true;
        this.setRotation(this.Rearfin, 0.0f, 0.0f, 0.0f);
        this.BackPiece.addChild(this.BodyBack);
        this.BackPiece.addChild(this.Rear);
        this.BackPiece.addChild(this.Rearfin);
        this.BodyBase.addChild(this.BackPiece);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        if (entity.isInWater()) {
            this.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        } else {
            this.setRotationAnglesOutOfWater(f, f1, f2, f3, f4, f5);
        }
        this.BodyBase.render(f5);
    }

    private void setRotationAnglesOutOfWater(float f, float f1, float f2, float f3, float f4, float f5) {
        this.BodyBase.rotateAngleZ = 1.5f;
        this.BodyBase.rotationPointY = 22.0f;
        this.Rearfin.rotateAngleY = 0.0f;
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    @Override
    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
        this.Rearfin.rotateAngleY = MathHelper.cos(f * 0.7853982f) * 2.0f * f1;
        this.BodyBase.rotateAngleZ = 0.0f;
        this.BodyBase.rotationPointY = 17.5f;
    }
}

