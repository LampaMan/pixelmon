/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.pokemon;

import com.pixelmongenerations.client.models.ModelCustomWrapper;
import com.pixelmongenerations.client.models.PixelmonModelBase;
import com.pixelmongenerations.client.models.PixelmonModelRenderer;
import com.pixelmongenerations.client.models.animations.SkeletonBase;
import com.pixelmongenerations.client.models.obj.ObjLoader;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.helpers.animation.IncrementingVariable;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;

public class ModelLampent
extends PixelmonModelBase {
    PixelmonModelRenderer Body;
    PixelmonModelRenderer Glass;
    PixelmonModelRenderer BFlame;
    PixelmonModelRenderer SFlame;
    PixelmonModelRenderer Arms;

    public ModelLampent() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.Body = new PixelmonModelRenderer(this, "Body");
        this.Body.setRotationPoint(0.0f, 23.0f, 0.0f);
        this.Body.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/lampent/LampentBody.obj"))));
        this.SFlame = new PixelmonModelRenderer(this, 0, 0);
        this.SFlame.setRotationPoint(0.0f, 7.26f, 0.0f);
        this.SFlame.setTransparent(0.75f);
        this.SFlame.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/lampent/LampentSFlame.obj"))));
        this.BFlame = new PixelmonModelRenderer(this, 0, 0);
        this.BFlame.setRotationPoint(0.0f, 6.55f, 0.0f);
        this.BFlame.setTransparent(0.85f);
        this.BFlame.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/lampent/LampentFlame.obj"))));
        this.Arms = new PixelmonModelRenderer(this, 0, 0);
        this.Arms.setRotationPoint(0.0f, 5.37f, 0.0f);
        this.Arms.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/lampent/LampentArms.obj"))));
        this.Glass = new PixelmonModelRenderer(this, 0, 0);
        this.Glass.setRotationPoint(0.0f, 6.4f, 0.0f);
        this.Glass.setTransparent(0.6f);
        this.Glass.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/lampent/LampentGlass.obj"))));
        this.Body.addChild(this.SFlame);
        this.Body.addChild(this.BFlame);
        this.Body.addChild(this.Glass);
        this.Body.addChild(this.Arms);
        int degrees = 180;
        float radians = (float)Math.toRadians(degrees);
        this.setRotation(this.Body, radians, 0.0f, 0.0f);
        this.skeleton = new SkeletonBase(this.Body);
        this.registerAnimationCounters();
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.Body.render(f5);
        this.scale = 1.0f;
        if (!(entity instanceof EntityPixelmon)) {
            return;
        }
        EntityPixelmon pixelmon = (EntityPixelmon)entity;
        this.Body.setRotationPoint(0.0f, -0.75f * MathHelper.cos(f2 * 0.1f) + 23.0f, 0.0f);
        IncrementingVariable counter = this.getCounter(0, pixelmon);
        if (counter == null) {
            counter = this.setCounter(0, 90, 2, pixelmon);
        }
        this.BFlame.rotateAngleY = -counter.value / counter.limit * 2.0f * (float)Math.PI;
        this.SFlame.rotateAngleY = counter.value / counter.limit * 2.0f * (float)Math.PI;
    }

    private void setRotation(PixelmonModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    @Override
    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
        this.Arms.rotateAngleZ = MathHelper.cos((float)Math.toRadians(35.0)) * 0.1f * MathHelper.cos(f2 * 0.1f) * (float)Math.PI * 0.25f;
    }
}

