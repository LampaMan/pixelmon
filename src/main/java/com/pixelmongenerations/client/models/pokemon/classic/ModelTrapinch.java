/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.pokemon.classic;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

public class ModelTrapinch
extends ModelBase {
    ModelRenderer body1;
    ModelRenderer body2;
    ModelRenderer Leg1;
    ModelRenderer Leg2;
    ModelRenderer Leg3;
    ModelRenderer Leg4;
    ModelRenderer HEADBASE;
    ModelRenderer TopOfHead;
    ModelRenderer Head;

    public ModelTrapinch() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.setTextureOffset("HEADBASE.Delete", 0, 0);
        this.body1 = new ModelRenderer(this, 0, 0);
        this.body1.addBox(-2.0f, -2.0f, -2.0f, 5, 5, 6);
        this.body1.setRotationPoint(0.0f, 18.0f, 0.5f);
        this.body1.setTextureSize(64, 32);
        this.body1.mirror = true;
        this.setRotation(this.body1, 0.0f, 0.0f, 0.0f);
        this.body2 = new ModelRenderer(this, 0, 11);
        this.body2.addBox(-3.0f, -2.0f, -2.0f, 6, 3, 4);
        this.body2.setRotationPoint(0.5f, 19.0f, 1.5f);
        this.body2.setTextureSize(64, 32);
        this.body2.mirror = true;
        this.setRotation(this.body2, 0.0f, 0.0f, 0.0f);
        this.Leg1 = new ModelRenderer(this, 24, 0);
        this.Leg1.addBox(-1.0f, 0.0f, -1.0f, 2, 4, 2);
        this.Leg1.setRotationPoint(2.0f, 20.5f, 0.0f);
        this.Leg1.setTextureSize(64, 32);
        this.Leg1.mirror = true;
        this.setRotation(this.Leg1, 0.0f, 0.0f, -0.2094395f);
        this.Leg2 = new ModelRenderer(this, 24, 0);
        this.Leg2.addBox(-1.0f, 0.0f, -1.0f, 2, 4, 2);
        this.Leg2.setRotationPoint(2.0f, 20.5f, 3.0f);
        this.Leg2.setTextureSize(64, 32);
        this.Leg2.mirror = true;
        this.setRotation(this.Leg2, 0.0f, 0.0f, -0.2094395f);
        this.Leg3 = new ModelRenderer(this, 24, 0);
        this.Leg3.addBox(-1.0f, 0.0f, -1.0f, 2, 4, 2);
        this.Leg3.setRotationPoint(-1.0f, 20.5f, 0.0f);
        this.Leg3.setTextureSize(64, 32);
        this.Leg3.mirror = true;
        this.setRotation(this.Leg3, 0.0f, 0.0f, 0.2094395f);
        this.Leg4 = new ModelRenderer(this, 24, 0);
        this.Leg4.addBox(-1.0f, 0.0f, -1.0f, 2, 4, 2);
        this.Leg4.setRotationPoint(-1.0f, 20.5f, 3.0f);
        this.Leg4.setTextureSize(64, 32);
        this.Leg4.mirror = true;
        this.setRotation(this.Leg4, 0.0f, 0.0f, 0.2094395f);
        this.HEADBASE = new ModelRenderer(this, "HEADBASE");
        this.HEADBASE.setRotationPoint(0.5f, 16.0f, -2.0f);
        this.setRotation(this.HEADBASE, 0.0f, 0.0f, 0.0f);
        this.HEADBASE.mirror = true;
        this.TopOfHead = new ModelRenderer(this, 30, 21);
        this.TopOfHead.addBox(-2.0f, -2.5f, -2.5f, 4, 6, 5);
        this.TopOfHead.setRotationPoint(0.0f, -0.5f, -2.5f);
        this.TopOfHead.setTextureSize(64, 32);
        this.TopOfHead.mirror = true;
        this.setRotation(this.TopOfHead, -0.0698132f, 0.0f, 0.0f);
        this.Head = new ModelRenderer(this, 0, 19);
        this.Head.addBox(-1.5f, -11.5f, -6.0f, 6, 6, 7);
        this.Head.setRotationPoint(-1.5f, 9.0f, -1.0f);
        this.Head.setTextureSize(64, 32);
        this.Head.mirror = true;
        this.setRotation(this.Head, -0.0698132f, 0.0f, 0.0f);
        this.HEADBASE.addChild(this.TopOfHead);
        this.HEADBASE.addChild(this.Head);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.body1.render(f5);
        this.body2.render(f5);
        this.Leg1.render(f5);
        this.Leg2.render(f5);
        this.Leg3.render(f5);
        this.Leg4.render(f5);
        this.HEADBASE.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    @Override
    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
        this.HEADBASE.rotateAngleX = f4 / 57.29578f;
        this.HEADBASE.rotateAngleY = f3 / 57.29578f;
        this.Leg1.rotateAngleX = MathHelper.cos(f * 0.7f) * 1.0f * f1;
        this.Leg2.rotateAngleX = MathHelper.cos(f * 0.7f + 3.14159f) * 1.0f * f1;
        this.Leg3.rotateAngleX = MathHelper.cos(f * 0.7f + 3.14159f) * 1.0f * f1;
        this.Leg4.rotateAngleX = MathHelper.cos(f * 0.7f) * 1.0f * f1;
    }
}

