/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.pokemon.classic;

import com.pixelmongenerations.client.models.PixelmonModelBase;
import com.pixelmongenerations.client.models.PixelmonModelRenderer;
import com.pixelmongenerations.client.models.animations.EnumArm;
import com.pixelmongenerations.client.models.animations.EnumLeg;
import com.pixelmongenerations.client.models.animations.EnumPhase;
import com.pixelmongenerations.client.models.animations.EnumRotation;
import com.pixelmongenerations.client.models.animations.ModuleArm;
import com.pixelmongenerations.client.models.animations.ModuleHead;
import com.pixelmongenerations.client.models.animations.ModuleLeg;
import com.pixelmongenerations.client.models.animations.ModuleTailBasic;
import com.pixelmongenerations.client.models.animations.biped.SkeletonBiped;
import net.minecraft.entity.Entity;

public class ModelSquirtle
extends PixelmonModelBase {
    PixelmonModelRenderer Body;

    public ModelSquirtle() {
        this.textureWidth = 512;
        this.textureHeight = 512;
        this.Body = new PixelmonModelRenderer(this, "Body");
        this.Body.setRotationPoint(0.0f, 0.0f, 0.0f);
        PixelmonModelRenderer Body1 = new PixelmonModelRenderer(this, 0, 13);
        Body1.addBox(0.0f, 0.0f, 0.0f, 7, 9, 2);
        Body1.setRotationPoint(-3.5f, 10.0f, 2.0f);
        Body1.setTextureSize(512, 512);
        Body1.mirror = true;
        this.setRotation(Body1, 0.0f, 0.0f, 0.0f);
        PixelmonModelRenderer Body2 = new PixelmonModelRenderer(this, 18, 13);
        Body2.addBox(0.0f, 0.0f, 0.0f, 8, 10, 2);
        Body2.setRotationPoint(-4.0f, 9.5f, 1.0f);
        Body2.setTextureSize(512, 512);
        Body2.mirror = true;
        this.setRotation(Body2, 0.0f, 0.0f, 0.0f);
        PixelmonModelRenderer Body3 = new PixelmonModelRenderer(this, 38, 13);
        Body3.addBox(0.0f, 0.0f, 0.0f, 6, 8, 2);
        Body3.setRotationPoint(-3.0f, 10.5f, -0.5f);
        Body3.setTextureSize(512, 512);
        Body3.mirror = true;
        this.setRotation(Body3, 0.0f, 0.0f, 0.0f);
        PixelmonModelRenderer Body4 = new PixelmonModelRenderer(this, 0, 24);
        Body4.addBox(0.0f, 0.0f, 0.0f, 4, 7, 1);
        Body4.setRotationPoint(-2.0f, 11.0f, -1.5f);
        Body4.setTextureSize(512, 512);
        Body4.mirror = true;
        this.setRotation(Body4, 0.0f, 0.0f, 0.0f);
        PixelmonModelRenderer Crouch = new PixelmonModelRenderer(this, 0, 0);
        Crouch.addBox(0.0f, 0.0f, 0.0f, 2, 2, 1);
        Crouch.setRotationPoint(-1.0f, 19.0f, 2.0f);
        Crouch.setTextureSize(512, 512);
        Crouch.mirror = true;
        this.setRotation(Crouch, 0.0f, 0.0f, 0.0f);
        this.Body.addChild(Body1);
        this.Body.addChild(Body2);
        this.Body.addChild(Body3);
        this.Body.addChild(Body4);
        this.Body.addChild(Crouch);
        PixelmonModelRenderer Head = new PixelmonModelRenderer(this, "Head");
        Head.setRotationPoint(0.0f, 9.5f, 2.0f);
        PixelmonModelRenderer HeadMain = new PixelmonModelRenderer(this, 66, 0);
        HeadMain.addBox(-3.5f, -7.0f, -2.0f, 7, 7, 6);
        HeadMain.setTextureSize(512, 512);
        HeadMain.mirror = true;
        this.setRotation(HeadMain, 0.0f, 0.0f, 0.0f);
        PixelmonModelRenderer HeadLeft = new PixelmonModelRenderer(this, 0, 0);
        HeadLeft.addBox(0.0f, 0.0f, 0.0f, 1, 5, 4);
        HeadLeft.setRotationPoint(-4.0f, -5.5f, -1.0f);
        HeadLeft.setTextureSize(512, 512);
        HeadLeft.mirror = true;
        this.setRotation(HeadLeft, 0.0f, 0.0f, 0.0f);
        PixelmonModelRenderer HeadRight = new PixelmonModelRenderer(this, 0, 0);
        HeadRight.addBox(0.0f, 0.0f, 0.0f, 1, 5, 4);
        HeadRight.setRotationPoint(3.0f, -5.5f, -1.0f);
        HeadRight.setTextureSize(512, 512);
        HeadRight.mirror = true;
        this.setRotation(HeadRight, 0.0f, 0.0f, 0.0f);
        Head.addChild(HeadMain);
        Head.addChild(HeadLeft);
        Head.addChild(HeadRight);
        this.Body.addChild(Head);
        PixelmonModelRenderer LeftArm = new PixelmonModelRenderer(this, 0, 0);
        LeftArm.addBox(-2.0f, 0.0f, 0.0f, 2, 2, 5);
        LeftArm.setRotationPoint(-2.0f, 10.0f, 3.0f);
        LeftArm.setTextureSize(512, 512);
        LeftArm.mirror = true;
        this.setRotation(LeftArm, -0.0569039f, -0.3490659f, 0.0f);
        this.Body.addChild(LeftArm);
        PixelmonModelRenderer RightArm = new PixelmonModelRenderer(this, 0, 0);
        RightArm.addBox(0.0f, 0.0f, 0.0f, 2, 2, 5);
        RightArm.setRotationPoint(2.0f, 10.0f, 3.0f);
        RightArm.setTextureSize(512, 512);
        RightArm.mirror = true;
        this.setRotation(RightArm, -0.1563567f, 0.4036978f, 0.0f);
        this.Body.addChild(RightArm);
        PixelmonModelRenderer LeftLeg = new PixelmonModelRenderer(this, 54, 0);
        LeftLeg.addBox(-1.5f, 0.0f, -1.5f, 3, 4, 3);
        LeftLeg.setRotationPoint(-3.0f, 18.0f, 3.0f);
        LeftLeg.setTextureSize(512, 512);
        LeftLeg.mirror = true;
        this.setRotation(LeftLeg, 0.0872665f, -1.989675f, -0.0523599f);
        this.Body.addChild(LeftLeg);
        PixelmonModelRenderer RightLeg = new PixelmonModelRenderer(this, 54, 0);
        RightLeg.addBox(-1.5f, 0.0f, -1.5f, 3, 4, 3);
        RightLeg.setRotationPoint(3.0f, 18.0f, 3.0f);
        RightLeg.setTextureSize(512, 512);
        RightLeg.mirror = true;
        this.setRotation(RightLeg, -0.0523599f, -0.9948377f, -0.0872665f);
        this.Body.addChild(RightLeg);
        PixelmonModelRenderer Tail = new PixelmonModelRenderer(this, "Tail");
        Tail.setRotationPoint(0.0f, 19.0f, 2.0f);
        PixelmonModelRenderer TailBase = new PixelmonModelRenderer(this, 0, 0);
        TailBase.addBox(-1.5f, 0.0f, -4.0f, 3, 2, 4);
        TailBase.setTextureSize(512, 512);
        TailBase.mirror = true;
        this.setRotation(TailBase, 0.0f, 0.0f, 0.0f);
        PixelmonModelRenderer Tail2 = new PixelmonModelRenderer(this, 28, 0);
        Tail2.addBox(0.0f, 0.0f, 0.0f, 3, 3, 4);
        Tail2.setRotationPoint(-1.5f, -2.0f, -6.0f);
        Tail2.setTextureSize(512, 512);
        Tail2.mirror = true;
        this.setRotation(Tail2, -0.3847986f, 0.0f, 0.0f);
        PixelmonModelRenderer Tail3 = new PixelmonModelRenderer(this, 43, 0);
        Tail3.addBox(0.0f, 0.0f, 0.0f, 1, 2, 4);
        Tail3.setRotationPoint(1.0f, -1.5f, -6.0f);
        Tail3.setTextureSize(512, 512);
        Tail3.mirror = true;
        this.setRotation(Tail3, -0.3839724f, 0.0f, 0.0f);
        PixelmonModelRenderer Tail4 = new PixelmonModelRenderer(this, 41, 0);
        Tail4.addBox(0.0f, 0.0f, 0.0f, 1, 2, 4);
        Tail4.setRotationPoint(-2.0f, -1.6f, -6.0f);
        Tail4.setTextureSize(512, 512);
        Tail4.mirror = true;
        this.setRotation(Tail4, -0.3839724f, 0.0f, 0.0f);
        Tail.addChild(TailBase);
        Tail.addChild(Tail2);
        Tail.addChild(Tail3);
        Tail.addChild(Tail4);
        this.Body.addChild(Tail);
        ModuleArm leftArmModule = new ModuleArm(LeftArm, EnumArm.Left, EnumRotation.x, 0.0f, 0.0f);
        ModuleArm rightArmModule = new ModuleArm(RightArm, EnumArm.Right, EnumRotation.x, 0.0f, 0.0f);
        ModuleHead headModule = new ModuleHead(Head);
        float legspeed = 0.5f;
        float legRotationLimit = 1.4f;
        ModuleLeg leftLegModule = new ModuleLeg(LeftLeg, EnumLeg.FrontLeft, EnumPhase.InPhase, EnumRotation.x, legRotationLimit, legspeed);
        ModuleLeg rightLegModule = new ModuleLeg(RightLeg, EnumLeg.FrontRight, EnumPhase.InPhase, EnumRotation.x, legRotationLimit, legspeed);
        ModuleTailBasic tailModule = new ModuleTailBasic(Tail, 0.2f, 0.05f, legspeed);
        this.skeleton = new SkeletonBiped(this.Body, headModule, leftArmModule, rightArmModule, leftLegModule, rightLegModule, tailModule);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5);
        this.Body.render(f5);
    }

    private void setRotation(PixelmonModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleX = y;
        model.rotateAngleX = z;
        this.Body.rotateAngleY = (float)Math.toRadians(180.0);
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5) {
    }
}

