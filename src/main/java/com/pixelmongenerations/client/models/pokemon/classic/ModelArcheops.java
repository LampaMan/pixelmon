/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.pokemon.classic;

import com.pixelmongenerations.client.models.PixelmonModelBase;
import com.pixelmongenerations.client.models.PixelmonModelRenderer;
import com.pixelmongenerations.client.models.animations.ModuleHead;
import com.pixelmongenerations.client.models.animations.biped.SkeletonBiped;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

public class ModelArcheops
extends PixelmonModelBase {
    PixelmonModelRenderer Body;
    PixelmonModelRenderer LeftWing;
    PixelmonModelRenderer RightWing;
    PixelmonModelRenderer LeftLeg;
    PixelmonModelRenderer RightLeg;
    PixelmonModelRenderer Tail;

    public ModelArcheops() {
        this.textureWidth = 128;
        this.textureHeight = 64;
        this.Body = new PixelmonModelRenderer(this, "Body");
        this.Body.setRotationPoint(0.0f, 14.0f, -2.0f);
        PixelmonModelRenderer neck_feathers_bottom = new PixelmonModelRenderer(this, 46, 0);
        neck_feathers_bottom.addBox(-2.5f, 0.2666667f, -6.133333f, 5, 1, 4);
        neck_feathers_bottom.setTextureSize(128, 64);
        neck_feathers_bottom.mirror = true;
        this.setRotation(neck_feathers_bottom, 0.5235988f, 0.0f, 0.0f);
        PixelmonModelRenderer neck_feathers_side_R_2 = new PixelmonModelRenderer(this, 65, 8);
        neck_feathers_side_R_2.addBox(-2.1f, -1.333333f, -5.9f, 1, 3, 4);
        neck_feathers_side_R_2.setTextureSize(128, 64);
        neck_feathers_side_R_2.mirror = true;
        this.setRotation(neck_feathers_side_R_2, 0.1919862f, 0.6283185f, -0.7504916f);
        PixelmonModelRenderer neck_feathers_side_L_2 = new PixelmonModelRenderer(this, 65, 0);
        neck_feathers_side_L_2.addBox(1.066667f, -1.333333f, -5.866667f, 1, 3, 4);
        neck_feathers_side_L_2.setTextureSize(128, 64);
        neck_feathers_side_L_2.mirror = true;
        this.setRotation(neck_feathers_side_L_2, 0.1919862f, -0.6283185f, 0.7504916f);
        PixelmonModelRenderer neck_feathers_side_L = new PixelmonModelRenderer(this, 76, 6);
        neck_feathers_side_L.addBox(0.6f, -0.6f, -4.866667f, 1, 3, 4);
        neck_feathers_side_L.setTextureSize(128, 64);
        neck_feathers_side_L.mirror = true;
        this.setRotation(neck_feathers_side_L, -0.715585f, -0.7853982f, 0.0698132f);
        PixelmonModelRenderer neck_feathers_side_R = new PixelmonModelRenderer(this, 87, 6);
        neck_feathers_side_R.addBox(-1.633333f, -0.6f, -4.866667f, 1, 3, 4);
        neck_feathers_side_R.setTextureSize(128, 64);
        neck_feathers_side_R.mirror = true;
        this.setRotation(neck_feathers_side_R, -0.715585f, 0.7853982f, -0.0698132f);
        PixelmonModelRenderer tail_base_feathers_side_2_L = new PixelmonModelRenderer(this, 96, 22);
        tail_base_feathers_side_2_L.addBox(-6.0f, -2.666667f, 5.866667f, 1, 3, 3);
        tail_base_feathers_side_2_L.setTextureSize(128, 64);
        tail_base_feathers_side_2_L.mirror = true;
        this.setRotation(tail_base_feathers_side_2_L, -1.082104f, 0.9075712f, -0.418879f);
        PixelmonModelRenderer tail_base_feathers_side_2_R = new PixelmonModelRenderer(this, 105, 22);
        tail_base_feathers_side_2_R.addBox(5.0f, -2.666667f, 5.866667f, 1, 3, 3);
        tail_base_feathers_side_2_R.setTextureSize(128, 64);
        tail_base_feathers_side_2_R.mirror = true;
        this.setRotation(tail_base_feathers_side_2_R, -1.082104f, -0.9075712f, 0.418879f);
        PixelmonModelRenderer tail_base_feathers_side_L = new PixelmonModelRenderer(this, 98, 14);
        tail_base_feathers_side_L.addBox(-2.8f, 3.733333f, 4.733333f, 1, 3, 4);
        tail_base_feathers_side_L.setTextureSize(128, 64);
        tail_base_feathers_side_L.mirror = true;
        this.setRotation(tail_base_feathers_side_L, 0.1047198f, 0.3490659f, -0.3141593f);
        PixelmonModelRenderer tail_base_feathers_side_R = new PixelmonModelRenderer(this, 98, 6);
        tail_base_feathers_side_R.addBox(1.8f, 3.733333f, 4.733333f, 1, 3, 4);
        tail_base_feathers_side_R.setTextureSize(128, 64);
        tail_base_feathers_side_R.mirror = true;
        this.setRotation(tail_base_feathers_side_R, 0.1047198f, -0.3490659f, 0.3141593f);
        PixelmonModelRenderer tail_base_feathers_top = new PixelmonModelRenderer(this, 95, 0);
        tail_base_feathers_top.addBox(-2.0f, 4.4f, 5.8f, 4, 1, 4);
        tail_base_feathers_top.setTextureSize(128, 64);
        tail_base_feathers_top.mirror = true;
        this.setRotation(tail_base_feathers_top, 0.0872665f, 0.0f, 0.0f);
        PixelmonModelRenderer neck_feathers_top = new PixelmonModelRenderer(this, 76, 0);
        neck_feathers_top.addBox(-2.5f, -1.266667f, -4.6f, 5, 1, 4);
        neck_feathers_top.setTextureSize(128, 64);
        neck_feathers_top.mirror = true;
        this.setRotation(neck_feathers_top, -1.151917f, 0.0f, 0.0f);
        PixelmonModelRenderer lower_body = new PixelmonModelRenderer(this, 0, 41);
        lower_body.addBox(-2.5f, -2.666667f, 3.066667f, 5, 5, 5);
        lower_body.setTextureSize(128, 64);
        lower_body.mirror = true;
        this.setRotation(lower_body, -0.7853982f, 0.0f, 0.0f);
        PixelmonModelRenderer main_body = new PixelmonModelRenderer(this, 0, 52);
        main_body.addBox(-3.0f, -2.0f, -2.0f, 6, 6, 6);
        main_body.setTextureSize(128, 64);
        main_body.mirror = true;
        this.setRotation(main_body, -0.5759587f, 0.0f, 0.0f);
        PixelmonModelRenderer neck_base = new PixelmonModelRenderer(this, 0, 32);
        neck_base.addBox(-2.5f, -2.666667f, -1.266667f, 5, 5, 3);
        neck_base.setRotationPoint(0.0f, 0.0f, -2.0f);
        neck_base.setTextureSize(128, 64);
        neck_base.mirror = true;
        this.setRotation(neck_base, -0.4537856f, 0.0f, 0.0f);
        PixelmonModelRenderer neck_1 = new PixelmonModelRenderer(this, 0, 23);
        neck_1.addBox(-2.0f, -2.133333f, -3.266667f, 4, 4, 4);
        neck_1.setRotationPoint(0.0f, -1.0f, -3.0f);
        neck_1.setTextureSize(128, 64);
        neck_1.mirror = true;
        this.setRotation(neck_1, -0.5235988f, 0.0f, 0.0f);
        PixelmonModelRenderer neck_2 = new PixelmonModelRenderer(this, 0, 15);
        neck_2.addBox(-1.5f, -1.6f, -2.866667f, 3, 3, 4);
        neck_2.setRotationPoint(0.0f, -3.0f, -6.0f);
        neck_2.setTextureSize(128, 64);
        neck_2.mirror = true;
        this.setRotation(neck_2, -0.4363323f, 0.0f, 0.0f);
        this.Body.addChild(neck_feathers_bottom);
        this.Body.addChild(neck_feathers_side_R_2);
        this.Body.addChild(neck_feathers_side_L_2);
        this.Body.addChild(neck_feathers_side_L);
        this.Body.addChild(neck_feathers_side_R);
        this.Body.addChild(tail_base_feathers_side_2_L);
        this.Body.addChild(tail_base_feathers_side_2_R);
        this.Body.addChild(tail_base_feathers_side_L);
        this.Body.addChild(tail_base_feathers_side_R);
        this.Body.addChild(tail_base_feathers_top);
        this.Body.addChild(neck_feathers_top);
        this.Body.addChild(lower_body);
        this.Body.addChild(main_body);
        this.Body.addChild(neck_base);
        this.Body.addChild(neck_2);
        this.Body.addChild(neck_1);
        PixelmonModelRenderer Head = new PixelmonModelRenderer(this, "Head");
        Head.setRotationPoint(0.0f, -4.0f, -9.0f);
        PixelmonModelRenderer head = new PixelmonModelRenderer(this, 25, 59);
        head.addBox(-2.0f, -1.3f, -1.0f, 4, 3, 2);
        head.setTextureSize(128, 64);
        head.mirror = true;
        this.setRotation(head, 0.2792527f, 0.0f, 0.0f);
        PixelmonModelRenderer upper_jaw = new PixelmonModelRenderer(this, 24, 53);
        upper_jaw.addBox(-1.0f, -1.7f, -4.6f, 2, 2, 3);
        upper_jaw.setTextureSize(128, 64);
        upper_jaw.mirror = true;
        this.setRotation(upper_jaw, 0.418879f, 0.0f, 0.0f);
        PixelmonModelRenderer upper_jaw_tooth_L = new PixelmonModelRenderer(this, 26, 0);
        upper_jaw_tooth_L.addBox(1.7f, 0.1f, -3.666667f, 1, 1, 1);
        upper_jaw_tooth_L.setTextureSize(128, 64);
        upper_jaw_tooth_L.mirror = true;
        this.setRotation(upper_jaw_tooth_L, 0.3316126f, 0.3665191f, 0.1919862f);
        PixelmonModelRenderer upper_jaw_tooth_R = new PixelmonModelRenderer(this, 31, 0);
        upper_jaw_tooth_R.addBox(-2.7f, 0.1f, -3.666667f, 1, 1, 1);
        upper_jaw_tooth_R.setTextureSize(128, 64);
        upper_jaw_tooth_R.mirror = true;
        this.setRotation(upper_jaw_tooth_R, 0.3316126f, -0.3665191f, -0.1919862f);
        PixelmonModelRenderer lower_jaw_tooth_R = new PixelmonModelRenderer(this, 36, 0);
        lower_jaw_tooth_R.addBox(-2.0f, -1.033333f, -1.966667f, 1, 1, 1);
        lower_jaw_tooth_R.setRotationPoint(0.0f, 1.2f, -0.06667f);
        lower_jaw_tooth_R.setTextureSize(128, 64);
        lower_jaw_tooth_R.mirror = true;
        this.setRotation(lower_jaw_tooth_R, 0.7330383f, -0.3316126f, -0.0523599f);
        PixelmonModelRenderer lower_jaw_tooth_L = new PixelmonModelRenderer(this, 41, 0);
        lower_jaw_tooth_L.addBox(1.0f, -1.033333f, -1.966667f, 1, 1, 1);
        lower_jaw_tooth_L.setRotationPoint(0.0f, 1.2f, -0.06667f);
        lower_jaw_tooth_L.setTextureSize(128, 64);
        lower_jaw_tooth_L.mirror = true;
        this.setRotation(lower_jaw_tooth_L, 0.7330383f, 0.3316126f, 0.0523599f);
        PixelmonModelRenderer head_side_top_L = new PixelmonModelRenderer(this, 29, 25);
        head_side_top_L.addBox(0.3f, -2.6f, -1.533333f, 1, 1, 3);
        head_side_top_L.setTextureSize(128, 64);
        head_side_top_L.mirror = true;
        this.setRotation(head_side_top_L, 0.418879f, 0.122173f, 0.6457718f);
        PixelmonModelRenderer head_side_top_R = new PixelmonModelRenderer(this, 29, 25);
        head_side_top_R.addBox(-1.3f, -2.6f, -1.533333f, 1, 1, 3);
        head_side_top_R.setTextureSize(128, 64);
        head_side_top_R.mirror = true;
        this.setRotation(head_side_top_R, 0.418879f, -0.122173f, -0.6457718f);
        PixelmonModelRenderer head_side_back_L = new PixelmonModelRenderer(this, 28, 15);
        head_side_back_L.addBox(1.7f, -1.366667f, -0.9f, 1, 2, 2);
        head_side_back_L.setTextureSize(128, 64);
        head_side_back_L.mirror = true;
        this.setRotation(head_side_back_L, 0.2268928f, -0.3490659f, -0.122173f);
        PixelmonModelRenderer head_side_back_R = new PixelmonModelRenderer(this, 28, 15);
        head_side_back_R.addBox(-2.7f, -1.366667f, -0.9f, 1, 2, 2);
        head_side_back_R.setTextureSize(128, 64);
        head_side_back_R.mirror = true;
        this.setRotation(head_side_back_R, 0.2268928f, 0.3490659f, 0.122173f);
        PixelmonModelRenderer head_top_2 = new PixelmonModelRenderer(this, 24, 20);
        head_top_2.addBox(-1.5f, -2.0f, -1.3f, 3, 1, 3);
        head_top_2.setTextureSize(128, 64);
        head_top_2.mirror = true;
        this.setRotation(head_top_2, 0.0698132f, 0.0f, 0.0f);
        PixelmonModelRenderer head_top_1 = new PixelmonModelRenderer(this, 15, 14);
        head_top_1.addBox(-1.0f, -2.366667f, -4.266667f, 2, 1, 4);
        head_top_1.setTextureSize(128, 64);
        head_top_1.mirror = true;
        this.setRotation(head_top_1, 0.5585054f, 0.0f, 0.0f);
        PixelmonModelRenderer head_side_R = new PixelmonModelRenderer(this, 16, 20);
        head_side_R.addBox(-2.6f, -1.3f, -4.0f, 1, 2, 5);
        head_side_R.setTextureSize(128, 64);
        head_side_R.mirror = true;
        this.setRotation(head_side_R, 0.3316126f, -0.3316126f, -0.0523599f);
        PixelmonModelRenderer head_side_L = new PixelmonModelRenderer(this, 17, 28);
        head_side_L.addBox(1.6f, -1.3f, -4.0f, 1, 2, 5);
        head_side_L.setTextureSize(128, 64);
        head_side_L.mirror = true;
        this.setRotation(head_side_L, 0.3316126f, 0.3316126f, 0.0523599f);
        PixelmonModelRenderer lower_jaw_R = new PixelmonModelRenderer(this, 17, 36);
        lower_jaw_R.addBox(-2.0f, -0.5666667f, -2.9f, 1, 1, 3);
        lower_jaw_R.setRotationPoint(0.0f, 1.2f, -0.06667f);
        lower_jaw_R.setTextureSize(128, 64);
        lower_jaw_R.mirror = true;
        this.setRotation(lower_jaw_R, 0.7679449f, -0.3316126f, -0.0349066f);
        PixelmonModelRenderer lower_jaw_L = new PixelmonModelRenderer(this, 16, 41);
        lower_jaw_L.addBox(1.0f, -0.5666667f, -2.9f, 1, 1, 3);
        lower_jaw_L.setRotationPoint(0.0f, 1.2f, -0.06667f);
        lower_jaw_L.setTextureSize(128, 64);
        lower_jaw_L.mirror = true;
        this.setRotation(lower_jaw_L, 0.7679449f, 0.3316126f, 0.0349066f);
        PixelmonModelRenderer lower_jaw = new PixelmonModelRenderer(this, 21, 47);
        lower_jaw.addBox(-1.0f, -0.5666667f, -3.5f, 2, 1, 4);
        lower_jaw.setRotationPoint(0.0f, 1.2f, -0.06667f);
        lower_jaw.setTextureSize(128, 64);
        lower_jaw.mirror = true;
        this.setRotation(lower_jaw, 0.715585f, 0.0f, 0.0f);
        Head.addChild(head);
        Head.addChild(upper_jaw);
        Head.addChild(upper_jaw_tooth_L);
        Head.addChild(upper_jaw_tooth_R);
        Head.addChild(lower_jaw_tooth_R);
        Head.addChild(lower_jaw_tooth_L);
        Head.addChild(head_side_top_L);
        Head.addChild(head_side_top_R);
        Head.addChild(head_side_back_L);
        Head.addChild(head_side_back_R);
        Head.addChild(head_top_2);
        Head.addChild(head_top_1);
        Head.addChild(head_side_R);
        Head.addChild(head_side_L);
        Head.addChild(lower_jaw_R);
        Head.addChild(lower_jaw_L);
        Head.addChild(lower_jaw);
        this.Body.addChild(Head);
        this.Tail = new PixelmonModelRenderer(this, "Tail");
        this.Tail.setRotationPoint(0.0f, 5.0f, 6.0f);
        PixelmonModelRenderer tail_1 = new PixelmonModelRenderer(this, 112, 0);
        tail_1.addBox(-2.0f, -1.6f, -0.4f, 4, 4, 4);
        tail_1.setTextureSize(128, 64);
        tail_1.mirror = true;
        this.setRotation(tail_1, -0.4363323f, 0.0f, 0.0f);
        PixelmonModelRenderer tail_2 = new PixelmonModelRenderer(this, 114, 9);
        tail_2.addBox(-1.5f, -1.666667f, -0.8f, 3, 3, 4);
        tail_2.setRotationPoint(0.0f, 1.66667f, 3.266667f);
        tail_2.setTextureSize(128, 64);
        tail_2.mirror = true;
        this.setRotation(tail_2, -0.296706f, 0.0f, 0.0f);
        PixelmonModelRenderer tail_3 = new PixelmonModelRenderer(this, 114, 17);
        tail_3.addBox(-1.0f, -0.8666667f, -0.06666667f, 2, 2, 5);
        tail_3.setRotationPoint(0.0f, 2.13333f, 6.4f);
        tail_3.setTextureSize(128, 64);
        tail_3.mirror = true;
        this.setRotation(tail_3, -0.1047198f, 0.0f, 0.0f);
        PixelmonModelRenderer tail_feathers = new PixelmonModelRenderer(this, 84, 53);
        tail_feathers.addBox(-6.0f, -0.6f, 3.666667f, 12, 1, 10);
        tail_feathers.setRotationPoint(0.0f, 2.8f, 10.53333f);
        tail_feathers.setTextureSize(128, 64);
        tail_feathers.mirror = true;
        this.setRotation(tail_feathers, 0.0349066f, 0.0f, 0.0f);
        PixelmonModelRenderer tail_4 = new PixelmonModelRenderer(this, 114, 25);
        tail_4.addBox(-0.5f, -0.8f, 0.4666667f, 1, 1, 6);
        tail_4.setRotationPoint(0.0f, 2.8f, 10.53333f);
        tail_4.setTextureSize(128, 64);
        tail_4.mirror = true;
        this.setRotation(tail_4, 0.0523599f, 0.0f, 0.0f);
        this.Tail.addChild(tail_1);
        this.Tail.addChild(tail_3);
        this.Tail.addChild(tail_2);
        this.Tail.addChild(tail_feathers);
        this.Tail.addChild(tail_4);
        this.Body.addChild(this.Tail);
        this.LeftLeg = new PixelmonModelRenderer(this, "Left Leg");
        this.LeftLeg.setRotationPoint(2.533333f, 6.0f, 4.0f);
        PixelmonModelRenderer leg_1_L = new PixelmonModelRenderer(this, 35, 3);
        leg_1_L.addBox(0.5f, 0.7f, -1.266667f, 1, 2, 4);
        leg_1_L.setTextureSize(128, 64);
        leg_1_L.mirror = true;
        this.setRotation(leg_1_L, -0.837758f, -0.3316126f, 0.0f);
        PixelmonModelRenderer leg_2_L = new PixelmonModelRenderer(this, 44, 6);
        leg_2_L.addBox(0.0f, -2.0f, -3.0f, 2, 3, 5);
        leg_2_L.setTextureSize(128, 64);
        leg_2_L.mirror = true;
        this.setRotation(leg_2_L, 0.6283185f, -0.3316126f, -0.0174533f);
        PixelmonModelRenderer foot_L_1 = new PixelmonModelRenderer(this, 56, 12);
        foot_L_1.addBox(0.5f, 3.0f, -3.266667f, 1, 1, 4);
        foot_L_1.setTextureSize(128, 64);
        foot_L_1.mirror = true;
        this.setRotation(foot_L_1, 0.0f, -0.4537856f, 0.0f);
        PixelmonModelRenderer foot_L_2 = new PixelmonModelRenderer(this, 55, 6);
        foot_L_2.addBox(0.5f, 3.0f, -3.466667f, 1, 1, 3);
        foot_L_2.setTextureSize(128, 64);
        foot_L_2.mirror = true;
        this.setRotation(foot_L_2, 0.0f, -0.8901179f, 0.0f);
        PixelmonModelRenderer foot_L_3 = new PixelmonModelRenderer(this, 55, 6);
        foot_L_3.addBox(0.5f, 3.0f, -2.6f, 1, 1, 3);
        foot_L_3.setTextureSize(128, 64);
        foot_L_3.mirror = true;
        this.setRotation(foot_L_3, 0.0f, 0.122173f, 0.0f);
        PixelmonModelRenderer leg_feathers_2_L = new PixelmonModelRenderer(this, 99, 40);
        leg_feathers_2_L.addBox(0.4333333f, 0.8f, -0.7333333f, 1, 5, 7);
        leg_feathers_2_L.setTextureSize(128, 64);
        leg_feathers_2_L.mirror = true;
        this.setRotation(leg_feathers_2_L, 0.2268928f, -0.122173f, -0.3141593f);
        PixelmonModelRenderer leg_feathers_1_L = new PixelmonModelRenderer(this, 116, 43);
        leg_feathers_1_L.addBox(-2.633333f, 0.2f, -2.2f, 1, 4, 5);
        leg_feathers_1_L.setTextureSize(128, 64);
        leg_feathers_1_L.mirror = true;
        this.setRotation(leg_feathers_1_L, -0.5235988f, -0.2268928f, -1.169371f);
        this.LeftLeg.addChild(leg_1_L);
        this.LeftLeg.addChild(leg_2_L);
        this.LeftLeg.addChild(foot_L_1);
        this.LeftLeg.addChild(foot_L_2);
        this.LeftLeg.addChild(foot_L_3);
        this.LeftLeg.addChild(leg_feathers_2_L);
        this.LeftLeg.addChild(leg_feathers_1_L);
        this.Body.addChild(this.LeftLeg);
        this.RightLeg = new PixelmonModelRenderer(this, "Right Leg");
        this.RightLeg.setRotationPoint(-2.466667f, 6.0f, 4.0f);
        PixelmonModelRenderer leg_2_R = new PixelmonModelRenderer(this, 46, 20);
        leg_2_R.addBox(-2.0f, -2.0f, -3.0f, 2, 3, 5);
        leg_2_R.setTextureSize(128, 64);
        leg_2_R.mirror = true;
        this.setRotation(leg_2_R, 0.6283185f, 0.3316126f, 0.0174533f);
        PixelmonModelRenderer leg_1_R = new PixelmonModelRenderer(this, 60, 18);
        leg_1_R.addBox(-1.5f, 0.7f, -1.266667f, 1, 2, 4);
        leg_1_R.setTextureSize(128, 64);
        leg_1_R.mirror = true;
        this.setRotation(leg_1_R, -0.837758f, 0.3316126f, 0.0f);
        PixelmonModelRenderer foot_R_1 = new PixelmonModelRenderer(this, 56, 12);
        foot_R_1.addBox(-1.5f, 3.0f, -3.333333f, 1, 1, 4);
        foot_R_1.setTextureSize(128, 64);
        foot_R_1.mirror = true;
        this.setRotation(foot_R_1, 0.0f, 0.3839724f, 0.0f);
        PixelmonModelRenderer foot_R_2 = new PixelmonModelRenderer(this, 55, 6);
        foot_R_2.addBox(-1.5f, 3.0f, -3.666667f, 1, 1, 3);
        foot_R_2.setTextureSize(128, 64);
        foot_R_2.mirror = true;
        this.setRotation(foot_R_2, 0.0f, 0.8901179f, 0.0f);
        PixelmonModelRenderer foot_R_3 = new PixelmonModelRenderer(this, 55, 6);
        foot_R_3.addBox(-1.5f, 3.0f, -2.266667f, 1, 1, 3);
        foot_R_3.setTextureSize(128, 64);
        foot_R_3.mirror = true;
        this.setRotation(foot_R_3, 0.0f, -0.122173f, 0.0f);
        PixelmonModelRenderer leg_feathers_2_R = new PixelmonModelRenderer(this, 99, 33);
        leg_feathers_2_R.addBox(-1.4f, 0.7666667f, -0.7333333f, 1, 5, 7);
        leg_feathers_2_R.setTextureSize(128, 64);
        leg_feathers_2_R.mirror = true;
        this.setRotation(leg_feathers_2_R, 0.2268928f, 0.122173f, 0.3141593f);
        PixelmonModelRenderer leg_feathers_1_R = new PixelmonModelRenderer(this, 116, 33);
        leg_feathers_1_R.addBox(1.6f, 0.2f, -2.2f, 1, 4, 5);
        leg_feathers_1_R.setTextureSize(128, 64);
        leg_feathers_1_R.mirror = true;
        this.setRotation(leg_feathers_1_R, -0.5235988f, 0.2268928f, 1.169371f);
        this.RightLeg.addChild(leg_2_R);
        this.RightLeg.addChild(leg_1_R);
        this.RightLeg.addChild(foot_R_1);
        this.RightLeg.addChild(foot_R_2);
        this.RightLeg.addChild(foot_R_3);
        this.RightLeg.addChild(leg_feathers_2_R);
        this.RightLeg.addChild(leg_feathers_1_R);
        this.Body.addChild(this.RightLeg);
        this.LeftWing = new PixelmonModelRenderer(this, "Left Wing");
        this.LeftWing.setRotationPoint(3.0f, 0.0f, -1.0f);
        PixelmonModelRenderer wing_1_L = new PixelmonModelRenderer(this, 77, 57);
        wing_1_L.addBox(0.0f, -0.4666667f, -1.066667f, 1, 5, 2);
        wing_1_L.setTextureSize(128, 64);
        wing_1_L.mirror = true;
        this.setRotation(wing_1_L, 0.0872665f, 0.2443461f, -0.7679449f);
        PixelmonModelRenderer wing_feathers_1_L = new PixelmonModelRenderer(this, 65, 41);
        wing_feathers_1_L.addBox(-0.5f, -0.4666667f, -1.066667f, 1, 7, 6);
        wing_feathers_1_L.setTextureSize(128, 64);
        wing_feathers_1_L.mirror = true;
        this.setRotation(wing_feathers_1_L, 0.0872665f, 0.2443461f, -0.7679449f);
        PixelmonModelRenderer wing_2_L = new PixelmonModelRenderer(this, 68, 54);
        wing_2_L.addBox(-0.7f, -0.4666667f, -1.666667f, 1, 7, 3);
        wing_2_L.setRotationPoint(4.0f, 3.0f, -0.533333f);
        wing_2_L.setTextureSize(128, 64);
        wing_2_L.mirror = true;
        this.setRotation(wing_2_L, -0.1047198f, 0.4712389f, -0.7679449f);
        PixelmonModelRenderer wing_feathers_2_L = new PixelmonModelRenderer(this, 80, 37);
        wing_feathers_2_L.addBox(-1.166667f, -0.5f, 0.3333333f, 1, 8, 8);
        wing_feathers_2_L.setRotationPoint(4.0f, 3.0f, -0.533333f);
        wing_feathers_2_L.setTextureSize(128, 64);
        wing_feathers_2_L.mirror = true;
        this.setRotation(wing_feathers_2_L, -0.1047198f, 0.4712389f, -0.7679449f);
        PixelmonModelRenderer wing_3_L = new PixelmonModelRenderer(this, 55, 54);
        wing_3_L.addBox(-0.7f, 3.533333f, -3.0f, 1, 5, 5);
        wing_3_L.setRotationPoint(4.0f, 3.0f, -0.533333f);
        wing_3_L.setTextureSize(128, 64);
        wing_3_L.mirror = true;
        this.setRotation(wing_3_L, -0.1047198f, 0.4712389f, -0.7330383f);
        PixelmonModelRenderer L_claw_1 = new PixelmonModelRenderer(this, 0, 0);
        L_claw_1.addBox(0.0f, 0.0f, 0.0f, 1, 2, 1);
        L_claw_1.setRotationPoint(6.0f, 7.0f, -3.0f);
        L_claw_1.setTextureSize(128, 64);
        L_claw_1.mirror = true;
        this.setRotation(L_claw_1, 0.8203047f, 0.0f, -0.0872665f);
        PixelmonModelRenderer L_claw_2 = new PixelmonModelRenderer(this, 0, 0);
        L_claw_2.addBox(0.0f, 0.0f, 0.0f, 1, 2, 1);
        L_claw_2.setRotationPoint(6.0f, 7.0f, -3.0f);
        L_claw_2.setTextureSize(128, 64);
        L_claw_2.mirror = true;
        this.setRotation(L_claw_2, -0.6457718f, 0.418879f, 0.0f);
        PixelmonModelRenderer L_claw_3 = new PixelmonModelRenderer(this, 0, 0);
        L_claw_3.addBox(0.0f, 0.0f, 0.0f, 1, 2, 1);
        L_claw_3.setRotationPoint(6.0f, 7.0f, -3.0f);
        L_claw_3.setTextureSize(128, 64);
        L_claw_3.mirror = true;
        this.setRotation(L_claw_3, 0.0f, 0.0f, 0.0f);
        this.LeftWing.addChild(wing_2_L);
        this.LeftWing.addChild(wing_1_L);
        this.LeftWing.addChild(wing_feathers_2_L);
        this.LeftWing.addChild(wing_feathers_1_L);
        this.LeftWing.addChild(wing_3_L);
        this.LeftWing.addChild(L_claw_1);
        this.LeftWing.addChild(L_claw_2);
        this.LeftWing.addChild(L_claw_3);
        this.Body.addChild(this.LeftWing);
        this.RightWing = new PixelmonModelRenderer(this, "Right Wing");
        this.RightWing.setRotationPoint(-3.0f, 0.0f, -1.0f);
        PixelmonModelRenderer wing_1_R = new PixelmonModelRenderer(this, 77, 57);
        wing_1_R.addBox(-1.0f, -0.4666667f, -1.066667f, 1, 5, 2);
        wing_1_R.setTextureSize(128, 64);
        wing_1_R.mirror = true;
        this.setRotation(wing_1_R, 0.0872665f, -0.2443461f, 0.7679449f);
        PixelmonModelRenderer wing_feathers_1_R = new PixelmonModelRenderer(this, 66, 29);
        wing_feathers_1_R.addBox(-0.5f, -0.4666667f, -1.066667f, 1, 7, 6);
        wing_feathers_1_R.setTextureSize(128, 64);
        wing_feathers_1_R.mirror = true;
        this.setRotation(wing_feathers_1_R, 0.0872665f, -0.2443461f, 0.7679449f);
        PixelmonModelRenderer wing_2_R = new PixelmonModelRenderer(this, 68, 54);
        wing_2_R.addBox(-0.3f, -0.4666667f, -1.666667f, 1, 7, 3);
        wing_2_R.setRotationPoint(-4.0f, 3.0f, -0.533333f);
        wing_2_R.setTextureSize(128, 64);
        wing_2_R.mirror = true;
        this.setRotation(wing_2_R, -0.1047198f, -0.4712389f, 0.7679449f);
        PixelmonModelRenderer wing_feathers_2_R = new PixelmonModelRenderer(this, 81, 21);
        wing_feathers_2_R.addBox(0.1666667f, -0.5f, 0.3333333f, 1, 8, 8);
        wing_feathers_2_R.setRotationPoint(-4.0f, 3.0f, -0.533333f);
        wing_feathers_2_R.setTextureSize(128, 64);
        wing_feathers_2_R.mirror = true;
        this.setRotation(wing_feathers_2_R, -0.1047198f, -0.4712389f, 0.7679449f);
        PixelmonModelRenderer wing_3_R = new PixelmonModelRenderer(this, 52, 43);
        wing_3_R.addBox(-0.3f, 3.533333f, -3.0f, 1, 5, 5);
        wing_3_R.setRotationPoint(-4.0f, 3.0f, -0.533333f);
        wing_3_R.setTextureSize(128, 64);
        wing_3_R.mirror = true;
        this.setRotation(wing_3_R, -0.1047198f, -0.4712389f, 0.7330383f);
        PixelmonModelRenderer R_claw_1 = new PixelmonModelRenderer(this, 0, 0);
        R_claw_1.addBox(-1.0f, 0.0f, 0.0f, 1, 2, 1);
        R_claw_1.setRotationPoint(-6.0f, 7.0f, -3.0f);
        R_claw_1.setTextureSize(128, 64);
        R_claw_1.mirror = true;
        this.setRotation(R_claw_1, 0.8203047f, 0.0f, 0.0872665f);
        PixelmonModelRenderer R_claw_3 = new PixelmonModelRenderer(this, 0, 0);
        R_claw_3.addBox(-1.0f, 0.0f, 0.0f, 1, 2, 1);
        R_claw_3.setRotationPoint(-6.0f, 7.0f, -3.0f);
        R_claw_3.setTextureSize(128, 64);
        R_claw_3.mirror = true;
        this.setRotation(R_claw_3, 0.0f, 0.0f, 0.0f);
        PixelmonModelRenderer R_claw_2 = new PixelmonModelRenderer(this, 0, 0);
        R_claw_2.addBox(-1.0f, 0.0f, 0.0f, 1, 2, 1);
        R_claw_2.setRotationPoint(-6.0f, 7.0f, -3.0f);
        R_claw_2.setTextureSize(128, 64);
        R_claw_2.mirror = true;
        this.setRotation(R_claw_2, -0.6457718f, -0.418879f, 0.0f);
        this.RightWing.addChild(wing_1_R);
        this.RightWing.addChild(wing_2_R);
        this.RightWing.addChild(wing_feathers_2_R);
        this.RightWing.addChild(wing_feathers_1_R);
        this.RightWing.addChild(wing_3_R);
        this.RightWing.addChild(R_claw_1);
        this.RightWing.addChild(R_claw_3);
        this.RightWing.addChild(R_claw_2);
        this.Body.addChild(this.RightWing);
        ModuleHead headModule = new ModuleHead(Head);
        this.skeleton = new SkeletonBiped(this.Body, headModule, null, null, null, null, null);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.Body.render(f5);
        if (!(entity instanceof EntityPixelmon)) {
            return;
        }
        EntityPixelmon pixelmon = (EntityPixelmon)entity;
        if (!pixelmon.onGround) {
            this.RightWing.rotateAngleZ = MathHelper.cos(f2 * 1.0f) * (float)Math.PI * 0.25f + 0.75f;
            this.LeftWing.rotateAngleZ = -this.RightWing.rotateAngleZ;
            this.LeftLeg.rotateAngleX = 0.0f;
            this.RightLeg.rotateAngleX = 0.0f;
        } else {
            this.LeftLeg.rotateAngleX = MathHelper.cos(f * 0.8f) * 2.0f * f1;
            this.RightLeg.rotateAngleX = -this.LeftLeg.rotateAngleX;
            this.RightWing.rotateAngleZ = 0.0f;
            this.LeftWing.rotateAngleZ = -this.RightWing.rotateAngleZ;
            this.Tail.rotateAngleX = MathHelper.cos(f * 0.8f) * (float)Math.PI * f1 * 0.2f + 0.1f;
        }
    }

    private void setRotation(PixelmonModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }
}

