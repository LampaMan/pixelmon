/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.pokemon.classic;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

public class ModelCharmander
extends ModelBase {
    ModelRenderer Body;
    ModelRenderer LegLeft;
    ModelRenderer LegRight;
    ModelRenderer Head;
    ModelRenderer Snout;
    ModelRenderer Neck;
    ModelRenderer ArmRight;
    ModelRenderer ArmLeft;
    ModelRenderer TailBase;
    ModelRenderer TailEnd;

    public ModelCharmander() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.Body = new ModelRenderer(this, 48, 0);
        this.Body.addBox(-2.0f, 0.0f, -3.0f, 4, 8, 4);
        this.Body.setRotationPoint(0.0f, 13.0f, 1.0f);
        this.Body.setTextureSize(64, 32);
        this.Body.mirror = true;
        this.setRotation(this.Body, 0.1745329f, 0.0f, 0.0f);
        this.LegLeft = new ModelRenderer(this, 0, 25);
        this.LegLeft.addBox(0.0f, -0.5f, -1.0f, 2, 5, 2);
        this.LegLeft.setRotationPoint(1.0f, 19.5f, 0.0f);
        this.LegLeft.setTextureSize(64, 32);
        this.LegLeft.mirror = true;
        this.setRotation(this.LegLeft, 0.0f, 0.0f, 0.0f);
        this.LegRight = new ModelRenderer(this, 0, 25);
        this.LegRight.addBox(-2.0f, -0.5f, -1.0f, 2, 5, 2);
        this.LegRight.setRotationPoint(-1.0f, 19.5f, 0.0f);
        this.LegRight.setTextureSize(64, 32);
        this.LegRight.mirror = true;
        this.setRotation(this.LegRight, 0.0f, 0.0f, 0.0f);
        this.LegRight.mirror = false;
        this.Head = new ModelRenderer(this, 38, 24);
        this.Head.addBox(-2.0f, -5.0f, -3.0f, 4, 4, 4);
        this.Head.setRotationPoint(0.0f, 13.5f, 1.0f);
        this.Head.setTextureSize(64, 32);
        this.Head.mirror = true;
        this.setRotation(this.Head, 0.0f, 0.0f, 0.0f);
        this.Snout = new ModelRenderer(this, 27, 29);
        this.Snout.addBox(-2.0f, -3.0f, -4.0f, 4, 2, 1);
        this.Snout.setRotationPoint(0.0f, 13.5f, 1.0f);
        this.Snout.setTextureSize(64, 32);
        this.Snout.mirror = true;
        this.setRotation(this.Snout, 0.0f, 0.0f, 0.0f);
        this.Neck = new ModelRenderer(this, 20, 0);
        this.Neck.addBox(-1.5f, -0.5f, -2.0f, 3, 1, 3);
        this.Neck.setRotationPoint(0.0f, 13.0f, 1.0f);
        this.Neck.setTextureSize(64, 32);
        this.Neck.mirror = true;
        this.setRotation(this.Neck, 0.0f, 0.0f, 0.0f);
        this.ArmRight = new ModelRenderer(this, 12, 26);
        this.ArmRight.addBox(-2.0f, -1.0f, -1.0f, 2, 4, 2);
        this.ArmRight.setRotationPoint(-2.0f, 15.0f, 0.0f);
        this.ArmRight.setTextureSize(64, 32);
        this.ArmRight.mirror = true;
        this.setRotation(this.ArmRight, 0.0f, 0.0f, 0.0f);
        this.ArmRight.mirror = false;
        this.ArmLeft = new ModelRenderer(this, 12, 26);
        this.ArmLeft.addBox(0.0f, -1.0f, -1.0f, 2, 4, 2);
        this.ArmLeft.setRotationPoint(2.0f, 15.0f, 0.0f);
        this.ArmLeft.setTextureSize(64, 32);
        this.ArmLeft.mirror = true;
        this.setRotation(this.ArmLeft, 0.0f, 0.0f, 0.0f);
        this.TailBase = new ModelRenderer(this, 10, 0);
        this.TailBase.addBox(-1.0f, 0.0f, -1.0f, 2, 3, 2);
        this.TailBase.setRotationPoint(0.0f, 19.7f, 3.0f);
        this.TailBase.setTextureSize(64, 32);
        this.TailBase.mirror = true;
        this.setRotation(this.TailBase, 1.58825f, 0.0f, 0.0f);
        this.TailEnd = new ModelRenderer(this, 0, 0);
        this.TailEnd.addBox(-1.0f, 0.0f, -1.0f, 2, 3, 2);
        this.TailEnd.setRotationPoint(0.0f, 19.7f, 5.5f);
        this.TailEnd.setTextureSize(64, 32);
        this.TailEnd.mirror = true;
        this.setRotation(this.TailEnd, 2.146755f, 0.0f, 0.0f);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.Body.render(f5);
        this.LegLeft.render(f5);
        this.LegRight.render(f5);
        this.Head.render(f5);
        this.Snout.render(f5);
        this.Neck.render(f5);
        this.ArmRight.render(f5);
        this.ArmLeft.render(f5);
        this.TailBase.render(f5);
        this.TailEnd.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    @Override
    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
        super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.Head.rotateAngleY = f3 / 57.295776f;
        this.Head.rotateAngleX = f4 / 57.295776f;
        this.Snout.rotateAngleY = this.Head.rotateAngleY;
        this.Snout.rotateAngleX = this.Head.rotateAngleX;
        this.ArmRight.rotateAngleX = MathHelper.cos(f * 0.6662f + (float)Math.PI) * 2.0f * f1 * 0.5f;
        this.ArmLeft.rotateAngleX = MathHelper.cos(f * 0.6662f) * 2.0f * f1 * 0.5f;
        this.ArmRight.rotateAngleZ = 0.0f;
        this.ArmLeft.rotateAngleZ = 0.0f;
        this.LegRight.rotateAngleX = MathHelper.cos(f * 0.6662f) * 1.4f * f1;
        this.LegLeft.rotateAngleX = MathHelper.cos(f * 0.6662f + (float)Math.PI) * 1.4f * f1;
        this.LegRight.rotateAngleY = 0.0f;
        this.LegLeft.rotateAngleY = 0.0f;
    }
}

