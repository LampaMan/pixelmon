/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.pokemon;

import com.pixelmongenerations.client.models.ModelCustomWrapper;
import com.pixelmongenerations.client.models.PixelmonModelBase;
import com.pixelmongenerations.client.models.PixelmonModelRenderer;
import com.pixelmongenerations.client.models.animations.SkeletonBase;
import com.pixelmongenerations.client.models.obj.ObjLoader;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;

public class ModelDuosion
extends PixelmonModelBase {
    PixelmonModelRenderer Body;
    PixelmonModelRenderer Cover;

    public ModelDuosion() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.Body = new PixelmonModelRenderer(this, "Body");
        this.Body.setRotationPoint(0.0f, 23.0f, 0.0f);
        this.Body.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/duosion/DuosionBody.obj"))));
        this.Cover = new PixelmonModelRenderer(this, 0, 0);
        this.Cover.setRotationPoint(0.0f, 0.0f, 0.0f);
        this.Cover.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/duosion/DuosionCover.obj"))));
        this.Cover.setTransparent(0.5f);
        this.Body.addChild(this.Cover);
        int degrees = 180;
        float radians = (float)Math.toRadians(degrees);
        this.setRotation(this.Body, radians, 0.0f, 0.0f);
        this.skeleton = new SkeletonBase(this.Body);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.Body.render(f5);
        this.scale = 1.1f;
        this.Body.setRotationPoint(0.0f, -0.5f * MathHelper.cos(f2 * 0.1f) + 23.0f, 0.0f);
    }

    private void setRotation(PixelmonModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    @Override
    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
    }
}

