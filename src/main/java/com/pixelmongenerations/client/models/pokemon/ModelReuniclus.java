/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.pokemon;

import com.pixelmongenerations.client.models.ModelCustomWrapper;
import com.pixelmongenerations.client.models.PixelmonModelBase;
import com.pixelmongenerations.client.models.PixelmonModelRenderer;
import com.pixelmongenerations.client.models.animations.SkeletonBase;
import com.pixelmongenerations.client.models.obj.ObjLoader;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.helpers.animation.IncrementingVariable;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;

public class ModelReuniclus
extends PixelmonModelBase {
    PixelmonModelRenderer Body;
    PixelmonModelRenderer Cover;
    PixelmonModelRenderer LArm;
    PixelmonModelRenderer LCover;
    PixelmonModelRenderer RArm;
    PixelmonModelRenderer RCover;

    public ModelReuniclus() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.Body = new PixelmonModelRenderer(this, "Body");
        this.Body.setRotationPoint(0.0f, 24.0f, 0.0f);
        this.Body.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/reuniclus/ReuniclusBody.obj"))));
        this.Cover = new PixelmonModelRenderer(this, 0, 0);
        this.Cover.setRotationPoint(0.0f, 0.0f, 0.0f);
        this.Cover.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/reuniclus/ReuniclusCover.obj"))));
        this.Cover.setTransparent(0.5f);
        this.LArm = new PixelmonModelRenderer(this, 0, 0);
        this.LArm.setRotationPoint(0.5f, 0.0f, 0.0f);
        this.LArm.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/reuniclus/ReuniclusLArm.obj"))));
        this.LCover = new PixelmonModelRenderer(this, 0, 0);
        this.LCover.setRotationPoint(0.0f, 0.0f, 0.0f);
        this.LCover.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/reuniclus/ReuniclusLCover.obj"))));
        this.LCover.setTransparent(0.5f);
        this.RArm = new PixelmonModelRenderer(this, 0, 0);
        this.RArm.setRotationPoint(-0.5f, 0.0f, 0.0f);
        this.RArm.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/reuniclus/ReuniclusRArm.obj"))));
        this.RCover = new PixelmonModelRenderer(this, 0, 0);
        this.RCover.setRotationPoint(0.0f, 0.0f, 0.0f);
        this.RCover.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/reuniclus/ReuniclusRCover.obj"))));
        this.RCover.setTransparent(0.5f);
        this.Body.addChild(this.Cover);
        this.Body.addChild(this.LArm);
        this.Body.addChild(this.RArm);
        this.LArm.addChild(this.LCover);
        this.RArm.addChild(this.RCover);
        int degrees = 180;
        float radians = (float)Math.toRadians(degrees);
        this.setRotation(this.Body, radians, 0.0f, 0.0f);
        this.skeleton = new SkeletonBase(this.Body);
        this.registerAnimationCounters();
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.Body.render(f5);
        this.scale = 1.4f;
        if (!(entity instanceof EntityPixelmon)) {
            return;
        }
        EntityPixelmon pixelmon = (EntityPixelmon)entity;
        this.Body.setRotationPoint(0.0f, -1.0f * MathHelper.cos(f2 * 0.15f) + 24.0f, 0.0f);
        IncrementingVariable animationCounterLeft = this.getCounter(0, pixelmon);
        IncrementingVariable animationCounterRight = this.getCounter(1, pixelmon);
        if (animationCounterLeft == null) {
            this.setInt(0, 0, pixelmon);
            this.setInt(1, 0, pixelmon);
            animationCounterLeft = this.setCounter(0, 0, 3, pixelmon);
            animationCounterRight = this.setCounter(1, 0, 2, pixelmon);
        }
        animationCounterLeft.increment = 1.0f;
        animationCounterRight.increment = 1.0f;
        if (animationCounterLeft.value == 0.0f) {
            this.setInt(0, (int)Math.round(Math.random()), pixelmon);
            animationCounterLeft.limit = 10 + 30 * (int)Math.round(Math.random() * 1.0 + 1.0);
        }
        if (animationCounterRight.value == 0.0f) {
            animationCounterRight.limit = 10 + 30 * (int)Math.round(Math.random() * 1.0 + 1.0);
        }
        if (this.getInt(0, pixelmon) == 0) {
            if (animationCounterLeft.value < 10.0f) {
                this.LArm.rotateAngleZ = (float)Math.toRadians(20.0) - (float)Math.toRadians(10.0) * MathHelper.cos(animationCounterLeft.value / 10.0f * 2.0f * (float)Math.PI);
                this.RArm.rotateAngleZ = -((float)Math.toRadians(20.0)) + (float)Math.toRadians(10.0) * MathHelper.cos(animationCounterLeft.value / 10.0f * 2.0f * (float)Math.PI);
            }
        } else {
            if (animationCounterLeft.value < 10.0f) {
                this.LArm.rotateAngleZ = (float)Math.toRadians(20.0) - (float)Math.toRadians(10.0) * MathHelper.cos(animationCounterLeft.value / 10.0f * 2.0f * (float)Math.PI);
            }
            if (animationCounterRight.value < 10.0f) {
                this.RArm.rotateAngleZ = -((float)Math.toRadians(20.0)) + (float)Math.toRadians(10.0) * MathHelper.cos(animationCounterRight.value / 10.0f * 2.0f * (float)Math.PI);
            }
        }
    }

    private void setRotation(PixelmonModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    @Override
    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
    }
}

