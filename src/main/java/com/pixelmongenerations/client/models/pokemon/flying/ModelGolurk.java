/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.pokemon.flying;

import com.pixelmongenerations.client.models.ModelCustomWrapper;
import com.pixelmongenerations.client.models.PixelmonModelBase;
import com.pixelmongenerations.client.models.PixelmonModelRenderer;
import com.pixelmongenerations.client.models.animations.SkeletonBase;
import com.pixelmongenerations.client.models.obj.ObjLoader;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;

public class ModelGolurk
extends PixelmonModelBase {
    PixelmonModelRenderer Body;
    PixelmonModelRenderer MFlame;
    PixelmonModelRenderer LArm;
    PixelmonModelRenderer RArm;
    PixelmonModelRenderer LFlame;
    PixelmonModelRenderer RFlame;

    public ModelGolurk() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.Body = new PixelmonModelRenderer(this, "Body");
        this.Body.setRotationPoint(0.0f, 23.8f, 0.0f);
        this.Body.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/golurk/flying/GolurkBody.obj"))));
        this.MFlame = new PixelmonModelRenderer(this, 0, 0);
        this.MFlame.setRotationPoint(0.0f, 10.15f, 4.74f);
        this.MFlame.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/golurk/flying/GolurkMFlame.obj"))));
        this.LArm = new PixelmonModelRenderer(this, 0, 0);
        this.LArm.setRotationPoint(2.7f, 15.4f, 7.5f);
        this.LArm.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/golurk/flying/GolurkLArm.obj"))));
        this.RArm = new PixelmonModelRenderer(this, 0, 0);
        this.RArm.setRotationPoint(-2.7f, 15.4f, 7.5f);
        this.RArm.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/golurk/flying/GolurkRArm.obj"))));
        this.LFlame = new PixelmonModelRenderer(this, 0, 0);
        this.LFlame.setRotationPoint(5.75f, -2.32f, -3.02f);
        this.LFlame.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/golurk/flying/GolurkLFlame.obj"))));
        this.RFlame = new PixelmonModelRenderer(this, 0, 0);
        this.RFlame.setRotationPoint(-5.75f, -2.32f, -3.02f);
        this.RFlame.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/golurk/flying/GolurkRFlame.obj"))));
        this.Body.addChild(this.LArm);
        this.Body.addChild(this.RArm);
        this.LArm.addChild(this.LFlame);
        this.RArm.addChild(this.RFlame);
        this.Body.addChild(this.MFlame);
        int degrees = 180;
        float radians = (float)Math.toRadians(degrees);
        this.setRotation(this.Body, radians, 0.0f, 0.0f);
        this.skeleton = new SkeletonBase(this.Body);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5);
        this.scale = 2.2f;
        this.Body.render(f5);
        this.Body.setRotationPoint(0.0f, -0.5f * MathHelper.cos(f2 * 0.1f) + 23.8f, 0.0f);
        this.LArm.rotateAngleX = MathHelper.cos((float)Math.toRadians(35.0)) * 0.2f * MathHelper.cos(f2 * 0.05f);
        this.RArm.rotateAngleX = MathHelper.cos((float)Math.toRadians(35.0)) * 0.2f * MathHelper.cos(f2 * 0.05f);
    }

    private void setRotation(PixelmonModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5) {
    }
}

