/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.pokemon.classic;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

public class ModelChansey
extends ModelBase {
    ModelRenderer face;
    ModelRenderer ear1;
    ModelRenderer ear5;
    ModelRenderer ear3;
    ModelRenderer ear6;
    ModelRenderer ear4;
    ModelRenderer ear2;
    ModelRenderer headtop;
    ModelRenderer head;
    ModelRenderer Egg;
    ModelRenderer eggPouch;
    ModelRenderer LeftLeg;
    ModelRenderer RightLeg;
    ModelRenderer Shape1;
    ModelRenderer Shape13;
    ModelRenderer Shape14;
    ModelRenderer Shape16;
    ModelRenderer Shape23;
    ModelRenderer Shape20;
    ModelRenderer Shape19;
    ModelRenderer Shape15;
    ModelRenderer LeftArm;
    ModelRenderer RightArm;
    ModelRenderer tailTip;
    ModelRenderer tailBase;

    public ModelChansey() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.face = new ModelRenderer(this, 28, 27);
        this.face.addBox(0.0f, 0.0f, 0.0f, 5, 3, 1);
        this.face.setRotationPoint(-2.0f, 14.5f, 0.0f);
        this.face.setTextureSize(64, 32);
        this.face.mirror = true;
        this.setRotation(this.face, 0.0f, 0.0f, 0.0f);
        this.ear1 = new ModelRenderer(this, 0, 20);
        this.ear1.addBox(0.0f, 0.0f, 0.0f, 1, 4, 1);
        this.ear1.setRotationPoint(-2.0f, 14.0f, 1.5f);
        this.ear1.setTextureSize(64, 32);
        this.ear1.mirror = true;
        this.setRotation(this.ear1, -0.4461433f, 0.0f, 0.8922867f);
        this.ear5 = new ModelRenderer(this, 0, 20);
        this.ear5.addBox(0.0f, 0.0f, 0.0f, 1, 4, 1);
        this.ear5.setRotationPoint(-2.5f, 14.5f, 5.0f);
        this.ear5.setTextureSize(64, 32);
        this.ear5.mirror = true;
        this.setRotation(this.ear5, 0.5576792f, 0.0f, 1.07818f);
        this.ear3 = new ModelRenderer(this, 0, 20);
        this.ear3.addBox(0.0f, 0.0f, 0.0f, 1, 4, 1);
        this.ear3.setRotationPoint(-2.0f, 14.0f, 3.0f);
        this.ear3.setTextureSize(64, 32);
        this.ear3.mirror = true;
        this.setRotation(this.ear3, 0.0f, 0.0f, 1.07818f);
        this.ear6 = new ModelRenderer(this, 5, 22);
        this.ear6.addBox(0.0f, 0.0f, 0.0f, 4, 1, 1);
        this.ear6.setRotationPoint(3.0f, 14.0f, 5.0f);
        this.ear6.setTextureSize(64, 32);
        this.ear6.mirror = true;
        this.setRotation(this.ear6, 0.0f, -0.4833219f, 0.4833219f);
        this.ear4 = new ModelRenderer(this, 5, 22);
        this.ear4.addBox(0.0f, 0.0f, 0.0f, 4, 1, 1);
        this.ear4.setRotationPoint(3.0f, 14.0f, 3.0f);
        this.ear4.setTextureSize(64, 32);
        this.ear4.mirror = true;
        this.setRotation(this.ear4, 0.0f, 0.0f, 0.4461433f);
        this.ear2 = new ModelRenderer(this, 5, 22);
        this.ear2.addBox(0.0f, 0.0f, 0.0f, 4, 1, 1);
        this.ear2.setRotationPoint(3.0f, 14.0f, 1.0f);
        this.ear2.setTextureSize(64, 32);
        this.ear2.mirror = true;
        this.setRotation(this.ear2, 0.0f, 0.5205006f, 0.5948578f);
        this.headtop = new ModelRenderer(this, 12, 0);
        this.headtop.addBox(0.0f, 0.0f, 0.0f, 4, 1, 5);
        this.headtop.setRotationPoint(-1.5f, 13.5f, 1.0f);
        this.headtop.setTextureSize(64, 32);
        this.headtop.mirror = true;
        this.setRotation(this.headtop, 0.0f, 0.0f, 0.0f);
        this.head = new ModelRenderer(this, 0, 0);
        this.head.addBox(0.0f, 0.0f, 0.0f, 6, 3, 6);
        this.head.setRotationPoint(-2.5f, 14.0f, 0.5f);
        this.head.setTextureSize(64, 32);
        this.head.mirror = true;
        this.setRotation(this.head, 0.0f, 0.0f, 0.0f);
        this.Egg = new ModelRenderer(this, 36, 0);
        this.Egg.addBox(0.0f, 0.0f, 0.0f, 3, 4, 1);
        this.Egg.setRotationPoint(-1.0f, 18.0f, -1.0f);
        this.Egg.setTextureSize(64, 32);
        this.Egg.mirror = true;
        this.setRotation(this.Egg, 0.0f, 0.0f, 0.0f);
        this.eggPouch = new ModelRenderer(this, 34, 5);
        this.eggPouch.addBox(0.0f, 0.0f, 0.0f, 4, 2, 2);
        this.eggPouch.setRotationPoint(-1.5f, 21.0f, -1.3f);
        this.eggPouch.setTextureSize(64, 32);
        this.eggPouch.mirror = true;
        this.setRotation(this.eggPouch, 0.0f, 0.0f, 0.0f);
        this.LeftLeg = new ModelRenderer(this, 25, 14);
        this.LeftLeg.addBox(0.0f, 0.0f, 0.0f, 1, 1, 7);
        this.LeftLeg.setRotationPoint(3.5f, 23.0f, -1.5f);
        this.LeftLeg.setTextureSize(64, 32);
        this.LeftLeg.mirror = true;
        this.setRotation(this.LeftLeg, 0.0f, -0.2602503f, 0.0f);
        this.RightLeg = new ModelRenderer(this, 25, 14);
        this.RightLeg.addBox(0.0f, 0.0f, 0.0f, 1, 1, 7);
        this.RightLeg.setRotationPoint(-3.5f, 23.0f, -1.3f);
        this.RightLeg.setTextureSize(64, 32);
        this.RightLeg.mirror = true;
        this.setRotation(this.RightLeg, 0.0f, 0.2230717f, 0.0f);
        this.Shape1 = new ModelRenderer(this, 0, 0);
        this.Shape1.addBox(0.0f, 0.0f, 0.0f, 8, 7, 7);
        this.Shape1.setRotationPoint(-3.5f, 16.5f, 0.0f);
        this.Shape1.setTextureSize(64, 32);
        this.Shape1.mirror = true;
        this.setRotation(this.Shape1, 0.0f, 0.0f, 0.0f);
        this.Shape13 = new ModelRenderer(this, 48, 17);
        this.Shape13.addBox(0.0f, 0.0f, 0.0f, 1, 6, 6);
        this.Shape13.setRotationPoint(-4.0f, 17.0f, 0.5f);
        this.Shape13.setTextureSize(64, 32);
        this.Shape13.mirror = true;
        this.setRotation(this.Shape13, 0.0f, 0.0f, 0.0f);
        this.Shape14 = new ModelRenderer(this, 50, 0);
        this.Shape14.addBox(0.0f, 0.0f, 0.0f, 1, 5, 5);
        this.Shape14.setRotationPoint(-4.5f, 18.0f, 1.0f);
        this.Shape14.setTextureSize(64, 32);
        this.Shape14.mirror = true;
        this.setRotation(this.Shape14, 0.0f, 0.0f, 0.0f);
        this.Shape16 = new ModelRenderer(this, 48, 17);
        this.Shape16.addBox(0.0f, 0.0f, 0.0f, 1, 6, 6);
        this.Shape16.setRotationPoint(4.0f, 17.0f, 0.5f);
        this.Shape16.setTextureSize(64, 32);
        this.Shape16.mirror = true;
        this.setRotation(this.Shape16, 0.0f, 0.0f, 0.0f);
        this.Shape23 = new ModelRenderer(this, 0, 0);
        this.Shape23.addBox(0.0f, 0.0f, 0.0f, 7, 6, 1);
        this.Shape23.setRotationPoint(-3.0f, 17.0f, -0.5f);
        this.Shape23.setTextureSize(64, 32);
        this.Shape23.mirror = true;
        this.setRotation(this.Shape23, 0.0f, 0.0f, 0.0f);
        this.Shape20 = new ModelRenderer(this, 47, 11);
        this.Shape20.addBox(0.0f, 0.0f, 0.0f, 5, 5, 1);
        this.Shape20.setRotationPoint(-2.0f, 18.0f, 7.0f);
        this.Shape20.setTextureSize(64, 32);
        this.Shape20.mirror = true;
        this.setRotation(this.Shape20, 0.0f, 0.0f, 0.0f);
        this.Shape19 = new ModelRenderer(this, 11, 14);
        this.Shape19.addBox(0.0f, 0.0f, 0.0f, 6, 6, 1);
        this.Shape19.setRotationPoint(-2.5f, 17.0f, 6.5f);
        this.Shape19.setTextureSize(64, 32);
        this.Shape19.mirror = true;
        this.setRotation(this.Shape19, 0.0f, 0.0f, 0.0f);
        this.Shape15 = new ModelRenderer(this, 50, 0);
        this.Shape15.addBox(0.0f, 0.0f, 0.0f, 1, 5, 5);
        this.Shape15.setRotationPoint(4.5f, 18.0f, 1.0f);
        this.Shape15.setTextureSize(64, 32);
        this.Shape15.mirror = true;
        this.setRotation(this.Shape15, 0.0f, 0.0f, 0.0f);
        this.LeftArm = new ModelRenderer(this, 36, 10);
        this.LeftArm.addBox(0.0f, 0.0f, 0.0f, 1, 3, 1);
        this.LeftArm.setRotationPoint(2.8f, 18.0f, 0.2f);
        this.LeftArm.setTextureSize(64, 32);
        this.LeftArm.mirror = true;
        this.setRotation(this.LeftArm, -0.669215f, 0.5948578f, 0.0174533f);
        this.RightArm = new ModelRenderer(this, 36, 10);
        this.RightArm.addBox(0.0f, 0.0f, 0.0f, 1, 3, 1);
        this.RightArm.setRotationPoint(-1.9f, 18.5f, 0.7f);
        this.RightArm.setTextureSize(64, 32);
        this.RightArm.mirror = true;
        this.setRotation(this.RightArm, 0.5948578f, 2.918521f, 0.2602503f);
        this.tailTip = new ModelRenderer(this, 0, 0);
        this.tailTip.addBox(0.0f, 0.0f, 0.0f, 1, 1, 1);
        this.tailTip.setRotationPoint(1.7f, 20.5f, 9.8f);
        this.tailTip.setTextureSize(64, 32);
        this.tailTip.mirror = true;
        this.setRotation(this.tailTip, 2.120575f, 0.8203047f, 0.0f);
        this.tailBase = new ModelRenderer(this, 0, 0);
        this.tailBase.addBox(0.0f, 0.0f, 0.0f, 2, 4, 2);
        this.tailBase.setRotationPoint(-1.0f, 23.0f, 8.0f);
        this.tailBase.setTextureSize(64, 32);
        this.tailBase.mirror = true;
        this.setRotation(this.tailBase, 2.120575f, 0.8203047f, 0.0f);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.face.render(f5);
        this.ear1.render(f5);
        this.ear5.render(f5);
        this.ear3.render(f5);
        this.ear6.render(f5);
        this.ear4.render(f5);
        this.ear2.render(f5);
        this.headtop.render(f5);
        this.head.render(f5);
        this.Egg.render(f5);
        this.eggPouch.render(f5);
        this.LeftLeg.render(f5);
        this.RightLeg.render(f5);
        this.Shape1.render(f5);
        this.Shape13.render(f5);
        this.Shape14.render(f5);
        this.Shape16.render(f5);
        this.Shape23.render(f5);
        this.Shape20.render(f5);
        this.Shape19.render(f5);
        this.Shape15.render(f5);
        this.LeftArm.render(f5);
        this.RightArm.render(f5);
        this.tailTip.render(f5);
        this.tailBase.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    @Override
    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
        super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.RightLeg.rotateAngleX = MathHelper.cos(f * 0.9f) * 1.0f * f1;
        this.LeftLeg.rotateAngleX = MathHelper.cos(f * 0.9f + (float)Math.PI) * 1.0f * f1;
    }
}

