/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models;

import com.pixelmongenerations.client.models.PixelmonModelHolder;
import com.pixelmongenerations.client.models.PixelmonModelSmd;
import com.pixelmongenerations.client.models.PixelmonSmdFactory;
import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import com.pixelmongenerations.core.enums.EnumPokemonModel;
import com.pixelmongenerations.core.proxy.ClientProxy;

public class DynamicModelHolder
extends PixelmonModelHolder<PixelmonModelSmd> {
    private String key;

    public DynamicModelHolder(String key) {
        super(new PixelmonSmdFactory(EnumPokemonModel.PikachuNormal));
        this.key = key;
    }

    @Override
    public PixelmonModelSmd getModel() {
        return new PixelmonSmdFactory.Impl((ValveStudioModel)ClientProxy.VALVE_MODEL_STORE.getObject(this.key).getModel());
    }

    @Override
    protected PixelmonModelSmd loadModel() {
        return this.getModel();
    }
}

