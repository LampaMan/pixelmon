/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models;

import com.pixelmongenerations.client.models.BikeModelBase;
import com.pixelmongenerations.client.models.smd.SmdAnimation;
import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import com.pixelmongenerations.common.entity.bikes.EntityBike;
import com.pixelmongenerations.common.entity.pixelmon.helpers.animation.IncrementingVariable;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;

public abstract class BikeModelSmd
extends BikeModelBase {
    public float animationIncrement = 1.0f;
    public ValveStudioModel theModel;
    @Deprecated
    public float scale = 0.0f;

    public BikeModelSmd() {
        this.registerAnimationCounters();
    }

    @Override
    public void render(Entity var1, float f, float f1, float f2, float f3, float f4, float f5) {
    }

    @Override
    public float getScale() {
        return this.theModel.getScale();
    }

    public void setAnimationIncrement(float f) {
        this.animationIncrement = f;
    }

    public void setupForRender(EntityBike bike) {
        this.setAnimation(bike.getCurrentAnimation().toString());
        if (this.getCounter(-1, bike) == null) {
            this.setCounter(-1, 2.14748365E9f, this.animationIncrement, bike);
        }
        this.tickAnimation(bike);
    }

    protected void tickAnimation(EntityBike bike) {
        SmdAnimation theAnim = this.theModel.currentAnimation;
        int frame = (int)Math.floor(this.getCounter((int)-1, (EntityBike)bike).value % (float)theAnim.totalFrames);
        theAnim.setCurrentFrame(frame);
        bike.world.profiler.startSection("bike_animate");
        this.theModel.animate();
        bike.world.profiler.endSection();
    }

    protected void setAnimation(String string) {
        this.theModel.setAnimation(string);
    }

    public static boolean isMinecraftPaused() {
        Minecraft m = Minecraft.getMinecraft();
        return m.isSingleplayer() && m.currentScreen != null && m.currentScreen.doesGuiPauseGame() && !m.getIntegratedServer().getPublic();
    }

    @Override
    protected void setInt(int id, int value, EntityBike pixelmon) {
        pixelmon.getAnimationVariables().setInt(id, value);
    }

    @Override
    protected int getInt(int id, EntityBike pixelmon) {
        return pixelmon.getAnimationVariables().getInt(id);
    }

    protected IncrementingVariable setCounter(int id, float limit, float increment, EntityBike pixelmon) {
        pixelmon.getAnimationVariables().setCounter(id, limit, increment);
        return this.getCounter(id, pixelmon);
    }

    @Override
    protected IncrementingVariable getCounter(int id, EntityBike pixelmon) {
        return pixelmon.getAnimationVariables().getCounter(id);
    }

    @Override
    protected void registerAnimationCounters() {
    }
}

