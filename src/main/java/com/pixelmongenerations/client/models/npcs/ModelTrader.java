/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.npcs;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

public class ModelTrader
extends ModelBase {
    ModelRenderer Body;
    ModelRenderer Head;
    ModelRenderer LeftArm;
    ModelRenderer RightArm;
    ModelRenderer LeftLeg;
    ModelRenderer RightLeg;

    public ModelTrader() {
        this.textureWidth = 64;
        this.textureHeight = 64;
        this.Body = new ModelRenderer(this, "Body");
        this.Body.setRotationPoint(0.0f, -1.0f, 0.0f);
        ModelRenderer body = new ModelRenderer(this, 18, 15);
        body.addBox(-4.0f, 0.0f, -2.5f, 8, 11, 5);
        body.setTextureSize(64, 64);
        body.mirror = true;
        this.setRotation(body, 0.0f, 0.0f, 0.0f);
        this.Body.addChild(body);
        this.Head = new ModelRenderer(this, "Head");
        this.Head.setRotationPoint(0.0f, -0.5f, 0.0f);
        ModelRenderer head = new ModelRenderer(this, 0, 0);
        head.addBox(-3.5f, -8.0f, -3.5f, 7, 8, 7);
        head.setTextureSize(64, 64);
        head.mirror = true;
        this.setRotation(head, 0.0f, 0.0f, 0.0f);
        this.Head.addChild(head);
        ModelRenderer main1 = new ModelRenderer(this, 0, 51);
        main1.addBox(-4.0f, -1.0f, -6.0f, 8, 1, 12);
        main1.setRotationPoint(0.0f, -5.0f, 0.0f);
        main1.setTextureSize(64, 64);
        main1.mirror = true;
        this.setRotation(main1, 0.0f, 0.0f, 0.0f);
        this.Head.addChild(main1);
        ModelRenderer main2 = new ModelRenderer(this, 0, 53);
        main2.addBox(-5.0f, -1.0f, -5.0f, 10, 1, 10);
        main2.setRotationPoint(0.0f, -5.0f, 0.0f);
        main2.setTextureSize(64, 64);
        main2.mirror = true;
        this.setRotation(main2, 0.0f, 0.0f, 0.0f);
        this.Head.addChild(main2);
        ModelRenderer sideL = new ModelRenderer(this, 0, 55);
        sideL.addBox(0.0f, -1.0f, -4.0f, 2, 1, 8);
        sideL.setRotationPoint(5.0f, -5.5f, 0.0f);
        sideL.setTextureSize(64, 64);
        sideL.mirror = true;
        this.setRotation(sideL, 0.0f, 0.0f, -0.6981317f);
        this.Head.addChild(sideL);
        ModelRenderer SideR = new ModelRenderer(this, 0, 55);
        SideR.addBox(-2.0f, -1.0f, -4.0f, 2, 1, 8);
        SideR.setRotationPoint(-5.0f, -5.5f, 0.0f);
        SideR.setTextureSize(64, 64);
        SideR.mirror = true;
        this.setRotation(SideR, 0.0f, 0.0f, 0.6981317f);
        this.Head.addChild(SideR);
        ModelRenderer neck = new ModelRenderer(this, 28, 0);
        neck.addBox(-2.5f, -1.0f, -2.5f, 5, 2, 5);
        neck.setTextureSize(64, 64);
        neck.mirror = true;
        this.setRotation(neck, 0.0f, 0.0f, 0.0f);
        this.Head.addChild(neck);
        this.Body.addChild(this.Head);
        this.RightArm = new ModelRenderer(this, "Right Arm");
        this.RightArm.setRotationPoint(-7.0f, 1.0f, 0.0f);
        ModelRenderer arm_R = new ModelRenderer(this, 44, 15);
        arm_R.addBox(-3.0f, -1.2f, -2.0f, 3, 13, 4);
        arm_R.setTextureSize(64, 64);
        arm_R.mirror = true;
        this.setRotation(arm_R, 0.0f, (float)Math.PI, 0.074351f);
        this.RightArm.addChild(arm_R);
        this.Body.addChild(this.RightArm);
        this.LeftArm = new ModelRenderer(this, "Left Arm");
        this.LeftArm.setRotationPoint(4.0f, 1.0f, 0.0f);
        ModelRenderer arm_L = new ModelRenderer(this, 44, 15);
        arm_L.addBox(0.0f, -1.0f, -2.0f, 3, 13, 4);
        arm_L.setTextureSize(64, 64);
        arm_L.mirror = true;
        this.setRotation(arm_L, 0.0f, 0.0f, -0.074351f);
        this.LeftArm.addChild(arm_L);
        this.Body.addChild(this.LeftArm);
        this.LeftLeg = new ModelRenderer(this, "Left Leg");
        this.LeftLeg.setRotationPoint(2.0f, 11.0f, 0.0f);
        ModelRenderer Leg_L = new ModelRenderer(this, 0, 15);
        Leg_L.addBox(-2.0f, 0.0f, -2.5f, 4, 12, 5);
        Leg_L.setTextureSize(64, 64);
        Leg_L.mirror = true;
        this.setRotation(Leg_L, 0.0f, 0.0f, -0.0174533f);
        this.LeftLeg.addChild(Leg_L);
        ModelRenderer feet_L = new ModelRenderer(this, 20, 32);
        feet_L.addBox(-2.0f, 11.0f, -4.5f, 4, 3, 7);
        feet_L.setTextureSize(64, 64);
        feet_L.mirror = true;
        this.setRotation(feet_L, 0.0f, 0.0f, -0.0174533f);
        this.LeftLeg.addChild(feet_L);
        this.Body.addChild(this.LeftLeg);
        this.RightLeg = new ModelRenderer(this, "Right Leg");
        this.RightLeg.setRotationPoint(-2.0f, 11.0f, 0.0f);
        ModelRenderer leg_R = new ModelRenderer(this, 0, 15);
        leg_R.addBox(-2.0f, 0.0f, -2.5f, 4, 12, 5);
        leg_R.setTextureSize(64, 64);
        leg_R.mirror = true;
        this.setRotation(leg_R, 0.0f, 0.0f, 0.0174533f);
        this.RightLeg.addChild(leg_R);
        ModelRenderer feet_R = new ModelRenderer(this, 20, 32);
        feet_R.addBox(-2.0f, 11.0f, -4.5f, 4, 3, 7);
        feet_R.setTextureSize(64, 64);
        feet_R.mirror = true;
        this.setRotation(feet_R, 0.0f, 0.0f, 0.0174533f);
        this.RightLeg.addChild(feet_R);
        this.Body.addChild(this.RightLeg);
        ModelRenderer belt_L = new ModelRenderer(this, 24, 47);
        belt_L.addBox(0.0f, -4.0f, 0.0f, 1, 4, 0);
        belt_L.setRotationPoint(1.0f, 3.55f, -3.0f);
        belt_L.setTextureSize(64, 64);
        belt_L.mirror = true;
        this.setRotation(belt_L, -0.1396263f, 0.0f, 0.418879f);
        this.Body.addChild(belt_L);
        ModelRenderer belt_R = new ModelRenderer(this, 24, 47);
        belt_R.addBox(-1.0f, -4.0f, 0.0f, 1, 4, 0);
        belt_R.setRotationPoint(-1.0f, 3.55f, -3.0f);
        belt_R.setTextureSize(64, 64);
        belt_R.mirror = true;
        this.setRotation(belt_R, -0.1396263f, 0.0f, -0.418879f);
        this.Body.addChild(belt_R);
        ModelRenderer adjustor = new ModelRenderer(this, 40, 50);
        adjustor.addBox(-0.5f, -0.5f, -1.0f, 1, 3, 1);
        adjustor.setRotationPoint(0.0f, 4.0f, -3.5f);
        adjustor.setTextureSize(64, 64);
        adjustor.mirror = true;
        this.setRotation(adjustor, 0.0f, 0.0f, 0.0f);
        this.Body.addChild(adjustor);
        ModelRenderer holder = new ModelRenderer(this, 40, 54);
        holder.addBox(-1.0f, 0.0f, -1.5f, 2, 2, 2);
        holder.setRotationPoint(0.0f, 4.0f, -3.5f);
        holder.setTextureSize(64, 64);
        holder.mirror = true;
        this.setRotation(holder, 0.0f, 0.0f, 0.0f);
        this.Body.addChild(holder);
        ModelRenderer look_L = new ModelRenderer(this, 44, 51);
        look_L.addBox(0.0f, -1.5f, -0.5f, 1, 2, 1);
        look_L.setRotationPoint(0.9f, 3.5f, -4.0f);
        look_L.setTextureSize(64, 64);
        look_L.mirror = true;
        this.setRotation(look_L, 0.0f, -0.2094395f, 0.0f);
        this.Body.addChild(look_L);
        ModelRenderer spy_L = new ModelRenderer(this, 40, 58);
        spy_L.addBox(-0.2f, 0.0f, -1.0f, 2, 4, 2);
        spy_L.setRotationPoint(0.9f, 3.5f, -4.0f);
        spy_L.setTextureSize(64, 64);
        spy_L.mirror = true;
        this.setRotation(spy_L, 0.0f, -0.2094395f, 0.0f);
        this.Body.addChild(spy_L);
        ModelRenderer look_R = new ModelRenderer(this, 44, 51);
        look_R.addBox(-1.0f, -1.5f, -0.5f, 1, 2, 1);
        look_R.setRotationPoint(-0.9f, 3.5f, -4.0f);
        look_R.setTextureSize(64, 64);
        look_R.mirror = true;
        this.setRotation(look_R, 0.0f, 0.2094395f, 0.0f);
        this.Body.addChild(look_R);
        ModelRenderer spy_R = new ModelRenderer(this, 40, 58);
        spy_R.addBox(-1.8f, 0.0f, -1.0f, 2, 4, 2);
        spy_R.setRotationPoint(-0.9f, 3.5f, -4.0f);
        spy_R.setTextureSize(64, 64);
        spy_R.mirror = true;
        this.setRotation(spy_R, 0.0f, 0.2094395f, 0.0f);
        this.Body.addChild(spy_R);
        ModelRenderer backpack = new ModelRenderer(this, 0, 34);
        backpack.addBox(-3.5f, 0.0f, 0.0f, 7, 8, 3);
        backpack.setRotationPoint(0.0f, 1.0f, 2.5f);
        backpack.setTextureSize(64, 64);
        backpack.mirror = true;
        this.setRotation(backpack, 0.0f, 0.0f, 0.0f);
        this.Body.addChild(backpack);
        ModelRenderer sleepbag = new ModelRenderer(this, 0, 45);
        sleepbag.addBox(-4.5f, -1.5f, -1.5f, 9, 3, 3);
        sleepbag.setRotationPoint(0.0f, 0.5f, 4.5f);
        sleepbag.setTextureSize(64, 64);
        sleepbag.mirror = true;
        this.setRotation(sleepbag, 0.7853982f, 0.0f, 0.0f);
        this.Body.addChild(sleepbag);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5);
        this.Body.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5) {
        this.Head.rotateAngleY = f3 / 57.295776f;
        this.Head.rotateAngleX = f4 / 57.295776f;
        this.RightLeg.rotateAngleX = MathHelper.cos(f * 0.6662f) * 1.4f * f1;
        this.LeftLeg.rotateAngleX = MathHelper.cos(f * 0.6662f + (float)Math.PI) * 1.4f * f1;
        this.RightLeg.rotateAngleY = 0.0f;
        this.LeftLeg.rotateAngleY = 0.0f;
        this.RightArm.rotateAngleX = MathHelper.cos(f * 0.6662f + (float)Math.PI) * 0.5f * f1;
        this.LeftArm.rotateAngleX = MathHelper.cos(f * 0.6662f) * 0.5f * f1;
    }
}

