/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.models;

import com.pixelmongenerations.client.models.ModelCustomWrapper;
import java.util.ArrayList;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GLAllocation;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.opengl.GL11;

public class PixelmonModelRenderer
extends ModelRenderer {
    private int displayList;
    private boolean compiled = false;
    public ArrayList<ModelCustomWrapper> objs = new ArrayList();
    private boolean isTransparent = false;
    private float transparency;

    public PixelmonModelRenderer(ModelBase par1ModelBase, String par2Str) {
        super(par1ModelBase, par2Str);
    }

    public PixelmonModelRenderer(ModelBase par1ModelBase) {
        super(par1ModelBase);
    }

    public PixelmonModelRenderer(ModelBase par1ModelBase, int par2, int par3) {
        super(par1ModelBase, par2, par3);
    }

    public void addCustomModel(ModelCustomWrapper model) {
        this.objs.add(model);
    }

    public void setTransparent(float transparency) {
        if (transparency > 0.0f) {
            this.isTransparent = true;
            this.transparency = transparency;
        }
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public void render(float par1) {
        if (!this.isHidden && this.showModel) {
            if (!this.compiled) {
                this.compileDisplayList(par1);
            }
            GlStateManager.translate(this.offsetX, this.offsetY, this.offsetZ);
            if (this.rotateAngleX == 0.0f && this.rotateAngleY == 0.0f && this.rotateAngleZ == 0.0f) {
                if (this.rotationPointX == 0.0f && this.rotationPointY == 0.0f && this.rotationPointZ == 0.0f) {
                    GlStateManager.callList(this.displayList);
                    this.renderCustomModels(par1);
                    if (this.childModels != null) {
                        for (int var2 = 0; var2 < this.childModels.size(); ++var2) {
                            ((ModelRenderer)this.childModels.get(var2)).render(par1);
                        }
                    }
                } else {
                    GlStateManager.translate(this.rotationPointX * par1, this.rotationPointY * par1, this.rotationPointZ * par1);
                    GlStateManager.callList(this.displayList);
                    this.renderCustomModels(par1);
                    if (this.childModels != null) {
                        for (int var2 = 0; var2 < this.childModels.size(); ++var2) {
                            ((ModelRenderer)this.childModels.get(var2)).render(par1);
                        }
                    }
                    GlStateManager.translate(-this.rotationPointX * par1, -this.rotationPointY * par1, -this.rotationPointZ * par1);
                }
            } else {
                GlStateManager.pushMatrix();
                GlStateManager.translate(this.rotationPointX * par1, this.rotationPointY * par1, this.rotationPointZ * par1);
                if (this.rotateAngleY != 0.0f) {
                    GlStateManager.rotate(this.rotateAngleY * 57.295776f, 0.0f, 1.0f, 0.0f);
                }
                if (this.rotateAngleZ != 0.0f) {
                    GlStateManager.rotate(this.rotateAngleZ * 57.295776f, 0.0f, 0.0f, 1.0f);
                }
                if (this.rotateAngleX != 0.0f) {
                    GlStateManager.rotate(this.rotateAngleX * 57.295776f, 1.0f, 0.0f, 0.0f);
                }
                GlStateManager.callList(this.displayList);
                this.renderCustomModels(par1);
                if (this.childModels != null) {
                    for (int var2 = 0; var2 < this.childModels.size(); ++var2) {
                        ((ModelRenderer)this.childModels.get(var2)).render(par1);
                    }
                }
                GlStateManager.popMatrix();
            }
            GlStateManager.translate(-this.offsetX, -this.offsetY, -this.offsetZ);
        }
    }

    @SideOnly(value=Side.CLIENT)
    private void compileDisplayList(float par1) {
        this.displayList = GLAllocation.generateDisplayLists(1);
        GL11.glNewList((int)this.displayList, (int)4864);
        Tessellator tessellator = Tessellator.getInstance();
        for (Object aCubeList : this.cubeList) {
            ((ModelBox)aCubeList).render(tessellator.getBuffer(), par1);
        }
        GL11.glEndList();
        this.compiled = true;
    }

    protected void renderCustomModels(float scale) {
        if (this.isTransparent) {
            GlStateManager.enableBlend();
            GlStateManager.depthMask(false);
            GlStateManager.color(1.0f, 1.0f, 1.0f, this.transparency);
        }
        for (ModelCustomWrapper obj : this.objs) {
            obj.render(scale);
        }
        if (this.isTransparent) {
            GlStateManager.disableBlend();
            GlStateManager.depthMask(true);
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        }
        if (OpenGlHelper.useVbo()) {
            OpenGlHelper.glBindBuffer(OpenGlHelper.GL_ARRAY_BUFFER, 0);
        }
    }
}

