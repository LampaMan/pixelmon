/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Sets
 */
package com.pixelmongenerations.client.models;

import com.google.common.collect.Sets;
import java.time.Instant;
import java.util.Set;
import net.minecraft.client.model.ModelBase;

public abstract class ModelHolder<M extends ModelBase> {
    static Set<ModelHolder> loadedHolders = Sets.newConcurrentHashSet();
    long lastAccess;
    protected M model = null;

    public M getModel() {
        this.lastAccess = Instant.now().getEpochSecond();
        if (this.model == null) {
            this.model = this.loadModel();
            loadedHolders.add(this);
        }
        return this.model;
    }

    public void clear() {
        loadedHolders.remove(this);
        this.model = null;
    }

    protected abstract M loadModel();
}

