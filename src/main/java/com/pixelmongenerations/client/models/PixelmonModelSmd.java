/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models;

import com.pixelmongenerations.client.models.PixelmonModelBase;
import com.pixelmongenerations.client.models.smd.SmdAnimation;
import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import com.pixelmongenerations.common.entity.pixelmon.Entity2HasModel;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.EntityStatue;
import com.pixelmongenerations.common.entity.pixelmon.helpers.animation.IncrementingVariable;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;

public abstract class PixelmonModelSmd
extends PixelmonModelBase {
    public float animationIncrement = 1.0f;
    public ValveStudioModel theModel;
    @Deprecated
    public float scale = 0.0f;

    public PixelmonModelSmd() {
        this.registerAnimationCounters();
    }

    @Override
    public void render(Entity var1, float f, float f1, float f2, float f3, float f4, float f5) {
    }

    @Override
    public float getScale() {
        return this.theModel.getScale();
    }

    public void setAnimationIncrement(float f) {
        this.animationIncrement = f;
    }

    public void setupForRender(Entity3HasStats pixelmon) {
        this.setAnimation(pixelmon.getCurrentAnimation().toString());
        if (pixelmon instanceof EntityStatue && !((EntityStatue)pixelmon).isAnimated()) {
            return;
        }
        if (this.getCounter(-1, pixelmon) == null) {
            this.setCounter(-1, 2.14748365E9f, this.animationIncrement, (Entity2HasModel)pixelmon);
        }
        this.tickAnimation(pixelmon);
    }

    protected void tickAnimation(Entity3HasStats pixelmon) {
        SmdAnimation theAnim = this.theModel.currentAnimation;
        int frame = (int)Math.floor(this.getCounter((int)-1, (Entity2HasModel)pixelmon).value % (float)theAnim.totalFrames);
        theAnim.setCurrentFrame(frame);
        pixelmon.world.profiler.startSection("pixelmon_animate");
        this.theModel.animate();
        pixelmon.world.profiler.endSection();
    }

    protected void setAnimation(String string) {
        this.theModel.setAnimation(string);
    }

    public static boolean isMinecraftPaused() {
        Minecraft m = Minecraft.getMinecraft();
        return m.isSingleplayer() && m.currentScreen != null && m.currentScreen.doesGuiPauseGame() && !m.getIntegratedServer().getPublic();
    }

    @Override
    protected void setInt(int id, int value, Entity2HasModel pixelmon) {
        pixelmon.getAnimationVariables().setInt(id, value);
    }

    @Override
    protected int getInt(int id, Entity2HasModel pixelmon) {
        return pixelmon.getAnimationVariables().getInt(id);
    }

    protected IncrementingVariable setCounter(int id, float limit, float increment, Entity2HasModel pixelmon) {
        pixelmon.getAnimationVariables().setCounter(id, limit, increment);
        return this.getCounter(id, pixelmon);
    }

    @Override
    protected IncrementingVariable getCounter(int id, Entity2HasModel pixelmon) {
        return pixelmon.getAnimationVariables().getCounter(id);
    }

    @Override
    protected void registerAnimationCounters() {
    }
}

