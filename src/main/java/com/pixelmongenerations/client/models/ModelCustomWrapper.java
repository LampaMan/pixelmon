/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models;

import com.pixelmongenerations.client.models.IPixelmonModel;
import net.minecraft.client.renderer.GlStateManager;

public class ModelCustomWrapper {
    public IPixelmonModel model;
    int frame = 0;
    float offsetX = 0.0f;
    float offsetY = 0.0f;
    float offsetZ = 0.0f;

    public ModelCustomWrapper(IPixelmonModel m) {
        this.model = m;
    }

    public ModelCustomWrapper(IPixelmonModel m, float x, float y, float z) {
        this.model = m;
        this.offsetX = x;
        this.offsetY = y;
        this.offsetZ = z;
    }

    public ModelCustomWrapper setOffsets(float x, float y, float z) {
        this.offsetX = x;
        this.offsetY = y;
        this.offsetZ = z;
        return this;
    }

    public void render(float scale) {
        GlStateManager.pushMatrix();
        GlStateManager.scale(scale, scale, scale);
        this.model.renderAll();
        GlStateManager.popMatrix();
    }

    public void renderOffset(float scale) {
        GlStateManager.pushMatrix();
        GlStateManager.scale(scale, scale, scale);
        GlStateManager.translate(this.offsetX, this.offsetZ, this.offsetY);
        this.model.renderAll();
    }
}

