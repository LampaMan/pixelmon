/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.plates;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelPlate
extends ModelBase {
    ModelRenderer Plate;

    public ModelPlate() {
        this.textureWidth = 32;
        this.textureHeight = 32;
        this.Plate = new ModelRenderer(this, 3, 20);
        this.Plate.addBox(-13.9f, 0.0f, -2.5f, 6, 1, 6);
        this.Plate.setRotationPoint(6.5f, 13.0f, 0.0f);
        this.Plate.setTextureSize(32, 32);
        this.Plate.mirror = true;
        this.setRotation(this.Plate, 0.0f, 0.0f, 0.0f);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.Plate.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void renderModel(float f5) {
        this.Plate.render(f5);
    }
}

