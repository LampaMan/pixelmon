/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.event;

import com.pixelmongenerations.client.gui.pokedex.ClientPokedexManager;
import com.pixelmongenerations.client.keybindings.TargetKeyBinding;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.ItemPokedex;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.pokedex.OpenPokedex;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class PlayerInteract {
    @SubscribeEvent
    public void onPlayerInteract(PlayerInteractEvent.RightClickItem event) {
        EntityPlayer player = event.getEntityPlayer();
        if (player.getHeldItem(EnumHand.MAIN_HAND) != ItemStack.EMPTY) {
            if (!(player.getHeldItem(EnumHand.MAIN_HAND).getItem() instanceof ItemPokedex)) {
                return;
            }
            try {
                RayTraceResult objectMouseOver = TargetKeyBinding.getTarget(false);
                if (objectMouseOver.typeOfHit == RayTraceResult.Type.ENTITY && objectMouseOver.entityHit instanceof EntityPixelmon) {
                    EntityPixelmon pokemon = (EntityPixelmon)objectMouseOver.entityHit;
                    Pixelmon.NETWORK.sendToServer(new OpenPokedex(pokemon.baseStats.pokemon));
                    return;
                }
                Pixelmon.NETWORK.sendToServer(new OpenPokedex(ClientPokedexManager.pokedex == null));
            }
            catch (UnsupportedOperationException unsupportedOperationException) {
                // empty catch block
            }
        }
    }
}

