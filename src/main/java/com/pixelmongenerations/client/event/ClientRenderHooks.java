/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.event;

import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.client.render.tileEntities.RenderTileEntityPokeChest;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;

@Mod.EventBusSubscriber(value={Side.CLIENT}, modid="pixelmon")
public class ClientRenderHooks {
    public static float partialTicks;
    public static float updateTimer;

    @SubscribeEvent
    public static void renderTick(TickEvent.RenderTickEvent event) {
        switch (event.phase) {
            case START: {
                partialTicks = event.renderTickTime;
                break;
            }
        }
    }

    @SubscribeEvent
    public static void renderTick(TickEvent.ClientTickEvent event) {
        switch (event.phase) {
            case START: {
                RenderTileEntityPokeChest.frame += 1.0f * partialTicks;
                GuiPixelmonOverlay.updateOverlays();
                break;
            }
        }
    }

    static {
        updateTimer = 0.0f;
    }
}

