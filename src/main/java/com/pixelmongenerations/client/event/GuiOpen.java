/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.event;

import com.pixelmongenerations.client.gui.GuiNewChatExt;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

@Mod.EventBusSubscriber(value={Side.CLIENT}, modid="pixelmon")
public class GuiOpen {
    @SubscribeEvent(priority=EventPriority.LOWEST)
    public static void onGuiOpen(GuiOpenEvent event) {
        if (event.getGui() instanceof GuiMainMenu) {
            Minecraft.getMinecraft().ingameGUI.persistantChatGUI = new GuiNewChatExt(Minecraft.getMinecraft());
        }
    }
}

