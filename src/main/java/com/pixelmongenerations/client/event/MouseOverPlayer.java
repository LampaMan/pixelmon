/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.event;

import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.client.gui.overlay.OverlayType;
import com.pixelmongenerations.client.gui.overlay.SpectateOverlay;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.battles.CheckPlayerBattle;
import java.util.List;
import java.util.UUID;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public class MouseOverPlayer {
    private static final int range = 10;
    private static final int tickInterval = 10;
    private final SpectateOverlay overlay = (SpectateOverlay)GuiPixelmonOverlay.getOverlay(OverlayType.SPECTATE);
    private int tick = 0;
    private UUID lastPlayer;

    @SubscribeEvent
    public void onPlayerTick(TickEvent.PlayerTickEvent event) {
        if (event.type == TickEvent.Type.PLAYER && event.player instanceof EntityPlayerSP && event.player == Minecraft.getMinecraft().player) {
            this.overlay.onPlayerTick();
            ++this.tick;
            if (this.tick >= 10) {
                this.tick = 0;
                EntityPlayer player = this.getLookAtPlayer();
                if (player != null) {
                    if (!player.getUniqueID().equals(this.lastPlayer)) {
                        this.sendCheckBattlePacket(player);
                        this.lastPlayer = player.getUniqueID();
                    }
                } else {
                    if (this.lastPlayer != null) {
                        this.overlay.hideSpectateMessage(this.lastPlayer);
                    }
                    this.lastPlayer = null;
                }
            }
        }
    }

    private void sendCheckBattlePacket(EntityPlayer player) {
        CheckPlayerBattle message = new CheckPlayerBattle(player);
        Pixelmon.NETWORK.sendToServer(message);
    }

    private EntityPlayer getLookAtPlayer() {
        Minecraft mc = Minecraft.getMinecraft();
        EntityPlayer pointedEntity = null;
        float partialTick = 1.0f;
        Vec3d Vec3d2 = mc.getRenderViewEntity().getPositionEyes(partialTick);
        Vec3d Vec3d1 = mc.getRenderViewEntity().getLook(partialTick);
        Vec3d Vec3d22 = Vec3d2.add(Vec3d1.x * 10.0, Vec3d1.y * 10.0, Vec3d1.z * 10.0);
        double d1 = 10.0;
        Vec3d2 = mc.getRenderViewEntity().getPositionEyes(partialTick);
        float f1 = 1.0f;
        List<EntityPlayer> list = mc.world.getEntitiesWithinAABB(EntityPlayer.class, mc.getRenderViewEntity().getEntityBoundingBox().expand(Vec3d1.x * 10.0, Vec3d1.y * 10.0, Vec3d1.z * 10.0).expand(f1, f1, f1));
        double d2 = d1;
        for (EntityPlayer entity : list) {
            double d;
            if (entity == Minecraft.getMinecraft().player || !entity.canBeCollidedWith()) continue;
            float f2 = entity.getCollisionBorderSize();
            AxisAlignedBB axisalignedbb = entity.getEntityBoundingBox().expand(f2, f2, f2);
            RayTraceResult movingobjectposition = axisalignedbb.calculateIntercept(Vec3d2, Vec3d22);
            if (axisalignedbb.contains(Vec3d2)) {
                if (0.0 >= d2 && d2 != 0.0) continue;
                pointedEntity = entity;
                d2 = 0.0;
                continue;
            }
            if (movingobjectposition == null) continue;
            double d3 = Vec3d2.distanceTo(movingobjectposition.hitVec);
            if (d3 >= d2 && d2 != 0.0) continue;
            if (entity == mc.getRenderViewEntity().getControllingPassenger() && !entity.canRiderInteract()) {
                if (d2 != 0.0) continue;
                pointedEntity = entity;
                continue;
            }
            pointedEntity = entity;
            d2 = d3;
        }
        return pointedEntity;
    }
}

