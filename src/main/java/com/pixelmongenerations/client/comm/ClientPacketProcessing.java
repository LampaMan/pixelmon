/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.comm;

import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import net.minecraft.client.Minecraft;

public class ClientPacketProcessing {
    public void handlePlayerDeath() {
        GuiPixelmonOverlay.isVisible = true;
        Minecraft.getMinecraft().gameSettings.thirdPersonView = 0;
        Minecraft.getMinecraft().gameSettings.hideGUI = false;
        Minecraft.getMinecraft().setRenderViewEntity(Minecraft.getMinecraft().player);
        Minecraft.getMinecraft().currentScreen = null;
    }
}

