/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client;

import net.minecraft.client.audio.Sound;
import net.minecraft.util.ResourceLocation;

public class GenerationsSound
extends Sound {
    public GenerationsSound(ResourceLocation location) {
        super(location.toString(), 1.0f, 1.0f, 1, Sound.Type.FILE, true);
    }

    @Override
    public ResourceLocation getSoundAsOggLocation() {
        return this.getSoundLocation();
    }
}

