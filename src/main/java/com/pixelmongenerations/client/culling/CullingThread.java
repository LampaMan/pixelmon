/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.culling;

import com.pixelmongenerations.client.culling.RayTracingCache;
import com.pixelmongenerations.client.culling.RayTracingEngine;
import com.pixelmongenerations.client.util.reflection.ReflectionField;
import com.pixelmongenerations.common.block.tileEntities.ICullable;
import java.util.Iterator;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ActiveRenderInfo;
import net.minecraft.client.renderer.culling.ClippingHelperImpl;
import net.minecraft.client.renderer.culling.Frustum;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class CullingThread extends Thread {

    private static final ReflectionField<ClippingHelperImpl> FIELD_INSTANCE = new ReflectionField(ClippingHelperImpl.class, "field_78563_e", "instance");
    private final RayTracingEngine.MutableRayTraceResult mutableRayTraceResult = new RayTracingEngine.MutableRayTraceResult();
    private final RayTracingCache cache = new RayTracingCache(12);
    private double sleepOverhead = 0.0;
    public long[] time = new long[10];
    private Frustum frustum;
    private double camX;
    private double camY;
    private double camZ;
    private int camBlockX;
    private int camBlockY;
    private int camBlockZ;
    private double x;
    private double y;
    private double z;

    public CullingThread() {
        this.setName("Culling Thread");
        this.setDaemon(true);
    }

    @Override
    public void run() {
        Minecraft mc = Minecraft.getMinecraft();
        while (true) {
            long t = System.nanoTime();
            try {
                RayTracingEngine.resetCache();
                this.cache.clearCache();
                if (mc.world != null && mc.getRenderViewEntity() != null) {
                    Entity renderViewEntity = mc.getRenderViewEntity();
                    float partialTicks = mc.getRenderPartialTicks();
                    this.x = renderViewEntity.lastTickPosX + (renderViewEntity.posX - renderViewEntity.lastTickPosX) * (double) partialTicks;
                    this.y = renderViewEntity.lastTickPosY + (renderViewEntity.posY - renderViewEntity.lastTickPosY) * (double) partialTicks;
                    this.z = renderViewEntity.lastTickPosZ + (renderViewEntity.posZ - renderViewEntity.lastTickPosZ) * (double) partialTicks;
                    this.frustum = new Frustum(FIELD_INSTANCE.get(null));
                    this.frustum.setPosition(this.x, this.y, this.z);
                    Vec3d cameraPosition = ActiveRenderInfo.getCameraPosition();
                    this.camX = this.x + cameraPosition.x;
                    this.camY = this.y + cameraPosition.y;
                    this.camZ = this.z + cameraPosition.z;
                    this.camBlockX = MathHelper.floor(this.camX);
                    this.camBlockY = MathHelper.floor(this.camY);
                    this.camBlockZ = MathHelper.floor(this.camZ);
                    Iterator<TileEntity> tileEntityIterator = mc.world.loadedTileEntityList.iterator();
                    while (tileEntityIterator.hasNext()) {
                        try {
                            TileEntity tileEntity = tileEntityIterator.next();
                            if (tileEntity instanceof ICullable) {
                                this.updateTileEntityCullingState(tileEntity);
                            }
                        } catch (Exception e) {
                            break;
                        }
                    }
                }
            } catch (Exception exception) {
                // empty catch block
            }
            t = System.nanoTime() - t;
            double d = (double)t / 1000000.0 + this.sleepOverhead;
            this.sleepOverhead = d % 1.0;
            long sleepTime = 10L - (long)d;
            if (sleepTime <= 0L) continue;
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    private void updateTileEntityCullingState(TileEntity tileEntity) {
        ((ICullable)((Object)tileEntity)).setCulled(!this.checkTileEntityVisibility(tileEntity));
    }

    private boolean checkTileEntityVisibility(TileEntity tileEntity) {
        AxisAlignedBB aabb = tileEntity.getRenderBoundingBox();
        double minX = aabb.minX;
        double minY = aabb.minY;
        double minZ = aabb.minZ;
        double maxX = aabb.maxX;
        double maxY = aabb.maxY;
        double maxZ = aabb.maxZ;
        if (tileEntity.getDistanceSq(this.x, this.y, this.z) >= tileEntity.getMaxRenderDistanceSquared()) {
            return true;
        }
        if (!this.frustum.isBoxInFrustum(minX, minY, minZ, maxX, maxY, maxZ)) {
            return true;
        }
        if (this.checkVisibility(tileEntity.getWorld(), this.camX, this.camY, this.camZ, (minX + maxX) * 0.5, (minY + maxY) * 0.5, (minZ + maxZ) * 0.5, 1.0)) {
            return true;
        }
        return this.checkBoundingBoxVisibility(tileEntity.getWorld(), minX, minY, minZ, maxX, maxY, maxZ);
    }

    private boolean checkBoundingBoxVisibility(World world, double minX, double minY, double minZ, double maxX, double maxY, double maxZ) {
        int startX = MathHelper.floor(minX);
        int startY = MathHelper.floor(minY);
        int startZ = MathHelper.floor(minZ);
        int endX = MathHelper.ceil(maxX);
        int endY = MathHelper.ceil(maxY);
        int endZ = MathHelper.ceil(maxZ);
        if (this.camX < (double)startX) {
            int x2 = startX;
            for (int y3 = startY; y3 <= endY; ++y3) {
                for (int z2 = startZ; z2 <= endZ; ++z2) {
                    if (this.checkVisibilityCached(world, x2, y3, z2)) {
                        return true;
                    }
                }
            }
        } else if (this.camX > (double)endX) {
            int x2 = endX;
            for (int y3 = startY; y3 <= endY; ++y3) {
                for (int z2 = startZ; z2 <= endZ; ++z2) {
                    if (this.checkVisibilityCached(world, x2, y3, z2)) {
                        return true;
                    }
                }
            }
        }
        if (this.camY < (double)startY) {
            int y2 = startY;
            for (int x = startX; x <= endX; ++x) {
                for (int z2 = startZ; z2 <= endZ; ++z2) {
                    if (this.checkVisibilityCached(world, x, y2, z2)) {
                        return true;
                    }
                }
            }
        } else if (this.camY > (double)endY) {
            int y2 = endY;
            for (int x = startX; x <= endX; ++x) {
                for (int z2 = startZ; z2 <= endZ; ++z2) {
                    if (this.checkVisibilityCached(world, x, y2, z2)) {
                        return true;
                    }
                }
            }
        }
        if (this.camZ < (double)startZ) {
            int z = startZ;
            for (int x = startX; x <= endX; ++x) {
                for (int y = startY; y <= endY; ++y) {
                    if (this.checkVisibilityCached(world, x, y, z)) {
                        return true;
                    }
                }
            }
        } else if (this.camZ > (double)endZ) {
            int z = endZ;
            for (int x = startX; x <= endX; ++x) {
                for (int y = startY; y <= endY; ++y) {
                    if (this.checkVisibilityCached(world, x, y, z)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean checkVisibilityCached(World world, int endX, int endY, int endZ) {
        int cachedValue;
        int cacheX = endX - this.camBlockX + this.cache.radiusBlocks;
        int cacheY = endY - this.camBlockY + this.cache.radiusBlocks;
        int cacheZ = endZ - this.camBlockZ + this.cache.radiusBlocks;
        RayTracingCache.RayTracingCacheChunk chunk = this.cache.getChunk(cacheX >> 4, cacheY >> 4, cacheZ >> 4);
        if (chunk != null && (cachedValue = chunk.getCachedValue(cacheX & 0xF, cacheY & 0xF, cacheZ & 0xF)) > 0) {
            return cachedValue >> 1 == 1;
        }
        boolean flag = this.checkVisibility(world, this.camX, this.camY, this.camZ, endX, endY, endZ, 1.0);
        if (chunk != null) {
            chunk.setCachedValue(cacheX & 0xF, cacheY & 0xF, cacheZ & 0xF, flag ? 2 : 1);
        }
        return flag;
    }

    private boolean checkVisibility(World world, double startX, double startY, double startZ, double endX, double endY, double endZ, double maxDiff) {
        RayTracingEngine.MutableRayTraceResult rayTraceResult = RayTracingEngine.rayTraceBlocks(world, startX, startY, startZ, endX, endY, endZ, true, maxDiff, this.mutableRayTraceResult);
        return rayTraceResult == null;
    }
}

