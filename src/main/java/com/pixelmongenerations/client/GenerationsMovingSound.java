/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client;

import com.pixelmongenerations.client.GenerationsSound;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSound;
import net.minecraft.client.audio.SoundEventAccessor;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(value=Side.CLIENT)
public class GenerationsMovingSound
extends PositionedSound {
    protected boolean donePlaying;
    private final EntityPlayerSP player;
    private float distance = 0.0f;
    private SoundEventAccessor soundEvent;

    public GenerationsMovingSound(ResourceLocation resource) {
        super(resource, SoundCategory.NEUTRAL);
        this.player = Minecraft.getMinecraft().player;
        this.repeat = false;
        this.sound = new GenerationsSound(resource);
        this.xPosF = (float)this.player.posX;
        this.yPosF = (float)this.player.posY;
        this.zPosF = (float)this.player.posZ;
        this.volume = 1.0f;
    }

    public void update() {
        if (this.player.isDead) {
            this.donePlaying = true;
        }
    }

    @Override
    public float getVolume() {
        return this.volume * this.sound.getVolume();
    }

    @Override
    public float getPitch() {
        return this.pitch * this.sound.getPitch();
    }

    public boolean isDonePlaying() {
        return this.donePlaying;
    }

    @Override
    public SoundEventAccessor createAccessor(SoundHandler handler) {
        if (this.soundEvent == null) {
            this.soundEvent = new SoundEventAccessor(this.getSoundLocation(), "streamedaudio");
        }
        return this.soundEvent;
    }
}

