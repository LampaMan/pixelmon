/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client;

import java.util.Map;
import java.util.function.ToIntFunction;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.DefaultStateMapper;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;

public class VariantRegister {
    public static void registerBlockItemModelForMeta(IBlockState state, int metadata) {
        Item item = Item.getItemFromBlock(state.getBlock());
        if (item != Items.AIR) {
            VariantRegister.registerItemModelForMeta(item, metadata, new DefaultStateMapper().getPropertyString((Map<IProperty<?>, Comparable<?>>)state.getProperties()));
        }
    }

    public static <T extends Comparable<T>> void registerVariantBlockItemModels(IBlockState baseState, IProperty<T> property, ToIntFunction<T> getMeta) {
        property.getAllowedValues().forEach(value -> VariantRegister.registerBlockItemModelForMeta(baseState.withProperty(property, value), getMeta.applyAsInt(value)));
    }

    public static void registerItemModelForMeta(Item item, int metadata, String variant) {
        VariantRegister.registerItemModelForMeta(item, metadata, new ModelResourceLocation(item.getRegistryName(), variant));
    }

    public static void registerItemModelForMeta(Item item, int metadata, ModelResourceLocation modelResourceLocation) {
        ModelLoader.setCustomModelResourceLocation(item, metadata, modelResourceLocation);
    }
}

