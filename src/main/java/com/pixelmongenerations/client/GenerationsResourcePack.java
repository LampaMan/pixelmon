/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableSet
 */
package com.pixelmongenerations.client;

import com.google.common.collect.ImmutableSet;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.client.resources.IResourcePack;
import net.minecraft.client.resources.data.IMetadataSection;
import net.minecraft.client.resources.data.MetadataSerializer;
import net.minecraft.client.resources.data.PackMetadataSection;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentString;

public class GenerationsResourcePack
implements IResourcePack {
    public static final String SPLIT_PATH_MODEL = "dynpixelmon:customassets/";
    public static final String SPLIT_PATH_ICON = "dynpixelmon:customicon/";
    public static final String SPLIT_PATH_SOUND = "dynpixelmon:customsound/";
    public static final Set<String> DEFAULT_RESOURCE_DOMAINS = ImmutableSet.of("dynpixelmon");

    @Override
    public InputStream getInputStream(ResourceLocation location) throws IOException {
        if (location.toString().startsWith(SPLIT_PATH_MODEL) || location.toString().startsWith(SPLIT_PATH_ICON) || location.toString().startsWith(SPLIT_PATH_SOUND)) {
            NetHandlerPlayClient netHandler = Minecraft.getMinecraft().player.connection;
            if (netHandler == null || netHandler.getNetworkManager() == null || netHandler.getNetworkManager().getRemoteAddress() == null) {
                return null;
            }
            String remoteAdd = netHandler.getNetworkManager().getRemoteAddress().toString();
            if (remoteAdd.contains("/")) {
                remoteAdd = remoteAdd.split("/")[1];
            }
            remoteAdd = remoteAdd.replaceAll(":", "-");
            File dir = new File("./resource-cache/" + (remoteAdd.contains("local") ? "singleplayer" : remoteAdd) + "/");
            dir.mkdirs();
            return new FileInputStream(new File(dir, location.toString().replaceAll(SPLIT_PATH_MODEL, "").replaceAll(SPLIT_PATH_ICON, "").replaceAll(SPLIT_PATH_SOUND, "")));
        }
        return null;
    }

    @Override
    public boolean resourceExists(ResourceLocation location) {
        if (location.toString().startsWith(SPLIT_PATH_MODEL) || location.toString().startsWith(SPLIT_PATH_ICON) || location.toString().startsWith(SPLIT_PATH_SOUND)) {
            NetHandlerPlayClient netHandler = Minecraft.getMinecraft().player.connection;
            if (netHandler == null || netHandler.getNetworkManager() == null || netHandler.getNetworkManager().getRemoteAddress() == null) {
                return false;
            }
            String remoteAdd = netHandler.getNetworkManager().getRemoteAddress().toString();
            if (remoteAdd.contains("/")) {
                remoteAdd = remoteAdd.split("/")[1];
            }
            remoteAdd = remoteAdd.replaceAll(":", "-");
            File dir = new File("./resource-cache/" + (remoteAdd.contains("local") ? "singleplayer" : remoteAdd) + "/");
            dir.mkdirs();
            return new File(dir, location.toString().replaceAll(SPLIT_PATH_MODEL, "").replaceAll(SPLIT_PATH_ICON, "").replaceAll(SPLIT_PATH_SOUND, "")).exists();
        }
        return false;
    }

    @Override
    public Set<String> getResourceDomains() {
        return DEFAULT_RESOURCE_DOMAINS;
    }

    @Override
    public <T extends IMetadataSection> T getPackMetadata(MetadataSerializer metadataSerializer, String metadataSectionName) throws IOException {
        return (T)new PackMetadataSection(new TextComponentString("Dynamic assets for Pixelmon"), 3);
    }

    @Override
    public BufferedImage getPackImage() throws IOException {
        BufferedImage bi = new BufferedImage(64, 64, 2);
        for (int x = 0; x < 64; ++x) {
            for (int y  = 0; y < 64; ++y) {
                bi.setRGB(x, y, 0xFFFFFF);
            }
        }
        return bi;
    }

    @Override
    public String getPackName() {
        return "Generations Dynamic Assets";
    }
}

