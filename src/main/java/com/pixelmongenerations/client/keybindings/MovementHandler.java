/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.keybindings;

import com.pixelmongenerations.client.keybindings.Descend;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumMovement;
import com.pixelmongenerations.core.network.packetHandlers.EnumMovementType;
import com.pixelmongenerations.core.network.packetHandlers.Movement;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public class MovementHandler {
    public static int jump = 0;

    @SubscribeEvent
    public void tickStart(TickEvent.ClientTickEvent event) {
        if (Minecraft.getMinecraft().player != null && Minecraft.getMinecraft().player.getRidingEntity() != null) {
            this.handleRidingMovement();
        }
    }

    private void handleRidingMovement() {
        if (Minecraft.getMinecraft().gameSettings.keyBindJump.isKeyDown()) {
            ++jump;
        }
        if (Descend.Instance.isKeyDown()) {
            --jump;
        }
        int numMovements = 0;
        if (jump != 0) {
            ++numMovements;
        }
        EnumMovement[] movements = new EnumMovement[numMovements];
        int i = 0;
        if (jump > 0) {
            movements[i++] = EnumMovement.Jump;
        }
        if (jump < 0) {
            movements[i++] = EnumMovement.Crouch;
        }
        if (numMovements > 0) {
            Pixelmon.NETWORK.sendToServer(new Movement(movements, EnumMovementType.Riding));
        }
        jump = 0;
    }
}

