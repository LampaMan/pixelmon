/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.keybindings;

import com.pixelmongenerations.client.gui.cosmetics.GuiCosmetics;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;

public class CosmeticsKey
extends KeyBinding {
    public CosmeticsKey() {
        super("key.cosmetics", 49, "key.categories.pixelmon");
    }

    @SubscribeEvent
    public void keyDown(InputEvent.KeyInputEvent event) {
        if (this.isPressed()) {
            Minecraft mc = Minecraft.getMinecraft();
            if (mc.world == null) {
                return;
            }
            mc.displayGuiScreen(new GuiCosmetics());
        }
    }
}

