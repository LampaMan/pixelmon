/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.keybindings;

import com.pixelmongenerations.client.keybindings.TargetKeyBinding;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.client.IModGuiFactory;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;

public class OptionsKey
extends TargetKeyBinding {
    public OptionsKey() {
        super("key.pixelmonoptions", 25, "key.categories.pixelmon");
    }

    @SubscribeEvent
    public void keyDown(InputEvent.KeyInputEvent event) {
        if (this.isPressed()) {
            for (ModContainer mod : Loader.instance().getModList()) {
                if (!mod.getModId().equals("pixelmon")) continue;
                try {
                    IModGuiFactory guiFactory = FMLClientHandler.instance().getGuiFactoryFor(mod);
                    GuiScreen newScreen = guiFactory.createConfigGui(null);
                    Minecraft.getMinecraft().displayGuiScreen(newScreen);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                return;
            }
        }
    }
}

