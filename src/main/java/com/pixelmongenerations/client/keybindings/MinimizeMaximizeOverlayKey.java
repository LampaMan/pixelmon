/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.keybindings;

import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.client.gui.overlay.OverlayType;
import com.pixelmongenerations.client.gui.overlay.PartyOverlay;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;

public class MinimizeMaximizeOverlayKey
extends KeyBinding {
    public MinimizeMaximizeOverlayKey() {
        super("key.minmaxoverlay", 35, "key.categories.pixelmon");
    }

    @SubscribeEvent
    public void keyDown(InputEvent.KeyInputEvent event) {
        if (this.isPressed()) {
            if (Minecraft.getMinecraft().currentScreen != null || Minecraft.getMinecraft().world == null) {
                return;
            }
            PartyOverlay overlay = (PartyOverlay)GuiPixelmonOverlay.getOverlay(OverlayType.PARTY);
            overlay.toggleMinimized();
        }
    }
}

