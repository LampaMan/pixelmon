/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.keybindings;

import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.client.gui.overlay.OverlayType;
import com.pixelmongenerations.client.gui.overlay.PartyOverlay;
import com.pixelmongenerations.client.keybindings.TargetKeyBinding;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.EnumPixelmonKeyType;
import com.pixelmongenerations.core.network.packetHandlers.KeyPacket;
import com.pixelmongenerations.core.util.PixelmonMethods;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;

public class ActionKey
extends TargetKeyBinding {
    public ActionKey() {
        super("key.pokemonaction", 47, "key.categories.pixelmon");
    }

    @SubscribeEvent
    public void keyDown(InputEvent.KeyInputEvent event) {
        if (this.isPressed()) {
            if (Minecraft.getMinecraft().world == null) {
                return;
            }
            PartyOverlay overlay = (PartyOverlay)GuiPixelmonOverlay.getOverlay(OverlayType.PARTY);
            PixelmonData currentPokemon = overlay.getSelectedPokemon();
            if (currentPokemon == null || !currentPokemon.outside) {
                return;
            }
            RayTraceResult objectMouseOver = ActionKey.getTarget(this.targetLiquids);
            boolean isSamePokemon = false;
            if (objectMouseOver == null) {
                return;
            }
            if (objectMouseOver.entityHit instanceof EntityPixelmon && PixelmonMethods.isIDSame((EntityPixelmon)objectMouseOver.entityHit, currentPokemon.pokemonID)) {
                isSamePokemon = true;
            }
            if (objectMouseOver.typeOfHit == RayTraceResult.Type.ENTITY && !isSamePokemon) {
                Entity entity = objectMouseOver.entityHit;
                Pixelmon.NETWORK.sendToServer(new KeyPacket(currentPokemon.pokemonID, entity.getEntityId(), EnumPixelmonKeyType.ActionKeyEntity));
            } else {
                Pixelmon.NETWORK.sendToServer(new KeyPacket(currentPokemon.pokemonID, -1, EnumPixelmonKeyType.ActionKeyEntity));
            }
        }
    }
}

