/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.keybindings;

import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.client.gui.overlay.OverlayType;
import com.pixelmongenerations.client.gui.overlay.PartyOverlay;
import com.pixelmongenerations.client.models.PixelmonModelRegistry;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.KeyPacket;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;

public class SendPokemonKey
extends KeyBinding {
    public SendPokemonKey() {
        super("key.sendpokemon", 19, "key.categories.pixelmon");
    }

    @SubscribeEvent
    public void keyDown(InputEvent.KeyInputEvent event) {
        if (this.isPressed()) {
            Minecraft mc = Minecraft.getMinecraft();
            if (mc.currentScreen != null || mc.world == null) {
                return;
            }
            PartyOverlay overlay = (PartyOverlay)GuiPixelmonOverlay.getOverlay(OverlayType.PARTY);
            PixelmonData current = overlay.getSelectedPokemon();
            if (current != null && !current.isEgg) {
                current.targetId = -1;
                PixelmonModelRegistry.getModel(current.getSpecies(), current.getSpecies().getFormEnum(current.form));
                Pixelmon.NETWORK.sendToServer(new KeyPacket(current.pokemonID));
            }
        }
    }
}

