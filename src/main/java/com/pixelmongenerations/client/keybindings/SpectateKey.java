/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.keybindings;

import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.client.gui.overlay.OverlayType;
import com.pixelmongenerations.client.gui.overlay.SpectateOverlay;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.battles.RequestSpectate;
import java.util.UUID;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;

public class SpectateKey
extends KeyBinding {
    public SpectateKey() {
        super("key.spectateBattle", 21, "key.categories.pixelmon");
    }

    @SubscribeEvent
    public void keyDown(InputEvent.KeyInputEvent event) {
        if (this.isPressed()) {
            if (Minecraft.getMinecraft().world == null) {
                return;
            }
            UUID uuid = ((SpectateOverlay)GuiPixelmonOverlay.getOverlay(OverlayType.SPECTATE)).getCurrentSpectatingUUID();
            if (uuid != null) {
                RequestSpectate message = new RequestSpectate(uuid);
                Pixelmon.NETWORK.sendToServer(message);
            }
        }
    }
}

