/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.keybindings;

import com.pixelmongenerations.client.gui.overlay.CurrentPokemonOverlay;
import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.client.gui.overlay.OverlayType;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;

public class NextExternalMoveKey
extends KeyBinding {
    public NextExternalMoveKey() {
        super("key.nextexternalmove", 48, "key.categories.pixelmon");
    }

    @SubscribeEvent
    public void keyDown(InputEvent.KeyInputEvent event) {
        if (this.isPressed()) {
            if (Minecraft.getMinecraft().world == null) {
                return;
            }
            CurrentPokemonOverlay overlay = (CurrentPokemonOverlay)GuiPixelmonOverlay.getOverlay(OverlayType.CURRENT_POKEMON);
            overlay.selectNextExternalMove();
        }
    }
}

