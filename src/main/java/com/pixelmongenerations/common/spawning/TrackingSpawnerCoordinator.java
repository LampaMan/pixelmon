/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning;

import com.pixelmongenerations.api.spawning.AbstractSpawner;
import com.pixelmongenerations.api.spawning.SpawnerCoordinator;
import com.pixelmongenerations.common.spawning.PlayerTrackingSpawner;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

public class TrackingSpawnerCoordinator
extends SpawnerCoordinator {
    public static PlayerTrackingSpawner.PlayerTrackingSpawnerBuilder<PlayerTrackingSpawner> spawnerPreset = new PlayerTrackingSpawner.PlayerTrackingSpawnerBuilder();

    public TrackingSpawnerCoordinator() {
        super(new AbstractSpawner[0]);
    }

    @Override
    public TrackingSpawnerCoordinator activate() {
        super.activate();
        this.spawners.removeIf(spawner -> spawner instanceof PlayerTrackingSpawner && !((PlayerTrackingSpawner)spawner).isTrackedPlayerOnline());
        for (EntityPlayerMP player : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers()) {
            this.spawners.add(spawnerPreset.apply(new PlayerTrackingSpawner(player.getUniqueID())));
        }
        return this;
    }

    @SubscribeEvent
    public void onPlayerLogin(PlayerEvent.PlayerLoggedInEvent event) {
        if (!event.player.world.isRemote) {
            this.spawners.add(spawnerPreset.apply(new PlayerTrackingSpawner(event.player.getUniqueID())));
        }
    }

    @SubscribeEvent
    public void onPlayerLogout(PlayerEvent.PlayerLoggedOutEvent event) {
        if (!event.player.world.isRemote) {
            this.spawners.removeIf(spawner -> spawner instanceof PlayerTrackingSpawner && ((PlayerTrackingSpawner)spawner).playerUUID.equals(event.player.getUniqueID()));
        }
    }
}

