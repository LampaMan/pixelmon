/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning.spawners;

import com.pixelmongenerations.common.spawning.EnumBiomeType;
import com.pixelmongenerations.common.spawning.PixelmonBiomeDictionary;
import com.pixelmongenerations.common.spawning.SpawnData;
import com.pixelmongenerations.common.spawning.SpawnRegistry;
import com.pixelmongenerations.common.spawning.spawners.SpawnerBase;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.database.SpawnLocation;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.minecraft.block.material.Material;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldType;

public class SpawnerNPC
extends SpawnerBase {
    static Set<Material> validFloorMaterials = new HashSet<Material>();
    static Set<Material> validAirMaterials;

    public SpawnerNPC() {
        super(SpawnLocation.LandNPC);
    }

    @Override
    public Integer getSpawnConditionY(World world, BlockPos pos) {
        if (world.getWorldInfo().getTerrainType() == WorldType.FLAT && this.isBlockValidForPixelmonSpawning(world, new BlockPos(pos.getX(), world.getHeight(pos).getY(), pos.getZ()))) {
            return world.getHeight(pos).getY();
        }
        Integer topEarth = this.getTopEarthBlock(world, pos, false);
        if (topEarth == null) {
            return null;
        }
        Integer cpY = null;
        if (this.isBlockValidForPixelmonSpawning(world, new BlockPos(pos.getX(), topEarth, pos.getZ()))) {
            cpY = topEarth;
        } else {
            String biomeID = world.getBiome(pos).getRegistryName().getPath();
            if (PixelmonBiomeDictionary.isBiomeOfType(biomeID, EnumBiomeType.JUNGLE)) {
                for (int i = 0; i < 5; ++i) {
                    if (!this.isBlockValidForPixelmonSpawning(world, new BlockPos(pos.getX(), topEarth + i, pos.getZ()))) continue;
                    cpY = topEarth + i;
                    break;
                }
            }
        }
        return cpY;
    }

    @Override
    public List<SpawnData> getEntityList(String biomeID) {
        return SpawnRegistry.getNPCSpawnsForBiome(biomeID);
    }

    @Override
    public boolean canPokemonSpawnHereImpl(World world, BlockPos pos) {
        return this.isBlockValidForPixelmonSpawning(world, pos);
    }

    @Override
    public Set<Material> getSpawnCheckMaterials() {
        return validFloorMaterials;
    }

    @Override
    public Set<Material> getValidSpawnAirMaterials() {
        return validAirMaterials;
    }

    @Override
    public int getMaxNum() {
        return PixelmonConfig.maxNumNPCs;
    }

    static {
        validFloorMaterials.add(Material.CRAFTED_SNOW);
        validFloorMaterials.add(Material.GRASS);
        validFloorMaterials.add(Material.GROUND);
        validFloorMaterials.add(Material.ICE);
        validFloorMaterials.add(Material.LEAVES);
        validFloorMaterials.add(Material.ROCK);
        validFloorMaterials.add(Material.SAND);
        validFloorMaterials.add(Material.SNOW);
        validFloorMaterials.add(Material.PACKED_ICE);
        validFloorMaterials.add(Material.PLANTS);
        validAirMaterials = new HashSet<Material>();
        validAirMaterials.add(Material.AIR);
        validAirMaterials.add(Material.PLANTS);
        validAirMaterials.add(Material.SNOW);
        validAirMaterials.add(Material.VINE);
    }
}

