/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning.spawners;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.spawning.SpawnData;
import com.pixelmongenerations.common.spawning.SpawnRegistry;
import com.pixelmongenerations.common.spawning.spawners.SpawnerBase;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class SpawnerUnderWater
extends SpawnerBase {
    private static Set<Material> validSpawnMaterials = new HashSet<Material>();
    private static Set<Material> validAirMaterials;

    public SpawnerUnderWater() {
        super(SpawnLocation.Water);
    }

    @Override
    public Integer getSpawnConditionY(World world, BlockPos pos) {
        Integer topEarth = this.getTopEarthBlock(world, pos, true);
        Integer topWater = this.getTopWaterBlock(world, pos);
        if (topEarth == null || topWater == null) {
            return null;
        }
        Integer cpY = null;
        if (topWater > topEarth) {
            Integer k = RandomHelper.getRandomNumberBetween(topEarth, topWater);
            if (this.isBlockValidForPixelmonSpawning(world, new BlockPos(pos.getX(), k, pos.getZ()))) {
                cpY = k;
            }
        }
        return cpY;
    }

    @Override
    public float getYOffset(float x, float y, float z, EntityLiving pokemon) {
        if (((EntityPixelmon)pokemon).getSwimmingParameters() != null && ((EntityPixelmon)pokemon).getSwimmingParameters().depthRangeStart == -1) {
            Integer topEarth = this.getTopEarthBlock(pokemon.world, new BlockPos(x, y, z), false);
            return (float)topEarth.intValue() - y;
        }
        return super.getYOffset(x, y, z, pokemon);
    }

    @Override
    public List<SpawnData> getEntityList(String biomeID) {
        return SpawnRegistry.getWaterSpawnsForBiome(biomeID);
    }

    @Override
    public boolean canPokemonSpawnHereImpl(World world, BlockPos pos) {
        return this.isBlockValidForPixelmonSpawning(world, pos);
    }

    @Override
    public Set<Material> getSpawnCheckMaterials() {
        return validSpawnMaterials;
    }

    @Override
    public Set<Material> getValidSpawnAirMaterials() {
        return validAirMaterials;
    }

    @Override
    public int getMaxNum() {
        return PixelmonConfig.maxNumWaterPokemon;
    }

    static {
        validSpawnMaterials.add(Material.WATER);
        validAirMaterials = new HashSet<Material>();
        validAirMaterials.add(Material.AIR);
        validAirMaterials.add(Material.WATER);
    }
}

