/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning.spawners;

import com.pixelmongenerations.common.entity.npcs.registry.DropItemRegistry;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.drops.TotemInfo;
import com.pixelmongenerations.common.spawning.SpawnData;
import com.pixelmongenerations.common.spawning.spawners.EntityData;
import com.pixelmongenerations.common.spawning.spawners.SpawnerTimed;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class SpawnerTotem
extends SpawnerTimed {
    public SpawnerTotem() {
        super(SpawnLocation.Totem);
    }

    @Override
    protected boolean isDisabled() {
        return PixelmonConfig.maxNumTotems <= 0;
    }

    @Override
    protected int getSpawnTickBase() {
        return PixelmonConfig.totemSpawnTicks;
    }

    @Override
    protected double getSpawnTickVariance() {
        return 0.6;
    }

    @Override
    public List<SpawnData> getEntityList(String biomeID) {
        return null;
    }

    @Override
    public EntityData getRandomEntity(World world, Random rand, String biomeID, BlockPos pos, int level) {
        ArrayList<TotemInfo> totemSpawns = DropItemRegistry.getTotemPokemon();
        if (totemSpawns.isEmpty()) {
            return null;
        }
        ArrayList<TotemInfo> possibleSpawns = new ArrayList<TotemInfo>();
        if (world.getBlockState(new BlockPos(pos.getX(), this.getTopEarthBlock(world, pos, false) - 1, pos.getZ())).getMaterial() == Material.WATER) {
            for (TotemInfo t : totemSpawns) {
                if (t.spawnLocation != SpawnLocation.Water || t.dimensions != null && !t.dimensions.isEmpty() && !t.dimensions.contains(world.provider.getDimension())) continue;
                possibleSpawns.add(t);
            }
        } else {
            for (TotemInfo t : totemSpawns) {
                if (t.spawnLocation == SpawnLocation.Water || t.dimensions != null && !t.dimensions.isEmpty() && !t.dimensions.contains(world.provider.getDimension())) continue;
                possibleSpawns.add(t);
            }
        }
        if (possibleSpawns.isEmpty()) {
            return null;
        }
        TotemInfo totemInfo = (TotemInfo)RandomHelper.getRandomElementFromList(possibleSpawns);
        return new EntityData(totemInfo.pokemon.name, level, totemInfo.form);
    }

    @Override
    public void modifyPokemon(EntityLiving entity, int x, int z) {
        if (entity instanceof EntityPixelmon) {
            EntityPixelmon pokemon = (EntityPixelmon)entity;
            pokemon.setTotem(true);
        }
    }

    @Override
    public SpawnLocation getActualSpawnLocation(EntityPixelmon pokemon) {
        ArrayList<TotemInfo> totemSpawns = DropItemRegistry.getTotemPokemon();
        for (TotemInfo t : totemSpawns) {
            if (t.pokemon != pokemon.baseStats.pokemon) continue;
            return t.spawnLocation;
        }
        return null;
    }

    @Override
    public int getMaxNum() {
        return PixelmonConfig.maxNumTotems;
    }
}

