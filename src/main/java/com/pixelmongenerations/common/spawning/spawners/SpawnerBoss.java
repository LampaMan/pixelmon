/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning.spawners;

import com.pixelmongenerations.common.entity.npcs.registry.DropItemRegistry;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.drops.BossInfo;
import com.pixelmongenerations.common.spawning.SpawnData;
import com.pixelmongenerations.common.spawning.spawners.EntityData;
import com.pixelmongenerations.common.spawning.spawners.SpawnerTimed;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class SpawnerBoss
extends SpawnerTimed {
    public SpawnerBoss() {
        super(SpawnLocation.Boss);
    }

    @Override
    protected boolean isDisabled() {
        return PixelmonConfig.maxNumBosses <= 0;
    }

    @Override
    protected int getSpawnTickBase() {
        return PixelmonConfig.bossSpawnTicks;
    }

    @Override
    protected double getSpawnTickVariance() {
        return 0.6;
    }

    @Override
    public List<SpawnData> getEntityList(String biomeID) {
        return null;
    }

    @Override
    public EntityData getRandomEntity(World world, Random rand, String biomeID, BlockPos pos, int level) {
        ArrayList<BossInfo> bossSpawns = DropItemRegistry.getBossPokemon();
        if (bossSpawns.isEmpty()) {
            return null;
        }
        ArrayList<String> possibleSpawns = new ArrayList<String>();
        if (world.getBlockState(new BlockPos(pos.getX(), this.getTopEarthBlock(world, pos, false) - 1, pos.getZ())).getMaterial() == Material.WATER) {
            for (BossInfo b : bossSpawns) {
                if (b.spawnLocation != SpawnLocation.Water || b.dimensions != null && !b.dimensions.isEmpty() && !b.dimensions.contains(world.provider.getDimension())) continue;
                possibleSpawns.add(b.pokemon.name);
            }
        } else {
            for (BossInfo b : bossSpawns) {
                if (b.spawnLocation == SpawnLocation.Water || b.dimensions != null && !b.dimensions.isEmpty() && !b.dimensions.contains(world.provider.getDimension())) continue;
                possibleSpawns.add(b.pokemon.name);
            }
        }
        if (possibleSpawns.isEmpty()) {
            return null;
        }
        return new EntityData((String)RandomHelper.getRandomElementFromList(possibleSpawns), level);
    }

    @Override
    public void modifyPokemon(EntityLiving pokemon, int x, int z) {
        if (pokemon instanceof EntityPixelmon) {
            ((EntityPixelmon)pokemon).setBoss(EnumBossMode.getRandomMode());
        }
    }

    @Override
    public SpawnLocation getActualSpawnLocation(EntityPixelmon pokemon) {
        ArrayList<BossInfo> bossSpawns = DropItemRegistry.getBossPokemon();
        for (BossInfo b : bossSpawns) {
            if (b.pokemon != pokemon.baseStats.pokemon) continue;
            return b.spawnLocation;
        }
        return null;
    }

    @Override
    public int getMaxNum() {
        return PixelmonConfig.maxNumBosses;
    }
}

