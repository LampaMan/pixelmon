/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning.spawners;

public class EntityData {
    public final int level;
    public final String name;
    public final Integer form;

    public EntityData(String name, int level) {
        this.level = level;
        this.name = name;
        this.form = null;
    }

    public EntityData(String name, int level, Integer form) {
        this.level = level;
        this.name = name;
        this.form = form;
    }
}

