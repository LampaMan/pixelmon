/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning.spawners;

import com.pixelmongenerations.core.database.SpawnLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.eventhandler.Event;

public class SpawnRequestEvent
extends Event {
    public final World world;
    public final BlockPos pos;
    public final SpawnLocation spawn;
    public boolean approved;

    public SpawnRequestEvent(World world, BlockPos pos, SpawnLocation spawn) {
        this.world = world;
        this.pos = pos;
        this.spawn = spawn;
        this.approved = true;
    }
}

