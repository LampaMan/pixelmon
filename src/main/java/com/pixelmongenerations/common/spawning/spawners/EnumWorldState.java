/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning.spawners;

import net.minecraft.util.IStringSerializable;

public enum EnumWorldState implements IStringSerializable
{
    day,
    dusk,
    dawn,
    night;


    @Override
    public String getName() {
        return this.toString();
    }

    public int getMeta() {
        return this.ordinal();
    }

    public static EnumWorldState fromMeta(int meta) {
        return EnumWorldState.values()[meta];
    }
}

