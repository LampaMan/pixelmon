/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning.spawners;

import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.Evolution;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types.LevelingEvolution;
import com.pixelmongenerations.common.spawning.SpawnData;
import com.pixelmongenerations.common.spawning.WorldVariable;
import com.pixelmongenerations.common.spawning.spawners.EntityData;
import com.pixelmongenerations.common.spawning.spawners.EnumWorldState;
import com.pixelmongenerations.common.spawning.spawners.SpawnRequestEvent;
import com.pixelmongenerations.common.spawning.spawners.SpawnerNPC;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import net.minecraft.block.Block;
import net.minecraft.block.BlockBush;
import net.minecraft.block.BlockFlower;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLiving;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.fml.common.eventhandler.EventBus;

public abstract class SpawnerBase {
    public static final EventBus SPAWN_DENIAL_BUS = new EventBus();
    public SpawnLocation spawnLocation;
    public WorldVariable<Integer> count = new WorldVariable<Integer>(0);
    private static Set<Material> validFloorMaterials = new HashSet<Material>();
    private static Set<Material> validAirMaterials;

    public SpawnerBase(SpawnLocation spawnLocation) {
        this.spawnLocation = spawnLocation;
    }

    public abstract int getMaxNum();

    public static void register(Object obj) {
        SPAWN_DENIAL_BUS.register(obj);
    }

    protected boolean requestSpawn(World world, BlockPos pos) {
        SpawnRequestEvent event = new SpawnRequestEvent(world, pos, this.spawnLocation);
        SPAWN_DENIAL_BUS.post(event);
        return event.approved;
    }

    public abstract Integer getSpawnConditionY(World var1, BlockPos var2);

    public abstract List<SpawnData> getEntityList(String var1);

    public final String getRandomEntity(World world, Random rand, String biomeID, BlockPos pos) {
        EntityData data = this.getRandomEntity(world, rand, biomeID, pos, -1);
        return data == null ? "" : data.name;
    }

    public EntityData getRandomEntity(World world, Random rand, String biomeID, BlockPos pos, int level) {
        List<SpawnData> spawnData = this.getEntityList(biomeID);
        if (spawnData == null || spawnData.isEmpty()) {
            return null;
        }
        if (PixelmonConfig.spawnLevelsByDistance && level >= 0) {
            if (this instanceof SpawnerNPC) {
                return new EntityData(SpawnerBase.getWeightedEntryFromList(world, spawnData), level);
            }
            if (level > 5) {
                float randNum = rand.nextFloat();
                if (randNum > 0.5f) {
                    level = (int)((randNum - 0.5f) / 0.5f * (float)(level - 5) + 5.0f);
                }
            }
            for (int i = 0; i < 3; ++i) {
                String testSpawn = SpawnerBase.getWeightedEntryFromList(world, spawnData);
                Optional<BaseStats> stats = Entity3HasStats.getBaseStats(testSpawn);
                if (!stats.isPresent()) continue;
                int slevel = 0;
                int maxLevel = PixelmonServerConfig.maxLevel;
                for (EnumSpecies enumSpecies : stats.get().preEvolutions) {
                    Optional<BaseStats> evostats = Entity3HasStats.getBaseStats(enumSpecies.name);
                    if (!evostats.isPresent()) continue;
                    for (Evolution evo : evostats.get().evolutions) {
                        if (evo == null || !(evo instanceof LevelingEvolution) || !evo.to.name.equals(stats.get().pokemon.name)) continue;
                        slevel = ((LevelingEvolution)evo).level;
                    }
                }
                for (Evolution evolution : stats.get().evolutions) {
                    if (evolution == null || !(evolution instanceof LevelingEvolution)) continue;
                    maxLevel = ((LevelingEvolution)evolution).level + 2;
                }
                if (level < slevel || level > maxLevel) continue;
                return new EntityData(testSpawn, level);
            }
            return null;
        }
        return new EntityData(SpawnerBase.getWeightedEntryFromList(world, spawnData), level);
    }

    public boolean canPokemonSpawnHere(World par1World, BlockPos pos) {
        if (!this.requestSpawn(par1World, pos)) {
            return false;
        }
        return this.canPokemonSpawnHereImpl(par1World, pos);
    }

    public float getYOffset(float x, float y, float z, EntityLiving pokemon) {
        return 0.0f;
    }

    public static EnumWorldState getWorldState(World world) {
        long time = world.getWorldTime() % 24000L;
        EnumWorldState state = time >= 22500L || time < 1000L ? EnumWorldState.dawn : (time >= 1000L && time < 11000L ? EnumWorldState.day : (time >= 11000L && time < 13500L ? EnumWorldState.dusk : EnumWorldState.night));
        return state;
    }

    public static String getWeightedEntryFromList(World world, List<SpawnData> spawnList) {
        EnumWorldState state = SpawnerBase.getWorldState(world);
        ArrayList<Integer> rarities = new ArrayList<Integer>();
        for (SpawnData aSpawnList1 : spawnList) {
            rarities.add(aSpawnList1.getRarity(state));
        }
        int spawnIndex = RandomHelper.getRandomIndexFromWeights(rarities);
        if (spawnIndex != -1) {
            return spawnList.get((int)spawnIndex).name;
        }
        return null;
    }

    protected abstract boolean canPokemonSpawnHereImpl(World var1, BlockPos var2);

    public boolean isBlockValidForPixelmonSpawning(World world, BlockPos pos) {
        IBlockState groundBlock = world.getBlockState(pos.down());
        Material spawnFloorGroundMaterial = groundBlock.getMaterial();
        Material[] spawnAirMaterial = new Material[]{world.getBlockState(pos).getMaterial(), world.getBlockState(pos.up()).getMaterial()};
        Set<Material> validFloorMaterials = this.getSpawnCheckMaterials();
        Set<Material> validAirMaterials = this.getValidSpawnAirMaterials();
        return validFloorMaterials.contains(spawnFloorGroundMaterial) && validAirMaterials.contains(spawnAirMaterial[0]) && validAirMaterials.contains(spawnAirMaterial[1]);
    }

    public Set<Material> getSpawnCheckMaterials() {
        return validFloorMaterials;
    }

    public Set<Material> getValidGroundMaterials() {
        return validFloorMaterials;
    }

    public Set<Material> getValidSpawnAirMaterials() {
        return validAirMaterials;
    }

    public Integer getTopEarthBlock(World world, BlockPos pos, boolean ignoreWater) {
        Chunk chunk = world.getChunk(pos);
        Integer k = null;
        BlockPos.MutableBlockPos p = new BlockPos.MutableBlockPos();
        k = Math.max(0, Math.min(chunk.getTopFilledSegment() + 15, 255));
        while (k > 0) {
            p.setPos(pos.getX(), k, pos.getZ());
            IBlockState state = world.getBlockState(p);
            Block block = state.getBlock();
            if (block != Blocks.AIR && (this.getValidGroundMaterials().contains(state.getMaterial()) || !ignoreWater && state.getMaterial() == Material.WATER) && !block.isFoliage(world, p)) {
                return k + 1;
            }
            k = k - 1;
        }
        return null;
    }

    public Integer getNextTopEarthBlock(World world, BlockPos pos, boolean ignoreWater) {
        Chunk chunk = world.getChunk(pos);
        Integer k = pos.getY() - 1;
        BlockPos.MutableBlockPos p = new BlockPos.MutableBlockPos();
        Integer last = null;
        while (k < pos.getY() + 10) {
            p.setPos(pos.getX(), k, pos.getZ());
            IBlockState state = world.getBlockState(p);
            Block block = state.getBlock();
            if (SpawnerBase.isSpawnableBlock(world.getBlockState(p.up()).getBlock()) && block != Blocks.AIR && (this.getValidGroundMaterials().contains(state.getMaterial()) || !ignoreWater && state.getMaterial() == Material.WATER) && !state.getBlock().isFoliage(world, p)) {
                return k + 1;
            }
            k = k + 1;
        }
        return last != null ? last : null;
    }

    public static boolean isSpawnableBlock(Block block) {
        return block == Blocks.AIR || block == Blocks.TALLGRASS || block == Blocks.DOUBLE_PLANT || block == Blocks.SNOW_LAYER || block instanceof BlockFlower || block instanceof BlockBush;
    }

    public Integer getTopWaterBlock(World world, BlockPos pos) {
        Chunk chunk = world.getChunk(pos);
        Integer k = null;
        BlockPos.MutableBlockPos p = new BlockPos.MutableBlockPos();
        k = Math.max(0, Math.min(chunk.getTopFilledSegment() + 15, 255));
        while (k > 0) {
            p.setPos(pos.getX(), k, pos.getZ());
            IBlockState state = world.getBlockState(p);
            Block block = state.getBlock();
            if (block != Blocks.AIR && state.getMaterial() == Material.WATER) {
                return k;
            }
            k = k - 1;
        }
        return null;
    }

    public void modifyPokemon(EntityLiving pokemon, int x, int z) {
    }

    public SpawnLocation getActualSpawnLocation(EntityPixelmon pokemon) {
        return this.spawnLocation;
    }

    public boolean shouldSpawnInThisChunk(int chunkCount) {
        return true;
    }

    static {
        validFloorMaterials.add(Material.GRASS);
        validFloorMaterials.add(Material.GROUND);
        validFloorMaterials.add(Material.ROCK);
        validFloorMaterials.add(Material.SAND);
        validFloorMaterials.add(Material.ICE);
        validFloorMaterials.add(Material.CRAFTED_SNOW);
        validFloorMaterials.add(Material.PACKED_ICE);
        validAirMaterials = new HashSet<Material>();
        validAirMaterials.add(Material.AIR);
    }
}

