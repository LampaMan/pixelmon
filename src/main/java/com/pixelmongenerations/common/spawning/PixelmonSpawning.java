/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning;

import com.pixelmongenerations.api.spawning.AbstractSpawner;
import com.pixelmongenerations.api.spawning.SpawnInfo;
import com.pixelmongenerations.api.spawning.SpawnSet;
import com.pixelmongenerations.api.spawning.SpawnerCoordinator;
import com.pixelmongenerations.api.spawning.archetypes.spawners.TriggerSpawner;
import com.pixelmongenerations.api.spawning.conditions.LocationType;
import com.pixelmongenerations.api.spawning.util.SetLoader;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.RockSmash;
import com.pixelmongenerations.common.spawning.PixelmonSpawner;
import com.pixelmongenerations.common.spawning.TrackingSpawnerCoordinator;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import java.io.File;
import java.util.ArrayList;

public class PixelmonSpawning {
    public static SpawnerCoordinator coordinator = null;
    public static ArrayList<SpawnSet> standard = new ArrayList();
    public static TriggerSpawner fishingSpawner = null;

    public static void startTrackingSpawner() {
        if (!PixelmonSpawner.spawners.contains(PixelmonSpawner.spawnerNPC)) {
            PixelmonSpawner.spawners.add(PixelmonSpawner.spawnerNPC);
        }
        if (!PixelmonSpawner.spawners.contains(PixelmonSpawner.spawnerBoss)) {
            PixelmonSpawner.spawners.add(PixelmonSpawner.spawnerBoss);
        }
        if (!PixelmonSpawner.spawners.contains(PixelmonSpawner.spawnerTotem)) {
            PixelmonSpawner.spawners.add(PixelmonSpawner.spawnerTotem);
        }
        if (coordinator != null && coordinator.getActive()) {
            coordinator.deactivate();
        }
        if (coordinator == null || !coordinator.getActive()) {
            coordinator = new TrackingSpawnerCoordinator().activate();
        }
    }

    public static void registerSpawnSets() {
        Pixelmon.LOGGER.info("Registering spawn sets.");
        if (PixelmonConfig.useExternalJSONFilesSpawning) {
            if ("default".equals(PixelmonConfig.spawnSetFolder)) {
                File spawnSetDir = new File("pixelmon/spawning/default");
                if (!spawnSetDir.isDirectory()) {
                    Pixelmon.LOGGER.info("Creating spawning directory");
                    spawnSetDir.mkdirs();
                }
                SetLoader.checkForMissingSpawnSets();
            }
            standard = SetLoader.importSetsFrom("pixelmon/spawning/" + PixelmonConfig.spawnSetFolder + "/");
        } else {
            ArrayList<SpawnSet> internalSets = SetLoader.retrieveSpawnSetsFromAssets();
            standard.addAll(internalSets);
        }
        TrackingSpawnerCoordinator.spawnerPreset.setSpawnSets(standard);
        SpawnSet rockSmashSet = new SpawnSet();
        rockSmashSet.id = "Rock Smash";
        SpawnSet fishingSet = new SpawnSet();
        fishingSet.id = "Fishing";
        SpawnSet lavaFishingSet = new SpawnSet();
        lavaFishingSet.id = "Lava Fishing";
        for (SpawnSet spawnSet : standard) {
            for (SpawnInfo spawnInfo : spawnSet.spawnInfos) {
                if (spawnInfo.locationTypes.contains(LocationType.ROCK_SMASH)) {
                    rockSmashSet.spawnInfos.add(spawnInfo);
                }
                if (spawnInfo.locationTypes.contains(LocationType.OLD_ROD) || spawnInfo.locationTypes.contains(LocationType.GOOD_ROD) || spawnInfo.locationTypes.contains(LocationType.SUPER_ROD)) {
                    fishingSet.spawnInfos.add(spawnInfo);
                }
                if (!spawnInfo.locationTypes.contains(LocationType.OLD_ROD_LAVA) && !spawnInfo.locationTypes.contains(LocationType.GOOD_ROD_LAVA) && !spawnInfo.locationTypes.contains(LocationType.SUPER_ROD_LAVA)) continue;
                lavaFishingSet.spawnInfos.add(spawnInfo);
            }
        }
        RockSmash.ROCK_SMASH_SPAWNER = (TriggerSpawner) new AbstractSpawner.SpawnerBuilder().setSpawnSets(rockSmashSet).apply(new TriggerSpawner());
        fishingSpawner = (TriggerSpawner) new AbstractSpawner.SpawnerBuilder().setSpawnSets(fishingSet, lavaFishingSet).apply(new TriggerSpawner());
    }
}

