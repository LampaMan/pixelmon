/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning;

import com.pixelmongenerations.api.events.PixelmonSpawnEvent;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.spawning.spawners.EntityData;
import com.pixelmongenerations.common.spawning.spawners.SpawnerBase;
import com.pixelmongenerations.common.spawning.spawners.SpawnerBoss;
import com.pixelmongenerations.common.spawning.spawners.SpawnerNPC;
import com.pixelmongenerations.common.spawning.spawners.SpawnerTotem;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.database.SpawnLocation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeEventFactory;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;

public class PixelmonSpawner {
    public static final ArrayList<SpawnerBase> spawners = new ArrayList();
    public static final SpawnerNPC spawnerNPC = new SpawnerNPC();
    public static final SpawnerBoss spawnerBoss = new SpawnerBoss();
    public static final SpawnerTotem spawnerTotem = new SpawnerTotem();
    private static final Set<ChunkPos> eligibleChunksForSpawning = new HashSet<ChunkPos>(324);
    private HashMap<Integer, Integer> spawnerTickArray = new HashMap();

    private static BlockPos getRandomSpawningPointInChunk(World world, int x, int z) {
        int xCoord = x * 16 + world.rand.nextInt(16);
        int zCoord = z * 16 + world.rand.nextInt(16);
        return new BlockPos(xCoord, 60, zCoord);
    }

    private static void findChunksForSpawning(WorldServer world) {
        eligibleChunksForSpawning.clear();
        for (EntityPlayer player : world.playerEntities) {
            if (player.isSpectator()) continue;
            for (int j = -PixelmonConfig.chunkSpawnRadius; j <= PixelmonConfig.chunkSpawnRadius; ++j) {
                for (int k = -PixelmonConfig.chunkSpawnRadius; k <= PixelmonConfig.chunkSpawnRadius; ++k) {
                    boolean isBorder = j == -PixelmonConfig.chunkSpawnRadius || j == PixelmonConfig.chunkSpawnRadius || k == -PixelmonConfig.chunkSpawnRadius || k == PixelmonConfig.chunkSpawnRadius;
                    ChunkPos cc = new ChunkPos(j + player.chunkCoordX, k + player.chunkCoordZ);
                    if (eligibleChunksForSpawning.contains(cc) || world.getWorldBorder() == null || !world.getChunkProvider().chunkExists(cc.x, cc.z) || isBorder || !world.getWorldBorder().contains(cc)) continue;
                    eligibleChunksForSpawning.add(cc);
                }
            }
        }
    }

    private static void doSpawning(WorldServer world) {
        if (PixelmonSpawner.countEntities(world)) {
            return;
        }
        int leftoverSpawns = PixelmonConfig.maxSpawnsPerTick;
        BlockPos chunkCoords = world.getSpawnPoint();
        ArrayList<ChunkPos> tmp = new ArrayList<ChunkPos>(eligibleChunksForSpawning);
        Collections.shuffle(tmp);
        for (ChunkPos ccIntPair : tmp) {
            BlockPos chunkPos = PixelmonSpawner.getRandomSpawningPointInChunk(world, ccIntPair.x, ccIntPair.z);
            Chunk c = world.getChunk(ccIntPair.x, ccIntPair.z);
            if (PixelmonSpawner.countEntitiesInChunk(c, EntityPixelmon.class) > 4) continue;
            int cpX = chunkPos.getX();
            int cpZ = chunkPos.getZ();
            for (SpawnerBase s : spawners) {
                Integer cpY;
                int maxSpawns = s.getMaxNum();
                if (maxSpawns <= 0 || s.count.get(world) > maxSpawns * eligibleChunksForSpawning.size() / 256 || !s.shouldSpawnInThisChunk(eligibleChunksForSpawning.size()) || (cpY = s.getSpawnConditionY(world, new BlockPos(cpX, 0, cpZ))) == null) continue;
                PixelmonSpawnEvent spawnEvent = new PixelmonSpawnEvent(world, cpX, cpY, cpZ);
                MinecraftForge.EVENT_BUS.post(spawnEvent);
                if (spawnEvent.isCanceled()) continue;
                int spawned = PixelmonSpawner.doSpawn(s, s.spawnLocation, world, cpX, cpY, cpZ, chunkCoords);
                s.count.set(world, s.count.get(world) + spawned);
                if ((leftoverSpawns -= spawned) >= 0) continue;
                return;
            }
        }
    }

    private static int countEntitiesInChunk(Chunk chunk, Class<? extends Entity> entityClass) {
        int entityCount = 0;
        for (int i = 0; i < chunk.getEntityLists().length; ++i) {
            for (Entity entity : chunk.getEntityLists()[i]) {
                if (!entityClass.isAssignableFrom(entity.getClass())) continue;
                ++entityCount;
            }
        }
        return entityCount;
    }

    private static boolean countEntities(WorldServer world) {
        for (SpawnerBase s : spawners) {
            s.count.set(world, 0);
        }
        int[] counts = new int[SpawnLocation.values().length];
        for (int i = 0; i < world.loadedEntityList.size(); ++i) {
            int n;
            int[] arrn;
            Entity entity = (Entity)world.loadedEntityList.get(i);
            if (entity instanceof EntityPixelmon) {
                EntityPixelmon pixelmon = (EntityPixelmon)entity;
                if (pixelmon.getSpawnLocation() == null) continue;
                arrn = counts;
                n = pixelmon.getSpawnLocation().ordinal();
                arrn[n] = arrn[n] + 1;
                continue;
            }
            if (!(entity instanceof EntityNPC)) continue;
            EntityNPC npc = (EntityNPC)entity;
            if (npc.npcLocation == null) continue;
            arrn = counts;
            n = npc.npcLocation.ordinal();
            arrn[n] = arrn[n] + 1;
        }
        for (SpawnerBase spawner : spawners) {
            spawner.count.set(world, counts[spawner.spawnLocation.ordinal()]);
        }
        for (SpawnerBase s : spawners) {
            if (s.getMaxNum() <= 0 || s.count.get(world) >= s.getMaxNum() * eligibleChunksForSpawning.size() / 256) continue;
            return false;
        }
        return true;
    }

    private static int doSpawn(SpawnerBase s, SpawnLocation spawnLocation, World world, int cpX, int cpY, int cpZ, BlockPos pos) {
        int numInChunk = 0;
        block2: for (int count = 0; count < 3; ++count) {
            int cpXtmp = cpX;
            int cpYtmp = cpY;
            int cpZtmp = cpZ;
            int rndmMax = 6;
            String entityName = null;
            Integer form = null;
            int maxInChunk = -1;
            for (int count2 = 0; count2 < 4; ++count2) {
                EntityLiving entity;
                double distance;
                float z;
                float y;
                float x;
                if (!s.canPokemonSpawnHere(world, new BlockPos(cpXtmp += world.rand.nextInt(rndmMax) - world.rand.nextInt(rndmMax), cpYtmp += world.rand.nextInt(1) - world.rand.nextInt(1), cpZtmp += world.rand.nextInt(rndmMax) - world.rand.nextInt(rndmMax))) || world.getClosestPlayer((double)(x = (float)cpXtmp + 0.5f), (double)(y = (float)cpYtmp + 0.5f), (double)(z = (float)cpZtmp + 0.5f), 24.0, false) != null) continue;
                float xd = x - (float)pos.getX();
                float yd = y - (float)pos.getY();
                float zd = z - (float)pos.getZ();
                float d = xd * xd + yd * yd + zd * zd;
                BlockPos spawnPos = new BlockPos(x, y, z);
                String biomeID = world.getBiomeForCoordsBody(spawnPos).getRegistryName().toString();
                int level = 3;
                if (d < 576.0f) continue;
                if (PixelmonConfig.spawnLevelsByDistance && (level = (int)((double)level + Math.floor((distance = Math.sqrt(pos.distanceSq(x, pos.getY(), z))) / (double)PixelmonConfig.distancePerLevel + Math.random() * 3.0))) > PixelmonConfig.maxLevelByDistance) {
                    level = PixelmonConfig.maxLevelByDistance;
                }
                if (entityName == null) {
                    EntityData data = s.getRandomEntity(world, world.rand, biomeID, spawnPos, level);
                    if (data != null) {
                        entityName = data.name;
                        level = data.level;
                        form = data.form;
                    }
                    if (entityName == null) continue block2;
                }
                try {
                    entity = PixelmonEntityList.createEntityByName(entityName, world, biomeID);
                    if (PixelmonConfig.spawnLevelsByDistance) {
                        if (entity instanceof EntityPixelmon) {
                            ((EntityPixelmon)entity).getLvl().setLevel(level);
                        } else if (entity instanceof NPCTrainer) {
                            ((NPCTrainer)entity).setLevel(level);
                        }
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                    return 0;
                }
                if (entity == null) {
                    return 0;
                }
                Event.Result canSpawn = ForgeEventFactory.canEntitySpawn(entity, world, x, y + s.getYOffset(x, y, z, entity), z, true);
                if (canSpawn != Event.Result.ALLOW && canSpawn != Event.Result.DEFAULT) continue;
                EntityPixelmon pixelmon = null;
                if (entity instanceof EntityPixelmon) {
                    pixelmon = (EntityPixelmon)entity;
                }
                if (pixelmon != null) {
                    pixelmon.setSpawnLocation(s.getActualSpawnLocation(pixelmon));
                    if (form != null) {
                        pixelmon.setForm(form);
                    }
                }
                entity.setLocationAndAngles(x, (double)y + (double)s.getYOffset(x, y, z, entity), z, world.rand.nextFloat() * 360.0f, 0.0f);
                if (entity instanceof EntityNPC) {
                    ((EntityNPC)entity).npcLocation = spawnLocation;
                }
                if (!entity.getCanSpawnHere()) continue;
                ++numInChunk;
                s.modifyPokemon(entity, cpXtmp, cpZtmp);
                AxisAlignedBB boundingBox = entity.getEntityBoundingBox();
                boolean notColliding = world.checkNoEntityCollision(boundingBox, entity) && entity.world.getCollisionBoxes(entity, boundingBox).isEmpty();
                world.spawnEntity(entity);
                if (maxInChunk == -1) {
                    maxInChunk = entity.getMaxSpawnedInChunk();
                }
                if (numInChunk < maxInChunk) continue;
                return numInChunk;
            }
        }
        return 0;
    }

    @SubscribeEvent
    public void tickEnd(TickEvent.WorldTickEvent event) {
        if (event.side == Side.SERVER && event.world.getMinecraftServer().getCanSpawnAnimals()) {
            try {
                World world = event.world;
                int dimId = world.provider.getDimension();
                if (this.allowDimension(dimId)) {
                    int spawnerTicks = -1;
                    if (this.spawnerTickArray.containsKey(dimId)) {
                        spawnerTicks = this.spawnerTickArray.get(dimId);
                    }
                    if (++spawnerTicks >= PixelmonConfig.spawnTickRate && PixelmonConfig.maxSpawnsPerTick > 0) {
                        PixelmonSpawner.findChunksForSpawning((WorldServer)world);
                        PixelmonSpawner.doSpawning((WorldServer)world);
                        spawnerTicks = 0;
                    }
                    this.spawnerTickArray.put(dimId, spawnerTicks);
                }
            }
            catch (Exception e) {
                Pixelmon.LOGGER.error("Error in spawning.");
                e.printStackTrace();
            }
        }
    }

    private boolean allowDimension(int dimensionId) {
        return PixelmonConfig.spawnDimensions.contains(dimensionId);
    }
}

