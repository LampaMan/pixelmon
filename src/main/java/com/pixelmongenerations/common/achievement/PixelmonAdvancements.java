/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.achievement;

import com.pixelmongenerations.common.achievement.triggers.BallCaptureTrigger;
import com.pixelmongenerations.common.achievement.triggers.CaptureTypeTrigger;
import com.pixelmongenerations.common.achievement.triggers.LegendaryCaptureTrigger;
import com.pixelmongenerations.common.achievement.triggers.PokedexTrigger;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.entity.player.EntityPlayerMP;

public class PixelmonAdvancements {
    public static final CaptureTypeTrigger CAPTURE_TYPE_TRIGGER = CriteriaTriggers.register(new CaptureTypeTrigger());
    public static final PokedexTrigger POKEDEX_TRIGGER = CriteriaTriggers.register(new PokedexTrigger());
    public static final BallCaptureTrigger BALL_CAPTURE_TRIGGER = CriteriaTriggers.register(new BallCaptureTrigger());
    public static final LegendaryCaptureTrigger LEGENDARY_CAPTURE_TRIGGER = CriteriaTriggers.register(new LegendaryCaptureTrigger());

    public PixelmonAdvancements() {
        Pixelmon.LOGGER.info("Loading Advancements");
    }

    public static void throwCaptureTriggers(EntityPlayerMP player, EnumPokeball ball, EntityPixelmon pokemon) {
        POKEDEX_TRIGGER.trigger(player);
        BALL_CAPTURE_TRIGGER.trigger(player, ball);
        LEGENDARY_CAPTURE_TRIGGER.trigger(player, pokemon.getSpecies());
    }
}

