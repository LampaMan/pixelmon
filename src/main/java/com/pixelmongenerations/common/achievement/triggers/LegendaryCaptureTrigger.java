/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 *  com.google.common.collect.Maps
 *  com.google.common.collect.Sets
 *  com.google.gson.JsonDeserializationContext
 *  com.google.gson.JsonObject
 */
package com.pixelmongenerations.common.achievement.triggers;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.minecraft.advancements.ICriterionTrigger;
import net.minecraft.advancements.PlayerAdvancements;
import net.minecraft.advancements.critereon.AbstractCriterionInstance;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;

public class LegendaryCaptureTrigger implements ICriterionTrigger<LegendaryCaptureTrigger.Instance> {

    private static final ResourceLocation ID = new ResourceLocation("pixelmon:legendary_capture_trigger");
    private final Map<PlayerAdvancements, Listeners> listeners = Maps.newHashMap();

    @Override
    public ResourceLocation getId() {
        return ID;
    }

    @Override
    public void addListener(PlayerAdvancements playerAdvancementsIn, ICriterionTrigger.Listener<Instance> listener) {
        Listeners legendarycapturetrigger$Listeners = this.listeners.get(playerAdvancementsIn);
        if (legendarycapturetrigger$Listeners == null) {
            legendarycapturetrigger$Listeners = new Listeners(playerAdvancementsIn);
            this.listeners.put(playerAdvancementsIn, legendarycapturetrigger$Listeners);
        }
        legendarycapturetrigger$Listeners.add(listener);
    }

    @Override
    public void removeListener(PlayerAdvancements playerAdvancementsIn, ICriterionTrigger.Listener<Instance> listener) {
        Listeners legendarycapturetrigger$Listeners = this.listeners.get(playerAdvancementsIn);
        if (legendarycapturetrigger$Listeners != null) {
            legendarycapturetrigger$Listeners.remove(listener);
            if (legendarycapturetrigger$Listeners.isEmpty()) {
                this.listeners.remove(playerAdvancementsIn);
            }
        }
    }

    @Override
    public void removeAllListeners(PlayerAdvancements playerAdvancementsIn) {
        this.listeners.remove(playerAdvancementsIn);
    }

    @Override
    public Instance deserializeInstance(JsonObject json, JsonDeserializationContext context) {
        String pokemon = json.has("pokemon") ? JsonUtils.getString(json, "pokemon") : "";
        String string = pokemon;
        if (EnumSpecies.getFromNameAnyCase(pokemon) == null && !pokemon.equalsIgnoreCase("legendary")) {
            Pixelmon.LOGGER.error("Invalid Pok\u00e9mon name for legendarycapturetrigger");
        }
        EnumSpecies pixelmon = EnumSpecies.getFromNameAnyCase(pokemon);
        return new Instance(this.getId(), pixelmon, pokemon);
    }

    public void trigger(EntityPlayerMP player, EnumSpecies pokemon) {
        Listeners legendarycapturetrigger$Listeners = this.listeners.get(player.getAdvancements());
        if (legendarycapturetrigger$Listeners != null) {
            legendarycapturetrigger$Listeners.trigger(pokemon);
        }
    }

    public static class Instance
    extends AbstractCriterionInstance {
        EnumSpecies pokemon;
        String name;

        public Instance(ResourceLocation criterionIn, EnumSpecies pokemon, String name) {
            super(criterionIn);
            this.pokemon = pokemon;
            this.name = name;
        }

        public boolean test(EnumSpecies pokemon) {
            if (this.pokemon == null && this.name.equalsIgnoreCase("legendary") && EnumSpecies.legendaries.contains(pokemon.name)) {
                return true;
            }
            return this.pokemon == pokemon;
        }
    }

    static class Listeners {
        private final PlayerAdvancements playerAdvancements;
        private final Set<ICriterionTrigger.Listener<Instance>> listeners = Sets.newHashSet();

        public Listeners(PlayerAdvancements playerAdvancementsIn) {
            this.playerAdvancements = playerAdvancementsIn;
        }

        public boolean isEmpty() {
            return this.listeners.isEmpty();
        }

        public void add(ICriterionTrigger.Listener<Instance> listener) {
            this.listeners.add(listener);
        }

        public void remove(ICriterionTrigger.Listener<Instance> listener) {
            this.listeners.remove(listener);
        }

        public void trigger(EnumSpecies pokemon) {
            List<ICriterionTrigger.Listener<Instance>> list = null;
            for (ICriterionTrigger.Listener<Instance> listener : this.listeners) {
                if (list == null) {
                    list = Lists.newArrayList();
                }
                list.add(listener);
            }
            if (list != null) {
                for (ICriterionTrigger.Listener<Instance> listener : list) {
                    if (!listener.getCriterionInstance().test(pokemon)) continue;
                    listener.grantCriterion(this.playerAdvancements);
                }
            }
        }
    }
}

