/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 *  com.google.common.collect.Maps
 *  com.google.common.collect.Sets
 *  com.google.gson.JsonDeserializationContext
 *  com.google.gson.JsonObject
 */
package com.pixelmongenerations.common.achievement.triggers;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.minecraft.advancements.ICriterionTrigger;
import net.minecraft.advancements.PlayerAdvancements;
import net.minecraft.advancements.critereon.AbstractCriterionInstance;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;

public class BallCaptureTrigger
implements ICriterionTrigger<BallCaptureTrigger.Instance> {
    private static final ResourceLocation ID = new ResourceLocation("pixelmon:ball_capture_trigger");
    private final Map<PlayerAdvancements, Listeners> listeners = Maps.newHashMap();

    @Override
    public ResourceLocation getId() {
        return ID;
    }

    @Override
    public void addListener(PlayerAdvancements playerAdvancementsIn, ICriterionTrigger.Listener<Instance> listener) {
        Listeners ballcapturetrigger$Listeners = this.listeners.get(playerAdvancementsIn);
        if (ballcapturetrigger$Listeners == null) {
            ballcapturetrigger$Listeners = new Listeners(playerAdvancementsIn);
            this.listeners.put(playerAdvancementsIn, ballcapturetrigger$Listeners);
        }
        ballcapturetrigger$Listeners.add(listener);
    }

    @Override
    public void removeListener(PlayerAdvancements playerAdvancementsIn, ICriterionTrigger.Listener<Instance> listener) {
        Listeners BallCaptureTrigger$listeners = this.listeners.get(playerAdvancementsIn);
        if (BallCaptureTrigger$listeners != null) {
            BallCaptureTrigger$listeners.remove(listener);
            if (BallCaptureTrigger$listeners.isEmpty()) {
                this.listeners.remove(playerAdvancementsIn);
            }
        }
    }

    @Override
    public void removeAllListeners(PlayerAdvancements playerAdvancementsIn) {
        this.listeners.remove(playerAdvancementsIn);
    }

    @Override
    public Instance deserializeInstance(JsonObject json, JsonDeserializationContext context) {
        String pokeball = json.has("pokeball") ? JsonUtils.getString(json, "pokeball") : "";
        String string = pokeball;
        if (EnumPokeball.getPokeballFromString(pokeball) == null) {
            Pixelmon.LOGGER.error("Invalid Pok\u00e9ball name for ballcapturetrigger");
        }
        EnumPokeball ball = EnumPokeball.getPokeballFromString(pokeball);
        return new Instance(this.getId(), ball);
    }

    public void trigger(EntityPlayerMP player, EnumPokeball pokeball) {
        Listeners ballcapturetrigger$Listeners = this.listeners.get(player.getAdvancements());
        if (ballcapturetrigger$Listeners != null) {
            ballcapturetrigger$Listeners.trigger(pokeball);
        }
    }

    public static class Instance
    extends AbstractCriterionInstance {
        private final EnumPokeball pokeball;

        public Instance(ResourceLocation criterionIn, EnumPokeball pokeball) {
            super(criterionIn);
            this.pokeball = pokeball;
        }

        public boolean test(EnumPokeball pokeball) {
            return this.pokeball == pokeball;
        }
    }

    static class Listeners {
        private final PlayerAdvancements playerAdvancements;
        private final Set<ICriterionTrigger.Listener<Instance>> listeners = Sets.newHashSet();

        public Listeners(PlayerAdvancements playerAdvancementsIn) {
            this.playerAdvancements = playerAdvancementsIn;
        }

        public boolean isEmpty() {
            return this.listeners.isEmpty();
        }

        public void add(ICriterionTrigger.Listener<Instance> listener) {
            this.listeners.add(listener);
        }

        public void remove(ICriterionTrigger.Listener<Instance> listener) {
            this.listeners.remove(listener);
        }

        public void trigger(EnumPokeball pokeball) {
            List<ICriterionTrigger.Listener<Instance>> list = null;
            for (ICriterionTrigger.Listener<Instance> listener : this.listeners) {
                if (list == null) {
                    list = Lists.newArrayList();
                }
                list.add(listener);
            }
            if (list != null) {
                for (ICriterionTrigger.Listener<Instance> listener : list) {
                    if (!listener.getCriterionInstance().test(pokeball)) continue;
                    listener.grantCriterion(this.playerAdvancements);
                }
            }
        }
    }
}

