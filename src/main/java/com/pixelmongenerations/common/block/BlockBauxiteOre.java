/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockBauxiteOre
extends Block {
    public BlockBauxiteOre(Material par2Material) {
        super(par2Material);
        this.setTranslationKey("bauxiteOre");
    }
}

