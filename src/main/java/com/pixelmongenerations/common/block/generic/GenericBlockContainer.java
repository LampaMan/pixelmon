/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.generic;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public abstract class GenericBlockContainer
extends BlockContainer {
    protected GenericBlockContainer(Material materialIn) {
        super(materialIn);
    }

    @Override
    public abstract TileEntity createNewTileEntity(World var1, int var2);
}

