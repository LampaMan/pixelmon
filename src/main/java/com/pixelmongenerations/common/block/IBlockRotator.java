/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import net.minecraft.block.Block;
import net.minecraft.util.EnumFacing;

public interface IBlockRotator {
    public int rotate(EnumFacing var1, Block var2, int var3);
}

