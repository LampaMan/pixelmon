/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.machines;

import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityCelestialAltar;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.BlockHorizontal;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class BlockCelestialAltar
extends GenericRotatableModelBlock {
    public static final PropertyDirection FACING = BlockHorizontal.FACING;

    public BlockCelestialAltar(Material material) {
        super(material);
        this.setTickRandomly(true);
        this.setBlockUnbreakable();
        this.setResistance(6000000.0f);
        this.setTranslationKey("celestial_altar");
        this.setDefaultState(this.blockState.getBaseState());
        this.setCreativeTab(PixelmonCreativeTabs.shrines);
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer((Block)this, FACING);
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState().withProperty(FACING, EnumFacing.byHorizontalIndex((int)(meta & 3)));
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        int b0 = 0;
        return b0 | state.getValue(FACING).getHorizontalIndex();
    }

    @Override
    public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer) {
        IBlockState iblockstate = this.getDefaultState();
        if (facing.getAxis().isHorizontal()) {
            iblockstate = iblockstate.withProperty(FACING, facing);
        }
        return iblockstate;
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        return new ItemStack(Item.getItemFromBlock(PixelmonBlocks.celestialAltar));
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return null;
    }

    @Override
    public int quantityDropped(Random random) {
        return 0;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public TileEntity createNewTileEntity(World world, int var1) {
        return new TileEntityCelestialAltar();
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        ItemStack heldItem = player.getHeldItem(hand);
        if (world.isRemote || hand == EnumHand.OFF_HAND) {
            return true;
        }
        TileEntityCelestialAltar tile = BlockHelper.getTileEntity(TileEntityCelestialAltar.class, world, pos);
        if (tile != null) {
            tile.activate(player, state, heldItem);
        }
        return player.isCreative();
    }
}

