/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.block.machines;

import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityCookingPot;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.enums.EnumGui;
import java.util.Random;
import javax.annotation.Nullable;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.NonNullList;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockCookingPot
extends GenericRotatableModelBlock {
    protected static final AxisAlignedBB[] TRANSMUTATION_CIRCLE_AABB = new AxisAlignedBB[]{new AxisAlignedBB(0.125, 0.0, 0.125, 0.875, 0.0, 0.875)};

    public BlockCookingPot() {
        super(Material.ROCK);
        this.setHardness(2.5f);
        this.setTranslationKey("cookingblock");
        this.setTickRandomly(true);
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        return new ItemStack(PixelmonBlocks.cookingPot);
    }

    @Override
    public int damageDropped(IBlockState state) {
        return this.getMetaFromState(state);
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return true;
    }

    @Override
    public void getDrops(NonNullList<ItemStack> drops, IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
        drops.add(new ItemStack(PixelmonBlocks.cookingPot));
    }

    @Override
    public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
        TileEntity tileentity = worldIn.getTileEntity(pos);
        if (tileentity instanceof TileEntityCookingPot) {
            int x = pos.getX();
            int y = pos.getY();
            int z = pos.getZ();
            for (ItemStack drop : ((TileEntityCookingPot)tileentity).getDrops()) {
                InventoryHelper.spawnItemStack(worldIn, x, y, z, drop);
            }
        }
        super.breakBlock(worldIn, pos, state);
    }

    @Override
    public boolean canPlaceBlockAt(World worldIn, BlockPos pos) {
        IBlockState downState = worldIn.getBlockState(pos.down());
        return downState.isTopSolid() || downState.getBlockFaceShape(worldIn, pos.down(), EnumFacing.UP) == BlockFaceShape.SOLID || worldIn.getBlockState(pos.down()).getBlock() == Blocks.GLOWSTONE;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.CUTOUT;
    }

    @Override
    public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos) {
        return state;
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return null;
    }

    @Override
    public boolean hasTileEntity(IBlockState state) {
        return true;
    }

    @Override
    public TileEntity createTileEntity(World world, IBlockState state) {
        return new TileEntityCookingPot();
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public void randomDisplayTick(IBlockState stateIn, World worldIn, BlockPos pos, Random rand) {
        TileEntityCookingPot potTE = BlockCookingPot.getTileEntity(TileEntityCookingPot.class, worldIn, pos);
        if (potTE.isCooking()) {
            double d0 = (double)pos.getX() + 0.5;
            double d1 = (double)pos.getY() + rand.nextDouble() * 6.0 / 16.0;
            double d2 = (double)pos.getZ() + 0.5;
            double d3 = 0.52;
            double d4 = rand.nextDouble() * 0.6 - 0.3;
            if (rand.nextDouble() < 0.1) {
                worldIn.playSound((double)pos.getX() + 0.5, (double)pos.getY(), (double)pos.getZ() + 0.5, SoundEvents.BLOCK_FURNACE_FIRE_CRACKLE, SoundCategory.BLOCKS, 1.0f, 1.0f, false);
            }
            worldIn.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, d0, d1, d2 + d4, 0.0, 0.0, 0.0, new int[0]);
            worldIn.spawnParticle(EnumParticleTypes.FLAME, d0, d1, d2 + d4, 0.0, 0.0, 0.0, new int[0]);
        }
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (!world.isRemote) {
            player.openGui(Pixelmon.INSTANCE, EnumGui.CookingPot.getIndex(), world, pos.getX(), pos.getY(), pos.getZ());
        }
        return true;
    }

    @Override
    @Nullable
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos) {
        return NULL_AABB;
    }

    @Override
    public boolean isPassable(IBlockAccess worldIn, BlockPos pos) {
        return true;
    }

    public static <T extends TileEntity> T getTileEntity(Class<T> clazz, IBlockAccess world, BlockPos pos) {
        TileEntity te = world.getTileEntity(pos);
        if (te != null && !clazz.isInstance(te)) {
            te.invalidate();
            te = world.getTileEntity(pos);
            if (te != null && !clazz.isInstance(te)) {
                te.invalidate();
                return null;
            }
            if (te != null && world instanceof WorldServer) {
                te.markDirty();
                ((WorldServer)world).getPlayerChunkMap().markBlockForUpdate(pos);
            }
        }
        try {
            return (T)te;
        }
        catch (Exception e) {
            te.invalidate();
            return null;
        }
    }
}

