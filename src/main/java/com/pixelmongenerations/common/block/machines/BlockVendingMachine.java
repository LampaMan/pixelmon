/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.machines;

import com.pixelmongenerations.common.block.enums.ColorEnum;
import com.pixelmongenerations.common.block.machines.VendingMachineShop;
import com.pixelmongenerations.common.block.multiBlocks.BlockGenericModelMultiblock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityVendingMachine;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.network.packetHandlers.vendingMachine.SetVendingMachineData;
import java.util.Optional;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class BlockVendingMachine
extends BlockGenericModelMultiblock {
    private final ColorEnum color;
    VendingMachineShop shop;

    public BlockVendingMachine(ColorEnum color) {
        super(Material.IRON, 1, 2.0, 1);
        this.setHardness(2.0f);
        this.setSoundType(SoundType.METAL);
        this.setTranslationKey((Object)((Object)color) + "VendingMachine");
        this.color = color;
    }

    @Override
    protected Optional<TileEntity> getTileEntity(World world, IBlockState state) {
        return Optional.of(new TileEntityVendingMachine());
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        return new ItemStack(this.getDroppedItem(world, pos));
    }

    @Override
    public Item getDroppedItem(World world, BlockPos pos) {
        switch (this.color) {
            case Blue: {
                return Item.getItemFromBlock(PixelmonBlocks.blueVendingMachineBlock);
            }
            case Green: {
                return Item.getItemFromBlock(PixelmonBlocks.greenVendingMachineBlock);
            }
            case Orange: {
                return Item.getItemFromBlock(PixelmonBlocks.orangeVendingMachineBlock);
            }
            case Pink: {
                return Item.getItemFromBlock(PixelmonBlocks.pinkVendingMachineBlock);
            }
            case Red: {
                return Item.getItemFromBlock(PixelmonBlocks.redVendingMachineBlock);
            }
            case Purple: {
                return Item.getItemFromBlock(PixelmonBlocks.purpleVendingMachineBlock);
            }
            case Yellow: {
                return Item.getItemFromBlock(PixelmonBlocks.yellowVendingMachineBlock);
            }
            case Black: {
                return Item.getItemFromBlock(PixelmonBlocks.blackVendingMachineBlock);
            }
            case Brown: {
                return Item.getItemFromBlock(PixelmonBlocks.brownVendingMachineBlock);
            }
            case Cyan: {
                return Item.getItemFromBlock(PixelmonBlocks.cyanVendingMachineBlock);
            }
            case Grey: {
                return Item.getItemFromBlock(PixelmonBlocks.greyVendingMachineBlock);
            }
            case LightBlue: {
                return Item.getItemFromBlock(PixelmonBlocks.lightBlueVendingMachineBlock);
            }
            case LightGrey: {
                return Item.getItemFromBlock(PixelmonBlocks.lightGreyVendingMachineBlock);
            }
            case Lime: {
                return Item.getItemFromBlock(PixelmonBlocks.limeVendingMachineBlock);
            }
            case Magenta: {
                return Item.getItemFromBlock(PixelmonBlocks.magentaVendingMachineBlock);
            }
            case White: {
                return Item.getItemFromBlock(PixelmonBlocks.whiteVendingMachineBlock);
            }
        }
        return null;
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (!world.isRemote && hand != EnumHand.OFF_HAND) {
            Pixelmon.NETWORK.sendTo(new SetVendingMachineData(this.getShop().getItems()), (EntityPlayerMP)player);
            player.openGui(Pixelmon.INSTANCE, EnumGui.VendingMachine.getIndex(), world, pos.getX(), pos.getY(), pos.getZ());
            return true;
        }
        return true;
    }

    public VendingMachineShop getShop() {
        if (this.shop == null) {
            this.shop = new VendingMachineShop();
        }
        return this.shop;
    }

    public ColorEnum getColor() {
        return this.color;
    }
}

