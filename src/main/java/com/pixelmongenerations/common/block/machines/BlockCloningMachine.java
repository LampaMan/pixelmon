/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.machines;

import com.pixelmongenerations.common.block.MultiBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityCloningMachine;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import java.util.Optional;
import java.util.Random;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockCloningMachine
extends MultiBlock {
    public BlockCloningMachine() {
        super(Material.IRON, 5, 3.0, 1);
        this.setHardness(1.0f);
        this.setTranslationKey("cloningMachine");
    }

    @Override
    public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state) {
        return ItemStack.EMPTY;
    }

    @Override
    public int quantityDropped(Random random) {
        return 1;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.INVISIBLE;
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (!world.isRemote || hand == EnumHand.OFF_HAND) {
            ItemStack heldItem = player.getHeldItem(hand);
            BlockPos loc = this.findBaseBlock(world, new BlockPos.MutableBlockPos(pos), state);
            TileEntityCloningMachine te = BlockHelper.getTileEntity(TileEntityCloningMachine.class, world, loc);
            if (te != null && player.getHeldItemMainhand().getItem() != PixelmonItems.chisel) {
                te.activate(player, heldItem);
            }
        }
        return true;
    }

    @Override
    public Item getDroppedItem(World world, BlockPos pos) {
        BlockPos loc = this.findBaseBlock(world, new BlockPos.MutableBlockPos(pos), world.getBlockState(pos));
        TileEntityCloningMachine te = BlockHelper.getTileEntity(TileEntityCloningMachine.class, world, loc);
        if (te == null || te.isBroken) {
            return null;
        }
        return Item.getItemFromBlock(PixelmonBlocks.cloningMachine);
    }

    @Override
    protected Optional<TileEntity> getTileEntity(World world, IBlockState state) {
        return Optional.of(new TileEntityCloningMachine());
    }
}

