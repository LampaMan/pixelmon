/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.machines;

import com.pixelmongenerations.core.enums.EnumSpecies;

public class PokemonRarity {
    public EnumSpecies pokemon;
    public int rarity;
    public int form;
    public int texture;
    public int shinyChance;

    public PokemonRarity(EnumSpecies pokemon, int rarity, int form, int texture, int shinyChance) {
        this.pokemon = pokemon;
        this.rarity = rarity;
        this.form = form;
        this.texture = texture;
        this.shinyChance = shinyChance;
    }
}

