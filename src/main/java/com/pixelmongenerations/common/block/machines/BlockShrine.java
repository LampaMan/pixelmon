/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.machines;

import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import com.pixelmongenerations.common.item.IShrineItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import java.util.Random;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public abstract class BlockShrine
extends GenericRotatableModelBlock {
    public BlockShrine() {
        super(Material.ROCK);
        this.setTickRandomly(true);
        this.setBlockUnbreakable();
        this.setResistance(6000000.0f);
        this.setCreativeTab(PixelmonCreativeTabs.shrines);
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        return new ItemStack(Item.getItemFromBlock(this));
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return null;
    }

    @Override
    public int quantityDropped(Random random) {
        return 0;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta) {
        return this.createTileEntity();
    }

    public abstract TileEntityShrine createTileEntity();

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.ENTITYBLOCK_ANIMATED;
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        ItemStack heldItem = player.getHeldItem(hand);
        if (world.isRemote || hand == EnumHand.OFF_HAND) {
            return true;
        }
        TileEntityShrine tile = BlockHelper.getTileEntity(TileEntityShrine.class, world, pos);
        if (tile != null) {
            if (player.isSneaking()) {
                ChatHandler.sendChat(player, tile.getRightClickMessage(heldItem, state), new Object[0]);
            }
            if (heldItem.getItem() instanceof IShrineItem) {
                if (this.canAccept(((IShrineItem)((Object)heldItem.getItem())).getGroup())) {
                    if (PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player).get().countAblePokemon() >= 1) {
                        if ((((IShrineItem)((Object)heldItem.getItem())).shouldCheckDamage() || heldItem.getItemDamage() >= heldItem.getMaxDamage()) && !tile.isSpawning() && this.checkExtraRequirements(player, world, pos)) {
                            tile.activate(player, world, this, state, heldItem);
                        }
                    } else {
                        player.sendMessage(new TextComponentString("Can't activate shrine due to team being unable to battle."));
                    }
                }
            } else {
                tile.activateOther(player, world, this, state, heldItem);
            }
            this.updateRedstoneOutput(world, pos);
            world.updateComparatorOutputLevel(pos, this);
        }
        return true;
    }

    protected abstract boolean checkExtraRequirements(EntityPlayer var1, World var2, BlockPos var3);

    public void updateRedstoneOutput(World world, BlockPos pos) {
        world.notifyNeighborsOfStateChange(pos, this, true);
    }

    @Override
    public int tickRate(World world) {
        return 2;
    }

    @Override
    public void onBlockAdded(World world, BlockPos pos, IBlockState state) {
        this.updateRedstoneOutput(world, pos);
    }

    @Override
    public void breakBlock(World world, BlockPos pos, IBlockState state) {
        this.updateRedstoneOutput(world, pos);
    }

    @Override
    public int getStrongPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side) {
        return 0;
    }

    @Override
    public boolean canProvidePower(IBlockState state) {
        return true;
    }

    public abstract boolean canAccept(PokemonGroup var1);
}

