/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.machines;

import com.pixelmongenerations.common.block.MultiBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityMechanicalAnvil;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import java.util.Optional;
import java.util.Random;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockMechanicalAnvil
extends MultiBlock {
    public BlockMechanicalAnvil() {
        super(Material.IRON, 1, 2.0, 1);
        this.setHardness(2.5f);
        this.setTranslationKey("mechanicalanvil");
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public int getLightValue(IBlockState state, IBlockAccess world, BlockPos pos) {
        TileEntityMechanicalAnvil mechanicalAnvil = BlockHelper.getTileEntity(TileEntityMechanicalAnvil.class, world, pos);
        if (mechanicalAnvil != null && mechanicalAnvil.isRunning()) {
            return 15;
        }
        return 0;
    }

    @Override
    public void randomDisplayTick(IBlockState state, World world, BlockPos pos, Random rand) {
        TileEntityMechanicalAnvil mechanicalAnvil = BlockHelper.getTileEntity(TileEntityMechanicalAnvil.class, world, pos);
        if (mechanicalAnvil != null && mechanicalAnvil.isRunning()) {
            EnumFacing enumfacing = state.getValue(FACING);
            double d0 = (double)pos.getX() + 0.5;
            double d1 = (double)pos.getY() + rand.nextDouble() * 6.0 / 16.0;
            double d2 = (double)pos.getZ() + 0.5;
            double d3 = 0.52;
            double d4 = rand.nextDouble() * 0.6 - 0.3;
            switch (enumfacing) {
                case EAST: {
                    world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, d0 - d3, d1, d2 + d4, 0.0, 0.0, 0.0, new int[0]);
                    world.spawnParticle(EnumParticleTypes.FLAME, d0 - d3, d1, d2 + d4, 0.0, 0.0, 0.0, new int[0]);
                    break;
                }
                case WEST: {
                    world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, d0 + d3, d1, d2 + d4, 0.0, 0.0, 0.0, new int[0]);
                    world.spawnParticle(EnumParticleTypes.FLAME, d0 + d3, d1, d2 + d4, 0.0, 0.0, 0.0, new int[0]);
                    break;
                }
                case SOUTH: {
                    world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, d0 + d4, d1, d2 - d3, 0.0, 0.0, 0.0, new int[0]);
                    world.spawnParticle(EnumParticleTypes.FLAME, d0 + d4, d1, d2 - d3, 0.0, 0.0, 0.0, new int[0]);
                    break;
                }
                case NORTH: {
                    world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, d0 + d4, d1, d2 + d3, 0.0, 0.0, 0.0, new int[0]);
                    world.spawnParticle(EnumParticleTypes.FLAME, d0 + d4, d1, d2 + d3, 0.0, 0.0, 0.0, new int[0]);
                    break;
                }
            }
        }
    }

    @Override
    public void breakBlock(World world, BlockPos pos, IBlockState state) {
        BlockPos base = this.findBaseBlock(world, new BlockPos.MutableBlockPos(pos), world.getBlockState(pos));
        TileEntityMechanicalAnvil mechanicalAnvil = BlockHelper.getTileEntity(TileEntityMechanicalAnvil.class, world, base);
        if (mechanicalAnvil != null) {
            InventoryHelper.dropInventoryItems(world, base, (IInventory)mechanicalAnvil);
        }
        world.updateComparatorOutputLevel(base, this);
        super.breakBlock(world, pos, state);
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (world.isRemote || hand == EnumHand.OFF_HAND) {
            return true;
        }
        TileEntityMechanicalAnvil mechanicalAnvil = BlockHelper.getTileEntity(TileEntityMechanicalAnvil.class, world, pos = this.findBaseBlock(world, new BlockPos.MutableBlockPos(pos), state));
        if (mechanicalAnvil != null) {
            player.openGui(Pixelmon.INSTANCE, EnumGui.MechaAnvil.getIndex(), world, pos.getX(), pos.getY(), pos.getZ());
        }
        return true;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }

    @Override
    public Item getDroppedItem(World world, BlockPos pos) {
        return Item.getItemFromBlock(this);
    }

    @Override
    protected Optional<TileEntity> getTileEntity(World world, IBlockState state) {
        return Optional.of(new TileEntityMechanicalAnvil());
    }
}

