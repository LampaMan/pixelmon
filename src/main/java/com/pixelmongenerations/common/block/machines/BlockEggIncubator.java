/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.machines;

import com.pixelmongenerations.common.block.MultiBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityEggIncubator;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import java.util.Optional;
import java.util.Random;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

public class BlockEggIncubator
extends MultiBlock {
    public BlockEggIncubator() {
        super(Material.IRON, 1, 1.0, 1);
        this.setHardness(1.0f);
        this.setSoundType(SoundType.STONE);
        this.setTranslationKey("egg_incubator");
        this.setCreativeTab(PixelmonCreativeTabs.decoration);
    }

    @Override
    public Item getDroppedItem(World world, BlockPos pos) {
        return Item.getItemFromBlock(this);
    }

    @Override
    public int quantityDropped(Random random) {
        return 1;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return face == EnumFacing.DOWN ? BlockFaceShape.SOLID : BlockFaceShape.UNDEFINED;
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.INVISIBLE;
    }

    @Override
    protected Optional<TileEntity> getTileEntity(World world, IBlockState state) {
        return Optional.of(new TileEntityEggIncubator());
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (!world.isRemote && hand == EnumHand.MAIN_HAND) {
            PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player).get();
            TileEntityEggIncubator incubator = BlockHelper.getTileEntity(TileEntityEggIncubator.class, world, pos);
            if (incubator != null && (incubator.getOwnerUUID() == null || player.getUniqueID().equals(incubator.getOwnerUUID()))) {
                int slot = storage.getFirstEggSlot();
                NBTTagCompound firstPoke = storage.getPokemonNbtFromSlot(slot);
                Optional<NBTTagCompound> nbt = incubator.extractEgg();
                if (nbt.isPresent()) {
                    int step = nbt.get().getInteger("steps");
                    int eggCycles = nbt.get().getInteger("eggCycles");
                    if (step >= PixelmonConfig.stepsPerEggCycle && eggCycles <= 0) {
                        storage.hatchEgg((EntityPlayerMP)player, nbt.get());
                    }
                    PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player).get().addToParty((EntityPixelmon)PixelmonEntityList.createEntityFromNBT(nbt.get(), world));
                } else if (incubator.insertEgg(firstPoke, (EntityPlayerMP)player)) {
                    storage.removeFromPartyPlayer(slot);
                }
                world.notifyNeighborsOfStateChange(pos, this, true);
                ((WorldServer)world).getPlayerChunkMap().markBlockForUpdate(pos);
            }
        }
        return true;
    }

    @Override
    public boolean removedByPlayer(IBlockState state, World world, BlockPos pos, EntityPlayer player, boolean willHarvest) {
        if (!player.capabilities.isCreativeMode) {
            this.onPlayerDestroy(world, pos, world.getBlockState(pos));
        }
        return super.removedByPlayer(state, world, pos, player, willHarvest);
    }

    @Override
    public void breakBlock(World world, BlockPos pos, IBlockState state) {
        TileEntityEggIncubator incubator;
        if (!world.isRemote && (incubator = BlockHelper.getTileEntity(TileEntityEggIncubator.class, world, pos)) != null && incubator.containsEgg()) {
            incubator.onDestroy();
        }
        super.breakBlock(world, pos, state);
    }

    @Override
    public int tickRate(World world) {
        return 1;
    }

    @Override
    public float getPlayerRelativeBlockHardness(IBlockState state, EntityPlayer player, World world, BlockPos pos) {
        BlockPos loc = this.findBaseBlock(world, new BlockPos.MutableBlockPos(pos), world.getBlockState(pos));
        TileEntityEggIncubator incuabtor = BlockHelper.getTileEntity(TileEntityEggIncubator.class, world, loc);
        if (incuabtor != null && (incuabtor.getOwnerUUID() == null || player.getUniqueID().equals(incuabtor.getOwnerUUID()) || player.capabilities.isCreativeMode)) {
            return super.getPlayerRelativeBlockHardness(state, player, world, pos);
        }
        return -1.0f;
    }

    @Override
    public int getWeakPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side) {
        TileEntityEggIncubator tile = BlockHelper.getTileEntity(TileEntityEggIncubator.class, blockAccess, pos);
        if (tile != null && tile.containsEgg()) {
            if ((double)tile.percent >= 1.0) {
                return 15;
            }
            return (int)(tile.percent * 15.0f + 1.0f);
        }
        return 0;
    }

    @Override
    public boolean canProvidePower(IBlockState state) {
        return true;
    }
}

