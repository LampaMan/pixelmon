/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.base.Enums
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.block.machines;

import com.google.common.base.Enums;
import com.pixelmongenerations.common.block.IBlockHasOwner;
import com.pixelmongenerations.common.block.MultiBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityTradeMachine;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import javax.annotation.Nullable;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.inventory.ContainerPlayer;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockTradeMachine
extends MultiBlock
implements IBlockHasOwner {
    public BlockTradeMachine() {
        super(Material.ROCK, 3, 2.0, 1);
        this.setHardness(3.5f);
        this.setSoundType(SoundType.STONE);
        this.setTranslationKey("trade_machine");
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return Item.getItemFromBlock(PixelmonBlocks.tradeMachine);
    }

    @Override
    public int quantityDropped(Random random) {
        return 1;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (!world.isRemote || hand == EnumHand.OFF_HAND) {
            ItemStack heldItem = player.getHeldItem(hand);
            pos = this.findBaseBlock(world, new BlockPos.MutableBlockPos(pos), state);
            if (!heldItem.isEmpty() && heldItem.getItem() instanceof ItemDye) {
                TileEntityTradeMachine tileEntityTradeMachine = BlockHelper.getTileEntity(TileEntityTradeMachine.class, world, pos);
                if (tileEntityTradeMachine != null) {
                    if (player.getUniqueID().equals(tileEntityTradeMachine.getOwnerUUID())) {
                        EnumDyeColor color;
                        if (!tileEntityTradeMachine.getRave() && !tileEntityTradeMachine.getColour().isEmpty() && (color = (EnumDyeColor)Enums.getIfPresent(EnumDyeColor.class, (String)tileEntityTradeMachine.getColour().toUpperCase()).orNull()) != null) {
                            ItemStack stack = new ItemStack(Items.DYE);
                            stack.setItemDamage(color.getDyeDamage());
                            world.spawnEntity(new EntityItem(world, (double)pos.getX() + 0.5, (double)pos.getY() + 0.5, (double)pos.getZ() + 0.5, stack));
                        }
                        EnumDyeColor dyeColor = EnumDyeColor.byDyeDamage(heldItem.getItemDamage());
                        tileEntityTradeMachine.setColour(dyeColor.getName());
                        if (!player.capabilities.isCreativeMode) {
                            heldItem.shrink(1);
                        }
                    } else {
                        ChatHandler.sendChat(player, "pixelmon.blocks.trader.ownership", new Object[0]);
                    }
                }
                return true;
            }
            try {
                if (!(player.openContainer instanceof ContainerPlayer) && !(player.openContainer instanceof ContainerEmpty)) {
                    return false;
                }
                TileEntityTradeMachine tileEntityTradeMachine = BlockHelper.getTileEntity(TileEntityTradeMachine.class, world, pos);
                if (tileEntityTradeMachine != null) {
                    if (tileEntityTradeMachine.playerCount < 2) {
                        tileEntityTradeMachine.registerPlayer((EntityPlayerMP)player);
                    } else {
                        ChatHandler.sendChat(player, "pixelmon.blocks.tradefull", new Object[0]);
                    }
                }
            }
            catch (Exception exception) {
                // empty catch block
            }
        }
        return true;
    }

    @Override
    protected Optional<TileEntity> getTileEntity(World world, IBlockState state) {
        return Optional.of(new TileEntityTradeMachine());
    }

    @Override
    public Item getDroppedItem(World world, BlockPos pos) {
        return Item.getItemFromBlock(PixelmonBlocks.tradeMachine);
    }

    @Override
    public void setOwner(BlockPos pos, EntityPlayer playerIn) {
        UUID playerID = playerIn.getUniqueID();
        TileEntityTradeMachine tileEntityTradeMachine = BlockHelper.getTileEntity(TileEntityTradeMachine.class, playerIn.getEntityWorld(), pos);
        tileEntityTradeMachine.setOwner(playerID);
    }

    @Override
    @Nullable
    public TileEntity createNewTileEntity(World world, int i) {
        return new TileEntityTradeMachine();
    }

    @Override
    public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
        TileEntityTradeMachine tileEntity = BlockHelper.getTileEntity(TileEntityTradeMachine.class, worldIn, pos);
        if (tileEntity != null && !tileEntity.getRave() && !tileEntity.getColour().isEmpty()) {
            EnumDyeColor color = (EnumDyeColor)Enums.getIfPresent(EnumDyeColor.class, (String)tileEntity.getColour().toUpperCase()).orNull();
            if (!tileEntity.getRave() && color != null) {
                ItemStack stack = new ItemStack(Items.DYE);
                stack.setItemDamage(color.getDyeDamage());
                InventoryHelper.spawnItemStack(worldIn, (double)pos.getX() + 0.5, (double)pos.getY() + 0.5, (double)pos.getZ() + 0.5, stack);
            }
        }
        super.breakBlock(worldIn, pos, state);
    }
}

