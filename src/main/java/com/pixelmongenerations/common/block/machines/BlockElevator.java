/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.machines;

import com.pixelmongenerations.api.events.ElevatorEvent;
import com.pixelmongenerations.common.block.generic.GenericBlock;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class BlockElevator
extends GenericBlock {
    public BlockElevator() {
        super(Material.IRON);
        this.setHardness(0.8f);
        this.setCreativeTab(PixelmonCreativeTabs.utilityBlocks);
    }

    @Override
    public void onLanded(World world, Entity entity) {
        if (!(entity instanceof EntityPlayerMP)) {
            return;
        }
        EntityPlayerMP player = (EntityPlayerMP)entity;
        if (player.isSneaking()) {
            this.takeElevator(world, entity.getPosition().down(), player, false);
        }
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        return false;
    }

    public void takeElevator(World world, BlockPos pos, EntityPlayerMP player, boolean upwards) {
        ElevatorEvent elevatorEvent;
        BlockPos elevatorPos = this.findNearestElevator(world, pos, upwards);
        if (elevatorPos != null && !MinecraftForge.EVENT_BUS.post(elevatorEvent = new ElevatorEvent(player, this, upwards, new BlockPos(player.posX, (double)elevatorPos.getY(), player.posZ)))) {
            BlockPos destination = elevatorEvent.getDestination();
            player.setPositionAndUpdate(player.posX, (double)destination.getY() + 0.5, player.posZ);
        }
    }

    private BlockPos findNearestElevator(World world, BlockPos pos, boolean upwards) {
        int searchRangeY = PixelmonConfig.elevatorSearchRange;
        if (!upwards) {
            searchRangeY *= -1;
        }
        int posY = pos.getY();
        BlockPos.MutableBlockPos newPos = new BlockPos.MutableBlockPos();
        int y = posY;
        while (upwards ? y < posY + searchRangeY : y > posY + searchRangeY) {
            newPos.setPos(pos.getX(), y, pos.getZ());
            if (!newPos.equals(pos) && !newPos.up().equals(pos) && world.getBlockState(newPos).getBlock() == this) {
                return newPos.up();
            }
            y += upwards ? 1 : -1;
        }
        return null;
    }
}

