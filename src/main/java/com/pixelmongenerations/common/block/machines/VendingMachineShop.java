/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.machines;

import com.pixelmongenerations.common.entity.npcs.registry.BaseShopItem;
import com.pixelmongenerations.common.entity.npcs.registry.ShopItem;
import com.pixelmongenerations.common.entity.npcs.registry.ShopItemWithVariation;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import java.util.ArrayList;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class VendingMachineShop {
    private ArrayList<ShopItemWithVariation> items = new ArrayList();

    public VendingMachineShop() {
        this.addShopItem(PixelmonItems.freshWater, 200, 100);
        this.addShopItem(PixelmonItems.sodaPop, 300, 150);
        this.addShopItem(PixelmonItems.lemonade, 350, 175);
        this.addShopItem(PixelmonItems.moomooMilk, 500, 250);
        this.addShopItem(PixelmonItemsHeld.berryJuice, 1500, 50);
    }

    private void addShopItem(Item item, int buyPrice, int sellPrice) {
        BaseShopItem baseItem = new BaseShopItem(item.getTranslationKey(), new ItemStack(item, 1), buyPrice, sellPrice);
        ShopItem shopItem = new ShopItem(baseItem, 1.0f, 1.0f, false);
        ShopItemWithVariation shopItemWithVariation = new ShopItemWithVariation(shopItem, 1.0f);
        this.items.add(shopItemWithVariation);
    }

    public ArrayList<ShopItemWithVariation> getItems() {
        return this.items;
    }
}

