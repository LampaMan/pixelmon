/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 *  javax.vecmath.Vector3d
 */
package com.pixelmongenerations.common.block.loot;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.DropQueryFactory;
import com.pixelmongenerations.api.events.PokeLootClaimedEvent;
import com.pixelmongenerations.api.spawning.AbstractSpawner;
import com.pixelmongenerations.api.spawning.SpawnAction;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.conditions.LocationType;
import com.pixelmongenerations.api.world.MutableLocation;
import com.pixelmongenerations.common.achievement.PixelmonAchievements;
import com.pixelmongenerations.common.block.IBlockHasOwner;
import com.pixelmongenerations.common.block.enums.EnumPokeChestType;
import com.pixelmongenerations.common.block.enums.EnumPokechestVisibility;
import com.pixelmongenerations.common.block.tileEntities.TileEntityPokeChest;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.npcs.registry.DropItemRegistry;
import com.pixelmongenerations.common.entity.npcs.registry.NPCRegistryTrainers;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.drops.DroppedItem;
import com.pixelmongenerations.common.spawning.PixelmonSpawning;
import com.pixelmongenerations.common.spawning.PlayerTrackingSpawner;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonItemsPokeballs;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumTrainerAI;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.util.PixelSounds;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import javax.vecmath.Vector3d;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.BlockHorizontal;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockPokeChest
extends BlockContainer
implements IBlockHasOwner {
    public static final PropertyDirection FACING = BlockHorizontal.FACING;
    private static final AxisAlignedBB AABB = new AxisAlignedBB(0.29f, 0.0, 0.29f, 0.72f, 0.44f, 0.72f);
    private Class<? extends TileEntityPokeChest> pokeChestTileEntityClass;
    protected EnumPokeChestType TYPE = EnumPokeChestType.POKEBALL;
    List<ItemStack> drops = null;

    public BlockPokeChest(Class<? extends TileEntityPokeChest> tileEntityClass) {
        super(Material.GLASS);
        this.setResistance(6000000.0f);
        this.pokeChestTileEntityClass = tileEntityClass;
        this.setTranslationKey("pokechest");
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer((Block)this, FACING);
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState().withProperty(FACING, EnumFacing.byHorizontalIndex((int)meta));
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return state.getValue(FACING).getHorizontalIndex();
    }

    protected List<ItemStack> getChestDrops() {
        if (this.TYPE == EnumPokeChestType.POKEBALL) {
            return Lists.newArrayList(DropItemRegistry.getTier1Drop());
        }
        if (this.TYPE == EnumPokeChestType.ULTRABALL) {
            return Lists.newArrayList(DropItemRegistry.getTier2Drop());
        }
        if (this.TYPE == EnumPokeChestType.MASTERBALL) {
            return Lists.newArrayList(DropItemRegistry.getTier3Drop());
        }
        if (this.TYPE == EnumPokeChestType.BEASTBALL) {
            return Lists.newArrayList(DropItemRegistry.getTier4Drop());
        }
        if (this.TYPE == EnumPokeChestType.POKESTOP) {
            return Lists.newArrayList(DropItemRegistry.getPokeStopDrops());
        }
        return null;
    }

    private ItemStack HiddenDrop() {
        int num = RandomHelper.rand.nextInt(100);
        if (num <= 50) {
            return DropItemRegistry.getTier1Drop();
        }
        if (num < 85) {
            return DropItemRegistry.getTier2Drop();
        }
        return DropItemRegistry.getTier3Drop();
    }

    private List<ItemStack> getRandomDrops(EnumPokechestVisibility visibility) {
        if (visibility == EnumPokechestVisibility.Visible) {
            return this.getChestDrops();
        }
        return Lists.newArrayList(this.HiddenDrop());
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        if (world.isRemote || hand == EnumHand.OFF_HAND) {
            return true;
        }
        TileEntity tileEntity = world.getTileEntity(pos);
        if (!(tileEntity instanceof TileEntityPokeChest)) {
            return true;
        }
        TileEntityPokeChest tile = BlockHelper.getTileEntity(TileEntityPokeChest.class, world, pos);
        if (this.TYPE == EnumPokeChestType.POKESTOP) {
            tile.setOwner(null);
            if (!tile.lureActivated() && player.getHeldItemMainhand().getItem().equals(PixelmonItems.lureModule)) {
                tile.setMaxSpawns(PixelmonConfig.lureSpawns);
                tile.setLureTimer(PixelmonConfig.lureTimer);
                tile.setActivated(true);
                ChatHandler.sendChat(player, "pixelmon.blocks.lureActivated", new Object[0]);
                player.getHeldItemMainhand().shrink(1);
                return true;
            }
            if (tile.lureActivated() && player.getHeldItemMainhand().getItem().equals(PixelmonItems.lureModule)) {
                ChatHandler.sendChat(player, "pixelmon.blocks.lureActive", new Object[0]);
                return true;
            }
        }
        UUID blockOwner = tile.getOwner();
        UUID playerID = player.getUniqueID();
        if (!playerID.equals(blockOwner)) {
            EnumPokechestVisibility visibility = tile.getVisibility();
            if (tile.canClaim(playerID) && !MinecraftForge.EVENT_BUS.post(new PokeLootClaimedEvent((EntityPlayerMP)player, tile))) {
                if (tile.shouldBreakBlock()) {
                    if (MinecraftForge.EVENT_BUS.post(new BlockEvent.BreakEvent(world, pos, state, player))) {
                        return true;
                    }
                    world.setBlockToAir(pos);
                }
                if (tile.isGrotto()) {
                    player.addStat(PixelmonAchievements.grottoChieve, 1);
                }
                this.drops = tile.isCustomDrop() ? tile.getCustomDrops() : this.getRandomDrops(visibility);
                DropQueryFactory factory = DropQueryFactory.newInstance().customTitle(new TextComponentTranslation(this.TYPE == EnumPokeChestType.POKESTOP ? "tile.pokestop.name" : "tile.poke_chest.name", new Object[0]));
                for (ItemStack drop : this.drops) {
                    if (drop == null) continue;
                    Item dropItem = drop.getItem();
                    if (dropItem == PixelmonItemsPokeballs.masterBall) {
                        player.addStat(PixelmonAchievements.masterBallChieve, 1);
                    }
                    ItemStack newDrop = new ItemStack(dropItem, drop.getCount(), drop.getItemDamage());
                    factory.addDrop(DroppedItem.of(newDrop));
                }
                factory.addTarget((EntityPlayerMP)player);
                factory.send();
                if (this.TYPE == EnumPokeChestType.POKESTOP) {
                    if (RandomHelper.getRandomChance(10)) {
                        NPCTrainer trainer = new NPCTrainer(player.world);
                        trainer.init(NPCRegistryTrainers.getByName("FemaleRocketGrunt"));
                        trainer.setPosition((float)pos.getX() + 2.0f, pos.getY() + 1, (float)pos.getZ() + 1.0f);
                        trainer.setAIMode(EnumTrainerAI.StandStill);
                        trainer.setBossMode(EnumBossMode.Rare);
                        trainer.updateDrops(DropItemRegistry.getRocketDrops().toArray(new ItemStack[0]));
                        trainer.ignoreDespawnCounter = true;
                        trainer.initAI();
                        Pixelmon.PROXY.spawnEntitySafely(trainer, player.world);
                        trainer.setPosition((float)pos.getX() + 2.0f, pos.getY() + 1, (float)pos.getZ() + 1.0f);
                        trainer.setStartRotationYaw(player.rotationYaw + 180.0f);
                        trainer.ignoreDespawnCounter = true;
                        ChatHandler.sendChat(player, "pixelmon.blocks.pokestoprockettrainer", new Object[0]);
                    }
                } else if (this.TYPE.isPokemonHabitable() && PixelmonConfig.wildPokemonHidingInLoot && RandomHelper.getRandomNumberBetween(1, PixelmonConfig.wildPokemonLootSpawnChance) == 1) {
                    SpawnAction<? extends Entity> spawnAction;
                    Entity entity;
                    SpawnLocation spawnLocation = new SpawnLocation(new MutableLocation(world, pos), LocationType.getPotentialTypes(world.getBlockState(player.getPosition().add(0, -1, 0))), world.getBlockState(pos).getBlock(), null, world.getBiome(pos), world.canSeeSky(pos), 6);
                    ArrayList<SpawnLocation> spawnLocations = new ArrayList<SpawnLocation>();
                    spawnLocations.add(spawnLocation);
                    PlayerTrackingSpawner playerTrackingSpawner = new PlayerTrackingSpawner(player.getUniqueID());
                    for (AbstractSpawner spawner : PixelmonSpawning.coordinator.spawners) {
                        if (!(spawner instanceof PlayerTrackingSpawner) || ((PlayerTrackingSpawner)spawner).playerUUID.compareTo(player.getUniqueID()) != 0) continue;
                        playerTrackingSpawner = (PlayerTrackingSpawner)spawner;
                    }
                    ArrayList<SpawnAction<? extends Entity>> possibleSpawns = playerTrackingSpawner.calculateSpawnActions(spawnLocations);
                    if (!possibleSpawns.isEmpty() && (entity = (spawnAction = possibleSpawns.get(0)).getOrCreateEntity()) instanceof EntityPixelmon) {
                        EntityPixelmon pixelmon = (EntityPixelmon)entity;
                        pixelmon.setPosition(pos.getX(), pos.getY(), pos.getZ());
                        world.spawnEntity(pixelmon);
                        TextComponentTranslation msg = new TextComponentTranslation("pokeloot.wildpokemonspawn", pixelmon.getLocalizedName(), this.TYPE.getLootPrefix());
                        msg.getStyle().setColor(TextFormatting.GRAY);
                        player.sendMessage(msg);
                    }
                }
                tile.removeClaimer(playerID);
                tile.addClaimer(playerID);
                world.playSound(null, player.posX, player.posY, player.posZ, PixelSounds.pokelootObtained, SoundCategory.BLOCKS, 0.2f, 1.0f);
                if (visibility == EnumPokechestVisibility.Hidden) {
                    player.addStat(PixelmonAchievements.hiddenLootChieve, 1);
                } else {
                    player.addStat(PixelmonAchievements.normalLootChieve, 1);
                }
            } else if (tile.isTimeEnabled()) {
                String message = this.TYPE == EnumPokeChestType.POKESTOP ? "pixelmon.blocks.timedpokestopclaim" : "pixelmon.blocks.timedclaim";
                ChatHandler.sendChat(player, message, new Object[0]);
            } else {
                ChatHandler.sendChat(player, "pixelmon.blocks.claimedloot", new Object[0]);
            }
        } else if (this.TYPE != EnumPokeChestType.POKESTOP) {
            boolean shiftClick = player.isSneaking();
            if (shiftClick) {
                tile.setOwner(null);
                ChatHandler.sendChat(player, "pixelmon.blocks.ownerchanged", new Object[0]);
            } else {
                ItemStack itemStack = player.getHeldItemMainhand();
                if (!itemStack.isEmpty()) {
                    tile.addCustomDrop(itemStack);
                    ChatHandler.sendChat(player, "pixelmon.blocks.chestset", I18n.translateToLocal(itemStack.getTranslationKey() + ".name"));
                } else {
                    EnumPokechestVisibility visibility = tile.getVisibility();
                    String metaMode = "";
                    if (visibility == EnumPokechestVisibility.Hidden) {
                        tile.setVisibility(EnumPokechestVisibility.Visible);
                        metaMode = "Normal";
                    } else {
                        tile.setVisibility(EnumPokechestVisibility.Hidden);
                        metaMode = "Hidden";
                    }
                    ChatHandler.sendChat(player, "pixelmon.blocks.visible", metaMode);
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer) {
        int i = MathHelper.floor((double)(placer.rotationYaw * 4.0f / 360.0f) + 0.5) & 3;
        EnumFacing enumfacing = EnumFacing.byHorizontalIndex((int)i);
        return this.getDefaultState().withProperty(FACING, enumfacing);
    }

    @Override
    public void onBlockClicked(World world, BlockPos pos, EntityPlayer player) {
        TileEntityPokeChest tile;
        UUID playerID;
        if (!world.isRemote && (playerID = player.getUniqueID()).equals((tile = BlockHelper.getTileEntity(TileEntityPokeChest.class, world, pos)).getOwner()) && this.TYPE != EnumPokeChestType.POKESTOP) {
            String mode = "pixelmon.blocks.chestmode";
            boolean chestMode = tile.getChestMode();
            boolean dropMode = tile.getDropMode();
            boolean timeEnabled = tile.isTimeEnabled();
            if (!(chestMode || dropMode || timeEnabled)) {
                tile.setChestOneTime(false);
                tile.setDropOneTime(true);
                tile.setTimeEnabled(false);
                mode = mode + "PL1D";
            } else if (!chestMode && dropMode && !timeEnabled) {
                tile.setDropOneTime(true);
                tile.setChestOneTime(false);
                tile.setTimeEnabled(true);
                mode = mode + "TD";
            } else if (timeEnabled) {
                tile.setTimeEnabled(false);
                tile.setDropOneTime(true);
                tile.setChestOneTime(true);
                mode = mode + "FCFS";
            } else if (chestMode && dropMode && !timeEnabled) {
                tile.setChestOneTime(false);
                tile.setDropOneTime(false);
                tile.setTimeEnabled(false);
                mode = mode + "PUD";
            }
            ChatHandler.sendChat(player, "pixelmon.blocks.chestmode", I18n.translateToLocal(mode));
        }
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        if (this.TYPE == EnumPokeChestType.POKEBALL) {
            return new ItemStack(PixelmonBlocks.pokeChest);
        }
        if (this.TYPE == EnumPokeChestType.MASTERBALL) {
            return new ItemStack(PixelmonBlocks.masterChest);
        }
        if (this.TYPE == EnumPokeChestType.ULTRABALL) {
            return new ItemStack(PixelmonBlocks.ultraChest);
        }
        if (this.TYPE == EnumPokeChestType.BEASTBALL) {
            return new ItemStack(PixelmonBlocks.beastChest);
        }
        if (this.TYPE == EnumPokeChestType.POKESTOP) {
            return new ItemStack(PixelmonBlocks.pokeStop);
        }
        return null;
    }

    @Override
    public TileEntity createNewTileEntity(World par1World, int var1) {
        try {
            return this.pokeChestTileEntityClass.newInstance();
        }
        catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void randomDisplayTick(IBlockState stateIn, World world, BlockPos pos, Random random) {
        TileEntityPokeChest tile = BlockHelper.getTileEntity(TileEntityPokeChest.class, world, pos);
        if (tile.getVisibility() == EnumPokechestVisibility.Hidden) {
            float rand = random.nextFloat() * 0.5f + 1.0f;
            double xVel = 0.1;
            double yVel = 0.2;
            double zVel = 0.1;
            world.spawnParticle(EnumParticleTypes.SPELL_INSTANT, (double)pos.getX() + 0.5, (double)pos.getY() + 0.5, (double)pos.getZ() + 0.5, xVel * (double)rand, yVel * (double)rand, zVel * (double)rand, new int[0]);
        }
        if (tile.getBlockType() != PixelmonBlocks.pokeStop) {
            return;
        }
        if (!tile.isActivated()) {
            return;
        }
        Vector3d position = new Vector3d((double)tile.getPos().getX(), (double)tile.getPos().getY(), (double)tile.getPos().getZ());
        ArrayList<Vector3d> positions = this.getCirclePositions(position, PixelmonConfig.lureRadius, PixelmonConfig.lureRadius * 6);
        for (Vector3d particlePos : positions) {
            world.spawnParticle(EnumParticleTypes.REDSTONE, particlePos.getX() + 0.5, particlePos.getY() + 0.5, particlePos.getZ() + 0.5, 1.0, 0.0, 1.0, new int[0]);
            world.spawnParticle(EnumParticleTypes.END_ROD, particlePos.getX() + 0.5, particlePos.getY() + 0.5, particlePos.getZ() + 0.5, 0.0, 0.0, 0.0, new int[0]);
        }
    }

    public ArrayList<Vector3d> getCirclePositions(Vector3d center, double radius, int amount) {
        double increment = Math.PI * 2 / (double)amount;
        ArrayList<Vector3d> positions = new ArrayList<Vector3d>();
        for (int i = 0; i < amount; ++i) {
            double angle = (double)i * increment;
            double x = center.getX() + radius * Math.cos(angle);
            double z = center.getZ() + radius * Math.sin(angle);
            positions.add(new Vector3d(x, center.getY(), z));
        }
        return positions;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return face == EnumFacing.DOWN ? BlockFaceShape.CENTER : BlockFaceShape.UNDEFINED;
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess access, BlockPos pos) {
        return AABB;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public AxisAlignedBB getSelectedBoundingBox(IBlockState state, World worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public void setOwner(BlockPos pos, EntityPlayer playerIn) {
        UUID playerID = playerIn.getUniqueID();
        TileEntityPokeChest tile = BlockHelper.getTileEntity(TileEntityPokeChest.class, playerIn.world, pos);
        if (this.TYPE == EnumPokeChestType.POKESTOP) {
            tile.setTimeEnabled(true);
        }
        tile.setOwner(playerID);
    }
}

