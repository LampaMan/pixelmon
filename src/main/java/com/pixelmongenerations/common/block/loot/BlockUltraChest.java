/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.loot;

import com.pixelmongenerations.common.block.enums.EnumPokeChestType;
import com.pixelmongenerations.common.block.loot.BlockPokeChest;
import com.pixelmongenerations.common.block.tileEntities.TileEntityPokeChest;

public class BlockUltraChest
extends BlockPokeChest {
    public BlockUltraChest() {
        super(TileEntityPokeChest.class);
        this.TYPE = EnumPokeChestType.ULTRABALL;
        this.setTranslationKey("ultrachest");
    }
}

