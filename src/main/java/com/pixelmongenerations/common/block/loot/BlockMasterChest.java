/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.loot;

import com.pixelmongenerations.common.block.enums.EnumPokeChestType;
import com.pixelmongenerations.common.block.loot.BlockPokeChest;
import com.pixelmongenerations.common.block.tileEntities.TileEntityPokeChest;

public class BlockMasterChest
extends BlockPokeChest {
    public BlockMasterChest() {
        super(TileEntityPokeChest.class);
        this.TYPE = EnumPokeChestType.MASTERBALL;
        this.setTranslationKey("masterchest");
    }
}

