/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.api.enums.ReceiveType;
import com.pixelmongenerations.api.events.PixelmonReceivedEvent;
import com.pixelmongenerations.common.achievement.PixelmonAchievements;
import com.pixelmongenerations.common.block.BlockPokegift;
import com.pixelmongenerations.common.block.tileEntities.EnumPokegiftType;
import com.pixelmongenerations.common.block.tileEntities.TileEntityPokegift;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.IVStore;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.enums.EnumNature;
import com.pixelmongenerations.core.enums.EnumPokegiftEventType;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelSounds;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class BlockPokegiftEvent
extends BlockPokegift {
    public BlockPokegiftEvent(Class<? extends TileEntityPokegift> tileEntityClass) {
        super(tileEntityClass);
        this.TYPE = EnumPokegiftType.EVENT;
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return null;
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        if (world.isRemote || hand == EnumHand.OFF_HAND) {
            return true;
        }
        if (PixelmonConfig.doPokegiftEvents) {
            TileEntityPokegift tile = BlockHelper.getTileEntity(TileEntityPokegift.class, world, pos);
            UUID blockOwner = tile.getOwner();
            UUID playerID = player.getUniqueID();
            if (playerID != blockOwner) {
                state.getBlock().getMetaFromState(state);
                if (tile.canClaim(playerID)) {
                    if (tile.shouldBreakBlock()) {
                        world.setBlockToAir(pos);
                    }
                    if (tile.getSpecialPixelmon().isEmpty()) {
                        ChatHandler.sendChat(player, "pixelutilities.blocks.emptygift", this.itemName);
                        world.playSound(null, player.posX, player.posY, player.posZ, SoundEvents.BLOCK_DISPENSER_FAIL, SoundCategory.BLOCKS, 0.7f, 1.0f);
                        return true;
                    }
                    ChatHandler.sendChat(player, "pixelmon.blocks.chestfound", this.itemName);
                    player.addStat(PixelmonAchievements.pokeGift, 1);
                    Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
                    if (optstorage.isPresent()) {
                        for (EntityPixelmon p : tile.getSpecialPixelmon()) {
                            MinecraftForge.EVENT_BUS.post(new PixelmonReceivedEvent((EntityPlayerMP)player, ReceiveType.PokeBall, p));
                            optstorage.get().addToParty(p);
                        }
                    }
                    tile.addClaimer(playerID);
                    world.playSound(null, player.posX, player.posY, player.posZ, PixelSounds.pokelootObtained, SoundCategory.BLOCKS, 0.2f, 1.0f);
                } else {
                    ChatHandler.sendChat(player, "pixelmon.blocks.claimedloot", new Object[0]);
                }
            }
        } else {
            ChatHandler.sendChat(player, "pixelutilities.event.noevents", new Object[0]);
        }
        return true;
    }

    @Override
    public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase player) {
        EnumPokegiftEventType type = this.checkTime();
        if (type == EnumPokegiftEventType.Christmas) {
            this.itemName = I18n.translateToLocal("pixelmon.blocks.christmaspokegift");
        } else if (type == EnumPokegiftEventType.Halloween) {
            this.itemName = I18n.translateToLocal("pixelmon.blocks.halloweenpokegift");
        } else if (type == EnumPokegiftEventType.Custom) {
            this.itemName = I18n.translateToLocal("pixelmon.blocks.custompokegift");
        }
        return super.getStateForPlacement(world, pos, facing, hitX, hitY, hitZ, meta, player);
    }

    @Override
    public TileEntity createNewTileEntity(World world, int var1) {
        try {
            TileEntityPokegift tileP = (TileEntityPokegift)this.pokeChestTileEntityClass.newInstance();
            tileP.setChestOneTime(false);
            tileP.setOwner(null);
            tileP.setAllSpecialPixelmon(this.generatePixelmon(world));
            return tileP;
        }
        catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    private ArrayList<EntityPixelmon> generatePixelmon(World world) {
        ArrayList<EntityPixelmon> pixelmon = new ArrayList<EntityPixelmon>();
        Random rng = world.rand;
        int max = PixelmonConfig.pokegiftEventMaxPokes;
        int numberToCreate = rng.nextInt(max);
        if (numberToCreate == 0 || numberToCreate > 6 || numberToCreate > max) {
            numberToCreate = 1;
        }
        for (int i = 0; i < numberToCreate; ++i) {
            String name = "";
            name = PixelmonConfig.pokegiftEventLegendaries ? EnumSpecies.randomPoke((boolean)false).name : EnumSpecies.randomPoke((boolean)true).name;
            EntityPixelmon p = (EntityPixelmon)PixelmonEntityList.createEntityByName(name, world);
            p.getLvl().setLevel(5);
            p.setGrowth(EnumGrowth.getRandomGrowth());
            if (PixelmonConfig.pokegiftEventShinies && rng.nextInt(PixelmonConfig.pokegiftEventShinyRate) == 0) {
                p.setShiny(true);
            }
            p.setNature(EnumNature.getRandomNature());
            p.caughtBall = EnumPokeball.CherishBall;
            p.friendship.setFriendship(150);
            p.baseStats.baseFriendship = 150;
            IVStore.createNewIVs();
            pixelmon.add(p);
        }
        return pixelmon;
    }

    public EnumPokegiftEventType checkTime() {
        LocalDate localDate = LocalDate.now();
        String[] dayMonth = PixelmonConfig.customPokegiftEventTime.split("/");
        if (dayMonth.length == 2 && !dayMonth[0].equalsIgnoreCase("D") && !dayMonth[1].equalsIgnoreCase("M")) {
            int day = Integer.parseInt(dayMonth[0]);
            int month = Integer.parseInt(dayMonth[1]);
            if (localDate.getMonthValue() == month && localDate.getDayOfMonth() == day) {
                return EnumPokegiftEventType.Custom;
            }
        }
        if (localDate.getMonth() == Month.OCTOBER && localDate.getDayOfMonth() > 12 && localDate.getDayOfMonth() < 26) {
            return EnumPokegiftEventType.Halloween;
        }
        if (localDate.getMonth() == Month.DECEMBER && localDate.getDayOfMonth() > 7 && localDate.getDayOfMonth() < 30) {
            return EnumPokegiftEventType.Christmas;
        }
        return EnumPokegiftEventType.None;
    }
}

