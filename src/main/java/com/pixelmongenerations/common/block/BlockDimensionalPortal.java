/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.common.block.generic.GenericBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityDimensionalPortal;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import javax.annotation.Nullable;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;

public class BlockDimensionalPortal
extends GenericBlock {
    public BlockDimensionalPortal() {
        super(Material.IRON);
        this.setCreativeTab(PixelmonCreativeTabs.utilityBlocks);
    }

    @Override
    public void onLanded(World world, Entity entity) {
        if (entity instanceof EntityPlayerMP) {
            EntityPlayerMP player = (EntityPlayerMP)entity;
            for (IProperty<?> iProperty : this.getBlockState().getProperties()) {
                player.sendMessage(new TextComponentString(iProperty.getName() + " : " + iProperty.getAllowedValues() + " : " + iProperty.getValueClass()));
            }
            TileEntity te = world.getTileEntity(player.getPosition().subtract(new Vec3i(0, 1, 0)));
            if (te == null) {
                return;
            }
            TileEntityDimensionalPortal portal = (TileEntityDimensionalPortal)te;
            if (portal.teleportLocation != null) {
                player.setPositionAndUpdate(portal.teleportLocation.getX() + 2, portal.teleportLocation.getY() + 2, portal.teleportLocation.getZ());
                player.sendMessage(new TextComponentString(portal.teleportLocation.getX() + ", " + portal.teleportLocation.getY() + "," + portal.teleportLocation.getZ()));
            }
        }
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (!worldIn.isRemote && hand == EnumHand.MAIN_HAND) {
            if (worldIn.getTileEntity(pos) == null) {
                return false;
            }
            TileEntityDimensionalPortal te = (TileEntityDimensionalPortal)worldIn.getTileEntity(pos);
            te.teleportLocation = new BlockPos(pos.getX() - 11, pos.getY(), pos.getZ());
            playerIn.sendMessage(new TextComponentString("BLOCK CLICKED TP TO :" + te.teleportLocation.getX() + ", " + te.teleportLocation.getY() + "," + te.teleportLocation.getZ()));
            worldIn.setBlockState(te.teleportLocation, PixelmonBlocks.dimensionalPortalBlock.getDefaultState());
            TileEntityDimensionalPortal newTE = (TileEntityDimensionalPortal)worldIn.getTileEntity(te.teleportLocation);
            newTE.teleportLocation = pos;
            playerIn.sendMessage(new TextComponentString("TP BLOCK TP LOCATON: " + newTE.teleportLocation.getX() + ", " + newTE.teleportLocation.getY() + "," + newTE.teleportLocation.getZ()));
        }
        return true;
    }

    @Override
    public void onBlockAdded(World worldIn, BlockPos pos, IBlockState state) {
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    @Nullable
    public TileEntity createTileEntity(World world, IBlockState state) {
        return new TileEntityDimensionalPortal();
    }
}

