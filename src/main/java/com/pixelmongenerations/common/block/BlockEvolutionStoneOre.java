/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumEvolutionStone;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockEvolutionStoneOre
extends Block {
    private EnumEvolutionStone type = null;

    public BlockEvolutionStoneOre(EnumEvolutionStone type, float hardness, String itemName) {
        super(Material.ROCK);
        this.type = type;
        this.setHardness(hardness);
        this.setSoundType(SoundType.STONE);
        if (type == EnumEvolutionStone.Waterstone) {
            this.setLightLevel(0.5f);
        }
        this.setCreativeTab(PixelmonCreativeTabs.natural);
        this.setTranslationKey(itemName);
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.MODEL;
    }

    @SideOnly(value=Side.CLIENT)
    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.SOLID;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return true;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return true;
    }

    @Override
    public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
        ArrayList<ItemStack> drops = new ArrayList<ItemStack>();
        Item item = null;
        switch (this.type) {
            case Thunderstone: {
                item = PixelmonItems.thunderStoneShard;
                break;
            }
            case Leafstone: {
                item = PixelmonItems.leafStoneShard;
                break;
            }
            case Waterstone: {
                item = PixelmonItems.waterStoneShard;
                break;
            }
            case Firestone: {
                item = PixelmonItems.fireStoneShard;
                break;
            }
            case Sunstone: {
                item = PixelmonItems.sunStoneShard;
                break;
            }
            case Dawnstone: {
                item = PixelmonItems.dawnStoneShard;
                break;
            }
            case Duskstone: {
                item = PixelmonItems.duskStoneShard;
                break;
            }
            case Moonstone: {
                item = PixelmonItems.moonStoneShard;
                break;
            }
            case Shinystone: {
                item = PixelmonItems.shinyStoneShard;
                break;
            }
            case Icestone: {
                item = PixelmonItems.iceStoneShard;
            }
        }
        if (item != null) {
            drops.add(new ItemStack(item, RandomHelper.getFortuneAmount(fortune)));
        }
        return drops;
    }
}

