/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.api.enums.ReceiveType;
import com.pixelmongenerations.api.events.PixelmonReceivedEvent;
import com.pixelmongenerations.common.achievement.PixelmonAchievements;
import com.pixelmongenerations.common.block.IBlockHasOwner;
import com.pixelmongenerations.common.block.enums.EnumPokechestVisibility;
import com.pixelmongenerations.common.block.tileEntities.EnumPokegiftType;
import com.pixelmongenerations.common.block.tileEntities.TileEntityPokegift;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelSounds;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.BlockHorizontal;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class BlockPokegift
extends BlockContainer
implements IBlockHasOwner {
    private static final AxisAlignedBB AABB = new AxisAlignedBB(0.29f, 0.0, 0.29f, 0.72f, 0.44f, 0.72f);
    public static final PropertyDirection FACING = BlockHorizontal.FACING;
    protected Class<? extends TileEntityPokegift> pokeChestTileEntityClass;
    protected String itemName = I18n.translateToLocal("item.pokeGift.name");
    protected double xVel = 0.1;
    protected double yVel = 0.2;
    protected double zVel = 0.1;
    protected EnumPokegiftType TYPE = EnumPokegiftType.GIFT;

    public BlockPokegift(Class<? extends TileEntityPokegift> tileEntityClass) {
        super(Material.GLASS);
        this.pokeChestTileEntityClass = tileEntityClass;
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer((Block)this, FACING);
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState().withProperty(FACING, EnumFacing.byHorizontalIndex((int)meta));
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return state.getValue(FACING).getHorizontalIndex();
    }

    @Override
    public IBlockState withRotation(IBlockState state, Rotation rot) {
        return state.withProperty(FACING, rot.rotate(state.getValue(FACING)));
    }

    @Override
    public IBlockState withMirror(IBlockState state, Mirror mirrorIn) {
        return state.withRotation(mirrorIn.toRotation(state.getValue(FACING)));
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        if (worldIn.isRemote || hand == EnumHand.OFF_HAND) {
            return true;
        }
        TileEntityPokegift tile = BlockHelper.getTileEntity(TileEntityPokegift.class, worldIn, pos);
        if (tile != null) {
            UUID blockOwner = tile.getOwner();
            UUID playerID = playerIn.getUniqueID();
            if (playerID != blockOwner) {
                state.getBlock().getMetaFromState(state);
                if (tile.canClaim(playerID)) {
                    if (tile.shouldBreakBlock()) {
                        worldIn.setBlockToAir(pos);
                    }
                    if (tile.getPixelmon() == null) {
                        ChatHandler.sendChat(playerIn, "pixelutilities.blocks.emptygift", this.itemName);
                        worldIn.playSound(null, playerIn.posX, playerIn.posY, playerIn.posZ, SoundEvents.BLOCK_DISPENSER_FAIL, SoundCategory.BLOCKS, 0.8f, 1.0f);
                        return true;
                    }
                    ChatHandler.sendChat(playerIn, "pixelmon.blocks.chestfound", this.itemName);
                    playerIn.addStat(PixelmonAchievements.pokeGift, 1);
                    Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)playerIn);
                    if (optstorage.isPresent()) {
                        if (tile.getPixelmon().getOwner() == null) {
                            ChatHandler.sendChat(playerIn, "pixelmon.block.error", this.itemName);
                            return false;
                        }
                    } else {
                        return false;
                    }
                    MinecraftForge.EVENT_BUS.post(new PixelmonReceivedEvent((EntityPlayerMP)playerIn, ReceiveType.PokeBall, tile.getPixelmon()));
                    optstorage.get().addToParty(tile.getPixelmon());
                    tile.addClaimer(playerID);
                    worldIn.playSound(null, playerIn.posX, playerIn.posY, playerIn.posZ, PixelSounds.pokelootObtained, SoundCategory.BLOCKS, 0.2f, 1.0f);
                } else {
                    ChatHandler.sendChat(playerIn, "pixelmon.blocks.claimedloot", new Object[0]);
                }
            } else {
                boolean shiftClick = playerIn.isSneaking();
                if (shiftClick) {
                    if (tile.getPixelmon() != null) {
                        tile.setOwner(null);
                        ChatHandler.sendChat(playerIn, "pixelmon.blocks.ownerchanged", new Object[0]);
                        playerIn.addStat(PixelmonAchievements.givenPokeGift, 1);
                        return true;
                    }
                    ChatHandler.sendChat(playerIn, "pixelmon.blocks.fillmefirst", new Object[0]);
                    return false;
                }
                EntityPlayerMP playerMP = (EntityPlayerMP)playerIn;
                Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(playerMP);
                if (optstorage.isPresent()) {
                    PlayerStorage ownerStorage = optstorage.get();
                    if (ownerStorage.count() == 1) {
                        ChatHandler.sendChat(playerMP, "pixelmon.blocks.lastpoke", new Object[0]);
                        return false;
                    }
                    int[] firstPokeID = null;
                    for (int i = 0; i < 6; ++i) {
                        int[] pixelmonID = ownerStorage.getIDFromPosition(i);
                        if (!ownerStorage.hasSentOut(pixelmonID)) continue;
                        firstPokeID = pixelmonID;
                        i = 6;
                    }
                    EntityPixelmon currentPixelmon = tile.getPixelmon();
                    if (firstPokeID == null) {
                        if (currentPixelmon == null || !playerIn.isCreative()) {
                            ChatHandler.sendChat(playerIn, "pixelmon.blocks.nothingtoadd", new Object[0]);
                        } else {
                            String mode = "";
                            if (tile.getChestMode() && tile.getDropMode()) {
                                tile.setChestOneTime(false);
                                tile.setDropOneTime(true);
                                mode = I18n.translateToLocal("pixelmon.blocks.chestmodePL1D");
                            } else {
                                tile.setDropOneTime(true);
                                tile.setChestOneTime(true);
                                mode = I18n.translateToLocal("pixelmon.blocks.chestmodeFCFS");
                            }
                            ChatHandler.sendChat(playerIn, "pixelmon.blocks.chestmode", mode);
                        }
                        return false;
                    }
                    ownerStorage.recallAllPokemon();
                    NBTTagCompound nbt = ownerStorage.getNBT(firstPokeID);
                    int position = ownerStorage.getPosition(firstPokeID);
                    EntityPixelmon toGive = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(nbt, worldIn);
                    if (currentPixelmon != null) {
                        tile.setPixelmon(toGive);
                        NBTTagCompound currentNBT = new NBTTagCompound();
                        currentPixelmon.writeEntityToNBT(currentNBT);
                        ownerStorage.changePokemonAndAssignID(position, currentNBT);
                    } else {
                        tile.setPixelmon(toGive);
                        ownerStorage.changePokemonAndAssignID(position, null);
                    }
                    ownerStorage.sendUpdatedList();
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase player) {
        int i = MathHelper.floor((double)(player.rotationYaw * 4.0f / 360.0f) + 0.5) & 3;
        EnumFacing enumfacing = EnumFacing.byHorizontalIndex((int)i);
        return this.getDefaultState().withProperty(FACING, enumfacing);
    }

    @Override
    public void onBlockClicked(World world, BlockPos pos, EntityPlayer player) {
        TileEntityPokegift tile;
        UUID playerID;
        if (!world.isRemote && (playerID = player.getUniqueID()) == (tile = BlockHelper.getTileEntity(TileEntityPokegift.class, world, pos)).getOwner()) {
            Optional<PlayerStorage> optStorage;
            world.setBlockToAir(pos);
            DropItemHelper.giveItemStackToPlayer(player, new ItemStack(PixelmonBlocks.pokegiftBlock));
            EntityPixelmon pokemon = tile.getPixelmon();
            if (pokemon != null && (optStorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player)).isPresent()) {
                optStorage.get().addToParty(pokemon);
            }
        }
    }

    @Override
    public TileEntity createNewTileEntity(World par1World, int var1) {
        try {
            TileEntityPokegift tileP = this.pokeChestTileEntityClass.newInstance();
            return tileP;
        }
        catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void randomDisplayTick(IBlockState stateIn, World world, BlockPos pos, Random random) {
        TileEntityPokegift tile = BlockHelper.getTileEntity(TileEntityPokegift.class, world, pos);
        if (tile.getVisibility() == EnumPokechestVisibility.Hidden) {
            float rand = random.nextFloat() * 0.5f + 1.0f;
            world.spawnParticle(EnumParticleTypes.SPELL_INSTANT, (double)pos.getX() + 0.5, (double)pos.getY() + 0.5, (double)pos.getZ() + 0.5, this.xVel * (double)rand, this.yVel * (double)rand, this.zVel * (double)rand, new int[0]);
        }
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess access, BlockPos pos) {
        return AABB;
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBox(IBlockState state, World worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public void setOwner(BlockPos pos, EntityPlayer playerIn) {
        UUID playerID = playerIn.getUniqueID();
        TileEntityPokegift tile = BlockHelper.getTileEntity(TileEntityPokegift.class, playerIn.world, pos);
        tile.setOwner(playerID);
        if (PixelmonConfig.pokegiftMany) {
            tile.setChestOneTime(false);
        }
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return null;
    }
}

