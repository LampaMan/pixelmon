/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.common.block.enums.EnumDawnDuskOre;
import com.pixelmongenerations.common.spawning.spawners.SpawnerBase;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockDawnDuskOre
extends Block {
    public static final PropertyEnum<EnumDawnDuskOre> CONTENTS = PropertyEnum.create("contents", EnumDawnDuskOre.class);

    public BlockDawnDuskOre(float hardness) {
        super(Material.ROCK);
        this.setHardness(hardness);
        this.setSoundType(SoundType.STONE);
        this.setTickRandomly(true);
        this.setTranslationKey("dawnduskore");
        this.setCreativeTab(PixelmonCreativeTabs.natural);
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer((Block)this, CONTENTS);
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState().withProperty(CONTENTS, EnumDawnDuskOre.values()[meta]);
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        EnumDawnDuskOre type = state.getValue(CONTENTS);
        return type.ordinal();
    }

    @Override
    public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer) {
        world.scheduleBlockUpdate(pos, this, 20, 1);
        return super.getStateForPlacement(world, pos, facing, hitX, hitY, hitZ, meta, placer);
    }

    @Override
    public void updateTick(World world, BlockPos pos, IBlockState state, Random rand) {
        EnumDawnDuskOre contents;
        switch (SpawnerBase.getWorldState(world)) {
            case dawn: {
                contents = EnumDawnDuskOre.dawn;
                break;
            }
            case dusk: {
                contents = EnumDawnDuskOre.dusk;
                break;
            }
            default: {
                contents = EnumDawnDuskOre.none;
            }
        }
        world.setBlockState(pos, state.withProperty(CONTENTS, contents));
        world.scheduleBlockUpdate(pos, this, 20, 1);
        super.updateTick(world, pos, state, rand);
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.MODEL;
    }

    @Override
    public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
        EnumDawnDuskOre contents = state.getValue(CONTENTS);
        ArrayList<ItemStack> drops = new ArrayList<ItemStack>();
        switch (contents) {
            case dawn: {
                drops.add(new ItemStack(PixelmonItems.dawnStoneShard, RandomHelper.getFortuneAmount(fortune)));
                break;
            }
            case dusk: {
                drops.add(new ItemStack(PixelmonItems.duskStoneShard, RandomHelper.getFortuneAmount(fortune)));
                break;
            }
            case none: {
                drops.add(new ItemStack(Block.getBlockFromName("cobblestone")));
            }
        }
        return drops;
    }
}

