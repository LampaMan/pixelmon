/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.common.block.BlockStickPlate;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockDynamicMovementPlate
extends Block {
    private static final int MAX_LOOP_LENGTH = 100;
    private static final AxisAlignedBB AABB = new AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.0625, 1.0);

    public BlockDynamicMovementPlate() {
        super(Material.IRON);
        this.setHardness(2.5f);
        this.setCreativeTab(PixelmonCreativeTabs.utilityBlocks);
    }

    public void onEntityCollision(World worldIn, BlockPos pos, IBlockState state, Entity entity) {
        EnumFacing facing = entity.getAdjustedHorizontalFacing();
        double speedModifier = 1.0;
        if (this.targetSticky(worldIn, entity, pos)) {
            speedModifier = 0.4;
        }
        double distanceFromCenterX = entity.posX - (double)pos.getX() - 0.5;
        double distanceFromCenterZ = entity.posZ - (double)pos.getZ() - 0.5;
        double pushToMiddle = 0.2;
        if (facing == EnumFacing.NORTH || facing == EnumFacing.SOUTH) {
            entity.motionZ = (double)facing.getZOffset() * speedModifier;
            if (distanceFromCenterX > 0.1) {
                if (entity.motionX > -0.2) {
                    entity.motionX = -1.0 * pushToMiddle;
                }
            } else if (distanceFromCenterX < -0.1) {
                if (entity.motionX < 0.2) {
                    entity.motionX = pushToMiddle;
                }
            } else {
                entity.motionX = 0.0;
            }
        } else if (facing == EnumFacing.EAST || facing == EnumFacing.WEST) {
            entity.motionX = (double)facing.getXOffset() * speedModifier;
            if (distanceFromCenterZ > 0.1) {
                if (entity.motionZ > -0.2) {
                    entity.motionZ = -1.0 * pushToMiddle;
                }
            } else if (distanceFromCenterZ < -0.1) {
                if (entity.motionZ < 0.2) {
                    entity.motionZ = pushToMiddle;
                }
            } else {
                entity.motionZ = 0.0;
            }
        }
        entity.motionY = (double)facing.getYOffset() * speedModifier;
    }

    public boolean targetSticky(World world, Entity entity, BlockPos pos) {
        BlockPos targetPos = null;
        switch (entity.getAdjustedHorizontalFacing()) {
            case NORTH: {
                targetPos = pos.north();
                break;
            }
            case SOUTH: {
                targetPos = pos.south();
                break;
            }
            case EAST: {
                targetPos = pos.east();
                break;
            }
            case WEST: {
                targetPos = pos.west();
                break;
            }
        }
        if (targetPos != null) {
            return world.getBlockState(targetPos).getBlock() instanceof BlockStickPlate;
        }
        return false;
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBox(IBlockState state, World worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
        return AABB;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @SideOnly(value=Side.CLIENT)
    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.TRANSLUCENT;
    }
}

