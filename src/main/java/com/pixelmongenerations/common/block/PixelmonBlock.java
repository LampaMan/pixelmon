/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.BlockRenderLayer;

public class PixelmonBlock
extends Block {
    public PixelmonBlock(Material par2Material) {
        super(par2Material);
        this.setHardness(0.5f);
    }

    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.CUTOUT;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        if (this.material == Material.GLASS) {
            return false;
        }
        return super.isFullCube(state);
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return this.material != Material.GLASS;
    }

    public Block setSound(SoundType sand) {
        this.setSoundType(sand);
        return this;
    }
}

