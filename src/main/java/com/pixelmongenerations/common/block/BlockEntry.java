/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import net.minecraft.block.Block;

public class BlockEntry {
    public Block block;
    public int meta;

    public BlockEntry(Block block) {
        this(block, 0);
    }

    public BlockEntry(Block block, int meta) {
        this.block = block;
        this.meta = meta;
    }

    public BlockEntry(int vanillaID, int meta) {
        this(Block.getBlockById(vanillaID), meta);
    }
}

