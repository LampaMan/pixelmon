/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.config.PixelmonItemsFossils;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;

public class BlockFossil
extends Block {
    public BlockFossil() {
        super(Material.ROCK);
        this.setSoundType(SoundType.STONE);
        this.setCreativeTab(PixelmonCreativeTabs.natural);
        this.setTranslationKey("fossil");
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.MODEL;
    }

    @Override
    public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
        ArrayList<ItemStack> items = new ArrayList<ItemStack>();
        int amount = RandomHelper.getFortuneAmount(fortune);
        for (int i = 0; i < amount; ++i) {
            items.add(PixelmonItemsFossils.getRandomFossil());
        }
        return items;
    }
}

