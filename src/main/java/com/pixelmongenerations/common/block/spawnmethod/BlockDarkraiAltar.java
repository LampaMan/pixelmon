/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.spawnmethod;

import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityDarkraiAltar;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockDarkraiAltar
extends BlockShrine {
    public BlockDarkraiAltar() {
        this.setTranslationKey("darkrai_altar");
        this.setBlockUnbreakable();
    }

    @Override
    protected boolean checkExtraRequirements(EntityPlayer player, World world, BlockPos pos) {
        return false;
    }

    @Override
    public boolean canAccept(PokemonGroup group) {
        return false;
    }

    @Override
    public TileEntityShrine createTileEntity() {
        return new TileEntityDarkraiAltar();
    }
}

