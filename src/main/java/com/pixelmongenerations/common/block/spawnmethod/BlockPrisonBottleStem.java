/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.block.spawnmethod;

import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityPrisonBottleStem;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import javax.annotation.Nullable;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockPrisonBottleStem
extends GenericRotatableModelBlock {
    public BlockPrisonBottleStem() {
        super(Material.ROCK);
        this.setHardness(2.5f);
        this.setTranslationKey("prison_bottle_stem");
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        ItemStack heldItem = player.getHeldItem(hand);
        if (world.isRemote || hand == EnumHand.OFF_HAND) {
            return true;
        }
        TileEntityPrisonBottleStem tile = BlockHelper.getTileEntity(TileEntityPrisonBottleStem.class, world, pos);
        if (tile != null) {
            tile.activate(player, heldItem, pos.getX(), pos.getY(), pos.getZ());
        }
        return true;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityPrisonBottleStem();
    }

    @Override
    public boolean removedByPlayer(IBlockState state, World world, BlockPos pos, EntityPlayer player, boolean willHarvest) {
        if (willHarvest) {
            return true;
        }
        return super.removedByPlayer(state, world, pos, player, willHarvest);
    }

    @Override
    public void harvestBlock(World worldIn, EntityPlayer player, BlockPos pos, IBlockState state, @Nullable TileEntity te, ItemStack stack) {
        super.harvestBlock(worldIn, player, pos, state, te, stack);
        worldIn.setBlockToAir(pos);
    }

    @Override
    public void getDrops(NonNullList<ItemStack> drops, IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
        TileEntityPrisonBottleStem tileEntity = BlockHelper.getTileEntity(TileEntityPrisonBottleStem.class, world, pos);
        int ringCount = 0;
        if (tileEntity != null) {
            ringCount = tileEntity.getRingCount();
        }
        if (ringCount >= 6) {
            drops.add(new ItemStack(Item.getItemFromBlock(PixelmonBlocks.prisonBottle)));
        } else {
            drops.add(new ItemStack(Item.getItemFromBlock(this)));
            drops.add(new ItemStack(PixelmonItems.hoopaRing, ringCount));
        }
    }
}

