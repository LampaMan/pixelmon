/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.spawnmethod;

import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.block.enums.EnumUsed;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityBirdShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import net.minecraft.block.Block;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockBirdShrine
extends BlockShrine {
    public static final PropertyEnum<EnumUsed> USED = PropertyEnum.create("used", EnumUsed.class);
    private final EnumSpecies species;

    public BlockBirdShrine(String name, EnumSpecies species) {
        this.setTranslationKey(name);
        this.species = species;
        this.setDefaultState(this.getBlockState().getBaseState().withProperty(USED, EnumUsed.NO));
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer((Block)this, FACING, USED);
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState().withProperty(FACING, EnumFacing.byHorizontalIndex((int)(meta & 3))).withProperty(USED, EnumUsed.fromMeta((meta & 0xF) >> 2));
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        int b0 = 0;
        int i = b0 | state.getValue(FACING).getHorizontalIndex();
        return i | state.getValue(USED).getMeta() << 2;
    }

    @Override
    public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer) {
        IBlockState iblockstate = this.getDefaultState();
        if (facing.getAxis().isHorizontal()) {
            iblockstate = iblockstate.withProperty(FACING, facing).withProperty(USED, EnumUsed.NO);
        }
        return iblockstate;
    }

    @Override
    public TileEntityShrine createTileEntity() {
        return new TileEntityBirdShrine();
    }

    @Override
    protected boolean checkExtraRequirements(EntityPlayer player, World world, BlockPos pos) {
        if (world.getBlockState(pos).getValue(USED) == EnumUsed.NO || PixelmonConfig.reusableBirdShrines) {
            return true;
        }
        ChatHandler.sendChat(player, "pixelmon.blocks.shrineused", new Object[0]);
        return false;
    }

    public boolean getIsUsed(TileEntityShrine tile) {
        EnumUsed used = tile.getWorld().getBlockState(tile.getPos()).getValue(USED);
        return used == EnumUsed.YES;
    }

    @Override
    public int getWeakPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side) {
        TileEntityShrine tile = BlockHelper.getTileEntity(TileEntityShrine.class, blockAccess, pos);
        if (this.getIsUsed(tile)) {
            return 15;
        }
        return 0;
    }

    @Override
    public boolean canAccept(PokemonGroup group) {
        return group.contains(this.species);
    }

    public EnumSpecies getSpecies() {
        return this.species;
    }
}

