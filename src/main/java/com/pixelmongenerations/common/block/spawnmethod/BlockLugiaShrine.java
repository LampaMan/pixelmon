/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.spawnmethod;

import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityLugiaShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import com.pixelmongenerations.core.enums.EnumSpecies;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockLugiaShrine
extends BlockShrine {
    @Override
    public TileEntityShrine createTileEntity() {
        return new TileEntityLugiaShrine();
    }

    @Override
    protected boolean checkExtraRequirements(EntityPlayer player, World world, BlockPos pos) {
        return true;
    }

    @Override
    public boolean canAccept(PokemonGroup group) {
        return group.contains(EnumSpecies.Articuno) || group.contains(EnumSpecies.Zapdos) || group.contains(EnumSpecies.Moltres) || group.contains(EnumSpecies.Lugia);
    }
}

