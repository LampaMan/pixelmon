/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.spawnmethod;

import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.block.decorative.BlockUnown;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityRegiShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.functional.TriFunction;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockRegiShrine
extends BlockShrine {
    private static final BiFunction<World, BlockPos, String> function = (world, pos) -> world.getBlockState((BlockPos)pos).getBlock() == PixelmonBlocks.fancyPillar || world.getBlockState((BlockPos)pos).getBlock() == PixelmonBlocks.castlePillar ? BlockUnown.symbolFromState(world.getBlockState(pos.up())).orElse("-") : "-";
    private static final TriFunction<World, EnumFacing, BlockPos, String> getSymbolSequence = (world, facing, pos) -> function.apply((World)world, pos.offset((EnumFacing)facing)) + function.apply((World)world, (BlockPos)pos) + function.apply((World)world, pos.offset(facing.getOpposite()));
    private static final BiFunction<String, Integer, String> substring = (cipher, i) -> cipher.substring((int)i, i + 3);
    private EnumSpecies species;
    private List<String> list;

    public BlockRegiShrine(EnumSpecies species) {
        String cipher = "-" + species.name.toUpperCase() + "-";
        this.list = IntStream.range(0, cipher.length() - 2).boxed().map(a -> substring.apply(cipher, (Integer)a)).collect(Collectors.toList());
        this.species = species;
        this.setTranslationKey(species.name.toLowerCase() + "_shrine");
    }

    @Override
    public TileEntityShrine createTileEntity() {
        return new TileEntityRegiShrine();
    }

    @Override
    public boolean canAccept(PokemonGroup group) {
        return group.contains(this.species);
    }

    public EnumSpecies getSpecies() {
        return this.species;
    }

    public List<BlockPos> checkForUnownSequence(World world, BlockPos pos) {
        BiFunction<EnumFacing, BlockPos, String> getIndex = (facing, blockPos) -> getSymbolSequence.apply(world, (EnumFacing)facing, (BlockPos)blockPos);
        return this.checkIfInSequence(EnumFacing.Axis.Z, pos, getIndex).orElse(this.checkIfInSequence(EnumFacing.Axis.X, pos, getIndex).orElseGet(ArrayList::new));
    }

    private Optional<List<BlockPos>> checkIfInSequence(EnumFacing.Axis axis, BlockPos pos, BiFunction<EnumFacing, BlockPos, String> getSymbol) {
        EnumFacing facing = EnumFacing.getFacingFromAxis(EnumFacing.AxisDirection.POSITIVE, axis);
        String sequence = getSymbol.apply(facing, pos);
        if (!this.list.contains(sequence)) {
            if (!this.list.contains(sequence = new StringBuilder(sequence).reverse().toString())) {
                return Optional.empty();
            }
            facing = facing.getOpposite();
        }
        BlockPos base = pos.offset(facing, this.list.indexOf(sequence));
        ArrayList<BlockPos> positions = new ArrayList<BlockPos>();
        for (int i = 0; i < this.list.size(); ++i) {
            BlockPos f = base.offset(facing.getOpposite(), i);
            if (!this.list.contains(getSymbol.apply(facing, f))) continue;
            positions.add(f);
        }
        return !positions.isEmpty() && positions.size() == this.list.size() ? Optional.of(positions) : Optional.empty();
    }

    @Override
    protected boolean checkExtraRequirements(EntityPlayer player, World world, BlockPos pos) {
        PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player).get();
        return IntStream.range(0, 6).mapToObj(storage::getIDFromPosition).map(storage::getNBT).filter(Objects::nonNull).map(a -> a.getString("Name")).map(EnumSpecies::getFromNameAnyCase).anyMatch(a -> a == EnumSpecies.Regigigas);
    }
}

