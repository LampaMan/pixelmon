/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.spawnmethod;

import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityPrisonBottle;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import java.util.Random;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockPrisonBottle
extends GenericRotatableModelBlock {
    public BlockPrisonBottle() {
        super(Material.ROCK);
        this.setHardness(2.5f);
        this.setTranslationKey("prison_bottle");
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return true;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityPrisonBottle();
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return Item.getItemFromBlock(PixelmonBlocks.prisonBottle);
    }
}

