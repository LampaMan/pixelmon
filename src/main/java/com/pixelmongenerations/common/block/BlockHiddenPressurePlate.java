/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import java.util.Random;
import net.minecraft.block.BlockPressurePlate;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class BlockHiddenPressurePlate
extends BlockPressurePlate {
    public BlockHiddenPressurePlate() {
        super(Material.WOOD, BlockPressurePlate.Sensitivity.MOBS);
        this.setHardness(1.0f);
        this.setBlockUnbreakable();
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return Item.getItemFromBlock(this);
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        return new ItemStack(Item.getItemFromBlock(this));
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.INVISIBLE;
    }

    @Override
    public int quantityDropped(Random random) {
        return 1;
    }
}

