/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.decorative.BlockCouchBase;
import com.pixelmongenerations.common.block.tileEntities.TileEntityCouchArmLeft;

public class BlockCouchArmLeft
extends BlockCouchBase<TileEntityCouchArmLeft> {
    public BlockCouchArmLeft() {
        super(TileEntityCouchArmLeft.class);
        this.setTranslationKey("couch_arm_left");
    }
}

