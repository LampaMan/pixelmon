/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityDisplayball;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockDisplayball
extends GenericRotatableModelBlock {
    public String name;
    public String modelName;

    public BlockDisplayball(String name, String modelName) {
        super(Material.ROCK);
        this.setSoundType(SoundType.STONE);
        this.setTranslationKey("displayball_" + name);
        this.setHardness(1.0f);
        this.setCreativeTab(PixelmonCreativeTabs.decoration);
        this.name = name;
        this.modelName = modelName;
        this.setLightLevel(1.0f);
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityDisplayball(this.name, this.modelName);
    }
}

