/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityLitwickCandles;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockLitwickCandles
extends GenericRotatableModelBlock {
    public BlockLitwickCandles() {
        super(Material.WOOD);
        this.setHardness(0.5f);
        this.setLightLevel(1.0f);
        this.setTranslationKey("litwick_candles");
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityLitwickCandles();
    }

    public Item getDroppedItem(World world, BlockPos pos) {
        return Item.getItemFromBlock(this);
    }
}

