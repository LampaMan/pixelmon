/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.decorative.BlockCouchBase;
import com.pixelmongenerations.common.block.tileEntities.TileEntityCouchArmRight;

public class BlockCouchArmRight
extends BlockCouchBase<TileEntityCouchArmRight> {
    public BlockCouchArmRight() {
        super(TileEntityCouchArmRight.class);
        this.setTranslationKey("couch_arm_right");
    }
}

