/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.generic.GenericBlock;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import java.util.Optional;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockUnown
extends GenericBlock {
    static PropertyInteger alphabetIndex = PropertyInteger.create("index", 0, 15);
    public String[] alphabetInUse = null;
    private static final String[] alphabet1 = new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P"};
    private static final String[] alphabet2 = new String[]{"Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "!", "?", "_"};

    public BlockUnown(boolean secondHalf) {
        super(Material.ROCK);
        this.setHardness(5.0f);
        this.alphabetInUse = secondHalf ? alphabet2 : alphabet1;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public void getSubBlocks(CreativeTabs creativeTabs, NonNullList<ItemStack> list) {
        for (int meta = 0; meta < this.alphabetInUse.length; ++meta) {
            list.add(new ItemStack(this, 1, meta));
        }
    }

    @Override
    public int damageDropped(IBlockState state) {
        return state.getValue(alphabetIndex);
    }

    @Override
    public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer) {
        return super.getStateForPlacement(worldIn, pos, facing, hitX, hitY, hitZ, meta, placer).withProperty(alphabetIndex, meta);
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState().withProperty(alphabetIndex, meta);
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return state.getValue(alphabetIndex);
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer((Block)this, alphabetIndex);
    }

    public static int getNumUnownBlocks() {
        return alphabet1.length + alphabet2.length;
    }

    public static int getBlankUnownBlockIndex() {
        return alphabet2.length - 1;
    }

    public static Optional<String> symbolFromState(IBlockState state) {
        if (state.getBlock() instanceof BlockUnown) {
            int index = state.getValue(alphabetIndex);
            String[] alphabet = ((BlockUnown)state.getBlock()).alphabetInUse;
            String letter = alphabet[index];
            return Optional.of(letter);
        }
        return Optional.empty();
    }

    public static IBlockState stateFromSymbol(String character) {
        BlockUnown block = (BlockUnown)PixelmonBlocks.blockUnown;
        int index = BlockUnown.findIndex(block.alphabetInUse, character);
        if (index == -1) {
            block = (BlockUnown)PixelmonBlocks.blockUnown2;
            index = BlockUnown.findIndex(block.alphabetInUse, character);
            if (index == -1) {
                index = BlockUnown.getBlankUnownBlockIndex();
            }
        }
        return block.getDefaultState().withProperty(alphabetIndex, index);
    }

    private static int findIndex(String[] alphabet, String character) {
        int found = -1;
        for (int i = 0; i < alphabet.length; ++i) {
            if (!alphabet[i].equals(character)) continue;
            found = i;
            break;
        }
        return found;
    }
}

