/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.enums.CushionEnum;
import com.pixelmongenerations.common.block.generic.GenericRotatableSittableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityFloorCushion;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.World;

public class FloorCushionBlock
extends GenericRotatableSittableModelBlock {
    private CushionEnum cushion;

    public FloorCushionBlock(CushionEnum color) {
        super(Material.IRON, (AxisAlignedBB)null);
        this.setHardness(1.0f);
        this.setSoundType(SoundType.METAL);
        this.cushion = color;
        this.setTranslationKey((Object)((Object)color) + "_floorcushion");
    }

    @Override
    public float getSittingHeight() {
        return 0.4f;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityFloorCushion();
    }

    public CushionEnum getColor() {
        return this.cushion;
    }
}

