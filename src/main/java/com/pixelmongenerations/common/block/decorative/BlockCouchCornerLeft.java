/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.decorative.BlockCouchBase;
import com.pixelmongenerations.common.block.tileEntities.TileEntityCouchCornerLeft;

public class BlockCouchCornerLeft
extends BlockCouchBase<TileEntityCouchCornerLeft> {
    public BlockCouchCornerLeft() {
        super(TileEntityCouchCornerLeft.class);
        this.setTranslationKey("couch_corner_left");
    }
}

