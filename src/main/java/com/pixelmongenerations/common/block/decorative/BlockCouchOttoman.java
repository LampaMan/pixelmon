/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.decorative.BlockCouchBase;
import com.pixelmongenerations.common.block.tileEntities.TileEntityCouchOttoman;

public class BlockCouchOttoman
extends BlockCouchBase<TileEntityCouchOttoman> {
    public BlockCouchOttoman() {
        super(TileEntityCouchOttoman.class);
        this.setTranslationKey("couch_ottoman");
    }
}

