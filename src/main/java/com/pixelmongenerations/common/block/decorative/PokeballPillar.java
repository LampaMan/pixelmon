/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.multiBlocks.BlockGenericModelMultiblock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityPokeballPillar;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import java.util.Optional;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class PokeballPillar
extends BlockGenericModelMultiblock {
    public PokeballPillar() {
        super(Material.ROCK, 1, 2.0, 1);
        this.setHardness(1.0f);
        this.setSoundType(SoundType.STONE);
        this.setTranslationKey("pokeball_pillar");
    }

    @Override
    protected Optional<TileEntity> getTileEntity(World var1, IBlockState var2) {
        return Optional.of(new TileEntityPokeballPillar());
    }

    @Override
    public Item getDroppedItem(World world, BlockPos pos) {
        return Item.getItemFromBlock(PixelmonBlocks.pokeballPillarBlock);
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }
}

