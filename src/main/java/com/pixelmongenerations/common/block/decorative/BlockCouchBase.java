/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.base.Enums
 */
package com.pixelmongenerations.common.block.decorative;

import com.google.common.base.Enums;
import com.pixelmongenerations.common.block.generic.GenericRotatableSittableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityCouchBase;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockCouchBase<T extends TileEntityCouchBase>
extends GenericRotatableSittableModelBlock {
    private final Class<T> type;

    public BlockCouchBase(Class<T> type) {
        super(Material.WOOD, (AxisAlignedBB)null);
        this.type = type;
        this.setSoundType(SoundType.WOOD);
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        try {
            return (TileEntity)this.type.newInstance();
        }
        catch (IllegalAccessException | InstantiationException e) {
            return null;
        }
    }

    @Override
    public float getSittingHeight() {
        return 0.3f;
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        TileEntityCouchBase tileEntityPC;
        ItemStack heldItem;
        if (!world.isRemote && hand == EnumHand.MAIN_HAND && !(heldItem = player.getHeldItem(hand)).isEmpty() && heldItem.getItem() instanceof ItemDye && (tileEntityPC = (TileEntityCouchBase)BlockHelper.getTileEntity(this.type, world, pos)) != null) {
            EnumDyeColor color = (EnumDyeColor)Enums.getIfPresent(EnumDyeColor.class, (String)tileEntityPC.getColour().toUpperCase()).orNull();
            EnumDyeColor dyeColor = EnumDyeColor.byDyeDamage(heldItem.getItemDamage());
            if (color != null && color == dyeColor) {
                return super.onBlockActivated(world, pos, state, player, hand, facing, hitX, hitY, hitZ);
            }
            if (color != null) {
                ItemStack stack = new ItemStack(Items.DYE);
                stack.setItemDamage(color.getDyeDamage());
                world.spawnEntity(new EntityItem(world, (double)pos.getX() + 0.5, (double)pos.getY() + 0.5, (double)pos.getZ() + 0.5, stack));
            }
            tileEntityPC.setColour(dyeColor.getName());
            if (!player.capabilities.isCreativeMode) {
                heldItem.shrink(1);
            }
            return true;
        }
        return super.onBlockActivated(world, pos, state, player, hand, facing, hitX, hitY, hitZ);
    }
}

