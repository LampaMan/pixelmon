/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityEndTable;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import net.minecraft.block.BlockPlanks;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class EndTableBlock
extends GenericRotatableModelBlock {
    private BlockPlanks.EnumType wood;

    public EndTableBlock(BlockPlanks.EnumType wood) {
        super(Material.WOOD);
        this.wood = wood;
        this.setHardness(1.0f);
        this.setSoundType(SoundType.WOOD);
        this.setTranslationKey("end_table" + (wood != BlockPlanks.EnumType.OAK ? "_" + wood.getName() : ""));
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (world.isRemote || hand == EnumHand.OFF_HAND) {
            return true;
        }
        TileEntityEndTable tile = BlockHelper.getTileEntity(TileEntityEndTable.class, world, pos);
        if (tile != null) {
            if (tile.isOpen()) {
                if (player.isSneaking()) {
                    tile.closeDrawer();
                } else {
                    player.displayGUIChest(tile);
                }
            } else {
                tile.openDrawer();
            }
        }
        return true;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityEndTable();
    }

    @Override
    public void breakBlock(World world, BlockPos pos, IBlockState state) {
        TileEntityEndTable tile = BlockHelper.getTileEntity(TileEntityEndTable.class, world, pos);
        if (tile != null) {
            InventoryHelper.dropInventoryItems(world, pos, (IInventory)tile);
        }
        super.breakBlock(world, pos, state);
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return face == EnumFacing.UP ? BlockFaceShape.SOLID : BlockFaceShape.UNDEFINED;
    }

    public BlockPlanks.EnumType getWood() {
        return this.wood;
    }
}

