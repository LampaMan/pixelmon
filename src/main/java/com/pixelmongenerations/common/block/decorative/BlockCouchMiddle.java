/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.decorative.BlockCouchBase;
import com.pixelmongenerations.common.block.tileEntities.TileEntityCouchMiddle;

public class BlockCouchMiddle
extends BlockCouchBase<TileEntityCouchMiddle> {
    public BlockCouchMiddle() {
        super(TileEntityCouchMiddle.class);
        this.setTranslationKey("couch_middle");
    }
}

