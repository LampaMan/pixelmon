/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.generic.GenericModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityTrashcan;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class TrashcanBlock
extends GenericModelBlock {
    private static final AxisAlignedBB AABB = new AxisAlignedBB(0.3f, 0.0, 0.3f, 0.7f, 0.5, 0.7f);

    public TrashcanBlock() {
        super(Material.IRON);
        this.setHardness(1.0f);
        this.setSoundType(SoundType.METAL);
        this.setTranslationKey("Trashcan");
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (world.isRemote || hand == EnumHand.OFF_HAND) {
            return true;
        }
        TileEntityTrashcan tileEntity = BlockHelper.getTileEntity(TileEntityTrashcan.class, world, pos);
        if (tileEntity != null) {
            player.openGui(Pixelmon.INSTANCE, EnumGui.TrashCan.getIndex(), world, pos.getX(), pos.getY(), pos.getZ());
        }
        return true;
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBox(IBlockState state, World worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityTrashcan();
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return face == EnumFacing.NORTH ? BlockFaceShape.SOLID : BlockFaceShape.UNDEFINED;
    }
}

