/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.IBlockHasOwner;
import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityGymSign;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import java.util.UUID;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

public class GymSignBlock
extends GenericRotatableModelBlock
implements IBlockHasOwner {
    public GymSignBlock() {
        super(Material.IRON);
        this.setHardness(1.0f);
        this.setSoundType(SoundType.METAL);
        this.setTranslationKey("GymSign");
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityGymSign();
    }

    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
        super.onBlockPlacedBy(worldIn, pos, state, placer, stack);
        if (placer instanceof EntityPlayer && !worldIn.isRemote) {
            this.setOwner(pos, (EntityPlayer)placer);
        }
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        TileEntityGymSign gymSign;
        ItemStack heldItem = player.getHeldItem(hand);
        if (!world.isRemote && hand != EnumHand.OFF_HAND && (gymSign = BlockHelper.getTileEntity(TileEntityGymSign.class, world, pos)) != null) {
            if (player.getUniqueID().equals(gymSign.getOwnerUUID())) {
                Item playerItem = null;
                if (!heldItem.isEmpty()) {
                    playerItem = heldItem.getItem();
                }
                boolean itemUsed = false;
                if (playerItem instanceof ItemDye) {
                    String dyeName;
                    EnumDyeColor dyeColor = EnumDyeColor.byDyeDamage(heldItem.getItemDamage());
                    String currentColour = gymSign.getColour();
                    if (!currentColour.equals(dyeName = dyeColor.getName())) {
                        gymSign.setColour(dyeName);
                        ((WorldServer)world).getPlayerChunkMap().markBlockForUpdate(pos);
                        if (!player.capabilities.isCreativeMode) {
                            heldItem.shrink(1);
                        }
                        itemUsed = true;
                    }
                }
                if (!itemUsed) {
                    ItemStack signStack = gymSign.getItemInSign();
                    ItemStack putting = null;
                    if (!heldItem.isEmpty() && signStack != null && heldItem.getItem() == signStack.getItem()) {
                        putting = null;
                    }
                    if (!heldItem.isEmpty()) {
                        putting = heldItem.copy();
                        putting.setCount(1);
                        if (!player.capabilities.isCreativeMode) {
                            heldItem.splitStack(1);
                        }
                    }
                    if (signStack != null && signStack != ItemStack.EMPTY && !player.capabilities.isCreativeMode) {
                        DropItemHelper.giveItemStackToPlayer(player, signStack);
                    }
                    gymSign.setItemInSign(putting);
                    ((WorldServer)world).getPlayerChunkMap().markBlockForUpdate(pos);
                }
            } else {
                ChatHandler.sendChat(player, "pixelmon.blocks.gymsign.ownership", new Object[0]);
            }
        }
        return true;
    }

    @Override
    public boolean removedByPlayer(IBlockState state, World world, BlockPos pos, EntityPlayer player, boolean willHarvest) {
        if (!player.capabilities.isCreativeMode) {
            this.onPlayerDestroy(world, pos, world.getBlockState(pos));
        }
        return super.removedByPlayer(state, world, pos, player, willHarvest);
    }

    public void onPlayerDestroy(World world, BlockPos pos, IBlockState state) {
        TileEntityGymSign gymSign;
        if (!world.isRemote && (gymSign = BlockHelper.getTileEntity(TileEntityGymSign.class, world, pos)) != null && gymSign.getItemInSign() != null && gymSign.isDroppable()) {
            ItemStack stack = gymSign.getItemInSign();
            stack.setCount(1);
            GymSignBlock.spawnAsEntity(world, pos, stack);
        }
    }

    @Override
    public void setOwner(BlockPos pos, EntityPlayer playerIn) {
        UUID playerID = playerIn.getUniqueID();
        TileEntityGymSign gymSign = BlockHelper.getTileEntity(TileEntityGymSign.class, playerIn.getEntityWorld(), pos);
        gymSign.setOwner(playerID);
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }
}

