/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.generic.GenericRotatableSittableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityBeanBag;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.World;

public class BeanBagBlock
extends GenericRotatableSittableModelBlock {
    public BeanBagBlock() {
        super(Material.WOOD, (AxisAlignedBB)null);
        this.setSoundType(SoundType.WOOD);
        this.setTranslationKey("bean_bag");
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityBeanBag();
    }

    @Override
    public float getSittingHeight() {
        return 0.8f;
    }
}

