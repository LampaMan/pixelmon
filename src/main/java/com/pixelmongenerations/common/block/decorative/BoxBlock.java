/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityBox;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BoxBlock
extends GenericRotatableModelBlock {
    private static final AxisAlignedBB AABB = new AxisAlignedBB(0.2f, 0.0, 0.2f, 0.8f, 0.6f, 0.8f);

    public BoxBlock() {
        super(Material.ROCK);
        this.setHardness(0.5f);
        this.setSoundType(SoundType.WOOD);
        this.setTranslationKey("Box");
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBox(IBlockState state, World worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (world.isRemote || hand == EnumHand.OFF_HAND) {
            return true;
        }
        TileEntityBox tile = BlockHelper.getTileEntity(TileEntityBox.class, world, pos);
        if (tile.isOpen()) {
            if (player.isSneaking()) {
                tile.closeBox(world.getMinecraftServer());
            } else {
                player.displayGUIChest(tile);
            }
        } else {
            tile.openBox();
        }
        return true;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityBox();
    }

    @Override
    public void breakBlock(World world, BlockPos pos, IBlockState state) {
        TileEntityBox tile = BlockHelper.getTileEntity(TileEntityBox.class, world, pos);
        if (tile != null) {
            InventoryHelper.dropInventoryItems(world, pos, (IInventory)tile);
        }
        super.breakBlock(world, pos, state);
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }
}

