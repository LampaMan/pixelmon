/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.multiBlocks.BlockGenericModelMultiblock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityBench;
import com.pixelmongenerations.common.entity.EntityChairMount;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Explosion;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BenchBlock
extends BlockGenericModelMultiblock {
    private static final AxisAlignedBB AABB = new AxisAlignedBB(0.1f, 0.0, 0.1f, 0.9f, 0.5, 0.9f);

    public BenchBlock() {
        super(Material.IRON, 2, 1.0, 1);
        this.setHardness(1.0f);
        this.setSoundType(SoundType.WOOD);
        this.setTranslationKey("bench");
        this.setHarvestLevel("axe", 0);
    }

    @Override
    protected Optional<TileEntity> getTileEntity(World var1, IBlockState var2) {
        return Optional.of(new TileEntityBench());
    }

    @Override
    public Item getDroppedItem(World world, BlockPos pos) {
        return Item.getItemFromBlock(PixelmonBlocks.benchBlock);
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess access, BlockPos pos) {
        return AABB;
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBox(IBlockState state, World worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        if (hand == EnumHand.OFF_HAND) {
            return false;
        }
        return this.mountBlock(world, pos, player);
    }

    public void onPlayerDestroy(World worldIn, BlockPos pos, IBlockState state) {
        this.unMountBlock(worldIn, pos);
        super.onPlayerDestroy(worldIn, pos, state);
    }

    public void onExplosionDestroy(World worldIn, BlockPos pos, Explosion explosionIn) {
        this.unMountBlock(worldIn, pos);
        super.onExplosionDestroy(worldIn, pos, explosionIn);
    }

    private void unMountBlock(World world, BlockPos pos) {
        List<EntityChairMount> list = world.getEntitiesWithinAABB(EntityChairMount.class, new AxisAlignedBB(pos.getX(), pos.getY() - 1, pos.getZ(), pos.getX() + 1, pos.getY() + 2, pos.getZ() + 1));
        Iterator<EntityChairMount> iterator = list.iterator();
        if (iterator.hasNext()) {
            Entity entity = iterator.next();
            entity.removePassengers();
            return;
        }
    }

    public boolean mountBlock(World world, BlockPos pos, EntityPlayer player) {
        if (world.isRemote) {
            return true;
        }
        List<Entity> list = world.getEntitiesWithinAABB(Entity.class, new AxisAlignedBB(pos.getX(), pos.getY() - 1, pos.getZ(), pos.getX() + 1, pos.getY() + 2, pos.getZ() + 1));
        for (Entity entity : list) {
            if (!(entity instanceof EntityChairMount)) continue;
            return false;
        }
        EntityChairMount mount = new EntityChairMount(world);
        mount.setPosition((float)pos.getX() + 0.5f, (float)pos.getY() - 0.65f + this.getSittingHeight(), (double)pos.getZ() + 0.5);
        world.spawnEntity(mount);
        player.startRiding(mount);
        return true;
    }

    public float getSittingHeight() {
        return 0.45f;
    }
}

