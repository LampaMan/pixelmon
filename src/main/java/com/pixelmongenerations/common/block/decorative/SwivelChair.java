/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.enums.ColorEnum;
import com.pixelmongenerations.common.block.generic.GenericRotatableSittableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntitySwivelChair;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.World;

public class SwivelChair
extends GenericRotatableSittableModelBlock {
    private ColorEnum color;

    public SwivelChair(ColorEnum color) {
        super(Material.WOOD, new AxisAlignedBB(0.1f, 0.0, 0.1f, 0.9f, 0.5, 0.9f));
        this.color = color;
        this.setHardness(0.5f);
        this.setSoundType(SoundType.CLOTH);
        this.setTranslationKey((Object)((Object)color) + "SwivelChair");
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntitySwivelChair();
    }

    public ColorEnum getColor() {
        return this.color;
    }

    @Override
    public float getSittingHeight() {
        return 0.45f;
    }
}

