/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.multiBlocks.BlockGenericModelMultiblock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShopFront;
import com.pixelmongenerations.core.enums.EnumShopFrontType;
import java.util.Optional;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockShopFront
extends BlockGenericModelMultiblock {
    private EnumShopFrontType type;

    public BlockShopFront(EnumShopFrontType type) {
        super(Material.IRON, 2, type.getHeight(), 1);
        this.type = type;
        this.setHardness(1.0f);
        this.setSoundType(SoundType.METAL);
        this.setTranslationKey("shopfront_" + type.name().toLowerCase());
        this.setHarvestLevel("axe", 0);
    }

    public EnumShopFrontType getShopFrontType() {
        return this.type;
    }

    @Override
    protected Optional<TileEntity> getTileEntity(World var1, IBlockState var2) {
        return Optional.of(new TileEntityShopFront());
    }

    @Override
    public Item getDroppedItem(World world, BlockPos pos) {
        return Item.getItemFromBlock(this);
    }
}

