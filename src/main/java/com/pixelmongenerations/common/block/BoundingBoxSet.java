/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import net.minecraft.util.math.AxisAlignedBB;

public class BoundingBoxSet {
    public AxisAlignedBB AABBBaseEast;
    public AxisAlignedBB AABBBaseWest;
    public AxisAlignedBB AABBBaseSouth;
    public AxisAlignedBB AABBBaseNorth;

    public BoundingBoxSet(int width, double height, int length) {
        this.AABBBaseSouth = new AxisAlignedBB(-width + 1, 0.0, 0.0, 1.0, height, length);
        this.AABBBaseNorth = new AxisAlignedBB(0.0, 0.0, 0.0, width, height, length);
        this.AABBBaseEast = new AxisAlignedBB(0.0, 0.0, 0.0, length, height, width);
        this.AABBBaseWest = new AxisAlignedBB(0.0, 0.0, (float)(-1 * width) + 1.0f, length, height, 1.0);
    }
}

