/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumBlockRenderType;

public class BlockLeafEvolutionOreOverlay
extends Block {
    public BlockLeafEvolutionOreOverlay(String itemName) {
        super(Material.ROCK);
        this.setHardness(-1.0f);
        this.setSoundType(SoundType.GROUND);
        this.setCreativeTab(null);
        this.setTranslationKey(itemName);
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.MODEL;
    }
}

