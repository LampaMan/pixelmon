/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.ranch;

import com.pixelmongenerations.common.block.ranch.BreedingConditions;
import com.pixelmongenerations.common.block.tileEntities.TileEntityRanchBase;
import com.pixelmongenerations.core.util.Bounds;
import java.util.ArrayList;
import java.util.Random;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public class RanchBounds
extends Bounds {
    public final TileEntityRanchBase ranch;
    private int originalTop;
    private int originalLeft;
    private int originalBottom;
    private int originalRight;
    private int height;
    private static final int HEIGHT_OFFSET = 3;

    public RanchBounds(TileEntityRanchBase tileEntityRanchBase) {
        this.ranch = tileEntityRanchBase;
    }

    public RanchBounds(TileEntityRanchBase ranch, int top, int left, int bottom, int right, int height) {
        super(top, left, bottom, right);
        this.ranch = ranch;
        this.originalTop = top;
        this.originalLeft = left;
        this.originalBottom = bottom;
        this.originalRight = right;
        this.height = height + 3;
    }

    @Override
    public void writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        nbt.setInteger("origTop", this.originalTop);
        nbt.setInteger("origLeft", this.originalLeft);
        nbt.setInteger("origBot", this.originalBottom);
        nbt.setInteger("origRight", this.originalRight);
        nbt.setInteger("height", this.height);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        this.originalTop = nbt.getInteger("origTop");
        this.originalLeft = nbt.getInteger("origLeft");
        this.originalBottom = nbt.getInteger("origBot");
        this.originalRight = nbt.getInteger("origRight");
        this.height = nbt.getInteger("height");
    }

    public BreedingConditions getContainingBreedingConditions(World world) {
        ArrayList<String> blockList = new ArrayList<String>();
        for (int x = this.left; x <= this.right; ++x) {
            for (int z = this.bottom; z <= this.top; ++z) {
                BlockPos pos;
                if (x == (this.left + this.right) / 2 && z == (this.top + this.bottom) / 2 || (pos = this.getTopBlock(world, new BlockPos(x, 0, z))).getY() <= -1) continue;
                blockList.add(world.getBlockState(pos.down()).getBlock().getRegistryName().toString());
                if (world.getBlockState(pos.up()).getMaterial() == Material.AIR) continue;
                blockList.add(world.getBlockState(pos).getBlock().getRegistryType().toString());
            }
        }
        return new BreedingConditions(blockList);
    }

    public BlockPos getTopBlock(World world, BlockPos pos) {
        BlockPos blockPos2;
        IBlockState state;
        Chunk chunk = world.getChunk(pos);
        BlockPos blockPos1 = new BlockPos(pos.getX(), this.height, pos.getZ());
        while (blockPos1.getY() >= 0 && (state = chunk.getBlockState(blockPos2 = blockPos1.down())).getMaterial() == Material.AIR) {
            blockPos1 = blockPos2;
        }
        return blockPos1;
    }

    @Override
    public boolean canExtend(int top, int left, int bottom, int right) {
        if (this.top + top - this.originalTop >= 4) {
            return false;
        }
        if (this.left - left - this.originalLeft <= -4) {
            return false;
        }
        if (this.bottom - bottom - this.originalBottom <= -4) {
            return false;
        }
        return this.right + right - this.originalRight < 4;
    }

    @Override
    public void Extend(EntityPlayerMP player, int top, int left, int bottom, int right) {
        if (!this.canExtend(top, left, bottom, right)) {
            return;
        }
        this.top += top;
        this.left -= left;
        this.bottom -= bottom;
        this.right += right;
        player.connection.sendPacket(this.ranch.getUpdatePacket());
    }

    @Override
    public boolean canExtend() {
        return this.canExtend(1, 0, 0, 0) || this.canExtend(0, 1, 0, 0) || this.canExtend(0, 0, 1, 0) || this.canExtend(0, 0, 0, 1);
    }

    public int[] getRandomLocation(Random rand) {
        int[] xz = new int[]{this.left + rand.nextInt(this.right - this.left + 1), this.bottom + rand.nextInt(this.top - this.bottom + 1)};
        return xz;
    }

    public int getUpgradeCount() {
        int botUpgrades = this.originalBottom - this.bottom;
        int leftUpgrades = this.originalLeft - this.left;
        int rightUpgrades = this.right - this.originalRight;
        int topUpgrades = this.top - this.originalTop;
        return botUpgrades + topUpgrades + leftUpgrades + rightUpgrades;
    }
}

