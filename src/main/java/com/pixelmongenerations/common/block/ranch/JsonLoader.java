/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.reflect.TypeToken
 *  com.google.gson.Gson
 *  com.google.gson.GsonBuilder
 */
package com.pixelmongenerations.common.block.ranch;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class JsonLoader {
    public static Gson gsonInstance = new GsonBuilder().setPrettyPrinting().create();

    public static <T> T setup(String name, TypeToken<T> token, Supplier<T> supplier, boolean useExternal) throws FileNotFoundException {
        return JsonLoader.setup(name, token, supplier, gsonInstance, useExternal);
    }

    public static <T> T setup(String name, TypeToken<T> token, Supplier<T> supplier, Gson gsonInstance, boolean useExternal) throws FileNotFoundException {
        Object t;
        try {
            File baseDir = new File(Pixelmon.modDirectory + "/pixelmon");
            if (PixelmonConfig.useExternalJSONFilesSpawnColors) {
                if (!baseDir.isDirectory()) {
                    baseDir.mkdir();
                    Pixelmon.LOGGER.info("Creating Pixelmon directory.");
                }
                JsonLoader.extract(name, baseDir);
            }
            InputStream iStream = !useExternal ? JsonLoader.class.getResourceAsStream("/assets/pixelmon/" + name + ".json") : new FileInputStream(new File(baseDir, name + ".json"));
            String s = new BufferedReader(new InputStreamReader(iStream, StandardCharsets.UTF_8)).lines().collect(Collectors.joining(""));
            t = gsonInstance.fromJson(s, token.getType());
            iStream.close();
            Pixelmon.LOGGER.info("Loaded " + name);
        }
        catch (Exception e) {
            t = null;
            Pixelmon.LOGGER.error("Failed to setup " + name);
            e.printStackTrace();
        }
        if (t == null) {
            t = supplier.get();
        }
        return (T)t;
    }

    private static void extract(String name, File dataDir) {
        ServerNPCRegistry.extractFile("/assets/pixelmon/" + name + ".json", dataDir, name + ".json");
    }
}

