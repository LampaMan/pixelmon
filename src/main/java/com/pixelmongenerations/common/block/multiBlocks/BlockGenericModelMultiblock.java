/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.multiBlocks;

import com.pixelmongenerations.common.block.MultiBlock;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public abstract class BlockGenericModelMultiblock
extends MultiBlock {
    protected BlockGenericModelMultiblock(Material material, int width, double height, int length) {
        super(material, width, height, length);
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public Item getDroppedItem(World world, BlockPos pos) {
        return null;
    }
}

