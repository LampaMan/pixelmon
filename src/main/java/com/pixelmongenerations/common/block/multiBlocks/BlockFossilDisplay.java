/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.block.multiBlocks;

import com.pixelmongenerations.common.block.IBlockHasOwner;
import com.pixelmongenerations.common.block.enums.ColorEnum;
import com.pixelmongenerations.common.block.multiBlocks.BlockGenericModelMultiblock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityFossilDisplay;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.common.item.ItemFossil;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.annotation.Nullable;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

public class BlockFossilDisplay
extends BlockGenericModelMultiblock
implements IBlockHasOwner {
    private ColorEnum color = null;

    public BlockFossilDisplay() {
        super(Material.ROCK, 1, 2.0, 1);
        this.setTranslationKey("FossilDisplay");
        this.setHardness(2.0f);
    }

    public BlockFossilDisplay(ColorEnum color) {
        this();
        this.color = color;
        this.setTranslationKey((Object)((Object)color) + "FossilDisplay");
    }

    @Override
    public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
        List<ItemStack> drops = super.getDrops(world, pos, state, fortune);
        BlockPos base = this.findBaseBlock(world, new BlockPos.MutableBlockPos(pos), world.getBlockState(pos));
        TileEntityFossilDisplay fossilDisplayEntity = BlockHelper.getTileEntity(TileEntityFossilDisplay.class, world, base);
        ItemFossil displayItem = fossilDisplayEntity.getItemInDisplay();
        if (displayItem != null) {
            drops.add(new ItemStack(displayItem));
        }
        drops.add(new ItemStack(PixelmonBlocks.fossilDisplayBlock, 1));
        return drops;
    }

    @Override
    protected Optional<TileEntity> getTileEntity(World world, IBlockState state) {
        return Optional.of(new TileEntityFossilDisplay());
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (!world.isRemote) {
            ItemStack heldItem = player.getHeldItem(hand);
            TileEntityFossilDisplay fossilDisplayEntity = BlockHelper.getTileEntity(TileEntityFossilDisplay.class, world, pos = this.findBaseBlock(world, new BlockPos.MutableBlockPos(pos), state));
            if (fossilDisplayEntity != null) {
                if (player.getUniqueID().equals(fossilDisplayEntity.getOwnerUUID())) {
                    if (fossilDisplayEntity.isOpen()) {
                        ItemFossil item = fossilDisplayEntity.getItemInDisplay();
                        if (item != null) {
                            if (heldItem.isEmpty() && !player.isSneaking()) {
                                fossilDisplayEntity.setItemInDisplay(null);
                                ((WorldServer)world).getPlayerChunkMap().markBlockForUpdate(pos);
                                if (!player.capabilities.isCreativeMode) {
                                    DropItemHelper.giveItemStackToPlayer(player, new ItemStack(item));
                                }
                            } else {
                                fossilDisplayEntity.closeGlass();
                            }
                        } else {
                            Item playerItem = heldItem.isEmpty() ? null : heldItem.getItem();
                            Item item2 = playerItem;
                            if (!heldItem.isEmpty() && !player.isSneaking()) {
                                if (playerItem instanceof ItemFossil) {
                                    fossilDisplayEntity.setItemInDisplay(playerItem);
                                    ((WorldServer)world).getPlayerChunkMap().markBlockForUpdate(pos);
                                    if (!player.capabilities.isCreativeMode) {
                                        heldItem.shrink(1);
                                    }
                                    return true;
                                }
                                ChatHandler.sendChat(player, "pixelmon.blocks.fossildisplay.placecheck", new Object[0]);
                                fossilDisplayEntity.closeGlass();
                            } else {
                                fossilDisplayEntity.closeGlass();
                                ((WorldServer)world).getPlayerChunkMap().markBlockForUpdate(pos);
                            }
                        }
                    } else {
                        fossilDisplayEntity.openGlass();
                    }
                } else {
                    ChatHandler.sendChat(player, "pixelmon.blocks.fossildisplay.ownership", new Object[0]);
                }
            }
        }
        return true;
    }

    @Override
    public void setOwner(BlockPos pos, EntityPlayer playerIn) {
        UUID playerID = playerIn.getUniqueID();
        TileEntityFossilDisplay fossilDisplay = BlockHelper.getTileEntity(TileEntityFossilDisplay.class, playerIn.world, pos);
        fossilDisplay.setOwner(playerID);
    }

    @Override
    @Nullable
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityFossilDisplay();
    }

    public ColorEnum getColor() {
        return this.color;
    }
}

