/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.multiBlocks;

import com.pixelmongenerations.common.block.multiBlocks.BlockGenericModelMultiblock;
import com.pixelmongenerations.common.entity.EntityChairMount;
import java.util.List;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public abstract class BlockGenericSittableModelMultiblock
extends BlockGenericModelMultiblock {
    private static final AxisAlignedBB AABB = new AxisAlignedBB(0.1, 0.0, 0.1, 0.9, 0.5, 0.9);

    protected BlockGenericSittableModelMultiblock(Material material, int width, double height, int length) {
        super(material, width, height, length);
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBox(IBlockState state, World worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (hand == EnumHand.OFF_HAND) {
            return true;
        }
        return this.mountBlock(world, pos, player);
    }

    public boolean mountBlock(World world, BlockPos pos, EntityPlayer player) {
        if (world.isRemote) {
            return true;
        }
        List<Entity> list = world.getEntitiesWithinAABB(Entity.class, new AxisAlignedBB(pos.getX(), pos.getY() - 1, pos.getZ(), pos.getX() + 1, pos.getY() + 2, pos.getZ() + 1));
        for (Entity entity : list) {
            if (!(entity instanceof EntityChairMount)) continue;
            return false;
        }
        EntityChairMount mount = new EntityChairMount(world);
        mount.setPosition((float)pos.getX() + 0.5f, (float)pos.getY() - 0.65f + this.getSittingHeight(), (double)pos.getZ() + 0.5);
        world.spawnEntity(mount);
        player.startRiding(mount);
        return true;
    }

    abstract float getSittingHeight();
}

