/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.multiBlocks;

import com.pixelmongenerations.common.block.multiBlocks.BlockGenericModelMultiblock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityFridge;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import java.util.List;
import java.util.Optional;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockFridge
extends BlockGenericModelMultiblock {
    public BlockFridge() {
        super(Material.ROCK, 1, 2.0, 1);
        this.setHardness(0.5f);
        this.setSoundType(SoundType.WOOD);
        this.setTranslationKey("Fridge");
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        TileEntityFridge fridgeEntity;
        if (!(world.isRemote && hand != EnumHand.OFF_HAND || (fridgeEntity = BlockHelper.getTileEntity(TileEntityFridge.class, world, pos = this.findBaseBlock(world, new BlockPos.MutableBlockPos(pos), state))) == null)) {
            if (fridgeEntity.isOpenDoor()) {
                if (player.isSneaking()) {
                    fridgeEntity.closeFridge();
                } else {
                    player.displayGUIChest(fridgeEntity);
                }
            } else {
                fridgeEntity.openFridge();
            }
        }
        return true;
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        return new ItemStack(PixelmonBlocks.fridgeBlock);
    }

    @Override
    public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
        List<ItemStack> drops = super.getDrops(world, pos, state, fortune);
        drops.add(new ItemStack(PixelmonBlocks.fridgeBlock, 1));
        return drops;
    }

    @Override
    protected Optional<TileEntity> getTileEntity(World world, IBlockState state) {
        return Optional.of(new TileEntityFridge());
    }

    @Override
    public void breakBlock(World world, BlockPos pos, IBlockState state) {
        BlockPos base = this.findBaseBlock(world, new BlockPos.MutableBlockPos(pos), world.getBlockState(pos));
        TileEntityFridge fridgeEntity = BlockHelper.getTileEntity(TileEntityFridge.class, world, base);
        if (fridgeEntity != null) {
            InventoryHelper.dropInventoryItems(world, pos, (IInventory)fridgeEntity);
        }
        super.breakBlock(world, pos, state);
    }
}

