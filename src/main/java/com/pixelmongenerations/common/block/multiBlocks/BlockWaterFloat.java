/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.multiBlocks;

import com.pixelmongenerations.common.block.enums.ColorEnum;
import com.pixelmongenerations.common.block.multiBlocks.BlockGenericSittableModelMultiblock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityWaterFloat;
import java.util.Optional;
import java.util.Random;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockWaterFloat
extends BlockGenericSittableModelMultiblock {
    private static final AxisAlignedBB AABB = new AxisAlignedBB(0.1, 0.0, 0.1, 0.9, 0.1875, 0.9);
    private ColorEnum color;

    public BlockWaterFloat(ColorEnum color) {
        super(Material.CLOTH, 2, 0.1875, 1);
        this.color = color;
        this.setHardness(1.0f);
        this.setSoundType(SoundType.CLOTH);
        this.setTranslationKey((Object)((Object)color) + "_water_float");
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBox(IBlockState state, World worldIn, BlockPos pos) {
        return AABB;
    }

    @SideOnly(value=Side.CLIENT)
    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.SOLID;
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        return new ItemStack(this.getFloatItem());
    }

    public Item getFloatItem() {
        return Item.getItemFromBlock(this);
    }

    @Override
    public int quantityDropped(Random random) {
        return 1;
    }

    @Override
    public Item getDroppedItem(World world, BlockPos pos) {
        return this.getFloatItem();
    }

    @Override
    public Optional<TileEntity> getTileEntity(World world, IBlockState state) {
        return Optional.of(new TileEntityWaterFloat());
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return face == EnumFacing.DOWN ? BlockFaceShape.SOLID : BlockFaceShape.UNDEFINED;
    }

    public ColorEnum getColor() {
        return this.color;
    }

    @Override
    float getSittingHeight() {
        return 0.16f;
    }
}

