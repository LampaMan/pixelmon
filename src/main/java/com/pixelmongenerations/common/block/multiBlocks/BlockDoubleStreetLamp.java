/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.multiBlocks;

import com.pixelmongenerations.common.block.multiBlocks.BlockGenericModelMultiblock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityDoubleStreetLamp;
import java.util.Optional;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockDoubleStreetLamp
extends BlockGenericModelMultiblock {
    public BlockDoubleStreetLamp() {
        super(Material.IRON, 1, 4.0, 1);
        this.setTranslationKey("double_street_lamp");
        this.setHardness(2.0f);
        this.setLightLevel(1.0f);
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        return new ItemStack(this.getDroppedItem(null, null));
    }

    @Override
    public Item getDroppedItem(World world, BlockPos pos) {
        return Item.getItemFromBlock(this);
    }

    @Override
    protected Optional<TileEntity> getTileEntity(World var1, IBlockState var2) {
        return Optional.of(new TileEntityDoubleStreetLamp());
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return face == EnumFacing.DOWN ? BlockFaceShape.SOLID : BlockFaceShape.UNDEFINED;
    }
}

