/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.block.multiBlocks;

import com.pixelmongenerations.common.block.enums.EnumMultiPos;
import com.pixelmongenerations.common.block.multiBlocks.BlockGenericModelMultiblock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityClothedTable;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import java.util.Optional;
import javax.annotation.Nullable;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockClothedTable
extends BlockGenericModelMultiblock {
    private static AxisAlignedBB AABBEast;
    private static AxisAlignedBB AABBWest;
    private static AxisAlignedBB AABBSouth;
    private static AxisAlignedBB AABBNorth;

    public BlockClothedTable() {
        super(Material.WOOD, 2, 1.0, 2);
        this.setHardness(0.5f);
        this.setTranslationKey("clothed_table");
        this.setSoundType(SoundType.WOOD);
    }

    @Override
    protected AxisAlignedBB getMultiBlockBoundingBox(IBlockAccess worldIn, BlockPos pos, EnumMultiPos multiPos, EnumFacing facing) {
        EnumFacing blockFacing;
        if (AABBEast == null) {
            AABBEast = new AxisAlignedBB(0.0, 0.0, 0.0, this.width, 1.0, this.length);
            AABBWest = new AxisAlignedBB(-this.width + 1, 0.0, -this.length + 1, 1.0, 1.0, 1.0);
            AABBSouth = new AxisAlignedBB(1.0, 0.0, 0.0, 1 - this.length, 1.0, this.width);
            AABBNorth = new AxisAlignedBB(0.0, 0.0, 1.0, this.length, 1.0, 1 - this.width);
        }
        if (multiPos == EnumMultiPos.BASE) {
            return facing == EnumFacing.EAST ? AABBEast : (facing == EnumFacing.WEST ? AABBWest : (facing == EnumFacing.SOUTH ? AABBSouth : AABBNorth));
        }
        BlockPos base = this.findBaseBlock(worldIn, new BlockPos.MutableBlockPos(pos), worldIn.getBlockState(pos));
        IBlockState baseBlock = worldIn.getBlockState(base);
        if (baseBlock.getMaterial() == Material.AIR) {
            return null;
        }
        try {
            blockFacing = baseBlock.getValue(FACING);
        }
        catch (IllegalArgumentException var9) {
            return null;
        }
        return this.getMultiBlockBoundingBox(worldIn, base, EnumMultiPos.BASE, blockFacing).offset(base.getX() - pos.getX(), base.getY() - pos.getY(), base.getZ() - pos.getZ());
    }

    @Override
    public void onBlockHarvested(World worldIn, BlockPos pos, IBlockState state, EntityPlayer player) {
        if (!worldIn.isRemote) {
            ItemStack itemStack = new ItemStack(PixelmonBlocks.clothedTableBlock, 1, 0);
            worldIn.spawnEntity(new EntityItem(worldIn, pos.getX(), pos.getY(), pos.getZ(), itemStack));
        }
    }

    @Override
    public Optional<TileEntity> getTileEntity(World world, IBlockState state) {
        return Optional.of(new TileEntityClothedTable());
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }

    @Override
    @Nullable
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityClothedTable();
    }
}

