/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.multiBlocks;

import com.pixelmongenerations.common.block.IBlockHasOwner;
import com.pixelmongenerations.common.block.multiBlocks.BlockGenericModelMultiblock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityTree;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

public class BlockTree
extends BlockGenericModelMultiblock
implements IBlockHasOwner {
    public BlockTree() {
        super(Material.WOOD, 1, 2.0, 1);
        this.setHardness(0.5f);
    }

    @Override
    public Item getDroppedItem(World world, BlockPos pos) {
        return PixelmonItems.treeItem;
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        return new ItemStack(PixelmonItems.treeItem);
    }

    @Override
    protected Optional<TileEntity> getTileEntity(World world, IBlockState state) {
        return Optional.of(new TileEntityTree());
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        TileEntityTree treeEntity;
        if ((!world.isRemote || hand == EnumHand.OFF_HAND) && (treeEntity = BlockHelper.getTileEntity(TileEntityTree.class, world, pos = this.findBaseBlock(world, new BlockPos.MutableBlockPos(pos), state))) != null && player.getUniqueID().equals(treeEntity.getOwnerUUID())) {
            if (treeEntity.getTreeType() < 4) {
                treeEntity.setTreeType(treeEntity.getTreeType() + 1);
            } else {
                treeEntity.setTreeType(1);
            }
            ((WorldServer)world).getPlayerChunkMap().markBlockForUpdate(pos);
            ChatHandler.sendChat(player, "pixelmon.blocks.tree.change" + treeEntity.getTreeType(), new Object[0]);
        }
        return true;
    }

    @Override
    public void setOwner(BlockPos pos, EntityPlayer playerIn) {
        UUID playerID = playerIn.getUniqueID();
        TileEntityTree tree = BlockHelper.getTileEntity(TileEntityTree.class, playerIn.world, pos);
        tree.setOwner(playerID);
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }
}

