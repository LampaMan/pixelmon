/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.spawning;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.block.machines.PokemonRarity;
import com.pixelmongenerations.common.block.spawning.BlockPixelmonSpawner;
import com.pixelmongenerations.common.block.spawning.BlockSpawningHandler;
import com.pixelmongenerations.common.block.spawning.TileEntityPixelmonGrass;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.enums.battle.EnumBattleStartTypes;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.Optional;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockCustomPixelmonGrass
extends BlockContainer {
    private static final AxisAlignedBB AABB = new AxisAlignedBB(0.1, 0.0, 0.1, 0.9, 0.8, 0.9);

    public BlockCustomPixelmonGrass() {
        super(Material.PLANTS);
        this.setTickRandomly(true);
        this.setSoundType(SoundType.PLANT);
        this.setBlockUnbreakable();
        this.setResistance(6000000.0f);
        this.setCreativeTab(PixelmonCreativeTabs.utilityBlocks);
        this.setTranslationKey("pixelmonCustomGrass");
    }

    @Override
    public void onEntityWalk(World worldIn, BlockPos pos, Entity entityIn) {
        if (entityIn instanceof EntityPlayerMP && !worldIn.isRemote) {
            this.onCollide((EntityPlayerMP)entityIn, worldIn, pos);
        }
    }

    public void onEntityCollision(World worldIn, BlockPos pos, IBlockState state, Entity entityIn) {
        if (entityIn instanceof EntityPlayerMP && !worldIn.isRemote) {
            this.onCollide((EntityPlayerMP)entityIn, worldIn, pos);
        }
    }

    public void onCollide(EntityPlayerMP player, World worldIn, BlockPos pos) {
        if (BattleRegistry.getBattle(player) != null) {
            return;
        }
        Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
        if (optstorage.isPresent()) {
            PokemonRarity rarity;
            TileEntityPixelmonGrass grass;
            PlayerStorage storage = optstorage.get();
            if (storage.getTicksTillEncounter() <= 1 && player.posY == (double)pos.getY() && (grass = BlockHelper.getTileEntity(TileEntityPixelmonGrass.class, worldIn, pos)) != null && !grass.pokemonList.isEmpty() && (rarity = grass.selectPokemonForSpawn()) != null) {
                EntityPixelmon startingPixelmon;
                PokemonSpec spec = PokemonSpec.from(rarity.pokemon.name);
                spec.form = rarity.form;
                spec.specialTexture = rarity.texture;
                spec.boss = null;
                spec.level = worldIn.rand.nextInt(grass.levelMax + 1 - grass.levelMin) + grass.levelMin;
                spec.shiny = RandomHelper.getRandomChance(1.0f / (float)rarity.shinyChance);
                EntityPixelmon pokemon = spec.create(worldIn);
                if (grass.bossRatio > 0 && worldIn.rand.nextInt(grass.bossRatio) == 0) {
                    pokemon.setBoss(EnumBossMode.getRandomMode());
                }
                if ((startingPixelmon = storage.getFirstAblePokemon(worldIn)) != null) {
                    startingPixelmon.loadMoveset();
                    pokemon.setPosition((double)pos.getX() + 0.5, pos.getY(), (double)pos.getZ() + 0.5);
                    worldIn.spawnEntity(pokemon);
                    BlockSpawningHandler.getInstance().performBattleStartCheck(worldIn, pos, player, pokemon, "", EnumBattleStartTypes.CUSTOMGRASS);
                }
            }
            storage.updateTicksTillEncounter();
        }
    }

    @Override
    public boolean isReplaceable(IBlockAccess worldIn, BlockPos pos) {
        return true;
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return null;
    }

    @Override
    public int quantityDroppedWithBonus(int fortune, Random random) {
        return 1 + random.nextInt(fortune * 2 + 1);
    }

    @Override
    public int damageDropped(IBlockState state) {
        return state.getBlock().getMetaFromState(state);
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return 0;
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState();
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer((Block)this, new IProperty[0]);
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBox(IBlockState state, World worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos) {
        return null;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.MODEL;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }

    @SideOnly(value=Side.CLIENT)
    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.CUTOUT;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityPixelmonGrass();
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (player instanceof EntityPlayerMP && hand == EnumHand.MAIN_HAND && BlockPixelmonSpawner.checkPermission((EntityPlayerMP)player)) {
            TileEntityPixelmonGrass grass = BlockHelper.getTileEntity(TileEntityPixelmonGrass.class, world, pos);
            ((EntityPlayerMP)player).connection.sendPacket(grass.getUpdatePacket());
            grass.onActivate();
            player.openGui(Pixelmon.INSTANCE, EnumGui.PixelmonCustomGrass.getIndex(), world, pos.getX(), pos.getY(), pos.getZ());
            return true;
        }
        return true;
    }

    public static boolean checkPermission(EntityPlayerMP player) {
        if (!PixelmonConfig.opToUseSpawners && player.capabilities.isCreativeMode) {
            return true;
        }
        if (PixelmonConfig.opToUseSpawners && player.canUseCommand(4, "pixelmon.customgrass.use")) {
            return true;
        }
        ChatHandler.sendChat(player, "pixelmon.general.needop", new Object[0]);
        return false;
    }
}

