/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.spawning;

import com.pixelmongenerations.common.block.spawning.TileEntityPixelmonSpawner;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockPixelmonSpawner
extends BlockContainer {
    public BlockPixelmonSpawner() {
        super(Material.WOOD);
        this.setBlockUnbreakable();
        this.setResistance(6000000.0f);
        this.setCreativeTab(PixelmonCreativeTabs.utilityBlocks);
        this.setTranslationKey("pixelmonSpawner");
    }

    @Override
    public TileEntity createNewTileEntity(World world, int var1) {
        return new TileEntityPixelmonSpawner();
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (player instanceof EntityPlayerMP && hand == EnumHand.MAIN_HAND && BlockPixelmonSpawner.checkPermission((EntityPlayerMP)player)) {
            TileEntityPixelmonSpawner spawner = BlockHelper.getTileEntity(TileEntityPixelmonSpawner.class, world, pos);
            ((EntityPlayerMP)player).connection.sendPacket(spawner.getUpdatePacket());
            spawner.onActivate();
            player.openGui(Pixelmon.INSTANCE, EnumGui.PixelmonSpawner.getIndex(), world, pos.getX(), pos.getY(), pos.getZ());
            return true;
        }
        return true;
    }

    @Override
    public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
        TileEntityPixelmonSpawner spawner = BlockHelper.getTileEntity(TileEntityPixelmonSpawner.class, worldIn, pos);
        if (spawner != null) {
            spawner.despawnAllPokemon();
        }
        super.breakBlock(worldIn, pos, state);
    }

    @Override
    public boolean canConnectRedstone(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing side) {
        return true;
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.MODEL;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @SideOnly(value=Side.CLIENT)
    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.CUTOUT_MIPPED;
    }

    @Override
    public void neighborChanged(IBlockState state, World world, BlockPos pos, Block blockIn, BlockPos fromPos) {
        boolean flag = world.isBlockPowered(pos) || world.isBlockPowered(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ()));
        boolean bl = flag;
        if (flag) {
            world.scheduleUpdate(pos, this, this.tickRate(world));
        }
    }

    @Override
    public void updateTick(World world, BlockPos pos, IBlockState state, Random rand) {
        TileEntityPixelmonSpawner spawner = BlockHelper.getTileEntity(TileEntityPixelmonSpawner.class, world, pos);
        spawner.updateRedstone();
    }

    @Override
    public int tickRate(World par1World) {
        return 4;
    }

    public static boolean checkPermission(EntityPlayerMP player) {
        if (!PixelmonConfig.opToUseSpawners && player.capabilities.isCreativeMode) {
            return true;
        }
        if (PixelmonConfig.opToUseSpawners && player.canUseCommand(4, "pixelmon.spawner.use")) {
            return true;
        }
        ChatHandler.sendChat(player, "pixelmon.general.needop", new Object[0]);
        return false;
    }
}

