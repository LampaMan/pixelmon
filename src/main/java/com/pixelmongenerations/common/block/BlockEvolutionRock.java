/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.common.block.MultiBlock;
import com.pixelmongenerations.common.block.enums.EnumMultiPos;
import com.pixelmongenerations.common.block.tileEntities.TileEntityEvolutionRock;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.enums.EnumEvolutionRock;
import java.util.Optional;
import java.util.Random;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockEvolutionRock
extends MultiBlock {
    private static AxisAlignedBB AABBEast;
    private static AxisAlignedBB AABBWest;
    private static AxisAlignedBB AABBSouth;
    private static AxisAlignedBB AABBNorth;
    public EnumEvolutionRock rockType;

    public BlockEvolutionRock(Material par2Material, EnumEvolutionRock rockType) {
        super(par2Material, 3, 2.0, 3);
        this.rockType = rockType;
        if (this.rockType == EnumEvolutionRock.MossyRock) {
            this.setTranslationKey("mossyrock");
        } else {
            this.setTranslationKey("icyrock");
        }
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        if (this.rockType == EnumEvolutionRock.MossyRock) {
            return new ItemStack(PixelmonBlocks.mossyRock);
        }
        return new ItemStack(PixelmonBlocks.icyRock);
    }

    @Override
    protected AxisAlignedBB getMultiBlockBoundingBox(IBlockAccess worldIn, BlockPos pos, EnumMultiPos multipos, EnumFacing facing) {
        if (AABBEast == null) {
            AABBEast = new AxisAlignedBB(0.0, 0.0, 0.0, this.width, 2.0, this.length);
            AABBWest = new AxisAlignedBB(-this.width + 1, 0.0, -this.length + 1, 1.0, 2.0, 1.0);
            AABBSouth = new AxisAlignedBB(1.0, 0.0, 0.0, 1 - this.length, 2.0, this.width);
            AABBNorth = new AxisAlignedBB(0.0, 0.0, 1.0, this.length, 2.0, 1 - this.width);
        }
        if (multipos == EnumMultiPos.BASE) {
            if (facing == EnumFacing.EAST) {
                return AABBEast;
            }
            if (facing == EnumFacing.WEST) {
                return AABBWest;
            }
            if (facing == EnumFacing.SOUTH) {
                return AABBSouth;
            }
            return AABBNorth;
        }
        BlockPos base = this.findBaseBlock(worldIn, new BlockPos.MutableBlockPos(pos), worldIn.getBlockState(pos));
        try {
            return this.getMultiBlockBoundingBox(worldIn, base, EnumMultiPos.BASE, worldIn.getBlockState(base).getValue(FACING)).offset(base.getX() - pos.getX(), base.getY() - pos.getY(), base.getZ() - pos.getZ());
        }
        catch (IllegalArgumentException e) {
            return new AxisAlignedBB(0.0, 0.0, 0.0, this.width, this.height, this.length);
        }
    }

    @Override
    public int quantityDropped(Random random) {
        return 0;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }

    @Override
    public Item getDroppedItem(World world, BlockPos pos) {
        return null;
    }

    @Override
    protected Optional<TileEntity> getTileEntity(World world, IBlockState state) {
        return Optional.of(new TileEntityEvolutionRock());
    }
}

