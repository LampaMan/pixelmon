/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;

public interface IBlockHasOwner {
    public void setOwner(BlockPos var1, EntityPlayer var2);
}

