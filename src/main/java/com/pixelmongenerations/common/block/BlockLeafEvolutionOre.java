/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumEvolutionStone;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockLeafEvolutionOre
extends Block {
    public BlockLeafEvolutionOre(EnumEvolutionStone type, float hardness, String itemName) {
        super(Material.ROCK);
        this.setHardness(hardness);
        this.setSoundType(SoundType.STONE);
        this.setCreativeTab(PixelmonCreativeTabs.natural);
        this.setTickRandomly(true);
        this.setTranslationKey(itemName);
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @SideOnly(value=Side.CLIENT)
    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.CUTOUT_MIPPED;
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.MODEL;
    }

    @Override
    public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
        ArrayList<ItemStack> drops = new ArrayList<ItemStack>();
        drops.add(new ItemStack(PixelmonItems.leafStoneShard, RandomHelper.getFortuneAmount(fortune)));
        return drops;
    }
}

