/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.enums;

import net.minecraft.util.IStringSerializable;

public enum EnumUsed implements IStringSerializable
{
    YES,
    NO;


    @Override
    public String getName() {
        return this.toString();
    }

    public int getMeta() {
        return this.ordinal();
    }

    public static EnumUsed fromMeta(int i) {
        return EnumUsed.values()[i];
    }

    public String toString() {
        return super.toString().toLowerCase();
    }
}

