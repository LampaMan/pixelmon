/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.enums;

public enum ColorEnum {
    Blue,
    Red,
    Green,
    Orange,
    Pink,
    Purple,
    Yellow,
    Black,
    Brown,
    Cyan,
    Grey,
    LightBlue,
    LightGrey,
    Lime,
    Magenta,
    White;

}

