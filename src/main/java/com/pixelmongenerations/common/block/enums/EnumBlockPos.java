/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.enums;

import net.minecraft.util.IStringSerializable;

public enum EnumBlockPos implements IStringSerializable
{
    TOP,
    BOTTOM;


    @Override
    public String getName() {
        return this.toString();
    }

    public int toMeta() {
        return this.ordinal();
    }

    public static EnumBlockPos fromMeta(int meta) {
        return EnumBlockPos.values()[meta];
    }

    public String toString() {
        return super.toString().toLowerCase();
    }
}

