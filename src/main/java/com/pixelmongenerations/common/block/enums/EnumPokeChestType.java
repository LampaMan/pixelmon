/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.enums;

public enum EnumPokeChestType {
    POKEBALL(true, "Poke"),
    ULTRABALL(true, "Ultra"),
    MASTERBALL(true, "Master"),
    SPECIAL(false),
    BEASTBALL(true, "Beast"),
    POKESTOP(false);

    private boolean pokemonHabitable;
    private String lootPrefix;

    private EnumPokeChestType(boolean pokemonHabitable) {
        this(pokemonHabitable, "");
    }

    private EnumPokeChestType(boolean pokemonHabitable, String lootPrefix) {
        this.pokemonHabitable = pokemonHabitable;
        this.lootPrefix = lootPrefix;
    }

    public boolean isPokemonHabitable() {
        return this.pokemonHabitable;
    }

    public String getLootPrefix() {
        return this.lootPrefix;
    }
}

