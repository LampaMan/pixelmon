/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.enums;

import net.minecraft.util.text.translation.I18n;

public enum EnumSpawnerAggression {
    Default,
    Timid,
    Passive,
    Aggressive;


    public static EnumSpawnerAggression getFromOrdinal(int ordinal) {
        try {
            return EnumSpawnerAggression.values()[ordinal];
        }
        catch (IndexOutOfBoundsException e) {
            return Default;
        }
    }

    public static EnumSpawnerAggression nextAggression(EnumSpawnerAggression aggression) {
        return EnumSpawnerAggression.getFromOrdinal((aggression.ordinal() + 1) % EnumSpawnerAggression.values().length);
    }

    public String getLocalizedName() {
        return I18n.translateToLocal("enum.aggro." + this.toString().toLowerCase());
    }
}

