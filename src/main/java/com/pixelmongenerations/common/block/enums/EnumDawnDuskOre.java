/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.enums;

import net.minecraft.util.IStringSerializable;

public enum EnumDawnDuskOre implements IStringSerializable
{
    dawn,
    dusk,
    none;


    public static EnumDawnDuskOre getValue(int index) {
        return index >= EnumDawnDuskOre.values().length || index < 0 ? none : EnumDawnDuskOre.values()[index];
    }

    @Override
    public String getName() {
        return this.toString();
    }
}

