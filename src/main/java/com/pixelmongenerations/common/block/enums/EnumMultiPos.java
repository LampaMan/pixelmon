/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.enums;

import net.minecraft.util.IStringSerializable;

public enum EnumMultiPos implements IStringSerializable
{
    TOP,
    BOTTOM,
    BASE;


    @Override
    public String getName() {
        return this.toString();
    }

    public int toMeta() {
        return this.ordinal();
    }

    public static EnumMultiPos fromMeta(int i) {
        return EnumMultiPos.values()[i];
    }

    public String toString() {
        return super.toString().toLowerCase();
    }
}

