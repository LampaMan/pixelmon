/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nonnull
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import javax.annotation.Nonnull;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IStringSerializable;

public abstract class BlockPixelmonSlab
extends BlockSlab {
    public static final PropertyEnum<EnumVariant> VARIANT = PropertyEnum.create("variant", EnumVariant.class);

    public BlockPixelmonSlab(Block parentBlock) {
        super(parentBlock.getMaterial(null));
        this.setHardness(parentBlock.getBlockHardness(null, null, null));
        this.setCreativeTab(PixelmonCreativeTabs.buildingBlocks);
        this.setSoundType(parentBlock.getSoundType());
        IBlockState state = this.blockState.getBaseState().withProperty(VARIANT, EnumVariant.DEFAULT);
        if (!this.isDouble()) {
            state = state.withProperty(BlockSlab.HALF, BlockSlab.EnumBlockHalf.BOTTOM);
        }
        this.setDefaultState(state);
        this.useNeighborBrightness = !this.isDouble();
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return this.isDouble() ? new BlockStateContainer((Block)this, VARIANT) : new BlockStateContainer((Block)this, HALF, VARIANT);
    }

    @Nonnull
    public String getTranslationKey(int meta) {
        return this.getTranslationKey();
    }

    @Override
    public boolean isDouble() {
        return false;
    }

    @Override
    @Nonnull
    public IProperty<?> getVariantProperty() {
        return VARIANT;
    }

    @Override
    @Nonnull
    public Comparable<?> getTypeForItem(ItemStack stack) {
        return EnumVariant.DEFAULT;
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        if (this.isDouble()) {
            return 0;
        }
        return ((BlockSlab.EnumBlockHalf)state.getValue(HALF)).ordinal();
    }

    @Override
    @Nonnull
    public IBlockState getStateFromMeta(int meta) {
        if (this.isDouble()) {
            return this.getDefaultState();
        }
        return this.getDefaultState().withProperty(HALF, BlockSlab.EnumBlockHalf.values()[meta % 2]);
    }

    public static class Half
    extends BlockPixelmonSlab {
        public Half(Block parentBlock) {
            super(parentBlock);
        }

        @Override
        public boolean isDouble() {
            return false;
        }
    }

    public static class Double
    extends BlockPixelmonSlab {
        public Double(Block parentBlock) {
            super(parentBlock);
        }

        @Override
        public boolean isDouble() {
            return true;
        }
    }

    public static enum EnumVariant implements IStringSerializable
    {
        DEFAULT;


        @Override
        @Nonnull
        public String getName() {
            return "default";
        }
    }
}

