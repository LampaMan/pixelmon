/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.decorative.SwivelChair;
import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class TileEntitySwivelChair
extends TileEntity
implements ISpecialTexture {
    @Override
    public ResourceLocation getTexture() {
        IBlockState state = this.getWorld().getBlockState(this.getPos());
        if (state.getBlock() instanceof SwivelChair) {
            SwivelChair block = (SwivelChair)state.getBlock();
            return new ResourceLocation("pixelmon:textures/blocks/swivelchairs/" + block.getColor().name().toLowerCase() + "swivelchair.png");
        }
        return new ResourceLocation("pixelmon:textures/blocks/swivelchairs/redswivelchair.png");
    }
}

