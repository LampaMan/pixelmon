/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.event.ForgeListener;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.util.PixelSounds;
import java.util.List;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;

public class TileEntityDarkraiAltar
extends TileEntityShrine {
    @Override
    public void activate(EntityPlayer player, World world, BlockShrine block, IBlockState state, ItemStack item) {
    }

    @Override
    protected PokemonGroup.PokemonData selectActiveGroup() {
        return PokemonGroup.PokemonData.of(EnumSpecies.Darkrai);
    }

    @Override
    public void activateOther(EntityPlayer player, World world, BlockShrine blockShrine, IBlockState state, ItemStack heldItem) {
        List<BlockPos> positions = ForgeListener.searchForBlock(world, this.pos, 8, 5, (w, blockPos) -> {
            IBlockState iBlockState = w.getBlockState((BlockPos)blockPos);
            return iBlockState.getBlock() == PixelmonBlocks.darkCrystal;
        });
        if (positions.size() < 5) {
            return;
        }
        if (PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player).get().countAblePokemon() < 1) {
            player.sendMessage(new TextComponentString("Unable to activate shrine. Your party is fainted."));
            return;
        }
        positions.forEach(blockPos -> {
            world.setBlockToAir((BlockPos)blockPos);
            world.playSound(null, (BlockPos)blockPos, PixelSounds.lava_crystal_shatter, SoundCategory.BLOCKS, 1.0f, 1.0f);
        });
        this.startSpawning(player, state, world, this.pos);
    }

    @Override
    public int maxTick() {
        return 100;
    }

    @Override
    public BlockPos getSpawnPos() {
        return super.getSpawnPos().up();
    }

    @Override
    protected EntityPixelmon setupPixelmon(World world) {
        return new PokemonSpec(EnumSpecies.Darkrai.name).create(world);
    }

    @Override
    public String getRightClickMessage(ItemStack heldStack, IBlockState state) {
        return "pixelmon.darkrai_altar.right_click";
    }
}

