/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableMap
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.SpawnColors;
import java.awt.Color;
import java.util.List;
import java.util.Map;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileEntityTimespaceAltar extends TileEntityShrine {

    public int orbIn = 0;
    public boolean chainIn = false;
    private Map<EnumSpecies, List<Color>> colors = ImmutableMap.of(
            EnumSpecies.Giratina, Lists.newArrayList(SpawnColors.GRAY, SpawnColors.GOLD, SpawnColors.BLACK),
            EnumSpecies.Palkia, Lists.newArrayList(SpawnColors.PINK, SpawnColors.PURPLE, SpawnColors.WHITE),
            EnumSpecies.Dialga, Lists.newArrayList(SpawnColors.BLUE, SpawnColors.LIGHT_BLUE, SpawnColors.WHITE));

    @Override
    public void activate(EntityPlayer player, World world, BlockShrine block, IBlockState state, ItemStack item) {
        boolean giratina;
        boolean palkia;
        boolean dialga;
        PlayerStorage storage;
        if (!PixelmonConfig.disableArceusMethod && item.getItem() == PixelmonItems.jewelOfLife) {
            storage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player).get();
            dialga = false;
            palkia = false;
            giratina = false;
        } else {
            if (item.getItem() == PixelmonItems.jewelOfLife) return;
            this.doLogic(player, item, state);
            return;
        }
        for (int i = 0; i < 6; ++i) {
            if (storage.getNBT(storage.getIDFromPosition(i)) == null) continue;
            String name = storage.getNBT(storage.getIDFromPosition(i)).getString("Name");
            if (name.equals("Giratina")) {
                giratina = true;
                continue;
            }
            if (name.equals("Palkia")) {
                palkia = true;
                continue;
            }
            if (!name.equals("Dialga")) continue;
            dialga = true;
        }
        if (dialga && palkia && giratina) {
            player.sendMessage(new TextComponentString("The Jewel of Life is reacting with the Time Space Altar!"));
            this.doLogic(player, item, state);
            return;
        }
        player.sendMessage(new TextComponentString("You must have a Dialga, Palkia, and Giratina in your party to summon Arceus!"));
    }

    @Override
    public void activateOther(EntityPlayer player, World world, BlockShrine blockShrine, IBlockState state, ItemStack heldItem) {
        boolean giratina;
        boolean palkia;
        boolean dialga;
        PlayerStorage storage;
        if (!PixelmonConfig.disableArceusMethod && heldItem.getItem() == PixelmonItems.jewelOfLife) {
            storage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player).get();
            dialga = false;
            palkia = false;
            giratina = false;
        } else {
            if (heldItem.getItem() == PixelmonItems.jewelOfLife) return;
            this.doLogic(player, heldItem, state);
            return;
        }
        for (int i = 0; i < 6; ++i) {
            if (storage.getNBT(storage.getIDFromPosition(i)) == null) continue;
            String name = storage.getNBT(storage.getIDFromPosition(i)).getString("Name");
            if (name.equals("Giratina")) {
                giratina = true;
                continue;
            }
            if (name.equals("Palkia")) {
                palkia = true;
                continue;
            }
            if (!name.equals("Dialga")) continue;
            dialga = true;
        }
        if (storage.canPartyBattle() && dialga && palkia && giratina) {
            player.sendMessage(new TextComponentString("The Jewel of Life is reacting with the Time Space Altar!"));
            this.doLogic(player, heldItem, state);
            return;
        }
        if (!storage.canPartyBattle()) {
            player.sendMessage(new TextComponentString("Your party is unable to battle!"));
            return;
        }
        player.sendMessage(new TextComponentString("You must have a Dialga, Palkia, and Giratina in your party to summon Arceus!"));
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        nbt.setInteger("OrbIn", this.orbIn);
        nbt.setBoolean("ChainIn", this.chainIn);
        return nbt;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        this.orbIn = nbt.getInteger("OrbIn");
        this.chainIn = nbt.getBoolean("ChainIn");
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        NBTTagCompound tagCompound = new NBTTagCompound();
        this.writeToNBT(tagCompound);
        return new SPacketUpdateTileEntity(this.pos, 0, tagCompound);
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBT(pkt.getNbtCompound());
    }

    @Override
    protected PokemonGroup.PokemonData selectActiveGroup() {
        switch (this.orbIn) {
            case 1: {
                return PokemonGroup.PokemonData.of(EnumSpecies.Palkia);
            }
            case 2: {
                return PokemonGroup.PokemonData.of(EnumSpecies.Dialga);
            }
            case 3: {
                return PokemonGroup.PokemonData.of(EnumSpecies.Giratina);
            }
            case 5: {
                return PokemonGroup.PokemonData.of(EnumSpecies.Arceus);
            }
        }
        return null;
    }

    public void doLogic(EntityPlayer player, ItemStack itemIn, IBlockState state) {
        if (this.isSpawning()) {
            return;
        }
        int orb = itemIn.isEmpty() ? 0 : (itemIn.getItem() == PixelmonItemsHeld.lustrous_orb ? 1 : (itemIn.getItem() == PixelmonItemsHeld.adamant_orb ? 2 : (itemIn.getItem() == PixelmonItemsHeld.griseous_orb ? 3 : (itemIn.getItem() == PixelmonItems.redchain ? 4 : (orb = itemIn.getItem() == PixelmonItems.jewelOfLife ? 5 : 0)))));
        if (this.chainIn && this.orbIn == 0) {
            this.setValues(player, this.orbIn, true, orb, itemIn, state);
        } else if (orb == 4) {
            this.setValues(player, this.orbIn, true, 0, itemIn, state);
        } else if (orb == 5) {
            System.out.println("ARCEUS");
            this.setValues(player, this.orbIn, true, 5, itemIn, state);
        }
        this.world.notifyBlockUpdate(this.pos, state, state, 3);
    }

    private void setValues(EntityPlayer player, int held, boolean chain, int orb, ItemStack itemIn, IBlockState state) {
        ItemStack stack;
        switch (held) {
            case 1: {
                stack = new ItemStack(PixelmonItemsHeld.lustrous_orb);
                break;
            }
            case 2: {
                stack = new ItemStack(PixelmonItemsHeld.adamant_orb);
                break;
            }
            case 3: {
                stack = new ItemStack(PixelmonItemsHeld.griseous_orb);
                break;
            }
            case 4: {
                stack = new ItemStack(PixelmonItems.redchain);
                break;
            }
            case 5: {
                stack = new ItemStack(PixelmonItems.jewelOfLife);
                break;
            }
            default: {
                stack = ItemStack.EMPTY;
            }
        }
        if (stack.isEmpty()) {
            itemIn.shrink(1);
        } else {
            player.setHeldItem(EnumHand.MAIN_HAND, stack);
        }
        this.chainIn = chain;
        this.orbIn = orb;
        if (chain && orb > 0 && orb != 4) {
            this.startSpawning(player, state, this.world, this.pos);
        }
        if (orb == 5) {
            this.startSpawning(player, state, this.world, this.pos);
        }
    }

    @Override
    protected void finish() {
        this.chainIn = false;
        this.orbIn = 0;
    }

    @Override
    protected EntityPixelmon setupPixelmon(World world) {
        switch (this.orbIn) {
            case 1: {
                return this.createWithHeldItem(world, EnumSpecies.Palkia, PixelmonItemsHeld.lustrous_orb);
            }
            case 2: {
                return this.createWithHeldItem(world, EnumSpecies.Dialga, PixelmonItemsHeld.adamant_orb);
            }
            case 3: {
                return this.createWithHeldItem(world, EnumSpecies.Giratina, PixelmonItemsHeld.griseous_orb);
            }
            case 5: {
                return new PokemonSpec(EnumSpecies.Arceus.name).create(world);
            }
        }
        return null;
    }

    private EntityPixelmon createWithHeldItem(World world, EnumSpecies species, Item item) {
        EntityPixelmon pixelmonEntity = new PokemonSpec(species.name).create(world);
        pixelmonEntity.setHeldItem(new ItemStack(item));
        return pixelmonEntity;
    }

    @Override
    public int maxTick() {
        return 200;
    }

    @Override
    public String getRightClickMessage(ItemStack heldStack, IBlockState state) {
        return "pixelmon.timespace_altar.right_click";
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public AxisAlignedBB getRenderBoundingBox() {
        return INFINITE_EXTENT_AABB;
    }
}

