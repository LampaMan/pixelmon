/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableMap
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.SpawnColors;
import java.awt.Color;
import java.util.List;
import java.util.Map;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class TileEntityTapuShrine extends TileEntityShrine {

    Map<EnumSpecies, List<Color>> colors = ImmutableMap.of(
        EnumSpecies.TapuKoko, Lists.newArrayList(SpawnColors.YELLOW, SpawnColors.ORANGE, SpawnColors.BLACK),
        EnumSpecies.TapuFini, Lists.newArrayList(SpawnColors.PURPLE, SpawnColors.LIGHT_BLUE, SpawnColors.BLACK),
        EnumSpecies.TapuLele, Lists.newArrayList(SpawnColors.PINK, SpawnColors.WHITE, SpawnColors.BLACK),
        EnumSpecies.TapuBulu, Lists.newArrayList(SpawnColors.RED, SpawnColors.GOLD, SpawnColors.BLACK));

    @Override
    public void activate(EntityPlayer player, World world, BlockShrine block, IBlockState state, ItemStack item) {
        if (this.isSpawning()) {
            return;
        }
        item.shrink(1);
        this.startSpawning(player, state, world, this.pos);
    }

    @Override
    protected PokemonGroup.PokemonData selectActiveGroup() {
        switch ((int)(Math.random() * 4.0)) {
            case 1: {
                return PokemonGroup.PokemonData.of(EnumSpecies.TapuFini);
            }
            case 2: {
                return PokemonGroup.PokemonData.of(EnumSpecies.TapuKoko);
            }
            case 3: {
                return PokemonGroup.PokemonData.of(EnumSpecies.TapuLele);
            }
        }
        return PokemonGroup.PokemonData.of(EnumSpecies.TapuBulu);
    }

    @Override
    public int maxTick() {
        return 100;
    }

    @Override
    public String getRightClickMessage(ItemStack heldStack, IBlockState state) {
        return "pixelmon.tapu_shrine.right_click";
    }
}

