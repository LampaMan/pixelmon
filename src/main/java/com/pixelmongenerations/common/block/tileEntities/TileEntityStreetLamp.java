/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class TileEntityStreetLamp
extends TileEntity
implements ISpecialTexture {
    private String name = null;
    private ResourceLocation texture;
    private BlockModelHolder<GenericSmdModel> model;

    public TileEntityStreetLamp() {
    }

    public TileEntityStreetLamp(String name) {
        this.name = name;
        this.getModelAndTexture();
    }

    public void getModelAndTexture() {
        if (this.name == null) {
            this.name = "black";
        }
        this.texture = new ResourceLocation("pixelmon:textures/blocks/street_lamp_" + this.name + ".png");
        this.model = new BlockModelHolder("blocks/street_lamp.pqc");
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        if (this.name != null && !this.name.isEmpty()) {
            nbt.setString("StreetLampName", this.name);
        }
        return nbt;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        this.name = nbt.hasKey("StreetLampName") ? nbt.getString("StreetLampName") : "black";
    }

    @Override
    public ResourceLocation getTexture() {
        if (this.texture == null) {
            this.getModelAndTexture();
        }
        return this.texture;
    }

    public BlockModelHolder<GenericSmdModel> getModel() {
        if (this.model == null) {
            this.getModelAndTexture();
        }
        return this.model;
    }
}

