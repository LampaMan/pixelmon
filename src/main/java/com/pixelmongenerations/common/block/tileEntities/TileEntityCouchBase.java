/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import java.util.HashMap;
import java.util.Map;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.WorldServer;

public class TileEntityCouchBase
extends TileEntity
implements ISpecialTexture {
    private static final Map<String, ResourceLocation> textures = new HashMap<String, ResourceLocation>();
    private String colour = "";

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        compound.setString("colour", this.colour);
        return compound;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        if (compound.hasKey("colour")) {
            this.setColour(compound.getString("colour"));
        } else {
            this.setColour("red");
        }
    }

    private void refreshTexture() {
        if (this.hasWorld() && !this.world.isRemote) {
            ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
            this.markDirty();
        }
    }

    public String getColour() {
        return this.colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
        this.refreshTexture();
    }

    @Override
    public ResourceLocation getTexture() {
        if (this.colour.isEmpty() || !textures.containsKey(this.colour)) {
            return textures.get("red");
        }
        return textures.get(this.colour);
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBT(nbt);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager networkManager, SPacketUpdateTileEntity packet) {
        this.colour = packet.getNbtCompound().getString("colour");
        this.refreshTexture();
    }

    static {
        for (EnumDyeColor color : EnumDyeColor.values()) {
            textures.put(color.getName().toLowerCase(), new ResourceLocation("pixelmon:textures/blocks/couch/texture_" + color.getName() + ".png"));
        }
    }
}

