/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class TileEntityDisplayball
extends TileEntity
implements ISpecialTexture {
    private String name = null;
    private String modelName = null;
    private ResourceLocation texture;
    private BlockModelHolder<GenericSmdModel> model;

    public TileEntityDisplayball() {
    }

    public TileEntityDisplayball(String name, String modelName) {
        this.name = name;
        this.modelName = modelName;
        this.getModelAndTexture();
    }

    public void getModelAndTexture() {
        if (this.name == null) {
            this.name = "poke";
        }
        this.texture = new ResourceLocation("pixelmon:textures/blocks/displayballs/" + this.name + "_ball.png");
        this.model = new BlockModelHolder("blocks/displayballs/" + this.modelName + "displayball.pqc");
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        if (this.name != null && !this.name.isEmpty()) {
            nbt.setString("DisplayballName", this.name);
        }
        return nbt;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        this.name = nbt.hasKey("DisplayballName") ? nbt.getString("DisplayballName") : "poke";
    }

    @Override
    public ResourceLocation getTexture() {
        if (this.texture == null) {
            this.getModelAndTexture();
        }
        return this.texture;
    }

    public BlockModelHolder<GenericSmdModel> getModel() {
        if (this.model == null) {
            this.getModelAndTexture();
        }
        return this.model;
    }
}

