/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.decorative.ClockBlock;
import com.pixelmongenerations.common.block.tileEntities.IFrameCounter;
import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileEntityClock
extends TileEntity
implements ISpecialTexture,
IFrameCounter {
    @Override
    public ResourceLocation getTexture() {
        IBlockState state = this.getWorld().getBlockState(this.getPos());
        if (state.getBlock() instanceof ClockBlock) {
            ClockBlock block = (ClockBlock)state.getBlock();
            return new ResourceLocation("pixelmon:textures/blocks/" + block.getColor().name() + "ClockModel.png");
        }
        return new ResourceLocation("pixelmon:textures/blocks/BlueClockModel.png");
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public int getFrame() {
        return (int)Math.floor(Minecraft.getMinecraft().world.getWorldTime() / 10L + 600L);
    }

    @Override
    public void setFrame(int frame) {
    }
}

