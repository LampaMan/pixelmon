/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import com.pixelmongenerations.common.item.IShrineItem;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumForms;
import com.pixelmongenerations.core.util.PixelSounds;
import com.pixelmongenerations.core.util.SpawnColors;
import java.awt.Color;
import java.util.List;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;

public class TileEntityLugiaShrine
extends TileEntityShrine {
    private List<Color> color = Lists.newArrayList(SpawnColors.WHITE, SpawnColors.LIGHT_BLUE, SpawnColors.BLACK);
    private byte articunoOrbIn;
    private byte zapdosOrbIn;
    private byte moltresOrbIn;

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        this.articunoOrbIn = compound.getByte("ArticunoOrbIn");
        this.zapdosOrbIn = compound.getByte("ZapdosOrbIn");
        this.moltresOrbIn = compound.getByte("MoltresOrbIn");
    }

    @Override
    protected PokemonGroup.PokemonData selectActiveGroup() {
        return PokemonGroup.PokemonData.of(EnumSpecies.Lugia);
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setByte("ArticunoOrbIn", this.articunoOrbIn);
        compound.setByte("ZapdosOrbIn", this.zapdosOrbIn);
        compound.setByte("MoltresOrbIn", this.moltresOrbIn);
        return super.writeToNBT(compound);
    }

    @Override
    public void activate(EntityPlayer player, World world, BlockShrine block, IBlockState state, ItemStack item) {
        if (this.isSpawning()) {
            return;
        }
        PokemonGroup group = ((IShrineItem)((Object)item.getItem())).getGroup();
        if (group.contains(EnumSpecies.Articuno)) {
            if (this.articunoOrbIn == 0) {
                item.shrink(1);
                this.articunoOrbIn = (byte)(group.contains(EnumForms.Galarian) ? 2 : 1);
            }
        } else if (group.contains(EnumSpecies.Zapdos)) {
            if (this.zapdosOrbIn == 0) {
                item.shrink(1);
                this.zapdosOrbIn = (byte)(group.contains(EnumForms.Galarian) ? 2 : 1);
            }
        } else if (group.contains(EnumSpecies.Moltres) && this.moltresOrbIn == 0) {
            item.shrink(1);
            this.moltresOrbIn = (byte)(group.contains(EnumForms.Galarian) ? 2 : 1);
        }
        if (this.articunoOrbIn != 0 && this.zapdosOrbIn != 0 && this.moltresOrbIn != 0 && group.contains(EnumSpecies.Lugia)) {
            this.moltresOrbIn = 0;
            this.zapdosOrbIn = 0;
            this.articunoOrbIn = 0;
            this.startSpawning(player, state, world, this.pos);
            world.playSound(null, player.posX, player.posY, player.posZ, PixelSounds.lugia_shrine_song, SoundCategory.BLOCKS, 1.0f, 1.0f);
        }
        world.notifyBlockUpdate(this.pos, state, state, 3);
    }

    @Override
    public void activateOther(EntityPlayer player, World world, BlockShrine blockShrine, IBlockState state, ItemStack heldItem) {
        if (this.articunoOrbIn != 0) {
            ItemStack stack = new ItemStack(this.articunoOrbIn == 1 ? PixelmonItems.unoOrb : PixelmonItems.mysticalOrb);
            this.articunoOrbIn = 0;
            stack.setItemDamage(stack.getMaxDamage());
            player.addItemStackToInventory(stack);
        } else if (this.zapdosOrbIn != 0) {
            ItemStack stack = new ItemStack(this.zapdosOrbIn == 1 ? PixelmonItems.dosOrb : PixelmonItems.martialOrb);
            this.zapdosOrbIn = 0;
            stack.setItemDamage(stack.getMaxDamage());
            player.addItemStackToInventory(stack);
        } else if (this.moltresOrbIn != 0) {
            ItemStack stack = new ItemStack(this.moltresOrbIn == 1 ? PixelmonItems.tresOrb : PixelmonItems.malevolentOrb);
            this.moltresOrbIn = 0;
            stack.setItemDamage(stack.getMaxDamage());
            player.addItemStackToInventory(stack);
        }
        world.notifyBlockUpdate(this.pos, state, state, 3);
    }

    @Override
    public int maxTick() {
        return 100;
    }

    public boolean hasZapdosOrbIn() {
        return this.zapdosOrbIn != 0;
    }

    public void setZapdosOrbIn(int zapdosOrbIn) {
        this.zapdosOrbIn = (byte)zapdosOrbIn;
    }

    public void setArticunoOrbIn(int articunoOrbIn) {
        this.articunoOrbIn = (byte)articunoOrbIn;
    }

    public void setMoltresOrbIn(int moltresOrbIn) {
        this.moltresOrbIn = (byte)moltresOrbIn;
    }

    public boolean hasMoltresOrbIn() {
        return this.moltresOrbIn != 0;
    }

    public boolean hasArticunoOrbIn() {
        return this.articunoOrbIn != 0;
    }

    @Override
    public String getRightClickMessage(ItemStack heldStack, IBlockState state) {
        return "pixelmon.lugia_shrine.right_click";
    }
}

