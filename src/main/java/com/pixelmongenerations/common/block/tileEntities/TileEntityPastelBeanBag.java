/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.decorative.PastelBeanBagBlock;
import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class TileEntityPastelBeanBag
extends TileEntity
implements ISpecialTexture {
    @Override
    public ResourceLocation getTexture() {
        IBlockState state = this.getWorld().getBlockState(this.getPos());
        if (state.getBlock() instanceof PastelBeanBagBlock) {
            PastelBeanBagBlock block = (PastelBeanBagBlock)state.getBlock();
            return new ResourceLocation("pixelmon:textures/blocks/pastel_bean_bag_" + block.getColor().name() + ".png");
        }
        return new ResourceLocation("pixelmon:textures/blocks/pastel_bean_bag_red.png");
    }
}

