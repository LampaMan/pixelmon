/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;

public class TileEntityDecorativeBase
extends TileEntity {
    public boolean isAnotherWithSameDirectionOnSide(World world, int x, int y, int z, EnumFacing dir) {
        TileEntity ent = null;
        return this.isSameDirectionAndType(ent);
    }

    public boolean isSameDirectionAndType(TileEntity ent) {
        return ent != null && ent.getClass() == this.getClass() && (ent.getBlockMetadata() & 0xC) == (this.getBlockMetadata() & 0xC);
    }
}

