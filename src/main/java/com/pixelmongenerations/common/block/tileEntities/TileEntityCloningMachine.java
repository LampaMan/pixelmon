/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.api.events.player.PlayerShinyChanceEvent;
import com.pixelmongenerations.common.battle.BattleFactory;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.WildPixelmonParticipant;
import com.pixelmongenerations.common.block.BlockRotation;
import com.pixelmongenerations.common.block.tileEntities.TileEntityDecorativeBase;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelmonMethods;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ITickable;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.MinecraftForge;

public class TileEntityCloningMachine
extends TileEntityDecorativeBase
implements ITickable {
    public boolean hasMew = false;
    public boolean processingClone = false;
    public boolean growingPokemon = false;
    public boolean isBroken = false;
    public float xboost = 0.0f;
    public float zboost = 0.0f;
    public EntityPixelmon pixelmon;
    public String pokemonName = "";
    public boolean isShiny = false;
    public int boostLevel = 0;
    public int boostCount = 0;
    boolean boostSet = false;
    boolean travDown = true;
    public float lasPos = -2.0f;
    int baseCount = 0;
    public int pokemonProgress = 0;
    public boolean isFinished = false;
    public String playerUUID = "";

    private void resetMachine() {
        this.isFinished = false;
        this.growingPokemon = false;
        this.processingClone = false;
        this.isShiny = false;
        this.hasMew = false;
        this.pokemonProgress = 0;
        this.boostLevel = 0;
        this.boostCount = 0;
        this.baseCount = 0;
        this.pokemonName = "";
        this.playerUUID = "";
        ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
    }

    @Override
    public void update() {
        if (!this.boostSet) {
            BlockRotation rot = BlockRotation.getRotationFromMetadata(this.getBlockMetadata());
            if (rot == BlockRotation.Normal) {
                this.xboost = (float)((double)this.xboost + 3.35);
            } else if (rot == BlockRotation.Rotate180) {
                this.xboost = (float)((double)this.xboost - 3.35);
            } else {
                this.zboost = rot == BlockRotation.CW ? (float)((double)this.zboost - 3.35) : (float)((double)this.zboost + 3.35);
            }
            this.boostSet = true;
        }
        if (!this.world.isRemote) {
            if (!this.processingClone && this.boostCount == 3 && this.hasMew) {
                if (this.baseCount < 30) {
                    ++this.baseCount;
                } else if (this.baseCount == 30) {
                    this.processingClone = true;
                    this.baseCount = 0;
                    ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
                }
            }
            if (this.processingClone) {
                ++this.baseCount;
                if (this.baseCount >= 280) {
                    Optional<PlayerStorage> playerStorageOpt;
                    boolean madeMewtwo = Math.random() < (double)((float)this.boostLevel / 40.0f);
                    this.pixelmon = madeMewtwo ? (EntityPixelmon)PixelmonEntityList.createEntityByName("Mewtwo", this.world) : (EntityPixelmon)PixelmonEntityList.createEntityByName("Ditto", this.world);
                    this.pixelmon.initAnimation();
                    boolean isShiny = this.pixelmon.isShiny();
                    if (!isShiny && !this.playerUUID.equals("") && (playerStorageOpt = PixelmonStorage.pokeBallManager.getPlayerStorageFromUUID(UUID.fromString(this.playerUUID))).isPresent()) {
                        EntityPlayerMP player = playerStorageOpt.get().getPlayer();
                        float chance = PixelmonConfig.shinyRate;
                        if (PixelmonConfig.enableCatchCombos) {
                            if (PixelmonConfig.enableCatchComboShinyLock && PixelmonMethods.isCatchComboSpecies(player, this.pixelmon.getSpecies())) {
                                chance = PixelmonMethods.getCatchComboChance(player);
                            } else if (!PixelmonConfig.enableCatchComboShinyLock) {
                                chance = PixelmonMethods.getCatchComboChance(player);
                            }
                        } else if (PixelmonMethods.isWearingShinyCharm(player)) {
                            chance = PixelmonConfig.shinyCharmRate;
                        }
                        PlayerShinyChanceEvent event = new PlayerShinyChanceEvent(player, chance);
                        MinecraftForge.EVENT_BUS.post(event);
                        chance = event.isCanceled() ? 0.0f : event.getChance();
                        isShiny = chance > 0.0f && RandomHelper.getRandomChance(1.0f / chance);
                    }
                    this.pixelmon.setShiny(isShiny);
                    this.isShiny = this.pixelmon.isShiny();
                    this.pokemonName = this.pixelmon.getPokemonName();
                    this.growingPokemon = true;
                    this.processingClone = false;
                    ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
                }
            }
            if (this.growingPokemon) {
                if (this.pokemonProgress < 200) {
                    ++this.pokemonProgress;
                } else {
                    if (this.pokemonName.equals("Mewtwo")) {
                        this.isBroken = true;
                        this.playerUUID = "";
                        this.releasePokemon();
                    } else {
                        this.isFinished = true;
                        this.playerUUID = "";
                    }
                    this.growingPokemon = false;
                    ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
                }
            }
        } else {
            if (this.travDown) {
                this.lasPos -= 0.1f;
                if (this.lasPos < -15.0f) {
                    this.travDown = false;
                }
            } else {
                this.lasPos += 0.1f;
                if (this.lasPos >= -2.0f) {
                    this.travDown = true;
                }
            }
            if (this.growingPokemon && this.pokemonProgress < 200) {
                ++this.pokemonProgress;
            }
            if (this.processingClone) {
                ++this.baseCount;
                if (this.baseCount > 80 && this.baseCount < 280) {
                    int i = 0;
                    while ((float)i < 30.0f * ((float)this.baseCount - 80.0f) / 200.0f) {
                        this.world.spawnParticle(EnumParticleTypes.REDSTONE, (float)this.pos.getX() + this.xboost + this.world.rand.nextFloat(), (double)((float)this.pos.getY() + 0.3f + this.world.rand.nextFloat() + this.world.rand.nextFloat()), (double)((float)this.pos.getZ() + this.zboost + this.world.rand.nextFloat()), -255.0, 1.0, 255.0, new int[0]);
                        ++i;
                    }
                }
            }
        }
        if (this.pokemonName.equals("") && this.pixelmon != null) {
            this.pixelmon = null;
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        this.hasMew = nbt.getBoolean("HasMew");
        this.boostCount = nbt.getShort("BoostCount");
        this.boostLevel = nbt.getShort("BoostLevel");
        this.isBroken = nbt.getBoolean("IsBroken");
        this.isFinished = nbt.getBoolean("IsFinished");
        this.processingClone = nbt.getBoolean("ProcessingClone");
        this.growingPokemon = nbt.getBoolean("GrowingPokemon");
        this.pokemonName = nbt.getString("PokemonName");
        this.baseCount = nbt.getInteger("BaseCount");
        this.pokemonProgress = nbt.getInteger("PokemonProgress");
        this.playerUUID = nbt.getString("PlayerUUID");
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        nbt.setBoolean("HasMew", this.hasMew);
        nbt.setShort("BoostCount", (short)this.boostCount);
        nbt.setShort("BoostLevel", (short)this.boostLevel);
        nbt.setBoolean("IsBroken", this.isBroken);
        nbt.setBoolean("IsFinished", this.isFinished);
        nbt.setBoolean("ProcessingClone", this.processingClone);
        nbt.setBoolean("GrowingPokemon", this.growingPokemon);
        nbt.setString("PokemonName", this.pokemonName);
        nbt.setInteger("BaseCount", this.baseCount);
        nbt.setInteger("PokemonProgress", this.pokemonProgress);
        nbt.setString("PlayerUUID", this.playerUUID);
        return nbt;
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBT(nbt);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBT(pkt.getNbtCompound());
    }

    public void activate(EntityPlayer player, ItemStack heldItem) {
        if (!PixelmonConfig.cloningMachineEnabled) {
            ChatHandler.sendChat(player, "pixelmon.general.disabledblock", new Object[0]);
            return;
        }
        if (this.isFinished) {
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
            if (optstorage.isPresent()) {
                PlayerStorage storage = optstorage.get();
                if (optstorage.get().countAblePokemon() == 0) {
                    this.releasePokemon();
                } else {
                    this.fightDitto((EntityPlayerMP)player, storage);
                }
            }
            this.resetMachine();
        } else if (!this.hasMew) {
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
            if (optstorage.isPresent()) {
                PlayerStorage storage = optstorage.get();
                for (NBTTagCompound nbt : storage.getList()) {
                    if (nbt == null || !nbt.getString("Name").equals("Mew") || nbt.getShort("NumCloned") >= PixelmonConfig.limitMew) continue;
                    this.hasMew = true;
                    this.playerUUID = player.getUniqueID().toString();
                    ((WorldServer)player.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
                    nbt.setShort("NumCloned", (short)(nbt.getShort("NumCloned") + 1));
                    storage.updateClient(nbt, EnumUpdateType.Clones);
                    break;
                }
                if (!this.hasMew) {
                    ChatHandler.sendChat(player, "pixelmon.blocks.noclone", new Object[0]);
                }
            }
        } else if (this.boostCount < 3 && heldItem != null && heldItem.getItem() instanceof ItemBlock) {
            Block block = ((ItemBlock)heldItem.getItem()).getBlock();
            if (block == Blocks.IRON_BLOCK) {
                ++this.boostCount;
                ++this.boostLevel;
                ((WorldServer)player.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
                if (!player.capabilities.isCreativeMode) {
                    heldItem.shrink(1);
                }
            } else if (block == Blocks.GOLD_BLOCK) {
                ++this.boostCount;
                this.boostLevel += 3;
                ((WorldServer)player.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
                if (!player.capabilities.isCreativeMode) {
                    heldItem.shrink(1);
                }
            } else if (block == Blocks.DIAMOND_BLOCK) {
                ++this.boostCount;
                this.boostLevel += 5;
                ((WorldServer)player.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
                if (!player.capabilities.isCreativeMode) {
                    heldItem.shrink(1);
                }
            }
        }
    }

    private void fightDitto(EntityPlayerMP player, PlayerStorage storage) {
        if (this.pixelmon == null) {
            return;
        }
        EntityPixelmon firstPokemon = storage.getFirstAblePokemon(player.world);
        if (!storage.entityAlreadyExists(firstPokemon)) {
            firstPokemon.setLocationAndAngles(player.posX, player.posY, player.posZ, player.rotationYaw, 0.0f);
            firstPokemon.releaseFromPokeball();
        }
        BattleFactory.createBattle().team1(new PlayerParticipant(player, firstPokemon)).team2(new WildPixelmonParticipant(this.pixelmon)).startBattle();
    }

    public void releasePokemon() {
        int xplus = 0;
        int zplus = 0;
        if (this.xboost > 0.0f) {
            ++zplus;
        } else if (this.xboost < 0.0f) {
            --zplus;
        } else if (this.zboost > 0.0f) {
            --xplus;
        } else if (this.zboost < 0.0f) {
            ++xplus;
        }
        if (this.pixelmon != null && !this.pixelmon.isEgg) {
            this.pixelmon.setLocationAndAngles((float)this.pos.getX() + this.xboost + (float)xplus, this.pos.getY(), (float)this.pos.getZ() + this.zboost + (float)zplus, 0.0f, 0.0f);
            this.pixelmon.releaseFromPokeball();
        }
    }
}

