/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.item.ItemPokeballDisc;
import com.pixelmongenerations.core.config.PixelmonItemsPokeballs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileEntityMechanicalAnvil
extends TileEntity
implements ISidedInventory,
ITickable {
    private NonNullList<ItemStack> anvilItemStacks = NonNullList.withSize(3, ItemStack.EMPTY);
    public int anvilRunTime;
    public int currentItemRunTime;
    public int anvilHammerTime;
    public boolean isRunning = false;
    public Item itemOnAnvil = null;
    private int stateTimer = 0;
    public int state = 0;
    public int frame = 0;

    @Override
    public int getSizeInventory() {
        return this.anvilItemStacks.size();
    }

    @Override
    public boolean isEmpty() {
        for (ItemStack itemstack : this.anvilItemStacks) {
            if (itemstack == null || itemstack.isEmpty()) continue;
            return false;
        }
        return true;
    }

    @Override
    public ItemStack getStackInSlot(int slotIndex) {
        return this.anvilItemStacks.get(slotIndex);
    }

    @Override
    public ItemStack decrStackSize(int slotIndex, int decreaseBy) {
        if (!this.anvilItemStacks.get(slotIndex).isEmpty()) {
            if (this.anvilItemStacks.get(slotIndex).getCount() <= decreaseBy) {
                ItemStack itemStack = this.anvilItemStacks.get(slotIndex);
                this.anvilItemStacks.set(slotIndex, ItemStack.EMPTY);
                return itemStack;
            }
            ItemStack itemStack = this.anvilItemStacks.get(slotIndex).splitStack(decreaseBy);
            if (this.anvilItemStacks.get(slotIndex).getCount() == 0) {
                this.anvilItemStacks.set(slotIndex, ItemStack.EMPTY);
            }
            return itemStack;
        }
        return ItemStack.EMPTY;
    }

    @Override
    public ItemStack removeStackFromSlot(int slotIndex) {
        if (!this.anvilItemStacks.get(slotIndex).isEmpty()) {
            ItemStack itemStack = this.anvilItemStacks.get(slotIndex);
            this.anvilItemStacks.set(slotIndex, ItemStack.EMPTY);
            return itemStack;
        }
        return ItemStack.EMPTY;
    }

    @Override
    public void setInventorySlotContents(int slotIndex, ItemStack itemStack) {
        this.anvilItemStacks.set(slotIndex, itemStack);
        if (!itemStack.isEmpty() && itemStack.getCount() > this.getInventoryStackLimit()) {
            itemStack.setCount(this.getInventoryStackLimit());
        }
    }

    @Override
    public int getInventoryStackLimit() {
        return 64;
    }

    @Override
    public boolean isUsableByPlayer(EntityPlayer player) {
        return this.world.getTileEntity(this.pos) == this && player.getDistanceSq((double)this.pos.getX() + 0.5, (double)this.pos.getY() + 0.5, (double)this.pos.getZ() + 0.5) <= 64.0;
    }

    @Override
    public boolean isItemValidForSlot(int slotIndex, ItemStack itemStack) {
        Item item = itemStack.getItem();
        switch (slotIndex) {
            case 0: {
                return item.equals(PixelmonItemsPokeballs.aluDisc) || item.equals(PixelmonItemsPokeballs.ironDisc) || item instanceof ItemPokeballDisc;
            }
            case 1: {
                return TileEntityFurnace.isItemFuel(itemStack);
            }
            case 2: {
                return false;
            }
        }
        return false;
    }

    @Override
    public int[] getSlotsForFace(EnumFacing side) {
        return new int[]{0, 1, 2};
    }

    @Override
    public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction) {
        return this.isItemValidForSlot(index, itemStackIn);
    }

    @Override
    public boolean canExtractItem(int slotIndex, ItemStack itemStack, EnumFacing direction) {
        switch (slotIndex) {
            case 0: {
                return false;
            }
            case 1: {
                return itemStack.getItem() == Items.BUCKET;
            }
            case 2: {
                return true;
            }
        }
        return false;
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBT(nbt);
        nbt.setBoolean("isRunning", this.isRunning());
        if (!this.anvilItemStacks.get(0).isEmpty()) {
            nbt.setTag("itemInAnvil", this.anvilItemStacks.get(0).writeToNBT(new NBTTagCompound()));
        }
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        NBTTagCompound nbt = pkt.getNbtCompound();
        this.isRunning = nbt.getBoolean("isRunning");
        Item item = this.itemOnAnvil = nbt.hasKey("itemInAnvil") ? new ItemStack(nbt.getCompoundTag("itemInAnvil")).getItem() : null;
        if (this.isRunning) {
            this.frame = 0;
        } else {
            this.itemOnAnvil = null;
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound nbtTagCompound) {
        super.readFromNBT(nbtTagCompound);
        NBTTagList nbttaglist = nbtTagCompound.getTagList("Items", 10);
        this.anvilItemStacks = NonNullList.withSize(this.getSizeInventory(), ItemStack.EMPTY);
        for (int i = 0; i < nbttaglist.tagCount(); ++i) {
            NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
            byte b0 = nbttagcompound1.getByte("Slot");
            if (b0 < 0 || b0 >= this.anvilItemStacks.size()) continue;
            this.anvilItemStacks.set(b0, new ItemStack(nbttagcompound1));
        }
        this.anvilRunTime = nbtTagCompound.getShort("RunTime");
        this.anvilHammerTime = nbtTagCompound.getShort("HammerTime");
        this.currentItemRunTime = TileEntityFurnace.getItemBurnTime(this.anvilItemStacks.get(1));
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbtTagCompound) {
        super.writeToNBT(nbtTagCompound);
        nbtTagCompound.setShort("RunTime", (short)this.anvilRunTime);
        nbtTagCompound.setShort("HammerTime", (short)this.anvilHammerTime);
        NBTTagList nbttaglist = new NBTTagList();
        for (int i = 0; i < this.anvilItemStacks.size(); ++i) {
            if (this.anvilItemStacks.get(i).isEmpty()) continue;
            NBTTagCompound nbttagcompound1 = new NBTTagCompound();
            nbttagcompound1.setByte("Slot", (byte)i);
            this.anvilItemStacks.get(i).writeToNBT(nbttagcompound1);
            nbttaglist.appendTag(nbttagcompound1);
        }
        nbtTagCompound.setTag("Items", nbttaglist);
        return nbtTagCompound;
    }

    @SideOnly(value=Side.CLIENT)
    public int getHammerProgressScaled(int p_145953_1_) {
        return this.anvilHammerTime * p_145953_1_ / 200;
    }

    @SideOnly(value=Side.CLIENT)
    public int getBurnTimeRemainingScaled(int p_145955_1_) {
        if (this.currentItemRunTime == 0) {
            this.currentItemRunTime = 200;
        }
        return this.anvilRunTime * p_145955_1_ / this.currentItemRunTime;
    }

    public boolean isRunning() {
        return this.anvilRunTime > 0;
    }

    @Override
    public void update() {
        boolean flag = this.anvilRunTime > 0;
        boolean flag1 = false;
        if (!this.world.isRemote) {
            if (this.anvilRunTime > 0) {
                --this.anvilRunTime;
                if (this.anvilRunTime == 0) {
                    ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
                }
            }
            if (this.anvilRunTime != 0 || !this.anvilItemStacks.get(1).isEmpty() && !this.anvilItemStacks.get(0).isEmpty()) {
                if (this.anvilRunTime == 0 && this.canHammer()) {
                    this.currentItemRunTime = this.anvilRunTime = TileEntityFurnace.getItemBurnTime(this.anvilItemStacks.get(1));
                    ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
                    if (this.anvilRunTime > 0) {
                        flag1 = true;
                        if (!this.anvilItemStacks.get(1).isEmpty()) {
                            if (this.anvilItemStacks.get(1).getCount() == 1) {
                                this.anvilItemStacks.set(1, this.anvilItemStacks.get(1).getItem().getContainerItem(this.anvilItemStacks.get(1)));
                            } else {
                                this.anvilItemStacks.get(1).shrink(1);
                            }
                        }
                    }
                }
                if (this.isRunning() && this.canHammer()) {
                    ++this.anvilHammerTime;
                    if (this.anvilHammerTime == 200) {
                        this.anvilHammerTime = 0;
                        this.hammerItem();
                        flag1 = true;
                    }
                } else {
                    this.anvilHammerTime = 0;
                }
            }
            if (flag != this.anvilRunTime > 0) {
                flag1 = true;
            }
        }
        if (flag1) {
            this.markDirty();
        }
        if (this.isRunning) {
            this.frame += 2;
            if (this.frame == 200) {
                this.world.playSound(null, this.pos, SoundEvents.ENTITY_IRONGOLEM_DEATH, SoundCategory.BLOCKS, 0.7f, 1.0f);
            }
            if (this.frame > 399) {
                this.frame = 0;
            }
            ++this.stateTimer;
            if (this.stateTimer > 66 && this.state == 0) {
                this.state = 1;
            }
            if (this.stateTimer > 133 && this.state == 1) {
                this.state = 2;
            }
            if (this.stateTimer >= 199) {
                this.state = 0;
            }
            if (this.stateTimer == 200) {
                this.stateTimer = 0;
            }
        } else {
            this.frame = 0;
            this.stateTimer = 0;
            this.state = 0;
        }
    }

    private boolean canHammer() {
        if (this.anvilItemStacks.get(0).isEmpty()) {
            return false;
        }
        ItemStack itemstack = TileEntityMechanicalAnvil.getHammerResult(this.anvilItemStacks.get(0));
        if (itemstack == null) {
            return false;
        }
        if (this.anvilItemStacks.get(2).isEmpty()) {
            return true;
        }
        if (!this.anvilItemStacks.get(2).isItemEqual(itemstack)) {
            return false;
        }
        int result = this.anvilItemStacks.get(2).getCount() + itemstack.getCount();
        return result <= this.getInventoryStackLimit() && result <= this.anvilItemStacks.get(2).getMaxStackSize();
    }

    public void hammerItem() {
        if (this.canHammer()) {
            ItemStack itemstack = TileEntityMechanicalAnvil.getHammerResult(this.anvilItemStacks.get(0));
            if (this.anvilItemStacks.get(2).isEmpty()) {
                this.anvilItemStacks.set(2, itemstack.copy());
            } else if (this.anvilItemStacks.get(2).getItem() == itemstack.getItem()) {
                this.anvilItemStacks.get(2).setCount(this.anvilItemStacks.get(2).getCount() + itemstack.getCount());
            }
            this.anvilItemStacks.get(0).shrink(1);
            if (this.anvilItemStacks.get(0).getCount() <= 0) {
                this.anvilItemStacks.set(0, ItemStack.EMPTY);
            }
        }
    }

    public static ItemStack getHammerResult(ItemStack itemStack) {
        Item itemInAnvil = itemStack.getItem();
        if (itemInAnvil == PixelmonItemsPokeballs.aluDisc) {
            return new ItemStack(PixelmonItemsPokeballs.aluBase);
        }
        if (itemInAnvil == PixelmonItemsPokeballs.ironDisc) {
            return new ItemStack(PixelmonItemsPokeballs.ironBase);
        }
        if (itemInAnvil instanceof ItemPokeballDisc) {
            return new ItemStack(PixelmonItemsPokeballs.getLidFromEnum(((ItemPokeballDisc)itemInAnvil).pokeball), 1);
        }
        return ItemStack.EMPTY;
    }

    @Override
    public void openInventory(EntityPlayer playerIn) {
    }

    @Override
    public void closeInventory(EntityPlayer playerIn) {
    }

    @Override
    public int getField(int id) {
        return 0;
    }

    @Override
    public void setField(int id, int value) {
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public void clear() {
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public boolean hasCustomName() {
        return false;
    }

    @Override
    public ITextComponent getDisplayName() {
        return null;
    }
}

