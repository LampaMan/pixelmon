/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.api.pokemon.SpawnPokemon;
import com.pixelmongenerations.common.block.spawnmethod.BlockMeloettaMusicBox;
import com.pixelmongenerations.common.block.tileEntities.IShrineBlock;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumMeloetta;
import com.pixelmongenerations.core.util.SpawnColors;
import java.awt.Color;
import java.util.List;
import net.minecraft.block.BlockJukebox;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

public class TileEntityMeloettaMusicBox
extends BlockJukebox.TileEntityJukebox
implements ITickable,
IShrineBlock {
    private static List<Color> color = Lists.newArrayList(SpawnColors.LIGHT_GREEN, SpawnColors.WHITE, SpawnColors.GRAY);
    private int songTick;
    private int maxSongTick;
    private boolean spawnMeloetta;
    private EntityPlayer spawningPlayer;

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBT(nbt);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        NBTTagCompound tagCompound = new NBTTagCompound();
        this.writeToNBT(tagCompound);
        return new SPacketUpdateTileEntity(this.pos, 0, tagCompound);
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        NBTTagCompound tag = super.writeToNBT(compound);
        tag.setInteger("songTick", this.songTick);
        tag.setInteger("maxSongTick", this.maxSongTick);
        return tag;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        this.songTick = compound.getInteger("songTick");
        this.maxSongTick = compound.getInteger("maxSongTick");
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBT(pkt.getNbtCompound());
        if (this.hasWorld() && !this.world.isRemote) {
            ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
            this.markDirty();
        }
    }

    @Override
    public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newSate) {
        return !(newSate.getBlock() instanceof BlockMeloettaMusicBox);
    }

    @Override
    public void update() {
        if (!this.world.isRemote && this.spawnMeloetta) {
            if (this.songTick >= this.maxSongTick) {
                SpawnPokemon.of(EnumSpecies.Meloetta, this.pos.up(), 0.0f).spawn((EntityPlayerMP)this.spawningPlayer, this.world);
                ((WorldServer)this.world).spawnParticle(EnumParticleTypes.NOTE, false, (double)this.pos.getX() + 0.5, (double)this.pos.getY() + 1.2, (double)this.pos.getZ() + 0.5, 1, 0.0, 1.0, 0.0, 0.5, 0);
                this.spawnMeloetta = false;
                this.spawningPlayer = null;
                this.songTick = 0;
                this.maxSongTick = 0;
            } else {
                ++this.songTick;
            }
        }
    }

    public boolean isSpawningMeloetta() {
        return this.spawnMeloetta;
    }

    public void spawnMeloetta(EntityPlayer player, int duration) {
        this.spawningPlayer = player;
        this.spawnMeloetta = true;
        this.maxSongTick = duration * 20;
    }

    @Override
    public int getTick() {
        return this.songTick;
    }

    @Override
    public PokemonGroup.PokemonData getActiveGroup() {
        return PokemonGroup.PokemonData.of(EnumSpecies.Meloetta, EnumMeloetta.ARIA);
    }

    @Override
    public Vec3i getSpawnPos() {
        return this.pos;
    }

    @Override
    public int maxTick() {
        return this.maxSongTick;
    }
}

