/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;

public interface IBasicInventory
extends IInventory {
    @Override
    default public ItemStack decrStackSize(int index, int count) {
        if (this.getStackInSlot(index) != null) {
            if (this.getStackInSlot(index).getCount() <= count) {
                ItemStack itemStack = this.getStackInSlot(index);
                this.removeStackFromSlot(index);
                return itemStack;
            }
            ItemStack itemstack = this.getStackInSlot(index).splitStack(count);
            if (this.getStackInSlot(index).getCount() == 0) {
                this.removeStackFromSlot(index);
            }
            return itemstack;
        }
        return null;
    }

    @Override
    default public int getInventoryStackLimit() {
        return 64;
    }

    @Override
    default public void openInventory(EntityPlayer player) {
    }

    @Override
    default public void closeInventory(EntityPlayer player) {
    }

    @Override
    default public boolean isItemValidForSlot(int index, ItemStack stack) {
        return true;
    }

    @Override
    default public int getField(int id) {
        return 0;
    }

    @Override
    default public void setField(int id, int value) {
    }

    @Override
    default public int getFieldCount() {
        return 0;
    }

    @Override
    default public void clear() {
        for (int i = 0; i < this.getSizeInventory(); ++i) {
            this.removeStackFromSlot(i);
        }
    }

    @Override
    default public boolean hasCustomName() {
        return false;
    }

    @Override
    default public ITextComponent getDisplayName() {
        return new TextComponentTranslation(this.getName(), new Object[0]);
    }
}

