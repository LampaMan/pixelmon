/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import net.minecraft.util.ResourceLocation;

public interface ISpecialTexture {
    public ResourceLocation getTexture();
}

