/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.tileEntities.IBasicInventory;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ContainerChest;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.IInteractionObject;

public abstract class TileEntityInventory
extends TileEntity
implements IBasicInventory,
IInteractionObject {
    protected NonNullList<ItemStack> contents = NonNullList.withSize(this.getSizeInventory(), ItemStack.EMPTY);
    private String customName;

    protected TileEntityInventory() {
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        if (nbt.hasKey("CustomName", 8)) {
            this.customName = nbt.getString("CustomName");
        }
        this.contents = NonNullList.withSize(this.getSizeInventory(), ItemStack.EMPTY);
        if (nbt.hasKey("Items")) {
            NBTTagList itemsList = nbt.getTagList("Items", 10);
            for (int i = 0; i < itemsList.tagCount(); ++i) {
                NBTTagCompound nbttagcompound = itemsList.getCompoundTagAt(i);
                byte slot = nbttagcompound.getByte("Slot");
                if (slot < 0 || slot >= this.contents.size()) continue;
                this.contents.set(slot, new ItemStack(nbttagcompound));
            }
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        if (this.hasCustomName()) {
            nbt.setString("CustomName", this.customName);
        }
        NBTTagList itemsList = new NBTTagList();
        nbt.setTag("Items", itemsList);
        for (int slot = 0; slot < this.contents.size(); ++slot) {
            if (this.contents.get(slot).isEmpty()) continue;
            NBTTagCompound item = new NBTTagCompound();
            item.setByte("Slot", (byte)slot);
            this.contents.get(slot).writeToNBT(item);
            itemsList.appendTag(item);
        }
        return nbt;
    }

    @Override
    public boolean isEmpty() {
        for (ItemStack itemstack : this.contents) {
            if (itemstack == null || itemstack.isEmpty()) continue;
            return false;
        }
        return true;
    }

    @Override
    public ItemStack getStackInSlot(int index) {
        return this.contents.get(index);
    }

    @Override
    public ItemStack decrStackSize(int index, int count) {
        if (!this.contents.get(index).isEmpty()) {
            if (this.contents.get(index).getCount() <= count) {
                ItemStack itemStack = this.contents.get(index);
                this.contents.set(index, ItemStack.EMPTY);
                this.markDirty();
                return itemStack;
            }
            ItemStack itemstack = this.contents.get(index).splitStack(count);
            if (this.contents.get(index).getCount() == 0) {
                this.contents.set(index, ItemStack.EMPTY);
                this.markDirty();
            }
            return itemstack;
        }
        return null;
    }

    @Override
    public ItemStack removeStackFromSlot(int index) {
        ItemStack itemStack = this.contents.get(index);
        this.contents.set(index, ItemStack.EMPTY);
        this.markDirty();
        return itemStack;
    }

    @Override
    public void setInventorySlotContents(int index, ItemStack stack) {
        this.contents.set(index, stack);
        this.markDirty();
    }

    @Override
    public int getInventoryStackLimit() {
        return 64;
    }

    @Override
    public boolean isUsableByPlayer(EntityPlayer player) {
        return player.getDistanceSq((double)this.pos.getX() + 0.5, (double)this.pos.getY() + 0.5, (double)this.pos.getZ() + 0.5) <= 64.0;
    }

    @Override
    public String getName() {
        return this.hasCustomName() ? this.customName : this.getInventoryName();
    }

    abstract String getInventoryName();

    @Override
    public boolean hasCustomName() {
        return this.customName != null && this.customName.length() > 0;
    }

    @Override
    public ITextComponent getDisplayName() {
        return this.hasCustomName() ? new TextComponentString(this.getName()) : new TextComponentTranslation(this.getName(), new Object[0]);
    }

    @Override
    public Container createContainer(InventoryPlayer playerInventory, EntityPlayer playerIn) {
        return new ContainerChest(playerInventory, this, playerIn);
    }

    @Override
    public String getGuiID() {
        return "minecraft:container";
    }
}

