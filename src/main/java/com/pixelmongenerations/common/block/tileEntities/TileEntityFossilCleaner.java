/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.tileEntities.IBasicInventory;
import com.pixelmongenerations.common.item.ItemCoveredFossil;
import com.pixelmongenerations.common.item.ItemFossil;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.world.WorldServer;

public class TileEntityFossilCleaner
extends TileEntity
implements ISidedInventory,
IBasicInventory,
ITickable {
    public Item itemInCleaner = null;
    public int timer = 360;
    public int renderPass = 0;

    public boolean isOn() {
        return this.itemInCleaner != null && this.timer > 0 && !this.isFossilClean();
    }

    public boolean isFossilClean() {
        return this.itemInCleaner != null && this.itemInCleaner instanceof ItemFossil;
    }

    public void setItemInCleaner(Item item) {
        if (item == null) {
            this.itemInCleaner = null;
            this.timer = 0;
            this.world.notifyNeighborsOfStateChange(this.pos, this.getBlockType(), true);
            ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
        } else if (item instanceof ItemFossil || item instanceof ItemCoveredFossil) {
            this.itemInCleaner = item;
            this.timer = 360;
            this.world.notifyNeighborsOfStateChange(this.pos, this.getBlockType(), true);
            ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
        }
    }

    @Override
    public boolean isEmpty() {
        return this.itemInCleaner == null;
    }

    public Item getItemInCleaner() {
        return this.itemInCleaner;
    }

    @Override
    public void update() {
        if (this.itemInCleaner != null && !this.isFossilClean()) {
            this.getWorld().notifyNeighborsOfStateChange(this.pos, this.getBlockType(), true);
            if (this.timer > 0) {
                --this.timer;
            } else if (this.itemInCleaner instanceof ItemCoveredFossil) {
                this.itemInCleaner = ((ItemCoveredFossil)this.itemInCleaner).cleanedFossil;
                if (this.world instanceof WorldServer) {
                    ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
                }
            }
        } else {
            this.timer = 360;
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        this.itemInCleaner = nbt.hasKey("ItemIn") && nbt.getInteger("ItemIn") != 0 ? Item.getItemById(nbt.getInteger("ItemIn")) : null;
        this.timer = nbt.hasKey("CleanerTimer") ? (int)nbt.getShort("CleanerTimer") : 360;
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        if (this.itemInCleaner != null) {
            nbt.setInteger("ItemIn", Item.getIdFromItem(this.itemInCleaner));
        }
        nbt.setShort("CleanerTimer", (short)this.timer);
        return nbt;
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBT(nbt);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBT(pkt.getNbtCompound());
    }

    @Override
    public boolean shouldRenderInPass(int pass) {
        this.renderPass = pass;
        return true;
    }

    @Override
    public int getSizeInventory() {
        return 1;
    }

    @Override
    public ItemStack getStackInSlot(int index) {
        return index == 0 && this.itemInCleaner != null ? new ItemStack(this.itemInCleaner) : ItemStack.EMPTY;
    }

    @Override
    public ItemStack removeStackFromSlot(int index) {
        if (index == 0 && this.itemInCleaner != null) {
            ItemStack stack = new ItemStack(this.itemInCleaner);
            this.setItemInCleaner(null);
            return stack;
        }
        return ItemStack.EMPTY;
    }

    @Override
    public void setInventorySlotContents(int index, ItemStack stack) {
        if (index == 0) {
            this.setItemInCleaner(stack.getItem());
        }
    }

    @Override
    public int getInventoryStackLimit() {
        return 1;
    }

    @Override
    public boolean isUsableByPlayer(EntityPlayer player) {
        return true;
    }

    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        return index == 0 && !stack.isEmpty() && stack.getItem() instanceof ItemCoveredFossil;
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public int[] getSlotsForFace(EnumFacing side) {
        return new int[]{0};
    }

    @Override
    public boolean canInsertItem(int index, ItemStack stack, EnumFacing direction) {
        return index == 0 && direction != EnumFacing.DOWN && !stack.isEmpty() && stack.getItem() instanceof ItemCoveredFossil;
    }

    @Override
    public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction) {
        return index == 0 && direction == EnumFacing.DOWN && !stack.isEmpty() && stack.getItem() instanceof ItemFossil;
    }
}

