/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.api.events.AnvilEvent;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.common.item.ItemPokeballDisc;
import com.pixelmongenerations.common.item.ItemPokeballLid;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonItemsPokeballs;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.MinecraftForge;

public class TileEntityAnvil
extends TileEntity {
    public Item itemOnAnvil = null;
    public int state = 0;
    int count = 0;

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        if (this.itemOnAnvil != null) {
            nbt.setInteger("ItemOnAnvil", Item.getIdFromItem(this.itemOnAnvil));
        }
        nbt.setInteger("AnvilItemState", this.state);
        return nbt;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        Item item = this.itemOnAnvil = nbt.hasKey("ItemOnAnvil") ? Item.getItemById(nbt.getInteger("ItemOnAnvil")) : null;
        if (nbt.hasKey("AnvilItemState")) {
            this.state = nbt.getInteger("AnvilItemState");
        }
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBT(nbt);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBT(pkt.getNbtCompound());
    }

    public boolean blockHit(int f, EntityPlayerMP player) {
        int neededHits = 0;
        if (this.itemOnAnvil == PixelmonItems.aluminiumIngot) {
            neededHits = 15;
        } else if (this.itemOnAnvil == PixelmonItems.aluminiumPlate || this.itemOnAnvil == PixelmonItemsPokeballs.aluBase || this.itemOnAnvil == PixelmonItemsPokeballs.ironBase || this.itemOnAnvil instanceof ItemPokeballLid || this.itemOnAnvil instanceof ItemPokeballDisc || this.itemOnAnvil == PixelmonItemsPokeballs.ironDisc || this.itemOnAnvil == PixelmonItemsPokeballs.aluDisc) {
            neededHits = 5;
        }
        AnvilEvent.BeatAnvilEvent beatAnvilEvent = new AnvilEvent.BeatAnvilEvent(player, this, this.itemOnAnvil, neededHits, f);
        if (MinecraftForge.EVENT_BUS.post(beatAnvilEvent)) {
            return false;
        }
        f = beatAnvilEvent.getForce();
        boolean returnVal = false;
        if (this.itemOnAnvil == PixelmonItems.aluminiumIngot) {
            this.count += f;
            if (this.count >= beatAnvilEvent.getNeededHits()) {
                this.count = 0;
                ++this.state;
                returnVal = true;
                this.world.playSound(null, this.pos, SoundEvents.BLOCK_ANVIL_LAND, SoundCategory.BLOCKS, 0.25f, 1.0f);
                if (this.state == 3) {
                    this.state = 0;
                    AnvilEvent.FinishedSmithEvent finishedSmithEvent = new AnvilEvent.FinishedSmithEvent(player, this, PixelmonItems.aluminiumPlate);
                    MinecraftForge.EVENT_BUS.post(finishedSmithEvent);
                    this.itemOnAnvil = finishedSmithEvent.getSmithedItem();
                }
                ((WorldServer)player.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
            }
        } else if (this.itemOnAnvil == PixelmonItems.aluminiumPlate || this.itemOnAnvil == PixelmonItemsPokeballs.aluBase || this.itemOnAnvil == PixelmonItemsPokeballs.ironBase || this.itemOnAnvil instanceof ItemPokeballLid) {
            AnvilEvent.MaterialChangedEvent materialChangedEvent;
            this.count += f;
            if (this.count >= beatAnvilEvent.getNeededHits() && !MinecraftForge.EVENT_BUS.post(materialChangedEvent = new AnvilEvent.MaterialChangedEvent(player, this, new ItemStack(this.itemOnAnvil), true))) {
                this.world.playSound(null, this.pos, SoundEvents.ENTITY_PLAYER_LEVELUP, SoundCategory.BLOCKS, 0.5f, 1.0f);
                DropItemHelper.giveItemStackToPlayer(player, (Integer)1, materialChangedEvent.getMaterial());
                if (PixelmonConfig.allowAnvilAutoloading) {
                    Item itemReplacement = null;
                    if (this.itemOnAnvil == PixelmonItems.aluminiumPlate) {
                        itemReplacement = PixelmonItems.aluminiumIngot;
                    } else if (this.itemOnAnvil == PixelmonItemsPokeballs.aluBase) {
                        itemReplacement = PixelmonItemsPokeballs.aluDisc;
                    } else if (this.itemOnAnvil == PixelmonItemsPokeballs.ironBase) {
                        itemReplacement = PixelmonItemsPokeballs.ironDisc;
                    } else if (this.itemOnAnvil instanceof ItemPokeballLid) {
                        itemReplacement = PixelmonItemsPokeballs.getDiscFromEnum(((ItemPokeballLid)this.itemOnAnvil).pokeball);
                    }
                    if (itemReplacement != null && player.inventory.hasItemStack(new ItemStack(itemReplacement))) {
                        boolean itemFound = false;
                        for (int i = 0; i < player.inventory.mainInventory.size(); ++i) {
                            if (player.inventory.mainInventory.get(i).isEmpty() || player.inventory.mainInventory.get(i).getItem() != itemReplacement) continue;
                            materialChangedEvent = new AnvilEvent.MaterialChangedEvent(player, this, player.inventory.mainInventory.get(i), false);
                            if (MinecraftForge.EVENT_BUS.post(materialChangedEvent)) break;
                            player.inventory.clearMatchingItems(itemReplacement, 0, 1, null);
                            this.itemOnAnvil = itemReplacement;
                            this.world.playSound(null, this.pos, SoundEvents.BLOCK_ANVIL_LAND, SoundCategory.BLOCKS, 0.15f, 0.9f);
                            this.count = 0;
                            this.state = 0;
                            itemFound = true;
                            break;
                        }
                        if (!(itemFound || player.getHeldItemOffhand().isEmpty() || player.getHeldItemOffhand().getItem() != itemReplacement || MinecraftForge.EVENT_BUS.post(materialChangedEvent = new AnvilEvent.MaterialChangedEvent(player, this, player.getHeldItemOffhand(), false)))) {
                            player.inventory.clearMatchingItems(itemReplacement, 0, 1, null);
                            this.itemOnAnvil = itemReplacement;
                            this.world.playSound(null, this.pos, SoundEvents.BLOCK_ANVIL_LAND, SoundCategory.BLOCKS, 0.15f, 0.9f);
                            this.count = 0;
                            this.state = 0;
                            itemFound = true;
                        }
                        if (!itemFound) {
                            this.itemOnAnvil = null;
                        }
                    } else {
                        this.itemOnAnvil = null;
                        this.state = 0;
                        this.count = 0;
                    }
                } else {
                    this.itemOnAnvil = null;
                    this.state = 0;
                    this.count = 0;
                }
                ((WorldServer)player.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
            }
        } else if (this.itemOnAnvil instanceof ItemPokeballDisc || this.itemOnAnvil == PixelmonItemsPokeballs.ironDisc || this.itemOnAnvil == PixelmonItemsPokeballs.aluDisc) {
            this.count += f;
            if (this.count >= beatAnvilEvent.getNeededHits()) {
                this.count = 0;
                ++this.state;
                returnVal = true;
                this.world.playSound(null, this.pos, SoundEvents.BLOCK_ANVIL_LAND, SoundCategory.BLOCKS, 0.2f, 1.0f);
                if (this.state == 3) {
                    this.count = 0;
                    this.state = 0;
                    Item item = this.itemOnAnvil == PixelmonItemsPokeballs.ironDisc ? PixelmonItemsPokeballs.ironBase : (this.itemOnAnvil == PixelmonItemsPokeballs.aluDisc ? PixelmonItemsPokeballs.aluBase : PixelmonItemsPokeballs.getLidFromEnum(((ItemPokeballDisc)this.itemOnAnvil).pokeball));
                    AnvilEvent.FinishedSmithEvent finishedSmithEvent = new AnvilEvent.FinishedSmithEvent(player, this, item);
                    MinecraftForge.EVENT_BUS.post(finishedSmithEvent);
                    this.itemOnAnvil = finishedSmithEvent.getSmithedItem();
                }
                ((WorldServer)player.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
            }
        }
        return returnVal;
    }
}

