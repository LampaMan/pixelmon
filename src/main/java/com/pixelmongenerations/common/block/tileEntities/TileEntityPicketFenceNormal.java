/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class TileEntityPicketFenceNormal
extends TileEntity
implements ISpecialTexture {
    private final ResourceLocation texture;

    public TileEntityPicketFenceNormal() {
        this("picketFenceTexture.png");
    }

    public TileEntityPicketFenceNormal(String texture) {
        this.texture = new ResourceLocation("pixelmon:textures/blocks/" + texture);
    }

    @Override
    public ResourceLocation getTexture() {
        return this.texture;
    }
}

