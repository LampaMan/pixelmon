/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.SpawnColors;
import java.awt.Color;
import java.util.List;
import net.minecraft.util.math.Vec3i;

public interface IShrineBlock {
    public int getTick();

    public PokemonGroup.PokemonData getActiveGroup();

    public Vec3i getSpawnPos();

    public int maxTick();

    default public List<Color> getColors(EnumSpecies species) {
        return SpawnColors.getColors(species);
    }
}

