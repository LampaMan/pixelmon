/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableMap
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.block.enums.EnumUsed;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.spawnmethod.BlockBirdShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import com.pixelmongenerations.common.item.IShrineItem;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumForms;
import com.pixelmongenerations.core.util.SpawnColors;
import java.awt.Color;
import java.util.List;
import java.util.Map;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class TileEntityBirdShrine extends TileEntityShrine {

    Map<EnumSpecies, List<Color>> colors = ImmutableMap.of(
            EnumSpecies.Articuno, Lists.newArrayList(SpawnColors.LIGHT_BLUE, SpawnColors.WHITE),
            EnumSpecies.Zapdos, Lists.newArrayList(SpawnColors.YELLOW, SpawnColors.ORANGE, SpawnColors.BLACK),
            EnumSpecies.Moltres, Lists.newArrayList(SpawnColors.ORANGE, SpawnColors.WHITE, SpawnColors.RED));

    public byte orbIn = 0;

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        nbt.setByte("OrbIn", this.orbIn);
        return nbt;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        this.orbIn = nbt.getByte("OrbIn");
    }

    @Override
    protected PokemonGroup.PokemonData selectActiveGroup() {
        Block block = this.world.getBlockState(this.pos).getBlock();
        if (block instanceof BlockBirdShrine) {
            EnumForms form = this.orbIn == 0 ? null : (this.orbIn == 2 ? EnumForms.Galarian : EnumForms.NoForm);
            return PokemonGroup.PokemonData.of(((BlockBirdShrine)block).getSpecies(), form);
        }
        return null;
    }

    @Override
    public int maxTick() {
        return 100;
    }

    @Override
    public List<Color> getColors(EnumSpecies species) {
        return this.colors.get((Object)species);
    }

    @Override
    public void activate(EntityPlayer player, World world, BlockShrine block, IBlockState state, ItemStack item) {
        if (this.orbIn != 0) {
            return;
        }
        IBlockState iBlockState = state;
        this.orbIn = (byte)(((IShrineItem)((Object)item.getItem())).getGroup().contains(EnumForms.Galarian) ? 2 : 1);
        if (!PixelmonConfig.reusableBirdShrines) {
            iBlockState = state.withProperty(BlockBirdShrine.USED, EnumUsed.YES);
            world.setBlockState(this.pos, iBlockState, 3);
        }
        item.shrink(1);
        this.startSpawning(player, iBlockState, world, this.pos);
    }

    @Override
    protected void finish() {
        this.orbIn = 0;
    }

    @Override
    public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newSate) {
        if (newSate.getBlock() instanceof BlockBirdShrine) {
            return false;
        }
        return super.shouldRefresh(world, pos, oldState, newSate);
    }

    @Override
    public String getRightClickMessage(ItemStack heldItem, IBlockState state) {
        if (state.getBlock() == PixelmonBlocks.shrineUno) {
            return "pixelmon.articuno_shrine.right_click";
        }
        if (state.getBlock() == PixelmonBlocks.shrineDos) {
            return "pixelmon.zapdos_shrine.right_click";
        }
        if (state.getBlock() == PixelmonBlocks.shrineTres) {
            return "pixelmon.moltres_shrine.right_click";
        }
        return "";
    }
}

