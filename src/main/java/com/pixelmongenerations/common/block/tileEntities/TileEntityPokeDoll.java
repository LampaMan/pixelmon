/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.common.block.tileEntities.ICullable;
import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import javax.annotation.Nullable;
import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class TileEntityPokeDoll
extends TileEntity
implements ISpecialTexture,
ICullable {
    private String pokemon = null;
    private ResourceLocation texture;
    private BlockModelHolder<GenericSmdModel> model;
    private boolean shiny;
    private float scale;
    private int rotation = 8;
    private boolean culled;

    public TileEntityPokeDoll() {
    }

    public TileEntityPokeDoll(String name, boolean shiny) {
        this(name, shiny, 1.0f);
    }

    public TileEntityPokeDoll(String name, boolean shiny, float scale) {
        this.pokemon = name;
        this.shiny = shiny;
        this.scale = scale;
        this.getModelAndTexture();
    }

    public void getModelAndTexture() {
        if (this.pokemon == null) {
            this.pokemon = "charizard";
        }
        if (!this.shiny) {
            this.texture = new ResourceLocation("pixelmon:textures/blocks/pokedolls/" + this.pokemon + ".png");
            this.model = new BlockModelHolder("blocks/pokedolls/" + this.pokemon + ".pqc");
        } else {
            this.texture = new ResourceLocation("pixelmon:textures/blocks/pokedolls/shiny/" + this.pokemon + ".png");
            this.model = new BlockModelHolder("blocks/pokedolls/" + this.pokemon + ".pqc");
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        if (this.pokemon != null && !this.pokemon.isEmpty()) {
            nbt.setString("Pokemon", this.pokemon);
        }
        nbt.setInteger("PokedollRotation", this.rotation);
        return nbt;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        this.pokemon = nbt.hasKey("Pokemon") ? nbt.getString("Pokemon") : "charizard";
        this.rotation = nbt.hasKey("PokedollRotation") ? nbt.getInteger("PokedollRotation") : 8;
    }

    @Override
    public ResourceLocation getTexture() {
        if (this.texture == null) {
            this.getModelAndTexture();
        }
        return this.texture;
    }

    public BlockModelHolder<GenericSmdModel> getModel() {
        if (this.model == null) {
            this.getModelAndTexture();
        }
        return this.model;
    }

    public float getScale() {
        return this.scale;
    }

    public int getRotationIndex() {
        return this.rotation;
    }

    public void rotate() {
        this.rotation = this.rotation == 8 ? 1 : ++this.rotation;
        this.sendUpdates();
    }

    @Override
    @Nullable
    public SPacketUpdateTileEntity getUpdatePacket() {
        NBTTagCompound compound = new NBTTagCompound();
        compound.setInteger("PokedollRotation", this.rotation);
        return new SPacketUpdateTileEntity(this.getPos(), 0, compound);
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.rotation = pkt.getNbtCompound().hasKey("PokedollRotation") ? pkt.getNbtCompound().getInteger("PokedollRotation") : 8;
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound compound = super.getUpdateTag();
        compound.setInteger("PokedollRotation", this.rotation);
        return compound;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        this.rotation = tag.hasKey("PokedollRotation") ? tag.getInteger("PokedollRotation") : 8;
    }

    private void sendUpdates() {
        this.world.markBlockRangeForRenderUpdate(this.pos, this.pos);
        this.world.notifyBlockUpdate(this.pos, this.getState(), this.getState(), 3);
        this.world.scheduleBlockUpdate(this.pos, this.getBlockType(), 0, 0);
        this.markDirty();
    }

    private IBlockState getState() {
        return this.world.getBlockState(this.pos);
    }

    @Override
    public void setCulled(boolean isCulled) {
        this.culled = isCulled;
    }

    @Override
    public boolean isCulled() {
        return this.culled;
    }
}

