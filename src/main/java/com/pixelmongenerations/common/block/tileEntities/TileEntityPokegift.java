/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.enums.EnumPokechestVisibility;
import com.pixelmongenerations.common.block.tileEntities.EnumPokegiftType;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.util.LootClaim;
import java.util.ArrayList;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.WorldServer;

public class TileEntityPokegift
extends TileEntity {
    private EnumPokechestVisibility visibility = EnumPokechestVisibility.Visible;
    private UUID ownerID = null;
    private boolean chestOneTime = true;
    private boolean dropOneTime = true;
    private EntityPixelmon pixelmon = null;
    private NBTTagCompound nbtPixelmon = new NBTTagCompound();
    private ArrayList<EntityPixelmon> specialPixelmon = new ArrayList();
    private ArrayList<NBTTagCompound> nbtSpecialPixelmon = new ArrayList();
    private EnumPokegiftType type = EnumPokegiftType.GIFT;
    private ArrayList<LootClaim> claimed = new ArrayList();

    public void setOwner(UUID id) {
        this.ownerID = id;
    }

    public UUID getOwner() {
        return this.ownerID;
    }

    public void setType(EnumPokegiftType t) {
        this.type = t;
    }

    public EnumPokegiftType getType() {
        return this.type;
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound tagger) {
        int i;
        super.writeToNBT(tagger);
        tagger.setLong("ownerIDMost", this.ownerID == null ? -1L : this.ownerID.getMostSignificantBits());
        tagger.setLong("ownerIDLeast", this.ownerID == null ? -1L : this.ownerID.getLeastSignificantBits());
        tagger.setBoolean("chestOneTime", this.chestOneTime);
        tagger.setBoolean("dropOneTime", this.dropOneTime);
        if (!this.claimed.isEmpty()) {
            NBTTagCompound claimedTag = new NBTTagCompound();
            for (i = 0; i < this.claimed.size(); ++i) {
                NBTTagCompound playerInfoTag = new NBTTagCompound();
                LootClaim playerClaim = this.claimed.get(i);
                playerInfoTag.setLong("most", playerClaim.getPlayerID().getMostSignificantBits());
                playerInfoTag.setLong("least", playerClaim.getPlayerID().getLeastSignificantBits());
                playerInfoTag.setLong("timeClaimed", playerClaim.getTimeClaimed());
                claimedTag.setTag("player" + i, playerInfoTag);
            }
            tagger.setTag("claimedPlayers", claimedTag);
        }
        tagger.setInteger("type", this.type.ordinal());
        if (!this.nbtPixelmon.isEmpty()) {
            if (this.pixelmon != null) {
                this.pixelmon.writeEntityToNBT(this.nbtPixelmon);
            }
            tagger.setTag("pixelmon", this.nbtPixelmon);
        } else if (this.type == EnumPokegiftType.EVENT) {
            if (!this.specialPixelmon.isEmpty()) {
                for (EntityPixelmon p : this.specialPixelmon) {
                    NBTTagCompound nbt = new NBTTagCompound();
                    p.writeEntityToNBT(nbt);
                    this.nbtSpecialPixelmon.add(nbt);
                }
            }
            if (!this.nbtSpecialPixelmon.isEmpty()) {
                NBTTagCompound specialTag = new NBTTagCompound();
                for (i = 0; i < this.nbtSpecialPixelmon.size(); ++i) {
                    specialTag.setTag("special" + i, this.nbtSpecialPixelmon.get(i));
                }
                tagger.setTag("specials", specialTag);
            }
        }
        return tagger;
    }

    @Override
    public void readFromNBT(NBTTagCompound tagger) {
        int i;
        if (tagger.getLong("ownerIDMost") != -1L) {
            this.ownerID = new UUID(tagger.getLong("ownerIDMost"), tagger.getLong("ownerIDLeast"));
        }
        this.chestOneTime = tagger.getBoolean("chestOneTime");
        this.dropOneTime = tagger.getBoolean("dropOneTime");
        if (tagger.hasKey("claimedPlayers")) {
            NBTTagCompound claimedTag = (NBTTagCompound)tagger.getTag("claimedPlayers");
            i = 0;
            while (claimedTag.hasKey("player" + i)) {
                NBTTagCompound playerTag = (NBTTagCompound)claimedTag.getTag("player" + i);
                this.claimed.add(new LootClaim(new UUID(playerTag.getLong("most"), playerTag.getLong("least")), playerTag.getLong("timeClaimed")));
                ++i;
            }
        }
        this.type = EnumPokegiftType.values()[tagger.getInteger("type")];
        if (this.type == EnumPokegiftType.GIFT) {
            this.nbtPixelmon = tagger.getCompoundTag("pixelmon");
        } else if (tagger.hasKey("specials")) {
            NBTTagCompound specialTag = (NBTTagCompound)tagger.getTag("specials");
            i = 0;
            while (specialTag.hasKey("special" + i)) {
                NBTTagCompound nbt = (NBTTagCompound)specialTag.getTag("special" + i);
                this.nbtSpecialPixelmon.add(nbt);
                ++i;
            }
        }
        super.readFromNBT(tagger);
    }

    public boolean canClaim(UUID playerID) {
        if (!this.dropOneTime) {
            return true;
        }
        LootClaim claim = this.getLootClaim(playerID);
        return claim == null;
    }

    public LootClaim getLootClaim(UUID playerID) {
        for (LootClaim claim : this.claimed) {
            if (!claim.getPlayerID().toString().equals(playerID.toString())) continue;
            return claim;
        }
        return null;
    }

    public void addClaimer(UUID playerID) {
        if (this.dropOneTime) {
            this.claimed.add(new LootClaim(playerID, System.currentTimeMillis()));
        }
    }

    public void removeClaimer(UUID playerID) {
        this.claimed.remove(this.getLootClaim(playerID));
    }

    public boolean shouldBreakBlock() {
        return this.chestOneTime;
    }

    public void setChestOneTime(boolean val) {
        this.chestOneTime = val;
    }

    public boolean getChestMode() {
        return this.chestOneTime;
    }

    public void setDropOneTime(boolean val) {
        this.dropOneTime = val;
    }

    public boolean getDropMode() {
        return this.dropOneTime;
    }

    public boolean isUsableByPlayer(EntityPlayer player) {
        return this.world.getTileEntity(this.pos) == this && player.getDistanceSq((double)this.pos.getX() + 0.5, (double)this.pos.getY() + 0.5, (double)this.pos.getZ() + 0.5) < 64.0;
    }

    public EntityPixelmon getPixelmon() {
        if (this.pixelmon == null && !this.nbtPixelmon.isEmpty()) {
            this.pixelmon = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(this.nbtPixelmon, this.world);
        }
        return this.pixelmon;
    }

    public ArrayList<EntityPixelmon> getSpecialPixelmon() {
        if (this.specialPixelmon.isEmpty() && !this.nbtSpecialPixelmon.isEmpty()) {
            for (NBTTagCompound nbt : this.nbtSpecialPixelmon) {
                EntityPixelmon p = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(nbt, this.world);
                this.specialPixelmon.add(p);
            }
        }
        return this.specialPixelmon;
    }

    public void setPixelmon(EntityPixelmon pixelmon) {
        this.pixelmon = pixelmon;
        pixelmon.writeEntityToNBT(this.nbtPixelmon);
    }

    public void setAllSpecialPixelmon(ArrayList<EntityPixelmon> pixelmon) {
        this.specialPixelmon.clear();
        this.specialPixelmon.addAll(pixelmon);
    }

    public void setSpecialPixelmon(EntityPixelmon pixelmon) {
        this.specialPixelmon.add(pixelmon);
    }

    public EnumPokechestVisibility getVisibility() {
        return this.visibility;
    }

    public void setVisibility(EnumPokechestVisibility visible) {
        this.visibility = visible;
        ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
    }
}

