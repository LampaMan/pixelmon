/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import java.util.UUID;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.WorldServer;

public class TileEntityTree
extends TileEntity {
    private int treeType = 1;
    private UUID owner = null;

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        this.setTreeType(nbt.getInteger("treeType"));
        if (nbt.hasKey("owner")) {
            UUID uuid;
            this.owner = uuid = UUID.fromString(nbt.getString("owner"));
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        nbt.setInteger("treeType", this.treeType);
        if (this.owner != null) {
            nbt.setString("owner", this.owner.toString());
        }
        return nbt;
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBT(nbt);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        NBTTagCompound nbt = pkt.getNbtCompound();
        this.setTreeType(nbt.getInteger("treeType"));
    }

    @Override
    public void onLoad() {
        if (this.hasWorld() && this.getWorld() instanceof WorldServer) {
            ((WorldServer)this.getWorld()).getPlayerChunkMap().markBlockForUpdate(this.pos);
        }
    }

    public UUID getOwnerUUID() {
        return this.owner;
    }

    public void setOwner(UUID owner) {
        this.owner = owner;
        this.markDirty();
    }

    public int getTreeType() {
        return this.treeType;
    }

    public void setTreeType(int type) {
        this.treeType = type;
        this.markDirty();
    }
}

