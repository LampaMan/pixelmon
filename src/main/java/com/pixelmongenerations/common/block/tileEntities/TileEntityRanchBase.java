/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.api.events.BreedEvent;
import com.pixelmongenerations.common.block.IPokemonOwner;
import com.pixelmongenerations.common.block.ranch.RanchBounds;
import com.pixelmongenerations.common.entity.pixelmon.Entity10CanBreed;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.EnumAggression;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumBreedingStrength;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import com.pixelmongenerations.core.util.Bounds;
import com.pixelmongenerations.core.util.PixelmonMethods;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.UUID;
import java.util.stream.Collectors;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public abstract class TileEntityRanchBase
extends TileEntity
implements IPokemonOwner,
ITickable {
    public static final String RANCH_ID_COUNT = "idCount";
    public static boolean enabled = PixelmonConfig.allowBreeding;
    private static final int REFRESH_RATE = 100;
    private static final int UPDATE_BREEDING_RATE = 400;
    protected ArrayList<RanchPokemon> ids = new ArrayList();
    protected final ArrayList<EntityPixelmon> entities = new ArrayList();
    protected RanchBounds ranchBounds = new RanchBounds(this);
    public int ranchWidth = 4;
    public int ranchLength = 4;
    int tick = 0;
    public boolean aboveGround = false;
    public int percentAbove = 0;

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        nbt.setInteger(RANCH_ID_COUNT, this.ids.size());
        for (int i = 0; i < this.ids.size(); ++i) {
            nbt.setIntArray("ID" + i, this.ids.get((int)i).pokemonID);
            nbt.setLong("ownerLeast" + i, this.ids.get((int)i).ownerUUID.getLeastSignificantBits());
            nbt.setLong("ownerMost" + i, this.ids.get((int)i).ownerUUID.getMostSignificantBits());
        }
        this.ranchBounds.writeToNBT(nbt);
        try {
            for (EntityPixelmon p : this.entities) {
                PlayerComputerStorage storage;
                EntityPlayerMP owner = (EntityPlayerMP)p.getOwner();
                if (owner == null || (storage = PixelmonStorage.computerManager.getPlayerStorage(owner)) == null) continue;
                storage.updatePokemonEntry(p);
            }
        }
        catch (Exception exception) {
            // empty catch block
        }
        return nbt;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        if (nbt.hasKey(RANCH_ID_COUNT)) {
            int count = nbt.getInteger(RANCH_ID_COUNT);
            this.ids.clear();
            for (int i = 0; i < count; ++i) {
                this.ids.add(new RanchPokemon(new UUID(nbt.getLong("ownerMost" + i), nbt.getLong("ownerLeast" + i)), nbt.getIntArray("ID" + i)));
            }
        }
        this.ranchBounds.readFromNBT(nbt);
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBT(nbt);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBT(pkt.getNbtCompound());
        this.updateStatus();
    }

    @Override
    public void update() {
        ++this.tick;
        if (!this.world.isRemote) {
            if (this.tick % 400 == 0 && this.isRanchOwnerInGame() && !this.entities.isEmpty()) {
                try {
                    this.entities.forEach(Entity10CanBreed::updateBreeding);
                }
                catch (ConcurrentModificationException concurrentModificationException) {
                    // empty catch block
                }
            }
            if (this.tick % 100 == 0 && this.isRanchOwnerInGame()) {
                for (RanchPokemon id : this.ids) {
                    EntityPixelmon p;
                    if (this.entityExists(id) || !this.ownerInGame(id) || (p = this.createEntity(id)) == null) continue;
                    this.entities.add(p);
                }
                Iterator<EntityPixelmon> i = this.entities.iterator();
                while (i.hasNext()) {
                    EntityPixelmon pixelmon = i.next();
                    if (pixelmon == null) {
                        i.remove();
                        continue;
                    }
                    if (!pixelmon.isInRanchBlock) {
                        pixelmon.unloadEntity();
                        i.remove();
                        this.removePokemon(new RanchPokemon(pixelmon.getOwnerId(), pixelmon.getPokemonId()));
                        continue;
                    }
                    if (pixelmon.isLoaded(false) || !this.setLocationForEntity(pixelmon)) continue;
                    pixelmon.releaseFromPokeball();
                }
            }
        } else {
            this.checkAboveGround();
        }
    }

    public abstract boolean isRanchOwnerInGame();

    public abstract void onActivate(EntityPlayer var1);

    @Override
    public void updateStatus() {
    }

    public abstract boolean hasBreedingPartner(EntityPixelmon var1);

    public EntityPixelmon getFirstBreedingPartner(EntityPixelmon pixelmon) {
        ArrayList possiblePartners = (ArrayList)this.entities.stream().filter(p -> p != pixelmon).filter(p -> Entity10CanBreed.canBreed(pixelmon, p)).collect(Collectors.toList());
        if (possiblePartners.size() == 1) {
            return (EntityPixelmon)possiblePartners.get(0);
        }
        if (possiblePartners.size() > 1) {
            EntityPixelmon highestPartner = (EntityPixelmon)possiblePartners.get(0);
            for (int i = 1; i < possiblePartners.size(); ++i) {
                if (((EntityPixelmon)possiblePartners.get(i)).getNumBreedingLevels() <= highestPartner.getNumBreedingLevels()) continue;
                highestPartner = (EntityPixelmon)possiblePartners.get(i);
            }
            return highestPartner;
        }
        return null;
    }

    public abstract void claimEgg(EntityPlayerMP var1);

    public void removePokemon(EntityPlayerMP player, int[] id) {
        try {
            PlayerComputerStorage storage = PixelmonStorage.computerManager.getPlayerStorage(player);
            NBTTagCompound pokemonNBT = storage.getPokemonNBT(id);
            pokemonNBT.setBoolean("isInRanch", false);
            pokemonNBT.setShort("BreedingInteractions", (short)-1);
            storage.setChanged(id);
            Iterator<EntityPixelmon> i = this.entities.iterator();
            while (i.hasNext()) {
                EntityPixelmon pixelmon = i.next();
                if (pixelmon == null || !PixelmonMethods.isIDSame(pixelmon, id)) continue;
                pixelmon.unloadEntity();
                i.remove();
                break;
            }
            this.ids.removeIf(p -> PixelmonMethods.isIDSame(p.pokemonID, id));
        }
        catch (Exception exception) {
            // empty catch block
        }
        this.updateStatus();
    }

    private void removePokemon(RanchPokemon poke) {
        try {
            NBTTagCompound pokemonNBT;
            PlayerComputerStorage storage = PixelmonStorage.computerManager.getPlayerStorageFromUUID(this.world, poke.ownerUUID);
            if (storage != null && (pokemonNBT = storage.getPokemonNBT(poke.pokemonID)) != null) {
                pokemonNBT.setBoolean("isInRanch", false);
                pokemonNBT.setShort("BreedingInteractions", (short)-1);
                storage.setChanged(poke.pokemonID);
                if (storage.isOffline()) {
                    PixelmonStorage.computerManager.savePlayer(storage);
                }
            }
            Iterator<EntityPixelmon> i = this.entities.iterator();
            while (i.hasNext()) {
                EntityPixelmon pixelmon = i.next();
                if (!PixelmonMethods.isIDSame(pixelmon, poke.pokemonID)) continue;
                pixelmon.unloadEntity();
                i.remove();
                break;
            }
            this.ids.removeIf(p -> PixelmonMethods.isIDSame(p.pokemonID, poke.pokemonID));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        this.updateStatus();
    }

    public void addPokemon(EntityPlayerMP player, int[] id) {
        try {
            PlayerComputerStorage storage = PixelmonStorage.computerManager.getPlayerStorage(player);
            RanchPokemon poke = new RanchPokemon(player.getUniqueID(), id);
            this.ids.add(poke);
            NBTTagCompound pokemonTag = storage.getPokemonNBT(id);
            pokemonTag.setBoolean("isInRanch", true);
            pokemonTag.setLong("lastBreedingTime", this.world.getTotalWorldTime());
            pokemonTag.setShort("BreedingInteractions", (short)0);
            storage.setChanged(id);
            EntityPixelmon pixelmon = this.createEntity(poke);
            if (pixelmon != null) {
                this.entities.add(pixelmon);
            }
        }
        catch (Exception exception) {
            // empty catch block
        }
        this.updateStatus();
    }

    public ArrayList<PixelmonData> getPokemonData() {
        ArrayList<PixelmonData> pokemonData = new ArrayList<PixelmonData>();
        try {
            for (RanchPokemon poke : this.ids) {
                try {
                    PlayerComputerStorage s = PixelmonStorage.computerManager.getPlayerStorageFromUUID(this.world, poke.ownerUUID);
                    pokemonData.add(new PixelmonData(s.getPokemonNBT(poke.pokemonID)));
                }
                catch (Exception e) {
                    System.out.println("Player has a Pok\u00e9mon in a ranch block which isn't in his/her PC.");
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return pokemonData;
    }

    @Override
    public int getEntityCount() {
        return this.entities.size();
    }

    public void onDestroy() {
        if (!this.world.isRemote) {
            try {
                while (this.ids.size() != 0) {
                    RanchPokemon poke = this.ids.get(0);
                    this.removePokemon(poke);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private EntityPixelmon createEntity(RanchPokemon poke) {
        EntityPixelmon pixelmon = null;
        try {
            PlayerComputerStorage s = PixelmonStorage.computerManager.getPlayerStorageFromUUID(this.world, poke.ownerUUID);
            pixelmon = s.getPokemonEntity(poke.pokemonID, this.world);
            if (pixelmon == null || !pixelmon.isInRanchBlock) {
                for (int i = 0; i < this.ids.size(); ++i) {
                    if (!PixelmonMethods.isIDSame(this.ids.get((int)i).pokemonID, poke.pokemonID)) continue;
                    this.ids.remove(i);
                    --i;
                }
                return null;
            }
            pixelmon.aggression = EnumAggression.passive;
            pixelmon.getEntityAttribute(SharedMonsterAttributes.FOLLOW_RANGE).setBaseValue(4.0);
            pixelmon.setSpawnLocation(SpawnLocation.Land);
            pixelmon.resetAI();
            if (this.setLocationForEntity(pixelmon)) {
                pixelmon.world.spawnEntity(pixelmon);
            }
            pixelmon.setRanchBlockOwner(this);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return pixelmon;
    }

    private boolean setLocationForEntity(EntityPixelmon pixelmon) {
        for (int i = 0; i < 5; ++i) {
            int[] xz = this.ranchBounds.getRandomLocation(this.world.rand);
            BlockPos pos = new BlockPos(xz[0], this.pos.getY() + RandomHelper.getRandomNumberBetween(0, 3), xz[1]);
            pos = pos.up();
            pixelmon.setLocationAndAngles(pos.getX(), pos.getY(), pos.getZ(), 0.0f, 0.0f);
            if (!pixelmon.getCanSpawnHere()) continue;
            return true;
        }
        return false;
    }

    private boolean ownerInGame(RanchPokemon poke) {
        if (this.world == null || poke == null) {
            return false;
        }
        return this.world.getPlayerEntityByUUID(poke.ownerUUID) != null;
    }

    protected abstract void checkAboveGround();

    private boolean entityExists(RanchPokemon poke) {
        for (EntityPixelmon p : this.entities) {
            if (!PixelmonMethods.isIDSame(p, poke.pokemonID)) continue;
            return true;
        }
        return false;
    }

    public void setInitBounds() {
        this.ranchBounds = new RanchBounds(this, this.pos.getZ() + this.ranchWidth, this.pos.getX() - this.ranchLength, this.pos.getZ() - this.ranchWidth, this.pos.getX() + this.ranchLength, this.pos.getY());
    }

    @Override
    public Bounds getBounds() {
        return this.ranchBounds;
    }

    @Override
    public void setWorld(World world) {
        super.setWorld(world);
        this.ranchBounds.setWorldObj(world);
    }

    public boolean applyHourglass() {
        boolean hourglassused = false;
        for (EntityPixelmon e : this.entities) {
            if (e.breedingStrength == EnumBreedingStrength.NONE || e.getNumBreedingLevels() >= PixelmonConfig.numBreedingLevels) continue;
            hourglassused = true;
            BreedEvent.BreedingLevelChangedEvent levelChangedEvent = new BreedEvent.BreedingLevelChangedEvent(e.getOwnerId(), this, e, e.getNumBreedingLevels(), e.getNumBreedingLevels() + 1);
            MinecraftForge.EVENT_BUS.post(levelChangedEvent);
            e.setNumBreedingLevels(levelChangedEvent.getNewLevel());
        }
        return hourglassused;
    }

    public class RanchPokemon {
        UUID ownerUUID;
        int[] pokemonID;

        public RanchPokemon(UUID ownerUUID, int[] pokemonID) {
            this.ownerUUID = ownerUUID;
            this.pokemonID = pokemonID;
        }
    }
}

