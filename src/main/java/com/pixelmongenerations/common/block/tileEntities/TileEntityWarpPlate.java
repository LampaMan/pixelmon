/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import net.minecraft.command.CommandBase;
import net.minecraft.command.NumberInvalidException;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;

public class TileEntityWarpPlate
extends TileEntity {
    private String warpX = null;
    private String warpY = null;
    private String warpZ = null;
    private BlockPos warpPosition = null;

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        if (this.warpX != null) {
            nbt.setString("warp_x", this.warpX);
            nbt.setString("warp_y", this.warpY);
            nbt.setString("warp_z", this.warpZ);
        }
        return nbt;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        if (nbt.hasKey("warp_x")) {
            this.warpX = nbt.getString("warp_x");
            this.warpY = nbt.getString("warp_y");
            this.warpZ = nbt.getString("warp_z");
            this.warpPosition = this.calculatePosition(this.warpX, this.warpY, this.warpZ);
        } else if (nbt.hasKey("warpPosition")) {
            this.warpPosition = BlockPos.fromLong(nbt.getLong("warpPosition"));
            this.warpX = String.valueOf(this.warpPosition.getX());
            this.warpY = String.valueOf(this.warpPosition.getY());
            this.warpZ = String.valueOf(this.warpPosition.getZ());
        }
    }

    public BlockPos getWarpPosition() {
        return this.warpPosition;
    }

    public void setWarpPosition(String x, String y, String z) {
        this.warpX = x;
        this.warpY = y;
        this.warpZ = z;
        this.warpPosition = this.calculatePosition(this.warpX, this.warpY, this.warpZ);
        this.markDirty();
    }

    public BlockPos calculatePosition(String warpX, String warpY, String warpZ) {
        String[] wPos = new String[]{warpX, warpY, warpZ};
        int[] pos = new int[]{this.getPos().getX(), this.getPos().getY(), this.getPos().getZ()};
        double[] nPos = new double[3];
        for (int i = 0; i < wPos.length; ++i) {
            try {
                nPos[i] = CommandBase.parseDouble(pos[i], wPos[i], false);
                continue;
            }
            catch (NullPointerException | NumberInvalidException e) {
                return null;
            }
        }
        return new BlockPos(nPos[0], nPos[1], nPos[2]);
    }
}

