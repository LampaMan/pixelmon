/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.BlockDimensionalPortal;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;

public class TileEntityDimensionalPortal
extends TileEntity {
    public BlockPos teleportLocation = null;

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        System.out.println("READUBG PORTAL");
        super.readFromNBT(compound);
        BlockDimensionalPortal block = (BlockDimensionalPortal)this.getBlockType();
        if (compound.hasKey("TeleportX")) {
            this.teleportLocation = new BlockPos(compound.getInteger("TeleportX"), compound.getInteger("TeleportY"), compound.getInteger("TeleportZ"));
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        BlockDimensionalPortal block = (BlockDimensionalPortal)this.getBlockType();
        if (this.teleportLocation != null) {
            compound.setInteger("TeleportX", this.teleportLocation.getX());
            compound.setInteger("TeleportY", this.teleportLocation.getY());
            compound.setInteger("TeleportZ", this.teleportLocation.getZ());
        }
        return compound;
    }
}

