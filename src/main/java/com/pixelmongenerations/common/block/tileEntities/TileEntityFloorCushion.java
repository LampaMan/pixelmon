/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.decorative.FloorCushionBlock;
import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileEntityFloorCushion
extends TileEntity
implements ISpecialTexture {
    @Override
    @SideOnly(value=Side.CLIENT)
    public AxisAlignedBB getRenderBoundingBox() {
        return super.getRenderBoundingBox().expand(0.0, 3.0, 0.0);
    }

    @Override
    public ResourceLocation getTexture() {
        IBlockState state = this.getWorld().getBlockState(this.getPos());
        if (state.getBlock() instanceof FloorCushionBlock) {
            FloorCushionBlock block = (FloorCushionBlock)state.getBlock();
            return new ResourceLocation("pixelmon:textures/blocks/floorcushion-" + block.getColor().name() + ".png");
        }
        return new ResourceLocation("pixelmon:textures/blocks/floorcushion-pokeball.png");
    }
}

