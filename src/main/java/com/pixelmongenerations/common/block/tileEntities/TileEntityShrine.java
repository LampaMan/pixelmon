/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.api.events.player.PlayerShinyChanceEvent;
import com.pixelmongenerations.api.events.player.ShrineSpawnEvent;
import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.api.pokemon.SpawnPokemon;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.tileEntities.IShrineBlock;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import com.pixelmongenerations.core.util.PixelmonMethods;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public abstract class TileEntityShrine
extends TileEntity
implements ITickable,
IShrineBlock {
    private int timeSpent = 0;
    private boolean spawning = false;
    protected EntityPlayer summoningPlayer = null;
    protected IBlockState summoningState = null;
    protected PokemonGroup.PokemonData active = null;

    public abstract void activate(EntityPlayer var1, World var2, BlockShrine var3, IBlockState var4, ItemStack var5);

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        compound.setInteger("TimeSpent", this.timeSpent);
        return compound;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        this.timeSpent = compound.getInteger("TimeSpent");
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        NBTTagCompound tagCompound = new NBTTagCompound();
        this.writeToNBT(tagCompound);
        if (this.active != null) {
            tagCompound.setString("species", this.active.getSpecies().name);
            if (this.active.getForm() != null) {
                tagCompound.setInteger("form", this.active.getForm().getForm());
            }
        }
        return new SPacketUpdateTileEntity(this.pos, 0, tagCompound);
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBT(pkt.getNbtCompound());
        EnumSpecies species = EnumSpecies.getFromNameAnyCase(pkt.getNbtCompound().getString("species"));
        IEnumForm form = pkt.getNbtCompound().hasKey("form") ? species.getFormEnum(pkt.getNbtCompound().getInteger("form")) : null;
        this.active = PokemonGroup.PokemonData.of(species, form);
    }

    public void startSpawning(EntityPlayer player, IBlockState state, World world, BlockPos pos) {
        this.summoningPlayer = player;
        this.summoningState = state;
        this.active = this.selectActiveGroup();
        this.spawning = true;
        if (MinecraftForge.EVENT_BUS.post(new ShrineSpawnEvent(this.active.getSpecies(), this.active.getForm(), (EntityPlayerMP)player, world, pos))) {
            this.summoningPlayer = null;
            this.summoningState = null;
            this.active = null;
            this.spawning = false;
        }
    }

    protected abstract PokemonGroup.PokemonData selectActiveGroup();

    @Override
    public void update() {
        if (this.spawning && !this.world.isRemote) {
            this.world.notifyBlockUpdate(this.pos, this.summoningState, this.summoningState, 3);
            this.checkTick(this.timeSpent);
            if (this.timeSpent >= this.maxTick()) {
                SpawnPokemon.builder().pixelmon(this::setupPixelmon).pos(this.getSpawnPos()).yaw(this.getSpawnYaw()).build().spawn((EntityPlayerMP)this.summoningPlayer, this.world);
                this.timeSpent = 0;
                this.world.notifyBlockUpdate(this.pos, this.summoningState, this.summoningState, 3);
                this.spawning = false;
                this.summoningState = null;
                this.summoningPlayer = null;
                this.finish();
                return;
            }
            ++this.timeSpent;
        }
    }

    protected EntityPixelmon setupPixelmon(World world) {
        PokemonSpec spec = new PokemonSpec(this.getActiveGroup().getSpecies().name);
        float chance = PixelmonConfig.shinyRate;
        if (PixelmonConfig.enableCatchCombos) {
            if (PixelmonConfig.enableCatchComboShinyLock && PixelmonMethods.isCatchComboSpecies((EntityPlayerMP)this.summoningPlayer, EnumSpecies.getFromNameAnyCase(spec.name))) {
                chance = PixelmonMethods.getCatchComboChance((EntityPlayerMP)this.summoningPlayer);
            } else if (!PixelmonConfig.enableCatchComboShinyLock) {
                chance = PixelmonMethods.getCatchComboChance((EntityPlayerMP)this.summoningPlayer);
            }
        } else if (PixelmonMethods.isWearingShinyCharm((EntityPlayerMP)this.summoningPlayer)) {
            chance = PixelmonConfig.shinyCharmRate;
        }
        PlayerShinyChanceEvent event = new PlayerShinyChanceEvent((EntityPlayerMP)this.summoningPlayer, chance);
        MinecraftForge.EVENT_BUS.post(event);
        chance = event.isCanceled() ? 0.0f : event.getChance();
        spec.shiny = chance > 0.0f && RandomHelper.getRandomChance(1.0f / chance);
        spec.form = this.getActiveGroup().getForm() != null ? (int)this.getActiveGroup().getForm().getForm() : -1;
        return spec.create(world);
    }

    @Override
    public abstract int maxTick();

    protected void checkTick(int timeSpent) {
    }

    protected void finish() {
    }

    @Override
    public int getTick() {
        return this.timeSpent;
    }

    public void activateOther(EntityPlayer player, World world, BlockShrine blockShrine, IBlockState state, ItemStack heldItem) {
    }

    @Override
    public BlockPos getSpawnPos() {
        return this.pos;
    }

    public float getSpawnYaw() {
        return this.world.getBlockState(this.pos).getValue(BlockShrine.FACING).getHorizontalAngle();
    }

    @Override
    public PokemonGroup.PokemonData getActiveGroup() {
        return this.active;
    }

    public String getRightClickMessage(ItemStack heldStack, IBlockState state) {
        return "";
    }

    public boolean isSpawning() {
        return this.spawning;
    }
}

