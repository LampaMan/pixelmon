/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.api.events.CurryEvent;
import com.pixelmongenerations.common.capabilities.curry.CurryData;
import com.pixelmongenerations.common.item.CurryIngredient;
import com.pixelmongenerations.common.item.ICurryRarity;
import com.pixelmongenerations.common.item.ItemCurry;
import com.pixelmongenerations.common.item.heldItems.ItemBerry;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.enums.EnumBerryQuality;
import com.pixelmongenerations.core.enums.EnumCurryTasteRating;
import com.pixelmongenerations.core.enums.EnumCurryType;
import com.pixelmongenerations.core.enums.EnumFlavor;
import java.lang.invoke.LambdaMetafactory;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import javax.annotation.Nullable;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.Tuple;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;

public class TileEntityCookingPot
extends TileEntity
implements ITickable {
    private ItemStackHandler handler = new ItemStackHandler(14){

        @Override
        protected void onContentsChanged(int slot) {
            super.onContentsChanged(slot);
        }
    };
    private String customName;
    private boolean isCooking;
    private int cookTime;

    @Override
    public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY;
    }

    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
        if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            return (T)this.handler;
        }
        return super.getCapability(capability, facing);
    }

    public boolean hasCustomName() {
        return this.customName != null && !this.customName.isEmpty();
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    @Override
    public ITextComponent getDisplayName() {
        return this.hasCustomName() ? new TextComponentString(this.customName) : new TextComponentTranslation("container.cooking_pot", new Object[0]);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        this.isCooking = compound.getBoolean("isCooking");
        this.cookTime = compound.getInteger("cookTime");
        this.handler.deserializeNBT(compound.getCompoundTag("inventory"));
        if (compound.hasKey("customName", 8)) {
            this.setCustomName(compound.getString("customName"));
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        compound.setBoolean("isCooking", this.isCooking);
        compound.setInteger("cookTime", this.cookTime);
        compound.setTag("inventory", this.handler.serializeNBT());
        if (this.hasCustomName()) {
            compound.setString("customName", this.customName);
        }
        return compound;
    }

    @Override
    public void update() {
        if (!this.world.isRemote) {
            boolean hasEverything;
            ItemStack[] berries = (ItemStack[])IntStream.rangeClosed(0, 9).mapToObj(this.handler::getStackInSlot).toArray(ItemStack[]::new);
            ItemStack bowl = this.handler.getStackInSlot(10);
            ItemStack mainIngredient = this.handler.getStackInSlot(11);
            ItemStack log = this.handler.getStackInSlot(12);
            boolean bl = hasEverything = this.isCooking && Stream.of(berries).anyMatch(a -> !a.isEmpty()) && !bowl.isEmpty() && !log.isEmpty();
            if (hasEverything) {
                boolean hasInserted = false;
                ++this.cookTime;
                if (this.cookTime >= 200) {
                    boolean hasBerry = Stream.of(berries).anyMatch(a -> a.getItem() instanceof ItemBerry);
                    boolean hasMaxMushrooms = Stream.of(berries).anyMatch(a -> a.getItem() == PixelmonItems.maxMushrooms);
                    if (hasBerry && !hasMaxMushrooms) {
                        List<Tuple<EnumBerry, EnumBerryQuality>> pairs;
                        EnumCurryType type = !mainIngredient.isEmpty() && mainIngredient.getItem() instanceof CurryIngredient ? ((CurryIngredient)mainIngredient.getItem()).getType() : EnumCurryType.None;
                        CurryEvent.Cook event = new CurryEvent.Cook(type, pairs = Arrays.stream(berries).filter(berry -> !berry.isEmpty() && berry.getItem() instanceof ItemBerry).map(a -> new Tuple<EnumBerry, EnumBerryQuality>(((ItemBerry)a.getItem()).getBerry(), ItemBerry.getQuality(a))).collect(Collectors.toList()), this.createData(type, pairs));
                        if (!MinecraftForge.EVENT_BUS.post(event)) {
                            hasInserted = this.handler.insertItem(13, ItemCurry.createStack(event.getOutput()), false).isEmpty();
                        }
                    } else if (hasMaxMushrooms && !hasBerry && (mainIngredient.isEmpty() || mainIngredient.getItem() == PixelmonItems.maxHoney)) {
                        AtomicInteger count = new AtomicInteger();
                        Stream.of(berries).filter(a -> a.getItem() == PixelmonItems.maxMushrooms).forEach(b -> count.addAndGet(b.getCount()));
                        if (count.get() > 2) {
                            int amountTaken = 0;
                            for (ItemStack itemStack : berries) {
                                int amountLeft = 3 - amountTaken;
                                if (itemStack.getCount() > amountLeft) {
                                    itemStack.shrink(amountLeft);
                                    amountTaken = 3;
                                    break;
                                }
                                amountTaken += itemStack.getCount();
                                itemStack.setCount(0);
                            }
                            ItemStack maxSoupStack = new ItemStack(PixelmonItems.maxSoup);
                            if (!mainIngredient.isEmpty() && mainIngredient.getItem() == PixelmonItems.maxHoney) {
                                NBTTagCompound compound = new NBTTagCompound();
                                compound.setBoolean("MaxSoupHoney", true);
                                compound.setBoolean("GlowEffect", true);
                                maxSoupStack.setTagCompound(compound);
                                log.shrink(1);
                                mainIngredient.shrink(1);
                                bowl.shrink(1);
                                this.cookTime = 0;
                            }
                            this.handler.insertItem(13, maxSoupStack, false);
                        } else {
                            this.setCooking(false);
                        }
                    }
                }
                if (hasInserted) {
                    Arrays.stream(berries).forEach(berry -> berry.shrink(1));
                    log.shrink(1);
                    mainIngredient.shrink(1);
                    bowl.shrink(1);
                    this.cookTime = 0;
                }
            } else {
                this.setCooking(false);
            }
            this.sendUpdates();
        }
    }

    private CurryData createData(EnumCurryType mainIngredient, List<Tuple<EnumBerry, EnumBerryQuality>> berries) {
        EnumFlavor flavor = EnumBerry.getDominantFlavor(berries.stream().map(Tuple::getFirst).toArray(EnumBerry[]::new));
        int average = (int) berries.stream().map(t -> t.getSecond()).mapToInt(q -> q.getIndex()).average().orElse(0.0D);
        EnumCurryTasteRating rating = EnumCurryTasteRating.fromId(average);
        int friendship = Stream.concat(berries.stream(), Stream.of(mainIngredient)).filter(ICurryRarity.class::isInstance).map(ICurryRarity.class::cast).mapToInt(ICurryRarity::getRarity).sum();
        if (mainIngredient == EnumCurryType.Gigantamax) {
            flavor = EnumFlavor.None;
        }
        CurryData data = new CurryData();
        rating.configureData(data);
        data.setRating(rating);
        data.setCurryType(mainIngredient);
        data.setFlavor(flavor);
        data.setFriendship(friendship);
        return data;
    }

    public boolean isUsableByPlayer(EntityPlayer player) {
        return this.world.getTileEntity(this.pos) == this && player.getDistanceSq((double)this.pos.getX() + 0.5, (double)this.pos.getY() + 0.5, (double)this.pos.getZ() + 0.5) <= 64.0;
    }

    public boolean isCooking() {
        return this.isCooking;
    }

    public ItemStack getIngredient() {
        return this.handler.getStackInSlot(11);
    }

    public ItemStack getOuput() {
        return this.handler.getStackInSlot(13);
    }

    public ItemStack getBerry(int index) {
        return index >= 0 && index < 10 ? this.handler.getStackInSlot(index) : ItemStack.EMPTY;
    }

    public void setCooking(boolean isCooking) {
        this.isCooking = isCooking;
        this.cookTime = 0;
        this.sendUpdates();
    }

    @Override
    @Nullable
    public SPacketUpdateTileEntity getUpdatePacket() {
        NBTTagCompound compound = new NBTTagCompound();
        compound.setBoolean("isCooking", this.isCooking);
        compound.setInteger("cookTime", this.cookTime);
        return new SPacketUpdateTileEntity(this.getPos(), 0, compound);
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.isCooking = pkt.getNbtCompound().getBoolean("isCooking");
        this.cookTime = pkt.getNbtCompound().getInteger("cookTime");
    }

    public int getCookTime() {
        return this.cookTime;
    }

    private void sendUpdates() {
        this.world.markBlockRangeForRenderUpdate(this.pos, this.pos);
        this.world.notifyBlockUpdate(this.pos, this.getState(), this.getState(), 3);
        this.world.scheduleBlockUpdate(this.pos, this.getBlockType(), 0, 0);
        this.markDirty();
    }

    private IBlockState getState() {
        return this.world.getBlockState(this.pos);
    }

    public List<ItemStack> getDrops() {
        return IntStream.range(0, this.handler.getSlots()).mapToObj(i -> this.handler.getStackInSlot(i)).filter(stack -> !stack.isEmpty()).collect(Collectors.toList());
    }
}

