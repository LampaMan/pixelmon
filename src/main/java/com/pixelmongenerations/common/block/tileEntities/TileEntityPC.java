/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.WorldServer;

public class TileEntityPC
extends TileEntity
implements ISpecialTexture,
ITickable {
    private static Map<String, ResourceLocation> textures = new HashMap<String, ResourceLocation>();
    private ResourceLocation texture = new ResourceLocation("pixelmon:textures/blocks/computer/Texture_red.png");
    private String colour = "";
    private UUID owner = null;
    private boolean rave = false;
    private short raveTick;

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        compound.setString("colour", this.colour);
        compound.setBoolean("rave", this.rave);
        if (this.owner != null) {
            compound.setString("owner", this.owner.toString());
        }
        return compound;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        if (compound.hasKey("colour")) {
            this.setColour(compound.getString("colour"));
            this.setRave(compound.getBoolean("rave"));
        } else {
            this.setColour("red");
        }
        if (compound.hasKey("owner")) {
            this.owner = UUID.fromString(compound.getString("owner"));
        }
    }

    private void refreshTexture() {
        if (this.hasWorld() && !this.world.isRemote) {
            ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
            this.markDirty();
        }
    }

    public boolean getRave() {
        return this.rave;
    }

    public void setRave(boolean rave) {
        this.rave = rave;
        this.refreshTexture();
    }

    public String getColour() {
        return this.colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
        this.refreshTexture();
    }

    @Override
    public ResourceLocation getTexture() {
        if (this.colour.isEmpty() || !textures.containsKey(this.colour)) {
            return textures.get("red");
        }
        return textures.get(this.colour);
    }

    public UUID getOwnerUUID() {
        return this.owner;
    }

    public void setOwner(UUID owner) {
        this.owner = owner;
        this.markDirty();
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBT(nbt);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager networkManager, SPacketUpdateTileEntity packet) {
        this.colour = packet.getNbtCompound().getString("colour");
        this.rave = packet.getNbtCompound().getBoolean("rave");
        this.refreshTexture();
    }

    @Override
    public void update() {
        if (this.getRave() && this.raveTick % 20 == 0) {
            this.setColour(EnumDyeColor.values()[RandomHelper.rand.nextInt(EnumDyeColor.values().length)].getName());
        } else {
            this.raveTick = (short)(this.raveTick + 1);
        }
    }

    static {
        for (EnumDyeColor color : EnumDyeColor.values()) {
            textures.put(color.getName().toLowerCase(), new ResourceLocation("pixelmon:textures/blocks/computer/texture_" + color.getName() + ".png"));
        }
    }
}

