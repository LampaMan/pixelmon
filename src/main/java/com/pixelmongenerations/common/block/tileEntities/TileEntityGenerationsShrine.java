/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.reflect.TypeToken
 *  com.google.gson.Gson
 *  com.google.gson.GsonBuilder
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.ranch.JsonLoader;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumForms;
import com.pixelmongenerations.core.util.Adapters;
import java.io.FileNotFoundException;
import java.util.function.Supplier;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class TileEntityGenerationsShrine
extends TileEntityShrine {
    public static Gson gsonInstance = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(PokemonGroup.PokemonData.class, (Object)new Adapters.PokemonDataAdapter()).create();
    private static PokemonGroup group;

    public static PokemonGroup getPokemonGroup() {
        return group;
    }

    @Override
    public void activate(EntityPlayer player, World world, BlockShrine block, IBlockState state, ItemStack item) {
        if (this.isSpawning()) {
            return;
        }
        item.shrink(1);
        this.startSpawning(player, state, world, this.pos);
    }

    @Override
    protected PokemonGroup.PokemonData selectActiveGroup() {
        return group.random();
    }

    @Override
    public int maxTick() {
        return 100;
    }

    @Override
    public String getRightClickMessage(ItemStack heldStack, IBlockState state) {
        return "pixelmon.generations_shrine.right_click";
    }

    public static void setup() throws FileNotFoundException {
        Supplier<PokemonGroup> supplier = () -> new PokemonGroup(PokemonGroup.PokemonData.of(EnumSpecies.Mew), PokemonGroup.PokemonData.of(EnumSpecies.Hooh), PokemonGroup.PokemonData.of(EnumSpecies.Latias), PokemonGroup.PokemonData.of(EnumSpecies.Latios), PokemonGroup.PokemonData.of(EnumSpecies.Moltres), PokemonGroup.PokemonData.of(EnumSpecies.Moltres, EnumForms.Galarian), PokemonGroup.PokemonData.of(EnumSpecies.Zapdos), PokemonGroup.PokemonData.of(EnumSpecies.Zapdos, EnumForms.Galarian), PokemonGroup.PokemonData.of(EnumSpecies.Articuno), PokemonGroup.PokemonData.of(EnumSpecies.Articuno, EnumForms.Galarian), PokemonGroup.PokemonData.of(EnumSpecies.Rayquaza), PokemonGroup.PokemonData.of(EnumSpecies.Jirachi), PokemonGroup.PokemonData.of(EnumSpecies.Deoxys), PokemonGroup.PokemonData.of(EnumSpecies.Uxie), PokemonGroup.PokemonData.of(EnumSpecies.Mesprit), PokemonGroup.PokemonData.of(EnumSpecies.Azelf), PokemonGroup.PokemonData.of(EnumSpecies.Heatran), PokemonGroup.PokemonData.of(EnumSpecies.Regigigas));
        group = JsonLoader.setup("generations_group", TypeToken.of(PokemonGroup.class), supplier, gsonInstance, PixelmonConfig.useExternalJSONFilesGenerationsGroup);
    }
}

