/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.entity.pixelmon.EntityStatue;
import com.pixelmongenerations.common.item.ItemFossil;
import com.pixelmongenerations.common.item.ItemPokeball;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import net.minecraft.item.Item;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ITickable;
import net.minecraft.world.WorldServer;

public class TileEntityFossilMachine
extends TileEntity
implements ITickable {
    public static final int MAX_POKEMON_PROGRESS = 3200;
    public static final int MAX_FOSSIL_PROGRESS = 1600;
    public int renderPass = 0;
    public EntityStatue statue = null;
    public float fossilJitter;
    public int screenFlickerTick;
    public boolean staticFlicker = false;
    public int dotTicks = 0;
    public String dots = "";
    public boolean pokemonOccupied = false;
    public ItemFossil currentFossil = null;
    public float fossilProgress = 0.0f;
    public String currentPokemon = "";
    public float pokemonProgress = 0.0f;
    public int completionRate = (int)((this.fossilProgress + this.pokemonProgress) * 2.0f / 96.0f);
    public int completionSync = 0;
    public boolean isShiny = false;
    public EnumPokeball pokeball = null;

    @Override
    public void update() {
        if (this.completionRate != 100 && (this.fossilProgress > 0.0f || this.pokemonProgress > 0.0f)) {
            this.completionRate = (int)((this.fossilProgress + this.pokemonProgress) * 2.0f / 96.0f);
            if (!this.getWorld().isRemote && this.completionRate % 10 == 0 && this.completionSync != this.completionRate) {
                ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
                this.completionSync = this.completionRate;
            }
        }
        if (this.currentFossil != null) {
            if (this.fossilProgress < 1600.0f) {
                this.fossilProgress += 1.0f;
            } else if (!this.getWorld().isRemote) {
                this.swapFossilForPokemon();
                ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
            }
        }
        if (this.pokemonOccupied && this.pokemonProgress < 3200.0f) {
            this.pokemonProgress += 1.0f;
        }
        if (++this.dotTicks > 10) {
            this.dotTicks = 0;
            this.dots = this.dots.length() < 6 ? this.dots + "." : "";
        }
        this.fossilJitter = this.fossilJitter == 0.0f ? 0.01f : 0.0f;
        float f = this.fossilJitter;
        int n = this.screenFlickerTick = this.staticFlicker ? (this.screenFlickerTick = this.screenFlickerTick - 1) : (this.screenFlickerTick = this.screenFlickerTick + 1);
        if (this.screenFlickerTick >= 8) {
            this.staticFlicker = true;
        } else if (this.screenFlickerTick <= 0) {
            this.staticFlicker = false;
        }
        if (this.completionRate < 98) {
            int num = this.completionRate <= 50 ? this.completionRate / 4 : (100 - this.completionRate) / 4;
            double var9 = (float)this.getPos().getY() + 0.4f;
            for (int i = 0; i < num; ++i) {
                this.getWorld().spawnParticle(EnumParticleTypes.REDSTONE, (double)this.getPos().getX() + 0.1 + (double)this.getWorld().rand.nextFloat() * 0.7, var9 + (double)this.getWorld().rand.nextFloat() + (double)(this.getWorld().rand.nextFloat() * 0.4f), (double)this.getPos().getZ() + 0.1 + (double)this.getWorld().rand.nextFloat() * 0.7, -1.0, 1.2, 1.0, new int[0]);
            }
        }
    }

    public void swapFossilForPokemon() {
        this.currentPokemon = this.currentFossil.getFossil().getPokemon().name;
        this.pokemonOccupied = true;
        this.isShiny = PixelmonConfig.shinyRate > 0.0f && RandomHelper.rand.nextFloat() < 1.0f / PixelmonConfig.shinyRate;
        this.currentFossil = null;
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        if (this.pokeball != null) {
            nbt.setInteger("PokeballType", this.pokeball.getIndex());
        }
        nbt.setInteger("CurrentFossil", Item.getIdFromItem(this.currentFossil));
        nbt.setFloat("PokemonProgress", this.pokemonProgress);
        nbt.setFloat("FossilProgress", this.fossilProgress);
        nbt.setString("CurrentPokemon", this.currentPokemon);
        nbt.setBoolean("IsShiny", this.isShiny);
        return nbt;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        if (nbt.hasKey("CurrentPokeball", 3)) {
            if (Item.getItemById(nbt.getInteger("CurrentPokeball")) instanceof ItemPokeball) {
                ItemPokeball itemPokeball = (ItemPokeball)Item.getItemById(nbt.getInteger("CurrentPokeball"));
                this.pokeball = itemPokeball.type;
            }
        } else {
            this.pokeball = nbt.hasKey("PokeballType", 3) ? EnumPokeball.getFromIndex(nbt.getInteger("PokeballType")) : null;
        }
        this.currentFossil = Item.getItemById(nbt.getInteger("CurrentFossil")) instanceof ItemFossil ? (ItemFossil)Item.getItemById(nbt.getInteger("CurrentFossil")) : null;
        this.fossilProgress = nbt.getFloat("FossilProgress");
        this.pokemonProgress = nbt.getFloat("PokemonProgress");
        this.currentPokemon = nbt.getString("CurrentPokemon");
        this.pokemonOccupied = !this.currentPokemon.equals("");
        this.completionRate = (int)((this.fossilProgress + this.pokemonProgress) * 2.0f / 96.0f);
        if (nbt.hasKey("IsShiny")) {
            this.isShiny = nbt.getBoolean("IsShiny");
        }
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBT(nbt);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBT(pkt.getNbtCompound());
    }

    @Override
    public boolean shouldRenderInPass(int pass) {
        this.renderPass = pass;
        return true;
    }
}

