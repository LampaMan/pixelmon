/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.enums.ColorEnum;
import com.pixelmongenerations.common.block.multiBlocks.BlockWaterFloat;
import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class TileEntityWaterFloat
extends TileEntity
implements ISpecialTexture {
    private static ResourceLocation[] textures = new ResourceLocation[ColorEnum.values().length];

    @Override
    public ResourceLocation getTexture() {
        IBlockState state = this.getWorld().getBlockState(this.getPos());
        if (state.getBlock() instanceof BlockWaterFloat) {
            BlockWaterFloat block = (BlockWaterFloat)state.getBlock();
            return textures[block.getColor().ordinal()];
        }
        return textures[0];
    }

    static {
        for (ColorEnum color : ColorEnum.values()) {
            TileEntityWaterFloat.textures[color.ordinal()] = new ResourceLocation("pixelmon:textures/blocks/WaterFloat/" + (Object)((Object)color) + "WaterFloatModel.png");
        }
    }
}

