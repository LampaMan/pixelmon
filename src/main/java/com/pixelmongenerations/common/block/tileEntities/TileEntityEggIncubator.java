/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.pokedex.EnumPokedexRegisterStatus;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class TileEntityEggIncubator
extends TileEntity
implements ITickable {
    public NBTTagCompound egg = null;
    public int maxEggCycles = 0;
    public int eggCycles = 0;
    public int steps = 0;
    public int timer = 0;
    private UUID ownerUUID = null;
    private String playerName = "";
    public int renderPass = 0;
    public float angle = 0.0f;
    public float percent = 0.0f;
    public String eggName = "";

    public boolean insertEgg(NBTTagCompound input, EntityPlayerMP player) {
        if (this.egg == null && input != null) {
            this.egg = input.copy();
            this.eggCycles = this.egg.getInteger("eggCycles");
            this.steps = this.egg.getInteger("steps");
            this.maxEggCycles = Entity3HasStats.getBaseStats(this.egg.getString("Name"), this.egg.getInteger("Variant")).map(a -> a.eggCycles).orElse(1);
            this.timer = 0;
            this.eggName = this.getEggName();
            this.percent = 1.0f - (float)((this.eggCycles + 1) * PixelmonConfig.stepsPerEggCycle * PixelmonConfig.ticksPerStep - this.steps * PixelmonConfig.ticksPerStep - this.timer) / (float)((this.maxEggCycles + 1) * PixelmonConfig.stepsPerEggCycle * PixelmonConfig.ticksPerStep);
            this.angle = (float)(this.steps * PixelmonConfig.ticksPerStep + this.timer) / (float)((PixelmonConfig.stepsPerEggCycle + 1) * PixelmonConfig.ticksPerStep) * 360.0f;
            this.ownerUUID = player.getUniqueID();
            this.playerName = player.getDisplayNameString();
            return true;
        }
        return false;
    }

    private String getEggName() {
        if (!this.isOn()) {
            return "";
        }
        EnumSpecies fromNameAnyCase = EnumSpecies.getFromNameAnyCase(this.egg.getString("Name"));
        if (fromNameAnyCase == null) {
            return "regular";
        }
        return fromNameAnyCase.name.toLowerCase();
    }

    public Optional<NBTTagCompound> extractEgg() {
        if (this.egg != null) {
            NBTTagCompound temp = this.egg.copy();
            temp.setInteger("eggCycles", this.eggCycles);
            temp.setInteger("steps", this.steps);
            this.maxEggCycles = 0;
            this.timer = 0;
            this.egg = null;
            this.eggName = "";
            this.angle = 0.0f;
            this.percent = 0.0f;
            this.ownerUUID = null;
            this.playerName = "";
            this.updateBlock();
            return Optional.of(temp);
        }
        return Optional.empty();
    }

    public boolean containsEgg() {
        return this.egg != null;
    }

    @Override
    public void update() {
        if (!(this.world.isRemote || this.egg == null || this.eggCycles <= 0 && this.steps > PixelmonConfig.stepsPerEggCycle)) {
            ++this.timer;
            if (this.timer == PixelmonConfig.ticksPerStep) {
                this.timer = 0;
                ++this.steps;
                if (this.steps == PixelmonConfig.stepsPerEggCycle && this.eggCycles > 0) {
                    --this.eggCycles;
                    this.steps = 0;
                }
            }
            this.percent = 1.0f - MathHelper.clamp((float)((this.eggCycles + 1) * PixelmonConfig.stepsPerEggCycle * PixelmonConfig.ticksPerStep - this.steps * PixelmonConfig.ticksPerStep - this.timer) / (float)((this.maxEggCycles + 1) * PixelmonConfig.stepsPerEggCycle * PixelmonConfig.ticksPerStep), 0.0f, 1.0f);
            this.angle = (float)(this.steps * PixelmonConfig.ticksPerStep + this.timer) / (float)((PixelmonConfig.stepsPerEggCycle + 1) * PixelmonConfig.ticksPerStep) * 360.0f;
            this.updateBlock();
        }
    }

    private void updateBlock() {
        this.getWorld().notifyNeighborsOfStateChange(this.pos, this.getBlockType(), true);
        ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        this.egg = nbt.hasKey("egg") ? nbt.getCompoundTag("egg") : null;
        this.maxEggCycles = nbt.getInteger("maxEggCycles");
        this.eggCycles = nbt.getInteger("eggCycles");
        this.steps = nbt.getInteger("steps");
        this.timer = nbt.getInteger("timer");
        this.angle = nbt.getFloat("angle");
        this.percent = nbt.getFloat("percent");
        this.eggName = nbt.getString("eggName");
        if (nbt.hasKey("playerName")) {
            this.playerName = nbt.getString("playerName");
        }
        this.ownerUUID = nbt.hasKey("UUIDMost", 4) && nbt.hasKey("UUIDLeast", 4) ? new UUID(nbt.getLong("UUIDMost"), nbt.getLong("UUIDLeast")) : null;
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        if (this.egg != null) {
            nbt.setTag("egg", this.egg);
        }
        nbt.setInteger("maxEggCycles", this.maxEggCycles);
        nbt.setInteger("eggCycles", this.eggCycles);
        nbt.setInteger("steps", this.steps);
        nbt.setInteger("timer", this.timer);
        nbt.setFloat("angle", this.angle);
        nbt.setFloat("percent", this.percent);
        nbt.setString("eggName", this.eggName);
        if (this.ownerUUID != null) {
            nbt.setLong("UUIDMost", this.ownerUUID.getMostSignificantBits());
            nbt.setLong("UUIDLeast", this.ownerUUID.getLeastSignificantBits());
        }
        if (!this.playerName.equals("")) {
            nbt.setString("playerName", this.playerName);
        }
        return nbt;
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = super.getUpdateTag();
        nbt.setFloat("angle", this.angle);
        nbt.setFloat("percent", this.percent);
        nbt.setString("eggName", this.eggName);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.angle = pkt.getNbtCompound().getFloat("angle");
        this.percent = pkt.getNbtCompound().getFloat("percent");
        this.eggName = pkt.getNbtCompound().getString("eggName");
    }

    @Override
    public boolean shouldRenderInPass(int pass) {
        this.renderPass = pass;
        return true;
    }

    public void setOwner(EntityPlayerMP entity) {
        this.ownerUUID = entity.getUniqueID();
        this.playerName = entity.getDisplayNameString();
    }

    public UUID getOwnerUUID() {
        return this.ownerUUID;
    }

    public void onDestroy() {
        if (!this.world.isRemote && this.egg != null) {
            try {
                this.egg.setInteger("eggCycles", this.eggCycles);
                this.egg.setInteger("steps", this.steps);
                EntityPlayerMP player = PixelmonStorage.pokeBallManager.getPlayerFromUUID(FMLCommonHandler.instance().getMinecraftServerInstance(), this.ownerUUID);
                if (player != null) {
                    PlayerStorage playerStorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player).get();
                    if (this.steps >= PixelmonConfig.stepsPerEggCycle && this.eggCycles <= 0) {
                        playerStorage.hatchEgg(player, this.egg);
                    }
                    playerStorage.addToParty((EntityPixelmon)PixelmonEntityList.createEntityFromNBT(this.egg, this.world));
                    PixelmonStorage.pokeBallManager.savePlayer(FMLCommonHandler.instance().getMinecraftServerInstance(), playerStorage);
                } else {
                    PlayerComputerStorage pc = Optional.ofNullable(PixelmonStorage.computerManager.getPlayerStorageOffline(FMLCommonHandler.instance().getMinecraftServerInstance(), this.ownerUUID)).orElseGet(() -> new PlayerComputerStorage(FMLCommonHandler.instance().getMinecraftServerInstance(), this.ownerUUID));
                    this.egg.setString("originalTrainer", this.playerName);
                    this.egg.setString("originalTrainerUUID", this.ownerUUID.toString());
                    this.egg.setBoolean("isEgg", false);
                    EnumSpecies species = EnumSpecies.getFromNameAnyCase(this.egg.getString("Name"));
                    pc.playerStorage.pokedex.set(species, EnumPokedexRegisterStatus.caught);
                    pc.addToComputer((EntityPixelmon)PixelmonEntityList.createEntityFromNBT(this.egg, this.world));
                    PixelmonStorage.computerManager.savePlayer(pc);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isOn() {
        return this.egg != null;
    }
}

