/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

public interface IFrameCounter {
    public int getFrame();

    public void setFrame(int var1);
}

