/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.core.util.Bounds;

public interface IPokemonOwner {
    public Bounds getBounds();

    public void updateStatus();

    public int getEntityCount();
}

