/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.core.config.PixelmonItems;
import java.util.Random;
import javax.annotation.Nullable;
import net.minecraft.block.BlockDoor;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class BlockHiddenIronDoor
extends BlockDoor {
    public BlockHiddenIronDoor() {
        super(Material.IRON);
        this.setHardness(2.0f);
        this.setBlockUnbreakable();
        this.setTranslationKey("hidden_iron_door");
    }

    @Override
    @Nullable
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return PixelmonItems.hiddenIronDoorItem;
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        return new ItemStack(PixelmonItems.hiddenIronDoorItem);
    }
}

