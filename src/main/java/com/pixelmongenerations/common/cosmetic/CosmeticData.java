/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.Gson
 */
package com.pixelmongenerations.common.cosmetic;

import com.google.gson.Gson;
import com.pixelmongenerations.common.cosmetic.CosmeticCategory;
import com.pixelmongenerations.common.cosmetic.CosmeticEntry;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.UpdateCosmeticData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class CosmeticData {
    private UUID playerId;
    private ArrayList<CosmeticEntry> wardrobe;
    private HashMap<CosmeticCategory, Integer> wearing;

    public CosmeticData(UUID playerId) {
        this.playerId = playerId;
        this.wardrobe = new ArrayList();
        this.wearing = new HashMap();
    }

    public void writeToNBT(NBTTagCompound nbt) {
        NBTTagCompound cosmeticTag = new NBTTagCompound();
        Gson gson = new Gson();
        if (this.wardrobe != null && !this.wardrobe.isEmpty()) {
            cosmeticTag.setInteger("CosmeticAmount", this.wardrobe.size());
            for (int i = 0; i < this.wardrobe.size(); ++i) {
                CosmeticEntry cosmeticEntry = this.wardrobe.get(i);
                cosmeticTag.setString("CosmeticEntry" + i, gson.toJson((Object)cosmeticEntry));
            }
        }
        if (this.wearing != null && !this.wearing.isEmpty()) {
            for (Map.Entry<CosmeticCategory, Integer> entry : this.wearing.entrySet()) {
                cosmeticTag.setInteger("Worn" + entry.getKey().name(), entry.getValue());
            }
        }
        nbt.setTag("Cosmetics", cosmeticTag);
    }

    public void readFromNBT(NBTTagCompound nbt) {
        this.wardrobe = new ArrayList();
        this.wearing = new HashMap();
        NBTTagCompound cosmeticTag = nbt.getCompoundTag("Cosmetics");
        Gson gson = new Gson();
        if (cosmeticTag.hasKey("CosmeticAmount")) {
            int cosmeticAmount = cosmeticTag.getInteger("CosmeticAmount");
            for (int i = 0; i < cosmeticAmount; ++i) {
                String json = cosmeticTag.getString("CosmeticEntry" + i);
                if (json.isEmpty()) continue;
                this.wardrobe.add((CosmeticEntry)gson.fromJson(json, CosmeticEntry.class));
            }
        }
        for (CosmeticCategory category : CosmeticCategory.values()) {
            if (!cosmeticTag.hasKey("Worn" + category.name())) continue;
            int index = cosmeticTag.getInteger("Worn" + category.name());
            this.wearing.put(category, index);
        }
    }

    public CosmeticEntry getCosmeticInSlot(CosmeticCategory category) {
        if (this.wearing == null) {
            return null;
        }
        Integer index = this.wearing.get((Object)category);
        if (index == null) {
            return null;
        }
        return this.wardrobe.size() > index && index > -1 ? this.wardrobe.get(index) : null;
    }

    public boolean addCosmetic(CosmeticEntry entry) {
        if (!this.wardrobe.contains(entry)) {
            boolean result = this.wardrobe.add(entry);
            if (result) {
                this.sendUpdateToOwner();
            }
            return result;
        }
        return false;
    }

    public void removeCosmetic(CosmeticCategory category) {
        this.wearing.remove((Object)category);
        Pixelmon.NETWORK.sendToAll(new UpdateCosmeticData(this));
    }

    public void setCosmetic(CosmeticEntry entry) {
        if (entry == null) {
            return;
        }
        if (this.wardrobe == null) {
            this.wardrobe = new ArrayList();
        }
        if (this.wearing == null) {
            this.wearing = new HashMap();
        }
        int size = this.wardrobe.size();
        if (this.wardrobe.contains(entry)) {
            for (int i = 0; i < size; ++i) {
                if (!this.wardrobe.get(i).equals(entry)) continue;
                this.wearing.put(entry.getCategory(), i);
                Pixelmon.NETWORK.sendToAll(new UpdateCosmeticData(this));
                break;
            }
        } else {
            this.wardrobe.add(entry);
            this.wearing.put(entry.getCategory(), size);
            Pixelmon.NETWORK.sendToAll(new UpdateCosmeticData(this));
        }
    }

    public void setCosmetic(int hashCode) {
        if (this.wardrobe == null) {
            return;
        }
        if (this.wearing == null) {
            return;
        }
        for (CosmeticCategory category : this.wearing.keySet()) {
            if (this.getCosmeticInSlot(category).hashCode() != hashCode) continue;
            this.removeCosmetic(category);
            return;
        }
        for (CosmeticEntry cosmetic : this.wardrobe) {
            if (cosmetic.hashCode() != hashCode) continue;
            this.setCosmetic(cosmetic);
            return;
        }
        Pixelmon.LOGGER.info("Invalid cosmetic hash code was sent from: " + this.playerId);
    }

    public List<CosmeticEntry> getCosmeticsFromCategory(CosmeticCategory category) {
        if (this.wardrobe == null) {
            return new ArrayList<CosmeticEntry>();
        }
        return this.wardrobe.stream().filter(cosmetic -> cosmetic.getCategory() == category).collect(Collectors.toList());
    }

    public UUID getEntityId() {
        return this.playerId;
    }

    public void sendUpdateToOwner() {
        EntityPlayerMP player = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUUID(this.playerId);
        if (player != null) {
            Pixelmon.NETWORK.sendTo(new UpdateCosmeticData(this), player);
        } else {
            Pixelmon.LOGGER.error("Went to send self update but owner player was not found with uuid: " + this.playerId);
        }
    }

    public ArrayList<CosmeticEntry> getWardrobe() {
        return this.wardrobe;
    }

    public void setWardrobe(ArrayList<CosmeticEntry> wardrobe) {
        this.wardrobe = wardrobe;
    }

    public HashMap<CosmeticCategory, Integer> getWearing() {
        return this.wearing;
    }

    public void setWearing(HashMap<CosmeticCategory, Integer> wearing) {
        this.wearing = wearing;
    }
}

