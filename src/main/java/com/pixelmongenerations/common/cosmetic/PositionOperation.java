/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.cosmetic;

import com.pixelmongenerations.common.cosmetic.PositionOperationType;
import java.util.Objects;

public class PositionOperation {
    private PositionOperationType type;
    private Float angle;
    private float x;
    private float y;
    private float z;

    public static PositionOperation scale(float x, float y, float z) {
        PositionOperation operation = new PositionOperation();
        operation.type = PositionOperationType.Scale;
        operation.x = x;
        operation.y = y;
        operation.z = z;
        return operation;
    }

    public static PositionOperation translate(float x, float y, float z) {
        PositionOperation operation = new PositionOperation();
        operation.type = PositionOperationType.Translate;
        operation.x = x;
        operation.y = y;
        operation.z = z;
        return operation;
    }

    public static PositionOperation rotate(float angle, float x, float y, float z) {
        PositionOperation operation = new PositionOperation();
        operation.type = PositionOperationType.Rotate;
        operation.angle = Float.valueOf(angle);
        operation.x = x;
        operation.y = y;
        operation.z = z;
        return operation;
    }

    public PositionOperationType getType() {
        return this.type;
    }

    public int hashCode() {
        return Objects.hash(new Object[]{this.angle, this.type, Float.valueOf(this.x), Float.valueOf(this.y), Float.valueOf(this.z)});
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        PositionOperation other = (PositionOperation)obj;
        return Objects.equals(this.angle, other.angle) && this.type == other.type && Float.floatToIntBits(this.x) == Float.floatToIntBits(other.x) && Float.floatToIntBits(this.y) == Float.floatToIntBits(other.y) && Float.floatToIntBits(this.z) == Float.floatToIntBits(other.z);
    }

    public float getAngle() {
        return this.angle.floatValue();
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }

    public float getZ() {
        return this.z;
    }
}

