/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.cosmetic;

import com.pixelmongenerations.client.assets.resource.MinecraftModelResource;
import com.pixelmongenerations.client.assets.resource.ValveModelResource;
import com.pixelmongenerations.common.cosmetic.CosmeticCategory;
import com.pixelmongenerations.common.cosmetic.EnumCosmetic;
import com.pixelmongenerations.common.cosmetic.PositionOperation;
import com.pixelmongenerations.core.data.asset.entry.MinecraftModelAsset;
import com.pixelmongenerations.core.data.asset.entry.ValveModelAsset;
import com.pixelmongenerations.core.enums.EnumCustomModel;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.util.Objects;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class CosmeticEntry {
    private String name;
    private String modelName;
    private String textureName;
    private CosmeticCategory category;
    private boolean modCosmetic = false;
    private transient Object model = null;
    private transient EnumCustomModel enumModel = null;

    public CosmeticEntry(CosmeticCategory category, String name, String modelName, String textureName) {
        this.category = category;
        this.name = name;
        this.modelName = modelName;
        this.textureName = textureName;
    }

    public static CosmeticEntry of(EnumCosmetic cosmetic) {
        return new CosmeticEntry(cosmetic.getCategory(), cosmetic.getName(), cosmetic.getModelName(), cosmetic.getTextureName());
    }

    public String getName() {
        return this.name;
    }

    public String getModelName() {
        return this.modelName;
    }

    public String getTextureName() {
        return this.textureName;
    }

    public CosmeticCategory getCategory() {
        return this.category;
    }

    public boolean isModCosmetic() {
        return this.modCosmetic;
    }

    public PositionOperation[] getOperations() {
        if (this.model == null && this.enumModel == null) {
            this.getModel();
        }
        if (this.enumModel != null) {
            return this.enumModel.getOperaitons();
        }
        MinecraftModelResource minecraftModelResource = ClientProxy.MINECRAFT_MODEL_STORE.getObject(this.modelName);
        if (minecraftModelResource != null) {
            return ((MinecraftModelAsset)minecraftModelResource.entry).operations;
        }
        ValveModelResource valveModelResource = ClientProxy.VALVE_MODEL_STORE.getObject(this.modelName);
        if (valveModelResource != null) {
            return ((ValveModelAsset)valveModelResource.entry).operations;
        }
        return null;
    }

    public int hashCode() {
        return Objects.hash(this.category.ordinal(), this.modelName, this.name, this.textureName, this.modCosmetic);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        CosmeticEntry other = (CosmeticEntry)obj;
        return this.modCosmetic == other.modCosmetic && this.category == other.category && Objects.equals(this.modelName, other.modelName) && Objects.equals(this.name, other.name) && Objects.equals(this.textureName, other.textureName);
    }

    @SideOnly(value=Side.CLIENT)
    public Object getModel() {
        if (this.model != null) {
            return this.model;
        }
        MinecraftModelResource minecraftModelResource = ClientProxy.MINECRAFT_MODEL_STORE.getObject(this.modelName);
        if (minecraftModelResource != null) {
            this.model = minecraftModelResource;
            return this.model;
        }
        ValveModelResource valveModelResource = ClientProxy.VALVE_MODEL_STORE.getObject(this.modelName);
        if (valveModelResource != null) {
            this.model = valveModelResource.getModel();
            return this.model;
        }
        EnumCustomModel customModel = EnumCustomModel.getFromString(this.modelName);
        if (customModel != null) {
            this.enumModel = customModel;
            this.model = customModel.getModel();
            return this.model;
        }
        return null;
    }
}

