/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.capabilities;

import javax.annotation.Nullable;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;

public class CapabilityProviderSimple<HANDLER>
implements ICapabilityProvider {
    protected final Capability<HANDLER> capability;
    protected final EnumFacing facing;
    protected final HANDLER instance;

    public CapabilityProviderSimple(HANDLER instance, Capability<HANDLER> capability, @Nullable EnumFacing facing) {
        this.instance = instance;
        this.capability = capability;
        this.facing = facing;
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == this.getCapability();
    }

    @Override
    @Nullable
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if (capability == this.getCapability()) {
            return (T)this.getCapability().cast(this.getInstance());
        }
        return null;
    }

    public final Capability<HANDLER> getCapability() {
        return this.capability;
    }

    @Nullable
    public EnumFacing getFacing() {
        return this.facing;
    }

    public final HANDLER getInstance() {
        return this.instance;
    }
}

