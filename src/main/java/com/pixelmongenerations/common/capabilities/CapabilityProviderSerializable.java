/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.capabilities;

import com.pixelmongenerations.common.capabilities.CapabilityProviderSimple;
import javax.annotation.Nullable;
import net.minecraft.nbt.NBTBase;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.INBTSerializable;

public class CapabilityProviderSerializable<HANDLER>
extends CapabilityProviderSimple<HANDLER>
implements INBTSerializable<NBTBase> {
    public CapabilityProviderSerializable(Capability<HANDLER> capability, @Nullable EnumFacing facing) {
        this(capability, facing, capability.getDefaultInstance());
    }

    public CapabilityProviderSerializable(Capability<HANDLER> capability, @Nullable EnumFacing facing, HANDLER instance) {
        super(instance, capability, facing);
    }

    @Override
    public NBTBase serializeNBT() {
        return this.getCapability().writeNBT(this.getInstance(), this.getFacing());
    }

    @Override
    public void deserializeNBT(NBTBase nbt) {
        this.getCapability().readNBT(this.getInstance(), this.getFacing(), nbt);
    }
}

