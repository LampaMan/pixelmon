/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.items.EnumPokeball;

public class ItemPokeballDisc
extends PixelmonItem {
    public EnumPokeball pokeball;

    public ItemPokeballDisc(EnumPokeball type) {
        super(type.getFilenamePrefix() + "_disc");
        this.pokeball = type;
        this.maxStackSize = 64;
        this.setMaxDamage(1000000);
        this.setCreativeTab(PixelmonCreativeTabs.pokeball);
        this.canRepair = false;
    }
}

