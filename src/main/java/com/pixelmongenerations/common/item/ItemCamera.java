/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.client.gui.overlay.IOverlay;
import com.pixelmongenerations.client.gui.overlay.OverlayType;
import com.pixelmongenerations.client.keybindings.TargetKeyBinding;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.camera.ItemCameraPacket;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemCamera
extends PixelmonItem {
    public ItemCamera() {
        super("camera");
        this.setMaxStackSize(1);
        this.setCreativeTab(CreativeTabs.MISC);
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public boolean onEntitySwing(EntityLivingBase entityLiving, ItemStack stack) {
        if (entityLiving.world.isRemote) {
            IOverlay overlay = GuiPixelmonOverlay.getOverlay(OverlayType.CAMERA);
            if (!overlay.isActive()) {
                return false;
            }
            RayTraceResult objectPosition = TargetKeyBinding.getTarget(false, 20.0);
            if (objectPosition != null && objectPosition.typeOfHit == RayTraceResult.Type.ENTITY) {
                if (!(objectPosition.entityHit instanceof EntityPixelmon)) {
                    return false;
                }
                EntityPixelmon pixelmon = (EntityPixelmon)objectPosition.entityHit;
                Pixelmon.NETWORK.sendToServer(new ItemCameraPacket(pixelmon.getEntityId()));
                overlay.setActive(false);
            }
        }
        return false;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand hand) {
        if (worldIn.isRemote) {
            IOverlay overlay = GuiPixelmonOverlay.getOverlay(OverlayType.CAMERA);
            overlay.setActive(!overlay.isActive());
        }
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, playerIn.getHeldItem(hand));
    }
}

