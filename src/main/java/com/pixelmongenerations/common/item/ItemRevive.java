/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.client.gui.overlay.OverlayType;
import com.pixelmongenerations.client.gui.overlay.PartyOverlay;
import com.pixelmongenerations.common.item.IMedicine;
import com.pixelmongenerations.common.item.ItemMedicine;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.UseRevive;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemRevive
extends ItemMedicine {
    public ItemRevive(String itemName, IMedicine ... healMethods) {
        super(itemName, healMethods);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer playerIn, EnumHand hand) {
        PixelmonData pokemon;
        if (world.isRemote && (pokemon = ((PartyOverlay)GuiPixelmonOverlay.getOverlay(OverlayType.PARTY)).getSelectedPokemon()) != null) {
            Pixelmon.NETWORK.sendToServer(new UseRevive(pokemon.pokemonID));
        }
        return super.onItemRightClick(world, playerIn, hand);
    }
}

