/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.items.EnumApricorns;

public class ItemApricornCooked
extends PixelmonItem {
    public EnumApricorns apricorn;

    public ItemApricornCooked(EnumApricorns apricorn) {
        super("cooked_" + apricorn.toString().toLowerCase() + "_apricorn");
        this.apricorn = apricorn;
        this.setMaxStackSize(64);
        this.setMaxDamage(1000000);
        this.setCreativeTab(PixelmonCreativeTabs.natural);
        this.canRepair = false;
    }
}

