/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemRotomCatalog
extends Item {
    public static int full = 200;

    public ItemRotomCatalog() {
        this.setCreativeTab(CreativeTabs.MISC);
        this.setMaxStackSize(1);
        this.setMaxDamage(full);
        this.setDamage(new ItemStack(this), 0);
        this.setTranslationKey("rotom_catalog");
        this.setRegistryName("rotom_catalog");
    }
}

