/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.EnumNature;
import java.util.HashMap;
import net.minecraft.item.Item;

public class ItemPokeMint
extends PixelmonItem {
    private EnumNature nature;
    public static final HashMap<EnumNature, Item> mints = new HashMap();

    public ItemPokeMint(EnumNature type) {
        super(type.name().toLowerCase() + "_mint");
        this.nature = type;
        this.maxStackSize = 64;
        this.setCreativeTab(PixelmonCreativeTabs.restoration);
        mints.put(type, this);
    }

    public EnumNature getMintType() {
        return this.nature;
    }
}

