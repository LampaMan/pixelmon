/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.EnumIncreaseEV;
import com.pixelmongenerations.core.network.EnumUpdateType;

public class IncreaseEV
extends PixelmonItem {
    public EnumIncreaseEV type;

    public IncreaseEV(EnumIncreaseEV type, String itemName) {
        super(itemName);
        this.type = type;
        this.setMaxStackSize(16);
        this.setCreativeTab(PixelmonCreativeTabs.restoration);
        this.canRepair = false;
    }

    public boolean vitaminEVs(EntityPixelmon entityPixelmon) {
        boolean success = entityPixelmon.stats.EVs.vitaminEVs(this.type.statAffected);
        if (success) {
            entityPixelmon.friendship.vitaminFriendship();
            entityPixelmon.updateStats();
            entityPixelmon.update(EnumUpdateType.Stats);
        }
        return success;
    }
}

