/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.projectiles.EntityHook;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.enums.items.EnumRodType;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Optional;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemFishingRod
extends PixelmonItem {
    private EnumRodType rodType;

    public ItemFishingRod(EnumRodType rodType, String name) {
        super(name);
        this.rodType = rodType;
        this.canRepair = false;
        this.setMaxStackSize(1);
        this.setMaxDamage(rodType.maxDamage);
        this.setCreativeTab(CreativeTabs.TOOLS);
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public boolean isFull3D() {
        return true;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public boolean shouldRotateAroundWhenRendering() {
        return true;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand hand) {
        if (playerIn.fishEntity != null) {
            if (playerIn.fishEntity.caughtEntity instanceof EntityNPC) {
                playerIn.fishEntity.setDead();
                playerIn.swingArm(hand);
                return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, playerIn.getHeldItem(hand));
            }
            int i = playerIn.fishEntity.handleHookRetraction();
            playerIn.getHeldItem(hand).damageItem(i, playerIn);
        } else if (!worldIn.isRemote) {
            Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)playerIn);
            if (storageOpt.isPresent() && storageOpt.get().countAblePokemon() > 0) {
                worldIn.playSound(null, playerIn.posX, playerIn.posY + 0.5, playerIn.posZ, SoundEvents.ENTITY_ARROW_SHOOT, SoundCategory.PLAYERS, 0.5f, 1.0f);
                worldIn.spawnEntity(new EntityHook(worldIn, playerIn, this.rodType));
            } else {
                TextComponentTranslation comp = new TextComponentTranslation("fishingrod.nopokemonmessage", new Object[0]);
                comp.getStyle().setColor(TextFormatting.GRAY);
                playerIn.sendMessage(comp);
            }
        }
        playerIn.swingArm(hand);
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, playerIn.getHeldItem(hand));
    }

    public EnumRodType getRodType() {
        return this.rodType;
    }
}

