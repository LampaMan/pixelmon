/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;

public class ItemGracidea
extends PixelmonItem {
    public ItemGracidea() {
        super("gracidea");
    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World world, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (world.isRemote || hand != EnumHand.MAIN_HAND) {
            return EnumActionResult.PASS;
        }
        IBlockState targetBlock = world.getBlockState(pos);
        ItemStack heldStack = player.getHeldItem(hand);
        pos = pos.add(0, 1, 0);
        if (facing == EnumFacing.UP && targetBlock.getBlock() == Blocks.GRASS && world.getBlockState(pos).getBlock() == Blocks.AIR && world.canSeeSky(pos) && world.getBiome(pos).getTempCategory() == Biome.TempCategory.MEDIUM) {
            IBlockState state = PixelmonBlocks.gracideaBlock.getDefaultState();
            world.setBlockState(pos, state);
            state.getBlock().onBlockPlacedBy(world, pos, state, player, heldStack);
            heldStack.shrink(1);
            return EnumActionResult.SUCCESS;
        }
        return EnumActionResult.PASS;
    }
}

