/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import net.minecraft.creativetab.CreativeTabs;

public class ItemRanchUpgrade
extends PixelmonItem {
    public ItemRanchUpgrade() {
        super("ranch_upgrade");
        this.setCreativeTab(CreativeTabs.TOOLS);
    }
}

