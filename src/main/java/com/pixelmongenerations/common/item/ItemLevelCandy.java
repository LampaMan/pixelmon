/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.items.EnumEXPAmount;

public class ItemLevelCandy
extends PixelmonItem {
    private EnumEXPAmount amount;

    public ItemLevelCandy(EnumEXPAmount amount) {
        super(amount.name().toLowerCase() + "_candy");
        this.amount = amount;
        this.maxStackSize = 64;
        this.setCreativeTab(PixelmonCreativeTabs.restoration);
    }

    public EnumEXPAmount getExpAmount() {
        return this.amount;
    }
}

