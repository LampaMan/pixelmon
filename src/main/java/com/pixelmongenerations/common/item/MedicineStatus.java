/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.item.IMedicine;
import com.pixelmongenerations.core.network.EnumUpdateType;

public class MedicineStatus
implements IMedicine {
    private StatusType[] statuses;

    public MedicineStatus(StatusType ... statuses) {
        this.statuses = statuses;
    }

    @Override
    public boolean useMedicine(PokemonLink target) {
        if (target.removeStatuses(this.statuses)) {
            target.update(EnumUpdateType.Status);
            return true;
        }
        return false;
    }
}

