/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.SpawningEntity;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.util.SpawnColors;
import com.pixelmongenerations.core.util.SpawnMethodCooldowns;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Biomes;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemTimeGlass
extends PixelmonItem {
    public static int full = 100;

    public ItemTimeGlass() {
        super("time_glass");
        this.setCreativeTab(CreativeTabs.MISC);
        this.setMaxStackSize(1);
        this.setMaxDamage(full);
        this.setDamage(new ItemStack(this), 0);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer player, EnumHand hand) {
        ItemStack stack = player.getHeldItem(hand);
        if (!worldIn.isRemote) {
            int damage = stack.getItemDamage();
            if (damage >= full) {
                if (!SpawnMethodCooldowns.isOnCooldown(player)) {
                    if (worldIn.getBiome(player.getPosition()) == Biomes.MUTATED_FOREST) {
                        SpawningEntity entity = SpawningEntity.builder().pixelmon(EnumSpecies.Celebi).colors(SpawnColors.GREEN, SpawnColors.LIGHT_GREEN).location(() -> player.getPositionVector().add(0.0, 0.0, 0.0)).maxTick(200).build(worldIn, (EntityPlayerMP)player);
                        worldIn.spawnEntity(entity);
                        player.getHeldItem(hand).shrink(1);
                        SpawnMethodCooldowns.addCooldown(player, 240);
                    } else {
                        ChatHandler.sendChat(player, "pixelmon.timeglass.wrongbiome", new Object[0]);
                    }
                } else {
                    ChatHandler.sendChat(player, "pixelmon.spawnmethod.cooldown", new Object[0]);
                }
            } else {
                ChatHandler.sendChat(player, "pixelmon.timeglass.amount", damage);
            }
        }
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, stack);
    }
}

