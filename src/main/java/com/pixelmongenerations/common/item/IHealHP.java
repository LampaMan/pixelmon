/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;

public interface  IHealHP {
    public int getHealAmount(PokemonLink var1);
}

