/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.items.EnumJuice;
import com.pixelmongenerations.core.network.EnumUpdateType;

public class ItemJuice
extends PixelmonItem {
    private EnumJuice juice;

    public ItemJuice(EnumJuice juice) {
        super(juice.getId());
        this.juice = juice;
        this.setCreativeTab(PixelmonCreativeTabs.restoration);
    }

    public boolean consumeJuice(EntityPixelmon entityPixelmon) {
        boolean success = entityPixelmon.stats.EVs.juiceEVs(this.juice.getStatType());
        if (success) {
            entityPixelmon.friendship.increaseFriendship(10);
            entityPixelmon.updateStats();
            entityPixelmon.update(EnumUpdateType.Stats);
        }
        return success;
    }

    public EnumJuice getJuice() {
        return this.juice;
    }
}

