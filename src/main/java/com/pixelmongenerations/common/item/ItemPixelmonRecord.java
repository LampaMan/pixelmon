/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.item;

import java.util.List;
import javax.annotation.Nullable;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemRecord;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemPixelmonRecord
extends ItemRecord {
    private final boolean canSpawnMeloetta;
    public String musicName;
    private int duration;

    public ItemPixelmonRecord(String name, SoundEvent record) {
        this(name, record, 0, false);
    }

    public ItemPixelmonRecord(String name, SoundEvent record, int duration) {
        this(name, record, duration, true);
    }

    private ItemPixelmonRecord(String name, SoundEvent record, int duration, boolean canSpawnMeloetta) {
        super(name, record);
        this.musicName = name;
        this.duration = duration;
        this.setTranslationKey(name);
        this.setRegistryName("pixelmon:record_" + name);
        this.canSpawnMeloetta = canSpawnMeloetta;
    }

    @Override
    public boolean hasEffect(ItemStack stack) {
        return this.canSpawnMeloetta(stack);
    }

    public boolean canSpawnMeloetta(ItemStack stack) {
        return this.canSpawnMeloetta && (stack.getTagCompound() == null || !stack.getTagCompound().getBoolean("CanSpawnMeloetta"));
    }

    public static void setUsed(ItemStack stack, boolean used) {
        if (stack.getItem() instanceof ItemPixelmonRecord) {
            if (stack.getTagCompound() == null) {
                stack.setTagCompound(new NBTTagCompound());
            }
            stack.getTagCompound().setBoolean("CanSpawnMeloetta", used);
        }
    }

    public int getDuration() {
        return this.duration;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        String info = I18n.translateToLocal("gui.shopkeeper." + this.getTranslationKey());
        if (!this.hasHideFlag(stack) && !info.startsWith("gui.shopkeeper.")) {
            tooltip.add(GuiScreen.isShiftKeyDown() ? info : "Hold shift for more info.");
        }
    }

    public boolean hasHideFlag(ItemStack stack) {
        return stack.hasTagCompound() && stack.getTagCompound().getBoolean("HideTooltip");
    }
}

