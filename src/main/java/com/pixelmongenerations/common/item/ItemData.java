/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

public class ItemData {
    public int id;
    public int count;

    public ItemData(int id, int count) {
        this.id = id;
        this.count = count;
    }
}

