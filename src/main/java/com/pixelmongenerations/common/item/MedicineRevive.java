/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.item.IHealHP;
import com.pixelmongenerations.common.item.IMedicine;
import com.pixelmongenerations.core.network.EnumUpdateType;

public class MedicineRevive
implements IMedicine {
    private IHealHP healHP;

    public MedicineRevive(IHealHP healHP) {
        this.healHP = healHP;
    }

    @Override
    public boolean useMedicine(PokemonLink target) {
        return this.healPokemon(target);
    }

    protected boolean healPokemon(PokemonLink pxm) {
        if (pxm.getHealth() <= 0) {
            pxm.setHealth(this.healHP.getHealAmount(pxm));
            pxm.update(EnumUpdateType.HP);
            pxm.sendMessage("pixelmon.effect.revived", pxm.getNickname());
            return true;
        }
        return false;
    }
}

