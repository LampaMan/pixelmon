/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.EntityLegendFinder;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.List;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;

public class ItemLegendFinder
extends PixelmonItem {
    public ItemLegendFinder() {
        super("legend_finder");
        this.setCreativeTab(CreativeTabs.MISC);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
        ItemStack itemstack = playerIn.getHeldItem(handIn);
        playerIn.setActiveHand(handIn);
        if (!worldIn.isRemote) {
            List<EntityPixelmon> entities = worldIn.getEntitiesWithinAABB(EntityPixelmon.class, new AxisAlignedBB(playerIn.getPosition().getX() - 100, playerIn.getPosition().getY() - 100, playerIn.getPosition().getZ() - 100, playerIn.getPosition().getX() + 100, playerIn.getPosition().getY() + 100, playerIn.getPosition().getZ() + 100));
            BlockPos blockPos = null;
            for (EntityPixelmon entity : entities) {
                if (entity.getOwner() != null || !EnumSpecies.legendaries.contains(entity.baseStats.pokemon.name)) continue;
                blockPos = entity.getPosition();
            }
            if (blockPos == null) {
                blockPos = playerIn.getPosition().add(0, 5, 0);
                playerIn.sendMessage(new TextComponentTranslation("legendary.not.found", new Object[0]));
            }
            EntityLegendFinder legendFinder = new EntityLegendFinder(worldIn, playerIn.posX, playerIn.posY + (double)(playerIn.height / 2.0f), playerIn.posZ);
            legendFinder.moveTowards(blockPos);
            worldIn.spawnEntity(legendFinder);
            worldIn.playSound(null, playerIn.posX, playerIn.posY, playerIn.posZ, SoundEvents.ENTITY_ITEM_PICKUP, SoundCategory.NEUTRAL, 0.5f, 0.4f / (itemRand.nextFloat() * 0.4f + 0.8f));
            if (!playerIn.capabilities.isCreativeMode) {
                itemstack.shrink(1);
            }
            return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemstack);
        }
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemstack);
    }
}

