/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.network.ChatHandler;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemMeltanBox
extends PixelmonItem {
    public static int full = 50;

    public ItemMeltanBox() {
        super("meltan_box");
        this.setCreativeTab(CreativeTabs.MISC);
        this.setMaxStackSize(1);
        this.setMaxDamage(full);
        this.setDamage(new ItemStack(this), 0);
    }

    @Override
    public void onUpdate(ItemStack par1ItemStack, World par2World, Entity par3Entity, int par4, boolean par5) {
        int damage = par1ItemStack.getItemDamage();
        if (damage > full) {
            par1ItemStack.setItemDamage(full);
        }
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand hand) {
        ItemStack stack = playerIn.getHeldItem(hand);
        int damage = stack.getItemDamage();
        if (damage >= full) {
            ChatHandler.sendChat(playerIn, "pixelmon.meltanbox.full", new Object[0]);
        } else {
            ChatHandler.sendChat(playerIn, "pixelmon.meltanbox.amount", damage);
        }
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, stack);
    }
}

