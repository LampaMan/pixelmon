/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.api.events.ApricornEvent;
import com.pixelmongenerations.common.block.apricornTrees.BlockApricornTree;
import com.pixelmongenerations.common.block.enums.EnumBlockPos;
import com.pixelmongenerations.common.block.tileEntities.TileEntityApricornTree;
import com.pixelmongenerations.common.block.tileEntities.TileEntityBerryTree;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonBlocksApricornTrees;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.items.EnumApricorns;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class ItemApricorn
extends PixelmonItem {
    public EnumApricorns apricorn;
    private final Block block;

    public ItemApricorn(EnumApricorns apricorn) {
        super(apricorn.toString().toLowerCase() + "_apricorn");
        this.apricorn = apricorn;
        this.setMaxStackSize(64);
        this.setMaxDamage(1000000);
        this.setCreativeTab(PixelmonCreativeTabs.natural);
        switch (apricorn) {
            case Black: {
                this.block = PixelmonBlocksApricornTrees.apricornTreeBlack;
                break;
            }
            case White: {
                this.block = PixelmonBlocksApricornTrees.apricornTreeWhite;
                break;
            }
            case Pink: {
                this.block = PixelmonBlocksApricornTrees.apricornTreePink;
                break;
            }
            case Green: {
                this.block = PixelmonBlocksApricornTrees.apricornTreeGreen;
                break;
            }
            case Blue: {
                this.block = PixelmonBlocksApricornTrees.apricornTreeBlue;
                break;
            }
            case Yellow: {
                this.block = PixelmonBlocksApricornTrees.apricornTreeYellow;
                break;
            }
            case Red: {
                this.block = PixelmonBlocksApricornTrees.apricornTreeRed;
                break;
            }
            default: {
                this.block = null;
            }
        }
        this.canRepair = false;
    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer playerIn, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (worldIn.isRemote) {
            return EnumActionResult.SUCCESS;
        }
        if (!PixelmonConfig.allowPlanting) {
            return EnumActionResult.FAIL;
        }
        ItemStack stack = playerIn.getHeldItem(hand);
        Block groundBlock = worldIn.getBlockState(pos).getBlock();
        if (facing != EnumFacing.UP || !BlockApricornTree.isSuitableSoil(groundBlock)) {
            return EnumActionResult.FAIL;
        }
        int count = BlockHelper.countTileEntitiesOfType(worldIn, new ChunkPos(pos), TileEntityApricornTree.class);
        if ((count += BlockHelper.countTileEntitiesOfType(worldIn, new ChunkPos(pos), TileEntityBerryTree.class)) >= PixelmonConfig.maximumPlants) {
            ChatHandler.sendChat(playerIn, "pixelmon.blocks.maxPlants", new Object[0]);
            return EnumActionResult.FAIL;
        }
        if (!playerIn.canPlayerEdit(pos = pos.up(), facing, stack)) {
            return EnumActionResult.FAIL;
        }
        if (stack.getCount() <= 0) {
            return EnumActionResult.FAIL;
        }
        if (worldIn.mayPlace(this.block, pos, false, facing, null)) {
            ApricornEvent.ApricornPlantedEvent plantEvent = new ApricornEvent.ApricornPlantedEvent(this.apricorn, pos, (EntityPlayerMP)playerIn);
            if (MinecraftForge.EVENT_BUS.post(plantEvent)) {
                return EnumActionResult.FAIL;
            }
            IBlockState state = this.block.getDefaultState().withProperty(BlockApricornTree.BLOCKPOS, EnumBlockPos.BOTTOM);
            worldIn.setBlockState(pos, state, 3);
            state = worldIn.getBlockState(pos);
            if (state.getBlock() == this.block) {
                ItemBlock.setTileEntityNBT(worldIn, playerIn, pos, stack);
                state.getBlock().onBlockPlacedBy(worldIn, pos, state, playerIn, stack);
            }
            worldIn.playSound(null, (double)pos.getX() + 0.5, (double)pos.getY() + 0.5, (double)pos.getZ() + 0.5, SoundEvents.BLOCK_GRASS_STEP, SoundCategory.PLAYERS, 0.5f, 1.0f);
            if (!playerIn.isCreative()) {
                if (stack.getCount() <= 1) {
                    playerIn.setHeldItem(hand, ItemStack.EMPTY);
                } else {
                    stack.shrink(1);
                }
            }
            return EnumActionResult.SUCCESS;
        }
        return EnumActionResult.PASS;
    }
}

