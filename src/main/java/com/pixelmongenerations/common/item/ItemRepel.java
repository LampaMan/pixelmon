/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.event.RepelHandler;
import com.pixelmongenerations.core.network.ChatHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

public class ItemRepel
extends PixelmonItem {
    EnumRepel repel;

    public ItemRepel(EnumRepel repel) {
        super(repel.name().toLowerCase());
        this.repel = repel;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
        ItemStack stack = player.getHeldItem(hand);
        if (hand == EnumHand.OFF_HAND || world.isRemote) {
            return new ActionResult<ItemStack>(EnumActionResult.PASS, stack);
        }
        ItemStack heldStack = player.getHeldItem(hand);
        if (!player.isCreative()) {
            heldStack.splitStack(1);
        }
        boolean hadRepel = RepelHandler.hasRepel(player.getUniqueID());
        RepelHandler.applyRepel(player.getUniqueID(), this.repel);
        if (hadRepel) {
            ChatHandler.sendFormattedChat(player, TextFormatting.DARK_GREEN, "item.repel.applyextended", new Object[0]);
        } else {
            ChatHandler.sendFormattedChat(player, TextFormatting.DARK_GREEN, "item.repel.apply", new Object[0]);
        }
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, stack);
    }

    public static enum EnumRepel {
        REPEL(6000),
        SUPER_REPEL(18000),
        MAX_REPEL(36000);

        public final int ticks;

        private EnumRepel(int ticks) {
            this.ticks = ticks;
        }
    }
}

