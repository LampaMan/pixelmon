/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.api.pokemon.SpawnPokemon;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemSecretArmorScroll
extends PixelmonItem {
    public static int full = 100;

    public ItemSecretArmorScroll() {
        super("secret_armor_scroll");
        this.setCreativeTab(CreativeTabs.MISC);
        this.setMaxStackSize(1);
        this.setMaxDamage(full);
        this.setDamage(new ItemStack(this), 0);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand hand) {
        ItemStack stack = playerIn.getHeldItem(hand);
        if (!worldIn.isRemote) {
            int damage = stack.getItemDamage();
            if (damage >= this.getMaxDamage(stack)) {
                PixelmonStorage.pokeBallManager.getPlayerStorageFromUUID(playerIn.getUniqueID()).ifPresent(storage -> {
                    if (storage.getTeam().stream().filter(data -> data.getString("Name").equals(EnumSpecies.Slowpoke.name)).count() < 3L) {
                        ChatHandler.sendChat(playerIn, "pixelmon.secret_armor_scroll.checkparty", new Object[0]);
                    } else {
                        stack.shrink(1);
                        SpawnPokemon.of(EnumSpecies.Kubfu, playerIn.getPosition(), playerIn.getRotationYawHead()).spawn((EntityPlayerMP)playerIn, playerIn.world);
                    }
                });
            } else {
                ChatHandler.sendChat(playerIn, "pixelmon.secret_armor_scroll.amountfull", this.getMaxDamage(stack) - damage);
                return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, stack);
            }
        }
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, stack);
    }
}

