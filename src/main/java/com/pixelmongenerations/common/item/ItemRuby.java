/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.extraStats.LakeTrioStats;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.EnumUpdateType;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;

public class ItemRuby
extends PixelmonItem {
    public ItemRuby() {
        super("ruby");
        this.setHasSubtypes(true);
        this.setMaxDamage(0);
    }

    @Override
    public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity) {
        if (!player.world.isRemote && stack.getMetadata() == 0) {
            EntityPlayerMP playerMP = (EntityPlayerMP)player;
            if (entity instanceof EntityPixelmon) {
                EntityPixelmon pixelmon = (EntityPixelmon)entity;
                EnumSpecies lakeGuardian = pixelmon.getSpecies();
                if (!pixelmon.isPokemon("Azelf")) {
                    if (!pixelmon.isPokemon("Mesprit")) {
                        if (!pixelmon.isPokemon("Uxie")) {
                            return false;
                        }
                    }
                }
                if (pixelmon.originalTrainerUUID.equalsIgnoreCase(player.getUniqueID().toString()) && pixelmon.isOwner(player)) {
                    LakeTrioStats lakeTrioStats = (LakeTrioStats)pixelmon.extraStats;
                    if (lakeTrioStats.numEnchanted < LakeTrioStats.MAX_ENCHANTED) {
                        if (pixelmon.friendship.getFriendship() >= 255) {
                            if (pixelmon.getLvl().getLevel() >= 60) {
                                ++lakeTrioStats.numEnchanted;
                                pixelmon.getLvl().setLevel(pixelmon.getLvl().getLevel() / 2 + 10);
                                pixelmon.friendship.setFriendship(200);
                                TextComponentTranslation translation = new TextComponentTranslation("ruby.success", pixelmon.getLocalizedName());
                                translation.getStyle().setColor(TextFormatting.GREEN);
                                playerMP.sendMessage(translation);
                                pixelmon.update(EnumUpdateType.Stats);
                                pixelmon.update(EnumUpdateType.Friendship);
                                if (stack.getCount() > 1) {
                                    ItemStack newStack = new ItemStack(PixelmonItems.ruby, 1, lakeGuardian.getNationalPokedexInteger() - 479);
                                    stack.shrink(1);
                                    player.addItemStackToInventory(newStack);
                                } else {
                                    stack.setItemDamage(lakeGuardian.getNationalPokedexInteger() - 479);
                                }
                            } else {
                                playerMP.sendMessage(new TextComponentString(String.format((Object)((Object)TextFormatting.GRAY) + I18n.translateToLocal("ruby.fail.level"), pixelmon.getLocalizedName())));
                            }
                        } else {
                            playerMP.sendMessage(new TextComponentString(String.format((Object)((Object)TextFormatting.GRAY) + I18n.translateToLocal("ruby.fail.happiness"), pixelmon.getLocalizedName())));
                        }
                    } else {
                        playerMP.sendMessage(new TextComponentString(String.format((Object)((Object)TextFormatting.GRAY) + I18n.translateToLocal("ruby.fail.count"), pixelmon.getLocalizedName())));
                    }
                } else {
                    playerMP.sendMessage(new TextComponentString(String.format((Object)((Object)TextFormatting.GRAY) + I18n.translateToLocal("ruby.fail.owner"), pixelmon.getLocalizedName())));
                }
            }
        }
        return false;
    }

    public String getTranslationKey(ItemStack stack) {
        if (stack.getItemDamage() == 1) {
            return "item.ruby_uxie";
        }
        if (stack.getItemDamage() == 2) {
            return "item.ruby_mesprit";
        }
        if (stack.getItemDamage() == 3) {
            return "item.ruby_azelf";
        }
        return "item.ruby";
    }

    @Override
    public boolean hasEffect(ItemStack stack) {
        return stack.getMetadata() > 0;
    }

    @Override
    public int getItemStackLimit(ItemStack stack) {
        return stack.getMetadata() > 0 ? 1 : 64;
    }

    @Override
    public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
        if (this.isInCreativeTab(tab)) {
            for (int i = 0; i < 4; ++i) {
                items.add(new ItemStack(this, 1, i));
            }
        }
    }
}

