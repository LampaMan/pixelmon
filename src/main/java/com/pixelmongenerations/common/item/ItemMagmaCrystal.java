/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.EntityMagmaCrystal;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.util.SpawnMethodCooldowns;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;

public class ItemMagmaCrystal
extends PixelmonItem {
    public ItemMagmaCrystal(String id) {
        super(id);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
        ItemStack itemstack = playerIn.getHeldItem(handIn);
        if (!worldIn.isRemote) {
            if (!SpawnMethodCooldowns.isOnCooldown(playerIn)) {
                if (!playerIn.capabilities.isCreativeMode) {
                    itemstack.shrink(1);
                }
                EntityMagmaCrystal entityegg = new EntityMagmaCrystal(worldIn, playerIn);
                entityegg.shoot(playerIn, playerIn.rotationPitch, playerIn.rotationYaw, 0.0f, 1.5f, 1.0f);
                worldIn.spawnEntity(entityegg);
                worldIn.playSound(null, playerIn.posX, playerIn.posY, playerIn.posZ, SoundEvents.ENTITY_EGG_THROW, SoundCategory.PLAYERS, 0.5f, 0.4f / (itemRand.nextFloat() * 0.4f + 0.8f));
                SpawnMethodCooldowns.addCooldown(playerIn, 240);
            } else {
                ChatHandler.sendChat(playerIn, "pixelmon.spawnmethod.cooldown", new Object[0]);
            }
        }
        playerIn.addStat(StatList.getObjectUseStats(this));
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemstack);
    }
}

