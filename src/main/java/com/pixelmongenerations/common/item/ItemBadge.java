/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;

public class ItemBadge
extends PixelmonItem {
    public ItemBadge(String badgeName) {
        super(badgeName);
        this.setMaxStackSize(64);
        this.setMaxDamage(1000000);
        this.setCreativeTab(PixelmonCreativeTabs.badges);
        this.canRepair = false;
    }
}

