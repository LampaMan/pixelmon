/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.api.events.StatueEvent;
import com.pixelmongenerations.common.entity.pixelmon.EntityStatue;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.network.packetHandlers.statueEditor.StatuePacketClient;
import java.util.UUID;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.server.permission.PermissionAPI;

public class ItemStatueMaker
extends PixelmonItem {
    public ItemStatueMaker() {
        super("chisel");
        this.setCreativeTab(CreativeTabs.TOOLS);
    }

    @Override
    public EnumActionResult onItemUseFirst(EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ, EnumHand hand) {
        if (side != EnumFacing.UP) {
            return EnumActionResult.FAIL;
        }
        if (!world.isRemote) {
            EntityStatue statue = new EntityStatue(world);
            statue.init("Abra");
            UUID uuid = UUID.randomUUID();
            statue.setPokemonId(new int[]{(int)uuid.getMostSignificantBits(), (int)uuid.getLeastSignificantBits()});
            statue.setPosition((float)pos.getX() + hitX, pos.getY() + 1, (float)pos.getZ() + hitZ);
            StatueEvent.CreateStatue createStatueEvent = new StatueEvent.CreateStatue((EntityPlayerMP)player, (WorldServer)world, pos, statue);
            if (MinecraftForge.EVENT_BUS.post(createStatueEvent)) {
                return EnumActionResult.FAIL;
            }
            statue = createStatueEvent.getStatue();
            world.spawnEntity(statue);
            statue.setPosition((float)pos.getX() + hitX, pos.getY() + 1, (float)pos.getZ() + hitZ);
            statue.setRotation(player.rotationYaw + 180.0f);
            Pixelmon.NETWORK.sendTo(new StatuePacketClient(statue.getPokemonId()), (EntityPlayerMP)player);
            player.openGui(Pixelmon.INSTANCE, EnumGui.StatueEditor.getIndex(), world, statue.getEntityId(), PermissionAPI.hasPermission(player, "pixelmon.admin.admin_statue") ? 1 : 0, 0);
            return EnumActionResult.SUCCESS;
        }
        return EnumActionResult.PASS;
    }

    @Override
    public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity) {
        if (entity instanceof EntityStatue && !player.world.isRemote && ItemStatueMaker.checkCanPlayerDeleteStatue((EntityStatue)entity, (EntityPlayerMP)player) && !MinecraftForge.EVENT_BUS.post(new StatueEvent.DestroyStatue((EntityPlayerMP)player, (EntityStatue)entity))) {
            entity.setDead();
        }
        return super.onLeftClickEntity(stack, player, entity);
    }

    public static boolean checkCanPlayerDeleteStatue(EntityStatue statue, EntityPlayerMP player) {
        return !statue.isAdminPlaced() || PermissionAPI.hasPermission(player, "pixelmon.admin.admin_statue") && player.isCreative();
    }
}

