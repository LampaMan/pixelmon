/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.ItemFossil;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.items.EnumFossils;
import java.util.ArrayList;

public class ItemCoveredFossil
extends PixelmonItem {
    public ItemFossil cleanedFossil;
    public static ArrayList<ItemCoveredFossil> fossilList = new ArrayList();
    public EnumFossils fossils;

    public ItemCoveredFossil(ItemFossil cleanedFossil, EnumFossils fossils) {
        super("covered_fossil_" + fossils.getIndex());
        this.cleanedFossil = cleanedFossil;
        this.fossils = fossils;
        this.canRepair = false;
        fossilList.add(this);
        this.setCreativeTab(PixelmonCreativeTabs.natural);
    }

    public EnumFossils getFossils() {
        return this.fossils;
    }

    public int getGeneration() {
        return this.fossils.getGeneration();
    }
}

