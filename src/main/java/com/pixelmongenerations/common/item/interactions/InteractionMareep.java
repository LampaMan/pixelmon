/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumMareep;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import java.time.Instant;
import java.util.List;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemShears;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;

public class InteractionMareep
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon pixelmon, EntityPlayer player, EnumHand hand, ItemStack itemStack) {
        Item item = itemStack.getItem();
        if (pixelmon.getOwner() == player) {
            if (pixelmon.isPokemon(EnumSpecies.Mareep) && pixelmon.getForm() != EnumMareep.Shaved.getForm()) {
                if (item instanceof ItemShears) {
                    List<ItemStack> stacks = ((EnumMareep)pixelmon.getFormEnum()).getShearDrop(pixelmon.getGrowth(), pixelmon.isShiny());
                    for (ItemStack stack : stacks) {
                        EntityItem entityitem = pixelmon.entityDropItem(stack, 1.0f);
                        entityitem.motionY += Math.random() * (double)0.05f;
                        entityitem.motionX += (Math.random() - Math.random()) * (double)0.1f;
                        entityitem.motionZ += (Math.random() - Math.random()) * (double)0.1f;
                    }
                    pixelmon.setForm(EnumMareep.Shaved.getForm());
                    pixelmon.getEntityData().setLong("fluffTime", Instant.now().toEpochMilli());
                    itemStack.damageItem(1, player);
                    pixelmon.playSound(SoundEvents.ENTITY_SHEEP_SHEAR, 1.0f, 1.0f);
                    return true;
                }
                if (item == Items.DYE) {
                    IEnumForm dyeColor = EnumMareep.getFormFromDye(EnumDyeColor.byDyeDamage(itemStack.getMetadata()));
                    IEnumForm formColor = pixelmon.getFormEnum();
                    if (formColor != EnumMareep.Shaved && dyeColor != formColor) {
                        pixelmon.setForm(dyeColor.getForm());
                        if (!player.capabilities.isCreativeMode) {
                            itemStack.shrink(1);
                        }
                        return true;
                    }
                }
            }
        }
        return false;
    }
}

