/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.heldItems.NoItem;
import com.pixelmongenerations.common.item.heldItems.ZCrystal;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;

public class InteractionHeldItem
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon entityPixelmon, EntityPlayer player, EnumHand hand, ItemStack itemStack) {
        Item item;
        if (player instanceof EntityPlayerMP && entityPixelmon.getOwner() == player && !entityPixelmon.isInRanchBlock && hand == EnumHand.MAIN_HAND && (item = itemStack.getItem()) instanceof ItemHeld) {
            ItemHeld itemHeld = (ItemHeld)item;
            if (itemHeld instanceof ZCrystal) {
                ZCrystal crystal = (ZCrystal)itemHeld;
                if (!crystal.canBeAppliedToPokemon(new PokemonSpec(entityPixelmon.getName()).setForm(entityPixelmon.getForm()))) {
                    return false;
                }
            }
            if (itemHeld.interact(entityPixelmon, itemStack, player)) {
                player.getHeldItem(hand).shrink(1);
                return true;
            }
            ItemStack currentItem = entityPixelmon.getHeldItemMainhand();
            if (currentItem != null && currentItem != ItemStack.EMPTY) {
                if (currentItem.getItem() == item) {
                    return true;
                }
                if (!entityPixelmon.world.isRemote) {
                    entityPixelmon.entityDropItem(entityPixelmon.heldItem.copy(), 1.0f);
                }
                entityPixelmon.setHeldItem(ItemStack.EMPTY);
            }
            ItemStack itemStack1 = itemStack.copy();
            player.getHeldItem(hand).shrink(1);
            itemStack1.setCount(1);
            entityPixelmon.setHeldItem(itemStack1);
            if (entityPixelmon.getItemHeld() == NoItem.noItem) {
                ChatHandler.sendChat(player, "Couldn't give item: " + itemStack1.getDisplayName(), new Object[0]);
            } else {
                entityPixelmon.update(EnumUpdateType.HeldItem);
                ChatHandler.sendChat(player, "pixelmon.interaction.helditem", entityPixelmon.getNickname(), itemHeld.getLocalizedName());
            }
            return true;
        }
        return false;
    }
}

