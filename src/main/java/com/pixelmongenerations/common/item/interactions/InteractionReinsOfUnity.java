/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumCalyrex;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;

public class InteractionReinsOfUnity
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon pixelmon, EntityPlayer player, EnumHand hand, ItemStack itemStack) {
        Item item = itemStack.getItem();
        if (pixelmon.getOwner() == player && item == PixelmonItems.reinsOfUnity) {
            if (pixelmon.isPokemon(EnumSpecies.Calyrex)) {
                PlayerStorage pstore = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player).get();
                if (pixelmon.getForm() == 0) {
                    for (int i = 0; i < pstore.partyPokemon.length; ++i) {
                        NBTTagCompound nbt = pstore.partyPokemon[i];
                        if (nbt == null || !nbt.getString("Name").equalsIgnoreCase("Glastrier") && !nbt.getString("Name").equalsIgnoreCase("Spectrier")) continue;
                        pixelmon.setForm(EnumCalyrex.getFromPokemon(nbt.getString("Name")).getForm());
                        pixelmon.embed(nbt);
                        pstore.recallAllPokemon();
                        pstore.removeFromPartyPlayer(i);
                        ChatHandler.sendChat(player, "pixelmon.abilities.changeform", pixelmon.getNickname());
                        return true;
                    }
                } else if (pixelmon.getForm() != 0) {
                    pixelmon.setForm(0);
                    for (NBTTagCompound nbt : pixelmon.embeddedPokemon) {
                        if (!nbt.getString("Name").equalsIgnoreCase("Spectrier") && !nbt.getString("Name").equalsIgnoreCase("Glastrier")) continue;
                        pstore.addToParty((EntityPixelmon)PixelmonEntityList.createEntityFromNBT(nbt, player.getEntityWorld()));
                        pixelmon.embeddedPokemon.remove(nbt);
                        ChatHandler.sendChat(player, "pixelmon.calyrex.changeformback", pixelmon.getNickname());
                        return true;
                    }
                }
                ChatHandler.sendChat(player, "pixelmon.calyrex.fail", new Object[0]);
            }
        }
        return false;
    }
}

