/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.ItemLevelCandy;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumExpSource;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;

public class InteractionLevelCandy
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon pixelmon, EntityPlayer player, EnumHand hand, ItemStack itemStack) {
        Item item = itemStack.getItem();
        if (pixelmon.getOwner() == player && item instanceof ItemLevelCandy) {
            itemStack.shrink(1);
            pixelmon.level.awardEXP(((ItemLevelCandy)item).getExpAmount().expAmount, PixelmonConfig.levelCandyTriggersLevelEvent, EnumExpSource.LevelCandy);
            return true;
        }
        return false;
    }
}

