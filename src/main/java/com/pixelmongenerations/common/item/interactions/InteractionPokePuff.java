/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.FriendShip;
import com.pixelmongenerations.common.item.ItemPokePuff;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentTranslation;

public class InteractionPokePuff
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon entityPixelmon, EntityPlayer player, EnumHand hand, ItemStack itemstack) {
        if (player instanceof EntityPlayerMP && entityPixelmon.getOwner() == player && !entityPixelmon.isInRanchBlock && itemstack.getItem() instanceof ItemPokePuff) {
            FriendShip friendship = entityPixelmon.friendship;
            if (friendship.getFriendship() < 255) {
                friendship.increaseFriendship(((ItemPokePuff)itemstack.getItem()).getFriendShipAmount());
                player.sendMessage(new TextComponentTranslation("pixelmon.general.happiness.raised", entityPixelmon.getName()));
                if (!player.capabilities.isCreativeMode) {
                    player.getHeldItem(hand).shrink(1);
                }
                return true;
            }
            player.sendMessage(new TextComponentTranslation("pixelmon.general.noeffect", new Object[0]));
        }
        return false;
    }
}

