/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.ItemPPUp;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.packetHandlers.battles.OpenBattleMode;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;

public class InteractionPPUp
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon entityPixelmon, EntityPlayer player, EnumHand hand, ItemStack itemstack) {
        Item item;
        if (player instanceof EntityPlayerMP && entityPixelmon.getOwner() == player && !entityPixelmon.isInRanchBlock && (item = itemstack.getItem()) instanceof ItemPPUp) {
            if (!entityPixelmon.getMoveset().hasMaxBasePP()) {
                Pixelmon.NETWORK.sendTo(new OpenBattleMode(BattleMode.ChoosePPUp, entityPixelmon.getPartyPosition()), (EntityPlayerMP)player);
                return true;
            }
            ChatHandler.sendChat(player, "pixelmon.interaction.ppupfail", entityPixelmon.getNickname());
        }
        return false;
    }
}

