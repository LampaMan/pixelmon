/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.ItemFeather;
import com.pixelmongenerations.core.network.ChatHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;

public class InteractionFeathers
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon entityPixelmon, EntityPlayer player, EnumHand hand, ItemStack itemstack) {
        Item item;
        if (player instanceof EntityPlayerMP && entityPixelmon.getOwner() == player && !entityPixelmon.isInRanchBlock && (item = itemstack.getItem()) instanceof ItemFeather) {
            ItemFeather feather = (ItemFeather)item;
            String nickname = entityPixelmon.getNickname();
            String statName = feather.type.getLocalizedName();
            if (feather.featherEVs(entityPixelmon)) {
                ChatHandler.sendChat(player, "pixelmon.interaction.feather", nickname, statName);
                if (!player.capabilities.isCreativeMode) {
                    player.getHeldItem(hand).shrink(1);
                }
                return true;
            }
            ChatHandler.sendChat(player, "pixelmon.interaction.featherfail", nickname, statName);
        }
        return false;
    }
}

