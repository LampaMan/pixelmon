/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.events.PokemonEatCurryEvent;
import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.capabilities.curry.CurryData;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.EntityLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.item.ItemCurry;
import com.pixelmongenerations.common.item.ItemElixir;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumExpSource;
import com.pixelmongenerations.core.enums.EnumMark;
import com.pixelmongenerations.core.enums.EnumMarkActivity;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.storage.PCServer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraftforge.common.MinecraftForge;

public class InteractionCurry
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon pixelmon, EntityPlayer player, EnumHand hand, ItemStack itemStack) {
        if (pixelmon.getOwner() == player && itemStack.getItem() instanceof ItemCurry) {
            CurryData data = ItemCurry.getData(itemStack);
            if (MinecraftForge.EVENT_BUS.post(new PokemonEatCurryEvent(pixelmon, player, data))) {
                return false;
            }
            this.apply(pixelmon, data);
            if (pixelmon.getMark() == EnumMark.None) {
                pixelmon.setMark(EnumMark.rollMark((EntityPlayerMP)player, pixelmon, EnumMarkActivity.Curry));
                pixelmon.update(EnumUpdateType.Mark);
            }
            itemStack.shrink(1);
            player.addItemStackToInventory(new ItemStack(Items.BOWL));
            if (!player.world.isRemote && PCServer.giveBackground((EntityPlayerMP)player, "box_great_outdoors")) {
                ChatHandler.sendChat(player, "pixelmon.backgrounds.unlocked", "Great Outdoors");
            }
            return true;
        }
        return false;
    }

    private void apply(EntityPixelmon pixelmon, CurryData data) {
        if (data.canHealStatus()) {
            pixelmon.clearStatus();
        }
        pixelmon.healByPercent((float)data.getHealthPercentage());
        pixelmon.level.awardEXP(data.getExperience(), PixelmonConfig.curryTriggersLevelEvent, EnumExpSource.Curry);
        pixelmon.friendship.increaseFriendship(data.getFriendship());
        for (int i = 0; i < pixelmon.getMoveset().size(); ++i) {
            ItemElixir.restorePP((PokemonLink)new EntityLink(pixelmon), i, true);
        }
    }
}

