/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumSpecies;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;

public class InteractionArmor
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon pixelmon, EntityPlayer player, EnumHand hand, ItemStack itemStack) {
        Item item = itemStack.getItem();
        if (item != PixelmonItems.mewtwoArmor) {
            return false;
        }
        if (pixelmon.getOwner() != player) {
            return false;
        }
        if (pixelmon.getSpecies() != EnumSpecies.Mewtwo) {
            return false;
        }
        if (pixelmon.isShiny()) {
            return false;
        }
        if (pixelmon.getSpecialTextureIndex() == 4) {
            pixelmon.setSpecialTexture(0);
            return true;
        }
        if (pixelmon.getSpecialTextureIndex() == 1) {
            return false;
        }
        pixelmon.setSpecialTexture(4);
        player.getHeldItemMainhand().shrink(1);
        return true;
    }
}

