/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.api.spawning.conditions.WorldTime;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumTextures;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.EnumUpdateType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class InteractionHalloweenSkins
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon pixelmon, EntityPlayer player, EnumHand enumHand, ItemStack itemStack) {
        if (pixelmon.getOwner() != player) {
            return false;
        }
        if (enumHand == EnumHand.MAIN_HAND && itemStack.getItem() == Item.getItemFromBlock(Blocks.LIT_PUMPKIN)) {
            World world = pixelmon.world;
            BlockPos pos = new BlockPos(pixelmon.posX, pixelmon.posY, pixelmon.posZ);
            if (world.getMoonPhase() == 0 && world.getLight(pos) >= 12 && world.canSeeSky(pos) && WorldTime.NIGHT.tickCondition.test((int)world.getWorldTime()) && this.isStarter(pixelmon.getSpecies()) && pixelmon.getSpecialTextureIndex() == 0 && pixelmon.getSpecies().getSpecialTexture(pixelmon.getFormEnum(), 1) == EnumTextures.Halloween) {
                pixelmon.setSpecialTexture(1);
                pixelmon.update(EnumUpdateType.Texture);
                return true;
            }
        }
        return false;
    }

    private boolean isStarter(EnumSpecies pokemon) {
        return pokemon == EnumSpecies.Venusaur || pokemon == EnumSpecies.Charizard || pokemon == EnumSpecies.Blastoise || pokemon == EnumSpecies.Meganium || pokemon == EnumSpecies.Typhlosion || pokemon == EnumSpecies.Feraligatr || pokemon == EnumSpecies.Sceptile || pokemon == EnumSpecies.Blaziken || pokemon == EnumSpecies.Swampert || pokemon == EnumSpecies.Torterra || pokemon == EnumSpecies.Infernape || pokemon == EnumSpecies.Empoleon || pokemon == EnumSpecies.Serperior || pokemon == EnumSpecies.Emboar || pokemon == EnumSpecies.Samurott || pokemon == EnumSpecies.Chesnaught || pokemon == EnumSpecies.Delphox || pokemon == EnumSpecies.Greninja || pokemon == EnumSpecies.Decidueye || pokemon == EnumSpecies.Incineroar || pokemon == EnumSpecies.Primarina;
    }
}

