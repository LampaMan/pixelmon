/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;

public class InteractionDynamaxCandy
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon pixelmon, EntityPlayer player, EnumHand hand, ItemStack itemStack) {
        Item item = itemStack.getItem();
        if (pixelmon.getOwner() == player && item == PixelmonItems.dynamaxCandy && player.isServerWorld()) {
            int newDynamaxLevel = pixelmon.getLvl().getDynamaxLevel() + 1;
            if (newDynamaxLevel <= 10) {
                pixelmon.getLvl().setDynamaxLevel(newDynamaxLevel);
                pixelmon.update(EnumUpdateType.Stats);
                ChatHandler.sendChat(player, ChatHandler.getMessage("pixelmon.interaction.dynamaxcandy", pixelmon.getName(), newDynamaxLevel));
                if (!player.capabilities.isCreativeMode) {
                    player.getHeldItem(hand).shrink(1);
                }
                return true;
            }
            ChatHandler.sendChat(player, ChatHandler.getMessage("pixelmon.interaction.dynamaxcandymax", pixelmon.getName()));
        }
        return false;
    }
}

