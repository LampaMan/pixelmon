/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;

public class InteractionSaddle
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon pixelmon, EntityPlayer player, EnumHand hand, ItemStack itemStack) {
        Item item = itemStack.getItem();
        if (item != Items.SADDLE) {
            return false;
        }
        if (pixelmon.getOwner() != player) {
            return false;
        }
        if (pixelmon.getSpecies() != EnumSpecies.Machamp && pixelmon.getSpecies() != EnumSpecies.Mudsdale && pixelmon.getSpecies() != EnumSpecies.Stoutland && pixelmon.getSpecies() != EnumSpecies.Tauros && pixelmon.getSpecies() != EnumSpecies.Charizard && pixelmon.getSpecies() != EnumSpecies.Lapras) {
            return false;
        }
        if (pixelmon.isShiny()) {
            return false;
        }
        if (pixelmon.getSpecialTextureIndex() == 3) {
            pixelmon.setSpecialTexture(0);
            return true;
        }
        if (pixelmon.getSpecialTextureIndex() != 0) {
            return false;
        }
        pixelmon.setSpecialTexture(3);
        player.getHeldItemMainhand().shrink(1);
        return true;
    }
}

