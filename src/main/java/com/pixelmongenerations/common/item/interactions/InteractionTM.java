/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.item.ItemTM;
import com.pixelmongenerations.common.item.ItemTR;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.packetHandlers.OpenReplaceMoveScreen;
import com.pixelmongenerations.core.util.MoveCostList;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentTranslation;

public class InteractionTM
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon entityPixelmon, EntityPlayer player, EnumHand hand, ItemStack itemstack) {
        if (player instanceof EntityPlayerMP && itemstack.getItem() instanceof ItemTM && !(itemstack.getItem() instanceof ItemTR)) {
            Optional<BaseStats> baseStats;
            int id = entityPixelmon.baseStats.baseFormID;
            if (entityPixelmon.hasForms() && entityPixelmon.getForm() != -1 && (baseStats = Entity3HasStats.getBaseStats(entityPixelmon.getSpecies(), entityPixelmon.getForm())).isPresent() && id != baseStats.get().id && !entityPixelmon.isMega) {
                id = baseStats.get().id;
            }
            if (player == entityPixelmon.getOwner() && !entityPixelmon.isInRanchBlock && DatabaseMoves.CanLearnAttack(id, ((ItemTM)itemstack.getItem()).attackName)) {
                Attack a = DatabaseMoves.getAttack(((ItemTM)itemstack.getItem()).attackName);
                if (a == null) {
                    ChatHandler.sendChat(entityPixelmon.getOwner(), ((ItemTM)itemstack.getItem()).attackName + " is corrupted", new Object[0]);
                } else if (entityPixelmon.getMoveset().size() >= 4) {
                    for (int i = 0; i < 4; ++i) {
                        if (!entityPixelmon.getMoveset().get(i).getAttackBase().equals(a.getAttackBase())) continue;
                        ChatHandler.sendChat(entityPixelmon.getOwner(), "pixelmon.interaction.tmknown", entityPixelmon.getNickname(), a.getAttackBase().getLocalizedName());
                        return true;
                    }
                    if (!(player.capabilities.isCreativeMode || ((ItemTM)itemstack.getItem()).isTR || PixelmonConfig.allowTMReuse)) {
                        MoveCostList.addToList((EntityPlayerMP)player, new ItemStack(itemstack.getItem()));
                    }
                    EntityPlayerMP targetPlayer = (EntityPlayerMP)entityPixelmon.getOwner();
                    Pixelmon.NETWORK.sendTo(new OpenReplaceMoveScreen(targetPlayer.getUniqueID(), entityPixelmon.getPokemonId(), a.getAttackBase().attackIndex, 0, entityPixelmon.getLvl().getLevel()), targetPlayer);
                } else {
                    for (int i = 0; i < entityPixelmon.getMoveset().size(); ++i) {
                        if (!entityPixelmon.getMoveset().get(i).getAttackBase().equals(a.getAttackBase())) continue;
                        ChatHandler.sendChat(entityPixelmon.getOwner(), "pixelmon.interaction.tmknown", entityPixelmon.getNickname(), a.getAttackBase().getLocalizedName());
                        return true;
                    }
                    entityPixelmon.getMoveset().add(a);
                    ChatHandler.sendChat(entityPixelmon.getOwner(), "pixelmon.stats.learnedmove", entityPixelmon.getNickname(), a.getAttackBase().getLocalizedName());
                    if (!(player.capabilities.isCreativeMode || ((ItemTM)itemstack.getItem()).isTR || PixelmonConfig.allowTMReuse)) {
                        player.getHeldItem(hand).shrink(1);
                    }
                }
                entityPixelmon.update(EnumUpdateType.Moveset);
            } else {
                ChatHandler.sendChat(entityPixelmon.getOwner(), "pixelmon.interaction.tmcantlearn", entityPixelmon.getNickname(), new TextComponentTranslation("attack." + ((ItemTM)itemstack.getItem()).attackName.toLowerCase() + ".name", new Object[0]));
            }
            return true;
        }
        if (player instanceof EntityPlayerMP && itemstack.getItem() instanceof ItemTR) {
            Optional<BaseStats> baseStats = Entity3HasStats.getBaseStats(entityPixelmon.getSpecies(), entityPixelmon.getForm());
            int id = entityPixelmon.baseStats.baseFormID;
            if (entityPixelmon.hasForms() && entityPixelmon.getForm() != -1 && baseStats.isPresent() && id != baseStats.get().id && !entityPixelmon.isMega) {
                id = baseStats.get().id;
            }
            if (player == entityPixelmon.getOwner() && !entityPixelmon.isInRanchBlock && DatabaseMoves.CanLearnAttack(id, ((ItemTM)itemstack.getItem()).attackName)) {
                Attack a = DatabaseMoves.getAttack(((ItemTM)itemstack.getItem()).attackName);
                if (a == null) {
                    ChatHandler.sendChat(entityPixelmon.getOwner(), ((ItemTM)itemstack.getItem()).attackName + " is corrupted", new Object[0]);
                } else if (entityPixelmon.getMoveset().size() >= 4) {
                    for (int i = 0; i < 4; ++i) {
                        if (!entityPixelmon.getMoveset().get(i).getAttackBase().equals(a.getAttackBase())) continue;
                        ChatHandler.sendChat(entityPixelmon.getOwner(), "pixelmon.interaction.tmknown", entityPixelmon.getNickname(), a.getAttackBase().getLocalizedName());
                        return true;
                    }
                    if (!player.capabilities.isCreativeMode && !((ItemTR)itemstack.getItem()).isTR || PixelmonConfig.allowTMReuse) {
                        MoveCostList.addToList((EntityPlayerMP)player, new ItemStack(itemstack.getItem()));
                    }
                    EntityPlayerMP targetPlayer = (EntityPlayerMP)entityPixelmon.getOwner();
                    Pixelmon.NETWORK.sendTo(new OpenReplaceMoveScreen(targetPlayer.getUniqueID(), entityPixelmon.getPokemonId(), a.getAttackBase().attackIndex, 0, entityPixelmon.getLvl().getLevel()), targetPlayer);
                } else {
                    for (int i = 0; i < entityPixelmon.getMoveset().size(); ++i) {
                        if (!entityPixelmon.getMoveset().get(i).getAttackBase().equals(a.getAttackBase())) continue;
                        ChatHandler.sendChat(entityPixelmon.getOwner(), "pixelmon.interaction.tmknown", entityPixelmon.getNickname(), a.getAttackBase().getLocalizedName());
                        return true;
                    }
                    entityPixelmon.getMoveset().add(a);
                    ChatHandler.sendChat(entityPixelmon.getOwner(), "pixelmon.stats.learnedmove", entityPixelmon.getNickname(), a.getAttackBase().getLocalizedName());
                    if (!player.capabilities.isCreativeMode && !((ItemTR)itemstack.getItem()).isTR || PixelmonConfig.allowTMReuse) {
                        player.getHeldItem(hand).shrink(1);
                    }
                }
                entityPixelmon.update(EnumUpdateType.Moveset);
            } else {
                ChatHandler.sendChat(entityPixelmon.getOwner(), "pixelmon.interaction.tmcantlearn", entityPixelmon.getNickname(), new TextComponentTranslation("attack." + ((ItemTM)itemstack.getItem()).attackName.toLowerCase() + ".name", new Object[0]));
            }
            return true;
        }
        return false;
    }
}

