/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumNecrozma;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.packetHandlers.OpenReplaceMoveScreen;
import com.pixelmongenerations.core.storage.ComputerBox;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;

public abstract class InteractionNBlankizer
implements IInteraction {
    private final Item item;
    private final String species;
    private final String move;
    private final int form;

    protected InteractionNBlankizer(Item item, String species, String move, int form) {
        this.item = item;
        this.species = species;
        this.move = move;
        this.form = form;
    }

    @Override
    public boolean processInteract(EntityPixelmon pixelmon, EntityPlayer player, EnumHand hand, ItemStack itemStack) {
        Item item = itemStack.getItem();
        if (pixelmon.getOwner() == player && item == this.item) {
            if (pixelmon.isPokemon(EnumSpecies.Necrozma) && pixelmon.getForm() != this.form) {
                PlayerStorage pstore = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player).get();
                boolean hasOtherNecrozmaForm = false;
                for (NBTTagCompound nbt : pstore.partyPokemon) {
                    hasOtherNecrozmaForm = hasOtherNecrozmaForm || nbt != null && nbt.getString("Name").equals("Necrozma") && nbt.getInteger("Variant") != 0;
                }
                for (ComputerBox box : PixelmonStorage.computerManager.getPlayerStorage((EntityPlayerMP)player).getBoxList()) {
                    for (NBTTagCompound nbt : box.getStoredPokemon()) {
                        hasOtherNecrozmaForm = hasOtherNecrozmaForm || nbt != null && nbt.getString("Name").equals("Necrozma") && nbt.getInteger("Variant") > 0;
                    }
                }
                if (pixelmon.getForm() == 0 && !hasOtherNecrozmaForm) {
                    for (int i = 0; i < pstore.partyPokemon.length; ++i) {
                        NBTTagCompound nbt = pstore.partyPokemon[i];
                        if (nbt == null || !nbt.getString("Name").equalsIgnoreCase(this.species)) continue;
                        pixelmon.setForm(EnumNecrozma.getFromPokemon(nbt.getString("Name")).getForm());
                        pixelmon.embed(nbt);
                        InteractionNBlankizer.learnMove(pixelmon, this.move);
                        pstore.recallAllPokemon();
                        pstore.removeFromPartyPlayer(i);
                        ChatHandler.sendChat(player, "pixelmon.abilities.changeform", pixelmon.getNickname());
                        return true;
                    }
                } else if (pixelmon.getForm() != 0) {
                    pixelmon.setForm(0);
                    for (NBTTagCompound nbt : pixelmon.embeddedPokemon) {
                        if (!nbt.getString("Name").equalsIgnoreCase(this.species)) continue;
                        pstore.addToParty((EntityPixelmon)PixelmonEntityList.createEntityFromNBT(nbt, player.getEntityWorld()));
                        pixelmon.embeddedPokemon.remove(nbt);
                        pixelmon.getMoveset().removeIf(a -> a.getAttackBase().getUnlocalizedName().equals(this.move));
                        pixelmon.update(EnumUpdateType.Moveset);
                        ChatHandler.sendChat(player, "pixelmon.necrozma.changeformback", pixelmon.getNickname());
                        return true;
                    }
                }
                ChatHandler.sendChat(player, "pixelmon.necrozma.fail", new Object[0]);
            }
        }
        return false;
    }

    public static void learnMove(EntityPixelmon p, String move) {
        Attack a = new Attack(move);
        if (!p.getMoveset().hasAttack(a)) {
            if (p.getMoveset().size() >= 4) {
                EntityPlayerMP player = (EntityPlayerMP)p.getOwner();
                Pixelmon.NETWORK.sendTo(new OpenReplaceMoveScreen(player.getUniqueID(), p.getPokemonId(), a.getAttackBase().attackIndex, 0, 0), player);
            } else {
                p.getMoveset().add(a);
                p.update(EnumUpdateType.Moveset);
            }
        }
    }
}

