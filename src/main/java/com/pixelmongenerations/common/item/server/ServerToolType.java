/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.server;

public enum ServerToolType {
    Pickaxe("pickaxe"),
    Axe("axe"),
    Shovel("shovel"),
    MultiTool("multitool"),
    None("none");

    private String toolName;

    private ServerToolType(String toolName) {
        this.toolName = toolName;
    }

    public String getName() {
        return this.toolName;
    }

    public static ServerToolType getToolType(short toolType) {
        if (toolType < ServerToolType.values().length) {
            return ServerToolType.values()[toolType];
        }
        return None;
    }
}

