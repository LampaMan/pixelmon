/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.items.EnumCustomIcon;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;

public class ItemCustomIcon
extends PixelmonItem {
    public ItemCustomIcon() {
        super("custom_icon");
        this.setCreativeTab(null);
    }

    public static ItemStack getIcon(EnumCustomIcon customIcon) {
        return ItemCustomIcon.getIcon(customIcon.getName());
    }

    public static ItemStack getIcon(String customIcon) {
        ItemStack itemStack = new ItemStack(PixelmonItems.itemCustomIcon);
        NBTTagCompound tagCompound = new NBTTagCompound();
        String displayName = I18n.translateToLocal("item.custom." + customIcon + ".name");
        tagCompound.setString("SpriteName", "pixelmon:customicon/" + customIcon);
        itemStack.setTagCompound(tagCompound);
        itemStack.setStackDisplayName((Object)((Object)TextFormatting.WHITE) + displayName);
        return itemStack;
    }
}

