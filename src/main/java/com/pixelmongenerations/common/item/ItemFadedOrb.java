/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.ItemShrineOrbWithInfo;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.List;
import net.minecraft.item.ItemStack;

public class ItemFadedOrb
extends ItemShrineOrbWithInfo {
    public static int full = 500;
    private final EnumType type;

    public ItemFadedOrb(String itemName, EnumSpecies species, EnumType type) {
        super(itemName, species, 500);
        this.type = type;
    }

    public EnumType getType() {
        return this.type;
    }

    @Override
    public boolean acceptsType(List<EnumType> type) {
        return type.contains((Object)this.type);
    }

    @Override
    public ItemStack getUsedForm() {
        return new ItemStack(this.getType() == EnumType.Water ? PixelmonItemsHeld.blueOrb : PixelmonItemsHeld.redOrb);
    }
}

