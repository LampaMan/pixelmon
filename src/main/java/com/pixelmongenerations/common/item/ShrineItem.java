/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.item;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.item.IShrineItem;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumForms;
import java.util.List;

public class ShrineItem extends PixelmonItem implements IShrineItem {

    private final PokemonGroup group;

    public ShrineItem(String name, EnumSpecies species) {
        this(name, new PokemonGroup.PokemonData(species, EnumForms.NoForm));
    }

    public ShrineItem(String name, PokemonGroup.PokemonData ... members) {
        this(name, Lists.newArrayList(members));
    }

    public ShrineItem(String name, List<PokemonGroup.PokemonData> members) {
        super(name);
        this.group = new PokemonGroup(members);
    }

    @Override
    public PokemonGroup getGroup() {
        return this.group;
    }
}

