/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.npcs.NPCDamos;
import com.pixelmongenerations.common.entity.npcs.NPCGroomer;
import com.pixelmongenerations.common.entity.npcs.NPCNurseJoy;
import com.pixelmongenerations.common.entity.npcs.NPCRelearner;
import com.pixelmongenerations.common.entity.npcs.NPCShopkeeper;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.network.ChatHandler;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemNPCEditor
extends PixelmonItem {
    public ItemNPCEditor() {
        super("trainer_editor");
        this.setCreativeTab(CreativeTabs.TOOLS);
        this.setMaxStackSize(1);
    }

    @Override
    public EnumActionResult onItemUseFirst(EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ, EnumHand hand) {
        if (side != EnumFacing.UP) {
            return EnumActionResult.FAIL;
        }
        if (!world.isRemote) {
            if (!ItemNPCEditor.checkPermission((EntityPlayerMP)player)) {
                return EnumActionResult.FAIL;
            }
            if (player.capabilities.isCreativeMode) {
                player.openGui(Pixelmon.INSTANCE, EnumGui.SelectNPCType.ordinal(), world, pos.getX(), pos.getY(), pos.getZ());
                return EnumActionResult.SUCCESS;
            }
        }
        return EnumActionResult.PASS;
    }

    @Override
    public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity) {
        if (player.world.isRemote) {
            return super.onLeftClickEntity(stack, player, entity);
        }
        if (entity instanceof NPCRelearner || entity instanceof NPCNurseJoy || entity instanceof NPCShopkeeper || entity instanceof NPCGroomer || entity instanceof NPCDamos) {
            entity.setDead();
            return true;
        }
        return super.onLeftClickEntity(stack, player, entity);
    }

    public static boolean checkPermission(EntityPlayerMP player) {
        if (player.canUseCommand(1, "pixelmon.npceditor.use")) {
            return true;
        }
        ChatHandler.sendChat(player, "pixelmon.general.needop", new Object[0]);
        return false;
    }
}

