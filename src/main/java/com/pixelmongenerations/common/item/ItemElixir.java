/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.WrapperLink;
import com.pixelmongenerations.common.item.ItemPPRestore;

public class ItemElixir
extends ItemPPRestore {
    public ItemElixir(String itemName, boolean allPP) {
        super(itemName, allPP);
    }

    public boolean useElixir(PokemonLink pokemon) {
        boolean success = false;
        for (int i = 0; i < pokemon.getMoveset().size(); ++i) {
            success = ItemElixir.restorePP(pokemon, i, this.allPP) || success;
        }
        String message = this.getMessage(success);
        String data = pokemon.getNickname();
        pokemon.sendMessage(message, data);
        return success;
    }

    @Override
    public boolean useFromBag(PixelmonWrapper userWrapper, PixelmonWrapper targetWrapper, int selectedMove) {
        this.useElixir(new WrapperLink(targetWrapper));
        return super.useFromBag(userWrapper, targetWrapper);
    }
}

