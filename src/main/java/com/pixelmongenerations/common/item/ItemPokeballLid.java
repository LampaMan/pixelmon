/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.items.EnumPokeball;

public class ItemPokeballLid
extends PixelmonItem {
    public EnumPokeball pokeball;

    public ItemPokeballLid(EnumPokeball type) {
        super(type.getFilenamePrefix() + "_lid");
        this.pokeball = type;
        this.maxStackSize = 64;
        this.setMaxDamage(1000000);
        this.setCreativeTab(PixelmonCreativeTabs.pokeball);
        this.canRepair = false;
    }
}

