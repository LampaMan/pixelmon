/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;

public class ItemPokePuff
extends PixelmonItem {
    private int friendship;

    public ItemPokePuff(String name, int friendship) {
        super(name);
        this.friendship = friendship;
    }

    public int getFriendShipAmount() {
        return this.friendship;
    }
}

