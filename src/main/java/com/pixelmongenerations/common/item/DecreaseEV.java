/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.heldItems.ItemBerry;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.enums.EnumDecreaseEV;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class DecreaseEV
extends ItemBerry {
    public EnumDecreaseEV type;

    public DecreaseEV(EnumDecreaseEV type, EnumBerry berry, String itemName) {
        super(EnumHeldItems.berryEVReducing, berry, itemName);
        this.type = type;
        this.setCreativeTab(PixelmonCreativeTabs.restoration);
        this.canRepair = false;
    }

    @Override
    public boolean interact(EntityPixelmon pokemon, ItemStack itemstack, EntityPlayer player) {
        return this.berryEVs(pokemon);
    }

    public boolean berryEVs(EntityPixelmon entityPixelmon) {
        boolean success = false;
        EntityLivingBase owner = entityPixelmon.getOwner();
        String nickname = entityPixelmon.getNickname();
        String statName = this.type.statAffected.getLocalizedName();
        if (entityPixelmon.stats.EVs.berryEVs(this.type.statAffected)) {
            ChatHandler.sendChat(owner, "pixelmon.interaction.berry", nickname, statName);
            success = true;
        }
        if (entityPixelmon.friendship.berryFriendship()) {
            ChatHandler.sendChat(owner, "pixelmon.interaction.berryfriend", nickname);
            success = true;
        }
        if (success) {
            entityPixelmon.updateStats();
            entityPixelmon.update(EnumUpdateType.Stats);
        } else {
            ChatHandler.sendChat(owner, "pixelmon.interaction.berryfail", nickname, statName);
        }
        return success;
    }
}

