/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Overcoat;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemSafetyGoggles
extends ItemHeld {
    public ItemSafetyGoggles() {
        super(EnumHeldItems.safetyGoggles, "safety_goggles");
    }

    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        if (Overcoat.isPowderMove(a)) {
            String nickname = pokemon.getNickname();
            pokemon.bc.sendToAll("pixelmon.abilities.activated", nickname, pokemon.getHeldItem().getLocalizedName());
            pokemon.bc.sendToAll("pixelmon.battletext.noeffect", nickname);
            return false;
        }
        return true;
    }
}

