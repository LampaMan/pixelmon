/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.heldItems.StatEnhancingItems;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemDittoEnhancing
extends StatEnhancingItems {
    public ItemDittoEnhancing(EnumHeldItems type, String itemName, StatsType[] raisedStats, float statMultiplier) {
        super(type, itemName, raisedStats, statMultiplier, EnumSpecies.Ditto);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    @Override
    protected boolean canAffect(PixelmonWrapper pokemon) {
        if (!super.canAffect(pokemon)) return false;
        if (pokemon.hasStatus(StatusType.Transformed)) return false;
        return true;
    }
}

