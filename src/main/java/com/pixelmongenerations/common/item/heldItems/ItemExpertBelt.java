/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemExpertBelt
extends ItemHeld {
    public ItemExpertBelt() {
        super(EnumHeldItems.expertBelt, "expert_belt");
    }

    @Override
    public double preProcessAttackUser(PixelmonWrapper attacker, PixelmonWrapper target, Attack attack, double damage) {
        if (attack.getTypeEffectiveness(attacker, target) >= 2.0) {
            damage *= 1.2;
        }
        return damage;
    }
}

