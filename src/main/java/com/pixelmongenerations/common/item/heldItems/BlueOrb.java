/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.HarshSunlight;
import com.pixelmongenerations.common.battle.status.HeavyRain;
import com.pixelmongenerations.common.entity.pixelmon.abilities.PrimordialSea;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumGroudon;
import com.pixelmongenerations.core.enums.forms.EnumKyogre;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import net.minecraft.util.text.TextComponentTranslation;

public class BlueOrb
extends ItemHeld {
    public BlueOrb() {
        super(EnumHeldItems.blueOrb, "blue_orb");
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.getSpecies() == EnumSpecies.Kyogre && newPokemon.getForm() != EnumKyogre.Primal.getForm()) {
            for (GlobalStatusBase globalStatusBase : newPokemon.bc.globalStatusController.getGlobalStatuses()) {
                if (!(globalStatusBase instanceof HarshSunlight)) continue;
                newPokemon.bc.globalStatusController.removeGlobalStatus(globalStatusBase);
                newPokemon.bc.sendToAll(new TextComponentTranslation("pixelmon.status.sunlightfaded", new Object[0]));
            }
            newPokemon.setTempAbility(new PrimordialSea(), true);
            newPokemon.primalEvolve();
        } else if (newPokemon.getSpecies() == EnumSpecies.Kyogre && newPokemon.getForm() == EnumGroudon.Primal.getForm()) {
            for (GlobalStatusBase globalStatusBase : newPokemon.bc.globalStatusController.getGlobalStatuses()) {
                if (!(globalStatusBase instanceof HarshSunlight)) continue;
                newPokemon.bc.globalStatusController.removeGlobalStatus(globalStatusBase);
                newPokemon.bc.sendToAll(new TextComponentTranslation("pixelmon.status.sunlightfaded", new Object[0]));
            }
            newPokemon.setTempAbility(new PrimordialSea(), true);
            newPokemon.primalEvolve();
        }
    }

    @Override
    public void applySwitchOutEffect(PixelmonWrapper pw) {
        for (GlobalStatusBase globalStatusBase : pw.bc.globalStatusController.getGlobalStatuses()) {
            if (!(globalStatusBase instanceof HeavyRain)) continue;
            pw.bc.globalStatusController.removeGlobalStatus(globalStatusBase);
            pw.bc.sendToAll(new TextComponentTranslation("pixelmon.status.rainstopped", new Object[0]));
        }
    }
}

