/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.heldItems.ItemRaiseStat;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemAbsorbBulb
extends ItemRaiseStat {
    public ItemAbsorbBulb() {
        super(EnumHeldItems.absorbbulb, "absorb_bulb", StatsType.SpecialAttack, EnumType.Water);
    }
}

