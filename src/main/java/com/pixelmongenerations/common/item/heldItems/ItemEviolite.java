/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.heldItems.StatEnhancingItems;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemEviolite
extends StatEnhancingItems {
    public ItemEviolite() {
        super(EnumHeldItems.eviolite, "eviolite", new StatsType[]{StatsType.Defence, StatsType.SpecialDefence}, 1.5f, new EnumSpecies[0]);
    }

    @Override
    protected boolean canAffect(PixelmonWrapper pokemon) {
        return pokemon.getBaseStats().evolutions.length > 0;
    }
}

