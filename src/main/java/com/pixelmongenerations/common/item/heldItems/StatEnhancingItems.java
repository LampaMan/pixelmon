/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class StatEnhancingItems
extends ItemHeld {
    private StatsType[] raisedStats;
    private float statMultiplier;
    private EnumSpecies[] affected;

    public StatEnhancingItems(EnumHeldItems type, String itemName, StatsType[] raisedStats, float statMultiplier, EnumSpecies ... affected) {
        super(type, itemName);
        this.raisedStats = raisedStats;
        this.statMultiplier = statMultiplier;
        this.affected = affected;
    }

    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        if (this.canAffect(user)) {
            for (StatsType stat : this.raisedStats) {
                int[] arrn = stats;
                int n = stat.getStatIndex();
                arrn[n] = (int)((float)arrn[n] * this.statMultiplier);
            }
        }
        return stats;
    }

    protected boolean canAffect(PixelmonWrapper pokemon) {
        EnumSpecies species = pokemon.getSpecies();
        for (EnumSpecies p : this.affected) {
            if (p != species) continue;
            return true;
        }
        return false;
    }
}

