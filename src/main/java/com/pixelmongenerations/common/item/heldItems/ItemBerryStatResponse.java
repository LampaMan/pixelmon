/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.heldItems.ItemBerry;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemBerryStatResponse
extends ItemBerry {
    private AttackCategory category;
    private StatsType stat;

    public ItemBerryStatResponse(EnumHeldItems type, EnumBerry berry, String name, AttackCategory category, StatsType stat) {
        super(type, berry, name);
        this.category = category;
        this.stat = stat;
    }

    @Override
    public void postProcessAttackTarget(PixelmonWrapper attacker, PixelmonWrapper target, Attack attack, float damage) {
        if (ItemBerryStatResponse.canEatBerry(target) && damage > 0.0f && attack.getAttackCategory() == this.category) {
            this.eatBerry(target);
            target.consumeItem();
        }
    }

    @Override
    public void eatBerry(PixelmonWrapper user) {
        user.bc.sendToAll("pixelmon.helditems.pinchberry", user.getNickname(), user.getHeldItem().getLocalizedName());
        user.getBattleStats().modifyStat(1, this.stat);
        super.eatBerry(user);
    }
}

