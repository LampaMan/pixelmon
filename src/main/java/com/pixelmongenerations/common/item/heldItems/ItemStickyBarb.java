/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemStickyBarb
extends ItemHeld {
    public ItemStickyBarb() {
        super(EnumHeldItems.stickyBarb, "sticky_barb");
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        if (pokemon.isAlive() && !(pokemon.getBattleAbility() instanceof MagicGuard)) {
            pokemon.bc.sendToAll("pixelmon.helditems.stickybarb", pokemon.getNickname());
            int damage = pokemon.getPercentMaxHealth(12.5f);
            pokemon.doBattleDamage(pokemon, damage, DamageTypeEnum.ITEM);
        }
    }

    @Override
    public void applyEffectOnContact(PixelmonWrapper user, PixelmonWrapper target) {
        if (!user.hasHeldItem()) {
            user.setHeldItem(target.getHeldItem());
            target.removeHeldItem();
        }
    }
}

