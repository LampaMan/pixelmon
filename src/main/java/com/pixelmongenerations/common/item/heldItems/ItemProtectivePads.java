/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.item.heldItems.HeldItem;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemProtectivePads
extends HeldItem {
    public ItemProtectivePads() {
        super(EnumHeldItems.protectivePads, "protective_pads");
    }
}

