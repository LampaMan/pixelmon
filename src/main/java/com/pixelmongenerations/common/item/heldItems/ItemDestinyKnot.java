/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Infatuated;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.item.heldItems.HeldItem;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemDestinyKnot
extends HeldItem {
    public ItemDestinyKnot() {
        super(EnumHeldItems.destinyKnot, "destiny_knot");
    }

    @Override
    public void onStatusAdded(PixelmonWrapper user, PixelmonWrapper opponent, StatusBase status) {
        if (status instanceof Infatuated) {
            if (!opponent.hasStatus(StatusType.Infatuated)) {
                opponent.addStatus(new Infatuated(user), user);
            }
        }
    }
}

