/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Burn;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemFlameOrb
extends ItemHeld {
    public ItemFlameOrb() {
        super(EnumHeldItems.flameOrb, "flame_orb");
    }

    @Override
    public void applyRepeatedEffectAfterStatus(PixelmonWrapper pokemon) {
        if (Burn.burn(pokemon, pokemon, null, false)) {
            pokemon.bc.sendToAll("pixelmon.helditems.flameorb", pokemon.getNickname());
        }
    }
}

