/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemPowerHerb
extends ItemHeld {
    public ItemPowerHerb() {
        super(EnumHeldItems.powerHerb, "power_herb");
    }

    @Override
    public boolean affectMultiturnMove(PixelmonWrapper user) {
        user.consumeItem();
        user.bc.sendToAll("pixelmon.helditems.powerherb", user.getNickname());
        return true;
    }
}

