/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemAssaultVest
extends ItemHeld {
    public ItemAssaultVest() {
        super(EnumHeldItems.assaultVest, "assault_vest");
    }

    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        int[] arrn = stats;
        int n = StatsType.SpecialDefence.getStatIndex();
        arrn[n] = (int)((double)arrn[n] * 1.5);
        return stats;
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        this.applyRepeatedEffect(newPokemon);
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        Moveset moveset = pokemon.getMoveset();
        for (int i = 0; i < moveset.size(); ++i) {
            Attack currentAttack = moveset.get(i);
            if (currentAttack.getAttackCategory() != AttackCategory.Status) continue;
            currentAttack.setDisabled(true, pokemon);
        }
    }
}

