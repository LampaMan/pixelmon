/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.PoisonBadly;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemToxicOrb
extends ItemHeld {
    public ItemToxicOrb() {
        super(EnumHeldItems.toxicOrb, "toxic_orb");
    }

    @Override
    public void applyRepeatedEffectAfterStatus(PixelmonWrapper pokemon) {
        if (PoisonBadly.poisonBadly(pokemon, pokemon, null, false)) {
            pokemon.bc.sendToAll("pixelmon.helditems.toxicorb", pokemon.getNickname());
        }
    }
}

