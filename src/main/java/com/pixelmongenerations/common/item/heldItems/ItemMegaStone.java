/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemMegaStone
extends ItemHeld {
    public EnumSpecies pokemon;
    public int form;

    public ItemMegaStone(String itemName, EnumSpecies pokemon) {
        this(itemName, pokemon, 1);
    }

    public ItemMegaStone(String itemName, EnumSpecies pokemon, int form) {
        super(EnumHeldItems.megaStone, itemName);
        this.pokemon = pokemon;
        this.form = form;
        if (!pokemon.hasMega()) {
            this.pokemon = null;
            this.setCreativeTab(null);
        }
    }
}

