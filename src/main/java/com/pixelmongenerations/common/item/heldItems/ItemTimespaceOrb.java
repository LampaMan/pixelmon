/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.IShrineItem;
import com.pixelmongenerations.common.item.heldItems.TypeEnhancingItems;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.forms.EnumForms;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import com.pixelmongenerations.core.enums.heldItems.EnumTypeEnhancingItems;

public class ItemTimespaceOrb
extends TypeEnhancingItems
implements IShrineItem {
    PokemonGroup group;

    public ItemTimespaceOrb(String itemName, EnumType type, EnumSpecies pokemon) {
        super(EnumTypeEnhancingItems.orb, itemName, type);
        this.group = new PokemonGroup(pokemon, (IEnumForm)EnumForms.NoForm);
    }

    @Override
    public double preProcessAttackUser(PixelmonWrapper attacker, PixelmonWrapper target, Attack attack, double damage) {
        if (this.group.contains(attacker.getSpecies())) {
            if (attack.getAttackBase().attackType == EnumType.Dragon) {
                return damage * 1.2;
            }
            if (this.type != null && attack.getAttackBase().attackType == this.type) {
                return damage * 1.2;
            }
        }
        return damage;
    }

    @Override
    public PokemonGroup getGroup() {
        return this.group;
    }
}

