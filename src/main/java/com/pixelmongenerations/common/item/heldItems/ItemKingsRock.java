/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Flinch;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SereneGrace;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class ItemKingsRock
extends ItemHeld {
    protected int randomInt = 0;

    public ItemKingsRock() {
        super(EnumHeldItems.kingsRock, "kings_rock");
    }

    @Override
    public void postProcessAttackUser(PixelmonWrapper attacker, PixelmonWrapper target, Attack attack, float damage) {
        this.randomInt = RandomHelper.rand.nextInt(9) + 1;
        attack.getAttackBase().effects.stream().filter(effect -> effect instanceof Flinch).forEach(effect -> {
            this.randomInt = 0;
        });
        if (attacker.getBattleAbility() instanceof SereneGrace && this.randomInt == 4) {
            this.randomInt = 5;
        }
        if (this.randomInt == 5) {
            Flinch.flinch(attacker, target);
        }
    }
}

