/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.item.heldItems.TypeEnhancingItems;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumTypeEnhancingItems;

public class ItemPlate
extends TypeEnhancingItems {
    public ItemPlate(String name, EnumType type) {
        super(EnumTypeEnhancingItems.plate, name, type);
    }
}

