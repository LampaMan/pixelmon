/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemBrightPowder
extends ItemHeld {
    public ItemBrightPowder(EnumHeldItems itemEnum, String itemName) {
        super(itemEnum, itemName);
    }

    @Override
    public int[] modifyPowerAndAccuracyTarget(int[] modifiedMoveStats, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (modifiedMoveStats[1] > 0) {
            modifiedMoveStats[1] = (int)((double)modifiedMoveStats[1] * 0.9090909090909091);
        }
        return modifiedMoveStats;
    }
}

