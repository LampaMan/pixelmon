/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.EntityLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.WrapperLink;
import com.pixelmongenerations.common.item.IMedicine;
import com.pixelmongenerations.common.item.MedicineStatus;
import com.pixelmongenerations.common.item.heldItems.ItemBerry;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.enums.heldItems.EnumBerryStatus;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class ItemBerryStatus
extends ItemBerry {
    public EnumBerryStatus berryType;
    private StatusType[] statusHealed;
    private IMedicine healMethod;

    public ItemBerryStatus(EnumBerryStatus berryType, EnumBerry berry, String itemName, StatusType ... statusHealed) {
        super(EnumHeldItems.berryStatus, berry, itemName);
        this.berryType = berryType;
        this.healMethod = new MedicineStatus(statusHealed);
        this.statusHealed = statusHealed;
    }

    @Override
    public boolean interact(EntityPixelmon pokemon, ItemStack itemstack, EntityPlayer player) {
        if (this.healMethod.useMedicine(new EntityLink(pokemon))) {
            this.consumeItem(player, itemstack);
            return true;
        }
        return false;
    }

    @Override
    public void onStatusAdded(PixelmonWrapper user, PixelmonWrapper opponent, StatusBase status) {
        if (user.isFainted()) {
            return;
        }
        this.eatBerry(user);
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        this.eatBerry(newPokemon);
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        this.applySwitchInEffect(pw);
    }

    @Override
    public void eatBerry(PixelmonWrapper pokemon) {
        if (ItemBerryStatus.canEatBerry(pokemon) && this.healStatus(pokemon)) {
            super.eatBerry(pokemon);
            pokemon.consumeItem();
        }
    }

    @Override
    public boolean useFromBag(PixelmonWrapper userWrapper, PixelmonWrapper targetWrapper) {
        if (!this.healMethod.useMedicine(new WrapperLink(targetWrapper))) {
            targetWrapper.bc.sendToAll("pixelmon.general.noeffect", new Object[0]);
        }
        return super.useFromBag(userWrapper, targetWrapper);
    }

    public boolean healStatus(PixelmonWrapper pokemon) {
        pokemon.eatingBerry = true;
        boolean healed = this.healMethod.useMedicine(new WrapperLink(pokemon));
        pokemon.eatingBerry = false;
        return healed;
    }

    public boolean canHealStatus(StatusType status) {
        for (StatusType type : this.statusHealed) {
            if (type != status) continue;
            return true;
        }
        return false;
    }
}

