/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.enums.heldItems.EnumTypeEnhancingItems;

public class TypeEnhancingItems
extends ItemHeld {
    public EnumTypeEnhancingItems enhanceType;
    protected EnumType type;

    public TypeEnhancingItems(EnumTypeEnhancingItems EnhanceType, String itemName, EnumType type) {
        super(EnumHeldItems.typeEnhancer, itemName);
        this.enhanceType = EnhanceType;
        this.type = type;
    }

    @Override
    public double preProcessAttackUser(PixelmonWrapper attacker, PixelmonWrapper target, Attack attack, double damage) {
        if (attack.getAttackBase().attackType == this.type) {
            return damage * 1.2;
        }
        return damage;
    }

    public EnumType getType() {
        return this.type;
    }
}

