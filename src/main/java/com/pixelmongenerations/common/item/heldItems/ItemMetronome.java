/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemMetronome
extends ItemHeld {
    public ItemMetronome() {
        super(EnumHeldItems.metronome, "metronome");
    }

    @Override
    public void onAttackUsed(PixelmonWrapper user, Attack attack) {
        if (!user.bc.simulateMode) {
            if (attack.equals(user.choiceLocked)) {
                user.metronomeBoost = Math.min(user.metronomeBoost + 20, 100);
            } else {
                user.choiceLocked = attack;
                user.metronomeBoost = 0;
            }
        }
    }

    @Override
    public double preProcessAttackUser(PixelmonWrapper attacker, PixelmonWrapper target, Attack attack, double damage) {
        if (attacker == target) {
            return damage;
        }
        return damage + damage / 100.0 * (double)attacker.metronomeBoost;
    }

    @Override
    public void onMiss(PixelmonWrapper attacker, PixelmonWrapper target, Object cause) {
        if (!attacker.bc.simulateMode) {
            this.reset(attacker);
        }
    }

    @Override
    public void applySwitchOutEffect(PixelmonWrapper pw) {
        if (!pw.bc.simulateMode) {
            this.reset(pw);
        }
    }

    private void reset(PixelmonWrapper pw) {
        pw.metronomeBoost = 0;
        pw.choiceLocked = null;
    }
}

