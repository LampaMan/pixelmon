/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumChoiceItems;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ChoiceItem
extends ItemHeld {
    private final EnumChoiceItems choiceItemType;

    public ChoiceItem(EnumChoiceItems choiceItemType, String itemName) {
        super(EnumHeldItems.choiceItem, itemName);
        this.choiceItemType = choiceItemType;
    }

    @Override
    public void onAttackUsed(PixelmonWrapper user, Attack attack) {
        if (!user.bc.simulateMode) {
            if (user.attack.isAttack("Transform")) {
                return;
            }
            if (user.isDynamaxed()) {
                user.choiceSwapped = false;
                user.choiceLocked = null;
                return;
            }
            user.choiceLocked = attack;
        }
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper pw) {
        if (!pw.bc.simulateMode && !pw.choiceSwapped) {
            pw.choiceLocked = null;
        }
        pw.choiceSwapped = false;
    }

    @Override
    public void applySwitchOutEffect(PixelmonWrapper pw) {
        if (!pw.bc.simulateMode && !pw.choiceSwapped) {
            pw.choiceLocked = null;
        }
    }

    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        if (user.isDynamaxed()) {
            return stats;
        }
        int n = this.choiceItemType.effectType.getStatIndex();
        stats[n] = (int)((double)stats[n] * 1.5);
        return stats;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        Moveset moveset = pokemon.getMoveset();
        if (pokemon.isDynamaxed()) {
            return;
        }
        for (Attack currentMove : moveset) {
            if (pokemon.choiceLocked == null || currentMove.equals(pokemon.choiceLocked)) continue;
            currentMove.setDisabled(true, pokemon);
        }
    }
}

