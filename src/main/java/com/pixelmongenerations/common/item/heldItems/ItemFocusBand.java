/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class ItemFocusBand
extends ItemHeld {
    public ItemFocusBand() {
        super(EnumHeldItems.focusband, "focus_band");
    }

    @Override
    public double modifyDamageIncludeFixed(double damage, PixelmonWrapper attacker, PixelmonWrapper target, Attack attack) {
        if (target != null && attacker != null && damage >= (double)target.getHealth() && RandomHelper.getRandomChance(10)) {
            if (attacker.bc != null) {
                attacker.bc.sendToAll("pixelmon.helditems.focusband", target.getNickname());
            }
            return target.getHealth() - 1;
        }
        return damage;
    }
}

