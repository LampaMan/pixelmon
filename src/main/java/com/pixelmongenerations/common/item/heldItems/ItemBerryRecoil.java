/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;
import com.pixelmongenerations.common.item.heldItems.ItemBerry;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemBerryRecoil
extends ItemBerry {
    private AttackCategory attackCategory;

    public ItemBerryRecoil(EnumHeldItems berryType, EnumBerry berry, String itemName, AttackCategory attackCategory) {
        super(berryType, berry, itemName);
        this.attackCategory = attackCategory;
    }

    @Override
    public void postProcessAttackTarget(PixelmonWrapper attacker, PixelmonWrapper target, Attack attack, float damage) {
        if (ItemBerryRecoil.canEatBerry(target) && damage > 0.0f && attack.getAttackCategory() == this.attackCategory && attacker.isAlive() && !(attacker.getBattleAbility() instanceof MagicGuard)) {
            attacker.bc.sendToAll("pixelmon.helditems.recoilberry", target.getNickname(), target.getHeldItem().getLocalizedName(), attacker.getNickname());
            attacker.doBattleDamage(target, attacker.getPercentMaxHealth(12.5f), DamageTypeEnum.ITEM);
            this.eatBerry(target);
            target.consumeItem();
        }
    }
}

