/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.heldItems.HeldItem;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemHeavyDutyBoots
extends HeldItem {
    public ItemHeavyDutyBoots() {
        super(EnumHeldItems.heavyDutyBoots, "heavyduty_boots");
    }

    @Override
    public double preProcessAttackTarget(PixelmonWrapper attacker, PixelmonWrapper target, Attack attack, double damage) {
        return damage;
    }
}

