/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.enums.heldItems.EnumIncenses;

public class ItemIncensePowerIncrease
extends ItemHeld {
    EnumType type;

    public ItemIncensePowerIncrease(EnumIncenses IncenseType, String itemName, EnumType type) {
        super(EnumHeldItems.incense, itemName);
        this.type = type;
    }

    @Override
    public double preProcessAttackUser(PixelmonWrapper attacker, PixelmonWrapper target, Attack attack, double damage) {
        if (damage > 0.0 && this.type != null && attack.getAttackBase().attackType == this.type) {
            return damage * 1.2;
        }
        return damage;
    }
}

