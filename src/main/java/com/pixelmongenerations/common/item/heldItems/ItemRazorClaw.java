/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemRazorClaw
extends ItemHeld {
    public ItemRazorClaw(EnumHeldItems heldItemType, String itemName) {
        super(heldItemType, itemName);
    }

    @Override
    public int adjustCritStage(PixelmonWrapper user) {
        return 1;
    }
}

