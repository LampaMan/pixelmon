/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.heldItems.HeldItem;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public abstract class SpawningAffectingHeldItem
extends HeldItem {
    public SpawningAffectingHeldItem(EnumHeldItems heldItemType, String itemName) {
        super(heldItemType, itemName);
    }

    public abstract void modifySpawn(EntityPixelmon var1);
}

