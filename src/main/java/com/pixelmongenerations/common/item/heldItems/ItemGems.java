/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.Pledge;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemGems
extends ItemHeld {
    private EnumType type;

    public ItemGems(String itemName, EnumType type) {
        super(EnumHeldItems.gems, itemName);
        this.type = type;
    }

    @Override
    public double preProcessAttackUser(PixelmonWrapper attacker, PixelmonWrapper target, Attack attack, double damage) {
        if (damage > 0.0 && attack.getAttackBase().attackType == this.type) {
            if (!attack.isAttack("Struggle")) {
                for (EffectBase effect : attack.getAttackBase().effects) {
                    if (!(effect instanceof Pledge)) continue;
                    return damage;
                }
                attacker.bc.sendToAll("pixelmon.helditems.gem", attacker.getNickname(), attacker.getHeldItem().getLocalizedName());
                attacker.consumeItem();
                return damage * 1.3;
            }
        }
        return damage;
    }
}

