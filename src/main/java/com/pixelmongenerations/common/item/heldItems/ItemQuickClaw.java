/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class ItemQuickClaw
extends ItemHeld {
    public ItemQuickClaw() {
        super(EnumHeldItems.quickClaw, "quick_claw");
    }

    @Override
    public float modifyPriority(PixelmonWrapper pokemon, float priority) {
        if (RandomHelper.getRandomChance(20)) {
            pokemon.bc.sendToAll("pixelmon.helditems.quickclaw", pokemon.getNickname());
            priority += 0.2f;
        }
        return priority;
    }
}

