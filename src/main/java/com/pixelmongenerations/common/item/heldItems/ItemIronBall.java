/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Levitate;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.List;

public class ItemIronBall
extends ItemHeld {
    public ItemIronBall() {
        super(EnumHeldItems.ironBall, "iron_ball");
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        block6: {
            block5: {
                if (newPokemon.removeStatuses(StatusType.MagnetRise, StatusType.Telekinesis)) break block5;
                if (!newPokemon.hasType(EnumType.Flying) && !(newPokemon.getBattleAbility() instanceof Levitate)) break block6;
            }
            if (!newPokemon.bc.globalStatusController.hasStatus(StatusType.Gravity)) {
                if (!newPokemon.hasStatus(StatusType.SmackedDown)) {
                    newPokemon.bc.sendToAll("pixelmon.helditems.ironball", newPokemon.getNickname());
                }
            }
        }
    }

    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        int[] arrn = stats;
        int n = StatsType.Speed.getStatIndex();
        arrn[n] = (int)((double)arrn[n] * 0.5);
        return stats;
    }

    @Override
    public List<EnumType> getEffectiveTypes(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.attack != null && user.attack.getAttackBase().attackType == EnumType.Ground) {
            return EnumType.ignoreType(target.type, EnumType.Flying);
        }
        return target.type;
    }
}

