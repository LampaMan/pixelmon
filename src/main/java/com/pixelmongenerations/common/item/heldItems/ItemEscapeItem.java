/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.item.heldItems.HeldItem;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemEscapeItem
extends HeldItem {
    public ItemEscapeItem(EnumHeldItems itemEnum, String itemName) {
        super(itemEnum, itemName);
    }
}

