/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nonnull
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.api.events.BerryEvent;
import com.pixelmongenerations.common.block.BlockBerryTree;
import com.pixelmongenerations.common.block.apricornTrees.BlockApricornTree;
import com.pixelmongenerations.common.block.enums.EnumBlockPos;
import com.pixelmongenerations.common.block.tileEntities.TileEntityApricornTree;
import com.pixelmongenerations.common.block.tileEntities.TileEntityBerryTree;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.enums.EnumBerryQuality;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagInt;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemBerry
extends ItemHeld
implements IPlantable {
    private EnumBerry berry;

    public ItemBerry(EnumHeldItems heldItemType, EnumBerry berry, String itemName) {
        super(heldItemType, itemName);
        this.berry = berry;
    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer playerIn, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        ItemStack stack = playerIn.getHeldItem(hand);
        if (worldIn.isRemote || this.berry == null) {
            return EnumActionResult.SUCCESS;
        }
        if (!PixelmonConfig.allowPlanting) {
            return EnumActionResult.FAIL;
        }
        if (!this.berry.isImplemented) {
            return EnumActionResult.FAIL;
        }
        Block groundBlock = worldIn.getBlockState(pos).getBlock();
        if (facing != EnumFacing.UP || !BlockApricornTree.isSuitableSoil(groundBlock)) {
            return EnumActionResult.FAIL;
        }
        int count = BlockHelper.countTileEntitiesOfType(worldIn, new ChunkPos(pos), TileEntityApricornTree.class);
        if ((count += BlockHelper.countTileEntitiesOfType(worldIn, new ChunkPos(pos), TileEntityBerryTree.class)) >= PixelmonConfig.maximumPlants) {
            ChatHandler.sendChat(playerIn, "pixelmon.blocks.maxPlants", new Object[0]);
            return EnumActionResult.FAIL;
        }
        pos = pos.up();
        Block berryTreeBlock = this.berry.getTreeBlock();
        if (!playerIn.canPlayerEdit(pos, facing, stack)) {
            return EnumActionResult.FAIL;
        }
        if (stack.getCount() <= 0) {
            return EnumActionResult.FAIL;
        }
        if (worldIn.mayPlace(berryTreeBlock, pos, false, facing, null)) {
            BerryEvent.BerryPlantedEvent plantEvent = new BerryEvent.BerryPlantedEvent(this.berry, pos, (EntityPlayerMP)playerIn);
            if (MinecraftForge.EVENT_BUS.post(plantEvent)) {
                return EnumActionResult.FAIL;
            }
            IBlockState state = berryTreeBlock.getDefaultState().withProperty(BlockBerryTree.BLOCKPOS, EnumBlockPos.BOTTOM);
            worldIn.setBlockState(pos, state, 3);
            state = worldIn.getBlockState(pos);
            if (state.getBlock() == berryTreeBlock) {
                ItemBlock.setTileEntityNBT(worldIn, playerIn, pos, stack);
                state.getBlock().onBlockPlacedBy(worldIn, pos, state, playerIn, stack);
            }
            worldIn.playSound(null, (double)pos.getX() + 0.5, (double)pos.getY() + 0.5, (double)pos.getZ() + 0.5, SoundEvents.BLOCK_GRASS_STEP, SoundCategory.PLAYERS, 0.5f, 1.0f);
            if (!playerIn.isCreative()) {
                if (stack.getCount() <= 1) {
                    playerIn.setHeldItem(hand, ItemStack.EMPTY);
                } else {
                    stack.shrink(1);
                }
            }
            return EnumActionResult.SUCCESS;
        }
        return EnumActionResult.PASS;
    }

    @Override
    public EnumPlantType getPlantType(IBlockAccess world, BlockPos pos) {
        return EnumPlantType.Plains;
    }

    @Override
    public IBlockState getPlant(IBlockAccess world, BlockPos pos) {
        Block berryTreeBlock = this.berry.getTreeBlock();
        return berryTreeBlock.getDefaultState();
    }

    public EnumBerry getBerry() {
        return this.berry;
    }

    public static void setQuality(@Nonnull ItemStack stack, @Nonnull EnumBerryQuality quality) {
        stack.setTagInfo("quality", new NBTTagInt(quality.getIndex()));
    }

    public static EnumBerryQuality getQuality(@Nonnull ItemStack stack) {
        if (stack.getTagCompound() == null) {
            stack.setTagInfo("quality", new NBTTagInt(0));
        }
        return EnumBerryQuality.getFromIndex(stack.getTagCompound().getInteger("quality"));
    }

    @Override
    public boolean getShareTag() {
        return true;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        String info = I18n.translateToLocal("gui.shopkeeper." + this.getTranslationKey());
        if (!this.hasHideFlag(stack) && !info.startsWith("gui.shopkeeper.")) {
            if (GuiScreen.isShiftKeyDown()) {
                tooltip.add(info);
                tooltip.add("Quality: " + ItemBerry.getQuality(stack).getName());
            } else {
                tooltip.add("Hold shift for more info.");
            }
        }
    }

    public int getRarity() {
        return this.berry.getRarity();
    }
}

