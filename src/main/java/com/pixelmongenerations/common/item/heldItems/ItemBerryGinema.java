/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.heldItems.ItemBerry;
import com.pixelmongenerations.common.item.heldItems.ItemWhiteHerb;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemBerryGinema
extends ItemBerry {
    public ItemBerryGinema() {
        super(EnumHeldItems.ginemaBerry, EnumBerry.Ginema, "ginema_berry");
    }

    @Override
    public void onStatModified(PixelmonWrapper affected) {
        if (ItemBerryGinema.canEatBerry(affected)) {
            this.eatBerry(affected);
        }
    }

    @Override
    public void eatBerry(PixelmonWrapper affected) {
        if (ItemWhiteHerb.healStats(affected)) {
            super.eatBerry(affected);
            affected.consumeItem();
        }
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        this.onStatModified(newPokemon);
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        this.onStatModified(pw);
    }

    @Override
    public boolean useFromBag(PixelmonWrapper userWrapper, PixelmonWrapper targetWrapper) {
        if (!ItemWhiteHerb.healStats(userWrapper)) {
            userWrapper.bc.sendToAll("pixelmon.general.noeffect", new Object[0]);
        }
        return super.useFromBag(userWrapper, targetWrapper);
    }
}

