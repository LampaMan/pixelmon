/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.TrickRoom;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemRoomService
extends ItemHeld {
    public ItemRoomService() {
        super(EnumHeldItems.roomservice, "room_service");
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        for (GlobalStatusBase globalStatusBase : newPokemon.bc.globalStatusController.getGlobalStatuses()) {
            if (!(globalStatusBase instanceof TrickRoom)) continue;
            newPokemon.getBattleStats().modifyStat(-1, StatsType.Speed, newPokemon, false);
            newPokemon.bc.sendToAll("pixelmon.roomservice.activated", new Object[0]);
        }
    }
}

