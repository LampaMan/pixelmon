/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemAirBalloon
extends ItemHeld {
    public ItemAirBalloon() {
        super(EnumHeldItems.airBalloon, "air_balloon");
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        newPokemon.bc.sendToAll("pixelmon.helditems.airballoon", newPokemon.getNickname());
    }

    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        if (a.getAttackBase().attackType.equals((Object)EnumType.Ground) && a.getAttackCategory() != AttackCategory.Status && !pokemon.isGrounded()) {
            if (!a.isAttack("Thousand Arrows")) {
                user.bc.sendToAll("pixelmon.battletext.noeffect", pokemon.getNickname());
                return false;
            }
        }
        return true;
    }

    @Override
    public void postProcessAttackTarget(PixelmonWrapper attacker, PixelmonWrapper holder, Attack attack, float damage) {
        if (damage > 0.0f) {
            holder.removeHeldItem();
            attacker.bc.sendToAll("pixelmon.helditems.airballoonpop", holder.getNickname());
        }
    }
}

