/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Gluttony;
import com.pixelmongenerations.common.item.heldItems.ItemBerry;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemBerryCustap
extends ItemBerry {
    public ItemBerryCustap() {
        super(EnumHeldItems.berryCustap, EnumBerry.Custap, "custap_berry");
    }

    @Override
    public float modifyPriority(PixelmonWrapper pokemon, float priority) {
        float lowHealth = pokemon.getBattleAbility() instanceof Gluttony ? 50.0f : 25.0f;
        float f = lowHealth;
        if (pokemon.getHealthPercent() <= lowHealth && ItemBerryCustap.canEatBerry(pokemon)) {
            priority += 0.2f;
            pokemon.consumeItem();
            pokemon.bc.sendToAll("pixelmon.helditems.custapberry", pokemon.getNickname());
            super.eatBerry(pokemon);
        }
        return priority;
    }

    @Override
    public void eatBerry(PixelmonWrapper user) {
    }
}

