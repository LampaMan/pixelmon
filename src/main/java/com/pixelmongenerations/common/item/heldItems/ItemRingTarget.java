/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Effectiveness;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.List;

public class ItemRingTarget
extends ItemHeld {
    public ItemRingTarget() {
        super(EnumHeldItems.ringTarget, "ring_target");
    }

    @Override
    public List<EnumType> getEffectiveTypes(PixelmonWrapper user, PixelmonWrapper target) {
        if (user != null && user.attack != null) {
            EnumType attackingType = user.attack.getAttackBase().attackType;
            for (EnumType targetType : target.type) {
                if (EnumType.getEffectiveness(attackingType, targetType) != Effectiveness.None.value) continue;
                return EnumType.ignoreType(target.type, targetType);
            }
        }
        return target.type;
    }
}

