/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class SoulDew
extends ItemHeld {
    private EnumType[] types = new EnumType[]{EnumType.Psychic, EnumType.Dragon};
    private EnumSpecies[] pokemons = new EnumSpecies[]{EnumSpecies.Latias, EnumSpecies.Latios};

    public SoulDew() {
        super(EnumHeldItems.soulDew, "soul_dew");
    }

    @Override
    public double preProcessAttackUser(PixelmonWrapper attacker, PixelmonWrapper target, Attack attack, double damage) {
        for (EnumType type : this.types) {
            if (attack.getAttackBase().attackType != type) continue;
            for (EnumSpecies pokemon : this.pokemons) {
                if (attacker.getSpecies() != pokemon) continue;
                return damage * 1.2;
            }
        }
        return damage;
    }
}

