/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SheerForce;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemShellBell
extends ItemHeld {
    public ItemShellBell() {
        super(EnumHeldItems.shellBell, "shell_bell");
    }

    @Override
    public void postProcessAttackUser(PixelmonWrapper holder, PixelmonWrapper defender, Attack attack, float damage) {
        if (damage > 0.0f && !holder.hasFullHealth()) {
            if (!holder.hasStatus(StatusType.HealBlock)) {
                if (holder.getBattleAbility() instanceof SheerForce && !attack.getAttackBase().hasSecondaryEffect()) {
                    return;
                }
                int healAmount = Math.max(1, (int)(damage / 8.0f));
                holder.bc.sendToAll("pixelmon.helditems.shellbell", holder.getNickname());
                holder.healEntityBy(healAmount);
            }
        }
    }
}

