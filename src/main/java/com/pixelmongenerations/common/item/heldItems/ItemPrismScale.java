/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemPrismScale
extends ItemHeld {
    public ItemPrismScale() {
        super(EnumHeldItems.prismScale, "prism_scale");
    }
}

