/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemLaggingTail
extends ItemHeld {
    public ItemLaggingTail(EnumHeldItems itemType, String itemName) {
        super(itemType, itemName);
    }

    @Override
    public float modifyPriority(PixelmonWrapper pokemon, float priority) {
        return priority - 0.2f;
    }
}

