/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemMail
extends ItemHeld {
    public ItemMail(String name) {
        super(EnumHeldItems.mail, "pokemail_" + name);
        this.maxStackSize = 64;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public boolean hasEffect(ItemStack itemStack) {
        return this.isMailSealed(itemStack);
    }

    private boolean isMailSealed(ItemStack itemStack) {
        return itemStack.hasTagCompound() && itemStack.getTagCompound().hasKey("editable") && !itemStack.getTagCompound().getBoolean("editable");
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer player, EnumHand hand) {
        ItemStack stack = player.getHeldItem(hand);
        if (!worldIn.isRemote && hand == EnumHand.MAIN_HAND) {
            player.openGui(Pixelmon.INSTANCE, EnumGui.Mail.getIndex(), worldIn, 0, 0, 0);
        }
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, stack);
    }
}

