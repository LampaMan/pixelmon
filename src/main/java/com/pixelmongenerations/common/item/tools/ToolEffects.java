/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.tools;

import net.minecraft.block.Block;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Enchantments;
import net.minecraft.init.MobEffects;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ToolEffects {
    public static boolean processEffect(ItemStack stack, EntityPlayer player, World world, BlockPos pos) {
        String toolName = stack.getTranslationKey().toLowerCase();
        Block blockAtPosition = world.getBlockState(pos).getBlock();
        if (toolName.contains("fire")) {
            if (blockAtPosition.equals(Blocks.WATER)) {
                world.setBlockState(pos, Blocks.OBSIDIAN.getDefaultState());
                stack.damageItem(1, player);
                return true;
            }
        } else if (toolName.contains("water")) {
            if (blockAtPosition.equals(Blocks.LAVA)) {
                world.setBlockState(pos, Blocks.OBSIDIAN.getDefaultState());
                stack.damageItem(1, player);
                return true;
            }
        } else if (toolName.contains("leaf")) {
            if (ItemDye.applyBonemeal(new ItemStack(stack.getItem()), world, pos.down(), player, null)) {
                if (!world.isRemote) {
                    world.playEvent(2005, pos.down(), 0);
                }
                stack.damageItem(12, player);
                return true;
            }
        } else if (toolName.contains("thunder")) {
            NBTTagList list = stack.getEnchantmentTagList();
            boolean hasSameEfficiency = false;
            short efficiency = 3;
            for (int i = 0; list.tagCount() > i; ++i) {
                NBTTagCompound tag = (NBTTagCompound)list.get(i);
                if (tag.getShort("id") != Enchantment.getEnchantmentID(Enchantments.EFFICIENCY)) continue;
                if (tag.getShort("lvl") == efficiency) {
                    hasSameEfficiency = true;
                    break;
                }
                list.removeTag(i);
                break;
            }
            if (!hasSameEfficiency) {
                stack.addEnchantment(Enchantments.EFFICIENCY, efficiency);
                stack.damageItem(1, player);
                return true;
            }
        } else if (toolName.contains("sun")) {
            if (Blocks.DIRT.canPlaceBlockAt(world, pos) && Blocks.TORCH.canPlaceBlockAt(world, pos)) {
                world.setBlockState(pos, Blocks.TORCH.getDefaultState());
                stack.damageItem(5, player);
                return true;
            }
        } else {
            if (toolName.contains("moon")) {
                player.addPotionEffect(new PotionEffect(MobEffects.NIGHT_VISION, 6000, 0, true, true));
                stack.damageItem(1, player);
                return true;
            }
            if (toolName.contains("dawn")) {
                player.addPotionEffect(new PotionEffect(MobEffects.HEALTH_BOOST, 6000, 0, true, true));
                stack.damageItem(1, player);
                return true;
            }
            if (toolName.contains("dusk")) {
                player.addPotionEffect(new PotionEffect(MobEffects.INVISIBILITY, 6000, 0, true, true));
                stack.damageItem(1, player);
                return true;
            }
        }
        return false;
    }
}

