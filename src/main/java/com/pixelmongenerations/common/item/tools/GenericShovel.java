/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.item.tools;

import com.pixelmongenerations.common.item.tools.ToolEffects;
import java.util.List;
import javax.annotation.Nullable;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class GenericShovel
extends ItemSpade {
    public GenericShovel(Item.ToolMaterial material, String itemName) {
        super(material);
        this.setTranslationKey(itemName);
        this.setRegistryName(itemName);
    }

    @Override
    public String getTranslationKey() {
        return super.getTranslationKey().substring(5);
    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer playerIn, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        EnumActionResult result = super.onItemUse(playerIn, worldIn, pos, hand, facing, hitX, hitY, hitZ);
        return result == EnumActionResult.SUCCESS ? EnumActionResult.SUCCESS : (ToolEffects.processEffect(playerIn.getHeldItem(hand), playerIn, worldIn, pos.up()) ? EnumActionResult.SUCCESS : EnumActionResult.FAIL);
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        String info = I18n.translateToLocal("gui.shopkeeper.item." + this.getTranslationKey());
        if (!this.hasHideFlag(stack) && !info.startsWith("gui.shopkeeper.")) {
            tooltip.add(GuiScreen.isShiftKeyDown() ? info : I18n.translateToLocal("pixelmon.item.tooltip"));
        }
    }

    public boolean hasHideFlag(ItemStack stack) {
        return stack.hasTagCompound() && stack.getTagCompound().getBoolean("HideTooltip");
    }
}

