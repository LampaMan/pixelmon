/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemTM
extends ItemHeld {
    public final String attackName;
    public final int index;
    public final EnumType moveType;
    public final boolean isTR;

    public ItemTM(String attackName, int tmIndex, EnumType moveType, boolean isTR) {
        super(EnumHeldItems.tm, (isTR ? "tr" : "tm") + tmIndex);
        this.index = tmIndex;
        this.moveType = moveType;
        this.isTR = isTR;
        this.attackName = attackName;
        this.setMaxStackSize(16);
        this.setCreativeTab(PixelmonCreativeTabs.tms);
        this.canRepair = false;
    }
}

