/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.WrapperLink;
import com.pixelmongenerations.common.item.IHealHP;

public class HealFraction
implements IHealHP {
    private float healFraction;

    public HealFraction(float healFraction) {
        this.healFraction = healFraction;
    }

    @Override
    public int getHealAmount(PokemonLink pokemon) {
        if (pokemon instanceof WrapperLink) {
            WrapperLink link = (WrapperLink)pokemon;
            if (link.getWrapper().isDynamaxed()) {
                return (int)((float)pokemon.getStats().HP * this.healFraction);
            }
        }
        return (int)((float)pokemon.getMaxHealth() * this.healFraction);
    }
}

