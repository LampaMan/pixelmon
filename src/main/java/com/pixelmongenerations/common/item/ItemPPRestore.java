/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.network.EnumUpdateType;

public abstract class ItemPPRestore
extends PixelmonItem {
    protected boolean allPP;

    public ItemPPRestore(String itemName, boolean allPP) {
        super(itemName);
        this.setMaxStackSize(16);
        this.setCreativeTab(PixelmonCreativeTabs.restoration);
        this.canRepair = false;
        this.allPP = allPP;
    }

    public static boolean restorePP(PokemonLink userPokemon, int moveIndex, boolean allPP) {
        if (moveIndex == -1) {
            moveIndex = 0;
        }
        return ItemPPRestore.restorePP(userPokemon, userPokemon.getMoveset().get(moveIndex), allPP);
    }

    public static boolean restorePP(PokemonLink userPokemon, Attack m, boolean allPP) {
        if (m == null || m.pp >= m.ppBase) {
            return false;
        }
        m.pp = allPP ? m.ppBase : Math.min(m.pp + 10, m.ppBase);
        userPokemon.update(EnumUpdateType.Moveset);
        return true;
    }

    protected String getMessage(boolean success) {
        return success ? "pixelmon.interaction.pprestore" : "pixelmon.interaction.ppfail";
    }
}

