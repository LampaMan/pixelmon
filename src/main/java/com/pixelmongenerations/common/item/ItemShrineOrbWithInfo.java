/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.item.IShrineItem;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumForms;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import com.pixelmongenerations.core.network.ChatHandler;
import java.util.List;
import javax.annotation.Nullable;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemShrineOrbWithInfo
extends Item
implements IShrineItem {
    private final PokemonGroup group;

    public ItemShrineOrbWithInfo(String itemName, EnumSpecies species, int maxDurability) {
        this(itemName, species, EnumForms.NoForm, maxDurability);
    }

    public ItemShrineOrbWithInfo(String itemName, EnumSpecies species, IEnumForm form, int maxDurability) {
        this.group = new PokemonGroup(species, form);
        this.setCreativeTab(CreativeTabs.MISC);
        this.setMaxStackSize(1);
        this.setMaxDamage(maxDurability);
        this.setTranslationKey(itemName);
        this.setRegistryName(itemName);
    }

    public ItemShrineOrbWithInfo(String itemName, int maxDurability, PokemonGroup group) {
        this.group = group;
        this.setCreativeTab(CreativeTabs.MISC);
        this.setMaxStackSize(1);
        this.setMaxDamage(maxDurability);
        this.setTranslationKey(itemName);
        this.setRegistryName(itemName);
    }

    @Override
    public void onUpdate(ItemStack itemStack, World world, Entity entity, int inventorySlot, boolean isCurrentItem) {
        itemStack.setItemDamage(Math.min(this.getMaxDamage(), itemStack.getItemDamage()));
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        tooltip.add(I18n.format("gui.shopkeeper." + this.getTranslationKey(), new Object[0]));
    }

    @Override
    public PokemonGroup getGroup() {
        return this.group;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand hand) {
        ItemStack stack = playerIn.getHeldItem(hand);
        int damage = stack.getItemDamage();
        if (damage >= this.getMaxDamage(stack)) {
            ChatHandler.sendChat(playerIn, "pixelmon.orb.isfull", new Object[0]);
        } else {
            if (stack.getItem() == PixelmonItems.sparklingStone) {
                ChatHandler.sendChat(playerIn, "pixelmon.sparkling_stone.amountfull", this.getMaxDamage(stack) - damage);
                return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, stack);
            }
            if (stack.getItem() == PixelmonItems.fadedBlueOrb) {
                ChatHandler.sendChat(playerIn, "pixelmon.faded_blue.amountfull", this.getMaxDamage(stack) - damage);
                return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, stack);
            }
            if (stack.getItem() == PixelmonItems.fadedRedOrb) {
                ChatHandler.sendChat(playerIn, "pixelmon.faded_red.amountfull", this.getMaxDamage(stack) - damage);
                return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, stack);
            }
            if (stack.getItem() == PixelmonItems.lightStone || stack.getItem() == PixelmonItems.darkStone || stack.getItem() == PixelmonItems.dragonStone) {
                ChatHandler.sendChat(playerIn, "pixelmon.tao_stone.amountfull", this.getMaxDamage(stack) - damage);
                return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, stack);
            }
            ChatHandler.sendChat(playerIn, "pixelmon.orb.amountfull", this.getMaxDamage(stack) - damage);
        }
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, stack);
    }
}

