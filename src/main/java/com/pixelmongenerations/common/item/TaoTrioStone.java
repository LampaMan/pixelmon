/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.ItemShrineOrbWithInfo;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.List;

public class TaoTrioStone
extends ItemShrineOrbWithInfo {
    public TaoTrioStone(String name, EnumSpecies species) {
        super(name, species, 100);
        this.setCreativeTab(PixelmonCreativeTabs.legendItems);
    }

    @Override
    public boolean acceptsType(List<EnumType> type) {
        return type.contains((Object)EnumType.Dragon);
    }
}

