/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.CheckPokemonEditorAllowed;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.SetEditedPlayer;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.UpdateSinglePokemon;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemPokemonEditor
extends PixelmonItem {
    public ItemPokemonEditor() {
        super("pokemon_editor");
        this.setCreativeTab(CreativeTabs.TOOLS);
        this.setMaxStackSize(1);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
        EntityPlayerMP playerMP;
        Optional<PlayerStorage> optstorage;
        if (!world.isRemote && player.capabilities.isCreativeMode && hand == EnumHand.MAIN_HAND && ItemPokemonEditor.checkPermission((EntityPlayerMP)player) && (optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(playerMP = (EntityPlayerMP)player)).isPresent()) {
            PlayerStorage storage = optstorage.get();
            storage.sendUpdatedList();
            storage.recallAllPokemon();
            Pixelmon.NETWORK.sendTo(new SetEditedPlayer(player.getUniqueID(), player.getDisplayNameString(), null), playerMP);
            player.openGui(Pixelmon.INSTANCE, EnumGui.PokemonEditor.ordinal(), world, 0, 0, 0);
        }
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, player.getHeldItem(hand));
    }

    @Override
    public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity) {
        if (entity instanceof EntityPlayerMP && player.capabilities.isCreativeMode) {
            EntityPlayerMP otherPlayer;
            Optional<PlayerStorage> optstorage;
            if (ItemPokemonEditor.checkPermission((EntityPlayerMP)player) && (optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(otherPlayer = (EntityPlayerMP)entity)).isPresent()) {
                PlayerStorage storage = optstorage.get();
                if (storage.guiOpened) {
                    ChatHandler.sendChat(player, "pixelmon.general.playerbusy", new Object[0]);
                    return true;
                }
                Pixelmon.NETWORK.sendTo(new CheckPokemonEditorAllowed(player.getUniqueID()), otherPlayer);
            }
            return true;
        }
        return false;
    }

    public static void updateEditedPlayer(EntityPlayerMP editingPlayer, UUID editedPlayer) {
        Optional<PlayerStorage> optstorage;
        if (!ItemPokemonEditor.checkPermission(editingPlayer)) {
            return;
        }
        if (!editingPlayer.getUniqueID().equals(editedPlayer) && (optstorage = PixelmonStorage.pokeBallManager.getPlayerStorageFromUUID(editingPlayer.getServer(), editedPlayer)).isPresent()) {
            PlayerStorage storage = optstorage.get();
            Pixelmon.NETWORK.sendTo(new SetEditedPlayer(editedPlayer, storage.convertToData()), editingPlayer);
        }
    }

    public static void updateSinglePokemon(EntityPlayerMP editingPlayer, UUID editedPlayer, int slot) {
        Optional<PlayerStorage> optstorage;
        if (!ItemPokemonEditor.checkPermission(editingPlayer)) {
            return;
        }
        if (!editingPlayer.getUniqueID().equals(editedPlayer) && (optstorage = PixelmonStorage.pokeBallManager.getPlayerStorageFromUUID(editingPlayer.getServer(), editedPlayer)).isPresent()) {
            PlayerStorage storage = optstorage.get();
            NBTTagCompound pokemonNBT = storage.getList()[slot];
            PixelmonData pokemonData = pokemonNBT == null ? null : new PixelmonData(pokemonNBT);
            Pixelmon.NETWORK.sendTo(new UpdateSinglePokemon(slot, pokemonData), editingPlayer);
        }
    }

    public static boolean checkPermission(EntityPlayerMP editingPlayer) {
        if (PixelmonConfig.allowPokemonEditors && editingPlayer.canUseCommand(4, "pixelmon.pokemoneditor.use")) {
            return true;
        }
        ChatHandler.sendChat(editingPlayer, "gui.pokemoneditor.notallowedserver", new Object[0]);
        return false;
    }
}

