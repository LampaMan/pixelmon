/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import java.util.List;
import javax.annotation.Nullable;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemZoneWand
extends PixelmonItem {
    public static final String POS1_KEY = "pos1";
    public static final String POS2_KEY = "pos2";

    public ItemZoneWand() {
        super("zone_wand");
    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (!worldIn.isRemote && ItemZoneWand.isValidWand(player)) {
            ItemStack heldStack = player.getHeldItem(EnumHand.MAIN_HAND);
            ItemZoneWand.setPosition(heldStack, SelectPoint.Two, pos);
            return EnumActionResult.SUCCESS;
        }
        return EnumActionResult.PASS;
    }

    @Override
    public boolean getShareTag() {
        return true;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public void addInformation(ItemStack itemStack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        if (ItemZoneWand.isValidWand(itemStack)) {
            if (itemStack.hasTagCompound()) {
                NBTTagCompound wandNBT = itemStack.getTagCompound();
                tooltip.add((Object)((Object)TextFormatting.AQUA) + "Position 1: " + (Object)((Object)TextFormatting.GRAY) + (wandNBT.hasKey(POS1_KEY) ? wandNBT.getString(POS1_KEY) : "Unset"));
                tooltip.add((Object)((Object)TextFormatting.AQUA) + "Position 2: " + (Object)((Object)TextFormatting.GRAY) + (wandNBT.hasKey(POS2_KEY) ? wandNBT.getString(POS2_KEY) : "Unset"));
            } else {
                tooltip.add((Object)((Object)TextFormatting.AQUA) + "Position 1: " + (Object)((Object)TextFormatting.GRAY) + "Unset");
                tooltip.add((Object)((Object)TextFormatting.AQUA) + "Position 2: " + (Object)((Object)TextFormatting.GRAY) + "Unset");
            }
        }
    }

    public static boolean isValidWand(EntityPlayer player) {
        if (player == null) {
            return false;
        }
        ItemStack heldStack = player.getHeldItem(EnumHand.MAIN_HAND);
        return ItemZoneWand.isValidWand(heldStack);
    }

    public static boolean isValidWand(ItemStack itemStack) {
        if (itemStack.isEmpty()) {
            return false;
        }
        return itemStack.getItem() instanceof ItemZoneWand;
    }

    public static boolean isValidNBTWand(EntityPlayer player) {
        if (player == null) {
            return false;
        }
        ItemStack heldStack = player.getHeldItem(EnumHand.MAIN_HAND);
        return ItemZoneWand.isValidNBTWand(heldStack);
    }

    public static boolean isValidNBTWand(ItemStack itemStack) {
        if (itemStack.isEmpty()) {
            return false;
        }
        if (!(itemStack.getItem() instanceof ItemZoneWand) || !itemStack.hasTagCompound()) {
            return false;
        }
        NBTTagCompound wandNBT = itemStack.getTagCompound();
        return wandNBT.hasKey(POS1_KEY) && wandNBT.hasKey(POS2_KEY);
    }

    public static Vec3d getPosition(NBTTagCompound tag, SelectPoint point) {
        String[] pos = tag.getString(point == SelectPoint.One ? POS1_KEY : POS2_KEY).split("/");
        return new Vec3d(Integer.parseInt(pos[0]), Integer.parseInt(pos[1]), Integer.parseInt(pos[2]));
    }

    public static Vec3d getPosition(ItemStack itemStack, SelectPoint point) {
        String[] pos = itemStack.getTagCompound().getString(point == SelectPoint.One ? POS1_KEY : POS2_KEY).split("/");
        return new Vec3d(Integer.parseInt(pos[0]), Integer.parseInt(pos[1]), Integer.parseInt(pos[2]));
    }

    public static void setPosition(ItemStack itemStack, SelectPoint point, BlockPos pos) {
        NBTTagCompound tag = itemStack.hasTagCompound() ? itemStack.getTagCompound() : new NBTTagCompound();
        tag.setString(point == SelectPoint.One ? POS1_KEY : POS2_KEY, String.format("%s/%s/%s", pos.getX(), pos.getY(), pos.getZ()));
        itemStack.setTagCompound(tag);
    }

    public static enum SelectPoint {
        One,
        Two;

    }
}

