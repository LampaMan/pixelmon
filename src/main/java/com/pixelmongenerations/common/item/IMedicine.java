/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;

@FunctionalInterface
public interface IMedicine {
    public boolean useMedicine(PokemonLink var1);
}

