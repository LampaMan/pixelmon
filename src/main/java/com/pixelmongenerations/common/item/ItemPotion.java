/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.IMedicine;
import com.pixelmongenerations.common.item.ItemMedicine;

public class ItemPotion
extends ItemMedicine {
    public ItemPotion(String itemName, IMedicine ... healMethods) {
        super(itemName, healMethods);
    }
}

