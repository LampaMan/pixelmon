/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.api.pokemon.SpawnPokemon;
import com.pixelmongenerations.common.block.BlockBerryTree;
import com.pixelmongenerations.common.block.enums.EnumBlockPos;
import com.pixelmongenerations.common.block.tileEntities.TileEntityBerryTree;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumBurmy;
import com.pixelmongenerations.core.network.ChatHandler;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemPesticide
extends PixelmonItem {
    public ItemPesticide(String itemName) {
        super(itemName);
        this.setCreativeTab(CreativeTabs.TOOLS);
        this.setMaxStackSize(1);
        this.setMaxDamage(32);
        this.canRepair = false;
    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World world, BlockPos pos, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        if (!player.canPlayerEdit(pos, side, player.getHeldItem(hand)) || world.isRemote) {
            return EnumActionResult.FAIL;
        }
        IBlockState state = world.getBlockState(pos);
        if (state.getBlock() instanceof BlockBerryTree) {
            TileEntityBerryTree tileEntity;
            BlockPos pos1 = pos;
            if (state.getValue(BlockBerryTree.BLOCKPOS) == EnumBlockPos.TOP) {
                pos1 = pos.down();
            }
            if ((tileEntity = (TileEntityBerryTree)world.getTileEntity(pos1)) != null && tileEntity.removePests()) {
                ChatHandler.sendChat(player, "The plant was freed of the infestation. Prepare for battle.", new Object[0]);
                this.generateWeedEncounter(pos1, player, tileEntity.getBerry().getColor());
                player.getHeldItem(hand).damageItem(1, player);
            }
            return EnumActionResult.SUCCESS;
        }
        return EnumActionResult.PASS;
    }

    private void generateWeedEncounter(BlockPos pos, EntityPlayer player, EnumBerry.EnumBerryColor color) {
        SpawnPokemon.Builder builder = SpawnPokemon.builder().pos(pos);
        builder.yaw(player.rotationYaw - 180.0f);
        PokemonSpec spec = new PokemonSpec(new String[0]);
        spec.level = player.world.rand.nextBoolean() ? 14 : 15;
        switch (color) {
            case Red: {
                spec.name = EnumSpecies.Ledyba.name;
                break;
            }
            case Blue: {
                spec.name = EnumSpecies.Volbeat.name;
                break;
            }
            case Purple: {
                spec.name = EnumSpecies.Illumise.name;
                break;
            }
            case Green: {
                spec.name = EnumSpecies.Burmy.name;
                spec.setForm(EnumBurmy.Plant.getForm());
                break;
            }
            case Yellow: {
                spec.name = EnumSpecies.Combee.name;
                break;
            }
            case Pink: {
                spec.name = EnumSpecies.Spewpa.name;
            }
        }
        builder.pixelmon(spec);
        builder.build().spawn((EntityPlayerMP)player, player.getEntityWorld());
    }
}

