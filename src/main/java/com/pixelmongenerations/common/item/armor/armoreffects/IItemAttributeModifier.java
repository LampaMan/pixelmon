/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Multimap
 */
package com.pixelmongenerations.common.item.armor.armoreffects;

import com.google.common.collect.Multimap;
import com.pixelmongenerations.common.item.armor.GenericArmor;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.item.ItemStack;

public interface IItemAttributeModifier {
    public Multimap<String, AttributeModifier> getAttributeModifiers(ItemStack var1, GenericArmor var2);
}

