/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.HashMultimap
 *  com.google.common.collect.Multimap
 */
package com.pixelmongenerations.common.item.armor.armoreffects;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.pixelmongenerations.common.item.armor.GenericArmor;
import com.pixelmongenerations.common.item.armor.armoreffects.IItemAttributeModifier;
import java.util.UUID;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.item.ItemStack;

public class SpeedModifier
implements IItemAttributeModifier {
    private UUID elementalBootsUUID = UUID.fromString("10ae6bcc-5b15-41b1-ba51-b6101e178401");
    private AttributeModifier elementalBootsModifier = null;
    private UUID thunderStoneBootsUUID = UUID.fromString("de4f0383-fcf9-4ba7-8ffc-0767c1ead7b9");
    private AttributeModifier elementalBootsModifier2x = null;
    private boolean doubleWhenSetEquipped = false;

    public SpeedModifier(float amount, boolean doubleWhenSetEquipped) {
        this.elementalBootsModifier = new AttributeModifier(this.elementalBootsUUID, SharedMonsterAttributes.MOVEMENT_SPEED.getName(), amount, 1);
        this.elementalBootsModifier2x = new AttributeModifier(this.thunderStoneBootsUUID, SharedMonsterAttributes.MOVEMENT_SPEED.getName(), amount * 2.0f, 1);
        this.doubleWhenSetEquipped = doubleWhenSetEquipped;
    }

    @Override
    public Multimap<String, AttributeModifier> getAttributeModifiers(ItemStack stack, GenericArmor armor) {
        HashMultimap o = HashMultimap.create();
        AttributeModifier modifier = this.isDouble(stack, armor) ? this.elementalBootsModifier2x : this.elementalBootsModifier;
        o.put((Object)SharedMonsterAttributes.MOVEMENT_SPEED.getName(), (Object)modifier);
        return o;
    }

    public boolean isDouble(ItemStack stack, GenericArmor armor) {
        return this.doubleWhenSetEquipped && armor.equippedSet;
    }

    public Multimap<String, AttributeModifier> getNormalAttribute() {
        HashMultimap o = HashMultimap.create();
        o.put((Object)SharedMonsterAttributes.MOVEMENT_SPEED.getName(), (Object)this.elementalBootsModifier);
        return o;
    }

    public Multimap<String, AttributeModifier> getDoubleAttribute() {
        HashMultimap o = HashMultimap.create();
        o.put((Object)SharedMonsterAttributes.MOVEMENT_SPEED.getName(), (Object)this.elementalBootsModifier2x);
        return o;
    }
}

