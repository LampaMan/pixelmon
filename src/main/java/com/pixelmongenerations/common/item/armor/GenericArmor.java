/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Multimap
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.item.armor;

import com.google.common.collect.Multimap;
import com.pixelmongenerations.common.item.armor.armoreffects.IArmorEffect;
import com.pixelmongenerations.common.item.armor.armoreffects.IItemAttributeModifier;
import java.util.List;
import javax.annotation.Nullable;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class GenericArmor
extends ItemArmor {
    public IArmorEffect effect = null;
    public IItemAttributeModifier itemAttributeModifier = null;
    public boolean equippedSet = false;
    public final ItemArmor.ArmorMaterial material;

    public GenericArmor(String itemName, ItemArmor.ArmorMaterial material, EntityEquipmentSlot armorType) {
        super(material, 0, armorType);
        this.material = material;
        this.setTranslationKey(itemName);
        this.setRegistryName(itemName);
    }

    public GenericArmor setEffect(IArmorEffect effect) {
        this.effect = effect;
        return this;
    }

    public GenericArmor setItemAttributeModifiers(IItemAttributeModifier modifier) {
        this.itemAttributeModifier = modifier;
        return this;
    }

    @Override
    public Multimap<String, AttributeModifier> getAttributeModifiers(EntityEquipmentSlot slot, ItemStack stack) {
        Multimap<String, AttributeModifier> multimap = super.getAttributeModifiers(slot, stack);
        if (this.itemAttributeModifier != null && slot == EntityEquipmentSlot.FEET) {
            multimap.putAll(this.itemAttributeModifier.getAttributeModifiers(stack, this));
        }
        return multimap;
    }

    @Override
    public void onArmorTick(World world, EntityPlayer player, ItemStack itemStack) {
        if (this.effect != null) {
            this.effect.onArmorTick(world, player, itemStack, this);
        }
    }

    public String getTranslationKey() {
        return super.getTranslationKey().substring(5);
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        String info = I18n.translateToLocal("gui.shopkeeper.item." + this.getTranslationKey());
        if (!this.hasHideFlag(stack) && !info.startsWith("gui.shopkeeper.")) {
            tooltip.add(GuiScreen.isShiftKeyDown() ? info : I18n.translateToLocal("pixelmon.item.tooltip"));
        }
    }

    public boolean hasHideFlag(ItemStack stack) {
        return stack.hasTagCompound() && stack.getTagCompound().getBoolean("HideTooltip");
    }
}

