/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.armor.armoreffects;

import com.pixelmongenerations.common.item.armor.GenericArmor;
import com.pixelmongenerations.common.item.armor.armoreffects.IArmorEffect;
import com.pixelmongenerations.core.config.PixelmonItemsTools;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class ThunderstoneEffect
implements IArmorEffect {
    @Override
    public void onArmorTick(World world, EntityPlayer player, ItemStack stack, GenericArmor armor) {
        if (world.isRemote || player.getItemStackFromSlot(EntityEquipmentSlot.FEET) == null) {
            return;
        }
        ItemStack boots = player.getItemStackFromSlot(EntityEquipmentSlot.FEET);
        if (boots != null && boots.getItem() == armor && armor.material == PixelmonItemsTools.THUNDERSTONEARMORMAT) {
            if (IArmorEffect.isWearingFullSet(player, armor.material)) {
                armor.equippedSet = true;
                player.addPotionEffect(new PotionEffect(MobEffects.HASTE, 20));
            } else {
                armor.equippedSet = false;
            }
        }
    }
}

