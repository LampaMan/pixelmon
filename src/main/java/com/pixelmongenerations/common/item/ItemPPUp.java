/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.network.EnumUpdateType;

public class ItemPPUp
extends PixelmonItem {
    protected boolean max;

    public ItemPPUp(String itemName, boolean max) {
        super(itemName);
        this.setMaxStackSize(16);
        this.setCreativeTab(PixelmonCreativeTabs.restoration);
        this.max = max;
        this.canRepair = false;
    }

    public static boolean increasePPBase(PokemonLink userPokemon, int moveIndex, boolean max) {
        if (moveIndex == -1) {
            moveIndex = 0;
        }
        return ItemPPUp.increasePPBase(userPokemon, userPokemon.getMoveset().get(moveIndex), max);
    }

    public static boolean increasePPBase(PokemonLink userPokemon, Attack m, boolean max) {
        if (m == null || m.ppBase >= m.getAttackBase().ppMax) {
            return false;
        }
        m.ppBase = max ? m.getAttackBase().ppMax : Math.min(m.ppBase + (m.getAttackBase().ppMax - m.getAttackBase().ppBase) / 3, m.getAttackBase().ppMax);
        userPokemon.update(EnumUpdateType.Moveset);
        return true;
    }

    public boolean usePPUp(PokemonLink pokemon, int selectedMove) {
        boolean success = ItemPPUp.increasePPBase(pokemon, selectedMove, this.max);
        String data = pokemon.getMoveset().get(selectedMove).getAttackBase().getLocalizedName();
        pokemon.sendMessage(this.getMessage(success), data);
        return success;
    }

    protected String getMessage(boolean success) {
        return success ? "pixelmon.interaction.ppup" : "pixelmon.interaction.ppupfail";
    }
}

