/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.ItemTM;
import com.pixelmongenerations.core.enums.EnumType;

public class ItemTR
extends ItemTM {
    public ItemTR(String attackName, int hmIndex, EnumType moveType) {
        super(attackName, hmIndex, moveType, true);
        this.setMaxStackSize(16);
    }
}

