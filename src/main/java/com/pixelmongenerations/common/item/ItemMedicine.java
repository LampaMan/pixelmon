/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.FriendShip;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.WrapperLink;
import com.pixelmongenerations.common.item.IMedicine;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemMedicine
extends PixelmonItem {
    private final IMedicine[] healMethods;
    private int friendshipDecreaseNormal;
    private int friendshipDecreaseHigh;

    public ItemMedicine(String itemName, IMedicine ... healMethods) {
        super(itemName);
        this.setMaxStackSize(16);
        this.setCreativeTab(PixelmonCreativeTabs.restoration);
        this.canRepair = false;
        this.healMethods = healMethods;
    }

    public ItemMedicine setFriendshipDecrease(int normal, int high) {
        this.friendshipDecreaseNormal = normal;
        this.friendshipDecreaseHigh = high;
        return this;
    }

    @Override
    public boolean useFromBag(PixelmonWrapper pixelmonWrapper, PixelmonWrapper target, int additionalInfo) {
        this.useMedicine(new WrapperLink(target), additionalInfo);
        return super.useFromBag(pixelmonWrapper, target, additionalInfo);
    }

    public boolean useMedicine(PokemonLink target, int additionalInfo) {
        boolean success = false;
        for (IMedicine healMethod : this.healMethods) {
            success = healMethod.useMedicine(target) || success;
        }
        if (success) {
            FriendShip friendship = target.getFriendship();
            friendship.decreaseFriendship(friendship.getFriendship() >= 200 ? this.friendshipDecreaseHigh : this.friendshipDecreaseNormal);
        } else {
            target.sendMessage("pixelmon.general.noeffect", new Object[0]);
        }
        return success;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer playerIn, EnumHand hand) {
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, playerIn.getHeldItem(hand));
    }
}

