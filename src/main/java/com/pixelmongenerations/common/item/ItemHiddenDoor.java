/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemDoor;

public class ItemHiddenDoor
extends ItemDoor {
    public ItemHiddenDoor(Block doorBlock, String itemName) {
        super(doorBlock);
        this.setCreativeTab(CreativeTabs.REDSTONE);
        this.setMaxStackSize(16);
        this.setTranslationKey(itemName);
        this.setRegistryName(itemName);
    }
}

