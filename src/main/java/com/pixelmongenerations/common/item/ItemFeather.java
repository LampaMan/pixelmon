/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;

public class ItemFeather
extends PixelmonItem {
    public StatsType type;

    public ItemFeather(StatsType type, String itemName) {
        super(itemName);
        this.type = type;
        this.setMaxStackSize(16);
        this.setCreativeTab(PixelmonCreativeTabs.restoration);
        this.canRepair = false;
    }

    public boolean featherEVs(EntityPixelmon entityPixelmon) {
        return entityPixelmon.stats.EVs.featherEVs(this.type);
    }
}

