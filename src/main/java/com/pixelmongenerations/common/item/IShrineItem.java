/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.List;
import net.minecraft.item.ItemStack;

public interface IShrineItem {
    public PokemonGroup getGroup();

    default public boolean shouldCheckDamage() {
        return false;
    }

    default public boolean acceptsType(List<EnumType> type) {
        return true;
    }

    default public ItemStack getUsedForm() {
        return ItemStack.EMPTY;
    }
}

