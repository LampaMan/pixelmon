/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Sets
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.item;

import com.google.common.collect.Sets;
import com.pixelmongenerations.api.events.AnvilEvent;
import com.pixelmongenerations.common.block.tileEntities.TileEntityAnvil;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonItemsTools;
import java.util.List;
import java.util.Set;
import javax.annotation.Nullable;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemHammer
extends ItemTool {
    private static final Set<Block> effectiveBlocks = Sets.newHashSet(PixelmonBlocks.anvil);

    public ItemHammer(Item.ToolMaterial material, String itemName) {
        super(2.0f, 1.0f, material, effectiveBlocks);
        this.setTranslationKey(itemName);
        this.setRegistryName(itemName);
    }

    @Override
    public boolean canHarvestBlock(IBlockState block) {
        return block.getBlock() != PixelmonBlocks.anvil;
    }

    @Override
    public boolean onBlockDestroyed(ItemStack stack, World worldIn, IBlockState state, BlockPos pos, EntityLivingBase entityLiving) {
        if ((double)state.getBlockHardness(worldIn, pos) != 0.0) {
            stack.damageItem(1, entityLiving);
        }
        return true;
    }

    @Override
    public boolean onBlockStartBreak(ItemStack itemstack, BlockPos pos, EntityPlayer player) {
        if (player.world.getBlockState(pos).getBlock() == PixelmonBlocks.anvil) {
            if (!player.world.isRemote && ((TileEntityAnvil)player.world.getTileEntity(pos)).blockHit((int)this.getDestroySpeed(null, PixelmonBlocks.anvil.getDefaultState()), (EntityPlayerMP)player)) {
                if (itemstack.getItemDamage() >= itemstack.getMaxDamage()) {
                    player.setHeldItem(EnumHand.MAIN_HAND, ItemStack.EMPTY);
                } else {
                    AnvilEvent.HammerDamageEvent hammerDamageEvent = new AnvilEvent.HammerDamageEvent((EntityPlayerMP)player, (TileEntityAnvil)player.world.getTileEntity(pos), itemstack, 3);
                    if (MinecraftForge.EVENT_BUS.post(hammerDamageEvent)) {
                        return true;
                    }
                    itemstack.attemptDamageItem(hammerDamageEvent.getDamage(), itemRand, null);
                }
            }
            return true;
        }
        return super.onBlockStartBreak(itemstack, pos, player);
    }

    @Override
    public float getDestroySpeed(ItemStack stack, IBlockState state) {
        if (state.getBlock() == PixelmonBlocks.anvil) {
            if (this.toolMaterial == Item.ToolMaterial.WOOD) {
                return 1.0f;
            }
            if (this.toolMaterial == Item.ToolMaterial.STONE) {
                return 2.0f;
            }
            if (this.toolMaterial == PixelmonItemsTools.ALUMINIUM || this.toolMaterial == Item.ToolMaterial.IRON) {
                return 3.0f;
            }
            if (this.toolMaterial == Item.ToolMaterial.GOLD) {
                return 4.0f;
            }
            if (this.toolMaterial == Item.ToolMaterial.DIAMOND) {
                return 5.0f;
            }
        }
        return 1.0f;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        String info = I18n.translateToLocal("gui.shopkeeper.item." + this.getTranslationKey());
        if (!info.startsWith("gui.shopkeeper.")) {
            if (GuiScreen.isShiftKeyDown()) {
                tooltip.add(info);
            } else {
                tooltip.add("Hold shift for more info.");
            }
        }
    }
}

