/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.api.events.ThrowPokeballEvent;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.WildPixelmonParticipant;
import com.pixelmongenerations.common.entity.pokeballs.EntityEmptyPokeball;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class ItemPokeball
extends PixelmonItem {
    public EnumPokeball type;
    public static boolean allowCapturingOutsideBattle = true;

    public ItemPokeball(EnumPokeball type) {
        super(type.getFilenamePrefix());
        this.maxStackSize = 64;
        this.setMaxDamage(1000000);
        this.type = type;
        this.setCreativeTab(PixelmonCreativeTabs.pokeball);
        this.canRepair = false;
    }

    public ItemPokeball(EnumPokeball type, String name) {
        super(name);
        this.maxStackSize = 64;
        this.setMaxDamage(1000000);
        this.type = type;
        this.setCreativeTab(PixelmonCreativeTabs.pokeball);
        this.canRepair = false;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer entityPlayer, EnumHand handIn) {
        ItemStack itemStack = entityPlayer.getHeldItem(handIn);
        ThrowPokeballEvent throwPokeBallEvent = new ThrowPokeballEvent(entityPlayer, itemStack, this.type, false);
        MinecraftForge.EVENT_BUS.post(throwPokeBallEvent);
        if (throwPokeBallEvent.isCanceled()) {
            return new ActionResult<ItemStack>(EnumActionResult.FAIL, itemStack);
        }
        if (allowCapturingOutsideBattle) {
            if (!entityPlayer.capabilities.isCreativeMode) {
                itemStack.splitStack(1);
            }
            world.playSound(null, entityPlayer.getPosition(), SoundEvents.ENTITY_ARROW_SHOOT, SoundCategory.PLAYERS, 0.5f, 1.0f);
            if (!world.isRemote) {
                world.spawnEntity(new EntityEmptyPokeball(world, entityPlayer, this.type, !entityPlayer.capabilities.isCreativeMode));
            }
        }
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemStack);
    }

    @Override
    public boolean useFromBag(PixelmonWrapper userWrapper, PixelmonWrapper targetWrapper) {
        EntityPlayerMP thrower = userWrapper.getPlayerOwner();
        if (!(targetWrapper.getParticipant() instanceof WildPixelmonParticipant)) {
            return false;
        }
        ThrowPokeballEvent throwPokeballEvent = new ThrowPokeballEvent(thrower, null, this.type, true);
        MinecraftForge.EVENT_BUS.post(throwPokeballEvent);
        World world = thrower.world;
        EntityEmptyPokeball p = new EntityEmptyPokeball(world, thrower, targetWrapper.pokemon, this.type, BattleRegistry.getBattle(thrower));
        world.spawnEntity(p);
        return super.useFromBag(userWrapper, targetWrapper);
    }
}

