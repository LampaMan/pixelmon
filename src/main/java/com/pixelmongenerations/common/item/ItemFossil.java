/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.items.EnumFossils;

public class ItemFossil
extends PixelmonItem {
    public EnumFossils fossil;

    public ItemFossil(EnumFossils fossil) {
        super(fossil.getItemName());
        this.fossil = fossil;
        this.setCreativeTab(PixelmonCreativeTabs.natural);
        this.canRepair = false;
    }

    public EnumFossils getFossil() {
        return this.fossil;
    }
}

