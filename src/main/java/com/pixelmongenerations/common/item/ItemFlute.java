/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.enums.items.EnumFlutes;
import com.pixelmongenerations.core.util.CustomTeleporter;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemFlute
extends PixelmonItem {
    private final EnumFlutes type;

    public ItemFlute(EnumFlutes type) {
        super(type.getName());
        this.type = type;
    }

    public EnumFlutes getType() {
        return this.type;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
        if (!world.isRemote && player.dimension == 24 && player.getHeldItem(hand).hasTagCompound()) {
            int[] location = player.getHeldItem(hand).getTagCompound().getIntArray("Location");
            CustomTeleporter.teleportToDimension(player, 0, location[0], location[1], location[2]);
        }
        return new ActionResult<ItemStack>(EnumActionResult.PASS, player.getHeldItem(hand));
    }
}

