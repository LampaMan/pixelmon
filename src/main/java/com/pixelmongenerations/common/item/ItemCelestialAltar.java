/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItemBlock;
import com.pixelmongenerations.common.spawning.spawners.EnumWorldState;
import com.pixelmongenerations.common.spawning.spawners.SpawnerBase;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class ItemCelestialAltar
extends PixelmonItemBlock {
    public ItemCelestialAltar(Block block) {
        super(block);
        this.addPropertyOverride(new ResourceLocation("time"), (stack, worldIn, entityIn) -> {
            Entity entity;
            boolean flag = entityIn != null;
            Entity entity2 = entity = flag ? entityIn : stack.getItemFrame();
            if (worldIn == null && entity != null) {
                worldIn = entity.world;
            }
            if (worldIn == null) {
                return -1.0f;
            }
            EnumWorldState worldState = SpawnerBase.getWorldState(worldIn);
            return worldState == EnumWorldState.dawn || worldState == EnumWorldState.day ? 0.0f : 1.0f;
        });
    }
}

