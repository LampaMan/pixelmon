/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.WrapperLink;
import com.pixelmongenerations.common.item.ItemPPRestore;

public class ItemEther
extends ItemPPRestore {
    public ItemEther(String itemName, boolean allPP) {
        super(itemName, allPP);
    }

    public boolean useEther(PokemonLink pokemon, int selectedMove) {
        String message = this.getMessage(ItemEther.restorePP(pokemon, selectedMove, this.allPP));
        String data = pokemon.getMoveset().get(selectedMove).getAttackBase().getLocalizedName();
        pokemon.sendMessage(message, data);
        return ItemEther.restorePP(pokemon, selectedMove, this.allPP);
    }

    @Override
    public boolean useFromBag(PixelmonWrapper userWrapper, PixelmonWrapper targetWrapper, int selectedMove) {
        this.useEther(new WrapperLink(targetWrapper), selectedMove);
        return super.useFromBag(userWrapper, targetWrapper);
    }
}

