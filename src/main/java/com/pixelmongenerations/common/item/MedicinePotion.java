/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.item.IHealHP;
import com.pixelmongenerations.common.item.IMedicine;
import com.pixelmongenerations.core.network.EnumUpdateType;

public class MedicinePotion
implements IMedicine {
    private IHealHP healHP;

    public MedicinePotion(IHealHP healHP) {
        this.healHP = healHP;
    }

    @Override
    public boolean useMedicine(PokemonLink target) {
        int maxHealth;
        int currentHealth = target.getHealth();
        if (currentHealth < (maxHealth = target.getMaxHealth())) {
            target.setHealth(Math.min(maxHealth, currentHealth + this.healHP.getHealAmount(target)));
            target.update(EnumUpdateType.HP);
            target.sendMessage("pixelmon.effect.restorehealth", target.getNickname());
            return true;
        }
        return false;
    }
}

