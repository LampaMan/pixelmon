/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.reflect.TypeToken
 */
package com.pixelmongenerations.common.starter;

import com.google.common.reflect.TypeToken;
import com.pixelmongenerations.common.entity.npcs.registry.PokemonForm;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.StarterList;
import com.pixelmongenerations.core.enums.EnumSpecies;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import net.minecraft.util.text.translation.I18n;

public class CustomStarters {
    private static List<String> starters = Arrays.asList("Bulbasaur", "Squirtle", "Charmander", "Chikorita", "Totodile", "Cyndaquil", "Treecko", "Mudkip", "Torchic", "Turtwig", "Piplup", "Chimchar", "Snivy", "Oshawott", "Tepig", "Chespin", "Froakie", "Fennekin", "Rowlet", "Popplio", "Litten", "Grookey", "Sobble", "Scorbunny");
    public static PokemonForm[] starterList = new PokemonForm[24];
    public static int starterLevel = 5;
    public static boolean shinyStarter = false;

    public static void loadConfig(CommentedConfigurationNode mainNode) throws ObjectMappingException {
        CommentedConfigurationNode customStarters = mainNode.getNode("Starters");
        starterLevel = Math.max(1, Math.min(customStarters.getNode("level").setComment(I18n.translateToLocal("pixelmon.config.starterLevel.comment")).getInt(5), PixelmonConfig.maxLevel));
        shinyStarter = customStarters.getNode("shiny").setComment(I18n.translateToLocal("pixelmon.config.starterShiny.comment")).getBoolean(false);
        starters = customStarters.getNode("starterList").setComment(I18n.translateToLocal("pixelmon.config.starterList.comment")).getList(new TypeToken<String>(){}, starters);
        int starterNum = 0;
        for (String pokemonName : starters) {
            Optional<EnumSpecies> pokemonOptional;
            int numberIndex = 0;
            if (starterNum > 24) break;
            if (!"Porygon2".equalsIgnoreCase(pokemonName)) {
                for (numberIndex = pokemonName.length() - 1; numberIndex >= 0 && pokemonName.charAt(numberIndex) >= '0' && pokemonName.charAt(numberIndex) <= '9'; --numberIndex) {
                }
            }
            if (!(pokemonOptional = EnumSpecies.getFromName(pokemonName.substring(0, numberIndex + 1))).isPresent()) continue;
            EnumSpecies pokemonEnum = pokemonOptional.get();
            PokemonForm pokemonForm = new PokemonForm(pokemonEnum);
            if (Entity3HasStats.hasForms(pokemonEnum)) {
                int formIndex = -1;
                try {
                    formIndex = Integer.parseInt(pokemonName.substring(numberIndex + 1));
                }
                catch (NumberFormatException numberFormatException) {
                    // empty catch block
                }
                if (formIndex == -1 && !EnumSpecies.hasNoFormForm(pokemonEnum)) {
                    formIndex = Entity3HasStats.getRandomForm(pokemonEnum);
                }
                pokemonForm.form = formIndex;
            }
            CustomStarters.starterList[starterNum] = pokemonForm;
            ++starterNum;
        }
        StarterList.setStarterList(starterList);
    }
}

