/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.pokedex;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.enums.EnumSpecies;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;

public class PokedexEntry {
    public final String name;
    public final int natPokedexNum;
    public final String heightM;
    public final String weightM;
    public final String heightC;
    public final String weightC;
    private EntityPixelmon renderTarget;

    public PokedexEntry(int i, String n, float w, float h) {
        this.natPokedexNum = i;
        this.name = n;
        this.heightM = h + " " + I18n.translateToLocal("gui.pokedex.meters");
        this.weightM = w + " " + I18n.translateToLocal("gui.pokedex.kilograms");
        float hc = h * 3.28084f;
        if (hc == 0.0f) {
            this.heightC = "??? ft.";
        } else {
            String s = hc + "";
            int i1 = s.indexOf(46);
            String feet = s.substring(0, i1);
            float inches = Float.parseFloat(0 + s.substring(i1)) * 12.0f;
            int in = Math.round(inches);
            this.heightC = feet + "'" + in + "\"";
        }
        float wc = w * 2.2046225f;
        this.weightC = (wc == 0.0f ? "???" : Integer.valueOf(Math.round(wc))) + " " + I18n.translateToLocal("gui.pokedex.pounds");
    }

    public String getPokedexDisplayNumber() {
        String s = "" + this.natPokedexNum;
        while (s.length() < 3) {
            s = "0" + s;
        }
        return s;
    }

    public EntityPixelmon getRenderTarget(World w) {
        if (this.renderTarget != null && w == this.renderTarget.world) {
            return this.renderTarget;
        }
        if (this.name == null || this.name.equals("???") || !EnumSpecies.hasPokemon(this.name)) {
            return null;
        }
        this.renderTarget = PokemonSpec.from(this.name).create(w);
        if (this.renderTarget.getFormEnum().toString().equalsIgnoreCase("None")) {
            this.renderTarget.setGender(Gender.Male);
        }
        if (this.renderTarget.getFormEnum().toString().equalsIgnoreCase("None")) {
            this.renderTarget.setGender(Gender.Female);
        }
        this.renderTarget.getLvl().setLevel(50);
        this.renderTarget.setGrowth(EnumGrowth.Ordinary);
        return this.renderTarget;
    }

    public String toString() {
        return this.getPokedexDisplayNumber() + " " + Entity1Base.getLocalizedName(this.name);
    }
}

