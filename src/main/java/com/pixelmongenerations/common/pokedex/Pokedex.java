/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.pokedex;

import com.pixelmongenerations.api.events.player.PlayerPokedexEvent;
import com.pixelmongenerations.common.pokedex.EnumPokedexRegisterStatus;
import com.pixelmongenerations.common.pokedex.PokedexEntry;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.data.pokemon.PokemonRegistry;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.enums.EnumShinyItem;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.PixelmonPokedexPacket;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraftforge.common.MinecraftForge;

public class Pokedex {
    public static final HashMap<Integer, PokedexEntry> fullPokedex = new HashMap();
    public static final HashMap<String, Integer> nameToID = new HashMap();
    public static final int pokedexSize = 898;
    public static final int pokedexCompleteSize;
    public Entity owner;
    private HashMap<Integer, EnumPokedexRegisterStatus> seenMap;

    public static int nameToID(String name) {
        if (name.equals("Ho-oh")) {
            name = "Ho-Oh";
        }
        return nameToID.getOrDefault(name, 0);
    }

    public static boolean isEntryEmpty(int i) {
        if (!fullPokedex.containsKey(i)) {
            return true;
        }
        String checkName = Pokedex.fullPokedex.get((Object)Integer.valueOf((int)i)).name;
        return checkName.equals("???") || !EnumSpecies.getNameList().contains((Object)checkName);
    }

    public Pokedex() {
        this(null);
    }

    public Pokedex(Entity e) {
        this.owner = e;
        this.seenMap = new HashMap();
    }

    public void checkDex() {
        EntityPlayerMP player;
        Optional<PlayerStorage> storageOpt;
        if (this.owner != null && this.owner instanceof EntityPlayerMP && (storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage(player = (EntityPlayerMP)this.owner)).isPresent() && this.isCompleted() && storageOpt.get().shinyData.getShinyItem() == EnumShinyItem.Disabled) {
            player.openGui(Pixelmon.INSTANCE, EnumGui.ShinyItem.getIndex(), player.world, 1, 0, 0);
            storageOpt.ifPresent(storage -> storage.shinyData.setShinyItem(EnumShinyItem.ShinyCharm, false));
            MinecraftForge.EVENT_BUS.post(new PlayerPokedexEvent.PokedexCompletionEvent(this, player));
        }
    }

    public NBTTagCompound readFromNBT(NBTTagCompound nbt) {
        this.seenMap.clear();
        NBTTagList nbtl = nbt.getTagList("Pokedex", 8);
        for (int i = 0; i < nbtl.tagCount(); ++i) {
            try {
                String[] s = nbtl.getStringTagAt(i).split(":");
                this.seenMap.put(Integer.parseInt(s[0]), EnumPokedexRegisterStatus.get(Integer.parseInt(s[1])));
                continue;
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return nbt;
    }

    public HashMap<Integer, EnumPokedexRegisterStatus> getSeenMap() {
        return this.seenMap;
    }

    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        NBTTagList nbtl = new NBTTagList();
        for (Map.Entry<Integer, EnumPokedexRegisterStatus> e : this.seenMap.entrySet()) {
            nbtl.appendTag(new NBTTagString(e.getKey() + ":" + e.getValue().ordinal()));
        }
        nbt.setTag("Pokedex", nbtl);
        return nbt;
    }

    public void sendToPlayer(EntityPlayerMP e) {
        PixelmonPokedexPacket p = new PixelmonPokedexPacket(this);
        Pixelmon.NETWORK.sendTo(p, e);
    }

    public void set(EnumSpecies species, EnumPokedexRegisterStatus drs) {
        this.set(species.getNationalPokedexInteger(), drs);
    }

    public void set(int id, EnumPokedexRegisterStatus drs) {
        if (!(this.seenMap.containsKey(id) && this.seenMap.get(id).ordinal() > drs.ordinal() || id <= 0 || id > 898)) {
            if (this.owner != null && this.owner instanceof EntityPlayerMP) {
                MinecraftForge.EVENT_BUS.post(new PlayerPokedexEvent.PokedexEntryEvent(this, (EntityPlayerMP)this.owner, id, drs));
            }
            this.seenMap.put(id, drs);
        }
    }

    public void fillDex(EnumPokedexRegisterStatus status) {
        int count = 0;
        for (EnumSpecies pokemon : EnumSpecies.values()) {
            if (pokemon == EnumSpecies.MissingNo) continue;
            ++count;
            this.seenMap.put(pokemon.getNationalPokedexInteger(), status);
            if (!this.isCompleted() || this.owner == null || !(this.owner instanceof EntityPlayerMP)) continue;
            EntityPlayerMP player = (EntityPlayerMP)this.owner;
            player.openGui(Pixelmon.INSTANCE, EnumGui.ShinyItem.getIndex(), player.world, 1, 0, 0);
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
            optstorage.ifPresent(storage -> storage.shinyData.setShinyItem(EnumShinyItem.ShinyCharm, false));
        }
        if (this.owner != null && this.owner instanceof EntityPlayerMP) {
            this.sendToPlayer((EntityPlayerMP)this.owner);
        }
    }

    public EnumPokedexRegisterStatus get(int id) {
        EnumPokedexRegisterStatus d = this.seenMap.get(id);
        if (d == null) {
            d = EnumPokedexRegisterStatus.unknown;
        }
        return d;
    }

    public boolean isUnknown(int id) {
        return this.get(id) == EnumPokedexRegisterStatus.unknown;
    }

    public boolean hasSeen(int id) {
        EnumPokedexRegisterStatus d = this.get(id);
        return d == EnumPokedexRegisterStatus.seen || this.hasCaught(id);
    }

    public boolean hasCaught(int id) {
        return this.get(id) == EnumPokedexRegisterStatus.caught;
    }

    public boolean hasCaught(EnumSpecies species) {
        return this.get(species.getNationalPokedexInteger()) == EnumPokedexRegisterStatus.caught;
    }

    public PokedexEntry getEntry(int id) {
        PokedexEntry e = fullPokedex.get(id);
        if (e == null) {
            e = new PokedexEntry(id, "???", 0.0f, 0.0f);
        }
        return e;
    }

    public int countCaught() {
        int count = 0;
        for (int i = 1; i <= 898; ++i) {
            if (!this.hasCaught(i)) continue;
            ++count;
        }
        return count;
    }

    public int seenCaught() {
        int count = 0;
        for (int i = 1; i <= 898; ++i) {
            if (!this.hasSeen(i)) continue;
            ++count;
        }
        return count;
    }

    public void setSeenList(HashMap<Integer, EnumPokedexRegisterStatus> data) {
        this.seenMap = data;
    }

    public static void getNBTTags(HashMap<String, Class> tags) {
        tags.put("Pokedex", NBTTagList.class);
    }

    public boolean isCompleted() {
        return this.countCaught() >= pokedexCompleteSize;
    }

    public double getCaughtPercentage() {
        return (double)this.countCaught() / 898.0;
    }

    static {
        PokemonRegistry.getAllPokemon().forEach(baseStats -> {
            String n = baseStats.pixelmonName;
            int i = baseStats.nationalPokedexNumber;
            float w = baseStats.weight;
            float h = baseStats.height;
            fullPokedex.put(i, new PokedexEntry(i, n, w, h));
            nameToID.put(n, i);
        });
        pokedexCompleteSize = fullPokedex.size() - 1;
    }
}

