/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableList
 */
package com.pixelmongenerations.common.pokedex;

import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.List;

public enum EnumPokedexRegisterStatus {
    unknown,
    seen,
    caught;

    private static List<String> statusNames;

    public static EnumPokedexRegisterStatus get(int i) {
        return EnumPokedexRegisterStatus.values()[i];
    }

    public static ImmutableList<String> getNameList() {
        return ImmutableList.copyOf(statusNames);
    }

    static {
        statusNames = new ArrayList<String>();
        for (EnumPokedexRegisterStatus enumCustomIcon : EnumPokedexRegisterStatus.values()) {
            statusNames.add(enumCustomIcon.name());
        }
    }
}

