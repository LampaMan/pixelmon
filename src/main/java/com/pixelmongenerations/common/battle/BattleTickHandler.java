/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle;

import com.pixelmongenerations.common.battle.BattleRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;

public class BattleTickHandler {
    @SubscribeEvent
    public void tickStart(TickEvent.WorldTickEvent event) {
        if (event.side == Side.SERVER) {
            BattleRegistry.updateBattles();
        }
    }
}

