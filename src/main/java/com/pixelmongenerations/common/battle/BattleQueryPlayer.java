/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle;

import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.network.packetHandlers.battles.EnumBattleQueryResponse;
import net.minecraft.entity.player.EntityPlayerMP;

class BattleQueryPlayer {
    EntityPlayerMP player;
    EntityPixelmon pokemon;
    EnumBattleQueryResponse response;
    int clauseVersionNumber;

    BattleQueryPlayer(EntityPlayerMP player, EntityPixelmon pokemon) {
        this.player = player;
        this.pokemon = pokemon;
        this.response = EnumBattleQueryResponse.None;
    }

    PlayerParticipant getParticipant() {
        return new PlayerParticipant(this.player, this.pokemon);
    }

    PlayerParticipant getParticipant(EntityPixelmon second) {
        return new PlayerParticipant(this.player, this.pokemon, second);
    }
}

