/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.participants;

import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.battle.EnumBattleAIMode;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.TextComponentBase;
import net.minecraft.util.text.TextComponentString;

public class TrainerParticipant
extends BattleParticipant {
    public NPCTrainer trainer;

    public TrainerParticipant(NPCTrainer trainer, EntityPlayer opponent, int numPokemon) throws IllegalStateException {
        super(numPokemon);
        this.setTrainer(trainer, opponent);
    }

    public TrainerParticipant(NPCTrainer trainer, EntityPlayer opponent, int numPokemon, List<PokemonLink> teamSelection) throws IllegalStateException {
        super(numPokemon);
        this.setTrainer(trainer, opponent);
        this.loadParty(teamSelection);
    }

    private void setTrainer(NPCTrainer trainer, EntityPlayer opponent) throws IllegalStateException {
        if (!trainer.canStartBattle(opponent, true)) {
            throw new IllegalStateException("NPC Trainer already battled: " + trainer.getName());
        }
        this.trainer = trainer;
        this.loadParty(trainer.getPokemonStorage());
    }

    public TrainerParticipant(NPCTrainer trainer, int numPokemon) throws IllegalStateException {
        super(numPokemon);
        if (trainer.battleController != null) {
            throw new IllegalStateException("NPC Trainer already battled: " + trainer.getName());
        }
        this.trainer = trainer;
        this.loadParty(trainer.getPokemonStorage());
    }

    @Override
    public void startBattle(BattleControllerBase bc) {
        int i;
        PlayerStorage storage = this.trainer.getPokemonStorage();
        if (this.trainer.getBossMode() != EnumBossMode.NotBoss) {
            int lvl = 1;
            for (BattleParticipant p : bc.participants) {
                if (p.team == this.team || !(p instanceof PlayerParticipant)) continue;
                lvl = Math.max(lvl, ((PlayerParticipant)p).getHighestLevel());
            }
            lvl += this.trainer.getBossMode().extraLevels;
            for (PixelmonWrapper pw : this.allPokemon) {
                pw.bc = bc;
                pw.setTempLevel(lvl);
            }
        } else {
            storage.updateStatsTrainer();
        }
        EntityPixelmon[] released = this.releasePokemon();
        ArrayList<EntityPixelmon> realReleased = new ArrayList<EntityPixelmon>();
        EntityPixelmon[] releasedpokemon = released;
        int trainerpokemon = released.length;
        for (i = 0; i < trainerpokemon; ++i) {
            EntityPixelmon aReleased = releasedpokemon[i];
            if (aReleased == null) continue;
            realReleased.add(aReleased);
        }
        i = 0;
        while (i < realReleased.size()) {
            PixelmonWrapper pw;
            EntityPixelmon p = (EntityPixelmon)realReleased.get(i);
            pw = this.getPokemonFromParty(p);
            this.controlledPokemon.add(pw);
            pw.battlePosition = i++;
        }
        super.startBattle(bc);
        for (PixelmonWrapper pw1 : this.allPokemon) {
            pw1.setHealth(pw1.getMaxHealth());
        }
        EnumBattleAIMode battleAIMode = this.trainer.getBattleAIMode();
        if (battleAIMode == EnumBattleAIMode.Default) {
            battleAIMode = PixelmonConfig.battleAITrainer;
        }
        this.setBattleAI(battleAIMode.createAI(this));
        this.trainer.setBattleController(bc);
        this.trainer.startBattle(bc.getOpponents(this).get(0));
    }

    private EntityPixelmon[] releasePokemon() {
        EntityPixelmon[] pokemon = new EntityPixelmon[this.numControlledPokemon];
        for (int i = 0; i < this.numControlledPokemon; ++i) {
            if (this.allPokemon.length <= i) continue;
            pokemon[i] = this.trainer.releasePokemon(this.allPokemon[i].getPokemonID());
        }
        return pokemon;
    }

    @Override
    public ParticipantType getType() {
        return ParticipantType.Trainer;
    }

    @Override
    public boolean hasMorePokemonReserve() {
        return this.countAblePokemon() > this.getActiveUnfaintedPokemon().size() + this.switchingOut.size();
    }

    @Override
    public boolean canGainXP() {
        return false;
    }

    @Override
    public void endBattle() {
        ArrayList<BattleParticipant> opponents = (ArrayList)this.bc.participants.stream().filter(p -> p.team != this.team).collect(Collectors.toList());
        if (this.trainer.battleController != null) {
            if (!this.hasMorePokemon() && opponents.stream().anyMatch(px -> px.hasMorePokemon())) {
                this.trainer.loseBattle(opponents);
            } else {
                this.trainer.winBattle(opponents);
            }
        }
        for (PixelmonWrapper p2 : this.controlledPokemon) {
            p2.getBattleStats().clearBattleStats();
            p2.clearStatus();
            if (p2.pokemon == null) continue;
            p2.pokemon.endBattle();
            p2.pokemon.setDead();
            p2.pokemon.unloadEntity();
        }
        this.trainer.setAttackTargetPix(null);
        this.trainer.healAllPokemon();
        this.trainer.setBattleController(null);
    }

    @Override
    public void getNextPokemon(int position) {
        PixelmonWrapper pw;
        Iterator controlledpokemon = this.controlledPokemon.iterator();
        do {
            if (!controlledpokemon.hasNext()) {
                return;
            }
            pw = (PixelmonWrapper)controlledpokemon.next();
        } while (pw.battlePosition != position);
        this.bc.switchPokemon(pw.getPokemonID(), this.getBattleAI().getNextSwitch(pw), true);
    }

    @Override
    public int[] getNextPokemonID() {
        return this.trainer.getNextPokemonID();
    }

    private String getTranslatedName() {
        String loc = null;
        for (BattleParticipant p : this.bc.getOpponents(this)) {
            if (!(p instanceof PlayerParticipant)) continue;
            loc = ((EntityPlayerMP)p.getEntity()).language;
        }
        if (loc == null) {
            return this.trainer.getName();
        }
        return this.trainer.getName(loc);
    }

    @Override
    public TextComponentBase getName() {
        return new TextComponentString(this.getTranslatedName());
    }

    @Override
    public MoveChoice getMove(PixelmonWrapper p) {
        return this.getBattleAI().getNextMove(p);
    }

    @Override
    public PixelmonWrapper switchPokemon(PixelmonWrapper oldPokemon, int[] newPixelmonId) {
        int index = oldPokemon.getControlledIndex();
        if (index == -1 && this.bc.simulateMode) {
            index = 0;
        }
        String beforeName = oldPokemon.getNickname();
        oldPokemon.beforeSwitch();
        if (!oldPokemon.isFainted() && !oldPokemon.nextSwitchIsMove) {
            this.bc.sendToOthers("playerparticipant.withdrew", this, this.getTranslatedName(), beforeName);
        }
        PixelmonWrapper newWrapper = this.getPokemonFromParty(newPixelmonId);
        if (!this.bc.simulateMode) {
            EntityPixelmon pixelmon;
            oldPokemon.pokemon.catchInPokeball();
            oldPokemon.pokemon = null;
            newWrapper.pokemon = pixelmon = this.trainer.releasePokemon(newPixelmonId);
        }
        if (this.trainer.getBossMode() != EnumBossMode.NotBoss) {
            int lvl = 1;
            for (BattleParticipant p : this.bc.participants) {
                if (p.team == this.team || !(p instanceof PlayerParticipant)) continue;
                lvl = Math.max(lvl, ((PlayerParticipant)p).getHighestLevel());
            }
            newWrapper.getLevel().setLevel(lvl += this.trainer.getBossMode().extraLevels);
        }
        this.controlledPokemon.set(index, newWrapper);
        newWrapper.getBattleAbility().beforeSwitch(newWrapper);
        if (!this.bc.simulateMode) {
            this.bc.sendToOthers("pixelmon.battletext.sentout", this, this.getTranslatedName(), newWrapper.getNickname());
            this.bc.participants.forEach(BattleParticipant::updateOtherPokemon);
        }
        newWrapper.afterSwitch();
        return newWrapper;
    }

    @Override
    public boolean checkPokemon() {
        for (NBTTagCompound n : this.trainer.getPokemonStorage().getList()) {
            if (n == null || n.getInteger("PixelmonNumberMoves") != 0) continue;
            if (PixelmonConfig.printErrors) {
                Pixelmon.LOGGER.info("Couldn't load Pok\u00e9mon's moves.");
            }
            return false;
        }
        return true;
    }

    @Override
    public void updatePokemon(PixelmonWrapper p) {
    }

    @Override
    public EntityLiving getEntity() {
        return this.trainer.getEntity();
    }

    @Override
    public void updateOtherPokemon() {
    }

    @Override
    public String getDisplayName() {
        return this.getTranslatedName();
    }

    @Override
    public PlayerStorage getStorage() {
        return this.trainer.getPokemonStorage();
    }
}

