/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.ai;

import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.controller.CalcPriority;
import com.pixelmongenerations.common.battle.controller.ai.AggressiveAI;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.ai.OpponentMemory;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.log.MoveResults;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.util.PixelmonMethods;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;

public class TacticalAI
extends AggressiveAI {
    ArrayList<OpponentMemory> memory = new ArrayList(6);

    public TacticalAI(BattleParticipant participant) {
        super(participant);
    }

    @Override
    public MoveChoice getNextMove(PixelmonWrapper pw) {
        ArrayList<MoveChoice> choices = this.getWeightedAttackChoices(pw);
        ArrayList<MoveChoice> bestChoices = this.getBestChoices(choices);
        return this.pickBestChoice(choices, bestChoices);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected ArrayList<MoveChoice> getWeightedAttackChoices(PixelmonWrapper pw) {
        ArrayList<MoveChoice> choices = this.getAttackChoices(pw);
        ArrayList<PixelmonWrapper> allies = pw.getTeamPokemonExcludeSelf();
        ArrayList<PixelmonWrapper> saveTurnList = this.bc.turnList;
        boolean wasSimulateMode = this.bc.simulateMode;
        this.bc.simulateMode = true;
        try {
            this.bc.modifyStats();
            this.bc.modifyStatsCancellable(pw);
            for (MoveChoice choice : choices) {
                if (choice.attack.getAttackCategory() == AttackCategory.Status) {
                    this.simulateStatusMove(pw, choice);
                    continue;
                }
                this.weightOffensiveMove(pw, choice, allies);
            }
            ArrayList<MoveChoice> opponentChoices = new ArrayList<MoveChoice>();
            ArrayList<MoveChoice> bestOpponentChoices = new ArrayList<MoveChoice>();
            for (PixelmonWrapper opponent : pw.getOpponentPokemon()) {
                ArrayList<MoveChoice> singleOpponentChoices = pw.getBattleAI().getWeightedOffensiveChoices(opponent);
                opponentChoices.addAll(singleOpponentChoices);
                bestOpponentChoices.addAll(this.getBestChoices(singleOpponentChoices));
            }
            for (int i = 0; i < opponentChoices.size(); ++i) {
                MoveChoice opponentChoice = (MoveChoice)opponentChoices.get(i);
                if (opponentChoice.tier != 0) continue;
                opponentChoices.remove(i--);
            }
            saveTurnList = this.bc.turnList;
            CalcPriority.checkMoveSpeed(pw.bc);
            ArrayList<MoveChoice> bestChoices = this.getBestChoices(choices, true);
            for (MoveChoice choice : choices) {
                if (!choice.isAttack() || choice.isOffensiveMove() && choice.tier == 0 && choice.result.result != AttackResult.hit || choice.result != null && choice.result.result == AttackResult.failed) continue;
                pw.setAttack(choice.attack, choice.targets, false, false);
                for (EffectBase effect : choice.attack.getAttackBase().effects) {
                    effect.weightEffect(pw, choice, choices, bestChoices, opponentChoices, bestOpponentChoices);
                }
                if (MoveChoice.canOutspeedAndOHKO(bestOpponentChoices, pw, choice.createList())) {
                    if (choice.tier == 2) {
                        choice.lowerTier(1);
                        continue;
                    }
                    if (choice.tier < 3) continue;
                    choice.weight = 0.0f;
                    continue;
                }
                if (choice.tier < 3) continue;
                float priority = choice.attack.getAttackBase().getPriority();
                if (priority > 0.0f) {
                    ++choice.tier;
                    continue;
                }
                if (priority >= 0.0f) continue;
                choice.weight = 1.0f;
            }
        }
        finally {
            this.bc.simulateMode = wasSimulateMode;
            this.bc.turnList = saveTurnList;
        }
        return choices;
    }

    protected MoveChoice pickBestChoice(ArrayList<MoveChoice> bestChoices) {
        return this.pickBestChoice(null, bestChoices);
    }

    protected MoveChoice pickBestChoice(ArrayList<MoveChoice> choices, ArrayList<MoveChoice> bestChoices) {
        if (bestChoices.isEmpty()) {
            return choices == null ? null : RandomHelper.getRandomElementFromList(choices);
        }
        if (bestChoices.size() > 1 && bestChoices.get(0).isMiddleTier()) {
            float totalWeight = 0.0f;
            for (MoveChoice choice : bestChoices) {
                totalWeight += choice.weight;
            }
            float random = RandomHelper.getRandomNumberBetween(0.0f, totalWeight);
            float counter = 0.0f;
            for (MoveChoice choice : bestChoices) {
                counter += choice.weight;
                if (counter < random) continue;
                return choice;
            }
            return bestChoices.get(bestChoices.size() - 1);
        }
        return RandomHelper.getRandomElementFromList(bestChoices);
    }

    @Override
    public void registerMove(PixelmonWrapper user) {
        OpponentMemory pokemon = this.getMemory(user);
        if (pokemon != null && user.attack != null) {
            pokemon.seeAttack(user.attack);
        }
    }

    public void simulateStatusMove(PixelmonWrapper pw, MoveChoice choice) {
        pw.setAttack(choice.attack, choice.targets, false, false);
        for (int j = 0; j < choice.targets.size(); ++j) {
            PixelmonWrapper target = choice.targets.get(j);
            MoveResults result = new MoveResults(target);
            result.priority = CalcPriority.calculatePriority(pw);
            choice.result = result;
            choice.attack.saveAttack();
            choice.attack.use(pw, target, result);
            choice.attack.restoreAttack();
        }
    }

    protected OpponentMemory getMemory(PixelmonWrapper pw) {
        if (this.participant.getOpponentPokemon().contains(pw)) {
            int[] userID = pw.getPokemonID();
            OpponentMemory pokemon = null;
            for (OpponentMemory p : this.memory) {
                if (!PixelmonMethods.isIDSame(p.pokemonID, userID)) continue;
                pokemon = p;
                break;
            }
            if (pokemon == null) {
                pokemon = new OpponentMemory(pw);
                this.memory.add(pokemon);
            }
            return pokemon;
        }
        return null;
    }

    protected ArrayList<MoveChoice> getBestChoices(ArrayList<MoveChoice> choices) {
        return this.getBestChoices(choices, false);
    }

    protected ArrayList<MoveChoice> getBestChoices(ArrayList<MoveChoice> choices, boolean excludeStatus) {
        ArrayList<MoveChoice> bestChoices = new ArrayList<MoveChoice>(choices.size());
        if (excludeStatus) {
            for (MoveChoice choice : choices) {
                MoveChoice.checkBestChoice(choice, bestChoices, excludeStatus);
            }
        } else {
            MoveChoice bestChoice = null;
            ArrayList<MoveChoice> bestOffensiveChoices = new ArrayList<MoveChoice>(choices.size());
            ArrayList<MoveChoice> bestSwitchChoices = new ArrayList<MoveChoice>(choices.size());
            ArrayList<MoveChoice> statusChoices = new ArrayList<MoveChoice>(choices.size());
            for (MoveChoice choice : choices) {
                if (choice.tier <= 0) continue;
                if (bestChoice == null || bestChoice.compareTo(choice) < 0) {
                    bestChoice = choice;
                }
                if (choice.isOffensiveMove()) {
                    this.compareToBestChoices(choice, bestOffensiveChoices);
                }
                if (!choice.isAttack()) {
                    this.compareToBestChoices(choice, bestSwitchChoices);
                }
                if (!choice.isStatusMove()) continue;
                statusChoices.add(choice);
            }
            if (bestChoice != null) {
                bestChoices.add(bestChoice);
                this.addSubchoices(bestChoice, bestChoices, bestOffensiveChoices);
                this.addSubchoices(bestChoice, bestChoices, bestSwitchChoices);
                this.addSubchoices(bestChoice, bestChoices, statusChoices);
            }
        }
        return bestChoices;
    }

    private void compareToBestChoices(MoveChoice choice, ArrayList<MoveChoice> bestChoices) {
        float currentComparison = 0.0f;
        if (!bestChoices.isEmpty()) {
            currentComparison = bestChoices.get(0).compareTo(choice);
        }
        if (currentComparison <= 0.0f) {
            if (currentComparison < 0.0f) {
                bestChoices.clear();
            }
            bestChoices.add(choice);
        }
    }

    private void addSubchoices(MoveChoice bestChoice, ArrayList<MoveChoice> bestChoices, ArrayList<MoveChoice> subchoices) {
        for (MoveChoice subchoice : subchoices) {
            if (subchoice == bestChoice || !subchoice.isSimilarWeight(bestChoice)) continue;
            bestChoices.add(subchoice);
        }
    }
}

