/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.log;

import com.pixelmongenerations.common.battle.controller.log.BattleActionBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.PixelmonItem;

public class BagItemAction
extends BattleActionBase {
    PixelmonItem item;
    String recipient;

    public BagItemAction(int turn, int pokemonPosition, PixelmonWrapper pokemon, String recipient, PixelmonItem item) {
        super(turn, pokemonPosition, pokemon);
        this.item = item;
        this.recipient = recipient;
    }
}

