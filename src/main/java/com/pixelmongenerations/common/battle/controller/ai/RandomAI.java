/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.ai;

import com.pixelmongenerations.common.battle.controller.ai.BattleAIBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class RandomAI
extends BattleAIBase {
    public RandomAI(BattleParticipant participant) {
        super(participant);
    }

    @Override
    public MoveChoice getNextMove(PixelmonWrapper pw) {
        return this.getRandomAttackChoice(pw);
    }

    @Override
    public int[] getNextSwitch(PixelmonWrapper pw) {
        return RandomHelper.getRandomElementFromList(this.getPossibleSwitchIDs());
    }
}

