/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.participants;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.battle.EnumBattleMusic;
import com.pixelmongenerations.core.network.packetHandlers.SetBattleMusic;
import com.pixelmongenerations.core.network.packetHandlers.battles.BattleMessage;
import com.pixelmongenerations.core.network.packetHandlers.battles.gui.HPPacket;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class Spectator {
    private EntityPlayerMP player;
    public String watchedName;

    public Spectator(EntityPlayerMP player, String watchedName) {
        this.player = player;
        this.watchedName = watchedName;
    }

    public EntityPlayerMP getEntity() {
        return this.player;
    }

    public void sendDamagePacket(PixelmonWrapper user, int damage) {
        this.sendMessage(new HPPacket(user, -damage));
    }

    public void sendHealPacket(PixelmonWrapper target, int amount) {
        this.sendMessage(new HPPacket(target, amount));
    }

    public void sendBattleMessage(TextComponentTranslation message) {
        this.sendMessage(new BattleMessage(message));
    }

    public void sendMusicPacket(EnumBattleMusic music) {
        this.sendMessage(new SetBattleMusic(music));
    }

    public void sendMessage(IMessage message) {
        Pixelmon.NETWORK.sendTo(message, this.player);
    }
}

