/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.ai;

import com.pixelmongenerations.common.battle.attacks.Effectiveness;
import com.pixelmongenerations.common.battle.controller.CalcPriority;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.ai.TacticalAI;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.EntryHazard;
import com.pixelmongenerations.common.battle.status.Perish;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class AdvancedAI
extends TacticalAI {
    public AdvancedAI(BattleParticipant participant) {
        super(participant);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public MoveChoice getNextMove(PixelmonWrapper pw) {
        ArrayList<MoveChoice> choices = this.getWeightedAttackChoices(pw);
        ArrayList<MoveChoice> bestChoices = this.getBestChoices(choices);
        MoveChoice bestChoice = this.pickBestChoice(choices, bestChoices);
        ArrayList<MoveChoice> switchChoices = this.getSwitchChoices(pw);
        ArrayList<PixelmonWrapper> opponents = pw.getOpponentPokemon();
        if (!switchChoices.isEmpty()) {
            int controlledIndex = pw.getControlledIndex();
            int statBoostMod = pw.getBattleStats().getSumStages() * -20;
            boolean willFaintPerishSong = false;
            Perish perishSong = (Perish)pw.getStatus(StatusType.Perish);
            if (perishSong != null) {
                willFaintPerishSong = perishSong.effectTurns == 1;
            }
            boolean wasSimulateMode = this.bc.simulateMode;
            this.bc.simulateMode = true;
            try {
                ArrayList<MoveChoice> bestOpponentChoicesBefore = this.getNextOpponentMoves(pw, opponents);
                float opponentChoicesBeforeSum = MoveChoice.sumWeights(bestOpponentChoicesBefore);
                for (MoveChoice switchChoice : switchChoices) {
                    pw.newPokemonID = switchChoice.switchPokemon;
                    PixelmonWrapper nextPokemon = pw.doSwitch();
                    int totalHazardDamage = 0;
                    int beforeHealth = nextPokemon.getHealth();
                    for (EntryHazard hazard2 : pw.getEntryHazards().stream().filter(hazard -> !hazard.isImmune(nextPokemon)).collect(Collectors.toList())) {
                        int damage = hazard2.getDamage(nextPokemon);
                        if (damage > 0) {
                            totalHazardDamage += damage;
                            switchChoice.raiseWeight(-damage);
                            continue;
                        }
                        float weight = -hazard2.getAIWeight() * hazard2.getNumLayers();
                        if (hazard2.type == StatusType.ToxicSpikes) {
                            if (pw.hasType(EnumType.Poison)) {
                                weight = -weight;
                            }
                        }
                        switchChoice.raiseWeight(weight);
                    }
                    if (totalHazardDamage >= beforeHealth) {
                        switchChoice.lowerTier(0);
                        continue;
                    }
                    nextPokemon.setHealth(beforeHealth - totalHazardDamage);
                    try {
                        MoveChoice choice = this.getNextMoveAttackOnly(nextPokemon);
                        CalcPriority.checkMoveSpeed(pw.bc);
                        ArrayList<MoveChoice> bestOpponentChoicesAfter = this.getNextOpponentMoves(pw, opponents);
                        if (choice.weight <= 0.0f || MoveChoice.canOutspeedAnd2HKO(bestOpponentChoicesAfter, nextPokemon)) continue;
                        float userChoiceWeight = choice.weight - bestChoice.weight;
                        float opponentChoiceWeight = opponentChoicesBeforeSum - MoveChoice.sumWeights(bestOpponentChoicesAfter);
                        for (PixelmonWrapper opponent : opponents) {
                            if (opponent.lastAttack == null || opponent.lastAttack.getAttackCategory() == AttackCategory.Status || opponent.lastAttack.moveResult.target != pw || opponent.lastAttack.getTypeEffectiveness(opponent, nextPokemon) != (double)Effectiveness.None.value) continue;
                            userChoiceWeight += 30.0f;
                        }
                        switchChoice.raiseWeight((userChoiceWeight += (float)statBoostMod) + opponentChoiceWeight);
                        if (!willFaintPerishSong) continue;
                        switchChoice.raiseTier(3);
                    }
                    finally {
                        nextPokemon.setHealth(beforeHealth);
                        this.resetSwitchSimulation(pw, controlledIndex, nextPokemon);
                    }
                }
            }
            finally {
                this.bc.simulateMode = wasSimulateMode;
            }
            choices.addAll(switchChoices);
        }
        bestChoices = this.getBestChoices(choices);
        bestChoice = this.pickBestChoice(choices, bestChoices);
        return bestChoice;
    }

    @Override
    protected MoveChoice getNextMoveAttackOnly(PixelmonWrapper pw) {
        return super.getNextMove(pw);
    }

    protected ArrayList<MoveChoice> getNextOpponentMoves(PixelmonWrapper pw, ArrayList<PixelmonWrapper> opponents) {
        ArrayList<MoveChoice> bestOpponentChoices = new ArrayList<MoveChoice>();
        for (PixelmonWrapper opponent : pw.getOpponentPokemon()) {
            ArrayList<MoveChoice> singleOpponentChoices = pw.getBattleAI().getWeightedOffensiveChoices(opponent);
            MoveChoice bestChoice = this.pickBestChoice(this.getBestChoices(singleOpponentChoices));
            if (bestChoice == null) continue;
            bestOpponentChoices.add(bestChoice);
        }
        return bestOpponentChoices;
    }

    @Override
    protected boolean validateSwitch(PixelmonWrapper nextPokemon) {
        this.bc.modifyStats();
        this.bc.modifyStatsCancellable(nextPokemon);
        ArrayList<PixelmonWrapper> saveTurnList = this.bc.turnList;
        CalcPriority.checkMoveSpeed(nextPokemon.bc);
        ArrayList<MoveChoice> opponentChoices = this.getNextOpponentMoves(nextPokemon, nextPokemon.getOpponentPokemon());
        boolean canSurvive = !MoveChoice.canOutspeedAndOHKO(opponentChoices, nextPokemon, this.getBestChoices(this.getWeightedAttackChoices(nextPokemon)));
        this.bc.turnList = saveTurnList;
        return canSurvive;
    }
}

