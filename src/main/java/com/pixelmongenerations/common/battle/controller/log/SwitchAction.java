/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.log;

import com.pixelmongenerations.common.battle.controller.log.BattleActionBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class SwitchAction
extends BattleActionBase {
    public int[] switchingTo;

    public SwitchAction(int turn, int pokemonPosition, PixelmonWrapper pokemon, int[] switchingTo) {
        super(turn, pokemonPosition, pokemon);
        this.switchingTo = switchingTo;
    }
}

