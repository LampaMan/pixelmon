/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.log;

import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class MoveResults {
    public PixelmonWrapper target;
    public int damage;
    public int fullDamage;
    public int accuracy;
    public float priority;
    public AttackResult result = AttackResult.proceed;
    public float weightMod;

    public MoveResults(PixelmonWrapper target) {
        this.target = target;
    }

    public MoveResults(PixelmonWrapper target, int damage, AttackResult result) {
        this.target = target;
        this.damage = damage;
        this.fullDamage = damage;
        this.result = result;
    }

    public MoveResults(PixelmonWrapper target, int damage, float priority, AttackResult result) {
        this(target, damage, result);
        this.priority = priority;
    }
}

