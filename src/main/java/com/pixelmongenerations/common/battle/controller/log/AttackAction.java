/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.log;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.log.BattleActionBase;
import com.pixelmongenerations.common.battle.controller.log.MoveResults;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.database.DatabaseMoves;

public class AttackAction
extends BattleActionBase {
    public final int attackID;
    public final MoveResults[] moveResults;

    public AttackAction(int turn, int pokemonPosition, PixelmonWrapper pokemon, int attackID, MoveResults[] moveResults) {
        super(turn, pokemonPosition, pokemon);
        this.moveResults = moveResults;
        this.attackID = attackID;
    }

    public Attack getAttack() {
        return DatabaseMoves.getAttack(this.attackID);
    }

    public MoveResults getResultsForPokemon(PixelmonWrapper target) {
        for (MoveResults mr : this.moveResults) {
            if (mr == null || mr.target != target) continue;
            return mr;
        }
        return null;
    }
}

