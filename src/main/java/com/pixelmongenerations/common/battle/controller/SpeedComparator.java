/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.TrickRoom;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.Comparator;

public class SpeedComparator
implements Comparator<PixelmonWrapper> {
    @Override
    public int compare(PixelmonWrapper pw1, PixelmonWrapper pw2) {
        return this.doesGoFirst(pw1, pw2) ? -1 : 1;
    }

    protected boolean doesGoFirst(PixelmonWrapper p, PixelmonWrapper foe) {
        try {
            for (int i = 0; i < p.bc.globalStatusController.getGlobalStatusSize(); ++i) {
                if (!(p.bc.globalStatusController.getGlobalStatus(i) instanceof TrickRoom)) continue;
                return ((TrickRoom)p.bc.globalStatusController.getGlobalStatus(i)).participantMovesFirst(p, foe);
            }
        }
        catch (Exception e) {
            p.bc.battleLog.onCrash(e, "Caught error in participantMovesFirst, cause in doesGoFirst().");
        }
        if (p.getBattleStats().getStatWithMod(StatsType.Speed) > foe.getBattleStats().getStatWithMod(StatsType.Speed)) {
            return true;
        }
        if (foe.getBattleStats().getStatWithMod(StatsType.Speed) > p.getBattleStats().getStatWithMod(StatsType.Speed)) {
            return false;
        }
        return RandomHelper.getRandomNumberBetween(0, 1) < 1;
    }
}

