/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.log;

public enum AttackResult {
    proceed,
    hit,
    ignore,
    killed,
    succeeded,
    charging,
    unable,
    failed,
    missed,
    notarget;

}

