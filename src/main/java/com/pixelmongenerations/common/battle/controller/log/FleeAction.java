/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.log;

import com.pixelmongenerations.common.battle.controller.log.BattleActionBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class FleeAction
extends BattleActionBase {
    public FleeAction(int turn, int pokemonPosition, PixelmonWrapper pokemon) {
        super(turn, pokemonPosition, pokemon);
    }
}

