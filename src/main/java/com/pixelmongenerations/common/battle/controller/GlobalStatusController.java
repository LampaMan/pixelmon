/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller;

import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.NoTerrain;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Terrain;
import com.pixelmongenerations.common.battle.status.Weather;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class GlobalStatusController {
    public BattleControllerBase bc;
    private ArrayList<GlobalStatusBase> globalStatuses = new ArrayList();

    public GlobalStatusController(BattleControllerBase bc) {
        this.bc = bc;
    }

    public ArrayList<GlobalStatusBase> getGlobalStatuses() {
        ArrayList<GlobalStatusBase> effectiveStatuses = new ArrayList<GlobalStatusBase>(this.globalStatuses.size());
        effectiveStatuses.addAll(this.globalStatuses.stream().filter(status -> !status.isWeather() || !GlobalStatusBase.ignoreWeather(this.bc)).collect(Collectors.toList()));
        return effectiveStatuses;
    }

    public Weather getWeather() {
        if (GlobalStatusBase.ignoreWeather(this.bc)) {
            return null;
        }
        return this.getWeatherIgnoreAbility();
    }

    public Weather getWeatherIgnoreAbility() {
        return this.globalStatuses.stream().filter(Weather.class::isInstance).map(Weather.class::cast).findFirst().orElse(null);
    }

    public Terrain getTerrain() {
        return this.globalStatuses.stream().filter(GlobalStatusBase::isTerrain).map(Terrain.class::cast).findAny().orElse(new NoTerrain());
    }

    public boolean removeGlobalStatus(StatusType status) {
        for (GlobalStatusBase g : this.globalStatuses) {
            if (g.type != status) continue;
            if (!this.bc.simulateMode) {
                this.globalStatuses.remove(g);
            }
            return true;
        }
        return false;
    }

    public void removeGlobalStatus(GlobalStatusBase g) {
        if (!this.bc.simulateMode) {
            this.globalStatuses.remove(g);
            if (g.isWeather()) {
                this.bc.sendToAll(((Weather)g).getLangEnd(), new Object[0]);
                this.triggerWeatherChange(null);
            }
        }
    }

    public void addGlobalStatus(GlobalStatusBase g) {
        if (!this.bc.simulateMode) {
            if (g.isWeather()) {
                this.globalStatuses.removeIf(GlobalStatusBase::isWeather);
                if (!GlobalStatusBase.ignoreWeather(this.bc)) {
                    this.triggerWeatherChange((Weather)g);
                }
            }
            this.globalStatuses.add(g);
        }
    }

    public GlobalStatusBase getGlobalStatus(int index) {
        return this.globalStatuses.get(index);
    }

    public GlobalStatusBase getGlobalStatus(StatusType type) {
        return this.globalStatuses.stream().filter(g -> g.type == type).findFirst().orElse(null);
    }

    public int getGlobalStatusSize() {
        return this.globalStatuses.size();
    }

    public boolean hasStatus(StatusType type) {
        return this.getGlobalStatus(type) != null;
    }

    public void endBattle() {
        this.globalStatuses.clear();
    }

    public void triggerWeatherChange(Weather weather) {
        for (PixelmonWrapper pw : this.bc.getActiveUnfaintedPokemon()) {
            pw.getBattleAbility().onWeatherChange(pw, weather);
        }
    }
}

