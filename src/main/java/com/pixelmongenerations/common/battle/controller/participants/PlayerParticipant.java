/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.participants;

import com.pixelmongenerations.client.gui.battles.PixelmonInGui;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.TerrainExamine;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.ai.RandomAI;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.Spectator;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.WrapperLink;
import com.pixelmongenerations.common.item.ItemShrineOrbWithInfo;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.enums.EnumMark;
import com.pixelmongenerations.core.enums.battle.EnumBattleEndCause;
import com.pixelmongenerations.core.enums.battle.EnumBattleMusic;
import com.pixelmongenerations.core.enums.forms.EnumBurmy;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.SetBattleMusic;
import com.pixelmongenerations.core.network.packetHandlers.battles.BackToMainMenu;
import com.pixelmongenerations.core.network.packetHandlers.battles.EnforcedSwitch;
import com.pixelmongenerations.core.network.packetHandlers.battles.ExitBattle;
import com.pixelmongenerations.core.network.packetHandlers.battles.SetBattlingPokemon;
import com.pixelmongenerations.core.network.packetHandlers.battles.SetPokemonBattleData;
import com.pixelmongenerations.core.network.packetHandlers.battles.SetPokemonTeamData;
import com.pixelmongenerations.core.network.packetHandlers.battles.StartBattle;
import com.pixelmongenerations.core.network.packetHandlers.battles.gui.HPPacket;
import com.pixelmongenerations.core.network.packetHandlers.clientStorage.Add;
import com.pixelmongenerations.core.network.packetHandlers.pcClientStorage.PCClear;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.AirSaver;
import com.pixelmongenerations.core.util.PixelmonMethods;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentBase;
import net.minecraft.util.text.TextComponentString;

public class PlayerParticipant
extends BattleParticipant {
    public EntityPlayerMP player;
    public PlayerStorage storage;
    long guiCheck = 0L;
    private static int idcounter = 0;
    public boolean hasAmuletCoin = false;
    public boolean hasHappyHour = false;
    public int payDay = 0;
    public int goldRush = 0;
    public int goldRushSuccession = 100;
    private AirSaver airSaver;
    private PlayerStorage cachedStorage = null;

    public PlayerParticipant(EntityPlayerMP p, EntityPixelmon ... startingPixelmon) {
        super(startingPixelmon.length);
        this.player = p;
        this.loadParty(this.getStorage());
        this.initialize(p, startingPixelmon);
    }

    public PlayerParticipant(EntityPlayerMP p, List<PokemonLink> teamSelection, int numControlledPokemon) {
        super(numControlledPokemon);
        this.loadParty(teamSelection);
        EntityPixelmon[] startingPixelmon = new EntityPixelmon[numControlledPokemon];
        for (int i = 0; i < numControlledPokemon; ++i) {
            if (teamSelection.size() <= i) continue;
            startingPixelmon[i] = teamSelection.get(i).getEntity();
        }
        this.initialize(p, startingPixelmon);
    }

    private void initialize(EntityPlayerMP p, EntityPixelmon ... startingPixelmon) {
        this.player = p;
        this.storage = this.getStorage();
        int positionIndex = 0;
        for (EntityPixelmon pokemon : startingPixelmon) {
            PixelmonWrapper pw;
            if (pokemon == null || (pw = this.getPokemonFromParty(pokemon)) == null) continue;
            this.controlledPokemon.add(pw);
            pw.battlePosition = positionIndex++;
        }
        this.airSaver = new AirSaver(p);
        p.setJumping(false);
        p.moveForward = 0.0f;
        p.moveStrafing = 0.0f;
        p.setSneaking(false);
    }

    private void enableReturnHeldItems() {
        for (PixelmonWrapper pw : this.allPokemon) {
            pw.enableReturnHeldItem();
        }
    }

    @Override
    public ParticipantType getType() {
        return ParticipantType.Player;
    }

    @Override
    public boolean canGainXP() {
        return true;
    }

    @Override
    public boolean hasMorePokemonReserve() {
        return this.countAblePokemon() > this.getActiveUnfaintedPokemon().size() + this.switchingOut.size();
    }

    @Override
    public void startBattle(BattleControllerBase bc) {
        super.startBattle(bc);
        this.setBattleAI(new RandomAI(this));
        this.player.capabilities.disableDamage = true;
        Pixelmon.NETWORK.sendTo(new PCClear(), this.player);
        boolean useTurnTime = false;
        int activateTime = 0;
        int turnTime = 0;
        if (bc.rules.turnTime > 0) {
            useTurnTime = true;
            activateTime = bc.rules.turnTime;
            turnTime = bc.rules.turnTime;
        } else if (PixelmonServerConfig.afkHandlerOn) {
            useTurnTime = true;
            activateTime = PixelmonServerConfig.afkTimerActivateSeconds;
            turnTime = PixelmonConfig.afkTimerTurnSeconds;
        }
        ParticipantType[][] battleType = bc.getBattleType(this);
        StartBattle startPacket = useTurnTime ? new StartBattle(bc.battleIndex, battleType, activateTime, turnTime, bc.rules) : new StartBattle(bc.battleIndex, battleType, bc.rules);
        Pixelmon.NETWORK.sendTo(startPacket, this.player);
        this.storage.pokedex.sendToPlayer(this.player);
        if (bc.rules.fullHeal || PixelmonConfig.returnHeldItems && bc.isPvP()) {
            this.enableReturnHeldItems();
        }
        int i = 0;
        while (i < this.allPokemon.length) {
            PixelmonWrapper pw = this.allPokemon[i];
            PixelmonData data = new PixelmonData(new WrapperLink(pw));
            data.inBattle = true;
            data.order = i++;
            Pixelmon.NETWORK.sendTo(new Add(data), this.player);
        }
        Pixelmon.NETWORK.sendTo(new SetBattlingPokemon(this.getTeamPokemonList()), this.player);
        PixelmonMethods.getAllActivePokemon(this.player).forEach(EntityPixelmon::unloadEntity);
        for (PixelmonWrapper pw : this.controlledPokemon) {
            if (!PixelmonConfig.useBattleDimension) {
                if (!this.storage.isInWorld(pw.getPokemonID())) {
                    pw.pokemon.setLocationAndAngles(this.player.posX, this.player.posY, this.player.posZ, this.player.rotationYaw, 0.0f);
                    pw.pokemon.releaseFromPokeball();
                }
                pw.pokemon.isDead = false;
            }
            if (PixelmonConfig.useBattleDimension) continue;
            pw.pokemon.battleController = bc;
        }
    }

    public ArrayList<PixelmonWrapper> getTeamPokemonList() {
        ArrayList<BattleParticipant> team = this.bc.getTeam(this);
        ArrayList<PixelmonWrapper> teamPokemon = new ArrayList<PixelmonWrapper>();
        for (BattleParticipant p : team) {
            teamPokemon.addAll(p.controlledPokemon);
        }
        return teamPokemon;
    }

    @Override
    public void endBattle() {
        MinecraftServer server;
        if (!this.player.capabilities.isCreativeMode) {
            this.player.capabilities.disableDamage = false;
        }
        int burmyForm = -1;
        for (PixelmonWrapper pixelmonWrapper : this.allPokemon) {
            NBTTagCompound nbt;
            if (!pixelmonWrapper.changeBurmy || this.controlledPokemon.isEmpty()) continue;
            if (burmyForm == -1) {
                burmyForm = EnumBurmy.getFromType(TerrainExamine.getTerrain((PixelmonWrapper)this.controlledPokemon.get(0))).ordinal();
            }
            if ((nbt = this.storage.getNBT(pixelmonWrapper.getPokemonID())) == null) continue;
            nbt.setInteger("Variant", burmyForm);
            this.storage.updateClient(nbt, EnumUpdateType.Stats);
        }
        for (PixelmonWrapper pw : this.controlledPokemon) {
            pw.resetOnSwitch();
            if (pw.pokemon == null) continue;
            if (pw.pokemon.isMega || pw.pokemon.isPrimal) {
                pw.pokemon.setForm(-1);
                pw.pokemon.isMega = false;
                pw.pokemon.isPrimal = false;
            }
            if (pw.pokemon.isAsh) {
                pw.pokemon.setForm(1);
                pw.pokemon.isAsh = false;
            }
            if (pw.pokemon.isUltra) {
                if (pw.getForm() == 3) {
                    pw.pokemon.setForm(1);
                }
                if (pw.getForm() == 4) {
                    pw.pokemon.setForm(2);
                }
                pw.isUltra = false;
            }
            pw.pokemon.endBattle();
            pw.pokemon.catchInPokeball();
        }
        for (PixelmonWrapper pixelmonWrapper : this.allPokemon) {
            if (pixelmonWrapper.nbt == null) continue;
            pixelmonWrapper.resetBattleEvolution();
            pixelmonWrapper.writeToNBT();
            this.storage.updateClient(pixelmonWrapper.nbt, EnumUpdateType.HP, EnumUpdateType.Ability, EnumUpdateType.HeldItem, EnumUpdateType.Stats, EnumUpdateType.Status, EnumUpdateType.Moveset);
        }
        int moneyPool = 0;
        if (this.goldRush > 0) {
            moneyPool += this.goldRush;
        }
        if (this.payDay > 0 && this.hasMorePokemon()) {
            this.payDay *= this.getPrizeMoneyMultiplier();
            ChatHandler.sendBattleMessage(this.getEntity(), "pixelmon.effect.paydayend", this.getName(), this.payDay);
            moneyPool += this.payDay;
        }
        if (moneyPool > 0) {
            if (moneyPool > 99999) {
                moneyPool = 99999;
            }
            this.storage.addCurrency(moneyPool);
            ChatHandler.sendBattleMessage(this.getEntity(), "pixelmon.effect.battlemoney", moneyPool);
        }
        Pixelmon.NETWORK.sendTo(new ExitBattle(), this.player);
        Pixelmon.NETWORK.sendTo(new PCClear(), this.player);
        if (this.bc != null && this.bc.participants.size() == 2 && this.bc.otherParticipant(this) instanceof PlayerParticipant) {
            if (!this.hasMorePokemon()) {
                this.storage.stats.addLoss();
            } else {
                this.storage.stats.addWin();
            }
        }
        if ((server = this.player.world.getMinecraftServer()) != null && server.worlds.length > 0) {
            PixelmonStorage.save(this.player);
        }
    }

    @Override
    public void getNextPokemon(int position) {
        if (position >= this.controlledPokemon.size()) {
            this.wait = false;
            return;
        }
        boolean switching = false;
        PixelmonWrapper switchingPokemon = (PixelmonWrapper)this.controlledPokemon.get(position);
        switchingPokemon.isSwitching = true;
        switching = true;
        switchingPokemon.wait = true;
        if (switchingPokemon.newPokemonID == null) {
            Pixelmon.NETWORK.sendTo(new EnforcedSwitch(position), this.player);
        }
        if (!switching) {
            this.wait = false;
        }
    }

    public int getPrizeMoneyMultiplier() {
        float multiplier = 1.0f;
        if (this.hasAmuletCoin) {
            multiplier *= PixelmonConfig.amuletCoinMultiplier;
        }
        if (this.hasHappyHour) {
            multiplier *= PixelmonConfig.happyHourMultiplier;
        }
        return (int)multiplier;
    }

    @Override
    public int[] getNextPokemonID() {
        return this.storage.getFirstAblePokemon(this.getEntity().world).getPokemonId();
    }

    @Override
    public TextComponentBase getName() {
        return new TextComponentString(this.player.getDisplayName().getUnformattedText());
    }

    @Override
    public void selectAction() {
        this.getMove(null);
    }

    @Override
    public MoveChoice getMove(PixelmonWrapper pokemon) {
        if (this.bc == null) {
            return null;
        }
        boolean canSwitch = true;
        boolean canFlee = true;
        ArrayList<PixelmonWrapper> pokemonToChoose = new ArrayList<PixelmonWrapper>();
        for (PixelmonWrapper p : this.controlledPokemon) {
            if (p.getMoveset().isEmpty()) {
                this.bc.endBattleWithoutXP();
                return null;
            }
            boolean[] canExit = PlayerParticipant.canSwitch(p);
            canSwitch = canExit[0];
            canFlee = canExit[1];
            if (p.attack == null || !p.attack.doesPersist(p)) {
                pokemonToChoose.add(p);
                p.wait = true;
                continue;
            }
            p.wait = false;
        }
        if (!pokemonToChoose.isEmpty()) {
            Pixelmon.NETWORK.sendTo(new BackToMainMenu(canSwitch, canFlee, pokemonToChoose), this.player);
            ArrayList<Spectator> playerSpectators = this.bc.getPlayerSpectators(this);
            playerSpectators.forEach(spectator -> spectator.sendMessage(new BackToMainMenu(true, true, pokemonToChoose)));
        }
        return null;
    }

    @Override
    public PixelmonWrapper switchPokemon(PixelmonWrapper pw, int[] newPixelmonId) {
        double x = this.player.posX;
        double y = this.player.posY;
        double z = this.player.posZ;
        String beforeName = pw.getNickname();
        pw.beforeSwitch();
        if (!pw.isFainted() && !pw.nextSwitchIsMove) {
            ChatHandler.sendBattleMessage(this.player, "playerparticipant.enough", pw.getNickname());
            this.bc.sendToOthers("playerparticipant.withdrew", this, this.player.getDisplayName().getUnformattedText(), beforeName);
        }
        PixelmonWrapper newWrapper = this.getPokemonFromParty(newPixelmonId);
        if (!this.bc.simulateMode) {
            pw.pokemon.catchInPokeball();
            pw.pokemon = null;
            Optional<EntityPixelmon> newPokemonOptional = this.storage.getAlreadyExists(newPixelmonId, this.player.world);
            EntityPixelmon newPixelmon = null;
            if (newPokemonOptional.isPresent()) {
                newPixelmon = newPokemonOptional.get();
                newPixelmon.motionZ = 0.0;
                newPixelmon.motionY = 0.0;
                newPixelmon.motionX = 0.0;
                newPixelmon.setLocationAndAngles(x, y, z, this.player.rotationYaw, 0.0f);
            } else {
                newPixelmon = this.storage.sendOut(newPixelmonId, pw.getWorld());
                if (newPixelmon == null) {
                    this.bc.sendToAll("Problem sending out Pok\u00e9mon, cancelling battle. Please report this.", new Object[0]);
                    this.bc.endBattle(EnumBattleEndCause.FORCE);
                    return null;
                }
                newPixelmon.motionZ = 0.0;
                newPixelmon.motionY = 0.0;
                newPixelmon.motionX = 0.0;
                newPixelmon.setLocationAndAngles(x, y, z, this.player.rotationYaw, 0.0f);
                newPixelmon.releaseFromPokeball();
            }
            newWrapper.pokemon = newPixelmon;
        }
        newWrapper.battlePosition = pw.battlePosition;
        newWrapper.getBattleAbility().beforeSwitch(newWrapper);
        String newNickname = newWrapper.getNickname();
        EnumMark mark = newWrapper.getInnerLink().getMark();
        if (mark != EnumMark.None) {
            ChatHandler.sendBattleMessage(this.player, "playerparticipant.gomark", newNickname, mark.getTitle());
        } else {
            ChatHandler.sendBattleMessage(this.player, "playerparticipant.go", newNickname);
        }
        this.bc.sendToOthers("battlecontroller.sendout", this, this.player.getDisplayName().getUnformattedText(), newNickname);
        int index = this.controlledPokemon.indexOf(pw);
        this.controlledPokemon.set(index, newWrapper);
        this.bc.participants.forEach(BattleParticipant::updateOtherPokemon);
        return newWrapper;
    }

    @Override
    public boolean checkPokemon() {
        for (NBTTagCompound n : this.storage.getList()) {
            if (n == null || n.getInteger("PixelmonNumberMoves") != 0) continue;
            ChatHandler.sendChat(this.player, "playerparticipant.load", new Object[0]);
            return false;
        }
        return true;
    }

    @Override
    public void updatePokemon(PixelmonWrapper pw) {
        pw.update(EnumUpdateType.HP);
    }

    @Override
    public EntityLivingBase getEntity() {
        return this.player;
    }

    @Override
    public void updateOtherPokemon() {
        ArrayList<Spectator> playerSpectators;
        if (this.bc == null) {
            this.endBattle();
            return;
        }
        if (this.bc.simulateMode) {
            return;
        }
        ArrayList<PixelmonWrapper> opponents = this.bc.getOpponentPokemon(this);
        for (int i = 0; i < opponents.size(); ++i) {
            if (opponents.get(i) != null) continue;
            opponents.remove(i);
            --i;
        }
        if (this.bc.battleTurn == -1) {
            this.updateOpponentPokemon();
            if (this.bc.getTeam(this).size() > 1) {
                ArrayList<PixelmonWrapper> otherTeamPokemon = this.getAllyData();
                Pixelmon.NETWORK.sendTo(new SetPokemonTeamData(otherTeamPokemon), this.player);
                playerSpectators = this.bc.getPlayerSpectators(this);
                playerSpectators.forEach(spectator -> spectator.sendMessage(new SetPokemonTeamData(otherTeamPokemon)));
            }
        }
        if (!this.bc.battleEnded) {
            ArrayList<PixelmonWrapper> teamPokemon = this.getTeamPokemonList();
            Pixelmon.NETWORK.sendTo(new SetBattlingPokemon(teamPokemon), this.player);
            playerSpectators = this.bc.getPlayerSpectators(this);
            playerSpectators.forEach(spectator -> spectator.sendMessage(new SetBattlingPokemon(teamPokemon)));
        }
    }

    public ArrayList<PixelmonWrapper> getAllyData() {
        ArrayList<BattleParticipant> team = this.bc.getTeam(this);
        ArrayList<PixelmonWrapper> otherTeamPokemon = new ArrayList<PixelmonWrapper>(6);
        team.stream().filter(p -> p != this).forEach(p -> otherTeamPokemon.addAll(p.controlledPokemon));
        otherTeamPokemon.stream().filter(wrapper -> PixelmonMethods.isIDSame(new int[]{-1, -1}, wrapper.getPokemonID())).forEach(wrapper -> {
            int[] arrn = new int[]{-1, idcounter++};
            wrapper.pokemon.setPokemonId(arrn);
        });
        return otherTeamPokemon;
    }

    public PixelmonInGui[] getOpponentData() {
        ArrayList<PixelmonWrapper> opponents = this.bc.getOpponentPokemon(this);
        return PixelmonInGui.convertToGUI(opponents);
    }

    public void updateOpponentPokemon() {
        PixelmonInGui[] data = this.getOpponentData();
        Pixelmon.NETWORK.sendTo(new SetPokemonBattleData(data), this.player);
        ArrayList<Spectator> playerSpectators = this.bc.getPlayerSpectators(this);
        playerSpectators.forEach(spectator -> spectator.sendMessage(new SetPokemonBattleData(data)));
    }

    public void updatePokemonHealth(EntityPixelmon pixelmon) {
        this.updateOtherPokemon();
    }

    public void checkPlayerItems(PixelmonWrapper p) {
        ArrayList<ItemStack> playerInv = new ArrayList<ItemStack>(this.player.inventory.mainInventory);
        playerInv.addAll(this.player.inventory.offHandInventory);
        for (ItemStack item : playerInv) {
            int dmg = item.getItemDamage();
            if (!(item.getItem() instanceof ItemShrineOrbWithInfo) || !((ItemShrineOrbWithInfo)item.getItem()).acceptsType(p.type)) continue;
            item.setItemDamage(dmg + 1);
            return;
        }
    }

    public int getHighestLevel() {
        int lvl = -1;
        for (PixelmonWrapper pw : this.allPokemon) {
            lvl = Math.max(lvl, pw.getLevelNum());
        }
        return lvl;
    }

    @Override
    public void tick() {
        this.airSaver.tick();
        if (this.storage != null && !this.storage.guiOpened) {
            this.openGui();
        }
        if (this.player.getHealth() <= 0.0f) {
            this.bc.endBattleWithoutXP();
            BattleRegistry.deRegisterBattle(this.bc);
        }
    }

    public void givePlayerExp(PixelmonWrapper pixelmon) {
        int opponentPixelmonLevel = pixelmon.getLevelNum();
        int expAmount = 0;
        int divisor = 5;
        expAmount = opponentPixelmonLevel >= 75 ? opponentPixelmonLevel / (divisor * 5) : (opponentPixelmonLevel >= 50 ? opponentPixelmonLevel / (divisor * 4) : (opponentPixelmonLevel >= 35 ? opponentPixelmonLevel / (divisor * 2) : (opponentPixelmonLevel > divisor ? opponentPixelmonLevel / divisor : 1)));
        this.player.addExperience(expAmount);
    }

    public void openGui() {
        if (this.guiCheck == 0L || System.currentTimeMillis() - this.guiCheck > 1000L) {
            this.player.openGui(Pixelmon.INSTANCE, EnumGui.Battle.getIndex(), this.player.world, this.bc.battleIndex, 0, 0);
            this.guiCheck = System.currentTimeMillis();
        }
    }

    @Override
    public void setPosition(double[] ds) {
        this.player.connection.setPlayerLocation(ds[0], ds[1], ds[2], this.player.rotationYaw, this.player.rotationPitch);
    }

    @Override
    public void sendDamagePacket(PixelmonWrapper pokemon, int damage) {
        Pixelmon.NETWORK.sendTo(new HPPacket(pokemon, -damage), this.player);
    }

    @Override
    public void sendHealPacket(PixelmonWrapper pokemon, int amount) {
        Pixelmon.NETWORK.sendTo(new HPPacket(pokemon, amount), this.player);
    }

    @Override
    public String getDisplayName() {
        return this.player.getDisplayNameString();
    }

    @Override
    public PlayerStorage getStorage() {
        if (this.cachedStorage == null) {
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(this.player);
            optstorage.ifPresent(playerStorage -> {
                this.cachedStorage = playerStorage;
            });
        }
        return this.cachedStorage;
    }

    @Override
    public boolean canMegaEvolve() {
        return !this.canDynamax() && this.getStorage().megaData.canEquipMegaItem();
    }

    @Override
    public boolean canDynamax() {
        return this.canDynamax && !this.canMegaEvolve() && this.getStorage().dynamaxData.canEquipDynamaxItem();
    }

    public void sendMusicPacket(EnumBattleMusic songId) {
        Pixelmon.NETWORK.sendTo(new SetBattleMusic(songId), this.player);
    }
}

