/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class SpeedSwap
extends EffectBase {
    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        int speedStat = user.getBattleStats().speedStat;
        int speedStats = user.getStats().Speed;
        user.getBattleStats().speedStat = target.getBattleStats().speedStat;
        target.getBattleStats().speedStat = speedStat;
        user.getStats().Speed = target.getStats().Speed;
        target.getStats().Speed = speedStats;
        user.bc.sendToAll("pixelmon.attack.speedswapped", new Object());
    }

    @Override
    public boolean cantMiss(PixelmonWrapper pokemon) {
        return true;
    }
}

