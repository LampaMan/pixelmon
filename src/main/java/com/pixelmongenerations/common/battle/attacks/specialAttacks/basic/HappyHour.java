/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;

public class HappyHour
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        user.bc.sendToAll("pixelmon.effect.happyhour", new Object[0]);
        BattleParticipant participant = user.getParticipant();
        if (!user.bc.simulateMode && participant.getType() == ParticipantType.Player) {
            PlayerParticipant player = (PlayerParticipant)participant;
            player.hasHappyHour = true;
        }
        return AttackResult.succeeded;
    }
}

