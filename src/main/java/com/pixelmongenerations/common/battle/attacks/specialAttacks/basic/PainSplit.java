/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import java.util.ArrayList;

public class PainSplit
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        float newHPBase = (float)(user.getBaseHealth() + target.getBaseHealth()) / 2.0f;
        if (user.getBaseHealth() != target.getBaseHealth()) {
            user.bc.sendToAll("pixelmon.effect.painsplit", new Object[0]);
            if ((float)user.getBaseHealth() < newHPBase) {
                user.healEntityBy((int)(newHPBase - (float)user.getHealth()));
            } else if ((float)user.getBaseHealth() > newHPBase) {
                user.doBattleDamage(user, (float)user.getBaseHealth() - newHPBase, DamageTypeEnum.SELF);
            }
            if ((float)target.getBaseHealth() < newHPBase) {
                target.healEntityBy((int)(newHPBase - (float)target.getBaseHealth()));
                return AttackResult.succeeded;
            }
            if ((float)target.getBaseHealth() > newHPBase) {
                target.doBattleDamage(user, (float)target.getBaseHealth() - newHPBase, DamageTypeEnum.ATTACKFIXED);
                return AttackResult.hit;
            }
        }
        return AttackResult.failed;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.hitsAlly()) {
            return;
        }
        for (PixelmonWrapper target : userChoice.targets) {
            float newHPBase = (float)(pw.getHealth() + target.getHealth()) / 2.0f;
            if (newHPBase < (float)pw.getHealth()) {
                return;
            }
            userChoice.raiseWeight(pw.getHealthPercent(Math.min(newHPBase - (float)pw.getHealth(), (float)pw.getHealthDeficit())));
            userChoice.raiseWeight(target.getHealthPercent((float)target.getHealth() - newHPBase));
        }
    }
}

