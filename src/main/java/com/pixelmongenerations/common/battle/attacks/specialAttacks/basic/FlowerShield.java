/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.ArrayList;

public class FlowerShield
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        boolean hadEffect = false;
        for (PixelmonWrapper pw : user.bc.getActiveUnfaintedPokemon()) {
            if (!pw.hasType(EnumType.Grass)) continue;
            pw.getBattleStats().modifyStat(1, StatsType.Defence, user, true);
            hadEffect = true;
        }
        if (hadEffect) {
            return AttackResult.succeeded;
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        for (PixelmonWrapper other : pw.getOpponentPokemon()) {
            if (!other.hasType(EnumType.Grass)) continue;
            userChoice.raiseWeight(-10.0f);
        }
        for (PixelmonWrapper other : pw.getTeamPokemon()) {
            if (!other.hasType(EnumType.Grass)) continue;
            userChoice.raiseWeight(10.0f);
        }
    }
}

