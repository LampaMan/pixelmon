/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import java.util.ArrayList;

public class VenomDrench
extends SpecialAttackBase {
    private static final StatsType[] lowerStats = new StatsType[]{StatsType.Attack, StatsType.SpecialAttack, StatsType.Speed};

    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.targetIndex == 0) {
            boolean hit = false;
            for (PixelmonWrapper pw : user.targets) {
                if (!pw.hasStatus(StatusType.Poison, StatusType.PoisonBadly)) continue;
                hit = true;
                pw.getBattleStats().modifyStat(-1, lowerStats, user);
            }
            if (hit) {
                return AttackResult.succeeded;
            }
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        return AttackResult.succeeded;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        StatsEffect[] statsEffects = new StatsEffect[lowerStats.length];
        for (int i = 0; i < lowerStats.length; ++i) {
            statsEffects[i] = new StatsEffect(lowerStats[i], -1, false);
        }
        block1: for (ArrayList<MoveChoice> choices : MoveChoice.splitChoices(pw.getOpponentPokemon(), bestOpponentChoices)) {
            for (MoveChoice choice : choices) {
                if (!choice.isAttack() || !choice.attack.getAttackBase().getMakesContact()) continue;
                ArrayList<PixelmonWrapper> saveTargets = userChoice.targets;
                ArrayList<PixelmonWrapper> newTargets = new ArrayList<PixelmonWrapper>();
                newTargets.add(choice.user);
                userChoice.targets = newTargets;
                for (StatsEffect statsEffect : statsEffects) {
                    statsEffect.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
                }
                userChoice.targets = saveTargets;
                continue block1;
            }
        }
    }
}

