/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableList
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.google.common.collect.ImmutableList;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import java.util.List;

public class ClangorousSoul extends SpecialAttackBase {

    private List<StatsType> toIncrease = ImmutableList.of(StatsType.Attack, StatsType.SpecialDefence, StatsType.Defence, StatsType.SpecialAttack, StatsType.Speed);

    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.getHealthPercent() > 30.0f) {
            user.doBattleDamage(user, user.getPercentMaxHealth(30.0f), DamageTypeEnum.SELF);
            user.getBattleStats().increaseStats(1, this.toIncrease, user, true);
            user.bc.sendToAll("pixelmon.effect.clangoroussoul", user.getNickname());
            return AttackResult.succeeded;
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }
}

