/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.EntryHazard;
import com.pixelmongenerations.common.battle.status.PartialTrap;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import java.util.ArrayList;
import net.minecraft.util.text.translation.I18n;

public class RapidSpin
extends SpecialAttackBase {
    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.isFainted()) {
            return;
        }
        PartialTrap partialTrap = (PartialTrap)user.getStatus(StatusType.PartialTrap);
        if (partialTrap != null) {
            user.bc.sendToAll("pixelmon.effect.freefrom", user.getNickname(), partialTrap.variant.getLocalizedName());
            user.removeStatus(partialTrap);
        }
        if (user.removeStatus(StatusType.Leech)) {
            user.bc.sendToAll("pixelmon.effect.freefrom", user.getNickname(), I18n.translateToLocal("attack.leech seed.name"));
        }
        if (user.removeTeamStatus(StatusType.Spikes, StatusType.StealthRock, StatusType.ToxicSpikes, StatusType.StickyWeb, StatusType.Steelsurge)) {
            user.bc.sendToAll("pixelmon.effect.clearspikes", user.getNickname());
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.hitsAlly()) {
            return;
        }
        for (StatusBase status : pw.getStatuses()) {
            StatusType type = status.type;
            if (type == StatusType.PartialTrap) {
                userChoice.raiseWeight(10.0f);
                continue;
            }
            if (type == StatusType.Leech) {
                userChoice.raiseWeight(20.0f);
                continue;
            }
            if (!(status instanceof EntryHazard)) continue;
            EntryHazard hazard = (EntryHazard)status;
            userChoice.raiseWeight(hazard.getAIWeight() * hazard.getNumLayers() * (pw.getParticipant().countAblePokemon() - pw.bc.rules.battleType.numPokemon));
        }
    }
}

