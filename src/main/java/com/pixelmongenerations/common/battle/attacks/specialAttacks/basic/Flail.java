/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class Flail
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        double healthPercent = user.getHealthPercent();
        user.attack.getAttackBase().basePower = healthPercent < 3.0 ? 200 : (healthPercent < 10.0 ? 150 : (healthPercent < 20.0 ? 100 : (healthPercent < 33.0 ? 80 : (healthPercent < 66.0 ? 40 : 20))));
        return AttackResult.proceed;
    }
}

