/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.BattleAIBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.BattleStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import java.util.ArrayList;

public abstract class Swap
extends SpecialAttackBase {
    String langString;
    StatsType[] swapStats;

    public Swap(String langString, StatsType ... swapStats) {
        this.langString = langString;
        this.swapStats = swapStats;
    }

    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        user.bc.sendToAll(this.langString, user.getNickname(), target.getNickname());
        user.getBattleStats().swapStats(target.getBattleStats(), this.swapStats);
        return AttackResult.succeeded;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.hitsAlly()) {
            return;
        }
        for (PixelmonWrapper target : userChoice.targets) {
            BattleStats[] originalStats = new BattleStats[]{pw.getBattleStats(), target.getBattleStats()};
            for (StatsType stat : this.swapStats) {
                if (originalStats[0].getStage(stat) < originalStats[1].getStage(stat)) break;
            }
            BattleStats[] saveStats = new BattleStats[2];
            pw.bc.simulateMode = false;
            saveStats[0] = new BattleStats(originalStats[0]);
            saveStats[1] = new BattleStats(originalStats[1]);
            ArrayList<PixelmonWrapper> opponents = pw.getOpponentPokemon();
            BattleAIBase ai = pw.getBattleAI();
            try {
                pw.getBattleStats().swapStats(target.getBattleStats(), this.swapStats);
                pw.bc.simulateMode = true;
                ArrayList<MoveChoice> bestUserChoicesAfter = ai.getBestAttackChoices(pw);
                ArrayList<ArrayList<MoveChoice>> bestOpponentChoicesAfter = ai.getBestAttackChoices(opponents);
                ai.weightFromUserOptions(pw, userChoice, bestUserChoices, bestUserChoicesAfter);
                ai.weightFromOpponentOptions(pw, userChoice, MoveChoice.splitChoices(opponents, bestOpponentChoices), bestOpponentChoicesAfter);
            }
            finally {
                pw.bc.simulateMode = false;
                for (int i = 0; i < saveStats.length; ++i) {
                    originalStats[i].copyStats(saveStats[i]);
                }
                pw.bc.simulateMode = true;
            }
        }
    }
}

