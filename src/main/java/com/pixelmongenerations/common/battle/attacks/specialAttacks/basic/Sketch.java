/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.Mimic;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.TempMoveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.core.network.EnumUpdateType;
import java.util.ArrayList;

public class Sketch
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        Moveset moveset;
        Attack copiedAttack;
        block7: {
            block6: {
                copiedAttack = target.lastAttack;
                moveset = user.getMoveset();
                if (copiedAttack == null) break block6;
                if (copiedAttack.isAttack("Chatter", "Struggle") || moveset.contains(copiedAttack)) break block6;
                if (moveset.hasAttack("Sketch")) break block7;
            }
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        if (!user.bc.simulateMode) {
            boolean isTrainer = user.getParticipant().getType() == ParticipantType.Trainer;
            boolean bl = isTrainer;
            if (isTrainer) {
                moveset = moveset.copy();
            }
            moveset.replaceMove("Sketch", new Attack(copiedAttack.getAttackBase()));
            if (isTrainer) {
                user.addStatus(new TempMoveset(moveset), user);
            }
        }
        user.bc.sendToAll("pixelmon.effect.sketch", user.getNickname(), copiedAttack.getAttackBase().getLocalizedName());
        user.update(EnumUpdateType.Moveset);
        return AttackResult.succeeded;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        Mimic mimic = new Mimic();
        mimic.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
    }
}

