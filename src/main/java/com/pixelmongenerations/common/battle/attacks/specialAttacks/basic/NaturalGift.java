/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.DecreaseEV;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.heldItems.ItemBerryRestoreHP;
import com.pixelmongenerations.common.item.heldItems.ItemBerryStatIncrease;
import com.pixelmongenerations.common.item.heldItems.ItemBerryStatus;
import com.pixelmongenerations.common.item.heldItems.ItemBerryTypeReducing;
import com.pixelmongenerations.core.enums.EnumType;

public class NaturalGift
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        ItemHeld userItem = user.getHeldItem();
        if (user.hasHeldItem() && ItemHeld.canEatBerry(user)) {
            block0 : switch (userItem.getHeldItemType()) {
                case berryStatIncrease: {
                    user.attack.getAttackBase().basePower = 100;
                    switch (((ItemBerryStatIncrease)userItem).berryType) {
                        case liechiBerry: {
                            user.attack.overrideType(EnumType.Grass);
                            break block0;
                        }
                        case ganlonBerry: {
                            user.attack.overrideType(EnumType.Ice);
                            break block0;
                        }
                        case petayaBerry: {
                            user.attack.overrideType(EnumType.Poison);
                            break block0;
                        }
                        case apicotBerry: {
                            user.attack.overrideType(EnumType.Ground);
                            break block0;
                        }
                        case salacBerry: {
                            user.attack.overrideType(EnumType.Fighting);
                            break block0;
                        }
                        case lansatBerry: {
                            user.attack.overrideType(EnumType.Flying);
                            break block0;
                        }
                        case starfBerry: {
                            user.attack.overrideType(EnumType.Psychic);
                        }
                    }
                    break;
                }
                case berryCustap: {
                    user.attack.overrideType(EnumType.Ghost);
                    user.attack.getAttackBase().basePower = 100;
                    break;
                }
                case berryMicle: {
                    user.attack.overrideType(EnumType.Rock);
                    user.attack.getAttackBase().basePower = 100;
                }
                case berryEnigma: 
                case ginemaBerry: {
                    user.attack.overrideType(EnumType.Bug);
                    user.attack.getAttackBase().basePower = 100;
                    break;
                }
                case jabocaBerry: {
                    user.attack.overrideType(EnumType.Dragon);
                    user.attack.getAttackBase().basePower = 100;
                    break;
                }
                case rowapBerry: 
                case marangaBerry: {
                    user.attack.overrideType(EnumType.Dark);
                    user.attack.getAttackBase().basePower = 100;
                    break;
                }
                case berryEVReducing: {
                    user.attack.getAttackBase().basePower = 90;
                    switch (((DecreaseEV)userItem).type) {
                        case PomegBerry: {
                            user.attack.overrideType(EnumType.Ice);
                            break block0;
                        }
                        case KelpsyBerry: {
                            user.attack.overrideType(EnumType.Fighting);
                            break block0;
                        }
                        case QualotBerry: {
                            user.attack.overrideType(EnumType.Poison);
                            break block0;
                        }
                        case HondewBerry: {
                            user.attack.overrideType(EnumType.Ground);
                            break block0;
                        }
                        case GrepaBerry: {
                            user.attack.overrideType(EnumType.Flying);
                            break block0;
                        }
                        case TamatoBerry: {
                            user.attack.overrideType(EnumType.Psychic);
                        }
                    }
                    break;
                }
                case berryTypeReducing: {
                    user.attack.getAttackBase().basePower = 80;
                    user.attack.getAttackBase().attackType = ((ItemBerryTypeReducing)userItem).typeReduced;
                    break;
                }
                case berryStatus: {
                    switch (((ItemBerryStatus)userItem).berryType) {
                        case pumkinBerry: 
                        case drashBerry: 
                        case eggantBerry: 
                        case yagoBerry: 
                        case tougaBerry: {
                            user.attack.overrideType(EnumType.Bug);
                            user.attack.getAttackBase().basePower = 100;
                            break block0;
                        }
                        case cheriBerry: {
                            user.attack.overrideType(EnumType.Fire);
                            user.attack.getAttackBase().basePower = 80;
                            break block0;
                        }
                        case chestoBerry: {
                            user.attack.overrideType(EnumType.Water);
                            user.attack.getAttackBase().basePower = 80;
                            break block0;
                        }
                        case pechaBerry: {
                            user.attack.overrideType(EnumType.Electric);
                            user.attack.getAttackBase().basePower = 80;
                            break block0;
                        }
                        case rawstBerry: {
                            user.attack.overrideType(EnumType.Grass);
                            user.attack.getAttackBase().basePower = 80;
                            break block0;
                        }
                        case aspearBerry: {
                            user.attack.overrideType(EnumType.Ice);
                            user.attack.getAttackBase().basePower = 80;
                            break block0;
                        }
                        case persimBerry: {
                            user.attack.overrideType(EnumType.Ground);
                            user.attack.getAttackBase().basePower = 80;
                            break block0;
                        }
                        case lumBerry: {
                            user.attack.overrideType(EnumType.Flying);
                            user.attack.getAttackBase().basePower = 80;
                        }
                    }
                    break;
                }
                case berryRestoreHP: {
                    user.attack.getAttackBase().basePower = 80;
                    switch (((ItemBerryRestoreHP)userItem).berryType) {
                        case oranBerry: {
                            user.attack.overrideType(EnumType.Poison);
                            break;
                        }
                        case sitrusBerry: {
                            user.attack.overrideType(EnumType.Psychic);
                            break;
                        }
                        case figyBerry: {
                            user.attack.overrideType(EnumType.Bug);
                            break;
                        }
                        case wikiBerry: {
                            user.attack.overrideType(EnumType.Rock);
                            break;
                        }
                        case magoBerry: {
                            user.attack.overrideType(EnumType.Ghost);
                            break;
                        }
                        case aguavBerry: {
                            user.attack.overrideType(EnumType.Dragon);
                            break;
                        }
                        case iapapaBerry: {
                            user.attack.overrideType(EnumType.Dark);
                        }
                    }
                    user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
                    return AttackResult.failed;
                }
                case leppa: {
                    user.attack.overrideType(EnumType.Fighting);
                    user.attack.getAttackBase().basePower = 80;
                    break;
                }
                case keeBerry: {
                    user.attack.overrideType(EnumType.Fairy);
                    user.attack.getAttackBase().basePower = 100;
                    break;
                }
                default: {
                    user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
                    return AttackResult.failed;
                }
            }
            user.consumeItem();
            return AttackResult.proceed;
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }
}

