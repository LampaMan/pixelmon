/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.attackModifiers;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.Value;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.attackModifiers.AttackModifierBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.log.MoveResults;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SkillLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class MultipleHit
extends AttackModifierBase {
    private int count = 0;
    private int targetCount = 0;
    private boolean inProgress = false;
    private int limit = 0;
    private int min;
    private int max;

    public MultipleHit(Value ... values) {
        this.min = values[0].value;
        this.max = values[1].value;
    }

    public boolean repeatsAttack() {
        if (this.max == 0) {
            if (this.count >= this.min) {
                return false;
            }
            ++this.count;
            return true;
        }
        if (this.count >= this.targetCount) {
            return false;
        }
        ++this.count;
        return true;
    }

    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (this.limit++ <= 5 && this.inProgress) {
            return AttackResult.proceed;
        }
        user.inMultipleHit = true;
        this.inProgress = true;
        this.limit = 0;
        if (this.max != 0) {
            if (this.min == 2 && this.max == 5) {
                int random = RandomHelper.getRandomNumberBetween(0, 9);
                this.targetCount = user.bc.simulateMode ? 1 : (random < 3 ? 2 : (random < 6 ? 3 : (random < 8 ? 4 : 5)));
            } else {
                this.targetCount = RandomHelper.getRandomNumberBetween(this.min, this.max);
            }
        }
        if (user.getBattleAbility() instanceof SkillLink) {
            this.targetCount = this.max;
        } else if (user.getSpecies().equals((Object)EnumSpecies.Greninja)) {
            if (user.attack.isAttack("Water Shuriken") && user.getForm() == 2) {
                this.targetCount = 3;
            }
        } else if (user.attack.isAttack("Scale Shot")) {
            boolean hasGainedStats = false;
            if (!hasGainedStats) {
                user.getBattleStats().modifyStat(1, StatsType.Speed);
                user.getBattleStats().modifyStat(-1, StatsType.Defence);
                hasGainedStats = true;
            }
        } else if (user.attack.isAttack("Dragon Darts")) {
            if (user.getOpponentPokemon().size() > 1) {
                if (user.attack.moveResult.result != AttackResult.hit) {
                    this.targetCount = 2;
                    this.count = 2;
                } else {
                    this.targetCount = 1;
                    this.count = 1;
                }
            } else {
                this.targetCount = 2;
                this.count = 2;
            }
        }
        this.count = 0;
        int initAccuracy = user.attack.getAttackBase().accuracy;
        boolean hasSubstitute = false;
        while (target.isAlive() && user.isAlive() && this.repeatsAttack()) {
            MoveResults[] results;
            user.attack.getAttackBase().accuracy = -1;
            hasSubstitute = target.hasStatus(StatusType.Substitute);
            for (MoveResults result : results = user.useAttackOnly()) {
                user.attack.moveResult.damage += result.damage;
                user.attack.moveResult.fullDamage += result.fullDamage;
            }
        }
        user.attack.getAttackBase().accuracy = initAccuracy;
        if (user.bc.simulateMode && this.targetCount == 1) {
            user.attack.moveResult.damage = (int)Math.min((double)user.attack.moveResult.damage * 3.168, (double)target.getHealth());
            user.attack.moveResult.fullDamage = (int)((double)user.attack.moveResult.fullDamage * 3.168);
        }
        user.inMultipleHit = false;
        this.inProgress = false;
        user.attack.sendEffectiveChat(user, target);
        user.bc.sendToAll("multiplehit.times", user.getNickname(), this.count);
        Attack.postProcessAttackAllHits(user, target, user.attack, user.attack.moveResult.damage, DamageTypeEnum.ATTACK, hasSubstitute);
        if (!hasSubstitute) {
            Attack.applyContactLate(user, target);
        }
        return AttackResult.hit;
    }
}

