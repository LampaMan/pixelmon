/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AsOne;
import com.pixelmongenerations.common.entity.pixelmon.abilities.BattleBond;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Comatose;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Disguise;
import com.pixelmongenerations.common.entity.pixelmon.abilities.FakemonRevealed;
import com.pixelmongenerations.common.entity.pixelmon.abilities.FlowerGift;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Forecast;
import com.pixelmongenerations.common.entity.pixelmon.abilities.GulpMissile;
import com.pixelmongenerations.common.entity.pixelmon.abilities.IceFace;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Illusion;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Imposter;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Multitype;
import com.pixelmongenerations.common.entity.pixelmon.abilities.PowerOfAlchemy;
import com.pixelmongenerations.common.entity.pixelmon.abilities.RKSSystem;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Receiver;
import com.pixelmongenerations.common.entity.pixelmon.abilities.StanceChange;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Trace;
import com.pixelmongenerations.common.entity.pixelmon.abilities.WonderGuard;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ZenMode;

public class RolePlay
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        AbilityBase targetAbility = target.getBattleAbility(false);
        AbilityBase userAbility = user.getBattleAbility(false);
        if (targetAbility instanceof AsOne || targetAbility instanceof Illusion || targetAbility instanceof Imposter || targetAbility instanceof Multitype || targetAbility instanceof StanceChange || targetAbility instanceof WonderGuard || targetAbility instanceof ZenMode || targetAbility instanceof Comatose || targetAbility.equals(userAbility) || targetAbility instanceof RKSSystem || targetAbility instanceof Forecast || targetAbility instanceof FlowerGift || targetAbility instanceof PowerOfAlchemy || targetAbility instanceof Receiver || targetAbility instanceof Disguise || targetAbility instanceof BattleBond || targetAbility instanceof Trace || targetAbility instanceof GulpMissile || targetAbility instanceof IceFace || targetAbility instanceof FakemonRevealed) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        user.bc.sendToAll("pixelmon.effect.entrainment", user.getNickname(), userAbility.getLocalizedName(), targetAbility.getLocalizedName());
        user.setTempAbility(targetAbility);
        return AttackResult.succeeded;
    }
}

