/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn.MultiTurnCharge;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import java.util.ArrayList;

public class Geomancy
extends MultiTurnCharge {
    private static final StatsType[] raiseStats = new StatsType[]{StatsType.SpecialAttack, StatsType.SpecialDefence, StatsType.Speed};

    public Geomancy() {
        super("pixelmon.effect.geomancy");
    }

    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        AttackResult baseResult = super.applyEffectDuring(user, target);
        if (baseResult == AttackResult.proceed) {
            user.getBattleStats().modifyStat(2, raiseStats);
        }
        return baseResult;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        for (StatsType stat : raiseStats) {
            StatsEffect statsEffect = new StatsEffect(stat, 2, true);
            statsEffect.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
        }
        super.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
    }
}

