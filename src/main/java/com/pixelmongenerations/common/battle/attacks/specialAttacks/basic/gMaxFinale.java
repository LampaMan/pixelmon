/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;

public class gMaxFinale
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        for (PixelmonWrapper ally : user.bc.getTeamPokemon(user.getParticipant())) {
            if (ally.hasStatus(StatusType.HealBlock) || ally.hasFullHealth()) continue;
            ally.healEntityBy(ally.getMaxHealth() / 6);
        }
        if (!user.hasFullHealth()) {
            if (!user.hasStatus(StatusType.HealBlock)) {
                user.healEntityBy(user.getMaxHealth() / 6);
            }
        }
        return AttackResult.proceed;
    }
}

