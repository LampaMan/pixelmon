/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import java.util.ArrayList;

public class Memento
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        user.doBattleDamage(user, (int)Math.floor(user.getHealth()), DamageTypeEnum.SELF);
        target.getBattleStats().modifyStat(-2, new StatsType[]{StatsType.Attack, StatsType.SpecialAttack}, user);
        return AttackResult.succeeded;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (pw.getParticipant().countAblePokemon() > 1 && MoveChoice.canOutspeedAnd2HKO(bestOpponentChoices, pw, bestUserChoices)) {
            StatsEffect attackDrop = new StatsEffect(StatsType.Attack, -2, false);
            StatsEffect spAttackDrop = new StatsEffect(StatsType.SpecialAttack, -2, false);
            attackDrop.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
            spAttackDrop.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
        }
    }
}

