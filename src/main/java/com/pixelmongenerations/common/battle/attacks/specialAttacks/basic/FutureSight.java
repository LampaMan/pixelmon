/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.FutureSighted;
import com.pixelmongenerations.common.battle.status.StatusType;
import java.util.ArrayList;

public class FutureSight
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        FutureSighted status = (FutureSighted)target.getStatus(StatusType.FutureSight);
        if (status == null) {
            if (user.bc.simulateMode) {
                return AttackResult.proceed;
            }
            user.bc.sendToAll("pixelmon.effect.foresawattack", user.getNickname());
            target.addStatus(new FutureSighted(user, user.attack), target);
            return AttackResult.succeeded;
        }
        if (status.turnsToGo <= 0) {
            return AttackResult.proceed;
        }
        target.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        userChoice.lowerTier(2);
    }
}

