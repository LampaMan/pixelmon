/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.TrainerParticipant;
import com.pixelmongenerations.common.battle.status.Autotomize;
import com.pixelmongenerations.common.battle.status.DefenseCurl;
import com.pixelmongenerations.common.battle.status.GuardSplit;
import com.pixelmongenerations.common.battle.status.Infatuated;
import com.pixelmongenerations.common.battle.status.PowerSplit;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusPersist;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Substitute;
import com.pixelmongenerations.common.battle.status.TempMoveset;
import com.pixelmongenerations.common.battle.status.Transformed;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.battles.EnforcedSwitch;
import java.util.ArrayList;

public class BatonPass
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        BattleParticipant userParticipant = user.getParticipant();
        if (!userParticipant.hasMorePokemonReserve()) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        if (user.bc.simulateMode) {
            return AttackResult.succeeded;
        }
        user.bc.sendToAll("batonpass.pass", user.getNickname());
        user.nextSwitchIsMove = true;
        if (userParticipant instanceof TrainerParticipant) {
            user.bc.switchPokemon(user.getPokemonID(), user.getBattleAI().getNextSwitch(user), true);
        } else if (userParticipant instanceof PlayerParticipant) {
            if (!user.bc.simulateMode) {
                user.wait = true;
                Pixelmon.NETWORK.sendTo(new EnforcedSwitch(user.bc.getPositionOfPokemon(user, user.getParticipant())), user.getPlayerOwner());
            }
        } else {
            user.nextSwitchIsMove = false;
        }
        return AttackResult.succeeded;
    }

    public static boolean isBatonPassable(StatusBase s) {
        return !(s instanceof StatusPersist) && !(s instanceof Autotomize) && !(s instanceof DefenseCurl) && !(s instanceof Infatuated) && !(s instanceof TempMoveset) && !(s instanceof Transformed) && !(s instanceof GuardSplit) && !(s instanceof PowerSplit);
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (pw.hasStatus(StatusType.Perish)) {
            return;
        }
        int stageMods = pw.getBattleStats().getSumStages();
        if (stageMods >= 0) {
            float weightIncrease = 0.0f;
            Substitute substitute = (Substitute)pw.getStatus(StatusType.Substitute);
            if (substitute != null) {
                weightIncrease += pw.getHealthPercent(substitute.health);
            }
            weightIncrease += (float)(stageMods * 20);
            if (weightIncrease > 0.0f && MoveChoice.canOutspeedAnd2HKO(bestOpponentChoices, pw, userChoice.createList())) {
                weightIncrease += 30.0f;
            }
            userChoice.raiseWeightLimited(weightIncrease);
        }
    }
}

