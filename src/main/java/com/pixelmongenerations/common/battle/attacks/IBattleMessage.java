/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks;

import com.pixelmongenerations.common.battle.attacks.MessageType;

public abstract class IBattleMessage {
    public MessageType messageType;
    public boolean viewed = false;
    public int[] pokemonID;

    public abstract void process();
}

