/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Focus;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import java.util.ArrayList;

public class FocusPunch
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.hasStatus(StatusType.Focus) || user.bc.simulateMode) {
            user.bc.sendToAll("pixelmon.battletext.used", user.getNickname(), user.attack.getAttackBase().getLocalizedName());
            user.removeStatus(StatusType.Focus);
            return AttackResult.proceed;
        }
        user.bc.sendToAll("pixelmon.effect.lostfocus", user.getNickname());
        return AttackResult.failed;
    }

    @Override
    public void applyEarlyEffect(PixelmonWrapper user) {
        user.bc.sendToAll("pixelmon.effect.focuspunch", user.getNickname());
        user.addStatus(new Focus(), user);
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (pw.hasStatus(StatusType.Substitute)) {
            return;
        }
        for (PixelmonWrapper opponent : pw.getOpponentPokemon()) {
            if (opponent.lastAttack == null || opponent.lastAttack.moveResult.target != pw || opponent.lastAttack.getAttackCategory() == AttackCategory.Status) continue;
            userChoice.lowerTier(2);
            userChoice.weight = 0.0f;
        }
    }
}

