/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.log.MoveResults;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SkillLink;

public class TripleKick
extends SpecialAttackBase {
    private transient int count = 0;

    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        boolean hasSubstitute = false;
        if (this.count != 0) {
            return AttackResult.proceed;
        }
        boolean missed = false;
        user.inMultipleHit = true;
        while (this.count < 3 && user.isAlive() && target.isAlive() && !missed) {
            MoveResults[] results;
            ++this.count;
            hasSubstitute = target.hasStatus(StatusType.Substitute);
            for (MoveResults result : results = user.useAttackOnly()) {
                if (result.result == AttackResult.failed || result.result == AttackResult.missed) {
                    --this.count;
                    missed = true;
                }
                user.attack.moveResult.damage += result.damage;
                user.attack.moveResult.fullDamage += result.fullDamage;
                user.attack.moveResult.accuracy = result.accuracy;
            }
            user.attack.getAttackBase().basePower = user.attack.isAttack("Triple Axel") ? (user.attack.getAttackBase().basePower += 20) : (user.attack.getAttackBase().basePower += 10);
            if (!(user.getBattleAbility() instanceof SkillLink)) continue;
            user.attack.getAttackBase().accuracy = -1;
        }
        user.inMultipleHit = false;
        user.attack.doMove(user, target);
        if (this.count > 0) {
            user.attack.sendEffectiveChat(user, target);
            user.bc.sendToAll("multiplehit.times", user.getNickname(), this.count);
        }
        this.count = 0;
        if (user.attack.isAttack("Triple Axel")) {
            user.attack.getAttackBase().basePower = 20;
            user.attack.savedPower = 20;
        } else {
            user.attack.getAttackBase().basePower = 10;
            user.attack.savedPower = 10;
        }
        user.attack.getAttackBase().accuracy = 90;
        Attack.postProcessAttackAllHits(user, target, user.attack, user.attack.moveResult.damage, DamageTypeEnum.ATTACK, hasSubstitute);
        if (!hasSubstitute) {
            Attack.applyContactLate(user, target);
        }
        return AttackResult.hit;
    }
}

