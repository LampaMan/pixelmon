/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.SmackedDown;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Levitate;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.ArrayList;

public class SmackDown
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        block6: {
            block5: {
                if (target.hasStatus(StatusType.Substitute)) {
                    return AttackResult.proceed;
                }
                if (target.hasStatus(StatusType.Flying)) {
                    target.canAttack = false;
                }
                if (target.removeStatuses(StatusType.Flying, StatusType.MagnetRise, StatusType.Telekinesis)) break block5;
                if (target.hasStatus(StatusType.SmackedDown)) break block6;
                if (!target.hasType(EnumType.Flying) && !(target.getBattleAbility() instanceof Levitate)) break block6;
            }
            user.bc.sendToAll("pixelmon.effect.smackdown", target.getNickname());
        }
        target.addStatus(new SmackedDown(), user);
        return AttackResult.proceed;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.hitsAlly()) {
            return;
        }
        for (PixelmonWrapper target : userChoice.targets) {
            if (target.hasStatus(StatusType.Flying)) {
                if (pw.bc.getFirstMover(pw, target) == pw) {
                    userChoice.raiseWeight(50.0f);
                }
            }
            if (!target.isAirborne()) continue;
            if (!pw.getMoveset().hasOffensiveAttackType(EnumType.Ground)) continue;
            userChoice.raiseWeight(25.0f);
        }
    }

    public void applySwitchInEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.hasStatus(StatusType.SmackedDown)) {
            target.removeStatus(new SmackedDown(), true);
        }
    }
}

