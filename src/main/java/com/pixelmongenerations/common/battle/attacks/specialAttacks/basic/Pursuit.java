/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.CalcPriority;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import java.util.ArrayList;

public class Pursuit
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.isSwitching) {
            user.attack.getAttackBase().basePower = 80;
        }
        return AttackResult.proceed;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.isSwitching && target.isFainted()) {
            for (int i = user.bc.turn + 1; i < user.bc.turnList.size(); ++i) {
                PixelmonWrapper pw = user.bc.turnList.get(i);
                if (pw.attack == null) continue;
                if (!pw.attack.isAttack("Pursuit")) continue;
                CalcPriority.recalculateMoveSpeed(user.bc, user.bc.turn + 1);
                break;
            }
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (!userChoice.hitsAlly()) {
            if (userChoice.tier >= 3) {
                userChoice.raiseWeight(1.0f);
            } else {
                for (PixelmonWrapper target : userChoice.targets) {
                    if (target.getHealthPercent(userChoice.weight) < 50.0f) continue;
                    userChoice.raiseWeight(userChoice.weight);
                }
            }
        }
    }
}

