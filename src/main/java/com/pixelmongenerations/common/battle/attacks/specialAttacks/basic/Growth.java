/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Sunny;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import java.util.ArrayList;

public class Growth
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        int amount = user.bc.globalStatusController.getWeather() instanceof Sunny ? 2 : 1;
        user.getBattleStats().modifyStat(amount, StatsType.Attack, StatsType.SpecialAttack);
        return AttackResult.succeeded;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        int amount = pw.bc.globalStatusController.getWeather() instanceof Sunny ? 2 : 1;
        StatsEffect attackBoost = new StatsEffect(StatsType.Attack, amount, true);
        StatsEffect specialAttackBoost = new StatsEffect(StatsType.SpecialAttack, amount, true);
        attackBoost.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
        specialAttackBoost.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
    }
}

