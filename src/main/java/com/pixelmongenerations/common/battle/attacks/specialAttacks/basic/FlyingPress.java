/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.List;

public class FlyingPress
extends SpecialAttackBase {
    @Override
    public double modifyTypeEffectiveness(List<EnumType> effectiveTypes, EnumType moveType, double baseEffectiveness) {
        return baseEffectiveness * (double)EnumType.getTotalEffectiveness(effectiveTypes, EnumType.Flying);
    }
}

