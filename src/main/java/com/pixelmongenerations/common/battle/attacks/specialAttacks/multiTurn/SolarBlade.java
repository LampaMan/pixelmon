/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn.MultiTurnCharge;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Hail;
import com.pixelmongenerations.common.battle.status.Rainy;
import com.pixelmongenerations.common.battle.status.Sandstorm;
import com.pixelmongenerations.common.battle.status.Sunny;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import java.util.ArrayList;

public class SolarBlade
extends MultiTurnCharge {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (!this.doesPersist(user)) {
            this.setPersists(user, true);
            this.setTurnCount(user, 2);
        }
        this.decrementTurnCount(user);
        if (this.getTurnCount(user) == 1) {
            user.bc.sendToAll("pixelmon.effect.storeenergy", user.getNickname());
            if (!(user.bc.globalStatusController.getWeather() instanceof Sunny && user.getHeldItem() != PixelmonItemsHeld.utilityUmbrella || user.getUsableHeldItem().affectMultiturnMove(user))) {
                return AttackResult.charging;
            }
        }
        if (user.bc.globalStatusController.getWeather() instanceof Rainy && user.getHeldItem() != PixelmonItemsHeld.utilityUmbrella) {
            user.attack.getAttackBase().basePower = (int)((double)user.attack.getAttackBase().basePower * 0.5);
        }
        if (user.bc.globalStatusController.getWeather() instanceof Sandstorm || user.bc.globalStatusController.getWeather() instanceof Hail) {
            user.attack.getAttackBase().basePower = (int)((double)user.attack.getAttackBase().basePower * 0.5);
        }
        this.setPersists(user, false);
        return AttackResult.proceed;
    }

    @Override
    public boolean shouldNotLosePP(PixelmonWrapper user) {
        return !(user.bc.globalStatusController.getWeather() instanceof Sunny) && super.shouldNotLosePP(user);
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (!(pw.bc.globalStatusController.getWeather() instanceof Sunny)) {
            super.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
        }
    }
}

