/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.GlobalStatusController;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.EnumType;

public class PlasmaFists
extends GlobalStatusBase {
    public PlasmaFists() {
        super(StatusType.PlasmaFists);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.globalStatusController.hasStatus(this.type)) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        } else {
            user.bc.sendToAll("pixelmon.status.iondeluge", new Object[0]);
            user.bc.globalStatusController.addGlobalStatus(new PlasmaFists());
        }
    }

    @Override
    public int[] modifyPowerAndAccuracyTarget(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (a.getAttackBase().attackType == EnumType.Normal) {
            if (!a.isAttack("Struggle")) {
                a.overrideType(EnumType.Electric);
            }
        }
        return new int[]{power, accuracy};
    }

    @Override
    public void applyRepeatedEffect(GlobalStatusController globalStatusController) {
        globalStatusController.removeGlobalStatus(this);
    }
}

