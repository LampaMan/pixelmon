/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.animations;

import net.minecraft.entity.EntityLiving;

public interface IAttackAnimation {
    public void doMove(EntityLiving var1, EntityLiving var2);
}

