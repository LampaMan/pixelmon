/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import java.util.ArrayList;

public class KnockOff
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.hasHeldItem() && !target.hasSpecialItem()) {
            if (user.attack.isAttack("Corrosive Gas")) {
                return AttackResult.proceed;
            }
            user.attack.getAttackBase().basePower = (int)((double)user.attack.getAttackBase().basePower * 1.5);
        }
        return AttackResult.proceed;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (!user.inParentalBond && target.hasHeldItem() && target.isItemRemovable(user)) {
            user.bc.sendToAll("pixelmon.effect.knockoff", user.getNickname(), target.getNickname(), target.getHeldItem().getLocalizedName());
            target.removeHeldItem();
            target.enableReturnHeldItem();
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.hitsAlly()) {
            return;
        }
        for (PixelmonWrapper target : userChoice.targets) {
            if (!target.isItemRemovable(pw)) continue;
            userChoice.raiseWeight(25.0f);
        }
    }
}

