/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import java.util.ArrayList;

public class Copycat
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        block5: {
            block4: {
                if (user.bc.simulateMode) {
                    return AttackResult.ignore;
                }
                if (user.bc.lastAttack == null) break block4;
                if (!user.bc.lastAttack.isAttack("Copycat", "Mirror Move", "Sketch")) break block5;
            }
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        user.useTempAttack(user.bc.lastAttack.isMaxMove ? user.bc.getOpponentPokemon((PixelmonWrapper)user).get((int)0).baseLastAttack : user.bc.lastAttack);
        return AttackResult.ignore;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        ArrayList<MoveChoice> possibleChoices;
        if (MoveChoice.canOutspeed(bestOpponentChoices, pw, userChoice.createList())) {
            possibleChoices = MoveChoice.createChoicesFromChoices(pw, bestOpponentChoices, false);
        } else if (pw.bc.lastAttack != null) {
            possibleChoices = pw.bc.lastAttack.createMoveChoices(pw, false);
        } else {
            return;
        }
        pw.getBattleAI().weightRandomMove(pw, userChoice, possibleChoices);
    }
}

