/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn;

import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.MultiTurn;
import com.pixelmongenerations.common.battle.status.StatusType;

public abstract class MultiTurnSpecialAttackBase
extends EffectBase {
    public MultiTurnSpecialAttackBase() {
        super(true);
    }

    private MultiTurn getMultiTurnStatus(PixelmonWrapper user) {
        return (MultiTurn)user.getStatus(StatusType.MultiTurn);
    }

    public int getTurnCount(PixelmonWrapper user) {
        MultiTurn multiTurn = this.getMultiTurnStatus(user);
        return multiTurn == null ? 0 : multiTurn.numTurns;
    }

    public void decrementTurnCount(PixelmonWrapper user) {
        MultiTurn multiTurn;
        if (!user.bc.simulateMode && (multiTurn = this.getMultiTurnStatus(user)) != null) {
            --multiTurn.numTurns;
        }
    }

    public void setTurnCount(PixelmonWrapper user, int value) {
        MultiTurn multiTurn;
        if (!user.bc.simulateMode && (multiTurn = this.getMultiTurnStatus(user)) != null) {
            multiTurn.numTurns = value;
        }
    }

    @Override
    public boolean doesPersist(PixelmonWrapper user) {
        return this.getMultiTurnStatus(user) != null;
    }

    protected void setPersists(PixelmonWrapper user, boolean value) {
        if (!user.bc.simulateMode) {
            if (value) {
                user.addStatus(new MultiTurn(), user);
            } else {
                user.removeStatus(StatusType.MultiTurn);
            }
        }
    }

    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        return AttackResult.proceed;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
    }

    @Override
    public boolean cantMiss(PixelmonWrapper user) {
        return false;
    }

    @Override
    public void applyMissEffect(PixelmonWrapper user, PixelmonWrapper target) {
        this.removeEffect(user, target);
    }

    public boolean ignoresType(PixelmonWrapper user) {
        return false;
    }

    public void removeEffect(PixelmonWrapper user, PixelmonWrapper target) {
    }

    public boolean isCharging(PixelmonWrapper user, PixelmonWrapper target) {
        return false;
    }

    public boolean shouldNotLosePP(PixelmonWrapper user) {
        return false;
    }
}

