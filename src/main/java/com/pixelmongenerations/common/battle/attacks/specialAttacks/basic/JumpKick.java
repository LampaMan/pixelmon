/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;

public class JumpKick
extends SpecialAttackBase {
    @Override
    public void applyMissEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.getBattleAbility() instanceof MagicGuard) {
            return;
        }
        user.bc.sendToAll("pixelmon.effect.hurtlanding", user.getNickname());
        user.doBattleDamage(user, user.getPercentMaxHealth(50.0f), DamageTypeEnum.CRASH);
    }
}

