/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.heldItems.NoItem;
import java.util.ArrayList;

public class gMaxReplenish
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (Math.random() >= 0.5) {
            for (PixelmonWrapper ally : user.bc.getTeamPokemon(user.getParticipant())) {
                ItemHeld consumedItem = ally.getConsumedItem();
                if (consumedItem == NoItem.noItem || ally.hasHeldItem() || !consumedItem.isBerry()) continue;
                ally.bc.sendToAll("pixelmon.effect.replenish", ally.getNickname(), consumedItem.getLocalizedName());
                ally.setHeldItem(consumedItem);
                ally.setConsumedItem(null);
                return AttackResult.proceed;
            }
            ItemHeld consumedItem = user.getConsumedItem();
            if (consumedItem != NoItem.noItem && !user.hasHeldItem() && consumedItem.isBerry()) {
                user.bc.sendToAll("pixelmon.effect.replenish", user.getNickname(), consumedItem.getLocalizedName());
                user.setHeldItem(consumedItem);
                user.setConsumedItem(null);
                return AttackResult.proceed;
            }
        }
        user.bc.sendToAll("pixelmon.effect.noreplenish", user.getNickname());
        return AttackResult.proceed;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        userChoice.raiseWeight(25.0f);
    }
}

