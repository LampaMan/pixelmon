/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.Judgment;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.enums.forms.EnumUrshifu;
import com.pixelmongenerations.core.network.PixelmonData;
import java.util.Arrays;
import java.util.List;

public class MaxAttackHelper {
    private static final List<String> moves150basePower = Arrays.asList("Blast Burn", "Eruption", "Eternabeam", "Explosion", "Frenzy Plant", "Giga Impact", "Head Smash", "Hydro Cannon", "Hyper Beam", "Mind Blown", "Prismatic Laser", "Roar of Time", "Rock Wrecker", "Self-Destruct", "Shell Trap", "V-create", "Water Spout");
    private static final List<String> moves140basePower = Arrays.asList("Aura Wheel", "Blizzard", "Blue Flare", "Bolt Strike", "Boomburst", "Brave Bird", "Burn Up", "Clanging Scales", "Crush Grip", "Doom Desire", "Double Iron Bash", "Double-Edge", "Draco Meteor", "Dragon Ascent", "Dynamax Cannon", "Fire Blast", "Flare Blitz", "Fleur Cannon", "Freeze Shock", "Future Sight", "Head Charge", "Hurricane", "Hydro Pump", "Ice Burn", "Last Resort", "Leaf Storm", "Light of Ruin", "Mega Kick", "Megahorn", "Origin Pulse", "Outrage", "Overheat", "Petal Dance", "Power Whip", "Precipice Blades", "Psycho Boost", "Pyro Ball", "Seed Flare", "Shadow Force", "Skull Bash", "Sky Attack", "Solar Beam", "Solar Blade", "Steam Eruption", "Steel Beam", "Techno Blast", "Thrash", "Thunder", "Volt Tackle", "Wood Hammer", "Zap Cannon");
    private static final List<String> moves130basePower = Arrays.asList("Aeroblast", "Air Slash", "Anchor Shot", "Apple Acid", "Aqua Tail", "Attack Order", "Beak Blast", "Behemoth Bash", "Behemoth Blade", "Blaze Kick", "Body Slam", "Bolt Beak", "Bone Rush", "Bonemerang", "Bounce", "Bug Buzz", "Bullet Seed", "Core Enforcer", "Crabhammer", "Crunch", "Crush Claw", "Dark Pulse", "Darkest Lariat", "Dazzling Gleam", "Diamond Storm", "Dig", "Discharge", "Dive", "Dragon Claw", "Dragon Darts", "Dragon Hammer", "Dragon Pulse", "Dragon Rush", "Dream Eater", "Drill Peck", "Drill Run", "Drum Beating", "Dual Chop", "Earth Power", "Earthquake", "Electro Ball", "Endeavor", "Energy Ball", "Extrasensory", "Extreme Speed", "False Surrender", "Fiery Dance", "Fire Lash", "Fire Pledge", "Fire Punch", "First Impression", "Fishious Rend", "Fissure", "Flail", "Flamethrower", "Flash Cannon", "Fly", "Foul Play", "Fusion Bolt", "Fusion Flare", "Gear Grind", "Giga Drain", "Grass Knot", "Grass Pledge", "Grav Apple", "Guillotine", "Gyro Ball", "Heat Crash", "Heat Wave", "Heavy Slam", "High Horsepower", "Horn Drill", "Horn Leech", "Hyper Fang", "Hyper Voice", "Hyperspace Fury", "Hyperspace Hole", "Ice Beam", "Ice Hammer", "Ice Punch", "Icicle Crash", "Icicle Spear", "Inferno", "Iron Head", "Iron Tail", "Jaw Lock", "Judgment", "Land's Wrath", "Lava Plume", "Leaf Blade", "Leech Life", "Liquidation", "Lunge", "Magma Storm", "Mega Punch", "Meteor Mash", "Moonblast", "Moongeist Beam", "Muddy Water", "Mystical Fire", "Night Daze", "Oblivion Wing", "Overdrive", "Petal Blizzard", "Phantom Force", "Photon Geyser", "Pin Missile", "Plasma Fists", "Play Rough", "Pollen Puff", "Power Gem", "Power Trip", "Psychic", "Psychic Fangs", "Psyshock", "Psystrike", "Razor Shell", "Relic Song", "Revelation Dance", "Rock Blast", "Rock Slide", "Sacred Fire", "Scald", "Searing Shot", "Seed Bomb", "Shadow Ball", "Shadow Bone", "Sheer Cold", "Slam", "Snipe Shot", "Spacial Rend", "Sparkling Aria", "Spectral Thief", "Spirit Break", "Spirit Shackle", "Stomping Tantrum", "Stone Edge", "Stored Power", "Strange Steam", "Strength", "Sunsteel Strike", "Surf", "Tail Slap", "Take Down", "Thousand Arrows", "Thousand Waves", "Throat Chop", "Thunder Punch", "Thunderbolt", "Tri Attack", "Uproar", "Water Pledge", "Waterfall", "Weather Ball", "Wild Charge", "X-Scissor", "Zen Headbutt", "Zing Zap");
    private static final List<String> moves120basePower = Arrays.asList("Aurora Beam", "Brine", "Bubble Beam", "Chatter", "Double Hit", "Facade", "Fire Fang", "Freeze-Dry", "Glaciate", "Headbutt", "Hex", "Horn Attack", "Ice Fang", "Knock Off", "Leaf Tornado", "Luster Purge", "Mist Ball", "Night Slash", "Octazooka", "Parabolic Charge", "Psybeam", "Psycho Cut", "Retaliate", "Shadow Claw", "Slash", "Smart Strike", "Spark", "Steel Wing", "Stomp", "Sucker Punch", "Thunder Fang", "Trop Kick", "U-turn", "Volt Switch");
    private static final List<String> moves110basePower = Arrays.asList("Acrobatics", "Aerial Ace", "Air Cutter", "Ancient Power", "Assurance", "Avalanche", "Bite", "Breaking Swipe", "Brutal Swing", "Bug Bite", "Bulldoze", "Covet", "Dragon Breath", "Dragon Tail", "Electroweb", "Flame Wheel", "Frost Breath", "Icy Wind", "Incinerate", "Magical Leaf", "Mud Shot", "Pluck", "Razor Leaf", "Rock Tomb", "Round", "Shadow Punch", "Shock Wave", "Snarl", "Swift", "Thief", "Vise Grip", "Water Pulse", "Wing Attack");
    private static final List<String> moves100basePower = Arrays.asList("Beat Up", "Charge Beam", "Confusion", "Cut", "Draining Kiss", "Fell Stinger", "Final Gambit", "Flame Charge", "Fling", "Focus Punch", "Fury Swipes", "Low Kick", "Metal Burst", "Metal Claw", "Meteor Assault", "Mirror Coat", "Nature's Madness", "Night Shade", "Payback", "Present", "Rapid Spin", "Reversal", "Rock Throw", "Smack Down", "Snore", "Spit Up", "Struggle Bug", "Super Fang", "Vine Whip");
    private static final List<String> moves95basePower = Arrays.asList("Belch", "Close Combat", "Focus Blast", "Gunk Shot", "High Jump Kick", "Multi-Attack", "Superpower");
    private static final List<String> moves90basePower = Arrays.asList("Absorb", "Accelerock", "Aqua Jet", "Astonish", "Aura Sphere", "Bind", "Body Press", "Branch Poke", "Brick Break", "Bullet Punch", "Cross Chop", "Disarming Voice", "Drain Punch", "Dynamic Punch", "Echoed Voice", "Ember", "Fairy Wind", "Fake Out", "False Swipe", "Feint", "Fire Spin", "Flying Press", "Fury Attack", "Fury Cutter", "Gust", "Hammer Arm", "Hold Back", "Ice Shard", "Infestation", "Leafage", "Lick", "Mega Drain", "Mud-Slap", "Nuzzle", "Pay Day", "Peck", "Poison Jab", "Pound", "Powder Snow", "Quick Attack", "Rollout", "Sacred Sword", "Sand Tomb", "Scratch", "Secret Sword", "Shadow Sneak", "Sludge Bomb", "Sludge Wave", "Snap Trap", "Submission", "Tackle", "Thunder Shock", "Twister", "Water Gun", "Water Shuriken", "Whirlpool", "Wrap");
    private static final List<String> moves85basePower = Arrays.asList("Cross Poison", "Low Sweep", "Sludge", "Venoshock", "Vital Throw");
    private static final List<String> moves80basePower = Arrays.asList("Circle Throw", "Double Kick", "Force Palm", "Revenge", "Storm Throw", "Triple Kick");
    private static final List<String> moves75basePower = Arrays.asList("Clear Smog", "Counter", "Poison Fang", "Poison Tail", "Seismic Toss");
    private static final List<String> moves70basePower = Arrays.asList("Acid", "Acid Spray", "Arm Thrust", "Mach Punch", "Poison Sting", "Power-Up Punch", "Rock Smash", "Smog", "Vacuum Wave");
    private static final List<String> maxMoves160BasePower = Arrays.asList("G-Max Fireball", "G-Max Drum Solo", "G-Max Hydrosnipe");

    public static Attack getMaxMove(PixelmonData pokemon, Attack attack) {
        Attack maxAttack = new Attack(MaxAttackHelper.getMaxMoveName(pokemon, attack));
        maxAttack.overrideAttackCategory(attack.getAttackCategory());
        maxAttack.pp = attack.pp;
        maxAttack.ppBase = attack.ppBase;
        MaxAttackHelper.applyBasePower(attack, maxAttack);
        maxAttack.isMaxMove = true;
        maxAttack.getAttackBase().targetingInfo = attack.getAttackBase().targetingInfo;
        return maxAttack;
    }

    private static String getMaxMoveName(PixelmonData pokemon, Attack attack) {
        boolean gmax;
        EnumSpecies species = pokemon.getSpecies();
        boolean bl = gmax = species.hasGmaxForm() && pokemon.hasGmaxFactor;
        if (species == EnumSpecies.Arceus) {
            if (attack.isAttack("Judgment")) {
                Judgment j = new Judgment();
                EnumType judgmentType = j.getTypeFromForm(pokemon.form);
                switch (judgmentType) {
                    case Bug: {
                        return "Max Flutterby";
                    }
                    case Dark: {
                        return "Max Darkness";
                    }
                    case Dragon: {
                        return "Max Wyrmwind";
                    }
                    case Electric: {
                        return "Max Lightning";
                    }
                    case Fairy: {
                        return "Max Starfall";
                    }
                    case Fire: {
                        return "Max Flare";
                    }
                    case Fighting: {
                        return "Max Knuckle";
                    }
                    case Flying: {
                        return "Max Airstream";
                    }
                    case Ghost: {
                        return "Max Phantasm";
                    }
                    case Grass: {
                        return "Max Overgrowth";
                    }
                    case Ground: {
                        return "Max Quake";
                    }
                    case Ice: {
                        return "Max Hailstorm";
                    }
                    case Normal: {
                        return "Max Strike";
                    }
                    case Poison: {
                        return "Max Ooze";
                    }
                    case Psychic: {
                        return "Max Mindstorm";
                    }
                    case Rock: {
                        return "Max Rockfall";
                    }
                    case Steel: {
                        return "Max Steelspike";
                    }
                    case Water: {
                        return "Max Geyser";
                    }
                }
            }
        }
        if (attack.getAttackCategory() == AttackCategory.Status) {
            return "Max Guard";
        }
        switch (attack.getType()) {
            case Bug: {
                return gmax && species == EnumSpecies.Butterfree ? "G-Max Befuddle" : "Max Flutterby";
            }
            case Dark: {
                return gmax && species == EnumSpecies.Grimmsnarl ? "G-Max Snooze" : (gmax && species == EnumSpecies.Urshifu && pokemon.form == EnumUrshifu.Single.getForm() ? "G-Max One Blow" : "Max Darkness");
            }
            case Dragon: {
                return gmax && species == EnumSpecies.Duraludon ? "G-Max Depletion" : "Max Wyrmwind";
            }
            case Electric: {
                return gmax && species == EnumSpecies.Pikachu ? "G-Max Volt Crash" : (gmax && species == EnumSpecies.Toxtricity ? "G-Max Stun Shock" : "Max Lightning");
            }
            case Fairy: {
                return gmax && species == EnumSpecies.Hatterene ? "G-Max Smite" : (gmax && species == EnumSpecies.Alcremie ? "G-Max Finale" : "Max Starfall");
            }
            case Fighting: {
                return gmax && species == EnumSpecies.Machamp ? "G-Max Chi Strike" : "Max Knuckle";
            }
            case Fire: {
                return gmax && species == EnumSpecies.Cinderace ? "G-Max Fireball" : (gmax && species == EnumSpecies.Charizard ? "G-Max Wildfire" : (gmax && species == EnumSpecies.Centiskorch ? "G-Max Centiferno" : "Max Flare"));
            }
            case Flying: {
                return gmax && species == EnumSpecies.Corviknight ? "G-Max Wind Rage" : "Max Airstream";
            }
            case Ghost: {
                return gmax && species == EnumSpecies.Gengar ? "G-Max Terror" : "Max Phantasm";
            }
            case Grass: {
                return gmax && species == EnumSpecies.Venusaur ? "G-Max Vine Lash" : (gmax && species == EnumSpecies.Rillaboom ? "G-Max Drum Solo" : (gmax && species == EnumSpecies.Flapple ? "G-Max Tartness" : (gmax && species == EnumSpecies.Appletun ? "G-Max Sweetness" : "Max Overgrowth")));
            }
            case Ground: {
                return gmax && species == EnumSpecies.Sandaconda ? "G-Max Sandblast" : "Max Quake";
            }
            case Ice: {
                return gmax && species == EnumSpecies.Lapras ? "G-Max Resonance" : "Max Hailstorm";
            }
            case Normal: {
                return gmax && species == EnumSpecies.Eevee ? "G-Max Cuddle" : (gmax && species == EnumSpecies.Meowth ? "G-Max Gold Rush" : (gmax && species == EnumSpecies.Snorlax ? "G-Max Replenish" : "Max Strike"));
            }
            case Poison: {
                return gmax && species == EnumSpecies.Garbodor ? "G-Max Malodor" : "Max Ooze";
            }
            case Psychic: {
                return gmax && species == EnumSpecies.Orbeetle ? "G-Max Gravitas" : "Max Mindstorm";
            }
            case Rock: {
                return gmax && species == EnumSpecies.Coalossal ? "G-Max Volcalith" : "Max Rockfall";
            }
            case Steel: {
                return gmax && species == EnumSpecies.Melmetal ? "G-Max Meltdown" : (gmax && species == EnumSpecies.Copperajah ? "G-Max Steelsurge" : "Max Steelspike");
            }
            case Water: {
                return gmax && species == EnumSpecies.Blastoise ? "G-Max Cannonade" : (gmax && species == EnumSpecies.Kingler ? "G-Max Foam Burst" : (gmax && species == EnumSpecies.Inteleon ? "G-Max Hydrosnipe" : (gmax && species == EnumSpecies.Drednaw ? "G-Max Stonesurge" : (gmax && species == EnumSpecies.Urshifu && pokemon.form == EnumUrshifu.Rapid.getForm() ? "G-Max Rapid Flow" : "Max Geyser"))));
            }
        }
        return "Error now default case";
    }

    private static void applyBasePower(Attack normal, Attack maxAttack) {
        if (maxMoves160BasePower.contains(maxAttack.getAttackBase().getUnlocalizedName())) {
            maxAttack.movePower = 160;
            return;
        }
        maxAttack.movePower = moves150basePower.contains(normal.getAttackBase().getUnlocalizedName()) ? 150 : (moves140basePower.contains(normal.getAttackBase().getUnlocalizedName()) ? 140 : (moves130basePower.contains(normal.getAttackBase().getUnlocalizedName()) ? 130 : (moves120basePower.contains(normal.getAttackBase().getUnlocalizedName()) ? 120 : (moves110basePower.contains(normal.getAttackBase().getUnlocalizedName()) ? 110 : (moves100basePower.contains(normal.getAttackBase().getUnlocalizedName()) ? 100 : (moves95basePower.contains(normal.getAttackBase().getUnlocalizedName()) ? 95 : (moves90basePower.contains(normal.getAttackBase().getUnlocalizedName()) ? 90 : (moves85basePower.contains(normal.getAttackBase().getUnlocalizedName()) ? 85 : (moves80basePower.contains(normal.getAttackBase().getUnlocalizedName()) ? 80 : (moves75basePower.contains(normal.getAttackBase().getUnlocalizedName()) ? 75 : (moves70basePower.contains(normal.getAttackBase().getUnlocalizedName()) ? 70 : 1)))))))))));
    }
}

