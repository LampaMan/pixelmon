/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Cursed;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;

public class Curse
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.hasType(EnumType.Ghost)) {
            if (user == target && (target = RandomHelper.getRandomElementFromList(user.getOpponentPokemon())) == null) {
                return AttackResult.notarget;
            }
            if (target.hasStatus(StatusType.Cursed)) {
                user.bc.sendToAll("attack.curse.alreadycursed", target.getNickname());
                return AttackResult.failed;
            }
            if (target.addStatus(new Cursed(), user)) {
                user.bc.sendToAll("attack.curse.curse", user.getNickname(), target.getNickname());
                user.doBattleDamage(user, user.getPercentMaxHealth(50.0f), DamageTypeEnum.STATUS);
                return AttackResult.succeeded;
            }
        } else {
            user.getBattleStats().modifyStat(new int[]{1, 1, -1}, new StatsType[]{StatsType.Attack, StatsType.Defence, StatsType.Speed});
            return AttackResult.succeeded;
        }
        return AttackResult.failed;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (pw.hasType(EnumType.Ghost)) {
            if (!userChoice.hitsAlly() && MoveChoice.getMaxDamagePercent(pw, bestOpponentChoices) <= pw.getHealthPercent() - 50.0f) {
                userChoice.raiseWeight(40.0f);
            }
        } else {
            StatsEffect attackBoost = new StatsEffect(StatsType.Attack, 1, true);
            StatsEffect defenseBoost = new StatsEffect(StatsType.Defence, 1, true);
            attackBoost.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
            defenseBoost.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
        }
    }
}

