/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.attackModifiers;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.Value;
import com.pixelmongenerations.common.battle.attacks.ValueType;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.attackModifiers.AttackModifierBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class Damage
extends AttackModifierBase {
    ValueType damageType;
    int damageValue;

    public Damage(Value ... values) {
        this.damageType = values[0].type;
        this.damageValue = values[0].value;
    }

    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        double dmg = this.damageType == ValueType.Percent ? (double)target.getPercentMaxHealth((float)this.damageValue / 100.0f) : (double)this.damageValue;
        target.doBattleDamage(user, (int)dmg, DamageTypeEnum.ATTACKFIXED);
        user.attack.doMove(user, target);
        return AttackResult.succeeded;
    }
}

