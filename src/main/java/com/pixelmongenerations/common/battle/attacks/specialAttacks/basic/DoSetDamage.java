/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.Value;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ParentalBond;

public class DoSetDamage
extends SpecialAttackBase {
    int damage;

    public DoSetDamage(Value ... values) {
        this.damage = values[0].value;
    }

    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        boolean hasParentalBond = user.getBattleAbility() instanceof ParentalBond;
        if (user.attack.isAttack("Seismic Toss")) {
            int damage;
            if (hasParentalBond && user.isAlive() && target.isAlive()) {
                damage = user.getLevelNum();
                target.doBattleDamage(user, damage, DamageTypeEnum.ATTACKFIXED);
                user.bc.sendToAll("multiplehit.times", user.getNickname(), 2);
            }
            damage = user.getLevelNum();
            target.doBattleDamage(user, damage, DamageTypeEnum.ATTACKFIXED);
        } else if (this.damage != 0) {
            target.doBattleDamage(user, this.damage, DamageTypeEnum.ATTACKFIXED);
            if (hasParentalBond && user.isAlive() && target.isAlive()) {
                target.doBattleDamage(user, this.damage, DamageTypeEnum.ATTACKFIXED);
                user.bc.sendToAll("multiplehit.times", user.getNickname(), 2);
            }
        } else {
            int newDamage = target.getBaseHealth() / 2;
            target.doBattleDamage(user, newDamage, DamageTypeEnum.ATTACKFIXED);
            if (hasParentalBond && user.isAlive() && target.isAlive()) {
                newDamage = target.getBaseHealth() / 2;
                target.doBattleDamage(user, newDamage, DamageTypeEnum.ATTACKFIXED);
                user.bc.sendToAll("multiplehit.times", user.getNickname(), 2);
            }
        }
        return AttackResult.hit;
    }
}

