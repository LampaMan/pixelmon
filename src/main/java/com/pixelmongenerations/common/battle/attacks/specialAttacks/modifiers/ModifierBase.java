/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.modifiers;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.modifiers.ModifierType;

public class ModifierBase {
    public transient ModifierType type;
    public float value;

    public ModifierBase(ModifierType type) {
        this.type = type;
    }
}

