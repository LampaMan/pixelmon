/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.Spectator;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.battle.EnumBattleType;
import com.pixelmongenerations.core.network.packetHandlers.battles.SwapPosition;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AllySwitch
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.simulateMode) {
            return AttackResult.failed;
        }
        ArrayList<PixelmonWrapper> team = user.bc.getTeamPokemon(user.getParticipant());
        if (team.size() > 1 && user.bc.getTeam(user.getParticipant()).size() == 1) {
            PixelmonWrapper switchPokemon = null;
            if (user.bc.rules.battleType == EnumBattleType.Double) {
                for (PixelmonWrapper pw : team) {
                    if (pw == user) continue;
                    switchPokemon = pw;
                }
            } else if (user.bc.rules.battleType == EnumBattleType.Triple) {
                if (user.battlePosition == 1) {
                    user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
                    return AttackResult.failed;
                }
                for (PixelmonWrapper pw : team) {
                    if (pw.battlePosition == 1 || pw == user) continue;
                    switchPokemon = pw;
                }
            }
            if (switchPokemon == null || switchPokemon.isFainted()) {
                user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
                return AttackResult.failed;
            }
            user.bc.sendToAll("pixelmon.effect.allyswitch", user.getNickname(), switchPokemon.getNickname());
            List<StatusBase> userStatuses = user.getStatuses();
            List<StatusBase> allyStatuses = switchPokemon.getStatuses();
            List<StatusBase> userPositionStatuses = this.removePositionStatuses(userStatuses);
            List<StatusBase> allyPositionStatuses = this.removePositionStatuses(allyStatuses);
            allyStatuses.addAll(userPositionStatuses.stream().collect(Collectors.toList()));
            userStatuses.addAll(allyPositionStatuses.stream().collect(Collectors.toList()));
            int userPosition = user.battlePosition;
            user.battlePosition = switchPokemon.battlePosition;
            switchPokemon.battlePosition = userPosition;
            for (int i = user.bc.turn + 1; i < user.bc.turnList.size(); ++i) {
                PixelmonWrapper current = user.bc.turnList.get(i);
                if (current == user || current == switchPokemon || current.attack == null || current.targets == null) continue;
                for (int j = 0; j < current.targets.size(); ++j) {
                    if (current.targets.get(j) == user) {
                        current.targets.set(j, switchPokemon);
                        continue;
                    }
                    if (current.targets.get(j) != switchPokemon) continue;
                    current.targets.set(j, user);
                }
            }
            for (BattleParticipant participant : user.bc.participants) {
                if (participant.getType() != ParticipantType.Player) continue;
                Pixelmon.NETWORK.sendTo(new SwapPosition(user, switchPokemon), ((PlayerParticipant)participant).player);
            }
            for (Spectator spectator : user.bc.spectators) {
                spectator.sendMessage(new SwapPosition(user, switchPokemon));
            }
            return AttackResult.succeeded;
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }

    private List<StatusBase> removePositionStatuses(List<StatusBase> statuses) {
        ArrayList<StatusBase> positionStatuses = new ArrayList<StatusBase>(statuses.size());
        for (int i = 0; i < statuses.size(); ++i) {
            StatusBase status = statuses.get(i);
            if (!status.isTeamStatus() || status.isWholeTeamStatus()) continue;
            statuses.remove(status);
            positionStatuses.add(status);
        }
        return positionStatuses;
    }
}

