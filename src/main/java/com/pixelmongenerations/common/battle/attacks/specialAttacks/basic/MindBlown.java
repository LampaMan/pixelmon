/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Damp;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;

public class MindBlown
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.participants.stream().anyMatch(part -> part.controlledPokemon.stream().anyMatch(p -> p.getBattleAbility() instanceof Damp))) {
            return AttackResult.failed;
        }
        if (!(user.getBattleAbility() instanceof MagicGuard)) {
            user.doBattleDamage(user, user.getPercentMaxHealth(50.0f), DamageTypeEnum.SELF);
        }
        return super.applyEffectDuring(user, target);
    }
}

