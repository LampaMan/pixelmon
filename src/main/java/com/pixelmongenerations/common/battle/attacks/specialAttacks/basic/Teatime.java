/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;

public class Teatime
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.hasHeldItem() && ItemHeld.canEatBerry(user)) {
            if (target.hasHeldItem() && ItemHeld.canEatBerry(target)) {
                target.getConsumedItem().eatBerry(target);
                target.bc.sendToAll("pixelmon.status.stuffcheeks", target.getNickname());
            }
            user.getConsumedItem().eatBerry(user);
            user.bc.sendToAll("pixelmon.status.stuffcheeks", user.getNickname());
            return AttackResult.succeeded;
        }
        if (target.hasHeldItem() && ItemHeld.canEatBerry(target)) {
            target.getConsumedItem().eatBerry(target);
            target.bc.sendToAll("pixelmon.status.stuffcheeks", target.getNickname());
            return AttackResult.succeeded;
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", user.getNickname());
        return AttackResult.failed;
    }
}

