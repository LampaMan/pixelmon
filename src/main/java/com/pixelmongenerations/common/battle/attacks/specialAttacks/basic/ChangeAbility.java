/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.BattleBond;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Comatose;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Disguise;
import com.pixelmongenerations.common.entity.pixelmon.abilities.FakemonRevealed;
import com.pixelmongenerations.common.entity.pixelmon.abilities.IceFace;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Multitype;
import com.pixelmongenerations.common.entity.pixelmon.abilities.RKSSystem;
import com.pixelmongenerations.common.entity.pixelmon.abilities.StanceChange;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Truant;
import java.util.ArrayList;
import net.minecraft.util.text.translation.I18n;

public class ChangeAbility
extends SpecialAttackBase {
    transient Class<? extends AbilityBase> ability;

    public ChangeAbility(Class<? extends AbilityBase> ability) {
        this.ability = ability;
    }

    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        AbilityBase targetAbility = target.getBattleAbility(false);
        if (targetAbility.getClass().equals(this.ability) || targetAbility instanceof Multitype || targetAbility instanceof StanceChange || targetAbility instanceof Truant || targetAbility instanceof Comatose || targetAbility instanceof RKSSystem || targetAbility instanceof BattleBond || targetAbility instanceof Disguise || targetAbility instanceof IceFace || targetAbility instanceof FakemonRevealed) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        user.bc.sendToAll("pixelmon.effect.entrainment", target.getNickname(), targetAbility.getLocalizedName(), I18n.translateToLocal("ability." + this.ability.getSimpleName() + ".name"));
        target.setTempAbility(AbilityBase.getNewInstance(this.ability));
        return AttackResult.succeeded;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        ArrayList<PixelmonWrapper> allies = pw.getTeamPokemon();
        for (PixelmonWrapper target : userChoice.targets) {
            AbilityBase targetAbility = target.getBattleAbility(false);
            boolean negativeAbility = targetAbility.isNegativeAbility();
            boolean allied = allies.contains(target);
            if (allied && negativeAbility) {
                userChoice.raiseWeight(40.0f);
            } else if (allied || negativeAbility) continue;
            userChoice.raiseWeight(25.0f);
        }
    }
}

