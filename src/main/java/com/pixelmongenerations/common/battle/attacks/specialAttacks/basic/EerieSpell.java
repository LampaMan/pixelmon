/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Soundproof;
import java.util.ArrayList;

public class EerieSpell
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.simulateMode) {
            return AttackResult.proceed;
        }
        if (target.lastAttack != null && target.lastAttack.pp > 0 && !(target.getBattleAbility() instanceof Soundproof)) {
            user.bc.sendToAll("pixelmon.effect.ppdrop", target.getNickname(), target.lastAttack.getAttackBase().getLocalizedName());
            target.lastAttack.pp = Math.max(target.lastAttack.pp - 3, 0);
            return AttackResult.proceed;
        }
        if (!(target.getBattleAbility() instanceof Soundproof)) {
            return AttackResult.proceed;
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.hitsAlly()) {
            return;
        }
        for (PixelmonWrapper target : userChoice.targets) {
            if (target.lastAttack == null) {
                if (pw.bc.getFirstMover(pw, target) != target) continue;
            }
            userChoice.raiseWeight(15.0f);
        }
    }
}

