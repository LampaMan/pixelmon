/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.StickyHold;
import com.pixelmongenerations.common.item.ItemHeld;
import java.util.ArrayList;

public class BugBite
extends SpecialAttackBase {
    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (!user.bc.simulateMode && (!user.inParentalBond || target.isFainted()) && this.canEatBerry(user, target)) {
            ItemHeld targetItem = target.getHeldItem();
            user.bc.sendToAll("pixelmon.effect.bugbite", user.getNickname(), target.getNickname(), targetItem.getLocalizedName());
            ItemHeld tempItem = user.getHeldItem();
            ItemHeld tempConsumedItem = user.getConsumedItem();
            user.setHeldItem(targetItem);
            target.setConsumedItem(null);
            user.getHeldItem().eatBerry(user);
            user.setHeldItem(tempItem);
            user.setConsumedItem(tempConsumedItem);
            target.removeHeldItem();
        }
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private boolean canEatBerry(PixelmonWrapper user, PixelmonWrapper target) {
        if (!target.getHeldItem().isBerry()) return false;
        if (target.hasStatus(StatusType.Substitute)) return false;
        if (target.getBattleAbility(user) instanceof StickyHold) return false;
        return true;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        for (PixelmonWrapper target : userChoice.targets) {
            if (!this.canEatBerry(pw, target)) continue;
            userChoice.raiseWeight(30.0f);
        }
    }
}

