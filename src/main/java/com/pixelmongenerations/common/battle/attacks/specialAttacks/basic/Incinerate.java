/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.StickyHold;
import java.util.ArrayList;

public class Incinerate
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (this.canDestroyBerry(user, target)) {
            target.bc.sendToAll("pixelmon.effect.incinerate", target.getNickname(), target.getHeldItem().getLocalizedName());
            target.setNewHeldItem(null);
        }
        return AttackResult.proceed;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private boolean canDestroyBerry(PixelmonWrapper user, PixelmonWrapper target) {
        if (!target.getHeldItem().isBerry()) return false;
        if (target.getBattleAbility(user) instanceof StickyHold) return false;
        if (target.hasStatus(StatusType.Substitute)) return false;
        return true;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        for (PixelmonWrapper target : userChoice.targets) {
            if (!this.canDestroyBerry(pw, target)) continue;
            userChoice.raiseWeight(20.0f);
        }
    }
}

