/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.enums.EnumType;

public class Synchronoise
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        for (EnumType type : user.type) {
            if (type == null) continue;
            if (!target.hasType(type)) continue;
            return AttackResult.proceed;
        }
        user.bc.sendToAll("pixelmon.battletext.noeffect", target.getNickname());
        return AttackResult.failed;
    }
}

