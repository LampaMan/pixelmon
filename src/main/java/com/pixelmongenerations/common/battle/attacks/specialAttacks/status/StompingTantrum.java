/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public class StompingTantrum
extends StatusBase {
    public StompingTantrum() {
        super(StatusType.StompingTantrum);
    }

    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        return new int[]{power * (user.lastAttack.moveResult.result == AttackResult.failed || user.lastAttack.moveResult.result == AttackResult.unable || user.lastAttack.moveResult.result == AttackResult.ignore || user.lastAttack.moveResult.result == AttackResult.notarget ? 2 : 1), accuracy};
    }
}

