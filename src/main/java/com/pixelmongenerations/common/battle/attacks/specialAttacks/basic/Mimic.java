/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.MirrorMove;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.TempMoveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import java.util.ArrayList;

public class Mimic
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.simulateMode) {
            return AttackResult.succeeded;
        }
        Moveset originalMoveset = user.getMoveset();
        if (target.lastAttack != null) {
            Moveset moveset;
            boolean valid;
            if (!target.lastAttack.isAttack("Chatter", "Metronome", "Sketch", "Struggle") && !originalMoveset.hasAttack(target.lastAttack) && (valid = (moveset = originalMoveset.copy()).replaceMove("Mimic", new Attack(target.lastAttack.getAttackBase())))) {
                user.bc.sendToAll("pixelmon.effect.mimic", user.getNickname(), target.getNickname(), target.lastAttack.getAttackBase().getLocalizedName());
                user.addStatus(new TempMoveset(moveset), user);
                return AttackResult.succeeded;
            }
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        MirrorMove mirrorMove = new MirrorMove();
        mirrorMove.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
        userChoice.lowerTier(2);
    }
}

