/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.Value;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.LiquidOoze;
import com.pixelmongenerations.common.item.heldItems.ItemBigRoot;
import java.util.ArrayList;

public class Drain
extends SpecialAttackBase {
    int drainPercent;

    public Drain(Value ... values) {
        this.drainPercent = values[0].value;
    }

    @Override
    public void dealtDamage(PixelmonWrapper user, PixelmonWrapper target, Attack attack, DamageTypeEnum damageType) {
        int restoration = this.getRestoration(user.attack.moveResult.damage, user, target);
        if (restoration > 0) {
            if (target.getBattleAbility() instanceof LiquidOoze) {
                user.bc.sendToAll("pixelmon.abilities.liquidooze", user.getNickname());
                user.doBattleDamage(target, restoration, DamageTypeEnum.ABILITY);
            } else if (!user.hasFullHealth()) {
                if (!user.hasStatus(StatusType.HealBlock)) {
                    user.bc.sendToAll("pixelmon.effect.regainedenergy", user.getNickname());
                    user.healEntityBy(restoration);
                }
            }
        }
    }

    private int getRestoration(float damage, PixelmonWrapper user, PixelmonWrapper target) {
        int restoration = 0;
        if (damage > 0.0f && user.isAlive()) {
            restoration = Math.max(1, (int)damage * this.drainPercent / 100);
            if (user.getUsableHeldItem() instanceof ItemBigRoot) {
                restoration = (int)((double)restoration * 1.3);
            }
        }
        return restoration;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.hitsAlly()) {
            return;
        }
        for (PixelmonWrapper target : userChoice.targets) {
            int restoration = this.getRestoration(userChoice.result.damage, pw, target);
            if (restoration <= 0) continue;
            float restorePercent = pw.getHealthPercent(restoration);
            if (target.getBattleAbility() instanceof LiquidOoze) {
                userChoice.weight -= restorePercent;
                continue;
            }
            if (pw.hasStatus(StatusType.HealBlock)) continue;
            float healthDeficit = pw.getHealthDeficit();
            if (healthDeficit < (float)restoration) {
                restorePercent = pw.getHealthPercent(healthDeficit);
            }
            userChoice.raiseWeight(restorePercent);
        }
    }
}

