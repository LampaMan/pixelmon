/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn.MultiTurnSpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.SkyDropped;
import com.pixelmongenerations.common.battle.status.SkyDropping;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.ArrayList;

public class SkyDrop
extends MultiTurnSpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.hasStatus(StatusType.Substitute) || user.bc.globalStatusController.hasStatus(StatusType.Gravity) || target.isDynamaxed() || target.getWeight(false) > 200.0f) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        if (!this.doesPersist(user)) {
            this.setPersists(user, true);
            this.setTurnCount(user, 2);
        }
        this.decrementTurnCount(user);
        if (this.getTurnCount(user) == 1) {
            user.bc.sendToAll("pixelmon.effect.skydrop", user.getNickname(), target.getNickname());
            user.addStatus(new SkyDropping(), user);
            target.addStatus(new SkyDropped(user), user);
            target.removeStatus(StatusType.FollowMe);
            target.removeStatus(StatusType.Enrage);
            return AttackResult.charging;
        }
        boolean succeeded = user.removeStatus(StatusType.SkyDropping);
        target.removeStatus(StatusType.SkyDropped);
        this.setPersists(user, false);
        return succeeded || user.bc.simulateMode ? AttackResult.proceed : AttackResult.failed;
    }

    @Override
    public void removeEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.hasStatus(StatusType.SkyDropping)) {
            user.removeStatus(StatusType.SkyDropping);
            target.removeStatus(StatusType.SkyDropped);
        }
        this.setPersists(user, false);
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (pw.getUsableHeldItem().getHeldItemType() == EnumHeldItems.powerHerb) {
            return;
        }
        userChoice.weight *= 0.9f;
        if (pw.hasStatus(StatusType.Confusion, StatusType.Infatuated, StatusType.Paralysis)) {
            userChoice.weight /= 2.0f;
        }
    }
}

