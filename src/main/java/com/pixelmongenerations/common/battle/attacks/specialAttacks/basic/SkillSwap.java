/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.Entrainment;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AsOne;
import com.pixelmongenerations.common.entity.pixelmon.abilities.BattleBond;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Comatose;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Disguise;
import com.pixelmongenerations.common.entity.pixelmon.abilities.FakemonRevealed;
import com.pixelmongenerations.common.entity.pixelmon.abilities.GulpMissile;
import com.pixelmongenerations.common.entity.pixelmon.abilities.IceFace;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Illusion;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Multitype;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Schooling;
import com.pixelmongenerations.common.entity.pixelmon.abilities.StanceChange;
import com.pixelmongenerations.common.entity.pixelmon.abilities.WonderGuard;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ZenMode;
import java.util.ArrayList;

public class SkillSwap
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        AbilityBase userAbility = user.getBattleAbility(false);
        AbilityBase targetAbility = target.getBattleAbility(false);
        if (this.canSwap(userAbility) && this.canSwap(targetAbility) && !target.isDynamaxed()) {
            user.bc.sendToAll("pixelmon.effect.skillswap", user.getNickname(), targetAbility.getLocalizedName());
            user.setTempAbility(targetAbility);
            user.bc.sendToAll("pixelmon.effect.skillswap", target.getNickname(), userAbility.getLocalizedName());
            target.setTempAbility(userAbility);
            return AttackResult.succeeded;
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }

    private boolean canSwap(AbilityBase ability) {
        return !(ability instanceof AsOne) || !(ability instanceof Illusion) && !(ability instanceof Multitype) && !(ability instanceof StanceChange) && !(ability instanceof WonderGuard) && !(ability instanceof ZenMode) && !(ability instanceof Comatose) && !(ability instanceof BattleBond) && !(ability instanceof Disguise) && !(ability instanceof IceFace) && !(ability instanceof FakemonRevealed) && !(ability instanceof Schooling) && !(ability instanceof GulpMissile);
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        Entrainment entrainment = new Entrainment();
        entrainment.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
    }
}

