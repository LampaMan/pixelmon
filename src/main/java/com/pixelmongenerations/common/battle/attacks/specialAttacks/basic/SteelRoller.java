/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.ElectricTerrain;
import com.pixelmongenerations.common.battle.status.GrassyTerrain;
import com.pixelmongenerations.common.battle.status.MistyTerrain;
import com.pixelmongenerations.common.battle.status.PsychicTerrain;
import com.pixelmongenerations.common.battle.status.StatusType;

public class SteelRoller
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.bc.globalStatusController.getTerrain() instanceof GrassyTerrain) {
            target.bc.globalStatusController.removeGlobalStatus(StatusType.GrassyTerrain);
            user.bc.sendToAll("pixelmon.status.grassyterrainend", new Object[0]);
            return AttackResult.proceed;
        }
        if (target.bc.globalStatusController.getTerrain() instanceof MistyTerrain) {
            target.bc.globalStatusController.removeGlobalStatus(StatusType.MistyTerrain);
            user.bc.sendToAll("pixelmon.status.mistyterrainend", new Object[0]);
            return AttackResult.proceed;
        }
        if (target.bc.globalStatusController.getTerrain() instanceof ElectricTerrain) {
            target.bc.globalStatusController.removeGlobalStatus(StatusType.ElectricTerrain);
            user.bc.sendToAll("pixelmon.status.electricterrainend", new Object[0]);
            return AttackResult.proceed;
        }
        if (target.bc.globalStatusController.getTerrain() instanceof PsychicTerrain) {
            target.bc.globalStatusController.removeGlobalStatus(StatusType.PsychicTerrain);
            user.bc.sendToAll("pixelmon.status.psychicterrainend", new Object[0]);
            return AttackResult.proceed;
        }
        user.bc.sendToAll("pixelmon.battletext.butmovefailed", new Object[0]);
        return AttackResult.failed;
    }
}

