/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Terrain;
import net.minecraft.block.Block;
import net.minecraft.block.BlockBone;
import net.minecraft.block.BlockMagma;
import net.minecraft.block.BlockNetherrack;
import net.minecraft.block.BlockSoulSand;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeSwamp;

public class TerrainExamine {
    public static TerrainType getTerrain(PixelmonWrapper user) {
        Block block;
        IBlockState state;
        Material blockMaterial;
        if (user == null || user.bc == null) {
            return TerrainType.Default;
        }
        Terrain terrain = user.bc.globalStatusController.getTerrain();
        switch (terrain.type) {
            case GrassyTerrain: {
                return TerrainType.Grass;
            }
            case MistyTerrain: {
                return TerrainType.Misty;
            }
            case ElectricTerrain: {
                return TerrainType.Electric;
            }
            case PsychicTerrain: {
                return TerrainType.Psychic;
            }
        }
        BlockPos userPosition = user.getWorldPosition();
        World world = user.getWorld();
        Biome biome = world.getBiome(userPosition);
        int numBelow = 0;
        do {
            state = world.getBlockState(userPosition.down(numBelow));
            block = state.getBlock();
        } while ((blockMaterial = state.getMaterial()) == Material.AIR && ++numBelow < 5);
        String blockName = block.getTranslationKey();
        if (blockMaterial == Material.WATER && biome instanceof BiomeSwamp) {
            return TerrainType.Swamp;
        }
        if (blockMaterial == Material.GRASS || blockMaterial == Material.WOOD || blockMaterial == Material.LEAVES || blockMaterial == Material.PLANTS || blockMaterial == Material.VINE || blockMaterial == Material.CACTUS || blockMaterial == Material.GOURD) {
            return TerrainType.Grass;
        }
        if (blockMaterial == Material.WATER || blockMaterial == Material.SPONGE || blockMaterial == Material.CORAL) {
            return TerrainType.Water;
        }
        if (blockMaterial == Material.ICE || blockMaterial == Material.PACKED_ICE) {
            return TerrainType.Ice;
        }
        if (blockMaterial == Material.SNOW || blockMaterial == Material.CRAFTED_SNOW) {
            return TerrainType.Snow;
        }
        if (blockMaterial == Material.AIR) {
            return TerrainType.Sky;
        }
        if (block instanceof BlockSoulSand || block instanceof BlockBone) {
            return TerrainType.Burial;
        }
        if (blockMaterial == Material.SAND || blockMaterial == Material.CLAY || blockMaterial == Material.GROUND) {
            return TerrainType.Sand;
        }
        if (blockMaterial == Material.PORTAL || blockMaterial == Material.DRAGON_EGG || blockMaterial == Material.STRUCTURE_VOID || blockName.equals("whiteStone") || blockName.contains("end") || blockName.contains("purpur")) {
            return TerrainType.Space;
        }
        if (blockMaterial == Material.LAVA || blockMaterial == Material.FIRE || blockName.contains("nether") || block instanceof BlockNetherrack || block instanceof BlockMagma) {
            return TerrainType.Volcano;
        }
        if (blockMaterial == Material.ROCK) {
            return TerrainType.Cave;
        }
        return TerrainType.Default;
    }

    public static enum TerrainType {
        Default,
        Cave,
        Sand,
        Swamp,
        Water,
        Snow,
        Ice,
        Volcano,
        Burial,
        Sky,
        Space,
        Grass,
        Misty,
        Electric,
        Psychic;

    }
}

