/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn.MultiTurnCharge;

public class RazorWind
extends MultiTurnCharge {
    public RazorWind() {
        super("pixelmon.effect.razorwind");
    }
}

