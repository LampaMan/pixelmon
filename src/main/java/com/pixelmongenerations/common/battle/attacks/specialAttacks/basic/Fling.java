/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Burn;
import com.pixelmongenerations.common.battle.status.Flinch;
import com.pixelmongenerations.common.battle.status.Paralysis;
import com.pixelmongenerations.common.battle.status.Poison;
import com.pixelmongenerations.common.battle.status.PoisonBadly;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.heldItems.EVAdjusting;
import com.pixelmongenerations.common.item.heldItems.ItemMentalHerb;
import com.pixelmongenerations.common.item.heldItems.ItemWhiteHerb;
import com.pixelmongenerations.common.item.heldItems.NoItem;
import com.pixelmongenerations.common.item.heldItems.TypeEnhancingItems;
import com.pixelmongenerations.core.enums.heldItems.EnumEvAdjustingItems;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.enums.heldItems.EnumTypeEnhancingItems;
import java.util.ArrayList;

public class Fling
extends SpecialAttackBase {
    private transient ItemHeld heldItem;

    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        user.attack.getAttackBase().basePower = 0;
        ItemHeld item = user.getUsableHeldItem();
        if (user.hasHeldItem() && user.isItemRemovable(user)) {
            switch (item.getHeldItemType()) {
                case ironBall: {
                    user.attack.getAttackBase().basePower = 130;
                    break;
                }
                case typeEnhancer: {
                    switch (((TypeEnhancingItems)item).enhanceType) {
                        case hardStone: {
                            user.attack.getAttackBase().basePower = 100;
                            break;
                        }
                        case plate: {
                            user.attack.getAttackBase().basePower = 90;
                            break;
                        }
                        case poisonBarb: 
                        case dragonFang: {
                            user.attack.getAttackBase().basePower = 70;
                            break;
                        }
                        case sharpBeak: {
                            user.attack.getAttackBase().basePower = 50;
                            break;
                        }
                        case blackBelt: 
                        case blackGlasses: 
                        case charcoal: 
                        case magnet: 
                        case miracleSeed: 
                        case mysticWater: 
                        case neverMeltIce: 
                        case spellTag: 
                        case twistedSpoon: {
                            user.attack.getAttackBase().basePower = 30;
                            break;
                        }
                    }
                    user.attack.getAttackBase().basePower = 10;
                    break;
                }
                case deepSeaTooth: 
                case gripClaw: 
                case thickClub: {
                    user.attack.getAttackBase().basePower = 90;
                    break;
                }
                case assaultVest: 
                case electirizer: 
                case magmarizer: 
                case protector: 
                case quickClaw: 
                case razorClaw: 
                case stickyBarb: {
                    user.attack.getAttackBase().basePower = 80;
                    break;
                }
                case evAdjusting: {
                    user.attack.getAttackBase().basePower = ((EVAdjusting)item).type == EnumEvAdjustingItems.MachoBrace ? 60 : 70;
                    break;
                }
                case dampRock: 
                case heatRock: 
                case rockyHelmet: 
                case leek: {
                    user.attack.getAttackBase().basePower = 60;
                    break;
                }
                case dubiousDisc: {
                    user.attack.getAttackBase().basePower = 50;
                    break;
                }
                case eviolite: 
                case icyRock: 
                case luckyPunch: {
                    user.attack.getAttackBase().basePower = 40;
                    break;
                }
                case absorbbulb: 
                case berryJuice: 
                case bindingBand: 
                case blackSludge: 
                case cellbattery: 
                case deepSeaScale: 
                case dragonScale: 
                case ejectButton: 
                case everStone: 
                case expShare: 
                case flameOrb: 
                case floatStone: 
                case kingsRock: 
                case lifeorb: 
                case lightBall: 
                case lightClay: 
                case luckyEgg: 
                case metalCoat: 
                case metalPowder: 
                case metronome: 
                case prismScale: 
                case quickPowder: 
                case razorFang: 
                case scopeLens: 
                case shellBell: 
                case smokeBall: 
                case soulDew: 
                case toxicOrb: 
                case upGrade: {
                    user.attack.getAttackBase().basePower = 30;
                    break;
                }
                case gems: 
                case mail: 
                case other: {
                    user.attack.getAttackBase().basePower = 0;
                    break;
                }
                default: {
                    user.attack.getAttackBase().basePower = 10;
                }
            }
        }
        if (user.attack.getAttackBase().basePower == 0) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            this.heldItem = NoItem.noItem;
            return AttackResult.failed;
        }
        user.bc.sendToAll("pixelmon.effect.fling", user.getNickname(), item.getLocalizedName());
        this.heldItem = user.getHeldItem();
        user.consumeItem();
        return AttackResult.proceed;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.isAlive() && this.heldItem != NoItem.noItem) {
            EnumHeldItems type = this.heldItem.getHeldItemType();
            if (type == EnumHeldItems.kingsRock || type == EnumHeldItems.razorFang) {
                Flinch.flinch(user, target);
            } else if (type == EnumHeldItems.lightBall) {
                Paralysis.paralyze(user, target, user.attack, true);
            } else if (type == EnumHeldItems.flameOrb) {
                Burn.burn(user, target, user.attack, true);
            } else if (type == EnumHeldItems.toxicOrb) {
                PoisonBadly.poisonBadly(user, target, user.attack, true);
            } else if (type == EnumHeldItems.typeEnhancer && ((TypeEnhancingItems)this.heldItem).enhanceType == EnumTypeEnhancingItems.poisonBarb) {
                Poison.poison(user, target, user.attack, true);
            } else if (this.heldItem.isBerry()) {
                ItemHeld tempItem = target.getHeldItem();
                ItemHeld tempConsumedItem = target.getConsumedItem();
                target.setHeldItem(this.heldItem);
                target.getHeldItem().eatBerry(target);
                target.setHeldItem(tempItem);
                target.setConsumedItem(tempConsumedItem);
            } else if (type == EnumHeldItems.whiteHerb) {
                ItemWhiteHerb.healStats(target);
            } else if (type == EnumHeldItems.mentalHerb) {
                ((ItemMentalHerb)this.heldItem).healStatus(target);
            }
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.hitsAlly()) {
            return;
        }
        for (int i = 0; i < userChoice.targets.size(); ++i) {
            if (userChoice.tier >= 3) continue;
            ItemHeld item = pw.getHeldItem();
            EnumHeldItems type = item.getHeldItemType();
            if (type == EnumHeldItems.kingsRock || type == EnumHeldItems.razorFang) {
                Flinch flinch = new Flinch();
                flinch.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
                continue;
            }
            if (type == EnumHeldItems.lightBall) {
                Paralysis paralysis = new Paralysis();
                paralysis.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
                continue;
            }
            if (type == EnumHeldItems.flameOrb) {
                Burn burn = new Burn();
                burn.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
                continue;
            }
            if (type == EnumHeldItems.toxicOrb) {
                PoisonBadly poisonBadly = new PoisonBadly();
                poisonBadly.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
                continue;
            }
            if (type != EnumHeldItems.typeEnhancer || ((TypeEnhancingItems)item).enhanceType != EnumTypeEnhancingItems.poisonBarb) continue;
            Poison poison = new Poison();
            poison.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
        }
    }
}

