/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.ArrayList;
import java.util.List;

public class Conversion
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        List<EnumType> types = this.getPossibleTypes(user);
        EnumType newType = types.get(0);
        if (user.isSingleType(newType)) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        user.bc.sendToAll("pixelmon.effect.changetype", user.getNickname(), newType.getLocalizedName());
        user.setTempType(newType);
        return AttackResult.proceed;
    }

    private List<EnumType> getPossibleTypes(PixelmonWrapper user) {
        ArrayList<EnumType> typeList = new ArrayList<EnumType>();
        typeList.add(user.getMoveset().get((int)0).getAttackBase().attackType);
        return typeList;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        pw.getBattleAI().weightTypeChange(pw, userChoice, this.getPossibleTypes(pw), bestUserChoices, bestOpponentChoices);
    }
}

