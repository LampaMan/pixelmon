/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.modifiers;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.modifiers.ModifierBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.modifiers.ModifierType;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class ChanceModifier
extends ModifierBase {
    public int multiplier = 1;

    public ChanceModifier(int ... values) {
        this();
        this.value = values[0];
    }

    public ChanceModifier() {
        super(ModifierType.Chance);
    }

    public boolean RollChance() {
        boolean success = (float)RandomHelper.rand.nextInt(100) < this.value * (float)this.multiplier;
        this.multiplier = 1;
        return success;
    }
}

