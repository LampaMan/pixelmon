/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn.MultiTurnCharge;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Vanish;

public class ShadowForce
extends MultiTurnCharge {
    public ShadowForce() {
        super("pixelmon.effect.shadowforce", Vanish.class, StatusType.Vanish);
    }
}

