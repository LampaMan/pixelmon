/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class HeavySlam
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        float i = target.getWeight(AbilityBase.ignoreAbility(user, target)) / user.getWeight(false) * 100.0f;
        if (target.isDynamaxed()) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        user.attack.getAttackBase().basePower = i <= 20.0f ? 120 : (i <= 25.0f ? 100 : ((double)i <= 33.333333333333336 ? 80 : (i <= 50.0f ? 60 : 40)));
        return AttackResult.proceed;
    }
}

