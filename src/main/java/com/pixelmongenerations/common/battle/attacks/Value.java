/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks;

import com.pixelmongenerations.common.battle.attacks.ValueType;

public class Value {
    public int value = -1;
    public String stringValue = "";
    public ValueType type;

    public Value(int value, ValueType type) {
        this.value = value;
        this.type = type;
    }

    public Value(String stringValue, ValueType type) {
        this.stringValue = stringValue;
        this.type = type;
    }

    public String toString() {
        if (this.type == ValueType.String) {
            return "String - " + this.stringValue;
        }
        return "Number - " + this.value;
    }
}

