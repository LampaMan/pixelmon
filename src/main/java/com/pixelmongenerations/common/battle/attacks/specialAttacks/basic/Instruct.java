/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;

public class Instruct
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.isDynamaxed()) {
            return AttackResult.failed;
        }
        if (Instruct.isAllowed(target)) {
            target.useTempAttack(target.lastAttack, true);
            user.bc.sendToAll("pixelmon.effect.instruct", user.getPokemonName(), target.getPokemonName());
            return AttackResult.succeeded;
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }

    public static boolean isAllowed(PixelmonWrapper target) {
        if (!target.hasStatus(StatusType.Bide, StatusType.Focus, StatusType.BeakBlastContact, StatusType.ShellTrap) && target.lastAttack != null) {
            return !target.lastAttack.isAttack("Instruct", "Bide", "Focus Punch", "Beak Blast", "Shell Trap", "Sketch", "Transform", "Mimic", "King's Shield", "Struggle", "Blast Burn", "Frenzy Plant", "Giga Impact", "Hydro Cannon", "Hyper Beam", "Prismatic Laser", "Roar of Time", "Rock Wrecker", "Shadow Half", "Bounce", "Dig", "Dive", "Fly", "Freeze Shock", "Geomancy", "Ice Burn", "Phantom Force", "Razor Wind", "Shadow Wind", "Shadow Force", "Skull Bash", "Sky Drop", "Solar Beam", "Solar Blade", "Assist", "Copycat", "Me First", "Metronome", "Mirror Move", "Nature Power", "Sleep Talk");
        }
        return false;
    }
}

