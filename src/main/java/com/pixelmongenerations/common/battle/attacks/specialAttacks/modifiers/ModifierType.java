/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.modifiers;

public enum ModifierType {
    Chance,
    User,
    Repeat,
    Awake;


    public static ModifierType getModifierType(String string) {
        for (ModifierType t : ModifierType.values()) {
            if (!t.toString().equalsIgnoreCase(string)) continue;
            return t;
        }
        return null;
    }
}

