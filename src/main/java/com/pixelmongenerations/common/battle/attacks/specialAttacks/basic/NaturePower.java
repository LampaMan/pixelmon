/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.ZAttackBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.TerrainExamine;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class NaturePower
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        TerrainExamine.TerrainType terrainType = TerrainExamine.getTerrain(user);
        String attackName = "Tri Attack";
        switch (terrainType) {
            case Cave: {
                attackName = "Power Gem";
                break;
            }
            case Sand: {
                attackName = "Earth Power";
                break;
            }
            case Swamp: {
                attackName = "Mud Bomb";
                break;
            }
            case Water: {
                attackName = "Hydro Pump";
                break;
            }
            case Snow: {
                attackName = "Frost Breath";
                break;
            }
            case Ice: {
                attackName = "Ice Beam";
                break;
            }
            case Volcano: {
                attackName = "Lava Plume";
                break;
            }
            case Burial: {
                attackName = "Shadow Ball";
                break;
            }
            case Sky: {
                attackName = "Air Slash";
                break;
            }
            case Space: {
                attackName = "Draco Meteor";
                break;
            }
            case Grass: {
                attackName = "Energy Ball";
                break;
            }
            case Misty: {
                attackName = "Moonblast";
                break;
            }
            case Electric: {
                attackName = "Thunderbolt";
                break;
            }
            case Psychic: {
                attackName = "Psychic";
                break;
            }
        }
        Attack calledAttack = new Attack(attackName);
        if (user.attack.getAttackBase() instanceof ZAttackBase) {
            calledAttack = ZAttackBase.getZMove(calledAttack, false);
        }
        user.bc.sendToAll("pixelmon.effect.naturepower", calledAttack.getAttackBase().getLocalizedName());
        user.useTempAttack(calledAttack, target);
        user.attack.moveResult.damage = calledAttack.moveResult.damage;
        user.attack.moveResult.fullDamage = calledAttack.moveResult.fullDamage;
        user.attack.moveResult.accuracy = calledAttack.moveResult.accuracy;
        return AttackResult.ignore;
    }
}

