/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class Drive
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        EnumType type = EnumType.Normal;
        if (user.getHeldItem() != null && user.getHeldItem().getHeldItemType() == EnumHeldItems.drive) {
            if (user.getHeldItem() == PixelmonItemsHeld.burnDrive) {
                type = EnumType.Fire;
            } else if (user.getHeldItem() == PixelmonItemsHeld.chillDrive) {
                type = EnumType.Ice;
            } else if (user.getHeldItem() == PixelmonItemsHeld.douseDrive) {
                type = EnumType.Water;
            } else if (user.getHeldItem() == PixelmonItemsHeld.shockDrive) {
                type = EnumType.Electric;
            }
        }
        user.attack.getAttackBase().attackType = type;
        return AttackResult.proceed;
    }
}

