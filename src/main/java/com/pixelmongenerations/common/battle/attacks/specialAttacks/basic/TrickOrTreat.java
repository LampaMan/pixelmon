/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.AddType;
import com.pixelmongenerations.core.enums.EnumType;

public class TrickOrTreat
extends AddType {
    public TrickOrTreat() {
        super(EnumType.Ghost);
    }
}

