/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.DamageReflect;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class Counter
extends DamageReflect {
    public Counter() {
        super(2.0f);
    }

    @Override
    public boolean isCorrectCategory(AttackCategory category) {
        return category == AttackCategory.Physical;
    }

    @Override
    public boolean isTypeAllowed(EnumType type) {
        return type != EnumType.Ghost;
    }
}

