/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.List;

public class FreezeDry
extends SpecialAttackBase {
    @Override
    public double modifyTypeEffectiveness(List<EnumType> effectiveTypes, EnumType moveType, double baseEffectiveness) {
        double effectiveness = baseEffectiveness;
        double waterEffectiveness = EnumType.getEffectiveness(moveType, EnumType.Water);
        if (waterEffectiveness <= 1.0 && effectiveTypes.contains((Object)EnumType.Water)) {
            effectiveness *= 2.0;
            if (waterEffectiveness <= 0.5) {
                effectiveness *= 2.0;
            }
        }
        return effectiveness;
    }
}

