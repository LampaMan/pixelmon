/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.zeffects;

import com.pixelmongenerations.common.battle.attacks.zeffects.ZEffect;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class CriticalHitRaise
implements ZEffect {
    @Override
    public void applyEffect(PixelmonWrapper pokemon) {
        pokemon.getBattleStats().increaseCritStage(pokemon.getBattleStats().getCritStage() + 2);
    }

    @Override
    public void getEffectText(PixelmonWrapper pixelmonWrapper) {
        pixelmonWrapper.bc.sendToAll("pixelmon.zeffect.critcal_hit", pixelmonWrapper.getNickname());
    }
}

