/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.ElectricTerrain;

public class MaxLightning
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (!(user.bc.globalStatusController.getTerrain() instanceof ElectricTerrain)) {
            ElectricTerrain terrain = new ElectricTerrain();
            terrain.applyEffect(user, target);
        }
        return AttackResult.proceed;
    }
}

