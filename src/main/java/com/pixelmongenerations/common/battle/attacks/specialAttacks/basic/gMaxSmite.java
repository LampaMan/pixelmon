/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Confusion;

public class gMaxSmite
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        for (PixelmonWrapper targets : target.bc.getTeamPokemon(target.getParticipant())) {
            targets.addStatus(new Confusion(), targets);
            targets.bc.sendToAll("pixelmon.effect.becameconfused", targets.getNickname());
        }
        return AttackResult.proceed;
    }
}

