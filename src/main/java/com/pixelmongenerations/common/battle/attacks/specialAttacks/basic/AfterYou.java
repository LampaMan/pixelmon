/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class AfterYou
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.turn < user.bc.turnList.size() - 2) {
            for (int i = user.bc.turn + 2; i < user.bc.turnList.size(); ++i) {
                if (user.bc.turnList.get(i) != target) continue;
                user.bc.sendToAll("pixelmon.effect.afteryou", user.getNickname(), target.getNickname());
                user.bc.turnList.add(user.bc.turn + 1, user.bc.turnList.remove(i));
                return AttackResult.succeeded;
            }
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }
}

