/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class BodyPress
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        user.attack.getAttackBase().basePower = (int)(80.0 + 25.0 * ((double)target.getBattleStats().getStatWithMod(StatsType.Defence) / (double)user.getBattleStats().getStatWithMod(StatsType.Defence)));
        if (user.attack.getAttackBase().basePower > 150) {
            user.attack.getAttackBase().basePower = 150;
        }
        return AttackResult.proceed;
    }
}

