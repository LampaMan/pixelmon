/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ParentalBond;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class Psywave
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        int numHits = user.getBattleAbility() instanceof ParentalBond ? 2 : 1;
        for (int i = 0; i < numHits; ++i) {
            float random = RandomHelper.getRandomNumberBetween(0.5f, 1.5f);
            if (user.bc.simulateMode) {
                random = 1.0f;
            }
            float damage = random * (float)user.getLevelNum();
            target.doBattleDamage(user, damage, DamageTypeEnum.ATTACKFIXED);
            if (i == 1) {
                user.bc.sendToAll("multiplehit.times", user.getNickname(), 2);
                continue;
            }
            if (user.isFainted() || target.isFainted()) break;
        }
        return AttackResult.hit;
    }
}

