/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.EnumType;

public class Toxic
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.hasType(EnumType.Poison)) {
            user.attack.getAttackBase().accuracy = -2;
        }
        if (target.hasStatus(StatusType.UnderGround) || target.hasStatus(StatusType.Flying) || target.hasStatus(StatusType.SkyDropping) || target.hasStatus(StatusType.Submerged)) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        return AttackResult.proceed;
    }
}

