/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.IVStore;
import com.pixelmongenerations.core.enums.EnumType;

public class HiddenPower
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        user.attack.getAttackBase().attackType = HiddenPower.getHiddenPowerType(user.getStats().IVs);
        return AttackResult.proceed;
    }

    public static EnumType getHiddenPowerType(IVStore ivs) {
        int f = ivs.SpDef % 2;
        int e = ivs.SpAtt % 2;
        int d = ivs.Speed % 2;
        int c = ivs.Defence % 2;
        int b = ivs.Attack % 2;
        int a = ivs.HP % 2;
        double fedbca = 32 * f + 16 * e + 8 * d + 4 * c + 2 * b + a;
        int type = (int)Math.floor(fedbca * 15.0 / 63.0);
        if (type == 0) {
            return EnumType.Fighting;
        }
        if (type == 1) {
            return EnumType.Flying;
        }
        if (type == 2) {
            return EnumType.Poison;
        }
        if (type == 3) {
            return EnumType.Ground;
        }
        if (type == 4) {
            return EnumType.Rock;
        }
        if (type == 5) {
            return EnumType.Bug;
        }
        if (type == 6) {
            return EnumType.Ghost;
        }
        if (type == 7) {
            return EnumType.Steel;
        }
        if (type == 8) {
            return EnumType.Fire;
        }
        if (type == 9) {
            return EnumType.Water;
        }
        if (type == 10) {
            return EnumType.Grass;
        }
        if (type == 11) {
            return EnumType.Electric;
        }
        if (type == 12) {
            return EnumType.Psychic;
        }
        if (type == 13) {
            return EnumType.Ice;
        }
        if (type == 14) {
            return EnumType.Dragon;
        }
        return EnumType.Dark;
    }

    public static IVStore getOptimalIVs(EnumType type) {
        IVStore ivs = new IVStore();
        ivs.HP = 31;
        ivs.Attack = 31;
        ivs.Defence = 31;
        ivs.SpAtt = 31;
        ivs.SpDef = 31;
        ivs.Speed = 31;
        switch (type) {
            case Bug: {
                ivs.Attack = 30;
                ivs.Defence = 30;
                ivs.SpDef = 30;
                break;
            }
            case Dark: {
                break;
            }
            case Dragon: {
                ivs.Attack = 30;
                break;
            }
            case Electric: {
                ivs.SpAtt = 30;
                break;
            }
            case Fighting: {
                ivs.Defence = 30;
                ivs.SpAtt = 30;
                ivs.SpDef = 30;
                ivs.Speed = 30;
                break;
            }
            case Fire: {
                ivs.Attack = 30;
                ivs.SpAtt = 30;
                ivs.Speed = 30;
                break;
            }
            case Flying: {
                ivs.HP = 30;
                ivs.Attack = 30;
                ivs.Defence = 30;
                ivs.SpAtt = 30;
                ivs.SpDef = 30;
                break;
            }
            case Ghost: {
                ivs.Attack = 30;
                ivs.SpDef = 30;
                break;
            }
            case Grass: {
                ivs.Attack = 30;
                ivs.SpAtt = 30;
                break;
            }
            case Ground: {
                ivs.SpAtt = 30;
                ivs.SpDef = 30;
                break;
            }
            case Ice: {
                ivs.Attack = 30;
                ivs.Defence = 30;
                break;
            }
            case Poison: {
                ivs.Defence = 30;
                ivs.SpAtt = 30;
                ivs.SpDef = 30;
                break;
            }
            case Psychic: {
                ivs.Attack = 30;
                ivs.Speed = 30;
                break;
            }
            case Rock: {
                ivs.Defence = 30;
                ivs.SpDef = 30;
                ivs.Speed = 30;
                break;
            }
            case Steel: {
                ivs.SpDef = 30;
                break;
            }
            case Water: {
                ivs.Attack = 30;
                ivs.Defence = 30;
                ivs.SpAtt = 30;
                break;
            }
        }
        return ivs;
    }
}

