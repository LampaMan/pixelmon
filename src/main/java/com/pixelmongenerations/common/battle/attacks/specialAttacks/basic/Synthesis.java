/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.Value;
import com.pixelmongenerations.common.battle.attacks.ValueType;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.Recover;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Sandstorm;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.StrongWinds;
import com.pixelmongenerations.common.battle.status.Sunny;
import com.pixelmongenerations.common.battle.status.Weather;
import java.util.ArrayList;

public class Synthesis
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.hasFullHealth()) {
            user.bc.sendToAll("pixelmon.effect.healfailed", user.getNickname());
            return AttackResult.failed;
        }
        if (user.hasStatus(StatusType.HealBlock)) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        double heal = (float)user.getMaxHealth() * this.getHealMultiplier(user);
        user.bc.sendToAll("pixelmon.effect.washealed", user.getNickname());
        user.healEntityBy((int)heal);
        return AttackResult.proceed;
    }

    private float getHealMultiplier(PixelmonWrapper user) {
        Weather weather = user.bc.globalStatusController.getWeather();
        if (user.attack.isAttack("Shore Up")) {
            if (weather instanceof Sandstorm) {
                return 0.6666667f;
            }
            return 0.5f;
        }
        if (weather instanceof Sunny) {
            return 0.6666667f;
        }
        if (weather != null && !(weather instanceof StrongWinds)) {
            return 0.25f;
        }
        return 0.5f;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        Recover heal = new Recover(new Value((int)(this.getHealMultiplier(pw) * 100.0f), ValueType.WholeNumber));
        heal.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
    }
}

