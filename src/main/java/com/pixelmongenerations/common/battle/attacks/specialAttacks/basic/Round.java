/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import java.util.ArrayList;

public class Round
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        ArrayList<PixelmonWrapper> allies = user.bc.getTeamPokemon(user.getParticipant());
        for (int i = 0; i < user.bc.turnList.size(); ++i) {
            PixelmonWrapper pw = user.bc.turnList.get(i);
            if (pw == user || pw.attack == null) continue;
            if (!pw.attack.isAttack("Round") || !allies.contains(pw)) continue;
            if (i > user.bc.turn) {
                if (user.bc.simulateMode) break;
                user.bc.turnList.remove(i);
                user.bc.turnList.add(user.bc.turn + 1, pw);
                break;
            }
            if (!pw.canAttack) continue;
            user.attack.getAttackBase().basePower = 120;
        }
        return AttackResult.proceed;
    }
}

