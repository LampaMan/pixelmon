/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class OHKO
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        int chance;
        block8: {
            int userLevel;
            int targetLevel;
            block11: {
                block10: {
                    block9: {
                        block7: {
                            targetLevel = target.getLevelNum();
                            if (targetLevel <= (userLevel = user.getLevelNum())) break block7;
                            chance = 0;
                            break block8;
                        }
                        chance = Math.min(userLevel - targetLevel + 30, 100);
                        if (!user.attack.isAttack("Fissure")) break block9;
                        if (target.hasStatus(StatusType.UnderGround)) break block10;
                    }
                    if (user.attack.moveAccuracy != -2) break block11;
                }
                user.attack.moveResult.accuracy = -2;
                return this.doOHKO(user, target);
            }
            if (user.attack.isAttack("Sheer Cold")) {
                chance = target.hasType(EnumType.Ice) ? 0 : Math.min(userLevel - targetLevel + 20, 100);
            }
        }
        user.attack.moveResult.accuracy = chance;
        if (chance == 0) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        if (user.isDynamaxed()) {
            chance = 0;
        }
        if (user.bc.simulateMode || RandomHelper.getRandomChance(chance)) {
            return this.doOHKO(user, target);
        }
        user.bc.sendToAll("pixelmon.battletext.missedattack", target.getNickname());
        return AttackResult.failed;
    }

    private AttackResult doOHKO(PixelmonWrapper user, PixelmonWrapper target) {
        user.bc.sendToAll("pixelmon.effect.OHKO", new Object[0]);
        int damage = target.getHealth();
        user.attack.moveResult.damage = (int)target.doBattleDamage(user, damage, DamageTypeEnum.ATTACKFIXED);
        user.attack.moveResult.fullDamage = target.getMaxHealth();
        return AttackResult.hit;
    }
}

