/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;

public class Conversion2
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.lastAttack == null) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        ArrayList<EnumType> ableTypes = this.getPossibleTypes(user, target);
        if (!ableTypes.isEmpty()) {
            EnumType chosenType = RandomHelper.getRandomElementFromList(ableTypes);
            user.setTempType(chosenType);
            user.bc.sendToAll("pixelmon.effect.changetype", user.getNickname(), chosenType.getLocalizedName());
            return AttackResult.succeeded;
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }

    private ArrayList<EnumType> getPossibleTypes(PixelmonWrapper user, PixelmonWrapper target) {
        ArrayList<EnumType> ableTypes = new ArrayList<EnumType>();
        for (EnumType type : EnumType.values()) {
            if (EnumType.getEffectiveness(target.lastAttack.getAttackBase().attackType, type) >= 1.0f) continue;
            ableTypes.add(type);
        }
        if (user.type.size() == 1) {
            ableTypes.remove((Object)user.type.get(0));
        }
        return ableTypes;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        for (PixelmonWrapper target : userChoice.targets) {
            pw.getBattleAI().weightTypeChange(pw, userChoice, this.getPossibleTypes(pw, target), bestUserChoices, bestOpponentChoices);
        }
    }
}

