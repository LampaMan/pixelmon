/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.Swap;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class GuardSwap
extends Swap {
    public GuardSwap() {
        super("pixelmon.effect.guardswap", StatsType.Defence, StatsType.SpecialDefence);
    }
}

