/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.gMaxSteelsurge;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Leech;
import com.pixelmongenerations.common.battle.status.LightScreen;
import com.pixelmongenerations.common.battle.status.Mist;
import com.pixelmongenerations.common.battle.status.PartialTrap;
import com.pixelmongenerations.common.battle.status.Reflect;
import com.pixelmongenerations.common.battle.status.SafeGuard;
import com.pixelmongenerations.common.battle.status.Spikes;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.StealthRock;
import com.pixelmongenerations.common.battle.status.StickyWeb;
import com.pixelmongenerations.common.battle.status.ToxicSpikes;

public class CourtChange
extends SpecialAttackBase {
    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.isFainted()) {
            return;
        }
        PartialTrap partialTrap = (PartialTrap)user.getStatus(StatusType.PartialTrap);
        if (partialTrap != null) {
            target.addTeamStatus(new PartialTrap(), user);
            user.bc.sendToAll("pixelmon.effect.courtchange", user.getNickname());
            user.removeStatus(partialTrap);
        }
        if (user.removeStatus(StatusType.Leech)) {
            target.addTeamStatus(new Leech(), user);
            user.bc.sendToAll("pixelmon.effect.courtchange", user.getNickname());
        }
        if (user.removeTeamStatus(StatusType.Spikes)) {
            target.addTeamStatus(new Spikes(), user);
            user.bc.sendToAll("pixelmon.effect.courtchange", user.getNickname());
        }
        if (user.removeTeamStatus(StatusType.StealthRock)) {
            target.addTeamStatus(new StealthRock(), user);
            user.bc.sendToAll("pixelmon.effect.courtchange", user.getNickname());
        }
        if (user.removeTeamStatus(StatusType.Steelsurge)) {
            target.addTeamStatus(new gMaxSteelsurge(), user);
            user.bc.sendToAll("pixelmon.effect.courtchange", user.getNickname());
        }
        if (user.removeTeamStatus(StatusType.ToxicSpikes)) {
            target.addTeamStatus(new ToxicSpikes(), user);
            user.bc.sendToAll("pixelmon.effect.courtchange", user.getNickname());
        }
        if (user.removeTeamStatus(StatusType.StickyWeb)) {
            target.addTeamStatus(new StickyWeb(), user);
            user.bc.sendToAll("pixelmon.effect.courtchange", user.getNickname());
        }
        if (target.removeTeamStatus(StatusType.LightScreen)) {
            user.addTeamStatus(new LightScreen(), target);
            user.bc.sendToAll("pixelmon.effect.courtchange", user.getNickname());
        }
        if (target.removeTeamStatus(StatusType.Reflect)) {
            user.addTeamStatus(new Reflect(), target);
            user.bc.sendToAll("pixelmon.effect.courtchange", user.getNickname());
        }
        if (target.removeTeamStatus(StatusType.Mist)) {
            user.addTeamStatus(new Mist(), target);
            user.bc.sendToAll("pixelmon.effect.courtchange", user.getNickname());
        }
        if (target.removeTeamStatus(StatusType.SafeGuard)) {
            user.addTeamStatus(new SafeGuard(), target);
            user.bc.sendToAll("pixelmon.effect.courtchange", user.getNickname());
        }
    }
}

