/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Soundproof;
import java.util.ArrayList;
import java.util.stream.Stream;

public class HealBell
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.targetIndex == 0 || user.bc.simulateMode) {
            if (user.attack.isAttack("Heal Bell")) {
                user.bc.sendToAll("pixelmon.effect.healbellstart", new Object[0]);
            } else if (user.attack.isAttack("Aromatherapy")) {
                user.bc.sendToAll("pixelmon.effect.aromatherapy", new Object[0]);
            }
            boolean healedSomething = false;
            for (PixelmonWrapper pw : user.getParticipant().allPokemon) {
                if (pw.getBattleAbility() instanceof Soundproof || pw.removePrimaryStatus() == null) continue;
                healedSomething = true;
            }
            if (!healedSomething) {
                return AttackResult.failed;
            }
        }
        return AttackResult.succeeded;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        int numStatus = (int)Stream.of(pw.getParticipant().allPokemon).filter(PixelmonWrapper::hasPrimaryStatus).count();
        userChoice.raiseWeight(numStatus * 30);
    }
}

