/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.AddType;
import com.pixelmongenerations.core.enums.EnumType;

public class ForestCurse
extends AddType {
    public ForestCurse() {
        super(EnumType.Grass);
    }
}

