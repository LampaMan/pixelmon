/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.ElectricTerrain;
import com.pixelmongenerations.common.battle.status.GrassyTerrain;
import com.pixelmongenerations.common.battle.status.MistyTerrain;
import com.pixelmongenerations.common.battle.status.PsychicTerrain;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MegaLauncher;
import com.pixelmongenerations.core.enums.EnumType;

public class TerrainPulse
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.bc.globalStatusController.getTerrain() == null) {
            user.attack.overrideType(EnumType.Normal);
            user.attack.movePower = 50;
        } else if (target.bc.globalStatusController.getTerrain() instanceof ElectricTerrain) {
            user.attack.movePower = user.isGrounded() ? 100 : 50;
            if (user.getBattleAbility() instanceof MegaLauncher) {
                user.attack.getAttackBase().basePower = (int)((double)user.attack.getAttackBase().basePower * 1.5);
            }
            user.attack.overrideType(EnumType.Electric);
        } else if (target.bc.globalStatusController.getTerrain() instanceof GrassyTerrain) {
            user.attack.movePower = user.isGrounded() ? 100 : 50;
            if (user.getBattleAbility() instanceof MegaLauncher) {
                user.attack.getAttackBase().basePower = (int)((double)user.attack.getAttackBase().basePower * 1.5);
            }
            user.attack.overrideType(EnumType.Grass);
        } else if (target.bc.globalStatusController.getTerrain() instanceof MistyTerrain) {
            user.attack.movePower = user.isGrounded() ? 100 : 50;
            if (user.getBattleAbility() instanceof MegaLauncher) {
                user.attack.getAttackBase().basePower = (int)((double)user.attack.getAttackBase().basePower * 1.5);
            }
            user.attack.overrideType(EnumType.Fairy);
        } else if (target.bc.globalStatusController.getTerrain() instanceof PsychicTerrain) {
            user.attack.movePower = user.isGrounded() ? 100 : 50;
            if (user.getBattleAbility() instanceof MegaLauncher) {
                user.attack.getAttackBase().basePower = (int)((double)user.attack.getAttackBase().basePower * 1.5);
            }
            user.attack.overrideType(EnumType.Psychic);
        }
        return AttackResult.proceed;
    }
}

