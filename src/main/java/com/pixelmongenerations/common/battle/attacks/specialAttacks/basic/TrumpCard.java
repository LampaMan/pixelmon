/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Pressure;

public class TrumpCard
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        int afterPP = user.attack.pp - 1;
        if (target.getBattleAbility() instanceof Pressure) {
            --afterPP;
        }
        user.attack.getAttackBase().basePower = afterPP >= 4 ? 40 : (afterPP == 3 ? 50 : (afterPP == 2 ? 60 : (afterPP == 1 ? 80 : 200)));
        return AttackResult.proceed;
    }
}

