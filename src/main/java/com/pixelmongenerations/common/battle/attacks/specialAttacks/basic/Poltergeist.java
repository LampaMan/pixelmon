/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Klutz;

public class Poltergeist
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.hasHeldItem() && !target.bc.globalStatusController.hasStatus(StatusType.MagicRoom) && !(target.getBattleAbility() instanceof Klutz)) {
            user.bc.sendToAll("pixelmon.effect.poltergeist", target.getNickname(), target.getHeldItem().getLocalizedName());
            return AttackResult.proceed;
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", user.getNickname());
        return AttackResult.failed;
    }
}

