/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.attackModifiers;

import com.pixelmongenerations.common.battle.attacks.Value;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.attackModifiers.AttackModifierBase;

public class Priority
extends AttackModifierBase {
    public int priority;

    public Priority(Value ... values) {
        this.priority = values[0].value;
    }
}

