/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks;

public enum ValueType {
    None,
    WholeNumber,
    Percent,
    Range,
    String;

}

