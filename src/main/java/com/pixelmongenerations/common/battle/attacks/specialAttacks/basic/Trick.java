/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.heldItems.NoItem;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.ArrayList;

public class Trick
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        ItemHeld userItem = user.getHeldItem();
        ItemHeld targetItem = target.getHeldItem();
        if (!user.hasHeldItem() && !target.hasHeldItem() || !user.isItemRemovable(user) || !target.isItemRemovable(user)) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        if (userItem.getHeldItemType() == EnumHeldItems.choiceItem && targetItem.getHeldItemType() == EnumHeldItems.choiceItem) {
            user.choiceSwapped = true;
            target.choiceSwapped = true;
        }
        userItem.applySwitchOutEffect(user);
        targetItem.applySwitchOutEffect(target);
        user.bc.sendToAll("pixelmon.effect.trick", user.getNickname(), target.getNickname());
        if (targetItem != NoItem.noItem) {
            user.bc.sendToAll("pixelmon.effect.trickitem", user.getNickname(), targetItem.getLocalizedName());
        }
        user.setNewHeldItem(targetItem);
        if (userItem != NoItem.noItem) {
            target.bc.sendToAll("pixelmon.effect.trickitem", target.getNickname(), userItem.getLocalizedName());
        }
        target.setNewHeldItem(userItem);
        user.bc.enableReturnHeldItems(user, target);
        return AttackResult.succeeded;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.hitsAlly()) {
            return;
        }
        ItemHeld userItem = pw.getHeldItem();
        boolean userNegative = userItem == null || userItem.hasNegativeEffect();
        for (PixelmonWrapper target : userChoice.targets) {
            ItemHeld targetItem = target.getHeldItem();
            boolean targetNegative = targetItem == null || targetItem.hasNegativeEffect();
            boolean bl = targetNegative;
            if (!userNegative || targetNegative) continue;
            userChoice.raiseWeight(40.0f);
        }
    }
}

