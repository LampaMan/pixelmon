/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.zeffects;

import com.pixelmongenerations.common.battle.attacks.zeffects.ZEffect;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class ResetStats
implements ZEffect {
    @Override
    public void applyEffect(PixelmonWrapper pokemon) {
        if (pokemon.getBattleStats().getStage(StatsType.Attack) < 0) {
            pokemon.getBattleStats().changeStat(StatsType.Attack, 0);
        }
        if (pokemon.getBattleStats().getStage(StatsType.Defence) < 0) {
            pokemon.getBattleStats().changeStat(StatsType.Defence, 0);
        }
        if (pokemon.getBattleStats().getStage(StatsType.SpecialAttack) < 0) {
            pokemon.getBattleStats().changeStat(StatsType.SpecialAttack, 0);
        }
        if (pokemon.getBattleStats().getStage(StatsType.SpecialDefence) < 0) {
            pokemon.getBattleStats().changeStat(StatsType.SpecialDefence, 0);
        }
        if (pokemon.getBattleStats().getStage(StatsType.Speed) < 0) {
            pokemon.getBattleStats().changeStat(StatsType.Speed, 0);
        }
    }

    @Override
    public void getEffectText(PixelmonWrapper pixelmonWrapper) {
        pixelmonWrapper.bc.sendToAll("pixelmon.zeffect.stat_reset", pixelmonWrapper.getNickname());
    }
}

