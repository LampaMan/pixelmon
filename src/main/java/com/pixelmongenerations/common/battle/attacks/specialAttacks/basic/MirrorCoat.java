/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.DamageReflect;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class MirrorCoat
extends DamageReflect {
    public MirrorCoat() {
        super(2.0f);
    }

    @Override
    public boolean isCorrectCategory(AttackCategory category) {
        return category == AttackCategory.Special;
    }

    @Override
    public boolean isTypeAllowed(EnumType type) {
        return type != EnumType.Dark;
    }
}

