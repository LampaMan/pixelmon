/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.TrainerParticipant;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicBounce;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.battles.EnforcedSwitch;
import java.util.ArrayList;

public class SwitchOut
extends SpecialAttackBase {
    @Override
    protected void applyEffect(PixelmonWrapper userWrapper, PixelmonWrapper targetWrapper) {
        BattleParticipant userParticipant;
        if (userWrapper.bc.simulateMode || userWrapper.inParentalBond && targetWrapper.isAlive()) {
            return;
        }
        ArrayList<PixelmonWrapper> activePokemon = userWrapper.bc.getActivePokemon();
        if (!activePokemon.contains(userWrapper) || !activePokemon.contains(targetWrapper)) {
            return;
        }
        if (userWrapper.getAbility() instanceof MagicBounce && userWrapper.isDynamaxed()) {
            if (targetWrapper.attack.isAttack("Parting Shot")) {
                userWrapper.bc.removeFromTurnList(userWrapper);
            }
        }
        if (!(userParticipant = userWrapper.getParticipant()).hasMorePokemonReserve() || targetWrapper.isFainted() && targetWrapper.getParticipant().countAblePokemon() == 0) {
            return;
        }
        userWrapper.nextSwitchIsMove = true;
        if (targetWrapper.attack != null) {
            if (targetWrapper.attack.isAttack("Pursuit") && !targetWrapper.hasMoved()) {
                targetWrapper.attack.getAttackBase().basePower = 80;
                userWrapper.bc.removeFromTurnList(targetWrapper);
                targetWrapper.takeTurn();
                targetWrapper.attack.getAttackBase().basePower = 40;
                if (userWrapper.isFainted()) {
                    return;
                }
            }
        }
        if (userWrapper.isFainted()) {
            return;
        }
        if (userParticipant instanceof TrainerParticipant) {
            userWrapper.bc.switchPokemon(userWrapper.getPokemonID(), userWrapper.getBattleAI().getNextSwitch(userWrapper), true);
        } else if (userParticipant instanceof PlayerParticipant) {
            userWrapper.wait = true;
            Pixelmon.NETWORK.sendTo(new EnforcedSwitch(userWrapper.bc.getPositionOfPokemon(userWrapper, userParticipant)), userWrapper.getPlayerOwner());
        } else {
            userWrapper.nextSwitchIsMove = false;
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (MoveChoice.canOutspeedAnd2HKO(bestOpponentChoices, pw, userChoice.createList())) {
            userChoice.raiseWeight(30.0f);
        }
    }
}

