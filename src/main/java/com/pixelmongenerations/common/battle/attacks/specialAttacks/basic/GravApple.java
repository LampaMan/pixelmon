/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;

public class GravApple
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.bc.globalStatusController.hasStatus(StatusType.Gravity)) {
            if (user.attack.isAttack("Grav Apple")) {
                user.attack.getAttackBase().basePower = 120;
                user.bc.sendToAll("pixelmon.effect.gravapple", user.getNickname());
            }
        }
        return AttackResult.proceed;
    }
}

