/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Stockpile;
import java.util.ArrayList;

public class Swallow
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        Stockpile stockpile;
        block5: {
            block4: {
                stockpile = (Stockpile)user.getStatus(StatusType.Stockpile);
                if (stockpile == null) break block4;
                if (!user.hasStatus(StatusType.HealBlock)) break block5;
            }
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        if (user.hasFullHealth()) {
            user.bc.sendToAll("pixelmon.effect.healfailed", user.getNickname());
            return AttackResult.failed;
        }
        user.healEntityBy(this.getHealAmount(user, stockpile));
        user.bc.sendToAll("pixelmon.effect.washealed", target.getNickname());
        stockpile.removeStockpile(user);
        return AttackResult.succeeded;
    }

    private int getHealAmount(PixelmonWrapper user, Stockpile stockpile) {
        return user.getPercentMaxHealth(100.0f / (float)(1 << 3 - stockpile.numStockpiles));
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        Stockpile stockpile = (Stockpile)pw.getStatus(StatusType.Stockpile);
        if (stockpile != null) {
            float heal = Math.min(this.getHealAmount(pw, stockpile), pw.getHealthDeficit());
            userChoice.raiseWeight(pw.getHealthPercent(heal));
        }
    }
}

