/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ParentalBond;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;

public abstract class DamageReflect
extends SpecialAttackBase {
    public transient float multiplier;
    private transient boolean inProgress;

    public DamageReflect(float multiplier) {
        this.multiplier = multiplier;
    }

    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (this.inProgress) {
            this.inProgress = false;
            return AttackResult.proceed;
        }
        if (user.bc.simulateMode) {
            return AttackResult.hit;
        }
        ArrayList<PixelmonWrapper> opponents = user.getOpponentPokemon();
        if (user.lastDirectDamage >= 0 && this.isCorrectCategory(user.lastDirectCategory) && !opponents.isEmpty() && this.isTypeAllowed(target.attack.getType())) {
            if (opponents.size() <= user.lastDirectPosition) {
                user.lastDirectPosition = 0;
            }
            target = opponents.get(user.lastDirectPosition);
            if (user.lastDirectDamage == 0) {
                user.attack.getAttackBase().basePower = 1;
                this.inProgress = true;
                user.useAttackOnly();
                return AttackResult.proceed;
            }
            float damage = (float)user.lastDirectDamage * this.multiplier;
            target.doBattleDamage(user, damage, DamageTypeEnum.ATTACKFIXED);
            if (target.isAlive() && user.isAlive() && user.getBattleAbility() instanceof ParentalBond) {
                target.doBattleDamage(user, damage, DamageTypeEnum.ATTACKFIXED);
                user.bc.sendToAll("multiplehit.times", user.getNickname(), 2);
            }
            return AttackResult.hit;
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }

    public abstract boolean isCorrectCategory(AttackCategory var1);

    public boolean isTypeAllowed(EnumType type) {
        return true;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        MoveChoice reflectChoice = null;
        for (MoveChoice opponentChoice : bestOpponentChoices) {
            if (!opponentChoice.isOffensiveMove() || !this.isCorrectCategory(opponentChoice.attack.getAttackCategory()) || !opponentChoice.targets.contains(pw) || !MoveChoice.canOutspeed(opponentChoice.createList(), pw, userChoice.createList())) continue;
            reflectChoice = opponentChoice;
            break;
        }
        if (reflectChoice != null) {
            for (PixelmonWrapper target : userChoice.targets) {
                float reflectDamage = (float)(Math.floor((float)reflectChoice.result.damage / (float)reflectChoice.targets.size()) * (double)this.multiplier);
                userChoice.raiseWeightLimited(target.getHealthPercent(reflectDamage));
            }
        }
        if (userChoice.isMiddleTier() && RandomHelper.getRandomChance()) {
            userChoice.setWeight(1.0f);
        }
    }
}

