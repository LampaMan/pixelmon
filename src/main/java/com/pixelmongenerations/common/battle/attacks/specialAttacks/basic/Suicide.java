/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.MistyTerrain;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Damp;
import java.util.ArrayList;

public class Suicide
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        boolean hasDamp = false;
        for (PixelmonWrapper pw : user.bc.getActiveUnfaintedPokemon()) {
            if (!(pw.getBattleAbility(user) instanceof Damp)) continue;
            hasDamp = true;
            break;
        }
        if (hasDamp) {
            if (target.getBattleAbility() instanceof Damp) {
                user.bc.sendToAll("pixelmon.abilities.damp", target.getNickname());
            }
            return AttackResult.failed;
        }
        if (target.isFainted()) {
            return AttackResult.notarget;
        }
        if (user.isAlive()) {
            if (user.attack.isAttack("Misty Explosion") && user.bc.globalStatusController.getTerrain() instanceof MistyTerrain) {
                user.attack.getAttackBase().basePower = (int)((double)user.attack.getAttackBase().basePower * 1.5);
            }
            user.bc.sendToAll("pixelmon.effect.suicide", user.getNickname());
            user.doBattleDamage(user, user.getHealth(), DamageTypeEnum.SELF);
        }
        return AttackResult.proceed;
    }

    @Override
    public void applyMissEffect(PixelmonWrapper user, PixelmonWrapper target) {
        try {
            this.applyEffectDuring(user, target);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        userChoice.weight = 0.0f;
    }
}

