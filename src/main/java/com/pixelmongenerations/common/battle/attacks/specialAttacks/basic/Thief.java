/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.StickyHold;
import com.pixelmongenerations.common.item.ItemHeld;
import java.util.ArrayList;

public class Thief
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (!user.hasHeldItem() && target.hasHeldItem() && target.isItemRemovable(user)) {
            ItemHeld targetItem = target.getHeldItem();
            user.bc.sendToAll("pixelmon.effect.thief", user.getNickname(), target.getNickname(), targetItem.getLocalizedName());
            user.setNewHeldItem(targetItem);
            target.setNewHeldItem(null);
            user.bc.enableReturnHeldItems(user, target);
        }
        return AttackResult.proceed;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.hitsAlly()) {
            return;
        }
        for (PixelmonWrapper target : userChoice.targets) {
            if (pw.hasHeldItem() || !target.hasHeldItem() || target.getHeldItem().hasNegativeEffect() || target.getBattleAbility() instanceof StickyHold) continue;
            userChoice.raiseWeight(25.0f);
        }
    }
}

