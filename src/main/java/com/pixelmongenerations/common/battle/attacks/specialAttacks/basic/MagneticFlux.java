/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Minus;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Plus;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import java.util.ArrayList;

public class MagneticFlux
extends SpecialAttackBase {
    private static final StatsType[] raiseStats = new StatsType[]{StatsType.Defence, StatsType.SpecialDefence};

    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.targetIndex > 0) {
            return AttackResult.succeeded;
        }
        boolean succeeded = false;
        for (PixelmonWrapper pw : user.getTeamPokemon()) {
            AbilityBase ability = pw.getBattleAbility();
            if (!(ability instanceof Plus) && !(ability instanceof Minus)) continue;
            pw.getBattleStats().modifyStat(1, raiseStats);
            succeeded = true;
        }
        if (succeeded) {
            return AttackResult.succeeded;
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        for (PixelmonWrapper pokemon : pw.getTeamPokemon()) {
            AbilityBase ability = pokemon.getBattleAbility();
            if (!(ability instanceof Plus) && !(ability instanceof Minus)) continue;
            userChoice.raiseWeight(15.0f);
        }
    }
}

