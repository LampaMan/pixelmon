/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.animations;

import com.pixelmongenerations.common.battle.attacks.animations.IAttackAnimation;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.math.MathHelper;

public class AttackAnimationLeapForward
implements IAttackAnimation {
    @Override
    public void doMove(EntityLiving user, EntityLiving target) {
        double d = target.posX - user.posX;
        double d1 = target.posZ - user.posZ;
        float f = MathHelper.sqrt(d * d + d1 * d1);
        user.motionX += d / (double)f * 0.5 * (double)0.8f + user.motionX * (double)0.2f;
        user.motionZ += d1 / (double)f * 0.5 * (double)0.8f + user.motionZ * (double)0.2f;
        user.motionY = 0.4;
    }
}

