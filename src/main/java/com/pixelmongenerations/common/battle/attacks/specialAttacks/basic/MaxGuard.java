/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Protect;
import com.pixelmongenerations.common.battle.status.ProtectVariation;
import com.pixelmongenerations.common.battle.status.StatusType;

public class MaxGuard
extends ProtectVariation {
    public MaxGuard() {
        super(StatusType.Protect);
    }

    @Override
    public boolean stopsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user) {
        return !pokemon.attack.isAttack("G-Max Rapid Flow", "G-Max One Blow");
    }

    @Override
    protected boolean addStatus(PixelmonWrapper var1) {
        return var1.addStatus(new Protect(), var1);
    }

    @Override
    protected void displayMessage(PixelmonWrapper var1) {
        var1.bc.sendToAll("pixelmon.effect.redaying", var1.getNickname());
    }
}

