/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.log.MoveResults;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;

public class BeatUp
extends SpecialAttackBase {
    private transient boolean inProgress = false;
    private transient int limit = 0;

    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (this.limit++ < 6 && this.inProgress) {
            return AttackResult.proceed;
        }
        user.inMultipleHit = true;
        this.inProgress = true;
        this.limit = 0;
        boolean hasSubstitute = false;
        boolean firstHit = true;
        for (PixelmonWrapper p : user.getParticipant().allPokemon) {
            if (!p.isAlive() || p.hasPrimaryStatus()) continue;
            if (!firstHit) {
                user.attack.getAttackBase().accuracy = -1;
            }
            hasSubstitute = target.hasStatus(StatusType.Substitute);
            this.doAttack(user, target, p);
            firstHit = false;
        }
        user.inMultipleHit = false;
        this.inProgress = false;
        user.attack.sendEffectiveChat(user, target);
        Attack.postProcessAttackAllHits(user, target, user.attack, user.attack.moveResult.damage, DamageTypeEnum.ATTACK, hasSubstitute);
        if (!hasSubstitute) {
            Attack.applyContactLate(user, target);
        }
        return AttackResult.hit;
    }

    private void doAttack(PixelmonWrapper user, PixelmonWrapper target, PixelmonWrapper currentAttacker) {
        MoveResults[] results;
        user.attack.getAttackBase().basePower = currentAttacker.getBaseStats().attack / 10 + 5;
        for (MoveResults result : results = user.useAttackOnly()) {
            user.attack.moveResult.damage += result.damage;
            user.attack.moveResult.fullDamage += result.fullDamage;
        }
    }
}

