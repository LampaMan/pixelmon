/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.MeFirstStatus;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import java.util.ArrayList;

public class MeFirst
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.simulateMode) {
            return AttackResult.ignore;
        }
        for (PixelmonWrapper pw : user.bc.turnList) {
            if (pw == user) break;
            if (pw != target) continue;
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        if (target.attack != null && target.attack.getAttackCategory() != AttackCategory.Status) {
            if (!target.attack.isAttack("Focus Punch")) {
                user.addStatus(new MeFirstStatus(), user);
                user.useTempAttack(target.attack);
                return AttackResult.ignore;
            }
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (MoveChoice.canOutspeed(bestOpponentChoices, pw, userChoice.createList())) {
            return;
        }
        ArrayList<MoveChoice> possibleChoices = MoveChoice.createChoicesFromChoices(pw, bestOpponentChoices, false);
        pw.bc.simulateMode = false;
        pw.addStatus(new MeFirstStatus(), pw);
        pw.bc.simulateMode = true;
        pw.getBattleAI().weightRandomMove(pw, userChoice, possibleChoices);
        pw.bc.simulateMode = false;
        pw.removeStatus(StatusType.MeFirst);
        pw.bc.simulateMode = true;
    }
}

