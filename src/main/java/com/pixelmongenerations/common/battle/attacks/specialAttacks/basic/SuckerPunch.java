/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;

public class SuckerPunch
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.simulateMode) {
            return AttackResult.proceed;
        }
        if (target.attack != null && target.attack.getAttackCategory() != AttackCategory.Status) {
            for (int i = user.bc.turn + 1; i < user.bc.turnList.size(); ++i) {
                if (user.bc.turnList.get(i) != target) continue;
                return AttackResult.proceed;
            }
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        int numOffensive = 0;
        int numStatus = 0;
        for (PixelmonWrapper target : userChoice.targets) {
            for (Attack attack : pw.getBattleAI().getMoveset(target)) {
                if (attack.getAttackCategory() == AttackCategory.Status) {
                    ++numStatus;
                    continue;
                }
                ++numOffensive;
            }
        }
        if (numStatus == 0) {
            return;
        }
        if (RandomHelper.getRandomChance((float)numStatus / ((float)numOffensive + (float)numStatus))) {
            userChoice.lowerTier(1);
        }
    }
}

