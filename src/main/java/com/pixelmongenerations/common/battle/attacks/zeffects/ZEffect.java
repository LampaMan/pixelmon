/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.zeffects;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public interface ZEffect {
    public void applyEffect(PixelmonWrapper var1);

    public void getEffectText(PixelmonWrapper var1);
}

