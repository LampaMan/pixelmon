/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.attackModifiers;

import com.pixelmongenerations.common.battle.attacks.Value;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.attackModifiers.AttackModifierBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import java.util.ArrayList;

public class CriticalHit
extends AttackModifierBase {
    public int stage;

    public CriticalHit(Value ... values) {
        this.stage = values[0].value;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.isOffensiveMove()) {
            userChoice.raiseWeight((float)this.stage / 10.0f);
        } else if (pw.getBattleStats().increaseCritStage(2)) {
            userChoice.raiseWeight(25.0f);
        }
    }
}

