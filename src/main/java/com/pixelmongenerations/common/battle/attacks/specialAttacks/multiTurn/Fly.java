/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn.MultiTurnCharge;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Flying;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public class Fly
extends MultiTurnCharge {
    public Fly() {
        super("pixelmon.effect.flyup", Flying.class, StatusType.Flying);
    }

    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.globalStatusController.hasStatus(StatusType.Gravity)) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        if (!this.doesPersist(user)) {
            this.setPersists(user, true);
            this.setTurnCount(user, 2);
        }
        this.decrementTurnCount(user);
        boolean skipCharge = false;
        if (this.getTurnCount(user) == 1) {
            if (user.attack.isAttack("Fly")) {
                user.bc.sendToAll("pixelmon.effect.flyup", user.getNickname());
            } else if (user.attack.isAttack("Bounce")) {
                user.bc.sendToAll("pixelmon.effect.bounce", user.getNickname());
            }
            if (!user.getUsableHeldItem().affectMultiturnMove(user)) {
                user.addStatus(StatusBase.getNewInstance(this.base), user);
                return AttackResult.charging;
            }
            skipCharge = true;
        }
        if (!user.bc.simulateMode && !skipCharge) {
            if (!user.hasStatus(this.type)) {
                this.setPersists(user, false);
                return AttackResult.failed;
            }
        }
        user.removeStatus(this.type);
        this.setPersists(user, false);
        return AttackResult.proceed;
    }
}

