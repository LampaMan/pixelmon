/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn.MultiTurnSpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.BeakBlastContact;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.ArrayList;

public class MultiTurnCharge
extends MultiTurnSpecialAttackBase {
    transient String message = "";
    transient Class<? extends StatusBase> base;
    transient StatusType type;

    public MultiTurnCharge() {
    }

    public MultiTurnCharge(String message) {
        this.message = message;
    }

    public MultiTurnCharge(String message, Class<? extends StatusBase> base, StatusType type) {
        this(message);
        this.base = base;
        this.type = type;
    }

    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (!this.doesPersist(user)) {
            this.setPersists(user, true);
            this.setTurnCount(user, 2);
        }
        this.decrementTurnCount(user);
        if (this.getTurnCount(user) == 1) {
            user.bc.sendToAll(this.message, user.getNickname());
            if (user.attack.isAttack("Meteor Beam")) {
                user.getBattleStats().modifyStat(1, StatsType.SpecialAttack, user, true);
            }
            if (user.attack.isAttack("Beak Blast")) {
                user.addStatus(new BeakBlastContact(), user);
            }
            if (user.attack.isAttack("Z-Geomancy")) {
                user.getBattleStats().modifyStat(1, StatsType.Attack, user, true);
                user.getBattleStats().modifyStat(1, StatsType.Defence, user, true);
                user.getBattleStats().modifyStat(1, StatsType.SpecialAttack, user, true);
                user.getBattleStats().modifyStat(1, StatsType.SpecialDefence, user, true);
                user.getBattleStats().modifyStat(1, StatsType.Speed, user, true);
            }
            if (!user.getUsableHeldItem().affectMultiturnMove(user)) {
                if (this.base != null) {
                    user.addStatus(StatusBase.getNewInstance(this.base), user);
                }
                return AttackResult.charging;
            }
        }
        if (this.type != null) {
            user.removeStatus(this.type);
        }
        if (user.lastAttack != null) {
            if (user.lastAttack.isAttack("Beak Blast")) {
                user.lastAttack = null;
            }
        }
        this.setPersists(user, false);
        return AttackResult.proceed;
    }

    @Override
    public boolean cantMiss(PixelmonWrapper user) {
        return this.getTurnCount(user) == -1;
    }

    @Override
    public void applyMissEffect(PixelmonWrapper user, PixelmonWrapper target) {
        this.removeEffect(user, target);
    }

    @Override
    public void removeEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (this.type != null) {
            user.removeStatus(this.type);
        }
        this.setPersists(user, false);
    }

    @Override
    public boolean isCharging(PixelmonWrapper user, PixelmonWrapper target) {
        return !this.doesPersist(user);
    }

    @Override
    public boolean shouldNotLosePP(PixelmonWrapper user) {
        return !this.doesPersist(user) && user.getUsableHeldItem().getHeldItemType() != EnumHeldItems.powerHerb;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (pw.getUsableHeldItem().getHeldItemType() == EnumHeldItems.powerHerb) {
            return;
        }
        if (this.base == null) {
            if (userChoice.tier >= 3) {
                userChoice.lowerTier(2);
            } else {
                userChoice.weight /= 2.0f;
            }
        } else {
            userChoice.weight *= 0.9f;
        }
        if (pw.hasStatus(StatusType.Confusion, StatusType.Infatuated, StatusType.Paralysis)) {
            userChoice.weight /= 2.0f;
        }
    }
}

