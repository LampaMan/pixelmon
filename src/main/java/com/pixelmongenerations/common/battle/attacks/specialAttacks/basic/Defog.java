/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.ElectricTerrain;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.GrassyTerrain;
import com.pixelmongenerations.common.battle.status.MistyTerrain;
import com.pixelmongenerations.common.battle.status.PsychicTerrain;
import com.pixelmongenerations.common.battle.status.StatusType;
import java.util.ArrayList;

public class Defog
extends SpecialAttackBase {
    @Override
    public void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        boolean clearedHazards = false;
        for (PixelmonWrapper pokemon : new PixelmonWrapper[]{user, user.getOpponentPokemon().get(0)}) {
            if (!pokemon.removeTeamStatus(StatusType.Spikes, StatusType.StealthRock, StatusType.Steelsurge, StatusType.ToxicSpikes, StatusType.StickyWeb)) continue;
            clearedHazards = true;
        }
        if (clearedHazards) {
            user.bc.sendToAll("pixelmon.effect.clearspikes", user.getNickname());
        }
        for (GlobalStatusBase globalStatusBase : target.bc.globalStatusController.getGlobalStatuses()) {
            if (globalStatusBase instanceof ElectricTerrain) {
                user.bc.globalStatusController.removeGlobalStatus(StatusType.ElectricTerrain);
                user.bc.sendToAll("pixelmon.status.electricterrainend", new Object[0]);
                continue;
            }
            if (globalStatusBase instanceof MistyTerrain) {
                user.bc.globalStatusController.removeGlobalStatus(StatusType.MistyTerrain);
                user.bc.sendToAll("pixelmon.status.mistyterrainend", new Object[0]);
                continue;
            }
            if (globalStatusBase instanceof GrassyTerrain) {
                user.bc.globalStatusController.removeGlobalStatus(StatusType.GrassyTerrain);
                user.bc.sendToAll("pixelmon.status.grassyterrainend", new Object[0]);
                continue;
            }
            if (!(globalStatusBase instanceof PsychicTerrain)) continue;
            user.bc.globalStatusController.removeGlobalStatus(StatusType.PsychicTerrain);
            user.bc.sendToAll("pixelmon.status.psychicterrainend", new Object[0]);
        }
        if (target.removeTeamStatus(StatusType.LightScreen)) {
            user.bc.sendToAll("pixelmon.status.lightscreenoff", new Object[0]);
        }
        if (target.removeTeamStatus(StatusType.Reflect)) {
            user.bc.sendToAll("pixelmon.status.reflectoff", new Object[0]);
        }
        if (target.removeTeamStatus(StatusType.Mist)) {
            user.bc.sendToAll("pixelmon.status.mistoff", new Object[0]);
        }
        if (target.removeTeamStatus(StatusType.SafeGuard)) {
            user.bc.sendToAll("pixelmon.status.safeguardoff", target.getNickname());
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        int total = 0;
        for (PixelmonWrapper target : userChoice.targets) {
            total += target.countStatuses(StatusType.LightScreen, StatusType.Reflect, StatusType.Mist, StatusType.SafeGuard, StatusType.ElectricTerrain, StatusType.MistyTerrain, StatusType.PsychicTerrain, StatusType.GrassyTerrain);
        }
        if (userChoice.hitsAlly()) {
            total = -total;
        }
        StatusType[] entryHazards = new StatusType[]{StatusType.Spikes, StatusType.StealthRock, StatusType.Steelsurge, StatusType.ToxicSpikes, StatusType.StickyWeb};
        total += pw.countStatuses(entryHazards);
        userChoice.raiseWeight(30 * (total -= pw.getOpponentPokemon().get(0).countStatuses(entryHazards)));
    }
}

