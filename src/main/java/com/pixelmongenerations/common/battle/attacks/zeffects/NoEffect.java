/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.zeffects;

import com.pixelmongenerations.common.battle.attacks.zeffects.ZEffect;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class NoEffect
implements ZEffect {
    @Override
    public void applyEffect(PixelmonWrapper pokemon) {
    }

    @Override
    public void getEffectText(PixelmonWrapper pixelmonWrapper) {
    }
}

