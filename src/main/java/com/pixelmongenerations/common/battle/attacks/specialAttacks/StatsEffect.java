/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks;

import com.pixelmongenerations.common.battle.attacks.AttackBase;
import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.OHKO;
import com.pixelmongenerations.common.battle.controller.CalcPriority;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Competitive;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Contrary;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Defiant;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Unaware;
import com.pixelmongenerations.common.entity.pixelmon.stats.BattleStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.ArrayList;

public class StatsEffect
extends EffectBase {
    private StatsType type;
    private boolean isUser = false;
    public int value;

    public StatsEffect(StatsType type, int value, boolean isUser) {
        this.type = type;
        this.value = value;
        this.isUser = isUser;
    }

    public AttackResult applyStatEffect(PixelmonWrapper user, PixelmonWrapper target, AttackBase a) {
        PixelmonWrapper affected = target;
        if (this.isUser) {
            affected = user;
        }
        if (affected.isFainted()) {
            if (a != null && a.getAttackCategory() == AttackCategory.Status) {
                return AttackResult.failed;
            }
            return AttackResult.succeeded;
        }
        if (this.checkChance()) {
            if (affected.getBattleStats().modifyStat(this.value, this.type, user, true)) {
                return AttackResult.succeeded;
            }
            if (a != null && a.getAttackCategory() != AttackCategory.Status) {
                return AttackResult.succeeded;
            }
            if (this.value == 0) {
                return AttackResult.failed;
            }
            boolean increase = this.value > 0;
            boolean bl = increase;
            if (affected.getBattleAbility(user) instanceof Contrary) {
                increase = !increase;
            }
            affected.getBattleStats().getStatFailureMessage(this.type, increase);
            return AttackResult.failed;
        }
        return AttackResult.succeeded;
    }

    @Override
    public void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        this.applyStatEffect(user, target, null);
    }

    @Override
    public boolean cantMiss(PixelmonWrapper user) {
        return this.isUser;
    }

    public StatsType getStatsType() {
        return this.type;
    }

    public boolean getUser() {
        return this.isUser;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        float chance = this.getChance();
        if (chance < 50.0f) {
            if (userChoice.isMiddleTier() || this.isUser) {
                userChoice.raiseWeight(chance / 100.0f);
            }
            return;
        }
        if (!this.isUser && this.value < 0) {
            if (userChoice.hitsAlly()) {
                return;
            }
            for (PixelmonWrapper target : userChoice.targets) {
                if (!(target.getBattleAbility() instanceof Competitive) && !(target.getBattleAbility() instanceof Defiant)) continue;
                userChoice.weight -= 50.0f;
            }
        }
        if (this.type == StatsType.Attack || this.type == StatsType.SpecialAttack || this.type == StatsType.Defence || this.type == StatsType.SpecialDefence) {
            pw.bc.sendMessages = false;
            if (this.isUser && (this.type == StatsType.Attack || this.type == StatsType.SpecialAttack) || !this.isUser && (this.type == StatsType.Defence || this.type == StatsType.SpecialDefence)) {
                ArrayList<MoveChoice> bestChoicesBefore = bestUserChoices;
                ArrayList<MoveChoice> bestChoicesAfter;
                pw.bc.simulateMode = false;
                BattleStats[] originalStats = new BattleStats[userChoice.targets.size()];
                BattleStats[] saveStats = new BattleStats[userChoice.targets.size()];
                if (this.isUser) {
                    originalStats[0] = pw.getBattleStats();
                    saveStats[0] = new BattleStats(originalStats[0]);
                } else {
                    for (int i = 0; i < originalStats.length; ++i) {
                        originalStats[i] = userChoice.targets.get(i).getBattleStats();
                        saveStats[i] = new BattleStats(originalStats[i]);
                    }
                }
                try {
                    if (this.isUser) {
                        pw.getBattleStats().modifyStat(this.value, this.type, pw, true);
                    } else {
                        for (PixelmonWrapper target : userChoice.targets) {
                            target.getBattleStats().modifyStat(this.value, this.type, pw, true);
                        }
                    }
                    pw.bc.simulateMode = true;
                    bestChoicesAfter = pw.getBattleAI().getBestAttackChoices(pw);
                    pw.bc.simulateMode = false;
                }
                finally {
                    pw.bc.simulateMode = false;
                    for (int i = 0; i < originalStats.length; ++i) {
                        originalStats[i].copyStats(saveStats[i]);
                    }
                }
                boolean weightNegative = true;
                if (this.isUser) {
                    EnumHeldItems heldItem = pw.getUsableHeldItem().getHeldItemType();
                    weightNegative = heldItem != EnumHeldItems.whiteHerb && heldItem != EnumHeldItems.ginemaBerry;
                }
                pw.getBattleAI().weightFromUserOptions(pw, userChoice, bestChoicesBefore, bestChoicesAfter, weightNegative);
                if (MoveChoice.canOutspeedAnd2HKO(bestOpponentChoices, pw, bestUserChoices) && !userChoice.isOffensiveMove()) {
                    userChoice.lowerTier(1);
                }
            } else if (this.isUser && (this.type == StatsType.Defence || this.type == StatsType.SpecialDefence) || !this.isUser && (this.type == StatsType.Attack || this.type == StatsType.SpecialAttack)) {
                int i;
                ArrayList<PixelmonWrapper> opponents = this.getAffectedOpponents(pw, userChoice);
                ArrayList bestChoicesBefore = MoveChoice.splitChoices(opponents, bestOpponentChoices);
                ArrayList bestChoicesAfter = new ArrayList(opponents.size());
                pw.bc.simulateMode = false;
                BattleStats[] originalStats = new BattleStats[userChoice.targets.size()];
                BattleStats[] saveStats = new BattleStats[userChoice.targets.size()];
                if (this.isUser) {
                    originalStats[0] = pw.getBattleStats();
                    saveStats[0] = new BattleStats(originalStats[0]);
                } else {
                    for (i = 0; i < originalStats.length; ++i) {
                        originalStats[i] = userChoice.targets.get(i).getBattleStats();
                        saveStats[i] = new BattleStats(originalStats[i]);
                    }
                }
                try {
                    if (this.isUser) {
                        pw.getBattleStats().modifyStat(this.value, this.type, pw, true);
                    } else {
                        for (i = 0; i < saveStats.length; ++i) {
                            PixelmonWrapper target = userChoice.targets.get(i);
                            target.getBattleStats().modifyStat(this.value, this.type, pw, true);
                        }
                    }
                    pw.bc.simulateMode = true;
                    for (PixelmonWrapper opponent : opponents) {
                        ArrayList<MoveChoice> after = pw.getBattleAI().getBestAttackChoices(opponent);
                        bestChoicesAfter.add((MoveChoice)((Object)after));
                    }
                    pw.bc.simulateMode = false;
                }
                finally {
                    pw.bc.simulateMode = false;
                    for (int i2 = 0; i2 < saveStats.length; ++i2) {
                        originalStats[i2].copyStats(saveStats[i2]);
                    }
                }
                if (bestChoicesBefore.size() != bestChoicesAfter.size()) {
                    pw.bc.simulateMode = true;
                    pw.bc.sendMessages = true;
                    return;
                }
                pw.getBattleAI().weightFromOpponentOptions(pw, userChoice, bestChoicesBefore, bestChoicesAfter, false);
            }
            pw.bc.simulateMode = true;
            pw.bc.sendMessages = true;
        } else if (this.type == StatsType.Speed) {
            if (!this.isUser) {
                bestOpponentChoices = MoveChoice.getAffectedChoices(userChoice, bestOpponentChoices);
            }
            if (MoveChoice.hasPriority(bestUserChoices, bestOpponentChoices)) {
                return;
            }
            ArrayList<PixelmonWrapper> opponents = this.getAffectedOpponents(pw, userChoice);
            int numOutspeedingOpponentsBefore = 0;
            int numOutspeedingOpponentsAfter2 = 0;
            for (PixelmonWrapper opponent : opponents) {
                if (pw.bc.getFirstMover(pw, opponent) != opponent) continue;
                ++numOutspeedingOpponentsBefore;
            }
            if (numOutspeedingOpponentsBefore == 0) {
                return;
            }
            pw.bc.sendMessages = false;
            pw.bc.simulateMode = false;
            BattleStats[] originalStats = new BattleStats[userChoice.targets.size()];
            BattleStats[] saveStats = new BattleStats[userChoice.targets.size()];
            if (this.isUser) {
                originalStats[0] = pw.getBattleStats();
                saveStats[0] = new BattleStats(originalStats[0]);
            } else {
                for (int i = 0; i < originalStats.length; ++i) {
                    originalStats[i] = userChoice.targets.get(i).getBattleStats();
                    saveStats[i] = new BattleStats(originalStats[i]);
                }
            }
            try {
                if (this.isUser) {
                    pw.getBattleStats().modifyStat(this.value, this.type, pw, true);
                } else {
                    for (PixelmonWrapper target : userChoice.targets) {
                        target.getBattleStats().modifyStat(this.value, this.type, pw, true);
                    }
                }
                CalcPriority.checkMoveSpeed(pw.bc);
                for (PixelmonWrapper opponent : opponents) {
                    if (pw.bc.getFirstMover(pw, opponent) != opponent) continue;
                    ++numOutspeedingOpponentsAfter2;
                }
            }
            finally {
                for (int i = 0; i < originalStats.length; ++i) {
                    originalStats[i].copyStats(saveStats[i]);
                }
            }
            pw.bc.simulateMode = true;
            pw.bc.sendMessages = true;
            if (numOutspeedingOpponentsAfter2 == 0) {
                userChoice.raiseWeight(75.0f);
            }
        } else if (this.type == StatsType.Accuracy || this.type == StatsType.Evasion) {
            boolean affectsUser = this.isUser && this.type == StatsType.Accuracy || !this.isUser && this.type == StatsType.Evasion;
            boolean bl = affectsUser;
            if (affectsUser) {
                for (MoveChoice bestUserChoice : bestUserChoices) {
                    if (!bestUserChoice.isOffensiveMove()) continue;
                    if (bestUserChoice.result.accuracy < 0 || bestUserChoice.result.accuracy == 100) {
                        return;
                    }
                    for (EffectBase effect : bestUserChoice.attack.getAttackBase().effects) {
                        if (!(effect instanceof OHKO)) continue;
                        return;
                    }
                }
                if (MoveChoice.canOutspeedAnd2HKO(bestOpponentChoices, pw, bestUserChoices)) {
                    return;
                }
            } else {
                for (MoveChoice bestOpponentChoice : bestOpponentChoices) {
                    if (!bestOpponentChoice.isOffensiveMove()) continue;
                    if (bestOpponentChoice.result.accuracy < 0) {
                        return;
                    }
                    for (EffectBase effect : bestOpponentChoice.attack.getAttackBase().effects) {
                        if (!(effect instanceof OHKO)) continue;
                        return;
                    }
                }
            }
            if (this.isUser && pw.getBattleAbility() instanceof Contrary) {
                return;
            }
            ArrayList<PixelmonWrapper> opponents2 = this.getAffectedOpponents(pw, userChoice);
            for (PixelmonWrapper opponent : opponents2) {
                AbilityBase opponentAbility = opponent.getBattleAbility();
                if (!(opponentAbility instanceof Unaware) && (this.isUser || !(opponentAbility instanceof Contrary))) continue;
                return;
            }
            userChoice.raiseWeight(20 * Math.abs(this.value));
        }
        if (this.isUser && this.value > 0) {
            if (MoveChoice.hasSuccessfulAttackChoice(userChoices, "Baton Pass", "Stored Power")) {
                userChoice.raiseWeight(10 * this.value);
            }
        }
        if (!this.isUser && userChoice.weight > 100.0f) {
            userChoice.weight = 100.0f;
        }
    }

    private ArrayList<PixelmonWrapper> getAffectedOpponents(PixelmonWrapper pw, MoveChoice choice) {
        return this.isUser ? pw.bc.getOpponentPokemon(pw) : choice.targets;
    }
}

