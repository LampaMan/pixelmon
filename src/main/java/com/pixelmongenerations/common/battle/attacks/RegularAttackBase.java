/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks;

import com.pixelmongenerations.common.battle.attacks.AttackBase;
import com.pixelmongenerations.common.battle.attacks.ZAttackBase;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Optional;

public class RegularAttackBase
extends AttackBase {
    private Map<String, ZAttackBase> zAttackBase = ZAttackBase.generate(this);

    public RegularAttackBase(int attackIndex, String moveName, ResultSet rs) throws SQLException {
        super(attackIndex, moveName, rs);
    }

    public Optional<ZAttackBase> getZAttackBase(String key) {
        ZAttackBase base = this.zAttackBase.get(key);
        return base != null ? Optional.of(base) : Optional.empty();
    }
}

