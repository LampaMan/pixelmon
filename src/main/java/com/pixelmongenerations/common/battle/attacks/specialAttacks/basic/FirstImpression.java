/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import net.minecraft.util.text.TextComponentTranslation;

public class FirstImpression
extends SpecialAttackBase {
    @Override
    public float modifyPriority(PixelmonWrapper pw) {
        return pw.isFirstTurn() ? pw.priority + 2.0f : pw.priority;
    }

    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.simulateMode) {
            return AttackResult.proceed;
        }
        if (user.isFirstTurn()) {
            return AttackResult.proceed;
        }
        user.bc.sendToAll(new TextComponentTranslation("pixelmon.status.usefailed", user.pokemon.getNickname(), user.attack.getAttackBase().getLocalizedName()));
        return AttackResult.failed;
    }
}

