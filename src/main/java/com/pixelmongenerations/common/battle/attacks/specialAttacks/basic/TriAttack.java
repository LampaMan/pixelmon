/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Burn;
import com.pixelmongenerations.common.battle.status.Freeze;
import com.pixelmongenerations.common.battle.status.Paralysis;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class TriAttack
extends SpecialAttackBase {
    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (this.checkChance()) {
            switch (RandomHelper.getRandomNumberBetween(0, 2)) {
                case 0: {
                    Burn.burn(user, target, user.attack, true);
                    break;
                }
                case 1: {
                    Paralysis.paralyze(user, target, user.attack, true);
                    break;
                }
                case 2: {
                    Freeze.freeze(user, target);
                }
            }
        }
    }
}

