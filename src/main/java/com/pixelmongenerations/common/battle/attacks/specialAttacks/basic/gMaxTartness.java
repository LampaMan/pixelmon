/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class gMaxTartness
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        for (PixelmonWrapper targets : target.bc.getTeamPokemon(target.getParticipant())) {
            targets.getBattleStats().modifyStat(-1, StatsType.Evasion);
        }
        return AttackResult.proceed;
    }
}

