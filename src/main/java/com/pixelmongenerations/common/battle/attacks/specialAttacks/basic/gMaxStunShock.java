/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Paralysis;
import com.pixelmongenerations.common.battle.status.Poison;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class gMaxStunShock
extends SpecialAttackBase {
    transient int stunshock;

    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        for (PixelmonWrapper targets : target.bc.getTeamPokemon(target.getParticipant())) {
            this.stunshock = RandomHelper.getRandomNumberBetween(1, 99);
            if (user.bc.simulateMode) {
                this.stunshock = 0;
                continue;
            }
            if (this.stunshock <= 50) {
                targets.addStatus(new Poison(), targets);
                continue;
            }
            if (this.stunshock > 100) continue;
            targets.addStatus(new Paralysis(), targets);
        }
        return AttackResult.proceed;
    }
}

