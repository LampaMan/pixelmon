/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.Swap;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class PowerSwap
extends Swap {
    public PowerSwap() {
        super("pixelmon.effect.powerswap", StatsType.Attack, StatsType.SpecialAttack);
    }
}

