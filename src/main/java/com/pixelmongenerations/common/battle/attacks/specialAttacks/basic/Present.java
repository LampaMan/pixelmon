/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class Present
extends SpecialAttackBase {
    transient int r;

    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        this.r = RandomHelper.getRandomNumberBetween(1, 100);
        if (user.bc.simulateMode) {
            this.r = 0;
            user.attack.getAttackBase().basePower = 52;
        } else if (this.r <= 40) {
            user.attack.getAttackBase().basePower = 40;
        } else if (this.r <= 70) {
            user.attack.getAttackBase().basePower = 80;
        } else if (this.r <= 80) {
            user.attack.getAttackBase().basePower = 120;
        }
        return AttackResult.proceed;
    }

    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (this.r > 80) {
            double restoration = (double)target.getMaxHealth() * 0.25;
            if (restoration > (double)(target.getMaxHealth() - target.getHealth())) {
                restoration = target.getMaxHealth() - target.getHealth();
            }
            if (restoration <= 0.0) {
                user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
                return AttackResult.failed;
            }
            target.healEntityBy((int)restoration);
            user.bc.sendToAll("pixelmon.effect.healother", user.getNickname(), target.getNickname());
        }
        return AttackResult.proceed;
    }
}

