/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.zeffects;

import com.pixelmongenerations.common.battle.attacks.zeffects.ZEffect;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class CompleteHeal
implements ZEffect {
    @Override
    public void applyEffect(PixelmonWrapper pokemon) {
        pokemon.healByPercent(100.0f);
    }

    @Override
    public void getEffectText(PixelmonWrapper pokemon) {
        pokemon.bc.sendToAll("%s had its HP restored.", pokemon.getNickname());
    }
}

