/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;

public class Feint
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        boolean showMessage = false;
        showMessage = target.removeStatus(StatusType.Protect);
        if (!user.bc.getTeamPokemon(user.getParticipant()).contains(target)) {
            showMessage = target.removeTeamStatus(StatusType.WideGuard) || showMessage;
            showMessage = target.removeTeamStatus(StatusType.QuickGuard) || showMessage;
            showMessage = target.removeTeamStatus(StatusType.CraftyShield) || showMessage;
            boolean bl = showMessage;
        }
        if (showMessage) {
            target.bc.sendToAll("pixelmon.effect.feint", user.getNickname(), target.getNickname());
        }
        return AttackResult.proceed;
    }
}

