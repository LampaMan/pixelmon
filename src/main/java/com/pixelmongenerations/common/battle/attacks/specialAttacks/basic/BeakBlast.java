/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.BeakBlastContact;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import java.util.ArrayList;

public class BeakBlast
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        block4: {
            block3: {
                block2: {
                    if (user.lastAttack == null) break block2;
                    if (user.lastAttack.isAttack("Beak Blast")) break block3;
                }
                if (!user.bc.simulateMode) break block4;
            }
            user.bc.sendToAll("pixelmon.battletext.used", user.getNickname(), user.attack.getAttackBase().getLocalizedName());
            user.removeStatus(StatusType.BeakBlastContact);
            user.priority = -3.0f;
            return AttackResult.proceed;
        }
        user.priority = 0.0f;
        return AttackResult.proceed;
    }

    @Override
    public void applyEarlyEffect(PixelmonWrapper user) {
        user.bc.sendToAll("pixelmon.effect.beakblast", user.getNickname());
        user.addStatus(new BeakBlastContact(), user);
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (pw.hasStatus(StatusType.Substitute)) {
            return;
        }
        for (PixelmonWrapper opponent : pw.getOpponentPokemon()) {
            if (opponent.lastAttack == null || opponent.lastAttack.moveResult.target != pw || opponent.lastAttack.getAttackCategory() == AttackCategory.Status) continue;
            userChoice.lowerTier(2);
            userChoice.weight = 0.0f;
        }
    }
}

