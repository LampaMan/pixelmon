/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn.MultiTurnCharge;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Submerged;

public class Dive
extends MultiTurnCharge {
    public Dive() {
        super("pixelmon.effect.dive", Submerged.class, StatusType.Submerged);
    }
}

