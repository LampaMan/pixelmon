/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.zeffects;

import com.pixelmongenerations.common.battle.attacks.zeffects.ZEffect;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.EnumType;

public class ZCurse
implements ZEffect {
    @Override
    public void applyEffect(PixelmonWrapper pokemon) {
        if (pokemon.hasType(EnumType.Ghost)) {
            pokemon.healByPercent(100.0f);
        } else {
            pokemon.getBattleStats().increaseStat(1, StatsType.Attack, pokemon, false, false);
        }
    }

    @Override
    public void getEffectText(PixelmonWrapper pokemon) {
        if (pokemon.hasType(EnumType.Ghost)) {
            pokemon.bc.sendToAll("%s had its HP restored.", pokemon.getNickname());
        } else {
            pokemon.bc.sendToAll("pixelmon.zeffect.stat_raise", new Object[]{pokemon.getNickname(), StatsType.Attack});
        }
    }
}

