/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.FirePledge;
import com.pixelmongenerations.common.battle.status.GrassPledge;
import com.pixelmongenerations.common.battle.status.WaterPledge;
import java.util.ArrayList;

public class Pledge
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (this.getPreviousPledge(user) != null) {
            user.attack.getAttackBase().basePower *= 2;
        }
        return AttackResult.proceed;
    }

    /*
     * Enabled aggressive block sorting
     */
    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        Attack beforePledge = this.getPreviousPledge(user);
        if (beforePledge == null) return;
        if (!beforePledge.isAttack("Fire Pledge")) {
            if (!user.attack.isAttack("Fire Pledge")) {
                target.bc.sendToAll("pixelmon.status.grasspledge", target.getNickname());
                target.addTeamStatus(new GrassPledge(), user);
                return;
            }
        }
        if (!beforePledge.isAttack("Grass Pledge")) {
            if (!user.attack.isAttack("Grass Pledge")) {
                user.bc.sendToAll("pixelmon.status.waterpledge", user.getNickname());
                user.addTeamStatus(new WaterPledge(), user);
                return;
            }
        }
        if (beforePledge.isAttack("Water Pledge")) return;
        if (user.attack.isAttack("Water Pledge")) return;
        target.bc.sendToAll("pixelmon.status.firepledge", target.getNickname());
        target.addTeamStatus(new FirePledge(), user);
    }

    private Attack getPreviousPledge(PixelmonWrapper user) {
        Attack beforePledge = null;
        ArrayList<PixelmonWrapper> team = user.bc.getTeamPokemon(user.getParticipant());
        for (int i = 0; i < user.bc.turn; ++i) {
            Attack currentAttack;
            PixelmonWrapper currentPokemon = user.bc.turnList.get(i);
            if (!currentPokemon.canAttack || !team.contains(currentPokemon) || (currentAttack = user.bc.turnList.get((int)i).attack) == null || !currentAttack.getAttackBase().getUnlocalizedName().contains("pledge") || user.attack.equals(currentAttack)) continue;
            beforePledge = currentAttack;
        }
        return beforePledge;
    }
}

