/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;

public class LastResort
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        Moveset moveset = user.getMoveset();
        int movesetSize = moveset.size();
        if (movesetSize == 1) {
            if (moveset.get(0).isAttack("Last Resort")) {
                user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
                return AttackResult.failed;
            }
        }
        for (Attack move : moveset) {
            if (move == null) continue;
            if (move.isAttack("Last Resort") || user.usedMoves.contains(move)) continue;
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        return AttackResult.proceed;
    }
}

