/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Sleep;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class SleepTalk
extends SpecialAttackBase {
    private static final String[] notAllowedList = new String[]{"Bounce", "Chatter", "Copycat", "Dig", "Dive", "Fly", "Focus Punch", "Me First", "Metronome", "Mirror Move", "Shadow Force", "Sketch", "Skull Bash", "Sky Attack", "Solar Beam", "Razor Wind", "Uproar", "Sleep Talk"};

    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (!user.hasStatus(StatusType.Sleep)) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        ArrayList<Attack> possibleAttacks = this.getPossibleAttacks(user);
        if (!possibleAttacks.isEmpty()) {
            Attack a = RandomHelper.getRandomElementFromList(possibleAttacks);
            user.useTempAttack(a);
            return AttackResult.ignore;
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }

    private boolean canSelect(Attack a) {
        return !a.isAttack(notAllowedList);
    }

    private ArrayList<Attack> getPossibleAttacks(PixelmonWrapper user) {
        return user.getMoveset().stream().filter(this::canSelect).collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        Sleep sleep = (Sleep)pw.getStatus(StatusType.Sleep);
        if (sleep == null || sleep.effectTurns == 0) {
            return;
        }
        ArrayList<Attack> possibleAttacks = this.getPossibleAttacks(pw);
        if (possibleAttacks.isEmpty()) {
            return;
        }
        pw.getBattleAI().weightRandomMove(pw, userChoice, MoveChoice.createMoveChoicesFromList(possibleAttacks, pw));
        if (userChoice.weight > 0.0f) {
            userChoice.raiseTier(4);
        }
    }
}

