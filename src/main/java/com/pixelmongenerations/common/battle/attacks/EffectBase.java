/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.EffectParser;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.modifiers.ChanceModifier;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.modifiers.ModifierBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.modifiers.ModifierType;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.ArrayList;
import java.util.List;

public abstract class EffectBase {
    public ArrayList<ModifierBase> modifiers = new ArrayList();
    private boolean persists;

    public EffectBase() {
        this.persists = false;
    }

    public EffectBase(boolean persists) {
        this.persists = persists;
    }

    public void addModifier(ModifierBase modifier) {
        this.modifiers.add(modifier);
    }

    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        return AttackResult.proceed;
    }

    protected abstract void applyEffect(PixelmonWrapper var1, PixelmonWrapper var2);

    public boolean checkChance() {
        for (ModifierBase m : this.modifiers) {
            if (m.type != ModifierType.Chance) continue;
            return ((ChanceModifier)m).RollChance();
        }
        return true;
    }

    public boolean isChance() {
        for (ModifierBase m : this.modifiers) {
            if (m.type != ModifierType.Chance) continue;
            return true;
        }
        return false;
    }

    public float getChance() {
        for (ModifierBase m : this.modifiers) {
            if (m.type != ModifierType.Chance) continue;
            return ((ChanceModifier)m).value;
        }
        return 100.0f;
    }

    public void changeChance(int multiplier) {
        this.modifiers.stream().filter(m -> m.type == ModifierType.Chance).forEach(m -> {
            ((ChanceModifier)m).multiplier = multiplier;
        });
    }

    public abstract boolean cantMiss(PixelmonWrapper var1);

    public static EffectBase getEffect(String e) {
        EffectParser p = new EffectParser();
        return p.ParseEffect(e);
    }

    public boolean doesPersist(PixelmonWrapper user) {
        return this.persists;
    }

    public void applyMissEffect(PixelmonWrapper user, PixelmonWrapper target) {
    }

    public void applyEarlyEffect(PixelmonWrapper user) {
    }

    public void dealtDamage(PixelmonWrapper attacker, PixelmonWrapper defender, Attack attack, DamageTypeEnum damageType) {
    }

    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
    }

    protected float getWeightWithChance(float weight) {
        float chance = this.getChance() / 100.0f;
        return chance >= 0.5f ? weight : chance;
    }

    public double modifyTypeEffectiveness(List<EnumType> effectiveTypes, EnumType moveType, double baseEffectiveness) {
        return baseEffectiveness;
    }

    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        return new int[]{power, accuracy};
    }

    public int[] modifyPowerAndAccuracyTarget(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        return new int[]{power, accuracy};
    }

    public boolean isAbility(AbilityBase targetAbility, Class<? extends AbilityBase> ... abilities) {
        for (Class<? extends AbilityBase> ability : abilities) {
            if (!targetAbility.getClass().equals(ability)) continue;
            return true;
        }
        return false;
    }
}

