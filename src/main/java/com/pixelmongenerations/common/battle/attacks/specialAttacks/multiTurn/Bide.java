/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn.MultiTurnSpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.BideStatus;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.ArrayList;

public class Bide
extends MultiTurnSpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.simulateMode) {
            return AttackResult.hit;
        }
        if (!this.doesPersist(user)) {
            this.setPersists(user, true);
            this.setTurnCount(user, 3);
        }
        this.decrementTurnCount(user);
        int turnCount = this.getTurnCount(user);
        if (turnCount == 2) {
            user.bc.sendToAll("pixelmon.effect.bidetime", user.getNickname());
            user.addStatus(new BideStatus(), user);
            return AttackResult.charging;
        }
        if (turnCount == 1) {
            user.bc.sendToAll("pixelmon.effect.storeenergy", user.getNickname());
            return AttackResult.charging;
        }
        if (turnCount <= 0) {
            this.setPersists(user, false);
            user.bc.sendToAll("pixelmon.effect.unleashenergy", user.getNickname());
            BideStatus status = (BideStatus)user.getStatus(StatusType.Bide);
            float damage = 0.0f;
            if (status != null) {
                damage = status.damageTaken * 2.0f;
            }
            if (damage == 0.0f || status == null || status.lastAttacker == null) {
                user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
                return AttackResult.failed;
            }
            if (status.lastAttacker.getEffectiveTypes(user, status.lastAttacker).contains((Object)EnumType.Ghost)) {
                user.bc.sendToAll("pixelmon.battletext.noeffect", status.lastAttacker.getNickname());
                return AttackResult.failed;
            }
            if (!status.lastAttacker.bc.isInBattle(status.lastAttacker)) {
                user.bc.getOppositePokemon(target).doBattleDamage(user, damage, DamageTypeEnum.ATTACKFIXED);
                return AttackResult.hit;
            }
            status.lastAttacker.doBattleDamage(user, damage, DamageTypeEnum.ATTACKFIXED);
            return AttackResult.hit;
        }
        return AttackResult.failed;
    }

    @Override
    public boolean cantMiss(PixelmonWrapper user) {
        return true;
    }

    @Override
    public boolean ignoresType(PixelmonWrapper user) {
        return true;
    }

    @Override
    public void removeEffect(PixelmonWrapper user, PixelmonWrapper target) {
        this.setPersists(user, false);
    }

    @Override
    public boolean isCharging(PixelmonWrapper user, PixelmonWrapper target) {
        return !this.doesPersist(user) || this.getTurnCount(user) > 1;
    }

    @Override
    public boolean shouldNotLosePP(PixelmonWrapper user) {
        return this.doesPersist(user);
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        PixelmonWrapper lastMover = pw.bc.getLastMover(pw.getOpponentPokemon());
        if (lastMover != null) {
            float maxDamagePercent = MoveChoice.getMaxDamagePercent(pw, MoveChoice.getTargetedChoices(pw, bestOpponentChoices));
            maxDamagePercent = maxDamagePercent / (float)pw.getMaxHealth() * (float)lastMover.getMaxHealth();
            userChoice.raiseWeight(maxDamagePercent * 2.0f / 3.0f);
        }
        if (MoveChoice.canOutspeedAndKO(3, bestOpponentChoices, pw, userChoice.createList())) {
            userChoice.lowerTier(1);
        }
    }
}

