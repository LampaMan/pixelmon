/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.rules.clauses;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClause;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.heldItems.ItemMegaStone;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.helper.ArrayHelper;
import java.util.Optional;

public class AbilityClause
extends BattleClause {
    private Class<? extends AbilityBase>[] abilities;

    public AbilityClause(String id, Class<? extends AbilityBase> ... abilities) {
        super(id);
        this.abilities = abilities;
        ArrayHelper.validateArrayNonNull(abilities);
    }

    @Override
    public boolean validateSingle(PokemonLink pokemon) {
        EnumSpecies species;
        ItemHeld heldItem = pokemon.getHeldItem();
        if (PixelmonWrapper.canMegaEvolve(heldItem, species = pokemon.getBaseStats().pokemon, pokemon.getForm())) {
            ItemMegaStone megaStone = (ItemMegaStone)heldItem;
            Optional<BaseStats> megaStats = Entity3HasStats.getBaseStats(species, megaStone.form);
            if (megaStats.isPresent()) {
                AbilityBase megaAbility = AbilityBase.getAbility(megaStats.get().abilities[0]).orElse(null);
                Class<?> megaAbilityClass = megaAbility == null ? null : megaAbility.getClass();
                Class<?> class_ = megaAbilityClass;
                if (ArrayHelper.contains(this.abilities, megaAbilityClass)) {
                    return false;
                }
            }
        }
        return !pokemon.getAbility().isAbility(this.abilities);
    }
}

