/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.rules.clauses.tiers;

import com.pixelmongenerations.common.battle.rules.clauses.tiers.TierSet;
import com.pixelmongenerations.common.entity.npcs.registry.PokemonForm;
import java.util.Set;
import java.util.function.Predicate;

public class TierHierarchical
extends TierSet {
    private final TierHierarchical tierAbove;

    public TierHierarchical(String id, Set<PokemonForm> pokemon, TierHierarchical tierAbove) {
        super(id, pokemon);
        this.tierAbove = tierAbove;
    }

    @Override
    protected Predicate<PokemonForm> getCondition() {
        return pokemonForm -> this.getID().equals("unrestricted") || this.isAllowed((PokemonForm)pokemonForm);
    }

    private boolean isAllowed(PokemonForm pokemonForm) {
        return this.isInSet(pokemonForm) || this.tierAbove != null && this.tierAbove.isAllowed(pokemonForm);
    }
}

