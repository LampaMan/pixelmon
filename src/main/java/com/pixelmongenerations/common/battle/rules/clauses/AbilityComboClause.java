/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.rules.clauses;

import com.pixelmongenerations.common.battle.rules.clauses.AbilityClause;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClauseAll;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class AbilityComboClause
extends BattleClauseAll {
    public AbilityComboClause(String id, Class<? extends AbilityBase> ... abilities) {
        super(id, AbilityComboClause.getClauses(abilities));
    }

    private static AbilityClause[] getClauses(Class<? extends AbilityBase> ... abilities) {
        AbilityClause[] abilityClauses = new AbilityClause[abilities.length];
        for (int i = 0; i < abilities.length; ++i) {
            abilityClauses[i] = new AbilityClause("", abilities[i]);
        }
        return abilityClauses;
    }
}

