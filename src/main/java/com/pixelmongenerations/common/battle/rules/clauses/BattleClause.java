/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.common.battle.rules.clauses;

import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.core.util.IEncodeable;
import io.netty.buffer.ByteBuf;
import java.util.List;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class BattleClause
implements Comparable<BattleClause>,
IEncodeable {
    private String id;
    private String description = "";

    public BattleClause(String id) {
        if (id == null) {
            id = "";
        }
        this.id = id;
    }

    public BattleClause setDescription(String description) {
        if (description == null) {
            description = "";
        }
        this.description = description;
        return this;
    }

    public String getID() {
        return this.id;
    }

    public String getDescription() {
        if (!this.description.isEmpty()) {
            return this.description;
        }
        return I18n.translateToLocal("gui.battlerules.description." + this.id);
    }

    public String getLocalizedName() {
        return BattleClause.getLocalizedName(this.id);
    }

    public String toString() {
        return this.getLocalizedName();
    }

    public static String getLocalizedName(String clauseID) {
        String langKey = "gui.battlerules." + clauseID;
        if (I18n.canTranslate(langKey)) {
            return I18n.translateToLocal(langKey);
        }
        return clauseID;
    }

    public boolean equals(Object other) {
        if (other instanceof BattleClause) {
            return ((BattleClause)other).getID().equals(this.id);
        }
        return false;
    }

    public int hashCode() {
        return this.id.hashCode();
    }

    public boolean validateSingle(PokemonLink pokemon) {
        return true;
    }

    public boolean validateTeam(List<PokemonLink> team) {
        return team.stream().allMatch(this::validateSingle);
    }

    @Override
    public int compareTo(BattleClause o) {
        return this.id.compareTo(o.getID());
    }

    @Override
    public void encodeInto(ByteBuf buffer) {
        ByteBufUtils.writeUTF8String(buffer, this.id);
        ByteBufUtils.writeUTF8String(buffer, this.description);
    }

    @Override
    public void decodeInto(ByteBuf buffer) {
        this.id = ByteBufUtils.readUTF8String(buffer);
        this.description = ByteBufUtils.readUTF8String(buffer);
    }
}

