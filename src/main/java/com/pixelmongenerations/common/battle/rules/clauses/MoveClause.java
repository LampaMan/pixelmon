/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.rules.clauses;

import com.pixelmongenerations.common.battle.rules.clauses.BattleClause;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.core.util.helper.ArrayHelper;

public class MoveClause
extends BattleClause {
    private String[] moves;

    public MoveClause(String id, String ... moves) {
        super(id);
        this.moves = moves;
        ArrayHelper.validateArrayNonNull(moves);
    }

    @Override
    public boolean validateSingle(PokemonLink pokemon) {
        return !pokemon.getMoveset().hasAttack(this.moves);
    }
}

