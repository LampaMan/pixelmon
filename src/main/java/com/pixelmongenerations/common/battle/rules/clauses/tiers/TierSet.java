/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.rules.clauses.tiers;

import com.pixelmongenerations.common.battle.rules.clauses.tiers.Tier;
import com.pixelmongenerations.common.entity.npcs.registry.PokemonForm;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import net.minecraft.client.resources.I18n;

public abstract class TierSet
extends Tier {
    private Set<PokemonForm> pokemon;

    protected TierSet(String id, Set<PokemonForm> pokemon) {
        super(id);
        this.setPokemon(pokemon);
        this.condition = this.getCondition();
        if (this.condition == null) {
            throw new NullPointerException("Condition cannot be null.");
        }
    }

    public TierSet setPokemon(Set<PokemonForm> pokemon) {
        if (pokemon == null) {
            pokemon = new HashSet<PokemonForm>();
        }
        this.pokemon = pokemon;
        return this;
    }

    protected abstract Predicate<PokemonForm> getCondition();

    protected boolean isInSet(PokemonForm pokemonForm) {
        return this.pokemon.stream().anyMatch(a -> a.equals(pokemonForm));
    }

    @Override
    public String getTierDescription() {
        if (this.pokemon.isEmpty()) {
            return I18n.format("pixelmon.command.tier.everything", new Object[0]);
        }
        ArrayList<String> names = new ArrayList<String>();
        for (PokemonForm form : this.pokemon) {
            names.add(form.getLocalizedName());
        }
        Collections.sort(names);
        StringBuilder builder = new StringBuilder();
        boolean first = true;
        for (String name : names) {
            if (!first) {
                builder.append(", ");
            }
            first = false;
            builder.append(name);
        }
        return builder.toString();
    }
}

