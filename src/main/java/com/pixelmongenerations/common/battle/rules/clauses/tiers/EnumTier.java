/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.rules.clauses.tiers;

public enum EnumTier {
    Unrestricted,
    OU,
    UU,
    RU,
    NU,
    LC,
    PU;


    public String getTierID() {
        return this.name().toLowerCase();
    }
}

