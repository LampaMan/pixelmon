/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.rules.teamselection;

import com.pixelmongenerations.common.battle.rules.BattleRules;
import com.pixelmongenerations.common.battle.rules.teamselection.TeamSelection;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.HashMap;
import java.util.Map;
import net.minecraft.entity.player.EntityPlayerMP;

public final class TeamSelectionList {
    private static int idCounter;
    private static final Map<Integer, TeamSelection> teamSelectMap;

    private TeamSelectionList() {
    }

    public static void addTeamSelection(BattleRules rules, boolean showRules, PlayerStorage ... storages) {
        int newID = idCounter++;
        TeamSelection selection = new TeamSelection(newID, rules, showRules, storages);
        teamSelectMap.put(newID, selection);
        selection.initializeClient();
    }

    public static void removeSelection(EntityPlayerMP player) {
        for (TeamSelection selection : teamSelectMap.values()) {
            if (!selection.hasPlayer(player)) continue;
            teamSelectMap.remove(selection.id);
            return;
        }
    }

    static void removeSelection(int id) {
        teamSelectMap.remove(id);
    }

    public static TeamSelection getTeamSelection(int id) {
        return teamSelectMap.get(id);
    }

    static {
        teamSelectMap = new HashMap<Integer, TeamSelection>();
    }
}

