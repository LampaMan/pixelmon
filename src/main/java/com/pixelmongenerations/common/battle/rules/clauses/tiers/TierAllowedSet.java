/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.rules.clauses.tiers;

import com.pixelmongenerations.common.battle.rules.clauses.tiers.TierSet;
import com.pixelmongenerations.common.entity.npcs.registry.PokemonForm;
import java.util.Set;
import java.util.function.Predicate;
import net.minecraft.client.resources.I18n;

public class TierAllowedSet
extends TierSet {
    private final boolean convertToBanlist;

    public TierAllowedSet(String id, Set<PokemonForm> pokemon, boolean convertToBanlist) {
        super(id, pokemon);
        this.convertToBanlist = convertToBanlist;
    }

    @Override
    protected Predicate<PokemonForm> getCondition() {
        return p -> this.convertToBanlist != this.isInSet((PokemonForm)p);
    }

    @Override
    public String getTierDescription() {
        StringBuilder builder = new StringBuilder();
        builder.append(I18n.format("gui.battlerules." + (this.convertToBanlist ? "banned" : "allowed"), new Object[0]));
        builder.append(": ");
        builder.append(super.getTierDescription());
        return builder.toString();
    }
}

