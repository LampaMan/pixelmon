/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.rules.clauses;

import com.pixelmongenerations.common.battle.rules.clauses.BattleClause;
import com.pixelmongenerations.common.battle.rules.clauses.MoveClause;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import java.util.List;

class BatonPass1Clause
extends BattleClause {
    private MoveClause batonPassCheck = new MoveClause("", "Baton Pass");

    BatonPass1Clause() {
        super("batonpass1");
    }

    @Override
    public boolean validateTeam(List<PokemonLink> team) {
        boolean hasOneBatonPass = false;
        for (PokemonLink pokemon : team) {
            if (this.batonPassCheck.validateSingle(pokemon)) continue;
            if (hasOneBatonPass) {
                return false;
            }
            hasOneBatonPass = true;
        }
        return true;
    }
}

