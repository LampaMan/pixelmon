/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.common.battle.rules.teamselection;

import com.pixelmongenerations.common.entity.npcs.registry.PokemonForm;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.util.IEncodeable;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class TeamSelectPokemon
implements IEncodeable {
    public PokemonForm pokemon;
    public int specialTexture;
    public int pokeBall;
    public boolean isShiny;
    public boolean isEgg;
    public int eggCycles;
    public String customTexture;

    public TeamSelectPokemon(NBTTagCompound nbt, EntityPlayer player) {
        this.pokemon = new PokemonForm(nbt);
        this.specialTexture = nbt.getShort("specialTexture");
        this.pokeBall = nbt.getInteger("CaughtBall");
        this.isShiny = nbt.getBoolean("IsShiny");
        this.isEgg = nbt.getBoolean("isEgg");
        this.eggCycles = nbt.getInteger("eggCycles");
        this.customTexture = nbt.getString("CustomTexture");
    }

    public TeamSelectPokemon(PixelmonData data) {
        this.pokemon = new PokemonForm(data.getSpecies(), data.form, data.gender);
        this.specialTexture = data.specialTexture;
        this.pokeBall = data.pokeball.getIndex();
        this.isShiny = data.isShiny;
        this.isEgg = data.isEgg;
        this.eggCycles = data.eggCycles;
        this.customTexture = data.customTexture;
    }

    public TeamSelectPokemon(ByteBuf buffer) {
        this.decodeInto(buffer);
    }

    @Override
    public void encodeInto(ByteBuf buffer) {
        this.pokemon.encodeInto(buffer);
        buffer.writeInt(this.specialTexture);
        buffer.writeInt(this.pokeBall);
        buffer.writeBoolean(this.isShiny);
        buffer.writeBoolean(this.isEgg);
        buffer.writeInt(this.eggCycles);
        ByteBufUtils.writeUTF8String(buffer, this.customTexture);
    }

    @Override
    public void decodeInto(ByteBuf buffer) {
        this.pokemon = new PokemonForm(buffer);
        this.specialTexture = buffer.readInt();
        this.pokeBall = buffer.readInt();
        this.isShiny = buffer.readBoolean();
        this.isEgg = buffer.readBoolean();
        this.eggCycles = buffer.readInt();
        this.customTexture = ByteBufUtils.readUTF8String(buffer);
    }
}

