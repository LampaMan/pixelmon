/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.rules.clauses;

import com.pixelmongenerations.common.battle.rules.clauses.BattleClause;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.util.helper.ArrayHelper;

public class ItemPreventClause
extends BattleClause {
    private EnumHeldItems[] heldItems;

    public ItemPreventClause(String id, EnumHeldItems ... heldItems) {
        super(id);
        this.heldItems = heldItems;
        ArrayHelper.validateArrayNonNull(heldItems);
    }

    @Override
    public boolean validateSingle(PokemonLink pokemon) {
        for (EnumHeldItems heldItem : this.heldItems) {
            if (pokemon.getHeldItem().getHeldItemType() != heldItem) continue;
            return false;
        }
        return true;
    }
}

