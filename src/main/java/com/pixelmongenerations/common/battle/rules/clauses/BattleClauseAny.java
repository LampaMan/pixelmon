/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.rules.clauses;

import com.pixelmongenerations.common.battle.rules.clauses.BattleClause;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.core.util.helper.ArrayHelper;
import java.util.List;

public class BattleClauseAny
extends BattleClause {
    private BattleClause[] clauses;

    public BattleClauseAny(String id, BattleClause ... clauses) {
        super(id);
        this.clauses = clauses;
        ArrayHelper.validateArrayNonNull(clauses);
    }

    @Override
    public boolean validateSingle(PokemonLink pokemon) {
        for (BattleClause clause : this.clauses) {
            if (clause.validateSingle(pokemon)) continue;
            return false;
        }
        return true;
    }

    @Override
    public boolean validateTeam(List<PokemonLink> team) {
        for (BattleClause clause : this.clauses) {
            if (clause.validateTeam(team)) continue;
            return false;
        }
        return true;
    }
}

