/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.rules.clauses;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClause;
import com.pixelmongenerations.common.battle.rules.clauses.MoveClause;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Download;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Moody;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SpeedBoost;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;

class SpeedPassClause
extends BattleClause {
    private MoveClause batonPassClause = new MoveClause("", "Baton Pass");

    SpeedPassClause() {
        super("speedpass");
    }

    @Override
    public boolean validateSingle(PokemonLink pokemon) {
        if (this.batonPassClause.validateSingle(pokemon)) {
            return true;
        }
        boolean hasSpeedBoost = false;
        boolean hasOtherBoost = false;
        AbilityBase ability = pokemon.getAbility();
        if (ability.isAbility(Moody.class)) {
            return false;
        }
        if (ability.isAbility(SpeedBoost.class)) {
            hasSpeedBoost = true;
        } else if (ability.isAbility(Download.class)) {
            hasOtherBoost = true;
        }
        for (Attack attack : pokemon.getMoveset().attacks) {
            if (attack == null) continue;
            if (attack.isAttack("Geomancy")) {
                return false;
            }
            for (EffectBase effect : attack.getAttackBase().effects) {
                if (effect.getChance() < 100.0f || !(effect instanceof StatsEffect)) continue;
                StatsEffect statsEffect = (StatsEffect)effect;
                StatsType stat = statsEffect.getStatsType();
                if (stat == StatsType.Speed) {
                    hasSpeedBoost = true;
                    continue;
                }
                hasOtherBoost = true;
            }
            if (!attack.isAttack("Belly Drum", "Curse", "Growth", "Magnetic Flux", "Minimize", "Rototiller", "Skull Bash", "Stockpile")) continue;
            hasOtherBoost = true;
        }
        return !hasSpeedBoost || !hasOtherBoost;
    }
}

