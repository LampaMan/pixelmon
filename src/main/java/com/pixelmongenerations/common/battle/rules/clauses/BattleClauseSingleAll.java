/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.rules.clauses;

import com.pixelmongenerations.common.battle.rules.clauses.BattleClause;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClauseAll;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import java.util.List;

public class BattleClauseSingleAll
extends BattleClauseAll {
    public BattleClauseSingleAll(String id, BattleClause ... clauses) {
        super(id, clauses);
    }

    @Override
    public boolean validateTeam(List<PokemonLink> team) {
        for (PokemonLink pokemon : team) {
            if (this.validateSingle(pokemon)) continue;
            return false;
        }
        return true;
    }
}

