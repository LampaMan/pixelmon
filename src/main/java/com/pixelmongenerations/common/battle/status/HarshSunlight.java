/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Sunny;
import com.pixelmongenerations.common.battle.status.Weather;

public class HarshSunlight
extends Sunny {
    public HarshSunlight() {
        this(-1);
    }

    public HarshSunlight(int turns) {
        super(turns);
    }

    @Override
    protected Weather getNewInstance(int turns) {
        return new HarshSunlight();
    }

    @Override
    protected int getStartTurns(PixelmonWrapper user) {
        return -1;
    }
}

