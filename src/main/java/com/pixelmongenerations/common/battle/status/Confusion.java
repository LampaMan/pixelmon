/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.AttackBase;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.IceFace;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.enums.forms.EnumEiscue;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.Iterator;
import net.minecraft.util.text.TextComponentTranslation;

public class Confusion
extends StatusBase {
    private transient int effectTurns = RandomHelper.getRandomNumberBetween(2, 5);

    public Confusion() {
        super(StatusType.Confusion);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (this.checkChance()) {
            TextComponentTranslation message = ChatHandler.getMessage("pixelmon.effect.becameconfused", target.getNickname());
            if (!target.addStatus(new Confusion(), user, message)) {
                if (user.hasStatus(StatusType.Confusion) && user.attack.getAttackCategory() == AttackCategory.Status) {
                    user.bc.sendToAll("pixelmon.effect.alreadyconfused", target.getNickname());
                    user.attack.moveResult.result = AttackResult.failed;
                }
            }
        }
    }

    @Override
    public boolean stopsStatusChange(StatusType t, PixelmonWrapper target, PixelmonWrapper user) {
        return t.isStatus(StatusType.PartialTrap);
    }

    @Override
    public boolean canAttackThisTurn(PixelmonWrapper user, Attack a) {
        if (--this.effectTurns <= 0) {
            user.removeStatus(this);
            return true;
        }
        user.bc.sendToAll("pixelmon.status.confused", user.getNickname());
        if (RandomHelper.getRandomChance(0.33f)) {
            user.bc.sendToAll("pixelmon.status.confusionhurtself", user.getNickname());
            if (user.getAbility() instanceof IceFace) {
                user.doBattleDamage(user, this.calculateConfusionDamage(user), DamageTypeEnum.STATUS);
                user.setForm(EnumEiscue.Noice.getForm());
                user.bc.sendToAll("pixelmon.abilities.iceface.noice", user.getNickname());
                return false;
            }
            user.doBattleDamage(user, this.calculateConfusionDamage(user), DamageTypeEnum.STATUS);
            return false;
        }
        return true;
    }

    private int calculateConfusionDamage(PixelmonWrapper user) {
        return new Attack(new AttackBase(EnumType.Mystery, 40, AttackCategory.Physical)).doDamageCalc(user, user, 1.0);
    }

    @Override
    public String getCureMessage() {
        return "pixelmon.status.confusionsnap";
    }

    @Override
    public String getCureMessageItem() {
        return "pixelmon.status.confusioncureitem";
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        boolean hitsAlly = userChoice.hitsAlly();
        boolean offensive = userChoice.isOffensiveMove();
        if (!hitsAlly) {
            Iterator<PixelmonWrapper> var9 = userChoice.targets.iterator();
            while (true) {
                if (!var9.hasNext()) {
                    return;
                }
                PixelmonWrapper target = var9.next();
                if (offensive && hitsAlly || !target.addStatus(new Confusion(), pw)) continue;
                userChoice.raiseWeight(this.getWeightWithChance(40.0f));
            }
        }
    }
}

