/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import java.util.ArrayList;

public class Imprison
extends StatusBase {
    transient PixelmonWrapper user;

    public Imprison() {
        super(StatusType.Imprison);
    }

    public Imprison(PixelmonWrapper user) {
        super(StatusType.Imprison);
        this.user = user;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.hasStatus(StatusType.Imprison)) {
            if (user.targetIndex == 0 || user.bc.simulateMode) {
                user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
                user.attack.moveResult.result = AttackResult.failed;
            }
        } else {
            user.bc.sendToAll("pixelmon.status.imprison", user.getNickname());
            target.addTeamStatus(new Imprison(user), user);
        }
    }

    @Override
    public boolean canAttackThisTurn(PixelmonWrapper user, Attack a) {
        if (this.user.getMoveset().hasAttack(a)) {
            user.bc.sendToAll("pixelmon.status.disabled", user.getNickname(), a.getAttackBase().getLocalizedName());
            return false;
        }
        return true;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (this.user.isAlive() && pw.bc.getActivePokemon().contains(this.user)) {
            pw.getMoveset().stream().filter(attack -> this.user.getMoveset().hasAttack((Attack)attack)).forEach(attack -> attack.setDisabled(true, pw));
        } else {
            for (Attack attack2 : pw.getMoveset()) {
                attack2.setDisabled(false, pw);
            }
            pw.removeTeamStatus(this);
        }
    }

    @Override
    public boolean isTeamStatus() {
        return true;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        Moveset moveset = pw.getMoveset();
        for (PixelmonWrapper target : pw.getOpponentPokemon()) {
            for (Attack attack : pw.getBattleAI().getMoveset(target)) {
                if (!moveset.hasAttack(attack)) continue;
                userChoice.raiseWeight(25.0f);
            }
        }
    }
}

