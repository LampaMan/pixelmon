/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public class ShellTrap
extends StatusBase {
    public ShellTrap() {
        super(StatusType.ShellTrap);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        super.applyEffect(user, target);
        user.bc.sendToAll("pixelmon.effect.shelltrapactivated", user.getPokemonName());
        user.addStatus(this, target);
    }
}

