/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Terrain;

public class TerrainRemoval
extends StatusBase {
    public TerrainRemoval() {
        super(StatusType.TerrainRemoval);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        Terrain terrain = user.bc.globalStatusController.getTerrain();
        if (terrain != null) {
            user.bc.globalStatusController.removeGlobalStatus(terrain);
        }
    }
}

