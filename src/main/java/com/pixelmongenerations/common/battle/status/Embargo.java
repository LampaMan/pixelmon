/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.item.ItemHeld;
import java.util.ArrayList;

public class Embargo
extends StatusBase {
    transient int turnsRemaining = 5;

    public Embargo() {
        super(StatusType.Embargo);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.addStatus(new Embargo(), target)) {
            user.bc.sendToAll("pixelmon.status.embargo", target.getNickname());
        } else {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        }
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (--this.turnsRemaining <= 0) {
            pw.bc.sendToAll("pixelmon.status.embargoend", pw.getNickname());
            pw.removeStatus(this);
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        for (PixelmonWrapper target : userChoice.targets) {
            ItemHeld heldItem = target.getUsableHeldItem();
            if (heldItem == null || heldItem.hasNegativeEffect()) continue;
            userChoice.raiseWeight(15.0f);
        }
    }
}

