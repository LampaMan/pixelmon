/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import net.minecraft.util.text.TextComponentTranslation;

public class Infatuated
extends StatusBase {
    transient PixelmonWrapper originalTarget;

    public Infatuated() {
        super(StatusType.Infatuated);
    }

    public Infatuated(PixelmonWrapper originalTarget) {
        this();
        this.originalTarget = originalTarget;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        Infatuated.infatuate(user, target, true);
    }

    public static boolean infatuate(PixelmonWrapper user, PixelmonWrapper target, boolean showMessage) {
        if (!user.getGender().isCompatible(target.getGender())) {
            if (showMessage) {
                if (!user.attack.isAttack("G-Max Cuddle")) {
                    user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
                }
                user.setAttackFailed();
            }
            return false;
        }
        if (target.hasStatus(StatusType.Infatuated)) {
            if (showMessage) {
                user.bc.sendToAll("pixelmon.effect.already", target.getNickname());
                user.setAttackFailed();
            }
            return false;
        }
        TextComponentTranslation message = null;
        if (showMessage) {
            message = ChatHandler.getMessage("pixelmon.effect.falleninlove", target.getNickname());
        }
        return target.addStatus(new Infatuated(user), user, message);
    }

    @Override
    public boolean canAttackThisTurn(PixelmonWrapper user, Attack a) {
        if (!user.bc.isInBattle(this.originalTarget)) {
            user.removeStatus(this);
            return true;
        }
        user.bc.sendToAll("pixelmon.status.inlove", user.getNickname(), this.originalTarget.getNickname());
        if (RandomHelper.getRandomChance(50)) {
            user.bc.sendToAll("pixelmon.status.immobilizedbylove", user.getNickname());
            return false;
        }
        return true;
    }

    @Override
    public String getCureMessage() {
        return "pixelmon.status.infatuatedcure";
    }

    @Override
    public String getCureMessageItem() {
        return "pixelmon.status.infatuatedcureitem";
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (!userChoice.hitsAlly()) {
            userChoice.raiseWeight(40.0f);
        }
    }
}

