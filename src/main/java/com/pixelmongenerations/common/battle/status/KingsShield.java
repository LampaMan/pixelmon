/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Protect;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.LongReach;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.heldItems.ItemProtectivePads;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.enums.forms.EnumAegislash;
import java.util.ArrayList;

public class KingsShield
extends Protect {
    public KingsShield() {
        super(StatusType.KingsShield);
    }

    @Override
    protected boolean addStatus(PixelmonWrapper user) {
        if (user.getSpecies() == EnumSpecies.Aegislash) {
            user.setForm(EnumAegislash.Shield.getForm());
            user.bc.sendToAll("pixelmon.abilities.stancechangetoshield", user.getNickname());
            user.bc.modifyStats();
        }
        return user.addStatus(new KingsShield(), user);
    }

    @Override
    public boolean stopsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user) {
        if (user.isDynamaxed()) {
            return false;
        }
        return user.attack.getAttackCategory() != AttackCategory.Status && super.stopsIncomingAttack(pokemon, user);
    }

    @Override
    public void stopsIncomingAttackMessage(PixelmonWrapper pokemon, PixelmonWrapper user) {
        super.stopsIncomingAttackMessage(pokemon, user);
        if (user.attack.getAttackBase().getMakesContact()) {
            if (user.hasHeldItem() && user.getHeldItem() instanceof ItemProtectivePads) {
                user.bc.sendToAll("pixelmon.effect.protectivepads", user.getNickname());
                return;
            }
            if (user.getBattleAbility() instanceof LongReach) {
                return;
            }
            user.getBattleStats().modifyStat(-1, StatsType.Attack, user, false);
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        super.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
        if (userChoice.weight != -1.0f) {
            StatsEffect statsEffect = new StatsEffect(StatsType.Attack, -2, false);
            block0: for (ArrayList<MoveChoice> choices : MoveChoice.splitChoices(pw.getOpponentPokemon(), bestOpponentChoices)) {
                for (MoveChoice choice : choices) {
                    if (!choice.isAttack() || !choice.attack.getAttackBase().getMakesContact()) continue;
                    ArrayList<PixelmonWrapper> saveTargets = userChoice.targets;
                    ArrayList<PixelmonWrapper> newTargets = new ArrayList<PixelmonWrapper>();
                    newTargets.add(choice.user);
                    userChoice.targets = newTargets;
                    statsEffect.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
                    userChoice.targets = saveTargets;
                    continue block0;
                }
            }
        }
    }
}

