/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusPersist;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Guts;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MarvelScale;
import com.pixelmongenerations.common.entity.pixelmon.abilities.QuickFeet;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Synchronize;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import net.minecraft.nbt.NBTTagCompound;

public class Paralysis
extends StatusPersist {
    public Paralysis() {
        super(StatusType.Paralysis);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (this.checkChance()) {
            Paralysis.paralyze(user, target, user.attack, true);
        }
    }

    public static boolean paralyze(PixelmonWrapper user, PixelmonWrapper target, Attack attack, boolean showMessage) {
        return new Paralysis().addStatus(user, target, attack, showMessage, "pixelmon.effect.alreadyparalyzed", "pixelmon.effect.isparalyzed");
    }

    @Override
    public boolean canAttackThisTurn(PixelmonWrapper user, Attack a) {
        if (RandomHelper.getRandomChance(25)) {
            user.bc.sendToAll("pixelmon.status.isparalyzed", user.getNickname());
            return false;
        }
        return true;
    }

    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        if (!(user.getBattleAbility() instanceof QuickFeet)) {
            int statIndex = StatsType.Speed.getStatIndex();
            stats[statIndex] = (int)((double)stats[statIndex] * 0.505);
        }
        return stats;
    }

    @Override
    public StatusPersist restoreFromNBT(NBTTagCompound nbt) {
        return new Paralysis();
    }

    @Override
    public boolean isImmune(PixelmonWrapper pokemon) {
        return pokemon.hasType(EnumType.Electric);
    }

    @Override
    public String getCureMessage() {
        return "pixelmon.status.paralysiscure";
    }

    @Override
    public String getCureMessageItem() {
        return "pixelmon.status.paralysiscureitem";
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (!userChoice.hitsAlly()) {
            float weight = this.getWeightWithChance(25.0f);
            if (userChoice.isMiddleTier()) {
                for (PixelmonWrapper target : userChoice.targets) {
                    AbilityBase ability = target.getBattleAbility();
                    if (ability instanceof Guts || ability instanceof MarvelScale || ability instanceof QuickFeet || ability instanceof Synchronize) {
                        return;
                    }
                    if (!MoveChoice.hasPriority(bestOpponentChoices) && MoveChoice.canOutspeed(bestOpponentChoices, pw, bestUserChoices)) {
                        userChoice.raiseWeight(weight * 3.0f);
                    }
                    userChoice.raiseWeight(weight);
                }
            }
        }
    }
}

