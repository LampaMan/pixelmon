/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.EntryHazard;
import com.pixelmongenerations.common.battle.status.Poison;
import com.pixelmongenerations.common.battle.status.PoisonBadly;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.item.heldItems.ItemHeavyDutyBoots;
import com.pixelmongenerations.core.enums.EnumType;

public class ToxicSpikes
extends EntryHazard {
    public ToxicSpikes() {
        super(StatusType.ToxicSpikes, 2);
    }

    @Override
    public boolean isUnharmed(PixelmonWrapper pw) {
        if (pw.hasType(EnumType.Steel)) {
            return true;
        }
        if (this.isAirborne(pw)) {
            return true;
        }
        return pw.getHeldItem() instanceof ItemHeavyDutyBoots;
    }

    @Override
    public void applyEffectOnSwitch(PixelmonWrapper pw) {
        if (!this.isUnharmed(pw)) {
            this.doEffect(pw);
        }
    }

    @Override
    protected void doEffect(PixelmonWrapper pw) {
        if (pw.hasPrimaryStatus()) {
            return;
        }
        if (pw.hasType(EnumType.Poison)) {
            pw.bc.sendToAll("pixelmon.status.toxicspikesabsorbed", pw.getNickname());
            pw.removeTeamStatus(this);
            return;
        }
        Poison poison = this.numLayers == 1 ? new Poison() : new PoisonBadly();
        Poison poison2 = poison;
        if (pw.addStatus(poison, pw)) {
            pw.bc.sendToAll(this.getAffectedMessage(), pw.getNickname());
        }
    }

    @Override
    public EntryHazard getNewInstance() {
        return new ToxicSpikes();
    }

    @Override
    protected String getFirstLayerMessage() {
        return "pixelmon.effect.toxicspikes";
    }

    @Override
    protected String getMultiLayerMessage() {
        return "pixelmon.effect.moretoxicspikes";
    }

    @Override
    protected String getAffectedMessage() {
        return this.numLayers == 1 ? "pixelmon.status.toxicspikespoisoned" : "pixelmon.status.toxicspikesbadlypoisoned";
    }

    @Override
    public int getAIWeight() {
        return 15;
    }
}

