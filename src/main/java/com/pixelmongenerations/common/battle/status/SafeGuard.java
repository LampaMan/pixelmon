/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Infiltrator;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import java.util.ArrayList;

public class SafeGuard
extends StatusBase {
    transient int effectTurns = 5;

    public SafeGuard() {
        super(StatusType.SafeGuard);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.targetIndex == 0 || user.bc.simulateMode) {
            if (user.hasStatus(this.type)) {
                if (user == target) {
                    user.bc.sendToAll("pixelmon.effect.alreadysafeguard", user.getNickname());
                }
                user.attack.moveResult.result = AttackResult.failed;
                return;
            }
            if (user.addTeamStatus(new SafeGuard(), user)) {
                user.bc.sendToAll("pixelmon.effect.guarded", user.getNickname());
            }
        }
    }

    @Override
    public boolean stopsStatusChange(StatusType t, PixelmonWrapper target, PixelmonWrapper user) {
        block6: {
            block5: {
                if (t.isPrimaryStatus()) break block5;
                if (!t.isStatus(StatusType.Confusion, StatusType.Yawn)) break block6;
            }
            if (!(user.getBattleAbility() instanceof Infiltrator)) {
                if (user != target && user.attack != null && user.attack.getAttackCategory() == AttackCategory.Status) {
                    target.bc.sendToAll("pixelmon.status.safeguard", target.getNickname());
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (--this.effectTurns <= 0) {
            pw.bc.sendToAll("pixelmon.status.safeguardoff", pw.getNickname());
            pw.removeTeamStatus(this);
        }
    }

    @Override
    public boolean isTeamStatus() {
        return true;
    }

    @Override
    public StatusBase copy() {
        SafeGuard copy = new SafeGuard();
        copy.effectTurns = this.effectTurns;
        return copy;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        for (PixelmonWrapper opponent : pw.getOpponentPokemon()) {
            if (!(opponent.getBattleAbility() instanceof Infiltrator)) continue;
            return;
        }
        userChoice.raiseWeight(15.0f);
    }
}

