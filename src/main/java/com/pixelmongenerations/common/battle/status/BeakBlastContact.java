/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Burn;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.LongReach;
import com.pixelmongenerations.common.item.heldItems.ItemProtectivePads;

public class BeakBlastContact
extends StatusBase {
    public BeakBlastContact() {
        super(StatusType.BeakBlastContact);
    }

    @Override
    public void onDamageReceived(PixelmonWrapper userWrapper, PixelmonWrapper pokemon, Attack attack, int damage, DamageTypeEnum damageType) {
        if (attack.getAttackBase().getMakesContact() && !pokemon.hasPrimaryStatus()) {
            if (pokemon.hasHeldItem() && pokemon.getHeldItem() instanceof ItemProtectivePads) {
                pokemon.bc.sendToAll("pixelmon.effect.protectivepads", pokemon.getNickname());
                pokemon.removeStatus(StatusType.BeakBlastContact);
                return;
            }
            if (pokemon.getBattleAbility() instanceof LongReach) {
                pokemon.removeStatus(StatusType.BeakBlastContact);
                return;
            }
            userWrapper.addStatus(new Burn(), userWrapper);
            userWrapper.bc.sendToAll("pixelmon.effect.burnt", userWrapper.getNickname());
            pokemon.removeStatus(StatusType.BeakBlastContact);
        }
    }

    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (!user.hasStatus(StatusType.BeakBlastContact)) {
            user.addStatus(new BeakBlastContact(), user);
        }
        return AttackResult.proceed;
    }

    @Override
    public void applyEarlyEffect(PixelmonWrapper user) {
        if (!user.hasStatus(StatusType.BeakBlastContact)) {
            user.addStatus(new BeakBlastContact(), user);
        }
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (!user.hasStatus(StatusType.BeakBlastContact)) {
            user.addStatus(new BeakBlastContact(), user);
        }
    }

    @Override
    public boolean cantMiss(PixelmonWrapper pokemon) {
        return false;
    }

    @Override
    public void onAttackEnd(PixelmonWrapper pw) {
        pw.removeStatus(StatusType.BeakBlastContact);
    }
}

