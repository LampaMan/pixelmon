/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.PledgeBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public class FirePledge
extends PledgeBase {
    public FirePledge() {
        super(StatusType.FirePledge);
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        super.applyRepeatedEffect(pw);
        if (this.remainingTurns > 0 && !pw.bc.globalStatusController.hasStatus(StatusType.Rainy)) {
            pw.bc.sendToAll("pixelmon.status.firepledgeeffect", pw.getNickname());
            pw.doBattleDamage(pw, pw.getPercentMaxHealth(12.5f), DamageTypeEnum.STATUS);
        }
    }
}

