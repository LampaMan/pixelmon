/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.EntryHazard;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;
import com.pixelmongenerations.common.item.heldItems.ItemHeavyDutyBoots;

public class Spikes
extends EntryHazard {
    public Spikes() {
        super(StatusType.Spikes, 3);
    }

    @Override
    public boolean isUnharmed(PixelmonWrapper pw) {
        if (this.isAirborne(pw)) {
            return true;
        }
        if (pw.getHeldItem() instanceof ItemHeavyDutyBoots) {
            return true;
        }
        return pw.getBattleAbility() instanceof MagicGuard;
    }

    @Override
    public int getDamage(PixelmonWrapper pw) {
        return pw.getPercentMaxHealth(100.0f / (float)(8 - (this.numLayers - 1) * 2));
    }

    @Override
    protected String getFirstLayerMessage() {
        return "pixelmon.effect.spikes";
    }

    @Override
    protected String getMultiLayerMessage() {
        return "pixelmon.effect.morespikes";
    }

    @Override
    protected String getAffectedMessage() {
        return "pixelmon.status.hurtbyspikes";
    }

    @Override
    public int getAIWeight() {
        return 20;
    }

    @Override
    public EntryHazard getNewInstance() {
        return new Spikes();
    }
}

