/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;

public class TempMoveset
extends StatusBase {
    transient Moveset moveset;

    public TempMoveset(Moveset newMoveset) {
        super(StatusType.TempMoveset);
        this.moveset = newMoveset;
    }

    public Moveset getMoveset() {
        return this.moveset;
    }
}

