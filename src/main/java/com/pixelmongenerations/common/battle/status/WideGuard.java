/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.ProtectVariationTeam;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import java.util.ArrayList;

public class WideGuard
extends ProtectVariationTeam {
    public WideGuard() {
        super(StatusType.WideGuard);
    }

    @Override
    public ProtectVariationTeam getNewInstance() {
        return new WideGuard();
    }

    @Override
    protected void displayMessage(PixelmonWrapper user) {
        user.bc.sendToAll("pixelmon.status.wideguard", user.getNickname());
    }

    @Override
    public boolean stopsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user) {
        if (user.isDynamaxed()) {
            return false;
        }
        return super.stopsIncomingAttack(pokemon, user) && user.attack.getAttackCategory() != AttackCategory.Status && user.attack.getAttackBase().targetingInfo.hitsAll && user.attack.getAttackBase().targetingInfo.hitsAdjacentFoe;
    }

    @Override
    public void stopsIncomingAttackMessage(PixelmonWrapper pokemon, PixelmonWrapper user) {
        user.bc.sendToAll("pixelmon.status.wideguardprotect", pokemon.getNickname());
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (MoveChoice.canBreakProtect(pw.getOpponentPokemon(), bestOpponentChoices)) {
            return;
        }
        if (pw.bc.rules.battleType.numPokemon > 1 && MoveChoice.hasSpreadMove(bestOpponentChoices)) {
            userChoice.raiseWeight(75.0f);
        }
    }
}

