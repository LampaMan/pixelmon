/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public class LunarDance
extends StatusBase {
    public LunarDance() {
        super(StatusType.LunarDance);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.getParticipant().hasMorePokemonReserve()) {
            user.addStatus(new LunarDance(), user);
            user.doBattleDamage(user, user.getHealth(), DamageTypeEnum.SELF);
        } else {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        }
    }

    @Override
    public boolean isTeamStatus() {
        return true;
    }

    @Override
    public boolean isWholeTeamStatus() {
        return false;
    }

    @Override
    public void applyEffectOnSwitch(PixelmonWrapper pw) {
        pw.bc.sendToAll("pixelmon.effect.lunardance", pw.getNickname());
        boolean didHeal = false;
        if (pw.getMaxHealth() > pw.getHealth()) {
            pw.healEntityBy(pw.getHealthDeficit());
            didHeal = true;
        }
        for (Attack attack : pw.getMoveset()) {
            if (attack == null || attack.pp >= attack.ppBase) continue;
            attack.pp = attack.ppBase;
            didHeal = true;
        }
        for (int i = 0; i < pw.getStatusSize(); ++i) {
            StatusType currentStatus = pw.getStatus((int)i).type;
            if (!currentStatus.isPrimaryStatus() && currentStatus != StatusType.LunarDance) continue;
            if (currentStatus != StatusType.LunarDance) {
                didHeal = true;
            }
            pw.removeStatus(i);
            --i;
        }
        if (!didHeal) {
            pw.bc.sendToAll("pixelmon.effect.healfailed", pw.getNickname());
        }
    }
}

