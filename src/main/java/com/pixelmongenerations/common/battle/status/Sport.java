/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.AttackBase;
import com.pixelmongenerations.common.battle.controller.GlobalStatusController;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.ArrayList;

public abstract class Sport
extends GlobalStatusBase {
    protected transient PixelmonWrapper user;
    protected transient EnumType affectedType;
    private transient int turnsLeft;
    private transient String moveName;

    public Sport(PixelmonWrapper user, StatusType status, EnumType affectedType, String moveName) {
        super(status);
        this.user = user;
        this.affectedType = affectedType;
        this.turnsLeft = 5;
        this.moveName = moveName;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.globalStatusController.hasStatus(this.type)) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        } else {
            user.bc.sendToAll("pixelmon.status.sport", this.affectedType.getLocalizedName());
            user.bc.globalStatusController.addGlobalStatus(this.getNewInstance(user));
        }
    }

    protected abstract Sport getNewInstance(PixelmonWrapper var1);

    @Override
    public int[] modifyPowerAndAccuracyTarget(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (a.getAttackBase().attackType == this.affectedType) {
            power /= 3;
        }
        return new int[]{power, accuracy};
    }

    @Override
    public void applyRepeatedEffect(GlobalStatusController gsc) {
        if (--this.turnsLeft <= 0) {
            gsc.removeGlobalStatus(this);
            gsc.bc.sendToAll("pixelmon.status.sportfade", AttackBase.getLocalizedName(this.moveName));
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (MoveChoice.hasOffensiveAttackType(bestOpponentChoices, this.affectedType)) {
            userChoice.raiseWeight(20.0f);
        }
        if (MoveChoice.hasOffensiveAttackType(bestUserChoices, this.affectedType)) {
            userChoice.raiseWeight(-20.0f);
        }
    }
}

