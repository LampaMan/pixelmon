/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.LiquidOoze;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.ArrayList;

public class Leech
extends StatusBase {
    transient PixelmonWrapper leechRecipient;
    transient int recipientPosition;

    public Leech() {
        super(StatusType.Leech);
    }

    public Leech(PixelmonWrapper leechRecipient) {
        super(StatusType.Leech);
        this.leechRecipient = leechRecipient;
        this.recipientPosition = leechRecipient.bc.getPositionOfPokemon(leechRecipient);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.hasStatus(StatusType.Leech)) {
            user.bc.sendToAll("pixelmon.effect.alreadyseeded", target.getNickname());
            user.attack.moveResult.result = AttackResult.failed;
            return;
        }
        if (!target.hasType(EnumType.Grass)) {
            if (target.addStatus(new Leech(user), user)) {
                user.bc.sendToAll("pixelmon.effect.seedplanted", user.getNickname());
            }
        } else {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        }
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        this.leechRecipient = pw.bc.getPokemonAtPosition(this.recipientPosition);
        AbilityBase victimAbility = pw.getBattleAbility();
        if (this.leechRecipient.isFainted() || victimAbility instanceof MagicGuard) {
            return;
        }
        pw.bc.sendToAll("pixelmon.status.drainhealth", this.leechRecipient.getNickname(), pw.getNickname());
        int dmg = pw.getPercentMaxHealth(12.5f);
        pw.doBattleDamage(this.leechRecipient, dmg, DamageTypeEnum.STATUS);
        if (ItemHeld.canUseItem(this.leechRecipient) && this.leechRecipient.getUsableHeldItem().getHeldItemType() == EnumHeldItems.bigRoot) {
            dmg = (int)((double)dmg * 1.3);
        }
        if (victimAbility instanceof LiquidOoze) {
            this.leechRecipient.bc.sendToAll("pixelmon.abilities.liquidooze", this.leechRecipient.getNickname());
            this.leechRecipient.doBattleDamage(pw, dmg, DamageTypeEnum.ABILITY);
        } else if (!this.leechRecipient.hasStatus(StatusType.HealBlock)) {
            this.leechRecipient.healEntityBy(dmg);
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.hitsAlly()) {
            return;
        }
        for (PixelmonWrapper target : userChoice.targets) {
            AbilityBase targetAbility = target.getBattleAbility();
            if (targetAbility instanceof LiquidOoze || targetAbility instanceof MagicGuard) continue;
            userChoice.raiseWeight(35.0f);
        }
    }
}

