/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Hail;
import com.pixelmongenerations.common.battle.status.ProtectVariation;
import com.pixelmongenerations.common.battle.status.Rainy;
import com.pixelmongenerations.common.battle.status.Sandstorm;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Sunny;
import com.pixelmongenerations.common.entity.pixelmon.abilities.*;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.ArrayList;

public class Protect
extends ProtectVariation {
    public Protect() {
        this(StatusType.Protect);
    }

    public Protect(StatusType type) {
        super(type);
    }

    @Override
    protected boolean addStatus(PixelmonWrapper user) {
        return user.addStatus(new Protect(), user);
    }

    @Override
    protected void displayMessage(PixelmonWrapper user) {
        user.bc.sendToAll("pixelmon.effect.redaying", user.getNickname());
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    @Override
    public boolean stopsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user) {
        if (user.isDynamaxed()) {
            return false;
        }
        if (user.getBattleAbility() instanceof UnseenFist && user.attack.getAttackBase().getMakesContact()) {
            user.bc.sendToAll("pixelmon.abilities.unseenfist", user.getNickname());
            return false;
        }
        if (!super.stopsIncomingAttack(pokemon, user)) return false;
        if (user.attack.isAttack("Acupressure", "Aromatic Mist", "Confide", "Conversion 2", "Curse", "Doom Desire", "Decorate", "Future Sight", "Hold Hands", "Play Nice", "Psych Up", "Roar", "Role Play", "Sketch", "Spikes", "Stealth Rock", "Toxic Spikes", "Transform", "Whirlwind", "Savage Spin-Out", "Black Hole Eclipse", "Devastating Drake", "Gigavolt Havoc", "Twinkle Tackle", "All-Out Pummeling", "Inferno Overdrive", "Supersonic Skystrike", "Never-Ending Nightmare", "Bloom Doom", "Tectonic Rage", "Subzero Slammer", "Breakneck Blitz", "Acid Downpour", "Shattered Psyche", "Continental Crush", "Corkscrew Crash", "Hydro Vortex", "Stroked Sparksurfer", "Sinister Arrow Raid", "Malicious Moonsault", "Clangorous Soulblaze", "Menacing Moonraze Maelstrom", "Splintered Stormshards", "Soul-Stealing 7-Star Strike", "Genesis Supernova", "Let's Snuggle Forever", "Catastropika", "10,000,000 Volt Thunderbolt", "Oceanic Operetta", "Pulverizing Pancake", "Searing Sunraze Smash", "Guardian of Alola", "Light That Burns The Sky")) return false;
        return true;
    }

    @Override
    public int[] modifyPowerAndAccuracyTarget(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (a.isAttack("Savage Spin-Out", "Black Hole Eclipse", "Devastating Drake", "Gigavolt Havoc", "Twinkle Tackle", "All-Out Pummeling", "Inferno Overdrive", "Supersonic Skystrike", "Never-Ending Nightmare", "Bloom Doom", "Tectonic Rage", "Subzero Slammer", "Breakneck Blitz", "Acid Downpour", "Shattered Psyche", "Continental Crush", "Corkscrew Crash", "Hydro Vortex", "Stroked Sparksurfer", "Sinister Arrow Raid", "Malicious Moonsault", "Clangorous Soulblaze", "Menacing Moonraze Maelstrom", "Splintered Stormshards", "Soul-Stealing 7-Star Strike", "Genesis Supernova", "Let's Snuggle Forever", "Catastropika", "10,000,000 Volt Thunderbolt", "Oceanic Operetta", "Pulverizing Pancake", "Searing Sunraze Smash", "Guardian of Alola", "Light That Burns The Sky")) {
            power = (int)((double)power * 0.25);
        } else if (user.isDynamaxed()) {
            if (!a.isAttack("G-Max Rapid Flow", "G-Max One Blow")) {
                power = (int)((double)power * 0.25);
            }
        }
        return new int[]{power, accuracy};
    }

    @Override
    public void stopsIncomingAttackMessage(PixelmonWrapper pokemon, PixelmonWrapper user) {
        user.bc.sendToAll("pixelmon.effect.redaying", pokemon.getNickname());
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        pw.removeStatus(this);
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (MoveChoice.canBreakProtect(pw.getOpponentPokemon(), bestOpponentChoices)) {
            userChoice.raiseWeight(-1.0f);
            return;
        }
        int numResidualUser = this.countStallBenefit(pw);
        int numResidualOpponent = 0;
        for (PixelmonWrapper opponent : pw.getOpponentPokemon()) {
            if (opponent.hasStatus(StatusType.Vanish)) {
                userChoice.raiseWeight(-1.0f);
                return;
            }
            numResidualOpponent += this.countStallBenefit(opponent);
        }
        if (numResidualUser < numResidualOpponent) {
            userChoice.raiseWeight(50.0f);
        }
        ArrayList<MoveChoice> targetedChoices = MoveChoice.getTargetedChoices(pw, bestOpponentChoices);
        if (MoveChoice.hasSuccessfulAttackChoice(targetedChoices, "Explosion", "Fake Out", "Jump Kick", "Hi Jump Kick", "Selfdestruct")) {
            userChoice.raiseWeight(50.0f);
        }
        if (pw.bc.rules.battleType.numPokemon > 1 && targetedChoices.size() > 0) {
            userChoice.raiseWeight(50.0f);
        }
        userChoice.weight /= (float)(1 << pw.protectsInARow);
    }

    private int countStallBenefit(PixelmonWrapper pw) {
        AbilityBase ability = pw.getBattleAbility();
        boolean hasMagicGuard = ability instanceof MagicGuard;
        EnumHeldItems heldItem = pw.getUsableHeldItem().getHeldItemType();
        Weather weather = pw.bc.globalStatusController.getWeather();
        boolean fullHealth = pw.hasFullHealth();
        int numResidual = 0;
        numResidual += pw.countStatuses(new StatusType[]{StatusType.FutureSight, StatusType.LightScreen, StatusType.LuckyChant, StatusType.MagnetRise, StatusType.MultiTurn, StatusType.Mist, StatusType.Nightmare, StatusType.Perish, StatusType.Reflect, StatusType.SafeGuard, StatusType.Tailwind, StatusType.UnderGround, StatusType.Yawn});
        numResidual -= pw.countStatuses(new StatusType[]{StatusType.AquaRing, StatusType.Disable, StatusType.Embargo, StatusType.Encore, StatusType.Freeze, StatusType.GrassPledge, StatusType.HealBlock, StatusType.Ingrain, StatusType.Sleep, StatusType.Taunt, StatusType.Telekinesis, StatusType.WaterPledge, StatusType.Wish});
        if (!hasMagicGuard) {
            numResidual += pw.countStatuses(new StatusType[]{StatusType.Burn, StatusType.Cursed, StatusType.FirePledge, StatusType.Leech, StatusType.PartialTrap});
            int numPoison = pw.countStatuses(new StatusType[]{StatusType.Poison, StatusType.PoisonBadly});
            if (ability instanceof PoisonHeal) {
                if (!fullHealth) {
                    numResidual -= numPoison;
                }
            } else {
                numResidual += numPoison;
            }
        }

        if (!fullHealth) {
            numResidual -= pw.countStatuses(new StatusType[]{StatusType.AquaRing, StatusType.Ingrain, StatusType.Wish});
        }
        if (!fullHealth && heldItem == EnumHeldItems.leftovers || heldItem == EnumHeldItems.blackSludge && pw.hasType(EnumType.Poison)) {
            --numResidual;
        } else if (!hasMagicGuard && (heldItem == EnumHeldItems.stickyBarb || heldItem == EnumHeldItems.blackSludge)) {
            ++numResidual;
        }

        if (weather instanceof Rainy) {
            if (!fullHealth && (ability instanceof DrySkin || ability instanceof RainDish)) {
                --numResidual;
            }
        } else if (weather instanceof Sunny) {
            if (ability instanceof DrySkin || ability instanceof SolarPower) {
                ++numResidual;
            }
        } else if (weather instanceof Sandstorm) {
            if (!weather.isImmune(pw)) {
                ++numResidual;
            }
        } else if (weather instanceof Hail) {
            if (!fullHealth && ability instanceof IceBody) {
                --numResidual;
            } else if (!weather.isImmune(pw)) {
                ++numResidual;
            }
        }
        if (ability instanceof Moody || ability instanceof SpeedBoost || ability instanceof SlowStart) return --numResidual;
        if (ability instanceof Truant == false) return numResidual;
        if (((Truant)ability).canMove == false) return numResidual;
        ++numResidual;
        return numResidual;
    }
}

