/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Poison;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusPersist;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.network.ChatHandler;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.TextComponentTranslation;

public class PoisonBadly
extends Poison {
    private transient int poisonSeverity = 1;

    public PoisonBadly() {
        super(StatusType.PoisonBadly);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (this.checkChance()) {
            PoisonBadly.poisonBadly(user, target, user.attack, true);
        }
    }

    public static boolean poisonBadly(PixelmonWrapper user, PixelmonWrapper target, Attack attack, boolean showMessage) {
        if (PoisonBadly.canPoison(user, target, attack, showMessage)) {
            boolean result;
            TextComponentTranslation message = null;
            if (showMessage) {
                message = ChatHandler.getMessage("pixelmon.effect.badlypoisoned", target.getNickname());
            }
            if (!(result = target.addStatus(new PoisonBadly(), user, message)) && attack != null && attack.getAttackCategory() == AttackCategory.Status) {
                user.setAttackFailed();
            }
            return result;
        }
        return false;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        super.applyRepeatedEffect(pw);
        ++this.poisonSeverity;
    }

    @Override
    protected float getPoisonDamage(PixelmonWrapper pw) {
        return pw.getPercentMaxHealth((float)this.poisonSeverity * 100.0f / 16.0f);
    }

    @Override
    public void applySwitchOutEffect(PixelmonWrapper pw) {
        this.poisonSeverity = 1;
    }

    @Override
    public void applyEndOfBattleEffect(PixelmonWrapper pokemon) {
        int index = pokemon.getStatusIndex(StatusType.PoisonBadly);
        if (index >= 0) {
            pokemon.setStatus(index, new Poison());
        }
    }

    @Override
    public StatusPersist restoreFromNBT(NBTTagCompound nbt) {
        return new PoisonBadly();
    }

    @Override
    public StatusBase copy() {
        return new PoisonBadly();
    }
}

