/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.network.ChatHandler;
import java.util.ArrayList;
import net.minecraft.util.text.TextComponentTranslation;

public class Torment
extends StatusBase {
    transient Attack lastAttack;

    public Torment() {
        super(StatusType.Torment);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.hasStatus(this.type) || target.isDynamaxed()) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        } else {
            TextComponentTranslation message = ChatHandler.getMessage("pixelmon.status.torment", target.getNickname());
            target.addStatus(new Torment(), user, message);
        }
    }

    @Override
    public void onAttackUsed(PixelmonWrapper user, Attack attack) {
        this.lastAttack = attack;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        for (Attack attack : pw.getMoveset().attacks) {
            if (attack == null || !attack.equals(this.lastAttack)) continue;
            attack.setDisabled(true, pw);
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (MoveChoice.getAffectedChoices(userChoice, bestOpponentChoices).size() <= 1) {
            userChoice.raiseWeight(20.0f);
        }
    }
}

