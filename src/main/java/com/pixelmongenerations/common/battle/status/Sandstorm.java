/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Weather;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Overcoat;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SandForce;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SandRush;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SandVeil;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.List;

public class Sandstorm
extends Weather {
    public Sandstorm() {
        this(5);
    }

    public Sandstorm(int turnsToGo) {
        super(StatusType.Sandstorm, turnsToGo, EnumHeldItems.smoothRock, "pixelmon.effect.sandstorm", "pixelmon.status.sandstormrage", "pixelmon.status.sandstormsubside");
    }

    @Override
    protected Weather getNewInstance(int turns) {
        return new Sandstorm(turns);
    }

    @Override
    public void applyRepeatedEffect(BattleControllerBase bc) {
        for (PixelmonWrapper p : bc.getDefaultTurnOrder()) {
            if (this.isImmune(p)) continue;
            p.bc.sendToAll("pixelmon.status.buffetedbysandstorm", p.getNickname());
            p.doBattleDamage(p, p.getPercentMaxHealth(6.25f), DamageTypeEnum.WEATHER);
        }
    }

    @Override
    public boolean isImmune(PixelmonWrapper p) {
        AbilityBase ability = p.getBattleAbility();
        return p.hasType(EnumType.Ground, EnumType.Rock, EnumType.Steel) || p.isFainted() || ability instanceof MagicGuard || ability instanceof Overcoat || ability instanceof SandForce || ability instanceof SandRush || ability instanceof SandVeil || p.getUsableHeldItem().getHeldItemType() == EnumHeldItems.safetyGoggles;
    }

    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        if (user.hasType(EnumType.Rock)) {
            int[] arrn = stats;
            int n = StatsType.SpecialDefence.getStatIndex();
            arrn[n] = (int)((double)arrn[n] * 1.5);
        }
        return stats;
    }

    @Override
    protected int countBenefits(PixelmonWrapper user, PixelmonWrapper target) {
        int benefits = 0;
        if (!this.isImmune(target)) {
            --benefits;
        } else if (target.hasType(EnumType.Rock)) {
            ++benefits;
        }
        AbilityBase ability = target.getBattleAbility();
        if (ability instanceof SandForce || ability instanceof SandRush || ability instanceof SandVeil) {
            ++benefits;
        }
        List<Attack> moveset = user.getBattleAI().getMoveset(target);
        if (Attack.hasAttack(moveset, "Weather Ball")) {
            ++benefits;
        }
        if (Attack.hasAttack(moveset, "Solar Beam")) {
            --benefits;
        }
        if (Attack.hasAttack(moveset, "Moonlight", "Morning Sun", "Synthesis")) {
            --benefits;
        }
        return benefits;
    }
}

