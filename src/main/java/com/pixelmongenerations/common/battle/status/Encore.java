/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.network.ChatHandler;
import java.util.ArrayList;
import net.minecraft.util.text.TextComponentTranslation;

public class Encore
extends StatusBase {
    transient Attack attack;
    transient int turns;

    public Encore() {
        super(StatusType.Encore);
    }

    public Encore(Attack attack) {
        super(StatusType.Encore);
        this.attack = attack;
        this.turns = 3;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        block5: {
            block4: {
                Attack lastAttack = target.lastAttack;
                TextComponentTranslation message = ChatHandler.getMessage("pixelmon.status.encore", target.getNickname());
                if (lastAttack == null || lastAttack.pp <= 0) break block4;
                if (!lastAttack.isAttack("Encore", "Mimic", "Mirror Move", "Sketch", "Struggle", "Transform") && target.addStatus(new Encore(lastAttack), target, message) && !target.isDynamaxed()) break block5;
            }
            if (target.hasStatus(StatusType.Disable)) {
                user.attack.moveResult.result = AttackResult.failed;
            }
            target.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        }
    }

    @Override
    public boolean canAttackThisTurn(PixelmonWrapper user, Attack a) {
        if (!user.attack.equals(this.attack)) {
            if (!a.isAttack("Struggle")) {
                user.attack = this.attack;
                user.targets = user.getTargets(this.attack);
            }
        }
        return true;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (--this.turns <= 0) {
            pw.bc.sendToAll("pixelmon.status.encoreend", pw.getNickname());
            pw.removeStatus(this);
            return;
        }
        for (Attack attack : pw.getMoveset()) {
            if (attack == null || attack.equals(this.attack)) continue;
            attack.setDisabled(true, pw);
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        for (PixelmonWrapper target : userChoice.targets) {
            if (target.hasStatus(StatusType.Encore) || MoveChoice.canOutspeed(bestOpponentChoices, pw, userChoice.createList()) || target.lastAttack == null || target.lastAttack.getAttackCategory() != AttackCategory.Status) continue;
            userChoice.raiseWeight(100.0f);
        }
    }
}

