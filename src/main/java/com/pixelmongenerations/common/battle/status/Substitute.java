/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Infiltrator;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Soundproof;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import java.util.ArrayList;

public class Substitute
extends StatusBase {
    public transient int health;

    public Substitute() {
        super(StatusType.Substitute);
    }

    public Substitute(int health) {
        super(StatusType.Substitute);
        this.health = health;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        float health = user.getPercentMaxHealth(25.0f);
        if (!user.hasStatus(this.type) && (float)user.getHealth() > health) {
            if (user.addStatus(new Substitute((int)health), user)) {
                user.doBattleDamage(user, health, DamageTypeEnum.SELF);
                user.bc.sendToAll("pixelmon.effect.createsubstitute", user.getNickname());
            }
        } else {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        }
    }

    @Override
    public boolean stopsStatusChange(StatusType t, PixelmonWrapper target, PixelmonWrapper user) {
        if (user != target) {
            if (!t.isStatus(StatusType.Cursed, StatusType.Disable, StatusType.Encore, StatusType.FirePledge, StatusType.FutureSight, StatusType.GrassPledge, StatusType.HealBlock, StatusType.Imprison, StatusType.Infatuated, StatusType.Perish, StatusType.Spikes, StatusType.StealthRock, StatusType.Steelsurge, StatusType.Taunt, StatusType.Torment, StatusType.ToxicSpikes, StatusType.WaterPledge, StatusType.StickyWeb) && !this.ignoreSubstitute(user)) {
                if (user.attack != null) {
                    if (!user.attack.getAttackBase().targetingInfo.hitsAdjacentFoe) {
                        return false;
                    }
                    if (user.attack.getAttackCategory() == AttackCategory.Status) {
                        target.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
                        user.attack.moveResult.result = AttackResult.failed;
                    }
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean stopsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user) {
        if (user.attack.getAttackCategory() == AttackCategory.Status && user.attack.getAttackBase().targetingInfo.hitsAdjacentFoe && !this.ignoreSubstitute(user)) {
            if (user.attack.isAttack("Attract", "Conversion 2", "Curse", "Defog", "Destiny Bond", "Disable", "Encore", "Foresight", "Grudge", "Guard Swap", "Haze", "Heal Block", "Heart Swap", "Imprison", "Miracle Eye", "Odor Sleuth", "Perish Song", "Psych Up", "Roar", "Role Play", "Skill Swap", "Spikes", "Spite", "Stealth Rock", "Taunt", "Torment", "Toxic", "Toxic Spikes", "Whirlwind", "Sticky Web")) {
                return false;
            }
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return true;
        }
        return false;
    }

    @Override
    public boolean allowsStatChange(PixelmonWrapper pokemon, PixelmonWrapper user, StatsEffect e) {
        return pokemon == user;
    }

    @Override
    public void onAttackEnd(PixelmonWrapper pw) {
        if (this.health <= 0) {
            pw.removeStatus(this);
        }
    }

    public float attackSubstitute(float damage, PixelmonWrapper pw) {
        float damageResult = Math.min((float)this.health, damage);
        if (pw.bc.simulateMode) {
            return damageResult;
        }
        this.health = (int)((float)this.health - damage);
        pw.bc.sendToAll("pixelmon.status.substitutedamage", pw.getNickname());
        if (this.health <= 0) {
            pw.bc.sendToAll("pixelmon.status.substitutefade", pw.getNickname());
        }
        return damageResult;
    }

    public boolean ignoreSubstitute(PixelmonWrapper attacker) {
        if (attacker.getBattleAbility() instanceof Infiltrator) {
            return true;
        }
        if (Soundproof.isSoundMove(attacker.attack)) {
            return true;
        }
        if (attacker.attack != null) {
            if (attacker.attack.isAttack("Spectral Thief")) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        boolean canSubstitute = false;
        for (MoveChoice choice : bestOpponentChoices) {
            if (!(choice.isOffensiveMove() && (choice.isMiddleTier() && choice.weight < 25.0f || choice.tier >= 3 && choice.weight <= 75.0f))) continue;
            canSubstitute = true;
            break;
        }
        if (!canSubstitute) {
            for (PixelmonWrapper opponent : pw.getOpponentPokemon()) {
                if (!opponent.hasStatus(StatusType.Bide, StatusType.Freeze, StatusType.MultiTurn, StatusType.PoisonBadly, StatusType.Sleep)) continue;
                canSubstitute = true;
                break;
            }
        }
        if (canSubstitute) {
            userChoice.raiseWeight(30.0f);
        }
    }
}

