/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.GlobalStatusController;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public class FairyLock
extends GlobalStatusBase {
    private transient int duration = 2;

    public FairyLock() {
        super(StatusType.FairyLock);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.globalStatusController.hasStatus(this.type)) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        } else {
            user.bc.globalStatusController.addGlobalStatus(new FairyLock());
            user.bc.sendToAll("pixelmon.status.fairylock", new Object[0]);
        }
    }

    @Override
    public void applyRepeatedEffect(GlobalStatusController globalStatusController) {
        if (--this.duration <= 0) {
            globalStatusController.removeGlobalStatus(this);
        }
    }

    @Override
    public boolean stopsSwitching() {
        return true;
    }
}

