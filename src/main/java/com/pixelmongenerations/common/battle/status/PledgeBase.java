/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public abstract class PledgeBase
extends StatusBase {
    transient int remainingTurns = 4;

    public PledgeBase(StatusType type) {
        super(type);
    }

    @Override
    public boolean isTeamStatus() {
        return true;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (--this.remainingTurns <= 0) {
            pw.bc.sendToAll("pixelmon.status." + this.type.toString().toLowerCase() + "end", pw.getNickname());
            pw.removeTeamStatus(this);
        }
    }

    @Override
    public StatusBase copy() {
        try {
            return (StatusBase)this.getClass().getConstructor(new Class[0]).newInstance(new Object[0]);
        }
        catch (Exception e) {
            return this;
        }
    }
}

