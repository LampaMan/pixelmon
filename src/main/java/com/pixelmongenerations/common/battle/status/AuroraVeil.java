/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Veil;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class AuroraVeil
extends Veil {
    public AuroraVeil() {
        this(5);
    }

    public AuroraVeil(int turns) {
        super(StatusType.Reflect, StatsType.Defence, StatsType.SpecialDefence, turns, "pixelmon.effect.auroraveil", "pixelmon.effect.alreadybarrier", "pixelmon.effect.auroraveiloff");
    }

    @Override
    protected Veil getNewInstance(int effectTurns) {
        return new AuroraVeil(effectTurns);
    }
}

