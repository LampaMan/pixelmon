/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import java.util.ArrayList;

public class HelpingHand
extends StatusBase {
    public HelpingHand() {
        super(StatusType.HelpingHand);
    }

    /*
     * Enabled aggressive block sorting
     */
    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.bc.rules.battleType.numPokemon != 1) {
            if (!target.hasStatus(StatusType.HelpingHand) && target.bc.getTurnForPokemon(target) >= target.bc.turn) {
                target.bc.sendToAll("pixelmon.status.helpinghand", user.getNickname(), target.getNickname());
                target.addStatus(new HelpingHand(), user);
                return;
            }
        }
        target.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        target.setAttackFailed();
    }

    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        return new int[]{(int)((double)power * 1.5), accuracy};
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        pw.removeStatus(this);
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        userChoice.raiseWeight(25.0f);
    }
}

