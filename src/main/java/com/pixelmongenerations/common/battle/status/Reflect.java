/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.status.Screen;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class Reflect
extends Screen {
    public Reflect() {
        this(5);
    }

    public Reflect(int turns) {
        super(StatusType.Reflect, StatsType.Defence, turns, "pixelmon.effect.reflectraised", "pixelmon.effect.alreadybarrier", "pixelmon.status.reflectoff");
    }

    @Override
    protected Screen getNewInstance(int effectTurns) {
        return new Reflect(effectTurns);
    }
}

