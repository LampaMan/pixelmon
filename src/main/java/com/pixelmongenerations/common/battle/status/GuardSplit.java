/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.status.Split;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class GuardSplit
extends Split {
    public GuardSplit() {
        super(StatusType.GuardSplit, StatsType.Defence, StatsType.SpecialDefence, "pixelmon.status.guardsplit");
    }

    @Override
    protected Split getNewInstance(int splitStat1, int splitStat2) {
        GuardSplit split = new GuardSplit();
        split.statValue1 = splitStat1;
        split.statValue2 = splitStat2;
        return split;
    }
}

