/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusPersist;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Comatose;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import net.minecraft.nbt.NBTTagCompound;

public class Sleep
extends StatusPersist {
    public transient int effectTurns = -1;

    public Sleep() {
        this(RandomHelper.getRandomNumberBetween(1, 3));
    }

    public Sleep(int i) {
        super(StatusType.Sleep);
        this.effectTurns = i;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (this.checkChance()) {
            Sleep.sleep(user, target, user.attack, true);
        }
    }

    public static boolean sleep(PixelmonWrapper user, PixelmonWrapper target, Attack attack, boolean showMessage) {
        return new Sleep().addStatus(user, target, attack, showMessage, "pixelmon.effect.alreadysleeping", "pixelmon.effect.fallasleep");
    }

    @Override
    public boolean isImmune(PixelmonWrapper pokemon) {
        if (pokemon.bc.rules.hasClause("sleep")) {
            for (PixelmonWrapper pw : pokemon.getParticipant().allPokemon) {
                if (pw.getPrimaryStatus().type != StatusType.Sleep) continue;
                return true;
            }
        }
        return Sleep.uproarActive(pokemon);
    }

    @Override
    public boolean canAttackThisTurn(PixelmonWrapper user, Attack a) {
        if (user.getBattleAbility() instanceof Comatose) {
            return true;
        }
        if (Sleep.uproarActive(user)) {
            this.effectTurns = 0;
        }
        if (this.effectTurns-- <= 0) {
            user.removeStatus(this);
            return true;
        }
        user.bc.sendToAll("pixelmon.status.stillsleeping", user.getNickname());
        return a.isAttack("Snore", "Sleep Talk");
    }

    @Override
    public void writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        nbt.setInteger("StatusSleepTurns", this.effectTurns);
    }

    @Override
    public StatusPersist restoreFromNBT(NBTTagCompound nbt) {
        if (nbt.hasKey("StatusSleepTurns")) {
            return new Sleep(nbt.getInteger("StatusSleepTurns"));
        }
        return new Sleep();
    }

    public static boolean uproarActive(PixelmonWrapper pokemon) {
        if (pokemon.bc == null) {
            return false;
        }
        for (PixelmonWrapper pw : pokemon.bc.getActiveUnfaintedPokemon()) {
            if (!pw.hasStatus(StatusType.Uproar)) continue;
            return true;
        }
        return false;
    }

    @Override
    public StatusBase copy() {
        return new Sleep(this.effectTurns);
    }

    @Override
    public String getCureMessage() {
        return "pixelmon.status.wokeup";
    }

    @Override
    public String getCureMessageItem() {
        return "pixelmon.status.sleepcureitem";
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (!userChoice.hitsAlly()) {
            userChoice.raiseWeight(this.getWeightWithChance(userChoice.result.accuracy));
        }
    }
}

