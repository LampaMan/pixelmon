/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.ProtectVariationTeam;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class CraftyShield
extends ProtectVariationTeam {
    public CraftyShield() {
        super(StatusType.CraftyShield);
    }

    @Override
    public ProtectVariationTeam getNewInstance() {
        return new CraftyShield();
    }

    @Override
    protected void displayMessage(PixelmonWrapper user) {
        user.bc.sendToAll("pixelmon.status.craftyshield", user.getNickname());
    }

    @Override
    public boolean stopsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user) {
        if (user.isDynamaxed()) {
            return false;
        }
        return super.stopsIncomingAttack(pokemon, user) && user.attack.getAttackCategory() == AttackCategory.Status;
    }

    @Override
    public void stopsIncomingAttackMessage(PixelmonWrapper pokemon, PixelmonWrapper user) {
        user.bc.sendToAll("pixelmon.status.craftyshieldprotect", pokemon.getNickname());
    }
}

