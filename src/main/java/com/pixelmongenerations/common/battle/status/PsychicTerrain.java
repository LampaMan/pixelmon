/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Terrain;
import com.pixelmongenerations.common.entity.pixelmon.abilities.GaleWings;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Levitate;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Prankster;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Triage;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.ArrayList;

public class PsychicTerrain
extends Terrain {
    public PsychicTerrain() {
        super(StatusType.PsychicTerrain, "pixelmon.status.psychicterrain", "pixelmon.status.psychicterraincontinue", "pixelmon.status.psychicterrainend");
    }

    @Override
    public Terrain getNewInstance() {
        return new PsychicTerrain();
    }

    @Override
    public int[] modifyPowerAndAccuracyTarget(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (this.affectsPokemon(user) && a.getAttackBase().attackType == EnumType.Psychic) {
            power = (int)((double)power * 1.3);
        }
        return new int[]{power, accuracy};
    }

    @Override
    public boolean stopsIncomingAttack(PixelmonWrapper user, PixelmonWrapper target) {
        if (!user.isGrounded() && user.type.contains((Object)EnumType.Flying)) {
            return false;
        }
        if (target.attack.isAttack("Pursuit")) {
            return false;
        }
        if (target.priority > 0.0f) {
            if (!user.hasStatus(StatusType.MagnetRise, StatusType.Telekinesis) && user.getUsableHeldItem().getHeldItemType() != EnumHeldItems.airBalloon && !(user.getBattleAbility() instanceof Levitate)) {
                return true;
            }
        }
        if (this.hasPriorityUpAbility(user)) {
            user.bc.sendToAll("pixelmon.battletext.noeffect", target.getNickname());
            return false;
        }
        return false;
    }

    public boolean hasPriorityUpAbility(PixelmonWrapper user) {
        ArrayList<Class<?>> abilities = new ArrayList<Class<?>>();
        abilities.add(Prankster.class);
        abilities.add(GaleWings.class);
        abilities.add(Triage.class);
        for (Class class_ : abilities) {
            if (!user.getAbility().isAbility(class_)) continue;
            return true;
        }
        return false;
    }

    @Override
    public void stopsIncomingAttackMessage(PixelmonWrapper pokemon, PixelmonWrapper user) {
        user.bc.sendToAll("pixelmon.status.psychicterrainprotect", pokemon.getNickname());
    }

    @Override
    protected int countBenefits(PixelmonWrapper user, PixelmonWrapper target) {
        return this.affectsPokemon(user) ? 1 : 0;
    }
}

