/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Weather;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Chlorophyll;
import com.pixelmongenerations.common.entity.pixelmon.abilities.DrySkin;
import com.pixelmongenerations.common.entity.pixelmon.abilities.FlowerGift;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Harvest;
import com.pixelmongenerations.common.entity.pixelmon.abilities.LeafGuard;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SolarPower;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.List;

public class Sunny
extends Weather {
    public Sunny() {
        this(5);
    }

    public Sunny(int turnsToGo) {
        super(StatusType.Sunny, turnsToGo, EnumHeldItems.heatRock, "pixelmon.effect.harshsunlight", "pixelmon.status.brightlight", "pixelmon.status.sunlightfaded");
    }

    @Override
    protected Weather getNewInstance(int turns) {
        return new Sunny(turns);
    }

    @Override
    public int[] modifyPowerAndAccuracyTarget(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (a.getAttackBase().attackType == EnumType.Fire && target.getHeldItem() != PixelmonItemsHeld.utilityUmbrella) {
            power = (int)((double)power * 1.5);
        } else if (a.getAttackBase().attackType == EnumType.Water && target.getHeldItem() != PixelmonItemsHeld.utilityUmbrella) {
            power = (int)((double)power * 0.5);
        }
        return new int[]{power, accuracy};
    }

    @Override
    protected int countBenefits(PixelmonWrapper user, PixelmonWrapper target) {
        List<Attack> moveset;
        int benefits;
        block14: {
            block13: {
                benefits = 0;
                AbilityBase ability = target.getBattleAbility();
                if (user.getHeldItem() == PixelmonItemsHeld.utilityUmbrella) {
                    return benefits;
                }
                if (ability instanceof Chlorophyll || ability instanceof FlowerGift || ability instanceof Harvest || ability instanceof LeafGuard || ability instanceof SolarPower) {
                    ++benefits;
                } else if (ability instanceof DrySkin) {
                    --benefits;
                }
                moveset = user.getBattleAI().getMoveset(target);
                if (Attack.hasOffensiveAttackType(moveset, EnumType.Fire)) break block13;
                if (!Attack.hasAttack(moveset, "Weather Ball")) break block14;
            }
            ++benefits;
        }
        if (Attack.hasAttack(moveset, "Solar Beam")) {
            ++benefits;
        }
        if (Attack.hasAttack(moveset, "Moonlight", "Morning Sun", "Synthesis")) {
            ++benefits;
        }
        if (Attack.hasAttack(moveset, "Growth")) {
            ++benefits;
        }
        if (Attack.hasOffensiveAttackType(moveset, EnumType.Water)) {
            --benefits;
        }
        if (Attack.hasAttack(moveset, "Hurricane")) {
            --benefits;
        }
        if (Attack.hasAttack(moveset, "Thunder")) {
            --benefits;
        }
        return benefits;
    }
}

