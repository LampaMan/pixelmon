/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Value;
import com.pixelmongenerations.common.battle.attacks.ValueType;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.Recover;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import java.util.ArrayList;

public class Wish
extends StatusBase {
    private transient int turnsLeft;
    private transient int healAmount;
    private transient String userName;

    public Wish() {
        super(StatusType.Wish);
    }

    public Wish(PixelmonWrapper user) {
        this();
        this.turnsLeft = 2;
        this.healAmount = user.getPercentMaxHealth(50.0f);
        this.userName = user.getNickname();
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.hasStatus(this.type)) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        } else {
            user.addStatus(new Wish(user), user);
        }
    }

    @Override
    public boolean isTeamStatus() {
        return true;
    }

    @Override
    public boolean isWholeTeamStatus() {
        return false;
    }

    /*
     * Enabled aggressive block sorting
     */
    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (--this.turnsLeft > 0) return;
        pw.removeStatus(this);
        if (this.turnsLeft != 0) return;
        if (pw.isAlive()) {
            if (!pw.hasStatus(StatusType.HealBlock)) {
                pw.bc.sendToAll("pixelmon.effect.wish", this.userName);
                if (!pw.hasFullHealth()) {
                    pw.healEntityBy(this.healAmount);
                    return;
                }
                pw.bc.sendToAll("pixelmon.effect.healfailed", pw.getNickname());
                return;
            }
        }
        pw.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
    }

    @Override
    public void applySwitchOutEffect(PixelmonWrapper pw) {
        if (pw.isFainted() && this.turnsLeft == 1) {
            this.turnsLeft = -1;
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        Recover heal = new Recover(new Value(50, ValueType.WholeNumber));
        heal.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
    }
}

