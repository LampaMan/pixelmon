/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.status.Screen;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class LightScreen
extends Screen {
    public LightScreen() {
        this(5);
    }

    public LightScreen(int turns) {
        super(StatusType.LightScreen, StatsType.SpecialDefence, turns, "pixelmon.effect.uplightscreen", "pixelmon.effect.alreadylightscreen", "pixelmon.status.lightscreenoff");
    }

    @Override
    protected Screen getNewInstance(int effectTurns) {
        return new LightScreen(effectTurns);
    }
}

