/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.ChangeAbility;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.BattleBond;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Comatose;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Disguise;
import com.pixelmongenerations.common.entity.pixelmon.abilities.GulpMissile;
import com.pixelmongenerations.common.entity.pixelmon.abilities.IceFace;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicBounce;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Multitype;
import com.pixelmongenerations.common.entity.pixelmon.abilities.PowerConstruct;
import com.pixelmongenerations.common.entity.pixelmon.abilities.RKSSystem;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Schooling;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ShieldsDown;
import com.pixelmongenerations.common.entity.pixelmon.abilities.StanceChange;
import java.util.ArrayList;

public class GastroAcid
extends StatusBase {
    public GastroAcid() {
        super(StatusType.GastroAcid);
    }

    /*
     * Enabled aggressive block sorting
     */
    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        AbilityBase targetAbility = target.getBattleAbility(false);
        if (!target.hasStatus(StatusType.GastroAcid)) {
            if (!this.isAbility(targetAbility, Multitype.class, StanceChange.class, Schooling.class, Comatose.class, ShieldsDown.class, Disguise.class, RKSSystem.class, BattleBond.class, PowerConstruct.class, IceFace.class, GulpMissile.class, MagicBounce.class)) {
                if (!target.addStatus(new GastroAcid(), target)) return;
                target.bc.sendToAll("pixelmon.status.gastroacid", target.getNickname());
                targetAbility.onAbilityLost(target);
                return;
            }
        }
        target.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        user.attack.moveResult.result = AttackResult.failed;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        ChangeAbility changeAbility = new ChangeAbility(null);
        changeAbility.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
    }
}

