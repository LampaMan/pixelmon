/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Protect;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.LongReach;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.heldItems.ItemProtectivePads;
import java.util.ArrayList;

public class Obstruct
extends Protect {
    public Obstruct() {
        super(StatusType.Obstruct);
    }

    @Override
    protected boolean addStatus(PixelmonWrapper user) {
        return user.addStatus(new Obstruct(), user);
    }

    @Override
    public void stopsIncomingAttackMessage(PixelmonWrapper pokemon, PixelmonWrapper user) {
        super.stopsIncomingAttackMessage(pokemon, user);
        if (user.attack.getAttackBase().getMakesContact()) {
            if (user.hasHeldItem() && user.getHeldItem() instanceof ItemProtectivePads) {
                user.bc.sendToAll("pixelmon.effect.protectivepads", user.getNickname());
                return;
            }
            if (user.getBattleAbility() instanceof LongReach) {
                return;
            }
            user.getBattleStats().modifyStat(-2, StatsType.Defence, user, false);
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        super.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
        if (userChoice.weight != -1.0f) {
            block0: for (ArrayList<MoveChoice> choices : MoveChoice.splitChoices(pw.getOpponentPokemon(), bestOpponentChoices)) {
                for (MoveChoice choice : choices) {
                    if (!choice.isAttack() || !choice.attack.getAttackBase().getMakesContact()) continue;
                    userChoice.raiseWeight(12.5f);
                    continue block0;
                }
            }
        }
    }
}

