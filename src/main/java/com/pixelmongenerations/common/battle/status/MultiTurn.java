/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public class MultiTurn
extends StatusBase {
    public transient int numTurns;

    public MultiTurn() {
        super(StatusType.MultiTurn);
    }
}

