/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import java.util.ArrayList;

public class LockOn
extends StatusBase {
    private transient PixelmonWrapper target;

    public LockOn() {
        super(StatusType.LockOn);
    }

    public LockOn(PixelmonWrapper target) {
        super(StatusType.LockOn);
        this.target = target;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        user.bc.sendToAll("pixelmon.status.lockon", user.getNickname(), target.getNickname());
        user.addStatus(new LockOn(target), user);
    }

    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (user != target) {
            if (target == this.target) {
                user.removeStatus(this);
                return new int[]{power, -2};
            }
            if (!user.targets.contains(this.target)) {
                user.removeStatus(this);
            }
        }
        return new int[]{power, accuracy};
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        int accuracy;
        if (userChoice.hitsAlly()) {
            return;
        }
        MoveChoice maxAccuracyChoice = null;
        for (MoveChoice choice : bestUserChoices) {
            if (maxAccuracyChoice != null && maxAccuracyChoice.result.accuracy >= choice.result.accuracy) continue;
            maxAccuracyChoice = choice;
        }
        if (maxAccuracyChoice != null && (accuracy = maxAccuracyChoice.result.accuracy) < 100 && accuracy > 0) {
            userChoice.raiseWeight(100 - maxAccuracyChoice.result.accuracy);
            userChoice.raiseTier(maxAccuracyChoice.tier);
        }
    }
}

