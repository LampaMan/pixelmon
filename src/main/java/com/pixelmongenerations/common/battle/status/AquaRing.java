/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.ArrayList;

public class AquaRing
extends StatusBase {
    public AquaRing() {
        super(StatusType.AquaRing);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.addStatus(new AquaRing(), user)) {
            user.bc.sendToAll("pixelmon.effect.surroundwithwater", user.getNickname());
        } else {
            user.bc.sendToAll("pixelmon.effect.surroundedbywater", user.getNickname());
            user.attack.moveResult.result = AttackResult.failed;
        }
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (!pw.hasStatus(StatusType.HealBlock)) {
            if (pw.getHealth() != pw.getMaxHealth()) {
                pw.bc.sendToAll("pixelmon.status.ringheal", pw.getNickname());
            }
            int healAmount = pw.getPercentMaxHealth(6.25f);
            if (pw.getUsableHeldItem().getHeldItemType() == EnumHeldItems.bigRoot) {
                healAmount = (int)((double)healAmount * 1.3);
            }
            pw.healEntityBy(healAmount);
        }
    }

    @Override
    public boolean stopsStatusChange(StatusType t, PixelmonWrapper target, PixelmonWrapper user) {
        return t.isStatus(StatusType.AquaRing);
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (!pw.hasStatus(StatusType.HealBlock)) {
            userChoice.raiseWeight(12.5f);
        }
    }
}

