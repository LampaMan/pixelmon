/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Confusion;
import com.pixelmongenerations.common.battle.status.Sleep;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Terrain;
import com.pixelmongenerations.common.battle.status.Yawn;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Levitate;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.List;

public class MistyTerrain
extends Terrain {
    public MistyTerrain() {
        super(StatusType.MistyTerrain, "pixelmon.status.mistyterrain", "pixelmon.status.mistyterraincontinue", "pixelmon.status.mistyterrainend");
    }

    @Override
    public Terrain getNewInstance() {
        return new MistyTerrain();
    }

    @Override
    public int[] modifyPowerAndAccuracyTarget(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (this.affectsPokemon(user) && a.getAttackBase().attackType == EnumType.Dragon) {
            power = (int)((double)power * 0.5);
        }
        return new int[]{power, accuracy};
    }

    @Override
    public boolean stopsStatusChange(StatusType t, PixelmonWrapper target, PixelmonWrapper user) {
        block11: {
            block10: {
                block8: {
                    block9: {
                        if (target.isGrounded()) break block8;
                        if (target.type.contains((Object)EnumType.Flying) || target.getBattleAbility() instanceof Levitate || target.getUsableHeldItem().getHeldItemType() == EnumHeldItems.airBalloon) break block9;
                        if (!target.hasStatus(StatusType.MagnetRise, StatusType.Telekinesis)) break block8;
                    }
                    return false;
                }
                if (!this.affectsPokemon(user)) break block10;
                if (t.isStatus(StatusType.Burn, StatusType.Freeze, StatusType.Paralysis, StatusType.Poison, StatusType.PoisonBadly, StatusType.Confusion, StatusType.Sleep, StatusType.Yawn)) break block11;
            }
            return false;
        }
        if (user != target && user.attack != null) {
            if (t.isStatus(StatusType.Burn, StatusType.Freeze, StatusType.Paralysis, StatusType.Poison, StatusType.PoisonBadly, StatusType.Confusion, StatusType.Sleep, StatusType.Yawn)) {
                target.bc.sendToAll("pixelmon.effect.mistyfail", target.getNickname());
            }
        }
        return true;
    }

    @Override
    protected int countBenefits(PixelmonWrapper user, PixelmonWrapper target) {
        int benefits = 0;
        if (this.affectsPokemon(target)) {
            List<Attack> moveset = user.getBattleAI().getMoveset(target);
            if (Attack.hasOffensiveAttackType(moveset, EnumType.Dragon)) {
                --benefits;
            }
            if (target.hasStatus(StatusType.Yawn)) {
                ++benefits;
            }
            if (Attack.hasAttack(moveset, "Rest")) {
                --benefits;
            }
            block0: for (Attack move : moveset) {
                for (EffectBase e : move.getAttackBase().effects) {
                    if (!(e instanceof Sleep) && !(e instanceof Yawn) && !(e instanceof Confusion)) continue;
                    --benefits;
                    continue block0;
                }
            }
            return benefits;
        }
        return benefits;
    }
}

