/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.ArrayList;

public class MagnetRise
extends StatusBase {
    public MagnetRise() {
        super(StatusType.MagnetRise);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.hasStatus(StatusType.MagnetRise, StatusType.Ingrain)) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        } else if (user.addStatus(new MagnetRise(), user)) {
            user.bc.sendToAll("pixelmon.effect.magnetrise", user.getNickname());
        }
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    @Override
    public boolean stopsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user) {
        if (user.attack.getAttackBase().attackType != EnumType.Ground) return false;
        if (user.attack.isAttack("Sand Attack", "Thousand Arrows")) return false;
        if (pokemon.isGrounded()) return false;
        return true;
    }

    @Override
    public void stopsIncomingAttackMessage(PixelmonWrapper pokemon, PixelmonWrapper user) {
        user.bc.sendToAll("pixelmon.battletext.noeffect", pokemon.getNickname());
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (MoveChoice.hasOffensiveAttackType(MoveChoice.getTargetedChoices(pw, bestOpponentChoices), EnumType.Ground)) {
            userChoice.raiseWeight(30.0f);
        }
    }
}

