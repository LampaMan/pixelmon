/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public class UproarStatus
extends StatusBase {
    public UproarStatus() {
        super(StatusType.Uproar);
    }
}

