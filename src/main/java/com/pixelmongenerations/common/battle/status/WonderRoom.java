/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.GlobalStatusController;
import com.pixelmongenerations.common.battle.controller.ai.BattleAIBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import java.util.ArrayList;

public class WonderRoom
extends GlobalStatusBase {
    private transient int duration = 5;

    public WonderRoom() {
        super(StatusType.WonderRoom);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.globalStatusController.removeGlobalStatus(this.type)) {
            user.bc.sendToAll("pixelmon.status.wonderroomend", new Object[0]);
        } else {
            user.bc.sendToAll("pixelmon.status.wonderroom", new Object[0]);
            user.bc.globalStatusController.addGlobalStatus(new WonderRoom());
        }
    }

    @Override
    public int[] modifyBaseStats(PixelmonWrapper user, int[] stats) {
        int tempDefense = stats[StatsType.Defence.getStatIndex()];
        stats[StatsType.Defence.getStatIndex()] = stats[StatsType.SpecialDefence.getStatIndex()];
        stats[StatsType.SpecialDefence.getStatIndex()] = tempDefense;
        return stats;
    }

    @Override
    public void applyRepeatedEffect(GlobalStatusController globalStatusController) {
        if (--this.duration <= 0) {
            globalStatusController.bc.sendToAll("pixelmon.status.wonderroomend", new Object[0]);
            globalStatusController.removeGlobalStatus(this);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        ArrayList<PixelmonWrapper> opponents = pw.getOpponentPokemon();
        WonderRoom wonderRoom = (WonderRoom)pw.bc.globalStatusController.getGlobalStatus(this.type);
        BattleAIBase ai = pw.getBattleAI();
        try {
            pw.bc.simulateMode = false;
            pw.bc.sendMessages = false;
            this.applyEffect(pw, pw);
            pw.bc.simulateMode = true;
            pw.bc.modifyStats();
            pw.bc.modifyStatsCancellable(pw);
            ArrayList<MoveChoice> bestUserChoicesAfter = ai.getBestAttackChoices(pw);
            ArrayList<ArrayList<MoveChoice>> bestOpponentChoicesAfter = ai.getBestAttackChoices(opponents);
            ai.weightFromUserOptions(pw, userChoice, bestUserChoices, bestUserChoicesAfter);
            ai.weightFromOpponentOptions(pw, userChoice, MoveChoice.splitChoices(opponents, bestOpponentChoices), bestOpponentChoicesAfter);
        }
        finally {
            pw.bc.simulateMode = false;
            if (wonderRoom != null) {
                pw.bc.globalStatusController.addGlobalStatus(wonderRoom);
            }
            pw.bc.simulateMode = true;
            pw.bc.sendMessages = true;
            pw.bc.modifyStats();
            pw.bc.modifyStatsCancellable(pw);
        }
    }
}

