/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Protect;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import java.util.ArrayList;

public class MatBlock
extends StatusBase {
    public MatBlock() {
        super(StatusType.MatBlock);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.isFirstTurn() && !user.bc.isLastMover() && user.addTeamStatus(new MatBlock(), user)) {
            user.bc.sendToAll("pixelmon.status.matblock", user.getNickname());
        } else {
            user.bc.sendToAll("pixelmon.effect.effectfailed", user.getNickname());
            user.attack.moveResult.result = AttackResult.failed;
        }
    }

    @Override
    public boolean isTeamStatus() {
        return true;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        pw.removeTeamStatus(this);
    }

    @Override
    public boolean stopsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user) {
        return user.attack.getAttackCategory() != AttackCategory.Status;
    }

    @Override
    public void stopsIncomingAttackMessage(PixelmonWrapper pokemon, PixelmonWrapper user) {
        user.bc.sendToAll("pixelmon.status.matblocked", pokemon.attack.getAttackBase().getLocalizedName());
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        for (ArrayList<MoveChoice> choices : MoveChoice.splitChoices(pw.getOpponentPokemon(), bestOpponentChoices)) {
            if (!MoveChoice.canOutspeed(choices, pw, bestUserChoices)) continue;
            return;
        }
        Protect protect = new Protect();
        protect.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
        if (pw.bc.rules.battleType.numPokemon > 1) {
            userChoice.raiseWeight(20 * pw.bc.rules.battleType.numPokemon);
        }
    }
}

