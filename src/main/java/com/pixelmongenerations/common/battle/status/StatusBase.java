/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.NoStatus;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.EnumType;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public abstract class StatusBase
extends EffectBase {
    public transient StatusType type;

    public StatusBase(StatusType type) {
        this.type = type;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
    }

    public void onAttackUsed(PixelmonWrapper user, Attack attack) {
    }

    public boolean stopsSwitching() {
        return false;
    }

    public void applyRepeatedEffect(PixelmonWrapper pw) {
    }

    public void onAttackEnd(PixelmonWrapper pw) {
    }

    public boolean canAttackThisTurn(PixelmonWrapper user, Attack a) {
        return true;
    }

    public boolean stopsSelfStatusMove(PixelmonWrapper user, PixelmonWrapper opponent, Attack attack) {
        return false;
    }

    public boolean stopsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user) {
        return false;
    }

    public boolean stopsIncomingAttackUser(PixelmonWrapper pokemon, PixelmonWrapper user) {
        return false;
    }

    public void stopsIncomingAttackMessage(PixelmonWrapper pokemon, PixelmonWrapper user) {
    }

    public boolean stopsStatusChange(StatusType t, PixelmonWrapper target, PixelmonWrapper user) {
        return false;
    }

    public void onDamageReceived(PixelmonWrapper userWrapper, PixelmonWrapper pokemon, Attack a, int damage, DamageTypeEnum damageType) {
    }

    public int modifyDamageIncludeFixed(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        return damage;
    }

    public boolean isTeamStatus() {
        return false;
    }

    public boolean isWholeTeamStatus() {
        return this.isTeamStatus();
    }

    public void applySwitchOutEffect(PixelmonWrapper pw) {
    }

    public void applyEffectOnSwitch(PixelmonWrapper pw) {
    }

    public void applyBeforeEffect(PixelmonWrapper victim, PixelmonWrapper opponent) {
    }

    public void applyEndOfBattleEffect(PixelmonWrapper pokemon) {
    }

    public List<EnumType> getEffectiveTypes(PixelmonWrapper user, PixelmonWrapper target) {
        return target.type;
    }

    public int[] modifyBaseStats(PixelmonWrapper user, int[] stats) {
        return stats;
    }

    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        return stats;
    }

    public int[] modifyStatsCancellable(PixelmonWrapper user, int[] stats) {
        return stats;
    }

    public boolean ignoreStatus(PixelmonWrapper user, PixelmonWrapper target) {
        return false;
    }

    public boolean allowsStatChange(PixelmonWrapper pokemon, PixelmonWrapper user, StatsEffect e) {
        return true;
    }

    public float modifyWeight(float initWeight) {
        return initWeight;
    }

    public boolean redirectAttack(PixelmonWrapper user, PixelmonWrapper targetAlly, Attack attack) {
        return false;
    }

    public boolean isImmune(PixelmonWrapper pokemon) {
        return false;
    }

    public StatusBase copy() {
        return this;
    }

    @Override
    public boolean cantMiss(PixelmonWrapper user) {
        return false;
    }

    public String getCureMessage() {
        return "";
    }

    public String getCureMessageItem() {
        return "";
    }

    public static StatusBase getNewInstance(Class<? extends StatusBase> statusClass) {
        try {
            return statusClass.getConstructor(new Class[0]).newInstance(new Object[0]);
        }
        catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            return NoStatus.noStatus;
        }
    }
}

