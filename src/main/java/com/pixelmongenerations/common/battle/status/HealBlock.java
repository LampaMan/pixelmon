/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.Drain;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.Recover;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.Synthesis;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.PoisonHeal;
import com.pixelmongenerations.common.entity.pixelmon.abilities.VoltAbsorb;
import com.pixelmongenerations.common.entity.pixelmon.abilities.WaterAbsorb;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.ArrayList;
import java.util.List;

public class HealBlock
extends StatusBase {
    transient int turnsLeft = 5;

    public HealBlock() {
        super(StatusType.HealBlock);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.hasStatus(StatusType.HealBlock)) {
            user.bc.sendToAll("pixelmon.status.healblockalready", target.getNickname());
            user.attack.moveResult.result = AttackResult.failed;
        } else if (target.addTeamStatus(new HealBlock(), user)) {
            user.bc.sendToAll("pixelmon.status.healblock", target.getNickname());
        }
    }

    @Override
    public boolean isTeamStatus() {
        return true;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (--this.turnsLeft <= 0) {
            pw.bc.sendToAll("pixelmon.status.healblockend", pw.getNickname());
            pw.removeTeamStatus(this);
        }
    }

    @Override
    public StatusBase copy() {
        HealBlock copy = new HealBlock();
        copy.turnsLeft = this.turnsLeft;
        return copy;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        boolean hasHeal = false;
        for (PixelmonWrapper target : pw.getOpponentPokemon()) {
            AbilityBase ability = target.getBattleAbility();
            hasHeal = hasHeal != false || ability instanceof PoisonHeal != false || ability instanceof VoltAbsorb != false || ability instanceof WaterAbsorb != false;
            EnumHeldItems heldItem = target.getUsableHeldItem().getHeldItemType();
            hasHeal = hasHeal != false || heldItem == EnumHeldItems.leftovers || heldItem == EnumHeldItems.shellBell;
            List<Attack> moveset = pw.getBattleAI().getMoveset(target);
            hasHeal = hasHeal || Attack.hasAttack(moveset, "Aqua Ring", "Ingrain", "Leech Seed", "Rest", "Swallow", "Wish");
            for (Attack attack : moveset) {
                for (EffectBase effect : attack.getAttackBase().effects) {
                    hasHeal = hasHeal != false || effect instanceof Drain != false || effect instanceof Recover != false || effect instanceof Synthesis != false;
                }
            }
        }
        if (!hasHeal) return;
        userChoice.raiseWeight(25.0f);
    }
}

