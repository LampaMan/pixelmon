/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusPersist;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Comatose;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Corrosion;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Guts;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MarvelScale;
import com.pixelmongenerations.common.entity.pixelmon.abilities.PoisonHeal;
import com.pixelmongenerations.common.entity.pixelmon.abilities.QuickFeet;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Synchronize;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ToxicBoost;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.network.ChatHandler;
import java.util.ArrayList;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.TextComponentTranslation;

public class Poison
extends StatusPersist {
    public Poison() {
        this(StatusType.Poison);
    }

    public Poison(StatusType type) {
        super(type);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (this.checkChance()) {
            Poison.poison(user, target, user.attack, true);
        }
    }

    public static boolean poison(PixelmonWrapper user, PixelmonWrapper target, Attack attack, boolean showMessage) {
        if (Poison.canPoison(user, target, attack, showMessage)) {
            boolean result;
            TextComponentTranslation message = null;
            if (showMessage) {
                message = ChatHandler.getMessage("pixelmon.effect.poisoned", target.getNickname());
            }
            if (user.getBattleAbility() instanceof Corrosion && attack != null && attack.getAttackCategory() == AttackCategory.Status) {
                if (target.hasType(EnumType.Steel, EnumType.Poison)) {
                    target.addStatusNoCheck(new Poison(), user, message);
                    boolean result2 = true;
                    return true;
                }
            }
            if (!(result = target.addStatus(new Poison(), user, message)) && new Poison().isImmune(target) && user.getBattleAbility() instanceof Corrosion) {
                target.addStatusNoCheck(new Poison(), user, message);
                result = true;
            }
            if (!result && attack != null && attack.getAttackCategory() == AttackCategory.Status) {
                user.setAttackFailed();
            }
            return result;
        }
        return false;
    }

    public static boolean canPoison(PixelmonWrapper user, PixelmonWrapper target, Attack attack, boolean showMessage) {
        boolean isStatusMove = attack != null && attack.getAttackCategory() == AttackCategory.Status;
        if (!target.hasStatus(StatusType.Poison)) {
            if (!target.hasStatus(StatusType.PoisonBadly)) {
                if (target.getBattleAbility() instanceof Comatose) {
                    if (showMessage && isStatusMove) {
                        user.bc.sendToAll("pixelmon.battletext.noeffect", target.getNickname());
                        user.setAttackFailed();
                    }
                    return false;
                }
                if (target.hasType(EnumType.Poison, EnumType.Steel)) {
                    if (user.getBattleAbility() instanceof Corrosion && isStatusMove) {
                        user.bc.sendToAll("pixelmon.abilities.corrosion", user.getNickname());
                        return true;
                    }
                    if (showMessage && isStatusMove) {
                        user.bc.sendToAll("pixelmon.battletext.noeffect", target.getNickname());
                        user.setAttackFailed();
                    }
                    return false;
                }
                if (target.hasPrimaryStatus()) {
                    if (showMessage && isStatusMove) {
                        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
                        user.setAttackFailed();
                    }
                    return false;
                }
                return true;
            }
        }
        if (showMessage && isStatusMove) {
            user.bc.sendToAll("pixelmon.effect.alreadypoisoned", target.getNickname());
            user.setAttackFailed();
        }
        return false;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        block7: {
            block4: {
                block6: {
                    block5: {
                        AbilityBase ability = pw.getBattleAbility();
                        if (ability instanceof MagicGuard) {
                            return;
                        }
                        if (!(ability instanceof PoisonHeal)) break block4;
                        if (pw.hasFullHealth()) break block5;
                        if (!pw.hasStatus(StatusType.HealBlock)) break block6;
                    }
                    return;
                }
                pw.bc.sendToAll("pixelmon.abilities.poisonheal", pw.getNickname());
                pw.healByPercent(12.5f);
                break block7;
            }
            pw.bc.sendToAll("pixelmon.status.hurtbypoison", pw.getNickname());
            pw.doBattleDamage(pw, this.getPoisonDamage(pw), DamageTypeEnum.STATUS);
        }
    }

    protected float getPoisonDamage(PixelmonWrapper pw) {
        return pw.getPercentMaxHealth(12.5f);
    }

    @Override
    public StatusPersist restoreFromNBT(NBTTagCompound nbt) {
        return new Poison();
    }

    @Override
    public boolean isImmune(PixelmonWrapper pokemon) {
        return false;
    }

    @Override
    public String getCureMessage() {
        return "pixelmon.status.poisoncure";
    }

    @Override
    public String getCureMessageItem() {
        return "pixelmon.status.poisoncureitem";
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        boolean offensive = userChoice.isOffensiveMove();
        float weight = this.getWeightWithChance(25.0f);
        if (offensive) {
            if (userChoice.isMiddleTier() == false) return;
        }
        boolean hitsAlly = userChoice.hitsAlly();
        if (offensive && hitsAlly) {
            return;
        }
        for (PixelmonWrapper target : userChoice.targets) {
            AbilityBase ability = target.getBattleAbility();
            boolean beneficial = ability instanceof Guts || ability instanceof MarvelScale || ability instanceof PoisonHeal || ability instanceof QuickFeet || ability instanceof ToxicBoost || Attack.hasAttack(pw.getBattleAI().getMoveset(target), "Facade");
            if (beneficial && hitsAlly) {
                userChoice.raiseWeight(weight);
            } else if (beneficial ^ hitsAlly) {
                userChoice.raiseWeight(-weight);
            } else if (!(ability instanceof MagicGuard) && !(ability instanceof Synchronize)) {
                userChoice.raiseWeight(weight);
            }
        }
    }
}

