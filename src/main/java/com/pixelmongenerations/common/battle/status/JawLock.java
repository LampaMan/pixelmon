/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public class JawLock
extends StatusBase {
    transient PixelmonWrapper locker;

    public JawLock() {
        super(StatusType.JawLock);
    }

    public JawLock(PixelmonWrapper locker) {
        super(StatusType.JawLock);
        this.locker = locker;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.addStatus(new JawLock(user), user)) {
            user.addStatus(new JawLock(target), user);
            target.bc.sendToAll("pixelmon.effect.noescape", user.getNickname());
            user.bc.sendToAll("pixelmon.effect.noescape", target.getNickname());
        } else {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        }
    }

    @Override
    public boolean stopsSwitching() {
        return true;
    }

    @Override
    public void onDamageReceived(PixelmonWrapper userWrapper, PixelmonWrapper pokemon, Attack a, int damage, DamageTypeEnum damageType) {
        if (userWrapper.isFainted() || pokemon.isFainted()) {
            userWrapper.removeStatus(StatusType.JawLock);
            pokemon.removeStatus(StatusType.JawLock);
        }
    }
}

