/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class Raging
extends StatusBase {
    public Raging() {
        super(StatusType.Raging);
    }

    @Override
    public void onDamageReceived(PixelmonWrapper user, PixelmonWrapper pokemon, Attack a, int damage, DamageTypeEnum damagetype) {
        if (pokemon.isAlive() && user != pokemon && (damagetype == DamageTypeEnum.ATTACK || damagetype == DamageTypeEnum.ATTACKFIXED) && pokemon.getBattleStats().modifyStat(1, StatsType.Attack)) {
            user.bc.sendToAll("pixelmon.status.ragebuilding", pokemon.getNickname());
        }
    }

    @Override
    public void onAttackUsed(PixelmonWrapper user, Attack attack) {
        user.removeStatus(this);
    }
}

