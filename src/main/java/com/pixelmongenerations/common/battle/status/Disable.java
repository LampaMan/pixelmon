/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.network.ChatHandler;
import java.util.ArrayList;
import net.minecraft.util.text.TextComponentTranslation;

public class Disable
extends StatusBase {
    transient Attack disabledMove;
    transient int effectiveTurns = 4;

    public Disable() {
        super(StatusType.Disable);
    }

    public Disable(Attack attack) {
        super(StatusType.Disable);
        this.disabledMove = attack;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        Attack lastAttack = target.lastAttack;
        if (target.hasStatus(StatusType.Disable) || target.isDynamaxed()) {
            target.bc.sendToAll("pixelmon.effect.alreadydisabled", target.getNickname());
            user.attack.moveResult.result = AttackResult.failed;
            return;
        }
        if (lastAttack != null) {
            TextComponentTranslation message = ChatHandler.getMessage("pixelmon.status.disable", target.getNickname(), lastAttack.getAttackBase().getLocalizedName());
            target.addStatus(new Disable(lastAttack), user, message);
        } else {
            target.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        }
    }

    @Override
    public boolean canAttackThisTurn(PixelmonWrapper user, Attack a) {
        if (a.equals(this.disabledMove)) {
            user.bc.sendToAll("pixelmon.status.disabled", user.getNickname(), a.getAttackBase().getLocalizedName());
            return false;
        }
        return true;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (--this.effectiveTurns <= 0) {
            pw.bc.sendToAll("pixelmon.status.nolongerdisabled", pw.getNickname(), this.disabledMove.getAttackBase().getLocalizedName());
            pw.removeStatus(this);
        } else {
            pw.getMoveset().stream().filter(attack -> attack.equals(this.disabledMove)).forEach(attack -> attack.setDisabled(true, pw));
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        for (PixelmonWrapper target : userChoice.targets) {
            String attackName = target.lastAttack == null ? "" : target.lastAttack.getAttackBase().getLocalizedName();
            String string = attackName;
            if (!MoveChoice.canOutspeed(bestOpponentChoices, pw, bestUserChoices)) {
                if (!MoveChoice.hasSuccessfulAttackChoice(bestOpponentChoices, attackName)) continue;
            }
            userChoice.raiseWeight(40.0f);
        }
    }
}

