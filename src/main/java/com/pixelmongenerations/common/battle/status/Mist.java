/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.modifiers.ModifierBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.modifiers.ModifierType;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.CottonDown;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Gooey;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Infiltrator;
import com.pixelmongenerations.common.entity.pixelmon.abilities.TanglingHair;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import java.util.ArrayList;

public class Mist
extends StatusBase {
    transient int effectTurns = 0;

    public Mist() {
        super(StatusType.Mist);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        Mist.doMistEffect(user, true);
    }

    public static void doMistEffect(PixelmonWrapper user, boolean isMove) {
        if (user.targetIndex == 0 || user.bc.simulateMode) {
            if (user.hasStatus(StatusType.Mist)) {
                user.bc.sendToAll("pixelmon.effect.alreadymist", user.getNickname());
                if (isMove) {
                    user.attack.moveResult.result = AttackResult.failed;
                }
                return;
            }
            if (user.addTeamStatus(new Mist(), user)) {
                user.bc.sendToAll("pixelmon.effect.usemist", user.getNickname());
            }
        }
    }

    @Override
    public boolean isTeamStatus() {
        return true;
    }

    @Override
    public boolean stopsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user) {
        if (!(user.attack.getAttackCategory() != AttackCategory.Status || user.getBattleAbility() instanceof Infiltrator || user.getBattleAbility() instanceof TanglingHair || user.getBattleAbility() instanceof CottonDown || user.getBattleAbility() instanceof Gooey || !this.hasTargetStatsEffect(user.attack))) {
            user.bc.sendToAll("pixelmon.status.mistprotect", pokemon.getNickname());
            return true;
        }
        return false;
    }

    private boolean hasTargetStatsEffect(Attack attack) {
        for (EffectBase e : attack.getAttackBase().effects) {
            if (!(e instanceof StatsEffect)) continue;
            for (ModifierBase m : ((StatsEffect)e).modifiers) {
                if (m.type != ModifierType.User) continue;
                return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean allowsStatChange(PixelmonWrapper pokemon, PixelmonWrapper user, StatsEffect e) {
        return e.getUser() || e.value > 0 || user.getBattleAbility() instanceof Infiltrator || user.getBattleAbility() instanceof TanglingHair || user.getBattleAbility() instanceof CottonDown || user.getBattleAbility() instanceof Gooey;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (++this.effectTurns >= 5) {
            pw.bc.sendToAll("pixelmon.status.mistoff", pw.getNickname());
            pw.removeTeamStatus(this);
        }
    }

    @Override
    public StatusBase copy() {
        Mist copy = new Mist();
        copy.effectTurns = this.effectTurns;
        return copy;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        boolean hasTargetStatsEffect = false;
        block0: for (PixelmonWrapper opponent : pw.getOpponentPokemon()) {
            if (opponent.getBattleAbility() instanceof Infiltrator || opponent.getBattleAbility() instanceof TanglingHair || opponent.getBattleAbility() instanceof CottonDown || opponent.getBattleAbility() instanceof Gooey) continue;
            for (Attack attack : pw.getBattleAI().getMoveset(opponent)) {
                if (!this.hasTargetStatsEffect(attack)) continue;
                hasTargetStatsEffect = true;
                continue block0;
            }
        }
        if (hasTargetStatsEffect) {
            userChoice.raiseWeight(15.0f);
        }
    }
}

