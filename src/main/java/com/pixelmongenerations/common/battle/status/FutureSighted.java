/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import java.util.ArrayList;

public class FutureSighted
extends StatusBase {
    public transient int turnsToGo = 3;
    transient PixelmonWrapper user;
    transient Attack attack;

    public FutureSighted(PixelmonWrapper user, Attack attack) {
        super(StatusType.FutureSight);
        this.user = user;
        this.attack = attack;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        --this.turnsToGo;
        if (this.turnsToGo <= 0) {
            if (pw.isAlive()) {
                pw.bc.sendToAll("pixelmon.status.takefuturesight", pw.getNickname(), this.attack.getAttackBase().getLocalizedName());
                this.user.attack = this.attack;
                this.user.targets = new ArrayList<PixelmonWrapper>();
                this.user.targets.add(pw);
                this.user.bc = pw.bc;
                if (this.user.isFainted()) {
                    this.user.setHealth(1);
                    this.user.useAttackOnly();
                    this.user.setHealth(0);
                } else {
                    this.user.useAttackOnly();
                }
            }
            pw.removeStatus(this);
        }
    }

    @Override
    public boolean isTeamStatus() {
        return true;
    }

    @Override
    public boolean isWholeTeamStatus() {
        return false;
    }
}

