/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.ArrayList;

public class Charge
extends StatusBase {
    transient int duration = 2;

    public Charge() {
        super(StatusType.Charge);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        user.bc.sendToAll("pixelmon.status.charge", user.getNickname());
        Charge charge = (Charge)user.getStatus(StatusType.Charge);
        if (charge == null) {
            user.addStatus(new Charge(), user);
        } else {
            charge.duration = 2;
        }
    }

    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (a.getAttackBase().attackType == EnumType.Electric) {
            power *= 2;
        }
        return new int[]{power, accuracy};
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (--this.duration <= 0) {
            pw.removeStatus(this);
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (!pw.hasStatus(StatusType.Charge) && MoveChoice.hasOffensiveAttackType(bestUserChoices, EnumType.Electric)) {
            userChoice.raiseWeight(50.0f);
        }
    }
}

