/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Sport;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.EnumType;

public class MudSport
extends Sport {
    public MudSport() {
        this((PixelmonWrapper)null);
    }

    public MudSport(PixelmonWrapper user) {
        super(user, StatusType.MudSport, EnumType.Electric, "Mud Sport");
    }

    @Override
    protected Sport getNewInstance(PixelmonWrapper user) {
        return new MudSport(user);
    }
}

