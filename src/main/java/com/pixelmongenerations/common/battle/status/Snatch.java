/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class Snatch
extends StatusBase {
    public Snatch() {
        super(StatusType.Snatch);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        user.bc.sendToAll("pixelmon.effect.snatch", user.getNickname());
        user.addStatus(new Snatch(), user);
    }

    @Override
    public boolean stopsSelfStatusMove(PixelmonWrapper user, PixelmonWrapper opponent, Attack attack) {
        block3: {
            block2: {
                if (attack.getAttackCategory() != AttackCategory.Status) break block2;
                if (!attack.isAttack("Helping Hand", "Metronome", "Snatch")) break block3;
            }
            return false;
        }
        user.bc.sendToAll("pixelmon.effect.snatched", user.getNickname(), opponent.getNickname());
        attack.applySelfStatusMove(user, attack.moveResult);
        user.removeStatus(this);
        return true;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        pw.removeStatus(this);
    }
}

