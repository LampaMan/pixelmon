/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.GlobalStatusController;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public class EchoedVoiceStatus
extends GlobalStatusBase {
    public int power = 80;
    public int turnInc;

    public EchoedVoiceStatus(int turnInc) {
        super(StatusType.EchoedVoice);
        this.turnInc = turnInc;
    }

    @Override
    public void applyRepeatedEffect(GlobalStatusController globalStatusController) {
        if (globalStatusController.bc.battleTurn != this.turnInc) {
            globalStatusController.removeGlobalStatus(this);
        }
    }
}

