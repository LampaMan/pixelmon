/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import java.util.ArrayList;

public class Grudge
extends StatusBase {
    public Grudge() {
        super(StatusType.Grudge);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        user.bc.sendToAll("pixelmon.status.grudge", user.getNickname());
        user.addStatus(new Grudge(), user);
    }

    @Override
    public void onAttackUsed(PixelmonWrapper user, Attack attack) {
        user.removeStatus(this);
    }

    @Override
    public void onDamageReceived(PixelmonWrapper userWrapper, PixelmonWrapper pokemon, Attack a, int damage, DamageTypeEnum damageType) {
        if (pokemon.isFainted() && damageType.isDirect() && a != null) {
            if (!a.isAttack("Struggle") && userWrapper.selectedAttack != null && userWrapper.selectedAttack.pp > 0) {
                userWrapper.bc.sendToAll("pixelmon.status.grudgeactivate", userWrapper.getNickname(), userWrapper.selectedAttack.getAttackBase().getLocalizedName());
                userWrapper.selectedAttack.pp = 0;
            }
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (pw.getParticipant().countAblePokemon() > 1 && MoveChoice.canOutspeedAnd2HKO(bestOpponentChoices, pw, userChoice.createList())) {
            userChoice.raiseWeight(25.0f);
        }
    }
}

