/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.Feint;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Vanish;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public abstract class ProtectVariation
extends StatusBase {
    public ProtectVariation(StatusType type) {
        super(type);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.simulateMode) {
            return;
        }
        float successChance = 1.0f / (float)Math.pow(3.0, user.protectsInARow);
        boolean successful = true;
        if (this.canFail()) {
            successful = RandomHelper.getRandomChance(successChance);
        }
        successful = successful && !user.bc.isLastMover() && this.addStatus(user);
        boolean bl = successful;
        if (successful) {
            if (user.protectsInARow < 6) {
                ++user.protectsInARow;
            }
            this.displayMessage(user);
        } else {
            user.protectsInARow = 0;
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        }
    }

    @Override
    public boolean stopsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user) {
        if (user.attack.getAttackBase().targetingInfo.hitsAll && user.attack.getAttackBase().targetingInfo.hitsSelf) {
            return false;
        }
        if (user.isDynamaxed()) {
            return false;
        }
        if (!pokemon.getTeamPokemon().contains(user)) {
            for (EffectBase e : user.attack.getAttackBase().effects) {
                if (!(e instanceof Vanish) && !(e instanceof Feint)) continue;
                return false;
            }
        }
        return true;
    }

    @Override
    public int[] modifyPowerAndAccuracyTarget(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (target.isDynamaxed()) {
            if (!a.isAttack("G-Max Rapid Flow", "G-Max One Blow")) {
                power = (int)((double)power * 0.25);
            }
        }
        return new int[]{power, accuracy};
    }

    protected abstract boolean addStatus(PixelmonWrapper var1);

    protected abstract void displayMessage(PixelmonWrapper var1);

    protected boolean canFail() {
        return true;
    }
}

