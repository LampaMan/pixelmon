/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.PledgeBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class GrassPledge
extends PledgeBase {
    public GrassPledge() {
        super(StatusType.GrassPledge);
    }

    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        int n = StatsType.Speed.getStatIndex();
        stats[n] = stats[n] / 2;
        return stats;
    }
}

