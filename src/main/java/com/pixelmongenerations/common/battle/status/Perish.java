/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Soundproof;
import java.util.ArrayList;

public class Perish
extends StatusBase {
    public int effectTurns = 4;

    public Perish() {
        super(StatusType.Perish);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.hasStatus(this.type)) {
            user.bc.sendToAll("applyperish.already", target.getNickname());
            user.attack.moveResult.result = AttackResult.failed;
        } else if (target.addStatus(new Perish(), target)) {
            user.bc.sendToAll("applyperish.heard", target.getNickname());
        }
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        --this.effectTurns;
        pw.bc.sendToAll("pixelmon.status.perishsongstruck", pw.getNickname(), this.effectTurns);
        if (this.effectTurns <= 0) {
            pw.doBattleDamage(pw, pw.getHealth(), DamageTypeEnum.STATUS);
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (pw.getParticipant().countAblePokemon() < pw.bc.rules.battleType.numPokemon * 2) {
            return;
        }
        int userStages = 0;
        int opponentStages = 0;
        for (PixelmonWrapper pixelmonWrapper : pw.getTeamPokemon()) {
            if (!BattleParticipant.canSwitch(pixelmonWrapper)[0]) {
                userChoice.raiseWeight(-100.0f);
            }
            if (pixelmonWrapper.getBattleAbility() instanceof Soundproof) continue;
            userStages += pixelmonWrapper.getBattleStats().getSumStages();
        }
        for (PixelmonWrapper pixelmonWrapper : pw.getOpponentPokemon()) {
            if (pixelmonWrapper.getBattleAbility() instanceof Soundproof) {
                return;
            }
            if (!BattleParticipant.canSwitch(pixelmonWrapper)[0]) {
                userChoice.raiseWeight(100.0f);
            }
            opponentStages += pixelmonWrapper.getBattleStats().getSumStages();
        }
        if (opponentStages > 1) {
            userChoice.raiseWeight((opponentStages - userStages) * 20);
        }
        for (BattleParticipant battleParticipant : pw.getParticipant().getOpponents()) {
            if (battleParticipant.countAblePokemon() > pw.bc.rules.battleType.numPokemon) continue;
            userChoice.raiseWeight(100.0f);
        }
    }
}

