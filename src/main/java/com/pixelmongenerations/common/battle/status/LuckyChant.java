/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import java.util.ArrayList;

public class LuckyChant
extends StatusBase {
    transient int duration = 5;

    public LuckyChant() {
        super(StatusType.LuckyChant);
    }

    @Override
    public void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.targetIndex == 0 || user.bc.simulateMode) {
            if (user.addTeamStatus(new LuckyChant(), user)) {
                user.bc.sendToAll("pixelmon.status.luckychant", user.getNickname());
            } else {
                user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
                user.attack.moveResult.result = AttackResult.failed;
            }
        }
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (--this.duration <= 0) {
            pw.bc.sendToAll("pixelmon.status.luckychantend", pw.getNickname());
            pw.removeTeamStatus(this);
        }
    }

    @Override
    public boolean isTeamStatus() {
        return true;
    }

    @Override
    public StatusBase copy() {
        LuckyChant copy = new LuckyChant();
        copy.duration = this.duration;
        return copy;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        userChoice.raiseWeight(10.0f);
    }
}

