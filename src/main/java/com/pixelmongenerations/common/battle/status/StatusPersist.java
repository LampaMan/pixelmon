/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.NoStatus;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.network.ChatHandler;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.TextComponentTranslation;

public abstract class StatusPersist
extends StatusBase {
    public StatusPersist(StatusType type) {
        super(type);
    }

    public void writeToNBT(NBTTagCompound nbt) {
        nbt.setInteger("Status", this.type.ordinal());
    }

    public abstract StatusPersist restoreFromNBT(NBTTagCompound var1);

    public static StatusPersist readStatusFromNBT(NBTTagCompound nbt) {
        StatusPersist status;
        block3: {
            int statusCount;
            block2: {
                status = NoStatus.noStatus;
                statusCount = 0;
                if (!nbt.hasKey("Status")) break block2;
                StatusPersist s = StatusType.getEffectInstance(nbt.getInteger("Status"));
                if (s == null) break block3;
                status = s.restoreFromNBT(nbt);
                break block3;
            }
            if (nbt.hasKey("StatusCount")) {
                statusCount = nbt.getShort("StatusCount");
                for (int i = 0; i < statusCount; ++i) {
                    StatusPersist s = StatusType.getEffectInstance(nbt.getInteger("Status" + i));
                    if (s == null) continue;
                    status = s.restoreFromNBT(nbt);
                    break;
                }
            }
        }
        return status;
    }

    protected boolean addStatus(PixelmonWrapper user, PixelmonWrapper target, Attack attack, boolean showMessage, String alreadyMessage, String addMessage) {
        boolean result;
        boolean isStatusMove = attack != null && attack.getAttackCategory() == AttackCategory.Status;
        boolean bl = isStatusMove;
        if (target.hasStatus(StatusType.Sleep)) {
            if (showMessage && isStatusMove) {
                user.bc.sendToAll(alreadyMessage, target.getNickname());
                user.setAttackFailed();
            }
            return false;
        }
        if (target.hasPrimaryStatus() || this.isImmune(target)) {
            if (showMessage && isStatusMove) {
                user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
                user.setAttackFailed();
            }
            return false;
        }
        TextComponentTranslation message = null;
        if (showMessage) {
            message = ChatHandler.getMessage(addMessage, target.getNickname());
        }
        if (!(result = target.addStatus(this, user, message)) && isStatusMove) {
            user.setAttackFailed();
        }
        return result;
    }

    protected boolean alreadyHasStatus(PixelmonWrapper pw) {
        return pw.hasStatus(this.type);
    }
}

