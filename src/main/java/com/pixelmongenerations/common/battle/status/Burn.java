/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusPersist;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.FlareBoost;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Guts;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Heatproof;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MarvelScale;
import com.pixelmongenerations.common.entity.pixelmon.abilities.QuickFeet;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Synchronize;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.network.ChatHandler;
import java.util.ArrayList;
import java.util.Iterator;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.TextComponentTranslation;

public class Burn
extends StatusPersist {
    private static final float AI_WEIGHT = 25.0f;

    public Burn() {
        super(StatusType.Burn);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (this.checkChance()) {
            Burn.burn(user, target, user.attack, true);
        }
    }

    public static boolean burn(PixelmonWrapper user, PixelmonWrapper target, Attack attack, boolean showMessage) {
        boolean result;
        boolean isStatusMove = attack != null && attack.getAttackCategory() == AttackCategory.Status;
        if (target.hasType(EnumType.Fire)) {
            if (showMessage && isStatusMove) {
                user.bc.sendToAll("pixelmon.battletext.noeffect", target.getNickname());
                user.setAttackFailed();
            }
            return false;
        }
        if (target.hasPrimaryStatus()) {
            if (showMessage && isStatusMove) {
                user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
                user.setAttackFailed();
            }
            return false;
        }
        TextComponentTranslation message = null;
        if (showMessage) {
            message = ChatHandler.getMessage("pixelmon.effect.burnt", target.getNickname());
        }
        if (!(result = target.addStatus(new Burn(), user, message)) && isStatusMove) {
            user.setAttackFailed();
        }
        return result;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        AbilityBase userAbility = pw.getBattleAbility();
        if (!(userAbility instanceof MagicGuard)) {
            pw.bc.sendToAll("pixelmon.status.burnhurt", pw.getNickname());
            int fraction = userAbility instanceof Heatproof ? 32 : 16;
            pw.doBattleDamage(pw, pw.getPercentMaxHealth(100.0f / (float)fraction), DamageTypeEnum.STATUS);
        }
    }

    @Override
    public StatusPersist restoreFromNBT(NBTTagCompound nbt) {
        return new Burn();
    }

    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        int a;
        if (user.attack != null) {
            if (user.attack.isAttack("Facade")) {
                return stats;
            }
        }
        if (user.getBattleAbility() instanceof Guts) {
            return stats;
        }
        int n = a = StatsType.Attack.getStatIndex();
        stats[n] = stats[n] / 2;
        return stats;
    }

    @Override
    public boolean isImmune(PixelmonWrapper pokemon) {
        return pokemon.hasType(EnumType.Fire);
    }

    @Override
    public String getCureMessage() {
        return "pixelmon.status.burncure";
    }

    @Override
    public String getCureMessageItem() {
        return "pixelmon.status.burncureitem";
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        boolean offensive = userChoice.isOffensiveMove();
        float weight = this.getWeightWithChance(25.0f);
        if (!offensive || userChoice.isMiddleTier()) {
            boolean hitsAlly = userChoice.hitsAlly();
            if (!offensive || !hitsAlly) {
                ArrayList<PixelmonWrapper> opponents = pw.getOpponentPokemon();
                Iterator<PixelmonWrapper> var11 = userChoice.targets.iterator();
                while (true) {
                    boolean beneficial;
                    if (!var11.hasNext()) {
                        return;
                    }
                    PixelmonWrapper target = var11.next();
                    if (offensive && !Burn.burn(pw, target, userChoice.attack, false)) continue;
                    AbilityBase ability = target.getBattleAbility();
                    boolean bl = beneficial = ability instanceof FlareBoost || ability instanceof Guts || ability instanceof MarvelScale || ability instanceof QuickFeet;
                    if (!beneficial || !hitsAlly) {
                        if (beneficial ^ hitsAlly) {
                            userChoice.raiseWeight(-weight);
                            continue;
                        }
                        if (ability instanceof Synchronize) continue;
                        pw.getBattleAI().weightStatusOpponentOptions(pw, userChoice, target, new Burn(), opponents, bestOpponentChoices);
                        if (ability instanceof MagicGuard) continue;
                        userChoice.raiseWeight(weight);
                        continue;
                    }
                    if (!(ability instanceof Guts) && target.getMoveset().hasAttackCategory(AttackCategory.Physical)) continue;
                    userChoice.raiseWeight(weight);
                }
            }
        }
    }
}

