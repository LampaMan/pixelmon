/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class MagicCoat
extends StatusBase {
    public MagicCoat() {
        super(StatusType.MagicCoat);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        user.bc.sendToAll("pixelmon.status.applymagiccoat", user.getNickname());
        user.addStatus(new MagicCoat(), user);
    }

    @Override
    public boolean stopsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user) {
        Attack a = user.attack;
        return MagicCoat.reflectMove(a, pokemon, user, "pixelmon.status.magiccoat");
    }

    public static boolean reflectMove(Attack a, PixelmonWrapper pokemon, PixelmonWrapper user, String message) {
        block5: {
            block4: {
                if (a.isAttack("Parting Shot")) break block4;
                if (a.getAttackCategory() != AttackCategory.Status) break block5;
                if (a.isAttack("Bestow", "Curse", "Guard Swap", "Heart Swap", "Lock-On", "Memento", "Mimic", "Power Swap", "Psych Up", "Psycho Shift", "Role Play", "Skill Swap", "Switcheroo", "Transform", "Trick", "Pain Split") || a.getAttackBase().targetingInfo.hitsAll && a.getAttackBase().targetingInfo.hitsSelf) break block5;
            }
            user.bc.sendToAll(message, pokemon.getNickname());
            pokemon.targetIndex = 0;
            if (a.hasNoEffect(pokemon, user)) {
                user.bc.sendToAll("pixelmon.battletext.noeffect", user.getNickname());
                return true;
            }
            a.applyAttackEffect(pokemon, user);
            return true;
        }
        return false;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        pw.removeStatus(this);
    }
}

