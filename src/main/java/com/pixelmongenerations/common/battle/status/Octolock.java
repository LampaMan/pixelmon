/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class Octolock
extends StatusBase {
    transient PixelmonWrapper locker;

    public Octolock() {
        super(StatusType.Octolock);
    }

    public Octolock(PixelmonWrapper locker) {
        super(StatusType.Octolock);
        this.locker = locker;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.addStatus(new Octolock(user), user)) {
            user.bc.sendToAll("pixelmon.effect.noescape", target.getNickname());
        } else {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        }
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (this.locker.isFainted() || !pw.bc.isInBattle(this.locker)) {
            pw.removeStatus(this);
        }
        pw.getBattleStats().modifyStat(-1, StatsType.Defence, pw, false);
        pw.getBattleStats().modifyStat(-1, StatsType.SpecialDefence, pw, false);
    }

    @Override
    public boolean stopsSwitching() {
        return true;
    }
}

