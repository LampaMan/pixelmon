/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;

public class Cursed
extends StatusBase {
    public Cursed() {
        super(StatusType.Cursed);
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (pw.getBattleAbility() instanceof MagicGuard) {
            return;
        }
        pw.bc.sendToAll("pixelmon.status.curseafflicted", pw.getNickname());
        pw.doBattleDamage(pw, pw.getPercentMaxHealth(25.0f), DamageTypeEnum.STATUS);
    }
}

