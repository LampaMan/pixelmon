/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.GlobalStatusController;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.HarshSunlight;
import com.pixelmongenerations.common.battle.status.HeavyRain;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.StrongWinds;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.ArrayList;

public abstract class Weather
extends GlobalStatusBase {
    protected transient int turnsToGo;
    protected transient EnumHeldItems weatherRock;
    protected transient String langStart;
    protected transient String langContinue;
    protected transient String langEnd;

    public Weather(StatusType type, int turnsToGo, EnumHeldItems weatherRock, String langStart, String langContinue, String langEnd) {
        super(type);
        this.turnsToGo = turnsToGo;
        this.weatherRock = weatherRock;
        this.langStart = langStart;
        this.langContinue = langContinue;
        this.langEnd = langEnd;
    }

    @Override
    public void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        Weather weather = user.bc.globalStatusController.getWeatherIgnoreAbility();
        if (Weather.isWeatherTrioStatus(weather) || weather != null && weather.type == this.type) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        } else {
            int turns = this.getStartTurns(user);
            user.bc.sendToAll(this.langStart, new Object[0]);
            user.bc.globalStatusController.addGlobalStatus(this.getNewInstance(turns));
        }
    }

    protected int getStartTurns(PixelmonWrapper user) {
        return user.getUsableHeldItem().getHeldItemType() == this.weatherRock ? 8 : 5;
    }

    public void setStartTurns(PixelmonWrapper user) {
        this.turnsToGo = this.getStartTurns(user);
    }

    protected abstract Weather getNewInstance(int var1);

    @Override
    public void applyRepeatedEffect(GlobalStatusController global) {
        if (this.turnsToGo != -1) {
            int simulate = this.turnsToGo - 1;
            if (simulate == 0) {
                global.removeGlobalStatus(this);
                global.bc.sendToAll(this.langEnd, new Object[0]);
                return;
            }
            this.turnsToGo = simulate;
        }
        this.applyRepeatedEffect(global.bc);
    }

    protected void applyRepeatedEffect(BattleControllerBase bc) {
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        int allyBenefits = 0;
        int opponentBenefits = 0;
        Weather currentWeather = pw.bc.globalStatusController.getWeather();
        for (PixelmonWrapper opponent : pw.getTeamPokemon()) {
            allyBenefits += this.countBenefits(pw, opponent);
            if (currentWeather == null) continue;
            allyBenefits -= currentWeather.countBenefits(pw, opponent);
        }
        for (PixelmonWrapper opponent : pw.getOpponentPokemon()) {
            opponentBenefits += this.countBenefits(pw, opponent);
            if (currentWeather == null) continue;
            opponentBenefits -= currentWeather.countBenefits(pw, opponent);
        }
        if (allyBenefits > opponentBenefits) {
            userChoice.raiseWeight(40 * (allyBenefits - opponentBenefits));
        }
    }

    public String getLangEnd() {
        return this.langEnd;
    }

    public String getLangStart() {
        return this.langStart;
    }

    public static boolean isWeatherTrioStatus(Weather weather) {
        return weather instanceof HarshSunlight || weather instanceof HeavyRain || weather instanceof StrongWinds;
    }

    protected abstract int countBenefits(PixelmonWrapper var1, PixelmonWrapper var2);
}

