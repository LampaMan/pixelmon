/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public class BideStatus
extends StatusBase {
    public transient float damageTaken;
    public transient PixelmonWrapper lastAttacker;

    public BideStatus() {
        super(StatusType.Bide);
    }

    @Override
    public void onDamageReceived(PixelmonWrapper userWrapper, PixelmonWrapper pokemon, Attack a, int damage, DamageTypeEnum damageType) {
        if (!userWrapper.bc.simulateMode && damageType == DamageTypeEnum.ATTACK || damageType == DamageTypeEnum.ATTACKFIXED) {
            this.damageTaken += (float)damage;
            this.lastAttacker = userWrapper;
        }
    }
}

