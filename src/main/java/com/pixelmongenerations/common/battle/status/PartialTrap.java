/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.AttackBase;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;

public class PartialTrap
extends StatusBase {
    public transient AttackBase variant;
    private transient int turnsToGo;
    private transient PixelmonWrapper trapper;

    public PartialTrap() {
        super(StatusType.PartialTrap);
    }

    public PartialTrap(AttackBase variant, PixelmonWrapper trapper) {
        super(StatusType.PartialTrap);
        this.variant = variant;
        this.trapper = trapper;
        this.turnsToGo = trapper.getUsableHeldItem().getHeldItemType() == EnumHeldItems.gripClaw ? 7 : RandomHelper.getRandomNumberBetween(4, 5);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        AttackBase variant = user.attack.getAttackBase();
        if (target.addStatus(new PartialTrap(variant, user), user)) {
            if (variant.isAttack("Sand Tomb")) {
                user.bc.sendToAll("pixelmon.effect.sandtomb", target.getNickname());
            } else if (variant.isAttack("Clamp")) {
                user.bc.sendToAll("pixelmon.effect.clamped", user.getNickname(), target.getNickname());
            } else if (variant.isAttack("Magma Storm")) {
                user.bc.sendToAll("pixelmon.effect.swirlingmagma", target.getNickname());
            } else if (variant.isAttack("Fire Spin")) {
                user.bc.sendToAll("pixelmon.effect.firevortex", target.getNickname());
            } else if (variant.isAttack("Bind")) {
                user.bc.sendToAll("pixelmon.effect.squeezed", target.getNickname(), user.getNickname());
            } else if (variant.isAttack("Wrap")) {
                user.bc.sendToAll("pixelmon.effect.wrapped", target.getNickname(), user.getNickname());
            } else if (variant.isAttack("Whirlpool")) {
                user.bc.sendToAll("pixelmon.effect.vortex", target.getNickname());
            } else if (variant.isAttack("Snap Trap")) {
                user.bc.sendToAll("pixelmon.effect.snaptrap", target.getNickname());
            } else if (variant.isAttack("Thunder Cage")) {
                user.bc.sendToAll("pixelmon.effect.thundercage", target.getNickname());
            } else if (variant.isAttack("G-Max Centiferno")) {
                user.bc.sendToAll("pixelmon.effect.gmaxcentiferno", target.getNickname());
            } else if (variant.isAttack("G-Max Sandblast")) {
                user.bc.sendToAll("pixelmon.effect.gmaxsandblast", target.getNickname());
            } else if (variant.isAttack("G-Max Cannonade")) {
                user.bc.sendToAll("pixelmon.effect.gmaxcannonade", target.getNickname());
            } else if (variant.isAttack("G-Max Wildfire")) {
                user.bc.sendToAll("pixelmon.effect.gmaxwildfire", target.getNickname());
            } else if (variant.isAttack("G-Max Vine Lash")) {
                user.bc.sendToAll("pixelmon.effect.gmaxvinelash", target.getNickname());
            } else if (variant.isAttack("G-Max Volcalith")) {
                user.bc.sendToAll("pixelmon.effect.gmaxvolcalith", target.getNickname());
            }
        }
    }

    @Override
    public boolean stopsSwitching() {
        return !this.variant.isAttack("G-Max Wildfire") && !this.variant.isAttack("G-Max Cannonade") && !this.variant.isAttack("G-Max Vine Lash") && !this.variant.isAttack("G-Max Volcalith");
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (!(pw.bc.getActiveUnfaintedPokemon().contains(this.trapper) || this.variant.isAttack("G-Max Centiferno") || this.variant.isAttack("G-Max Sandblast"))) {
            pw.removeStatus(this);
        } else if (--this.turnsToGo <= 0) {
            pw.removeStatus(this);
            if (this.variant.isAttack("Sand Tomb")) {
                pw.bc.sendToAll("pixelmon.status.freefromsandtomb", pw.getNickname());
            } else if (this.variant.isAttack("Clamp")) {
                pw.bc.sendToAll("pixelmon.status.freefromclamp", pw.getNickname());
            } else if (this.variant.isAttack("Whirlpool")) {
                pw.bc.sendToAll("pixelmon.status.freefromwhirlpool", pw.getNickname());
            } else if (this.variant.isAttack("Wrap")) {
                pw.bc.sendToAll("pixelmon.status.freefromwrap", pw.getNickname());
            } else if (this.variant.isAttack("Bind")) {
                pw.bc.sendToAll("pixelmon.status.freefrombind", pw.getNickname());
            } else if (this.variant.isAttack("Fire Spin")) {
                pw.bc.sendToAll("pixelmon.status.freefromfirespin", pw.getNickname());
            } else if (this.variant.isAttack("Thunder Cage")) {
                pw.bc.sendToAll("pixelmon.status.freefromthundercage", pw.getNickname());
            } else if (this.variant.isAttack("G-Max Centiferno")) {
                pw.bc.sendToAll("pixelmon.status.freefromcentiferno", pw.getNickname());
            } else if (this.variant.isAttack("G-Max Sandblast")) {
                pw.bc.sendToAll("pixelmon.status.freefromsandblast", pw.getNickname());
            } else if (this.variant.isAttack("G-Max Cannonade")) {
                pw.bc.sendToAll("pixelmon.status.freefromcannonade", pw.getNickname());
            } else if (this.variant.isAttack("G-Max Wildfire")) {
                pw.bc.sendToAll("pixelmon.status.freefromwildfire", pw.getNickname());
            } else if (this.variant.isAttack("G-Max Vine Lash")) {
                pw.bc.sendToAll("pixelmon.status.freefromvinelash", pw.getNickname());
            } else if (this.variant.isAttack("G-Max Volcalith")) {
                pw.bc.sendToAll("pixelmon.status.freefromvolcalith", pw.getNickname());
            } else if (this.variant.isAttack("Snap Trap")) {
                pw.bc.sendToAll("pixelmon.status.freefromsnaptrap", pw.getNickname());
            }
        } else if (!(pw.getBattleAbility() instanceof MagicGuard)) {
            int fraction;
            int n = fraction = this.trapper.getUsableHeldItem().getHeldItemType() == EnumHeldItems.bindingBand || this.variant.isAttack("G-Max Cannonade") || this.variant.isAttack("G-Max Wildfire") || this.variant.isAttack("G-Max Vine Lash") || this.variant.isAttack("G-Max Volcalith") ? 6 : 8;
            if (this.variant.isAttack("G-Max Cannonade")) {
                if (pw.hasType(EnumType.Water)) {
                    pw.bc.sendToAll("pixelmon.status.noteffectedbycannonade", pw.getNickname());
                    return;
                }
            }
            if (this.variant.isAttack("G-Max Wildfire")) {
                if (pw.hasType(EnumType.Fire)) {
                    pw.bc.sendToAll("pixelmon.status.noteffectedbywildfire", pw.getNickname());
                    return;
                }
            }
            if (this.variant.isAttack("G-Max Vine Lash")) {
                if (pw.hasType(EnumType.Grass)) {
                    pw.bc.sendToAll("pixelmon.status.noteffectedbyvinelash", pw.getNickname());
                    return;
                }
            }
            if (this.variant.isAttack("G-Max Volcalith")) {
                if (pw.hasType(EnumType.Rock)) {
                    pw.bc.sendToAll("pixelmon.status.noteffectedbyvolcalith", pw.getNickname());
                    return;
                }
            }
            pw.doBattleDamage(pw, pw.getPercentMaxHealth(100.0f / (float)fraction), DamageTypeEnum.STATUS);
            pw.bc.sendToAll("pixelmon.status.hurtby", pw.getNickname(), this.variant.getLocalizedName());
        }
    }

    @Override
    public boolean stopsStatusChange(StatusType t, PixelmonWrapper target, PixelmonWrapper user) {
        return t.isStatus(StatusType.PartialTrap);
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (!userChoice.hitsAlly()) {
            for (PixelmonWrapper target : userChoice.targets) {
                if (target.getBattleAbility() instanceof MagicGuard) {
                    return;
                }
                float weight = 12.5f;
                if (pw.getUsableHeldItem().getHeldItemType() == EnumHeldItems.bindingBand) {
                    weight *= 2.0f;
                }
                userChoice.raiseWeight(weight);
            }
        }
    }
}

