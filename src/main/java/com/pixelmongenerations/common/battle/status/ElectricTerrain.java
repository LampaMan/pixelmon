/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Sleep;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Terrain;
import com.pixelmongenerations.common.battle.status.Yawn;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Levitate;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.List;

public class ElectricTerrain
extends Terrain {
    public ElectricTerrain() {
        super(StatusType.ElectricTerrain, "pixelmon.status.electricterrain", "pixelmon.status.electricterraincontinue", "pixelmon.status.electricterrainend");
    }

    @Override
    public Terrain getNewInstance() {
        return new ElectricTerrain();
    }

    @Override
    public int[] modifyPowerAndAccuracyTarget(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (this.affectsPokemon(user) && a.getAttackBase().attackType == EnumType.Electric) {
            power = (int)((double)power * 1.3);
        }
        return new int[]{power, accuracy};
    }

    @Override
    public boolean stopsStatusChange(StatusType t, PixelmonWrapper target, PixelmonWrapper user) {
        block10: {
            block12: {
                block11: {
                    block8: {
                        block9: {
                            if (target.isGrounded()) break block8;
                            if (target.type.contains((Object)EnumType.Flying) || target.getBattleAbility() instanceof Levitate || target.getUsableHeldItem().getHeldItemType() == EnumHeldItems.airBalloon) break block9;
                            if (!target.hasStatus(StatusType.MagnetRise, StatusType.Telekinesis)) break block8;
                        }
                        return false;
                    }
                    if (!this.affectsPokemon(target)) break block10;
                    if (!this.affectsPokemon(user)) break block11;
                    if (t.isStatus(StatusType.Sleep, StatusType.Yawn)) break block12;
                }
                return false;
            }
            if (t.isStatus(StatusType.Sleep, StatusType.Yawn)) {
                if (user != target && user.attack.getAttackCategory() == AttackCategory.Status) {
                    target.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
                }
                return true;
            }
        }
        return false;
    }

    @Override
    protected int countBenefits(PixelmonWrapper user, PixelmonWrapper target) {
        int benefits = 0;
        if (this.affectsPokemon(target)) {
            List<Attack> moveset = user.getBattleAI().getMoveset(target);
            if (Attack.hasOffensiveAttackType(moveset, EnumType.Electric)) {
                ++benefits;
            }
            if (target.hasStatus(StatusType.Yawn)) {
                ++benefits;
            }
            if (Attack.hasAttack(moveset, "Rest")) {
                --benefits;
            }
            block0: for (Attack move : moveset) {
                for (EffectBase e : move.getAttackBase().effects) {
                    if (!(e instanceof Sleep) && !(e instanceof Yawn)) continue;
                    --benefits;
                    continue block0;
                }
            }
        }
        return benefits;
    }
}

