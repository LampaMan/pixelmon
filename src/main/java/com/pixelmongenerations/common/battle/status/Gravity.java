/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SmackDown;
import com.pixelmongenerations.common.battle.controller.GlobalStatusController;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Levitate;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Gravity
extends GlobalStatusBase {
    private transient int duration = 5;

    public Gravity() {
        super(StatusType.Gravity);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.globalStatusController.hasStatus(StatusType.Gravity)) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        } else {
            user.bc.sendToAll("pixelmon.status.gravity", new Object[0]);
            user.bc.globalStatusController.addGlobalStatus(new Gravity());
            for (PixelmonWrapper pokemon : user.bc.getActiveUnfaintedPokemon()) {
                if (pokemon.hasStatus(StatusType.Flying)) {
                    pokemon.canAttack = false;
                }
                if (!pokemon.removeStatuses(StatusType.Flying, StatusType.MagnetRise, StatusType.Telekinesis, StatusType.SkyDropped, StatusType.SkyDropping)) {
                    if (!pokemon.hasType(EnumType.Flying) && !(pokemon.getBattleAbility() instanceof Levitate) && pokemon.getUsableHeldItem().getHeldItemType() != EnumHeldItems.airBalloon) continue;
                }
                if (pokemon.hasStatus(StatusType.SmackedDown)) continue;
                user.bc.sendToAll("pixelmon.status.gravityaffected", pokemon.getNickname());
            }
        }
    }

    @Override
    public void applyEffectOnSwitch(PixelmonWrapper pw) {
        if (pw.hasType(EnumType.Flying) || pw.getBattleAbility() instanceof Levitate || pw.getUsableHeldItem().getHeldItemType() == EnumHeldItems.airBalloon) {
            pw.bc.sendToAll("pixelmon.status.gravityaffected", pw.getNickname());
        }
    }

    @Override
    public boolean stopsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user) {
        return user.attack.isAttack("Bounce", "Fly", "Flying Press", "Hi Jump Kick", "Jump Kick", "Magnet Rise", "Sky Drop", "Splash", "Telekinesis");
    }

    @Override
    public void stopsIncomingAttackMessage(PixelmonWrapper pokemon, PixelmonWrapper user) {
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
    }

    @Override
    public List<EnumType> getEffectiveTypes(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.attack != null && user.attack.getAttackBase().attackType == EnumType.Ground) {
            ArrayList types = (ArrayList)target.type.stream().filter(type -> type != EnumType.Flying).collect(Collectors.toList());
            if (types.size() == 0) {
                types.add(EnumType.Normal);
            }
            return types;
        }
        return target.type;
    }

    @Override
    public void applyRepeatedEffect(GlobalStatusController globalStatusController) {
        --this.duration;
        if (this.duration <= 0) {
            globalStatusController.bc.sendToAll("pixelmon.status.gravityend", new Object[0]);
            globalStatusController.removeGlobalStatus(this);
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        SmackDown smackDown = new SmackDown();
        smackDown.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
        StatsEffect statsEffect = new StatsEffect(StatsType.Evasion, -2, false);
        statsEffect.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
    }
}

