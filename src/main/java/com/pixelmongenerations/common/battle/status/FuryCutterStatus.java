/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public class FuryCutterStatus
extends StatusBase {
    public transient int power = 40;
    public transient int turnInc;

    public FuryCutterStatus(int turnInc) {
        super(StatusType.FuryCutter);
        this.turnInc = turnInc;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (this.turnInc != pw.bc.battleTurn) {
            pw.removeStatus(this);
        }
    }
}

