/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.EntryHazard;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.heldItems.ItemHeavyDutyBoots;
import java.util.ArrayList;

public class StickyWeb
extends EntryHazard {
    public StickyWeb() {
        super(StatusType.StickyWeb, 1);
    }

    @Override
    protected void doEffect(PixelmonWrapper pw) {
        PixelmonWrapper opponent = pw;
        ArrayList<PixelmonWrapper> opponents = pw.getOpponentPokemon();
        if (!opponents.isEmpty()) {
            opponent = opponents.get(0);
        }
        pw.getBattleStats().modifyStat(-1, StatsType.Speed, opponent, false);
    }

    @Override
    public boolean isUnharmed(PixelmonWrapper pw) {
        if (pw.getHeldItem() instanceof ItemHeavyDutyBoots) {
            return true;
        }
        return this.isAirborne(pw);
    }

    @Override
    public EntryHazard getNewInstance() {
        return new StickyWeb();
    }

    @Override
    protected String getFirstLayerMessage() {
        return "pixelmon.status.stickyweb";
    }

    @Override
    protected String getAffectedMessage() {
        return "pixelmon.status.stickywebcaught";
    }

    @Override
    public int getAIWeight() {
        return 20;
    }
}

