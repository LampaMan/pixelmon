/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import java.util.ArrayList;

public class DestinyBond
extends StatusBase {
    public DestinyBond() {
        super(StatusType.DestinyBond);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.lastAttack != null) {
            if (user.lastAttack.isAttack("Destiny Bond")) {
                user.bc.sendToAll("pixelmon.battletext.movefailed", new Object[0]);
                return;
            }
        }
        if (user.getStatus(StatusType.DestinyBond) == null && !user.isDynamaxed()) {
            user.addStatus(new DestinyBond(), user);
            user.bc.sendToAll("pixelmon.effect.applydestinybond", user.getNickname());
        } else {
            user.removeStatus(StatusType.DestinyBond);
        }
    }

    @Override
    public void onAttackUsed(PixelmonWrapper user, Attack attack) {
        user.removeStatus(this);
    }

    @Override
    public void onDamageReceived(PixelmonWrapper user, PixelmonWrapper pokemon, Attack a, int damage, DamageTypeEnum damagetype) {
        if (pokemon.isFainted() && (damagetype == DamageTypeEnum.ATTACK || damagetype == DamageTypeEnum.ATTACKFIXED) && !user.bc.getTeamPokemon(pokemon.getParticipant()).contains(user)) {
            user.bc.sendToAll("pixelmon.effect.destinybond", pokemon.getNickname());
            user.doBattleDamage(pokemon, user.getHealth(), DamageTypeEnum.STATUS);
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (MoveChoice.canOutspeedAnd2HKO(bestOpponentChoices, pw, userChoice.createList())) {
            userChoice.raiseWeight(75.0f);
        }
    }
}

