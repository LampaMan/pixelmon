/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public class HealingWish
extends StatusBase {
    public HealingWish() {
        super(StatusType.HealingWish);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.getParticipant().hasMorePokemonReserve()) {
            user.addStatus(new HealingWish(), user);
            user.doBattleDamage(user, user.getHealth(), DamageTypeEnum.SELF);
        } else {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        }
    }

    @Override
    public boolean isTeamStatus() {
        return true;
    }

    @Override
    public boolean isWholeTeamStatus() {
        return false;
    }

    @Override
    public void applyEffectOnSwitch(PixelmonWrapper pw) {
        pw.bc.sendToAll("pixelmon.effect.healingwish", new Object[0]);
        boolean didHeal = false;
        if (pw.getMaxHealth() > pw.getHealth()) {
            pw.healEntityBy(pw.getHealthDeficit());
            didHeal = true;
        }
        for (int i = 0; i < pw.getStatusSize(); ++i) {
            StatusType currentStatus = pw.getStatus((int)i).type;
            if (!currentStatus.isPrimaryStatus() && currentStatus != StatusType.HealingWish) continue;
            if (currentStatus != StatusType.HealingWish) {
                didHeal = true;
            }
            pw.removeStatus(i);
            --i;
        }
        if (!didHeal) {
            pw.bc.sendToAll("pixelmon.effect.healfailed", pw.getNickname());
        }
    }
}

