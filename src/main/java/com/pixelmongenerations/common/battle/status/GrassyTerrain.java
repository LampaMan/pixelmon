/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.GlobalStatusController;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Terrain;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Levitate;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.List;

public class GrassyTerrain
extends Terrain {
    private static final String[] groundMoves = new String[]{"Bulldoze", "Earthquake", "Magnitude"};

    public GrassyTerrain() {
        super(StatusType.GrassyTerrain, "pixelmon.status.grassyterrain", "pixelmon.status.grassyterraincontinue", "pixelmon.status.grassyterrainend");
    }

    @Override
    public Terrain getNewInstance() {
        return new GrassyTerrain();
    }

    @Override
    public int[] modifyPowerAndAccuracyTarget(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (this.affectsPokemon(user) && a.getAttackBase().attackType == EnumType.Grass) {
            power = (int)((double)power * 1.3);
        }
        if (a.isAttack(groundMoves)) {
            power = (int)((double)power * 0.5);
        }
        return new int[]{power, accuracy};
    }

    @Override
    public void applyRepeatedEffect(GlobalStatusController gsc) {
        if (gsc.hasStatus(this.type)) {
            for (PixelmonWrapper p : gsc.bc.getDefaultTurnOrder()) {
                if (p.hasFullHealth() || p.isFainted()) continue;
                if (p.hasStatus(StatusType.HealBlock)) continue;
                if (!p.isGrounded()) {
                    if (p.type.contains((Object)EnumType.Flying) || p.getBattleAbility() instanceof Levitate || p.getUsableHeldItem().getHeldItemType() == EnumHeldItems.airBalloon) continue;
                    if (p.hasStatus(StatusType.MagnetRise, StatusType.Telekinesis)) continue;
                }
                p.healByPercent(6.25f);
                p.bc.sendToAll("pixelmon.effect.restorehealth", p.getNickname());
            }
        }
        super.applyRepeatedEffect(gsc);
    }

    @Override
    protected int countBenefits(PixelmonWrapper user, PixelmonWrapper target) {
        int benefits = 0;
        List<Attack> moveset = user.getBattleAI().getMoveset(target);
        if (this.affectsPokemon(target)) {
            if (Attack.hasOffensiveAttackType(moveset, EnumType.Grass)) {
                ++benefits;
            }
            ++benefits;
        }
        if (Attack.hasAttack(moveset, groundMoves)) {
            --benefits;
        }
        return benefits;
    }
}

