/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.status.StatusPersist;
import com.pixelmongenerations.common.battle.status.StatusType;
import net.minecraft.nbt.NBTTagCompound;

public class NoStatus
extends StatusPersist {
    public static final NoStatus noStatus = new NoStatus();

    private NoStatus() {
        super(StatusType.None);
    }

    @Override
    public StatusPersist restoreFromNBT(NBTTagCompound nbt) {
        return noStatus;
    }

    @Override
    public void writeToNBT(NBTTagCompound nbt) {
        nbt.removeTag("Status");
    }
}

