/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Steadfast;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import java.util.ArrayList;

public class Flinch
extends StatusBase {
    public Flinch() {
        super(StatusType.Flinch);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.isDynamaxed()) {
            return;
        }
        if (this.checkChance()) {
            Flinch.flinch(user, target);
        }
    }

    public static void flinch(PixelmonWrapper user, PixelmonWrapper target) {
        if (!target.hasStatus(StatusType.Flinch) && target.bc != null && target.bc.battleLog.getActionForPokemon(target.bc.battleTurn, target) == null && target.attack != null && !target.isDynamaxed()) {
            target.addStatus(new Flinch(), user);
        }
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        pw.removeStatus(this);
    }

    @Override
    public boolean canAttackThisTurn(PixelmonWrapper user, Attack a) {
        user.bc.sendToAll("battlecontroller.flinched", user.getNickname());
        user.removeStatus(this);
        AbilityBase ability = user.getBattleAbility();
        if (ability instanceof Steadfast) {
            ability.sendActivatedMessage(user);
            user.getBattleStats().modifyStat(1, StatsType.Speed);
        }
        return false;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        float chance = this.getChance();
        if (userChoice.hitsAlly()) {
            return;
        }
        for (PixelmonWrapper target : userChoice.targets) {
            if (MoveChoice.canOutspeed(bestOpponentChoices, pw, userChoice.createList()) || !target.addStatus(new Flinch(), pw)) continue;
            if (chance == 100.0f) {
                userChoice.raiseWeight(chance);
                continue;
            }
            if (chance >= 50.0f) {
                userChoice.raiseWeight(chance / 2.0f);
                continue;
            }
            userChoice.raiseWeight(chance / 100.0f);
        }
    }
}

