/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Sport;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.EnumType;

public class WaterSport
extends Sport {
    public WaterSport() {
        this((PixelmonWrapper)null);
    }

    public WaterSport(PixelmonWrapper user) {
        super(user, StatusType.WaterSport, EnumType.Fire, "Water Sport");
    }

    @Override
    protected Sport getNewInstance(PixelmonWrapper user) {
        return new WaterSport(user);
    }
}

