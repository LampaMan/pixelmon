/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public class UnderGround
extends StatusBase {
    public UnderGround() {
        super(StatusType.UnderGround);
    }

    @Override
    public boolean stopsSwitching() {
        return true;
    }

    @Override
    public boolean stopsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user) {
        if (user.attack.isAttack("Earthquake", "Magnitude")) {
            user.attack.movePower *= 2;
            user.attack.moveAccuracy = -1;
            return false;
        }
        if (user.attack.isAttack("Fissure")) {
            user.attack.moveAccuracy = -1;
            return false;
        }
        if (user.attack.canHitNoTarget()) {
            user.attack.moveAccuracy = -1;
            return false;
        }
        return user.attack.moveAccuracy != -2 && !pokemon.bc.simulateMode;
    }

    @Override
    public void stopsIncomingAttackMessage(PixelmonWrapper pokemon, PixelmonWrapper user) {
        user.bc.sendToAll("pixelmon.battletext.missedattack", pokemon.getNickname());
    }
}

