/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Levitate;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.ArrayList;

public abstract class EntryHazard
extends StatusBase {
    private transient int maxLayers;
    protected transient int numLayers = 1;

    public EntryHazard(StatusType type, int maxLayers) {
        super(type);
        this.maxLayers = maxLayers;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.targetIndex == 0 || user.bc.simulateMode) {
            int message = 0;
            EntryHazard hazard = (EntryHazard)target.getStatus(this.type);
            if (hazard == null) {
                target.addTeamStatus(this.getNewInstance(), user);
                message = 3;
            } else if (hazard.numLayers >= this.maxLayers) {
                message = 1;
            } else {
                if (!user.bc.simulateMode) {
                    ++hazard.numLayers;
                }
                message = 2;
            }
            switch (message) {
                case 1: {
                    user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
                    user.attack.moveResult.result = AttackResult.failed;
                    break;
                }
                case 2: {
                    user.bc.sendToAll(this.getMultiLayerMessage(), target.getNickname());
                    break;
                }
                case 3: {
                    user.bc.sendToAll(this.getFirstLayerMessage(), target.getNickname());
                }
            }
        }
    }

    public int getNumLayers() {
        return this.numLayers;
    }

    public abstract EntryHazard getNewInstance();

    protected abstract String getFirstLayerMessage();

    protected String getMultiLayerMessage() {
        return this.getFirstLayerMessage();
    }

    protected abstract String getAffectedMessage();

    @Override
    public boolean isTeamStatus() {
        return true;
    }

    @Override
    public void applyEffectOnSwitch(PixelmonWrapper pw) {
        if (!this.isUnharmed(pw)) {
            pw.bc.sendToAll(this.getAffectedMessage(), pw.getNickname());
            this.doEffect(pw);
        }
    }

    protected void doEffect(PixelmonWrapper pw) {
        pw.doBattleDamage(pw, this.getDamage(pw), DamageTypeEnum.STATUS);
    }

    public int getDamage(PixelmonWrapper pw) {
        return 0;
    }

    @Override
    public StatusBase copy() {
        EntryHazard copy = this.getNewInstance();
        copy.numLayers = this.numLayers;
        return copy;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    protected boolean isAirborne(PixelmonWrapper pw) {
        if (pw.isGrounded()) return false;
        if (pw.hasType(EnumType.Flying)) return true;
        if (pw.getBattleAbility() instanceof Levitate) {
            if (!this.ignoreLevitate(pw)) return true;
        }
        if (pw.getUsableHeldItem().getHeldItemType() == EnumHeldItems.airBalloon) return true;
        if (!pw.hasStatus(StatusType.MagnetRise, StatusType.Telekinesis)) return false;
        return true;
    }

    private boolean ignoreLevitate(PixelmonWrapper pw) {
        if (pw.bc.turn >= pw.bc.turnList.size()) {
            return false;
        }
        PixelmonWrapper attacker = pw.bc.turnList.get(pw.bc.turn);
        return AbilityBase.ignoreAbility(attacker, pw) && attacker.targets.get((int)0).battlePosition == pw.battlePosition && this.wasForcedSwitch(attacker);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private boolean wasForcedSwitch(PixelmonWrapper opponent) {
        if (opponent.attack == null) return false;
        if (!opponent.attack.isAttack("Circle Throw", "Dragon Tail", "Roar", "Whirlwind")) return false;
        if (opponent.attack.moveResult.result == AttackResult.hit) return true;
        if (opponent.attack.moveResult.result != AttackResult.proceed) return false;
        return true;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        int totalReserve = 0;
        for (BattleParticipant opponent : pw.getParticipant().getOpponents()) {
            totalReserve += opponent.countAblePokemon() - opponent.controlledPokemon.size();
        }
        userChoice.raiseWeight(totalReserve * this.getAIWeight());
    }

    public abstract int getAIWeight();

    protected abstract boolean isUnharmed(PixelmonWrapper var1);
}

