/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AuraBreak;
import com.pixelmongenerations.core.enums.EnumType;

public class AuraStatus
extends GlobalStatusBase {
    private transient EnumType boostType;

    public AuraStatus(EnumType boostType, StatusType auraStatus) {
        super(auraStatus);
        this.boostType = boostType;
    }

    @Override
    public int[] modifyPowerAndAccuracyTarget(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (a.getAttackBase().attackType == this.boostType) {
            float multiplier = 1.333f;
            for (PixelmonWrapper pw : user.bc.getActiveUnfaintedPokemon()) {
                if (!(pw.getBattleAbility() instanceof AuraBreak)) continue;
                multiplier = 0.666f;
                break;
            }
            power = (int)((float)power * multiplier);
        }
        return new int[]{power, accuracy};
    }
}

