/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Weather;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class StrongWinds
extends Weather {
    public StrongWinds() {
        super(StatusType.StrangeWinds, -1, EnumHeldItems.leppa, "pixelmon.effect.strongwindsstart", "pixelmon.effect.strongwindscontinue", "pixelmon.effect.strongwindsend");
    }

    @Override
    protected Weather getNewInstance(int var1) {
        return new StrongWinds();
    }

    @Override
    protected int countBenefits(PixelmonWrapper var1, PixelmonWrapper var2) {
        return 0;
    }
}

