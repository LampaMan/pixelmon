/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.ai.BattleAIBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import java.util.ArrayList;

public abstract class Split
extends StatusBase {
    transient StatsType stat1;
    transient StatsType stat2;
    transient int statValue1;
    transient int statValue2;
    transient String langString;

    public Split(StatusType type, StatsType stat1, StatsType stat2, String langString) {
        super(type);
        this.stat1 = stat1;
        this.stat2 = stat2;
        this.langString = langString;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        int[] userStats = user.getBattleStats().getBaseBattleStats();
        int[] targetStats = target.getBattleStats().getBaseBattleStats();
        int splitStat1 = (userStats[this.stat1.getStatIndex()] + targetStats[this.stat1.getStatIndex()]) / 2;
        int splitStat2 = (userStats[this.stat2.getStatIndex()] + targetStats[this.stat2.getStatIndex()]) / 2;
        user.removeStatus(this.type);
        target.removeStatus(this.type);
        if (target.addStatus(this.getNewInstance(splitStat1, splitStat2), user) && user.addStatus(this.getNewInstance(splitStat1, splitStat2), user)) {
            user.bc.sendToAll(this.langString, user.getNickname(), target.getNickname());
        }
    }

    protected abstract Split getNewInstance(int var1, int var2);

    @Override
    public int[] modifyBaseStats(PixelmonWrapper user, int[] stats) {
        stats[this.stat1.getStatIndex()] = this.statValue1;
        stats[this.stat2.getStatIndex()] = this.statValue2;
        return stats;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.hitsAlly()) {
            return;
        }
        StatusBase userSplit = pw.getStatus(this.type);
        ArrayList<PixelmonWrapper> opponents = pw.getOpponentPokemon();
        for (PixelmonWrapper target : userChoice.targets) {
            StatusBase targetSplit = target.getStatus(this.type);
            BattleAIBase ai = pw.getBattleAI();
            try {
                pw.bc.simulateMode = false;
                pw.bc.sendMessages = false;
                this.applyEffect(pw, target);
                pw.bc.simulateMode = true;
                pw.bc.modifyStats();
                pw.bc.modifyStatsCancellable(pw);
                ArrayList<MoveChoice> bestUserChoicesAfter = ai.getBestAttackChoices(pw);
                ArrayList<ArrayList<MoveChoice>> bestOpponentChoicesAfter = ai.getBestAttackChoices(opponents);
                ai.weightFromUserOptions(pw, userChoice, bestUserChoices, bestUserChoicesAfter);
                ai.weightFromOpponentOptions(pw, userChoice, MoveChoice.splitChoices(opponents, bestOpponentChoices), bestOpponentChoicesAfter);
            }
            finally {
                pw.bc.simulateMode = false;
                this.restoreSplit(pw, userSplit);
                this.restoreSplit(target, targetSplit);
                pw.bc.simulateMode = true;
                pw.bc.sendMessages = true;
                pw.bc.modifyStats();
                pw.bc.modifyStatsCancellable(pw);
            }
        }
    }

    private void restoreSplit(PixelmonWrapper pw, StatusBase split) {
        pw.removeStatus(this.type);
        if (split != null) {
            pw.addStatus(split, pw);
        }
    }
}

