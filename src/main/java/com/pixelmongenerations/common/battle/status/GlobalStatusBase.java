/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.GlobalStatusController;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Terrain;
import com.pixelmongenerations.common.battle.status.Weather;

public abstract class GlobalStatusBase
extends StatusBase {
    public GlobalStatusBase(StatusType type) {
        super(type);
    }

    public void applyRepeatedEffect(GlobalStatusController globalStatusController) {
    }

    public boolean isWeather() {
        return this instanceof Weather;
    }

    public boolean isTerrain() {
        return this instanceof Terrain;
    }

    public static boolean ignoreWeather(BattleControllerBase bc) {
        for (PixelmonWrapper pw : bc.getActiveUnfaintedPokemon()) {
            if (!pw.getBattleAbility().ignoreWeather()) continue;
            return true;
        }
        return false;
    }
}

