/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.ProtectVariationTeam;
import com.pixelmongenerations.common.battle.status.StatusType;
import java.util.ArrayList;

public class QuickGuard
extends ProtectVariationTeam {
    public QuickGuard() {
        super(StatusType.QuickGuard);
    }

    @Override
    public ProtectVariationTeam getNewInstance() {
        return new QuickGuard();
    }

    @Override
    protected void displayMessage(PixelmonWrapper user) {
        user.bc.sendToAll("pixelmon.status.quickguard", user.getNickname());
    }

    @Override
    public boolean stopsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user) {
        if (user.isDynamaxed()) {
            return false;
        }
        return !user.attack.isAttack("Quick Guard") && user.attack.getAttackBase().getPriority() >= 1 && super.stopsIncomingAttack(pokemon, user);
    }

    @Override
    public void stopsIncomingAttackMessage(PixelmonWrapper pokemon, PixelmonWrapper user) {
        user.bc.sendToAll("pixelmon.status.quickguardprotect", pokemon.getNickname());
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (MoveChoice.canBreakProtect(pw.getOpponentPokemon(), bestOpponentChoices)) {
            return;
        }
        if (pw.bc.rules.battleType.numPokemon > 1 && !MoveChoice.canOutspeed(bestOpponentChoices, pw, userChoice.createList())) {
            if (MoveChoice.hasPriority(bestOpponentChoices)) {
                userChoice.raiseWeight(50.0f);
            }
        }
    }
}

