/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle;

import com.pixelmongenerations.client.gui.battles.rules.EnumRulesGuiState;
import com.pixelmongenerations.common.battle.BattleFactory;
import com.pixelmongenerations.common.battle.BattleQueryPlayer;
import com.pixelmongenerations.common.battle.rules.BattleRules;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClauseRegistry;
import com.pixelmongenerations.common.battle.rules.teamselection.TeamSelectionList;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.battle.EnumBattleType;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.packetHandlers.battles.BattleQueryPacket;
import com.pixelmongenerations.core.network.packetHandlers.battles.EnumBattleQueryResponse;
import com.pixelmongenerations.core.network.packetHandlers.battles.rules.CheckRulesVersionChoose;
import com.pixelmongenerations.core.network.packetHandlers.battles.rules.DisplayBattleQueryRules;
import com.pixelmongenerations.core.network.packetHandlers.battles.rules.SetProposedRules;
import com.pixelmongenerations.core.network.packetHandlers.battles.rules.UpdateBattleQueryRules;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelmonMethods;
import java.util.ArrayList;
import java.util.Optional;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;

public class BattleQuery {
    private static ArrayList<BattleQuery> queryList = new ArrayList();
    static int index = 0;
    private BattleQueryPlayer[] players;
    public int queryIndex;
    public BattleRules battleRules = new BattleRules();

    public BattleQuery(EntityPlayerMP player1, EntityPixelmon pokemon1, EntityPlayerMP player2, EntityPixelmon pokemon2) {
        this.players = new BattleQueryPlayer[]{new BattleQueryPlayer(player1, pokemon1), new BattleQueryPlayer(player2, pokemon2)};
        this.queryIndex = index++;
        this.sendQuery();
    }

    private void sendQuery() {
        int i;
        Optional[] optStorages = new Optional[this.players.length];
        for (i = 0; i < this.players.length; ++i) {
            optStorages[i] = PixelmonStorage.pokeBallManager.getPlayerStorage(this.players[i].player);
            if (optStorages[i].isPresent()) continue;
            return;
        }
        for (i = 0; i < this.players.length; ++i) {
            int other = i == 0 ? 1 : 0;
            BattleQueryPacket packet = new BattleQueryPacket(this.queryIndex, this.players[other].player.getUniqueID(), (PlayerStorage)optStorages[other].get());
            Pixelmon.NETWORK.sendTo(packet, this.players[i].player);
        }
        queryList.add(this);
    }

    public void acceptQuery(EntityPlayerMP player, EnumBattleQueryResponse response) {
        BattleQueryPlayer currentPlayer = null;
        for (BattleQueryPlayer queryPlayer : this.players) {
            if (player != queryPlayer.player) continue;
            queryPlayer.response = response;
            currentPlayer = queryPlayer;
            break;
        }
        if (currentPlayer == null) {
            return;
        }
        boolean allConfirm = true;
        boolean allAccept = true;
        for (BattleQueryPlayer queryPlayer : this.players) {
            if (!queryPlayer.response.isAcceptResponse()) {
                allConfirm = false;
            }
            if (queryPlayer.response == EnumBattleQueryResponse.Accept) continue;
            allAccept = false;
        }
        if (allConfirm) {
            if (allAccept) {
                this.startBattle();
            } else {
                BattleQueryPlayer proposePlayer = this.players[currentPlayer == this.players[0] ? 1 : 0];
                if (proposePlayer.response != EnumBattleQueryResponse.Rules) {
                    proposePlayer = currentPlayer;
                }
                BattleQueryPlayer otherPlayer = proposePlayer;
                for (BattleQueryPlayer queryPlayer : this.players) {
                    queryPlayer.response = queryPlayer == proposePlayer ? EnumBattleQueryResponse.Accept : EnumBattleQueryResponse.Rules;
                    EnumBattleQueryResponse enumBattleQueryResponse = queryPlayer.response;
                    if (queryPlayer == proposePlayer) continue;
                    otherPlayer = queryPlayer;
                }
                int clauseVersion = BattleClauseRegistry.getClauseVersion();
                Pixelmon.NETWORK.sendTo(new CheckRulesVersionChoose(clauseVersion, new DisplayBattleQueryRules(this.queryIndex, true)), proposePlayer.player);
                Pixelmon.NETWORK.sendTo(new CheckRulesVersionChoose(clauseVersion, new DisplayBattleQueryRules(this.queryIndex, false)), otherPlayer.player);
            }
        }
    }

    private void startBattle() {
        int i;
        PlayerStorage[] storages = new PlayerStorage[this.players.length];
        for (i = 0; i < this.players.length; ++i) {
            Optional<PlayerStorage> optStorage = PixelmonStorage.pokeBallManager.getPlayerStorage(this.players[i].player);
            if (!optStorage.isPresent()) {
                return;
            }
            storages[i] = optStorage.get();
        }
        if (this.battleRules.isDefault()) {
            for (i = 0; i < storages.length; ++i) {
                BattleQueryPlayer queryPlayer = this.players[i];
                if (queryPlayer.pokemon != null) continue;
                queryPlayer.pokemon = storages[i].getFirstAblePokemon(queryPlayer.player.world);
            }
            if (this.battleRules.battleType == EnumBattleType.Single) {
                BattleFactory.createBattle().team1(this.players[0].getParticipant()).team2(this.players[1].getParticipant()).rules(this.battleRules).startBattle();
            } else {
                EntityPixelmon[] secondPokemon = new EntityPixelmon[this.players.length];
                boolean valid = true;
                for (int j = 0; j < secondPokemon.length; ++j) {
                    PlayerStorage storage = storages[j];
                    BattleQueryPlayer player = this.players[j];
                    for (int i2 = 0; i2 < 6; ++i2) {
                        int[] pixId = null;
                        pixId = storage.getIDFromPosition(i2);
                        if (PixelmonMethods.isIDSame(player.pokemon.getPokemonId(), pixId) || storage.isFainted(pixId) || storage.isEgg(pixId)) continue;
                        secondPokemon[j] = storage.sendOut(pixId, player.player.world);
                        break;
                    }
                    if (secondPokemon[j] != null) continue;
                    ChatHandler.sendChat((ICommandSender)this.players[0].player, this.players[1].player, "gui.acceptdeny.invaliddouble", player.player.getDisplayName());
                    valid = false;
                    break;
                }
                if (valid) {
                    BattleFactory.createBattle().team1(this.players[0].getParticipant(secondPokemon[0])).team2(this.players[0].getParticipant(secondPokemon[1])).rules(this.battleRules).startBattle();
                }
            }
        } else {
            TeamSelectionList.addTeamSelection(this.battleRules, false, storages);
        }
        queryList.remove(this);
    }

    public void declineQuery(EntityPlayerMP player) {
        boolean valid = false;
        for (BattleQueryPlayer queryPlayer : this.players) {
            if (player != queryPlayer.player) continue;
            valid = true;
            break;
        }
        if (!valid) {
            return;
        }
        for (BattleQueryPlayer queryPlayer : this.players) {
            if (player == queryPlayer.player) continue;
            queryPlayer.player.closeScreen();
        }
        ChatHandler.sendChat((ICommandSender)this.players[0].player, this.players[1].player, "battlequery.declined", player.getDisplayNameString());
        queryList.remove(this);
    }

    public void proposeRules(EntityPlayerMP player, BattleRules rules) {
        boolean valid = false;
        for (BattleQueryPlayer queryPlayer : this.players) {
            if (player != queryPlayer.player) continue;
            queryPlayer.response = EnumBattleQueryResponse.Accept;
            valid = true;
            break;
        }
        if (!valid) {
            return;
        }
        rules.validateRules();
        this.battleRules = rules;
        EntityPlayerMP otherPlayer = this.getOtherPlayer(player);
        Pixelmon.NETWORK.sendTo(new SetProposedRules(this.battleRules), otherPlayer);
        Pixelmon.NETWORK.sendTo(new UpdateBattleQueryRules(EnumRulesGuiState.WaitAccept), player);
        Pixelmon.NETWORK.sendTo(new UpdateBattleQueryRules(EnumRulesGuiState.Accept), otherPlayer);
    }

    public void changeRules(EntityPlayerMP player) {
        Pixelmon.NETWORK.sendTo(new UpdateBattleQueryRules(EnumRulesGuiState.Propose), player);
        Pixelmon.NETWORK.sendTo(new UpdateBattleQueryRules(EnumRulesGuiState.WaitChange), this.getOtherPlayer(player));
    }

    private EntityPlayerMP getOtherPlayer(EntityPlayerMP player) {
        return player == this.players[0].player ? this.players[1].player : this.players[0].player;
    }

    public static BattleQuery getQuery(int index) {
        for (BattleQuery aQueryList : queryList) {
            if (aQueryList.queryIndex != index) continue;
            return aQueryList;
        }
        return null;
    }

    public static BattleQuery getQuery(EntityPlayerMP player) {
        for (BattleQuery aQueryList : queryList) {
            for (BattleQueryPlayer queryPlayer : aQueryList.players) {
                if (queryPlayer.player != player) continue;
                return aQueryList;
            }
        }
        return null;
    }
}

