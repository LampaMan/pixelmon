/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;

public class ScheduledRemount {
    private final EntityPixelmon mount;
    private final EntityPlayerMP player;

    public ScheduledRemount(EntityPixelmon mount, EntityPlayerMP player) {
        this.mount = mount;
        this.player = player;
    }

    public void performRemount(EntityPlayerMP entityPlayerMP, EntityPixelmon pixelmon) {
        Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage(entityPlayerMP);
        if (storageOpt.isPresent()) {
            PlayerStorage storage = storageOpt.get();
            for (int i = 0; i < 6; ++i) {
                EntityPixelmon pokemon = storage.getPokemon(storage.getIDFromPosition(i), entityPlayerMP.getEntityWorld());
                if (pokemon == null || pokemon.getSpecies() != pixelmon.getSpecies()) continue;
                if (pixelmon.getHealth() == 0.0f) {
                    pixelmon.setHealth(1.0f);
                }
                pixelmon.releaseFromPokeball();
                entityPlayerMP.startRiding(pixelmon);
            }
        }
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public EntityPixelmon getMount() {
        return this.mount;
    }
}

