/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.animations.particles;

import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.client.particle.ParticleArcanery;
import com.pixelmongenerations.client.particle.ParticleEffect;
import com.pixelmongenerations.core.proxy.ClientProxy;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public class ParticleShiny extends ParticleEffect {

    private String particle = "";
    private int particleTint = -1;
    private int particleTintAlpha = 255;

    public ParticleShiny(String particleId, int particleTint, int particleTintAlpha) {
        this.particle = particleId;
        this.particleTint = particleTint;
        this.particleTintAlpha = particleTintAlpha;
    }

    @Override
    public void init(ParticleArcanery particle, World world, double x, double y, double z, double vx, double vy, double vz, float size) {
        if (this.particleTint != -1) {
            int color = this.particleTint | this.particleTintAlpha << 24;
            float alpha = (float)(color >> 24 & 0xFF) / 255.0f;
            float red = (float)(color >> 16 & 0xFF) / 255.0f;
            float green = (float)(color >> 8 & 0xFF) / 255.0f;
            float blue = (float)(color & 0xFF) / 255.0f;
            particle.setRGBA(alpha, red, green, blue);
        }
        particle.setMotion(0.01f, 0.01f, 0.01f);
        particle.setScale(particle.getScale() * 0.3f);
        particle.setScale(particle.getScale() * size);
        particle.setMaxAge(16);
    }

    @Override
    public void update(ParticleArcanery particle) {
        particle.setPrevPos(particle.getX(), particle.getY(), particle.getZ());
        particle.incrementAge();
        if (particle.getAge() >= particle.getMaxAge()) {
            particle.setExpired();
        }
        particle.setMotion(particle.getMotionX(), particle.getMotionY() + 0.004, particle.getMotionZ());
        particle.move(particle.getMotionX(), particle.getMotionY(), particle.getMotionZ());
        particle.setMotion(particle.getMotionX() * (double)0.9f, particle.getMotionY() * (double)0.9f, particle.getMotionZ() * (double)0.9f);
        if (particle.onGround()) {
            particle.setMotion(particle.getMotionX() * (double)0.7f, particle.getMotionY(), particle.getMotionZ() * (double)0.7f);
        }
        particle.setAngle(90.0f);
    }

    @Override
    public ResourceLocation texture() {
        TextureResource dynTexture = ClientProxy.TEXTURE_STORE.getObject(this.particle);
        if (dynTexture != null) {
            dynTexture.bindTexture(false);
        } else {
            Minecraft.getMinecraft().renderEngine.bindTexture(new ResourceLocation("pixelmon", "textures/particles/" + this.particle + ".png"));
        }
        return null;
    }
}

