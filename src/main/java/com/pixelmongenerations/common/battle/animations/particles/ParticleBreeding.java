/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.animations.particles;

import com.pixelmongenerations.core.enums.EnumBreedingParticles;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.ParticleHeart;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public class ParticleBreeding
extends ParticleHeart {
    private static final ResourceLocation particleTextures = new ResourceLocation("textures/particle/particles.png");
    public EnumBreedingParticles particleHeart = EnumBreedingParticles.grey;

    public ParticleBreeding(World par1World, double par2, double par4, double par6, double par8, double par10, double par12, EnumBreedingParticles enumParticle) {
        this(par1World, par2, par4, par6, par8, par10, par12, 2.0f, enumParticle);
    }

    public ParticleBreeding(World par1World, double par2, double par4, double par6, double par8, double par10, double par12, float par14, EnumBreedingParticles enumParticle) {
        super(par1World, par2, par4, par6, 0.0, 0.0, 0.0);
        this.particleHeart = enumParticle;
        this.motionX *= (double)0.01f;
        this.motionY *= (double)0.01f;
        this.motionZ *= (double)0.01f;
        this.motionY += 0.1;
        this.particleScale *= 0.45f;
        this.particleScale *= par14;
        this.particleMaxAge = 16;
    }

    @Override
    public void onUpdate() {
        this.prevPosX = this.posX;
        this.prevPosY = this.posY;
        this.prevPosZ = this.posZ;
        if (this.particleAge++ >= this.particleMaxAge) {
            this.setExpired();
        }
        this.move(this.motionX, this.motionY, this.motionZ);
        if (this.posY == this.prevPosY) {
            this.motionX *= 1.1;
            this.motionZ *= 1.1;
        }
        this.motionX *= (double)0.86f;
        this.motionY *= (double)0.86f;
        this.motionZ *= (double)0.86f;
        if (this.onGround) {
            this.motionX *= (double)0.7f;
            this.motionZ *= (double)0.7f;
        }
    }

    @Override
    public void renderParticle(BufferBuilder buffer, Entity p_180434_2_, float p_180434_3_, float p_180434_4_, float p_180434_5_, float p_180434_6_, float p_180434_7_, float p_180434_8_) {
        float f10 = 0.1f * this.particleScale;
        float f11 = (float)(this.prevPosX + (this.posX - this.prevPosX) * (double)p_180434_3_ - interpPosX);
        float f12 = (float)(this.prevPosY + (this.posY - this.prevPosY) * (double)p_180434_3_ - interpPosY);
        float f13 = (float)(this.prevPosZ + (this.posZ - this.prevPosZ) * (double)p_180434_3_ - interpPosZ);
        Tessellator.getInstance().draw();
        Minecraft.getMinecraft().renderEngine.bindTexture(this.particleHeart.location);
        buffer.begin(7, DefaultVertexFormats.POSITION_TEX);
        buffer.pos(f11 - p_180434_4_ * f10 - p_180434_7_ * f10, f12 - p_180434_5_ * f10, f13 - p_180434_6_ * f10 - p_180434_8_ * f10).tex(1.0, 1.0).endVertex();
        buffer.pos(f11 - p_180434_4_ * f10 + p_180434_7_ * f10, f12 + p_180434_5_ * f10, f13 - p_180434_6_ * f10 + p_180434_8_ * f10).tex(1.0, 0.0).endVertex();
        buffer.pos(f11 + p_180434_4_ * f10 + p_180434_7_ * f10, f12 + p_180434_5_ * f10, f13 + p_180434_6_ * f10 + p_180434_8_ * f10).tex(0.0, 0.0).endVertex();
        buffer.pos(f11 + p_180434_4_ * f10 - p_180434_7_ * f10, f12 - p_180434_5_ * f10, f13 + p_180434_6_ * f10 - p_180434_8_ * f10).tex(0.0, 1.0).endVertex();
        Tessellator.getInstance().draw();
        Minecraft.getMinecraft().renderEngine.bindTexture(particleTextures);
        buffer.begin(7, DefaultVertexFormats.POSITION_TEX);
    }

    @Override
    public int getBrightnessForRender(float p_70070_1_) {
        return 257;
    }
}

