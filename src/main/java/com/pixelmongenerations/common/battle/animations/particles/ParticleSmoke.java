/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.animations.particles;

import net.minecraft.client.particle.ParticleSmokeNormal;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(value=Side.CLIENT)
public class ParticleSmoke
extends ParticleSmokeNormal {
    public ParticleSmoke(World par1World, double par2, double par4, double par6, double par8, double par10, double par12) {
        super(par1World, par2, par4, par6, par8, par10, par12, 1.0f);
    }

    @Override
    public void setMaxAge(int i) {
        this.particleMaxAge = i;
    }

    public int maxAge() {
        return this.particleMaxAge;
    }

    public int currentAge() {
        return this.particleAge;
    }
}

