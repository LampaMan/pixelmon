/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.animations.particles;

import net.minecraft.client.particle.Particle;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.entity.Entity;
import net.minecraft.world.World;

public class ParticleGastly
extends Particle {
    float oSize;

    public ParticleGastly(World par1World, double par2, double par4, double par6, double par8, double par10, double par12, boolean isShiny) {
        super(par1World, par2, par4, par6, 0.0, 0.0, 0.0);
        float var14 = 3.0f;
        this.motionX *= (double)0.1f;
        this.motionY *= (double)0.1f;
        this.motionZ *= (double)0.1f;
        this.motionX += par8;
        this.motionY += par10;
        this.motionZ += par12;
        this.particleRed = 0.19f;
        this.particleGreen = 0.0f;
        this.particleBlue = 0.38f;
        this.particleScale *= 0.5f;
        this.particleScale *= var14;
        this.oSize = this.particleScale;
        this.particleMaxAge = (int)(2.0 / (Math.random() * 0.8 + 0.3));
        this.particleMaxAge = (int)((float)this.particleMaxAge * var14);
        if (isShiny) {
            this.particleRed = 0.35f;
            this.particleGreen = 0.52f;
            this.particleBlue = 0.68f;
        } else {
            this.particleRed = 0.23f;
            this.particleGreen = 0.13f;
            this.particleBlue = 0.4f;
        }
    }

    @Override
    public void renderParticle(BufferBuilder buffer, Entity p_180434_2_, float p_180434_3_, float p_180434_4_, float p_180434_5_, float p_180434_6_, float p_180434_7_, float p_180434_8_) {
        float var8 = ((float)this.particleAge + p_180434_3_) / (float)this.particleMaxAge * 32.0f;
        if (var8 < 0.0f) {
            var8 = 0.0f;
        }
        if (var8 > 1.0f) {
            var8 = 1.0f;
        }
        this.particleScale = this.oSize * var8;
        super.renderParticle(buffer, p_180434_2_, p_180434_3_, p_180434_4_, p_180434_5_, p_180434_6_, p_180434_7_, p_180434_8_);
    }

    @Override
    public void onUpdate() {
        this.prevPosX = this.posX;
        this.prevPosY = this.posY;
        this.prevPosZ = this.posZ;
        if (this.particleAge++ >= this.particleMaxAge) {
            this.setExpired();
        }
        this.setParticleTextureIndex(7 - this.particleAge * 8 / this.particleMaxAge);
        this.motionX *= (double)0.96f;
        this.motionY *= (double)0.96f;
        this.motionZ *= (double)0.96f;
    }
}

