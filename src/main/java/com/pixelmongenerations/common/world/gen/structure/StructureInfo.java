/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.structure;

import com.pixelmongenerations.common.world.Schematic;
import com.pixelmongenerations.common.world.gen.structure.towns.NPCPlacementInfo;
import com.pixelmongenerations.common.world.gen.structure.util.GeneralScattered;
import com.pixelmongenerations.common.world.gen.structure.util.StructureScattered;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;

public abstract class StructureInfo {
    public String id;
    protected int depth = 0;
    protected ArrayList<NPCPlacementInfo> npcs = new ArrayList();
    protected Schematic snapshot;
    protected boolean underwater;

    public void addNPC(NPCPlacementInfo info) {
        this.npcs.add(info);
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public void setUnderwater(boolean underwater) {
        this.underwater = underwater;
    }

    public void setSnapshot(NBTTagCompound nbt) {
        this.snapshot = Schematic.loadFromNBT(nbt);
    }

    public Schematic getSnapshot() {
        return this.snapshot;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getDepth() {
        return this.depth;
    }

    public boolean isUnderwater() {
        return this.underwater;
    }

    public StructureScattered createStructure(Random random, BlockPos pos, boolean doRotation, boolean forceGeneration, EnumFacing facing) {
        return new GeneralScattered(random, pos, this.snapshot, this, doRotation, forceGeneration, facing);
    }

    public List<NPCPlacementInfo> getNPCS() {
        return this.npcs;
    }
}

