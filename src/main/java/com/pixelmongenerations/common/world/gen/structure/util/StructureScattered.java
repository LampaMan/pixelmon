/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.structure.util;

import com.pixelmongenerations.common.world.gen.structure.util.IVillageStructure;
import com.pixelmongenerations.core.util.helper.WorldHelper;
import java.util.Random;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.gen.structure.StructureBoundingBox;
import net.minecraft.world.gen.structure.StructureComponent;

public abstract class StructureScattered
extends StructureComponent
implements IVillageStructure {
    public int scatteredFeatureSizeX;
    public int scatteredFeatureSizeY;
    public int scatteredFeatureSizeZ;
    protected int horizontalPos = -1;
    protected boolean shouldSave;
    public ItemStack signItem;

    protected StructureScattered(Random par1Random, BlockPos pos, int width, int height, int length, boolean doRotation) {
        this(par1Random, pos, width, height, length, false, doRotation);
    }

    protected StructureScattered(Random par1Random, BlockPos pos, int width, int height, int length, boolean save, boolean doRotation) {
        super(0);
        this.scatteredFeatureSizeX = width;
        this.scatteredFeatureSizeY = height;
        this.scatteredFeatureSizeZ = length;
        if (doRotation) {
            this.setCoordBaseMode(EnumFacing.values()[par1Random.nextInt(4)]);
        } else {
            this.setCoordBaseMode(EnumFacing.DOWN);
        }
        switch (this.getCoordBaseMode()) {
            case EAST: 
            case WEST: {
                this.boundingBox = new StructureBoundingBox(pos.getX(), pos.getY(), pos.getZ(), pos.getX() + width - 1, pos.getY() + height - 1, pos.getZ() + length - 1);
                break;
            }
            default: {
                this.boundingBox = new StructureBoundingBox(pos.getX(), pos.getY(), pos.getZ(), pos.getX() + length - 1, pos.getY() + height - 1, pos.getZ() + width - 1);
            }
        }
        this.shouldSave = save;
    }

    protected StructureScattered() {
    }

    public abstract String getName();

    public void setShouldSave(boolean save) {
        this.shouldSave = save;
    }

    public final boolean generate(World world, Random random) {
        return this.generateImpl(world, random);
    }

    public abstract boolean generateImpl(World var1, Random var2);

    protected abstract boolean canStructureFitAtCoords(World var1);

    public abstract int getX(int var1, int var2);

    public abstract int getY(int var1);

    public abstract int getZ(int var1, int var2);

    public int getTopSolidBlock(World world, int par1, int par2) {
        Chunk chunk = world.getChunk(new BlockPos(par1, 0, par2));
        par1 &= 0xF;
        par2 &= 0xF;
        for (int k = chunk.getTopFilledSegment() + 15; k > 0; --k) {
            IBlockState block = chunk.getBlockState(par1, k, par2);
            if (block.getBlock() == Blocks.AIR || !block.getMaterial().blocksMovement() || block.getMaterial() == Material.LEAVES || block.getMaterial() == Material.WOOD || block.getBlock().isFoliage(world, new BlockPos(par1, k, par2))) continue;
            return k;
        }
        return -1;
    }

    public void fixLighting(World world) {
        WorldHelper.fixLighting(world, this.boundingBox);
    }

    public EnumFacing getFacing() {
        return this.getCoordBaseMode();
    }

    @Override
    public int getNPCXWithOffset(int x, int z) {
        return super.getXWithOffset(x, z);
    }

    @Override
    public int getNPCYWithOffset(int y) {
        return super.getYWithOffset(y);
    }

    @Override
    public int getNPCZWithOffset(int x, int z) {
        return super.getZWithOffset(x, z);
    }
}

