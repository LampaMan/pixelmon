/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.structure.standalone;

import com.pixelmongenerations.common.world.gen.structure.StructureInfo;
import java.util.ArrayList;
import net.minecraft.world.biome.Biome;

public class StandaloneStructureInfo
extends StructureInfo {
    int rarity = 0;
    ArrayList<Biome> biomes = new ArrayList();

    public void setRarity(int rarity) {
        this.rarity = rarity;
    }

    public int getRarity() {
        return this.rarity;
    }

    public void addBiome(Biome biome) {
        this.biomes.add(biome);
    }

    public ArrayList<Biome> getBiomes() {
        return this.biomes;
    }
}

