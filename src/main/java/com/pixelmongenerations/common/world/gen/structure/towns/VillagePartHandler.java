/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.structure.towns;

import com.pixelmongenerations.common.world.gen.structure.StructureInfo;
import com.pixelmongenerations.common.world.gen.structure.StructureRegistry;
import com.pixelmongenerations.common.world.gen.structure.towns.ComponentTownPart;
import java.util.List;
import java.util.Random;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.gen.structure.StructureComponent;
import net.minecraft.world.gen.structure.StructureVillagePieces;
import net.minecraftforge.fml.common.registry.VillagerRegistry;

public class VillagePartHandler
implements VillagerRegistry.IVillageCreationHandler {
    public static int weightSum = 0;
    public static int numCompSum = 0;

    @Override
    public StructureVillagePieces.PieceWeight getVillagePieceWeight(Random random, int i) {
        return new StructureVillagePieces.PieceWeight(ComponentTownPart.class, weightSum, numCompSum);
    }

    @Override
    public Class<?> getComponentClass() {
        return ComponentTownPart.class;
    }

    @Override
    public StructureVillagePieces.Village buildComponent(StructureVillagePieces.PieceWeight villagePiece, StructureVillagePieces.Start startPiece, List<StructureComponent> pieces, Random random, int p1, int p2, int p3, EnumFacing facing, int p5) {
        try {
            StructureInfo info = StructureRegistry.getRandomTownPiece(random, pieces);
            return ComponentTownPart.buildComponent(startPiece, pieces, random, p1, p2, p3, facing, p5, info);
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

