/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.feature;

import com.pixelmongenerations.core.config.PixelmonBlocks;
import java.util.Random;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

public class WorldGenFireStoneOre
extends WorldGenerator
implements IWorldGenerator {
    @Override
    public boolean generate(World world, Random rand, BlockPos pos) {
        int y = this.findTopLavaBlock(world, pos);
        if (world.getBlockState(pos = new BlockPos(pos.getX(), ++y, pos.getZ())).getMaterial() == Material.AIR) {
            for (int i = 1; i < 10; ++i) {
                if (world.getBlockState(pos = new BlockPos(pos.getX(), ++y, pos.getZ())).getMaterial() == Material.AIR) continue;
                world.setBlockState(pos, PixelmonBlocks.fireStoneOre.getDefaultState(), 0);
                return true;
            }
        }
        return false;
    }

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        for (int i = 0; i < 30; ++i) {
            int zPos;
            int xPos = random.nextInt(16) + chunkX * 16;
            if (!this.hasLava(world, new BlockPos(xPos, 0, zPos = random.nextInt(16) + chunkZ * 16))) continue;
            this.generate(world, random, new BlockPos(xPos, 0, zPos));
        }
    }

    private boolean hasLava(World world, BlockPos pos) {
        for (int i = 0; i < 30; ++i) {
            if (world.getBlockState(new BlockPos(pos.getX(), i, pos.getZ())).getBlock() != Blocks.LAVA) continue;
            return true;
        }
        return false;
    }

    private int findTopLavaBlock(World world, BlockPos pos) {
        for (int i = 0; i < 30; ++i) {
            if (world.getBlockState(new BlockPos(pos.getX(), i, pos.getZ())).getBlock() != Blocks.LAVA) continue;
            return i;
        }
        return -1;
    }
}

