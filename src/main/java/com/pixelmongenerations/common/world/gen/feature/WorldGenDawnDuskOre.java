/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.feature;

import com.pixelmongenerations.core.config.PixelmonBlocks;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.fml.common.IWorldGenerator;

public class WorldGenDawnDuskOre
implements IWorldGenerator {
    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        Biome biome = world.getBiome(new BlockPos(chunkX * 16, 0, chunkZ * 16));
        float likelyHood = BiomeDictionary.getTypes(biome).contains(BiomeDictionary.Type.PLAINS) ? 0.02f : -1.0f;
        float f = likelyHood;
        if (random.nextFloat() <= likelyHood) {
            BlockPos.MutableBlockPos pos = new BlockPos.MutableBlockPos();
            for (int i = 0; i < 30; ++i) {
                int xPos = random.nextInt(16) + chunkX * 16;
                int zPos = random.nextInt(16) + chunkZ * 16;
                int yPos = world.getTopSolidOrLiquidBlock(pos.setPos(xPos, 0, zPos)).getY() - random.nextInt(10);
                pos.setPos(xPos, yPos, zPos);
                if (world.getBlockState(pos).getBlock() != Block.getBlockFromName("stone")) continue;
                world.setBlockState(pos, PixelmonBlocks.dawnduskStoneOre.getDefaultState());
            }
        }
    }
}

