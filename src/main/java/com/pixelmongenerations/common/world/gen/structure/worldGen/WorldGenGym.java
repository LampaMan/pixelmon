/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.structure.worldGen;

import com.pixelmongenerations.api.events.PixelmonStructureGenEvent;
import com.pixelmongenerations.common.entity.npcs.NPCChatting;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.npcs.registry.GymNPCData;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.item.ItemBadge;
import com.pixelmongenerations.common.world.gen.structure.StructureInfo;
import com.pixelmongenerations.common.world.gen.structure.StructureRegistry;
import com.pixelmongenerations.common.world.gen.structure.gyms.GymInfo;
import com.pixelmongenerations.common.world.gen.structure.gyms.WorldGymData;
import com.pixelmongenerations.common.world.gen.structure.gyms.WorldGymInfo;
import com.pixelmongenerations.common.world.gen.structure.towns.ComponentTownPart;
import com.pixelmongenerations.common.world.gen.structure.towns.NPCPlacementInfo;
import com.pixelmongenerations.common.world.gen.structure.util.StructureScattered;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonItemsBadges;
import com.pixelmongenerations.core.enums.EnumEncounterMode;
import com.pixelmongenerations.core.enums.EnumNPCType;
import com.pixelmongenerations.core.enums.EnumTrainerAI;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import net.minecraft.entity.EntityLiving;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.structure.MapGenScatteredFeature;
import net.minecraft.world.gen.structure.StructureBoundingBox;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.IWorldGenerator;

public class WorldGenGym
extends MapGenScatteredFeature
implements IWorldGenerator {
    public static StructureBoundingBox lastTownBB;
    public static boolean hasGenerated;
    static final int distance = 30;
    public static List<StructureBoundingBox> usedTownsList;

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        if (lastTownBB == null) {
            return;
        }
        int minX = chunkX * 16;
        int maxX = minX + 16;
        int minZ = chunkZ * 16;
        int maxZ = minZ + 16;
        if (this.isClose(minX, maxX, WorldGenGym.lastTownBB.minX, WorldGenGym.lastTownBB.maxX) && this.isClose(minZ, maxZ, WorldGenGym.lastTownBB.minZ, WorldGenGym.lastTownBB.maxZ)) {
            GymInfo gyminfo;
            WorldGymData data = (WorldGymData)world.getPerWorldStorage().getOrLoadData(WorldGymData.class, "gyminfo");
            if (data == null) {
                data = new WorldGymData();
                world.getPerWorldStorage().setData("gyminfo", data);
            }
            if ((gyminfo = StructureRegistry.getNextGym(data, random)) == null || gyminfo.type == null) {
                if (gyminfo != null) {
                    Pixelmon.LOGGER.info("Gym type info not found. External JSON files are probably not updated correctly.");
                }
                return;
            }
            this.getBadge(data, gyminfo.type);
            int xPos = random.nextInt(16) + chunkX * 16;
            BlockPos pos = new BlockPos(xPos, 64, random.nextInt(16) + chunkZ * 16);
            StructureScattered s = gyminfo.createStructure(random, pos, true, false, null);
            if (!this.canSpawnStructureAtCoords(world, s, gyminfo, pos) || ComponentTownPart.getCenter(s.getBoundingBox()).distanceSq(ComponentTownPart.getCenter(lastTownBB)) <= 100.0) {
                return;
            }
            GymPlacementInfo info = new GymPlacementInfo(s, gyminfo, pos, lastTownBB);
            if (!usedTownsList.contains(lastTownBB) && !hasGenerated) {
                ItemStack badge;
                info.struc.signItem = badge = this.getBadge(data, info.gyminfo.type);
                boolean generated = info.struc.generate(world, world.rand);
                PixelmonStructureGenEvent.Post generationEvent = new PixelmonStructureGenEvent.Post(world, info.struc, (StructureInfo)info.gyminfo, info.gymPos, generated);
                MinecraftForge.EVENT_BUS.post(generationEvent);
                if (generated) {
                    info.gyminfo.level = data.getGymLevel();
                    WorldGenGym.spawnNPCs(world, info.struc, info.gyminfo, badge);
                    data.addGym(info.gyminfo.name, badge, info.gyminfo.level);
                    usedTownsList.add(info.lastTownBB);
                    hasGenerated = true;
                }
            }
        }
    }

    private ItemStack getBadge(WorldGymData data, EnumType[] types) {
        ArrayList<ItemBadge> clearedList = PixelmonItemsBadges.getBadgeList(types);
        for (WorldGymInfo info : data.getGymList()) {
            if (!clearedList.contains(info.badge.getItem())) continue;
            clearedList.remove(info.badge.getItem());
        }
        if (clearedList.size() == 0) {
            clearedList = PixelmonItemsBadges.getBadgeList(types);
        }
        return new ItemStack(RandomHelper.getRandomElementFromList(clearedList));
    }

    public static void spawnNPCs(World world, StructureScattered s, GymInfo gymInfo) {
        WorldGenGym.spawnNPCs(world, s, gymInfo, new ItemStack(PixelmonItemsBadges.getRandomBadge(gymInfo.type)));
    }

    public static void spawnNPCs(World world, StructureScattered s, GymInfo gymInfo, ItemStack badge) {
        for (NPCPlacementInfo npcInfo : gymInfo.getNPCS()) {
            GymNPCData data;
            if (npcInfo.npcType == EnumNPCType.ChattingNPC) {
                NPCChatting npc = new NPCChatting(world);
                data = ServerNPCRegistry.getGymMember(npcInfo.npcName);
                if (data == null) {
                    Pixelmon.LOGGER.warn("Can't find Gym NPC " + npcInfo.npcName);
                    continue;
                }
                npc.init(data);
                npc.setCustomSteveTexture(data.textures.get(data.getRandomTextureIndex()));
                npc.initDefaultAI();
                npc.ignoreDespawnCounter = true;
                WorldGenGym.spawnVillager(world, s, npcInfo.x, npcInfo.y, npcInfo.z, npc);
                continue;
            }
            if (npcInfo.npcType != EnumNPCType.Trainer) continue;
            NPCTrainer trainer = new NPCTrainer(world);
            data = ServerNPCRegistry.getGymMember(npcInfo.npcName);
            if (data == null) {
                Pixelmon.LOGGER.warn("Can't find Gym NPC " + npcInfo.npcName);
                continue;
            }
            trainer.init(data, gymInfo, npcInfo.tier);
            trainer.setAIMode(EnumTrainerAI.StillAndEngage);
            trainer.initAI();
            trainer.setEncounterMode(EnumEncounterMode.OncePerPlayer);
            trainer.setStartRotationYaw(WorldGenGym.getRotation(npcInfo.rotation, s.getFacing()));
            if (npcInfo.drops != null) {
                ArrayList<ItemStack> dropsList = new ArrayList<ItemStack>();
                dropsList.add(RandomHelper.getRandomElementFromList(npcInfo.drops));
                if (npcInfo.tier == 0 && badge != null) {
                    dropsList.add(badge);
                }
                trainer.updateDrops(dropsList.toArray(new ItemStack[dropsList.size()]));
            }
            trainer.ignoreDespawnCounter = true;
            WorldGenGym.spawnVillager(world, s, npcInfo.x, npcInfo.y, npcInfo.z, trainer);
        }
    }

    private static float getRotation(int rotation, EnumFacing facing) {
        switch (facing) {
            case EAST: {
                return rotation;
            }
            case WEST: {
                return rotation + 180;
            }
            case NORTH: {
                return rotation - 90;
            }
            case SOUTH: {
                return rotation + 90;
            }
        }
        return 0.0f;
    }

    protected static void spawnVillager(World worldIn, StructureScattered s, int x, int y, int z, EntityLiving entity) {
        int xoff = s.getX(x, z);
        int yoff = s.getY(y);
        int zoff = s.getZ(x, z);
        if (!s.getBoundingBox().isVecInside(new BlockPos(xoff, yoff, zoff))) {
            return;
        }
        entity.setLocationAndAngles((double)xoff + 0.5, yoff, (double)zoff + 0.5, 0.0f, 0.0f);
        worldIn.spawnEntity(entity);
    }

    private boolean isClose(int min1, int max1, int min2, int max2) {
        return Math.abs(min1 - min2) < 30 || Math.abs(min1 - max2) < 30 || Math.abs(max1 - min2) < 30 || Math.abs(max1 - max2) < 30;
    }

    protected boolean canSpawnStructureAtCoords(World world, StructureScattered s, GymInfo gyminfo, BlockPos pos) {
        PixelmonStructureGenEvent.Pre generationEvent = new PixelmonStructureGenEvent.Pre(world, s, gyminfo, pos);
        MinecraftForge.EVENT_BUS.post(generationEvent);
        return !generationEvent.isCanceled();
    }

    static {
        usedTownsList = Collections.synchronizedList(new ArrayList());
    }

    private class GymPlacementInfo {
        public StructureScattered struc;
        public GymInfo gyminfo;
        public BlockPos gymPos;
        public StructureBoundingBox lastTownBB;

        public GymPlacementInfo(StructureScattered s, GymInfo gyminfo, BlockPos pos, StructureBoundingBox lastTownBB) {
            this.struc = s;
            this.gyminfo = gyminfo;
            this.gymPos = pos;
            this.lastTownBB = lastTownBB;
        }
    }
}

