/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraftforge.fml.common.IWorldGenerator;

public class GenericOreGenerator
implements IWorldGenerator {
    private Block blockToGenerate;

    public GenericOreGenerator(Block blockToGenerate) {
        this.blockToGenerate = blockToGenerate;
    }

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        if (!world.provider.isSurfaceWorld()) {
            return;
        }
        for (int i = 0; i < 4; ++i) {
            BlockPos pos = new BlockPos(random.nextInt(16) + chunkX * 16, RandomHelper.rand.nextInt(64), random.nextInt(16) + chunkZ * 16);
            WorldGenMinable worldGenMinable = new WorldGenMinable(this.blockToGenerate.getDefaultState(), 5);
            try {
                worldGenMinable.generate(world, random, pos);
                continue;
            }
            catch (NullPointerException e) {
                Pixelmon.LOGGER.error((Object)e);
            }
        }
    }
}

