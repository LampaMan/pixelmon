/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.structure.worldGen;

import com.pixelmongenerations.api.events.PixelmonStructureGenEvent;
import com.pixelmongenerations.common.world.gen.structure.StructureInfo;
import com.pixelmongenerations.common.world.gen.structure.StructureRegistry;
import com.pixelmongenerations.common.world.gen.structure.standalone.StandaloneStructureInfo;
import com.pixelmongenerations.common.world.gen.structure.util.StructureScattered;
import com.pixelmongenerations.common.world.ultraspace.UltraSpaceBiomeProvider;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.Random;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.structure.MapGenScatteredFeature;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.IWorldGenerator;

public class WorldGenScatteredFeature
extends MapGenScatteredFeature
implements IWorldGenerator {
    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        int xPos = random.nextInt(16) + chunkX * 16;
        int yPos = 64;
        int zPos = random.nextInt(16) + chunkZ * 16;
        BlockPos pos = new BlockPos(xPos, 64, zPos);
        boolean ultraSpace = this.isUltraSpace(world.getBiome(pos));
        if (!ultraSpace && random.nextInt(12) != 1) {
            return;
        }
        StandaloneStructureInfo structure = StructureRegistry.getScatteredStructureFromBiome(random, world.getBiome(pos));
        if (structure == null) {
            return;
        }
        StructureScattered s = structure.createStructure(random, pos, true, false, null);
        if (this.canSpawnStructureAtCoords(world, s, structure, pos, ultraSpace)) {
            PixelmonStructureGenEvent.Post generationEvent = new PixelmonStructureGenEvent.Post(world, s, (StructureInfo)structure, pos, s.generate(world, random));
            MinecraftForge.EVENT_BUS.post(generationEvent);
        }
    }

    protected boolean canSpawnStructureAtCoords(World world, StructureScattered s, StandaloneStructureInfo structure, BlockPos pos, boolean ultraSpace) {
        if (RandomHelper.getRandomNumberBetween(0, !ultraSpace ? 40 : 20) == 0) {
            PixelmonStructureGenEvent.Pre generationEvent = new PixelmonStructureGenEvent.Pre(world, s, structure, pos);
            MinecraftForge.EVENT_BUS.post(generationEvent);
            return !generationEvent.isCanceled();
        }
        return false;
    }

    public boolean isUltraSpace(Biome biome) {
        return UltraSpaceBiomeProvider.allowedBiomes.stream().anyMatch(ultraBiome -> ultraBiome.biomeName.equalsIgnoreCase(biome.biomeName));
    }
}

