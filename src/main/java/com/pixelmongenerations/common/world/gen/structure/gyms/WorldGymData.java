/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.structure.gyms;

import com.pixelmongenerations.common.world.gen.structure.gyms.WorldGymInfo;
import java.util.ArrayList;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.world.storage.WorldSavedData;

public class WorldGymData
extends WorldSavedData {
    private ArrayList<WorldGymInfo> gymList = new ArrayList();

    public WorldGymData() {
        super("gyminfo");
        this.markDirty();
    }

    public WorldGymData(String name) {
        super(name);
    }

    public void addGym(String name, ItemStack badge, int level) {
        this.gymList.add(new WorldGymInfo(name, badge, level));
        this.setDirty(true);
    }

    public int getGymCount() {
        return this.gymList.size();
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        NBTTagList tagList = nbt.getTagList("gymlist", 10);
        for (int i = 0; i < tagList.tagCount(); ++i) {
            if (!(tagList.get(i) instanceof NBTTagCompound)) continue;
            NBTTagCompound gymTag = tagList.getCompoundTagAt(i);
            this.gymList.add(new WorldGymInfo(gymTag));
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        NBTTagList nbttaglist = new NBTTagList();
        for (WorldGymInfo worldGymInfo : this.gymList) {
            nbttaglist.appendTag(worldGymInfo.getTag());
        }
        nbt.setTag("gymlist", nbttaglist);
        return nbt;
    }

    public ArrayList<WorldGymInfo> getGymList() {
        return this.gymList;
    }

    public int getGymLevel() {
        int size = this.gymList.size();
        if (size == 0) {
            return 15;
        }
        if (size == 1) {
            return 25;
        }
        if (size == 2) {
            return 35;
        }
        if (size == 3) {
            return 50;
        }
        return -1;
    }
}

