/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.structure;

import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ObsidianMagmaBoulder {
    public boolean generate(World world, BlockPos pos) {
        int i = pos.getX();
        int j = pos.getY();
        int k = pos.getZ();
        IBlockState obsidianState = Blocks.OBSIDIAN.getDefaultState();
        IBlockState magmaState = Blocks.MAGMA.getDefaultState();
        world.setBlockState(new BlockPos(i, j, k), obsidianState);
        world.setBlockState(new BlockPos(i, j + 1, k), obsidianState);
        world.setBlockState(new BlockPos(i + 1, j, k), obsidianState);
        world.setBlockState(new BlockPos(i, j, k + 1), obsidianState);
        world.setBlockState(new BlockPos(i, j, k - 1), obsidianState);
        world.setBlockState(new BlockPos(i - 1, j, k), magmaState);
        return true;
    }
}

