/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.feature;

import com.pixelmongenerations.common.block.MultiBlock;
import com.pixelmongenerations.common.item.PixelmonItemBlock;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.enums.EnumEvolutionRock;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

public class WorldGenEvolutionRock
implements IWorldGenerator {
    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        if (!world.provider.isDaytime()) {
            return;
        }
        int xPos = random.nextInt(16) + chunkX * 16;
        int zPos = random.nextInt(16) + chunkZ * 16;
        int yPos = world.getHeight(new BlockPos(xPos, 0, zPos)).getY() - 1;
        Biome biome = world.getBiome(new BlockPos(xPos, 0, zPos));
        Block block = world.getBlockState(new BlockPos(xPos, yPos - 1, zPos)).getBlock();
        if (block == Blocks.GRASS || block == Blocks.DIRT) {
            for (EnumEvolutionRock r : EnumEvolutionRock.values()) {
                for (Biome b : r.biomes) {
                    if (b != biome || random.nextDouble() >= 0.05) continue;
                    boolean canSpawn = true;
                    for (int ix = -1; ix < 2; ++ix) {
                        for (int iz = -1; iz < 2; ++iz) {
                            Block block2 = world.getBlockState(new BlockPos(xPos + ix, yPos - 1, zPos + iz)).getBlock();
                            if (block2 == Blocks.GRASS || block2 == Blocks.DIRT) continue;
                            canSpawn = false;
                        }
                    }
                    if (canSpawn) {
                        if (r == EnumEvolutionRock.IcyRock) {
                            PixelmonItemBlock.setMultiBlocksWidth(new BlockPos(xPos, yPos + 1, zPos), EnumFacing.EAST, world, (MultiBlock)PixelmonBlocks.icyRock, PixelmonBlocks.icyRock, null);
                        } else if (r == EnumEvolutionRock.MossyRock) {
                            PixelmonItemBlock.setMultiBlocksWidth(new BlockPos(xPos, yPos + 1, zPos), EnumFacing.EAST, world, (MultiBlock)PixelmonBlocks.mossyRock, PixelmonBlocks.mossyRock, null);
                        }
                    }
                    return;
                }
            }
        }
    }
}

