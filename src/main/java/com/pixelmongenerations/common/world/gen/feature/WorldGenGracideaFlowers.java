/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.feature;

import com.pixelmongenerations.core.config.PixelmonBlocks;
import java.util.Random;
import net.minecraft.init.Biomes;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

public class WorldGenGracideaFlowers
implements IWorldGenerator {
    @Override
    public void generate(Random r, int chunkX, int chunkZ, World world, IChunkGenerator chunkGen, IChunkProvider chunkProvider) {
        if (!world.provider.isSurfaceWorld()) {
            return;
        }
        Biome biome = world.getBiome(new BlockPos(chunkX * 16, 0, chunkZ * 16));
        if (biome == Biomes.MUTATED_FOREST) {
            int maxInChunk = r.nextInt(3);
            for (int i = 0; i < maxInChunk - 1; ++i) {
                int z;
                int y;
                int x = r.nextInt(16) + chunkX * 16;
                if (world.getBlockState(new BlockPos(x, (y = world.getHeight(new BlockPos(x, 0, z = r.nextInt(16) + chunkZ * 16)).getY()) - 1, z)).getBlock() != Blocks.GRASS) continue;
                world.setBlockState(new BlockPos(x, y, z), PixelmonBlocks.gracideaBlock.getDefaultState(), 1);
            }
        }
    }
}

