/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.JsonArray
 *  com.google.gson.JsonObject
 *  com.google.gson.JsonParser
 */
package com.pixelmongenerations.common.world.gen.structure.gyms;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.pixelmongenerations.common.entity.npcs.registry.BaseShopItem;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.world.gen.structure.StructureInfo;
import com.pixelmongenerations.common.world.gen.structure.gyms.PokemonDefinition;
import com.pixelmongenerations.common.world.gen.structure.towns.NPCPlacementInfo;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumNPCType;
import com.pixelmongenerations.core.enums.EnumType;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import net.minecraft.util.JsonUtils;

public class GymInfo
extends StructureInfo {
    public ArrayList<PokemonDefinition> pokemon = new ArrayList();
    public String name;
    public int level;
    public EnumType[] type;

    public void setGymInfo(String name, InputStream resourceAsStream) throws Exception {
        this.name = name;
        try {
            int i;
            JsonArray jsonarray;
            JsonObject json = new JsonParser().parse((Reader)new InputStreamReader(resourceAsStream, StandardCharsets.UTF_8)).getAsJsonObject();
            if (json.has("type")) {
                jsonarray = JsonUtils.getJsonArray(json, "type");
                this.type = new EnumType[jsonarray.size()];
                for (i = 0; i < jsonarray.size(); ++i) {
                    this.type[i] = EnumType.parseType(jsonarray.get(i).getAsString());
                }
            }
            if (json.has("pokemon")) {
                jsonarray = JsonUtils.getJsonArray(json, "pokemon");
                for (i = 0; i < jsonarray.size(); ++i) {
                    JsonObject pokemonel = jsonarray.get(i).getAsJsonObject();
                    this.pokemon.add(PokemonDefinition.readPokemonDefinition(name, pokemonel));
                }
            }
            if (json.has("npcs")) {
                jsonarray = JsonUtils.getJsonArray(json, "npcs");
                for (i = 0; i < jsonarray.size(); ++i) {
                    JsonObject npcel = jsonarray.get(i).getAsJsonObject();
                    String npcname = npcel.get("name").getAsString();
                    EnumNPCType type = EnumNPCType.getFromString(npcel.get("type").getAsString());
                    int x = npcel.get("x").getAsInt();
                    int y = npcel.get("y").getAsInt();
                    int z = npcel.get("z").getAsInt();
                    NPCPlacementInfo info = null;
                    int tier = -1;
                    if (npcel.has("tier")) {
                        tier = npcel.get("tier").getAsInt();
                    }
                    NPCPlacementInfo nPCPlacementInfo = npcel.has("rotation") ? new NPCPlacementInfo(npcname, type, x, y, z, npcel.get("rotation").getAsInt(), tier) : (info = tier != -1 ? new NPCPlacementInfo(npcname, type, x, y, z, 0, tier) : new NPCPlacementInfo(npcname, type, x, y, z));
                    if (npcel.has("drops")) {
                        JsonArray dropsArray = npcel.get("drops").getAsJsonArray();
                        for (int j = 0; j < dropsArray.size(); ++j) {
                            String itemName = dropsArray.get(j).getAsString();
                            BaseShopItem itembase = ServerNPCRegistry.shopkeepers.getItem(itemName);
                            if (itembase == null || itembase.getItem() == null) {
                                Pixelmon.LOGGER.warn("Item " + itemName + " could not be found in Gym " + name + " drops.");
                                continue;
                            }
                            info.addDrop(itembase.getItem());
                        }
                    }
                    this.npcs.add(info);
                }
            }
        }
        catch (Exception e) {
            throw new Exception("Failed to load Gym NPC data: " + name, e);
        }
    }
}

