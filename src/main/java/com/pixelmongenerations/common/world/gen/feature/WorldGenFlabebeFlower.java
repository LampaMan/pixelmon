/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.feature;

import com.pixelmongenerations.common.block.apricornTrees.BlockApricornTree;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.BlockBush;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

public class WorldGenFlabebeFlower
implements IWorldGenerator {
    private BlockBush[] flowers;

    @Override
    public void generate(Random r, int chunkX, int chunkZ, World world, IChunkGenerator chunkGen, IChunkProvider chunkProvider) {
        if (!world.provider.isSurfaceWorld()) {
            return;
        }
        if (r.nextInt(3) != 0) {
            return;
        }
        int maxInChunk = r.nextInt(6);
        for (int i = 0; i < maxInChunk - 1; ++i) {
            int z;
            int y;
            int x = r.nextInt(16) + chunkX * 16;
            Block block = world.getBlockState(new BlockPos(x, (y = world.getHeight(new BlockPos(x, 0, z = r.nextInt(16) + chunkZ * 16)).getY()) - 1, z)).getBlock();
            if (!BlockApricornTree.isSuitableSoil(block)) continue;
            BlockBush flower = this.getRandomFlower(r);
            world.setBlockState(new BlockPos(x, y, z), flower.getDefaultState(), 1);
        }
    }

    private BlockBush getRandomFlower(Random rand) {
        if (this.flowers == null) {
            this.flowers = new BlockBush[]{(BlockBush)PixelmonBlocks.flabebeFlowerAz, (BlockBush)PixelmonBlocks.flabebeFlowerBlue, (BlockBush)PixelmonBlocks.flabebeFlowerOrange, (BlockBush)PixelmonBlocks.flabebeFlowerRed, (BlockBush)PixelmonBlocks.flabebeFlowerWhite, (BlockBush)PixelmonBlocks.flabebeFlowerYellow};
        }
        return this.flowers[rand.nextInt(this.flowers.length)];
    }
}

