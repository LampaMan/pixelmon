/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.JsonArray
 *  com.google.gson.JsonObject
 *  com.google.gson.JsonParser
 */
package com.pixelmongenerations.common.world.gen.structure;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.world.gen.structure.StructureInfo;
import com.pixelmongenerations.common.world.gen.structure.gyms.GymInfo;
import com.pixelmongenerations.common.world.gen.structure.gyms.WorldGymData;
import com.pixelmongenerations.common.world.gen.structure.standalone.StandaloneStructureInfo;
import com.pixelmongenerations.common.world.gen.structure.towns.ComponentTownPart;
import com.pixelmongenerations.common.world.gen.structure.towns.NPCPlacementInfo;
import com.pixelmongenerations.common.world.gen.structure.towns.TownStructureInfo;
import com.pixelmongenerations.common.world.gen.structure.towns.VillagePartHandler;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.BetterSpawnerConfig;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumNPCType;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import com.pixelmongenerations.core.util.helper.WorldHelper;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.structure.MapGenStructureIO;
import net.minecraft.world.gen.structure.StructureComponent;
import net.minecraftforge.fml.common.registry.VillagerRegistry;

public class StructureRegistry {
    public static ArrayList<TownStructureInfo> townStructures = new ArrayList();
    public static ArrayList<StandaloneStructureInfo> standaloneStructures = new ArrayList();
    public static ArrayList<GymInfo> gyms = new ArrayList();
    public static HashMap<Biome, ArrayList<StandaloneStructureInfo>> structuresByBiome = new HashMap();
    public static ArrayList<String> structureIds = new ArrayList();

    public static void registerStructures() throws Exception {
        JsonObject struct;
        int i;
        JsonArray jsonArray;
        InputStream istream;
        JsonObject json;
        Pixelmon.LOGGER.info("Registering structures.");
        String path = Pixelmon.modDirectory + "/pixelmon/structures/";
        File structuresDir = new File(path);
        if (PixelmonConfig.useExternalJSONFilesStructures && !structuresDir.isDirectory()) {
            Pixelmon.LOGGER.info("Creating structures directory.");
            File baseDir = new File(Pixelmon.modDirectory + "/pixelmon");
            if (!baseDir.isDirectory()) {
                baseDir.mkdir();
            }
            structuresDir.mkdir();
            StructureRegistry.extractStructuresDir(structuresDir);
        }
        if ((json = new JsonParser().parse((Reader)new InputStreamReader(istream = !PixelmonConfig.useExternalJSONFilesStructures ? StructureRegistry.class.getResourceAsStream("/assets/pixelmon/structures/structures.json") : new FileInputStream(new File(structuresDir, "structures.json")), StandardCharsets.UTF_8)).getAsJsonObject()).has("towns")) {
            jsonArray = JsonUtils.getJsonArray(json, "towns");
            for (i = 0; i < jsonArray.size(); ++i) {
                struct = jsonArray.get(i).getAsJsonObject();
                String structureName = struct.get("id").getAsString();
                if (!PixelmonConfig.spawnPokemarts && structureName.equals("townmart1")) continue;
                try {
                    StructureRegistry.registerTownStructure(struct, path + "towns/");
                    continue;
                }
                catch (Exception e) {
                    throw new Exception("Failed to load town structure " + structureName + ".", e);
                }
            }
            MapGenStructureIO.registerStructureComponent(ComponentTownPart.class, "TownPart");
            VillagerRegistry.instance().registerVillageCreationHandler(new VillagePartHandler());
        }
        if (json.has("gyms")) {
            jsonArray = JsonUtils.getJsonArray(json, "gyms");
            for (i = 0; i < jsonArray.size(); ++i) {
                struct = jsonArray.get(i).getAsJsonObject();
                try {
                    StructureRegistry.registerGym(struct, path + "gyms/");
                    continue;
                }
                catch (Exception e) {
                    throw new Exception("Failed to load gym: " + struct.get("id").getAsString() + ".", e);
                }
            }
        }
        if (json.has("standalone")) {
            BetterSpawnerConfig.INSTANCE.biomeCategories.forEach((k, v) -> Pixelmon.LOGGER.debug("Mapped Biome category '" + k + "' to biomes: " + String.join((CharSequence)", ", v)));
            jsonArray = JsonUtils.getJsonArray(json, "standalone");
            for (i = 0; i < jsonArray.size(); ++i) {
                struct = jsonArray.get(i).getAsJsonObject();
                try {
                    StructureRegistry.registerStandaloneStructure(struct, path + "standAlone/");
                    continue;
                }
                catch (Exception e) {
                    throw new Exception("Failed to load standalone structure " + struct.get("id").getAsString() + ".", e);
                }
            }
        }
    }

    private static void extractStructuresDir(File structuresDir) {
        JsonObject strucel;
        File standAloneDir;
        String filename;
        int i;
        JsonArray jsonarray;
        InputStream istream = StructureRegistry.class.getResourceAsStream("/assets/pixelmon/structures/structures.json");
        StructureRegistry.extractFile("/assets/pixelmon/structures/structures.json", structuresDir, "structures.json");
        JsonObject json = new JsonParser().parse((Reader)new InputStreamReader(istream, StandardCharsets.UTF_8)).getAsJsonObject();
        if (json.has("towns")) {
            File townsDir = new File(structuresDir, "towns");
            townsDir.mkdir();
            jsonarray = JsonUtils.getJsonArray(json, "towns");
            for (i = 0; i < jsonarray.size(); ++i) {
                JsonObject townel = jsonarray.get(i).getAsJsonObject();
                filename = townel.get("filename").getAsString();
                StructureRegistry.extractFile("/assets/pixelmon/structures/towns/" + filename, townsDir, filename);
            }
        }
        if (json.has("gyms")) {
            File gymsDir = new File(structuresDir, "gyms");
            gymsDir.mkdir();
            jsonarray = JsonUtils.getJsonArray(json, "gyms");
            for (i = 0; i < jsonarray.size(); ++i) {
                JsonObject gymel = jsonarray.get(i).getAsJsonObject();
                filename = gymel.get("filename").getAsString();
                StructureRegistry.extractFile("/assets/pixelmon/structures/gyms/" + filename, gymsDir, filename);
                String npcfilename = gymel.get("npcdata").getAsString();
                StructureRegistry.extractFile("/assets/pixelmon/structures/gyms/" + npcfilename, gymsDir, npcfilename);
            }
        }
        if (json.has("standalone")) {
            standAloneDir = new File(structuresDir, "standAlone");
            standAloneDir.mkdir();
            jsonarray = JsonUtils.getJsonArray(json, "standalone");
            for (i = 0; i < jsonarray.size(); ++i) {
                strucel = jsonarray.get(i).getAsJsonObject();
                filename = strucel.get("filename").getAsString();
                StructureRegistry.extractFile("/assets/pixelmon/structures/standAlone/" + filename, standAloneDir, filename);
            }
        }
        if (json.has("extradimensional")) {
            standAloneDir = new File(structuresDir, "extraDimensional");
            standAloneDir.mkdir();
            jsonarray = JsonUtils.getJsonArray(json, "extradimensional");
            for (i = 0; i < jsonarray.size(); ++i) {
                strucel = jsonarray.get(i).getAsJsonObject();
                filename = strucel.get("filename").getAsString();
                StructureRegistry.extractFile("/assets/pixelmon/structures/extraDimensional/" + filename, standAloneDir, filename);
            }
        }
    }

    private static void extractFile(String resourceName, File dir, String filename) {
        try {
            File file = new File(dir, filename);
            if (!file.exists()) {
                int nBytes;
                InputStream link = ServerNPCRegistry.class.getResourceAsStream(resourceName);
                BufferedInputStream in = new BufferedInputStream(link);
                BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file));
                byte[] buffer = new byte[2048];
                while ((nBytes = in.read(buffer)) > 0) {
                    out.write(buffer, 0, nBytes);
                }
                out.flush();
                out.close();
                in.close();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void registerStandaloneStructure(JsonObject strucObj, String path) throws IOException {
        String filename;
        StandaloneStructureInfo info = new StandaloneStructureInfo();
        String id = strucObj.get("id").getAsString();
        if (!PixelmonConfig.spawnBirdShrines && id.contains("shrine")) {
            return;
        }
        info.setId(id);
        structureIds.add(id);
        if (strucObj.has("depth")) {
            int depth = strucObj.get("depth").getAsInt();
            info.setDepth(depth);
        }
        if (strucObj.has("underwater")) {
            info.setUnderwater(strucObj.get("underwater").getAsBoolean());
        }
        if (!(filename = strucObj.get("filename").getAsString()).contains(".schem")) {
            System.out.println(String.format("%s filename needs to be in .schem format.", info.id));
            return;
        }
        if (!PixelmonConfig.useExternalJSONFilesStructures) {
            info.setSnapshot(StructureRegistry.loadSnapshot(StructureRegistry.class.getResourceAsStream("/assets/pixelmon/structures/standAlone/" + filename)));
        } else {
            info.setSnapshot(StructureRegistry.loadSnapshot(path + filename));
        }
        int rarity = strucObj.get("rarity").getAsInt();
        info.setRarity(rarity);
        if (strucObj.has("biomes")) {
            JsonArray biomeArray = strucObj.get("biomes").getAsJsonArray();
            for (int i = 0; i < biomeArray.size(); ++i) {
                String biome = biomeArray.get(i).getAsString();
                if (BetterSpawnerConfig.INSTANCE.biomeCategories.containsKey(biome)) {
                    BetterSpawnerConfig.INSTANCE.biomeCategories.get(biome).stream().map(ResourceLocation::new).map(WorldHelper::demandBiome).filter(Optional::isPresent).map(Optional::get).forEach(info::addBiome);
                    continue;
                }
                WorldHelper.demandBiome(new ResourceLocation(biome)).ifPresent(info::addBiome);
            }
        }
        if (strucObj.has("npcs")) {
            JsonArray npcsArray = strucObj.getAsJsonArray("npcs");
            for (int j = 0; j < npcsArray.size(); ++j) {
                JsonObject npcsObj = npcsArray.get(j).getAsJsonObject();
                EnumNPCType type = EnumNPCType.getFromString(npcsObj.get("type").getAsString());
                int x = npcsObj.get("x").getAsInt();
                int y = npcsObj.get("y").getAsInt();
                int z = npcsObj.get("z").getAsInt();
                NPCPlacementInfo npcInfo = new NPCPlacementInfo(type, x, y, z);
                if (npcsObj.has("data")) {
                    npcInfo.setData(npcsObj.get("data").getAsString());
                }
                info.addNPC(npcInfo);
            }
        }
        for (Biome biome : info.getBiomes()) {
            ArrayList<StandaloneStructureInfo> structures = structuresByBiome.containsKey(biome) ? structuresByBiome.get(biome) : new ArrayList<>();
            structures.add(info);
            structuresByBiome.put(biome, structures);
        }
        standaloneStructures.add(info);
    }

    private static void registerGym(JsonObject strucObj, String path) throws Exception {
        GymInfo info = new GymInfo();
        String id = strucObj.get("id").getAsString();
        info.setId(id);
        structureIds.add(id);
        if (strucObj.has("depth")) {
            int depth = strucObj.get("depth").getAsInt();
            info.setDepth(depth);
        }
        String filename = strucObj.get("filename").getAsString();
        String npcfilename = strucObj.get("npcdata").getAsString();
        if (!filename.contains(".schem")) {
            System.out.println(String.format("%s filename needs to be in .schem format.", info.id));
            return;
        }
        if (!PixelmonConfig.useExternalJSONFilesStructures) {
            info.setSnapshot(StructureRegistry.loadSnapshot(StructureRegistry.class.getResourceAsStream("/assets/pixelmon/structures/gyms/" + filename)));
            info.setGymInfo(npcfilename, StructureRegistry.class.getResourceAsStream("/assets/pixelmon/structures/gyms/" + npcfilename));
        } else {
            info.setSnapshot(StructureRegistry.loadSnapshot(path + filename));
            info.setGymInfo(npcfilename, new FileInputStream(path + npcfilename));
        }
        if (strucObj.has("npcs")) {
            JsonArray npcsArray = strucObj.getAsJsonArray("npcs");
            for (int j = 0; j < npcsArray.size(); ++j) {
                JsonObject npcsObj = npcsArray.get(j).getAsJsonObject();
                EnumNPCType type = EnumNPCType.getFromString(npcsObj.get("type").getAsString());
                int x = npcsObj.get("x").getAsInt();
                int y = npcsObj.get("y").getAsInt();
                int z = npcsObj.get("z").getAsInt();
                NPCPlacementInfo npcInfo = new NPCPlacementInfo(type, x, y, z);
                if (npcsObj.has("data")) {
                    npcInfo.setData(npcsObj.get("data").getAsString());
                }
                info.addNPC(npcInfo);
            }
        }
        gyms.add(info);
    }

    private static void registerTownStructure(JsonObject townObj, String path) throws IOException {
        String filename;
        TownStructureInfo info = new TownStructureInfo();
        String id = townObj.get("id").getAsString();
        info.setId(id);
        structureIds.add(id);
        int weighting = townObj.get("weighting").getAsInt();
        info.setWeighting(weighting);
        int maxNum = townObj.get("maxnum").getAsInt();
        info.setMaxNum(maxNum);
        VillagePartHandler.weightSum += weighting;
        VillagePartHandler.numCompSum += maxNum;
        if (townObj.has("depth")) {
            int depth = townObj.get("depth").getAsInt();
            info.setDepth(depth);
        }
        if (!(filename = townObj.get("filename").getAsString()).contains(".schem")) {
            return;
        }
        if (!PixelmonConfig.useExternalJSONFilesStructures) {
            info.setSnapshot(StructureRegistry.loadSnapshot(StructureRegistry.class.getResourceAsStream("/assets/pixelmon/structures/towns/" + filename)));
        } else {
            info.setSnapshot(StructureRegistry.loadSnapshot(path + filename));
        }
        if (townObj.has("npcs")) {
            JsonArray npcsArray = townObj.getAsJsonArray("npcs");
            for (int j = 0; j < npcsArray.size(); ++j) {
                JsonObject npcsObj = npcsArray.get(j).getAsJsonObject();
                EnumNPCType type = EnumNPCType.getFromString(npcsObj.get("type").getAsString());
                int x = npcsObj.get("x").getAsInt();
                int y = npcsObj.get("y").getAsInt();
                int z = npcsObj.get("z").getAsInt();
                NPCPlacementInfo npcInfo = new NPCPlacementInfo(type, x, y, z);
                if (npcsObj.has("data")) {
                    npcInfo.setData(npcsObj.get("data").getAsString());
                }
                info.addNPC(npcInfo);
            }
        }
        townStructures.add(info);
    }

    private static NBTTagCompound loadSnapshot(String snapshotPath) throws IOException {
        return StructureRegistry.loadSnapshot(new FileInputStream(new File(snapshotPath)));
    }

    private static NBTTagCompound loadSnapshot(InputStream stream) throws IOException {
        return CompressedStreamTools.readCompressed(new DataInputStream(stream));
    }

    public static StructureInfo getByID(String id) {
        for (StructureInfo structureInfo : townStructures) {
            if (!id.equalsIgnoreCase(structureInfo.id)) continue;
            return structureInfo;
        }
        for (StructureInfo structureInfo : standaloneStructures) {
            if (!id.equalsIgnoreCase(structureInfo.id)) continue;
            return structureInfo;
        }
        for (StructureInfo structureInfo : gyms) {
            if (!id.equalsIgnoreCase(structureInfo.id)) continue;
            return structureInfo;
        }
        return null;
    }

    public static StructureInfo getRandomTownPiece(Random random, List<StructureComponent> pieces) throws Exception {
        int weightSum = 0;
        for (TownStructureInfo info : townStructures) {
            if (!info.canPlaceMore(pieces)) continue;
            weightSum += info.getWeight();
        }
        int weight = random.nextInt(Math.max(1, weightSum));
        int count = 0;
        for (TownStructureInfo info : townStructures) {
            if (!info.canPlaceMore(pieces) || weight > (count += info.getWeight())) continue;
            return info;
        }
        Pixelmon.LOGGER.error("Couldn't get a random structure for town generation.");
        return null;
    }

    public static StandaloneStructureInfo getScatteredStructureFromBiome(Random random, Biome biomeGenForCoords) {
        ArrayList<StandaloneStructureInfo> possibleStructures = null;
        possibleStructures = structuresByBiome.get(biomeGenForCoords);
        if (possibleStructures != null && !possibleStructures.isEmpty()) {
            int weightSum = 0;
            for (StandaloneStructureInfo info : possibleStructures) {
                weightSum += info.getRarity();
            }
            if (weightSum <= 0) {
                return null;
            }
            int weight = random.nextInt(weightSum);
            int count = 0;
            for (StandaloneStructureInfo info : possibleStructures) {
                if (weight > (count += info.getRarity())) continue;
                return info;
            }
        }
        return null;
    }

    public static GymInfo getNextGym(WorldGymData data, Random random) {
        return Math.random() < (double)PixelmonConfig.gymChance ? RandomHelper.getRandomElementFromList(gyms) : null;
    }
}

