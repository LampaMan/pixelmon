/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world;

import com.pixelmongenerations.common.world.ultraspace.biome.UltraBurstBiome;
import com.pixelmongenerations.common.world.ultraspace.biome.UltraDarkForestBiome;
import com.pixelmongenerations.common.world.ultraspace.biome.UltraDeepSeaBiome;
import com.pixelmongenerations.common.world.ultraspace.biome.UltraDesertBiome;
import com.pixelmongenerations.common.world.ultraspace.biome.UltraForestBiome;
import com.pixelmongenerations.common.world.ultraspace.biome.UltraJungleBiome;
import com.pixelmongenerations.common.world.ultraspace.biome.UltraPlantBiome;
import com.pixelmongenerations.common.world.ultraspace.biome.UltraRuinBiome;
import com.pixelmongenerations.common.world.ultraspace.biome.UltraSwampBiome;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.registries.IForgeRegistry;

@GameRegistry.ObjectHolder(value="pixelmon")
public class ModBiomes {
    public static final UltraDarkForestBiome ultradarkforest = null;
    public static final UltraBurstBiome ultraburst = null;
    public static final UltraJungleBiome ultrajungle = null;
    public static final UltraForestBiome ultraforest = null;
    public static final UltraDesertBiome ultradesert = null;
    public static final UltraDeepSeaBiome ultradeepsea = null;
    public static final UltraSwampBiome ultraswamp = null;
    public static final UltraRuinBiome ultraruin = null;
    public static final UltraPlantBiome ultraplant = null;

    @Mod.EventBusSubscriber(modid="pixelmon")
    public static class RegistrationHandler {
        @SubscribeEvent
        public static void onEvent(RegistryEvent.Register<Biome> event) {
            IForgeRegistry<Biome> registry = event.getRegistry();
            registry.register((Biome)new UltraDarkForestBiome(new Biome.BiomeProperties("ultradarkforest").setTemperature(0.7f).setRainfall(0.8f)).setRegistryName("pixelmon", "ultradarkforest"));
            registry.register((Biome)new UltraBurstBiome(new Biome.BiomeProperties("ultraburst").setTemperature(0.7f).setRainfall(0.8f)).setRegistryName("pixelmon", "ultraburst"));
            registry.register((Biome)new UltraJungleBiome(false, new Biome.BiomeProperties("ultrajungle").setTemperature(0.7f).setRainfall(0.8f)).setRegistryName("pixelmon", "ultrajungle"));
            registry.register((Biome)new UltraForestBiome(new Biome.BiomeProperties("ultraforest").setTemperature(0.7f).setRainfall(0.8f)).setRegistryName("pixelmon", "ultraforest"));
            registry.register((Biome)new UltraDesertBiome(new Biome.BiomeProperties("ultradesert").setTemperature(0.7f).setRainfall(0.8f)).setRegistryName("pixelmon", "ultradesert"));
            registry.register((Biome)new UltraDeepSeaBiome(new Biome.BiomeProperties("ultradeepsea").setBaseHeight(-1.0f).setHeightVariation(0.1f)).setRegistryName("pixelmon", "ultradeepsea"));
            registry.register((Biome)new UltraSwampBiome(new Biome.BiomeProperties("ultraswamp").setBaseHeight(-0.2f).setHeightVariation(0.1f).setTemperature(0.8f).setRainfall(0.9f).setWaterColor(14745518)).setRegistryName("pixelmon", "ultraswamp"));
            registry.register((Biome)new UltraRuinBiome(true, false, new Biome.BiomeProperties("ultraruin").setTemperature(2.0f).setRainfall(0.0f).setRainDisabled()).setRegistryName("pixelmon", "ultraruin"));
            registry.register((Biome)new UltraPlantBiome(new Biome.BiomeProperties("ultraplant").setTemperature(0.7f).setRainfall(0.8f)).setRegistryName("pixelmon", "ultraplant"));
        }
    }
}

