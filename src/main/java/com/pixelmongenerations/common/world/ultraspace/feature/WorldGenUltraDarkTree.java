/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.ultraspace.feature;

import com.pixelmongenerations.core.config.PixelmonBlocks;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.BlockStainedGlass;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenAbstractTree;

public class WorldGenUltraDarkTree
extends WorldGenAbstractTree {
    private static final IBlockState LOG = PixelmonBlocks.ultraDarkLog.getDefaultState();
    private static final IBlockState LEAF = Blocks.STAINED_GLASS.getDefaultState().withProperty(BlockStainedGlass.COLOR, EnumDyeColor.PURPLE);
    private static final IBlockState LEAF2 = Blocks.STAINED_GLASS.getDefaultState().withProperty(BlockStainedGlass.COLOR, EnumDyeColor.MAGENTA);
    private final boolean useExtraRandomHeight;

    public WorldGenUltraDarkTree(boolean notify, boolean useExtraRandomHeightIn) {
        super(notify);
        this.useExtraRandomHeight = useExtraRandomHeightIn;
    }

    @Override
    protected boolean canGrowInto(Block blockType) {
        Material material = blockType.getDefaultState().getMaterial();
        return blockType == Blocks.OBSIDIAN || blockType == Blocks.GRASS || blockType == Blocks.DIRT;
    }

    @Override
    public boolean generate(World worldIn, Random rand, BlockPos position) {
        int i = rand.nextInt(3) + 5;
        if (this.useExtraRandomHeight) {
            i += rand.nextInt(7);
        }
        boolean flag = true;
        if (position.getY() >= 1 && position.getY() + i + 1 <= 256) {
            boolean isSoil;
            for (int j = position.getY(); j <= position.getY() + 1 + i; ++j) {
                int k = 1;
                if (j == position.getY()) {
                    k = 0;
                }
                if (j >= position.getY() + 1 + i - 2) {
                    k = 2;
                }
                BlockPos.MutableBlockPos blockpos$mutableblockpos = new BlockPos.MutableBlockPos();
                for (int l = position.getX() - k; l <= position.getX() + k && flag; ++l) {
                    for (int i1 = position.getZ() - k; i1 <= position.getZ() + k && flag; ++i1) {
                        if (j >= 0 && j < worldIn.getHeight()) {
                            if (this.isReplaceable(worldIn, blockpos$mutableblockpos.setPos(l, j, i1))) continue;
                            flag = false;
                            continue;
                        }
                        flag = false;
                    }
                }
            }
            if (!flag) {
                return false;
            }
            BlockPos down = position.down();
            IBlockState state = worldIn.getBlockState(down);
            boolean bl = isSoil = state.getBlock() == Blocks.GRASS || state.getBlock() == Blocks.DIRT;
            if (isSoil && position.getY() < worldIn.getHeight() - i - 1) {
                state.getBlock().onPlantGrow(state, worldIn, down, position);
                for (int i2 = position.getY() - 3 + i; i2 <= position.getY() + i; ++i2) {
                    int k2 = i2 - (position.getY() + i);
                    int l2 = 1 - k2 / 2;
                    for (int i3 = position.getX() - l2; i3 <= position.getX() + l2; ++i3) {
                        int j1 = i3 - position.getX();
                        for (int k1 = position.getZ() - l2; k1 <= position.getZ() + l2; ++k1) {
                            BlockPos blockpos;
                            IBlockState state2;
                            int l1 = k1 - position.getZ();
                            if (Math.abs(j1) == l2 && Math.abs(l1) == l2 && (rand.nextInt(2) == 0 || k2 == 0) || !(state2 = worldIn.getBlockState(blockpos = new BlockPos(i3, i2, k1))).getBlock().isAir(state2, worldIn, blockpos) && !state2.getBlock().isAir(state2, worldIn, blockpos)) continue;
                            this.setBlockAndNotifyAdequately(worldIn, blockpos, this.getRandomLeaf());
                        }
                    }
                }
                for (int j2 = 0; j2 < i; ++j2) {
                    BlockPos upN = position.up(j2);
                    IBlockState state2 = worldIn.getBlockState(upN);
                    if (!state2.getBlock().isAir(state2, worldIn, upN) && !state2.getBlock().isLeaves(state2, worldIn, upN)) continue;
                    this.setBlockAndNotifyAdequately(worldIn, position.up(j2), LOG);
                }
                return true;
            }
            return false;
        }
        return false;
    }

    public IBlockState getRandomLeaf() {
        Random random = new Random();
        if (random.nextInt(5) == 0) {
            return LEAF;
        }
        return LEAF2;
    }
}

