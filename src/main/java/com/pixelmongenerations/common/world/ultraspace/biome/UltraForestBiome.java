/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.ultraspace.biome;

import java.util.Random;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.feature.WorldGenAbstractTree;
import net.minecraft.world.gen.feature.WorldGenBigMushroom;
import net.minecraft.world.gen.feature.WorldGenBigTree;
import net.minecraft.world.gen.feature.WorldGenCanopyTree;
import net.minecraftforge.event.terraingen.DecorateBiomeEvent;
import net.minecraftforge.event.terraingen.TerrainGen;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class UltraForestBiome
extends Biome {
    protected static final WorldGenBigTree BIG_TREE = new WorldGenBigTree(false);
    protected static final WorldGenCanopyTree CANOPY_TREE = new WorldGenCanopyTree(false);
    private static final IBlockState MAGMA = Blocks.MAGMA.getDefaultState();
    private static final IBlockState OBSIDIAN = Blocks.OBSIDIAN.getDefaultState();

    public UltraForestBiome(Biome.BiomeProperties properties) {
        super(properties);
        this.topBlock = Blocks.GRASS.getDefaultState();
        this.fillerBlock = Blocks.DIRT.getDefaultState();
        this.decorator.treesPerChunk = 10;
    }

    @Override
    public WorldGenAbstractTree getRandomTreeFeature(Random rand) {
        if (rand.nextInt(10) == 0) {
            // empty if block
        }
        if (rand.nextInt(2) == 1) {
            return BIG_TREE;
        }
        return CANOPY_TREE;
    }

    @Override
    public void decorate(World worldIn, Random rand, BlockPos pos) {
        this.addMushrooms(worldIn, rand, pos);
        super.decorate(worldIn, rand, pos);
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public int getGrassColorAtPos(BlockPos pos) {
        return this.getModdedBiomeGrassColor(0xCCFFCC);
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public int getFoliageColorAtPos(BlockPos pos) {
        double d0 = 0.9f;
        double d1 = 0.9f;
        return this.getModdedBiomeFoliageColor(16766962);
    }

    public void addMushrooms(World p_185379_1_, Random p_185379_2_, BlockPos p_185379_3_) {
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
                int k = i * 4 + 1 + 8 + p_185379_2_.nextInt(3);
                int l = j * 4 + 1 + 8 + p_185379_2_.nextInt(3);
                BlockPos blockpos = p_185379_1_.getHeight(p_185379_3_.add(k, 0, l));
                if (p_185379_2_.nextInt(20) == 0 && TerrainGen.decorate(p_185379_1_, p_185379_2_, new ChunkPos(p_185379_3_), blockpos, DecorateBiomeEvent.Decorate.EventType.BIG_SHROOM)) {
                    WorldGenBigMushroom worldgenbigmushroom = new WorldGenBigMushroom();
                    worldgenbigmushroom.generate(p_185379_1_, p_185379_2_, blockpos);
                    continue;
                }
                if (!TerrainGen.decorate(p_185379_1_, p_185379_2_, new ChunkPos(p_185379_3_), blockpos, DecorateBiomeEvent.Decorate.EventType.TREE)) continue;
                WorldGenAbstractTree worldgenabstracttree = this.getRandomTreeFeature(p_185379_2_);
                worldgenabstracttree.setDecorationDefaults();
                if (!worldgenabstracttree.generate(p_185379_1_, p_185379_2_, blockpos)) continue;
                worldgenabstracttree.generateSaplings(p_185379_1_, p_185379_2_, blockpos);
            }
        }
    }
}

