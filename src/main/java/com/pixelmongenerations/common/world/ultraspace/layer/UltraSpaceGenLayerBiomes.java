/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.ultraspace.layer;

import com.pixelmongenerations.common.world.ModBiomes;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.layer.GenLayer;
import net.minecraft.world.gen.layer.IntCache;

public class UltraSpaceGenLayerBiomes
extends GenLayer {
    protected Biome[] allowedBiomes = new Biome[]{ModBiomes.ultraburst, ModBiomes.ultradarkforest, ModBiomes.ultrajungle, ModBiomes.ultraforest, ModBiomes.ultradesert, ModBiomes.ultradeepsea, ModBiomes.ultraswamp, ModBiomes.ultraruin, ModBiomes.ultraplant};

    public UltraSpaceGenLayerBiomes(long seed, GenLayer genlayer) {
        super(seed);
        this.parent = genlayer;
    }

    public UltraSpaceGenLayerBiomes(long seed) {
        super(seed);
    }

    @Override
    public int[] getInts(int x, int z, int width, int depth) {
        int[] dest = IntCache.getIntCache(width * depth);
        for (int dz = 0; dz < depth; ++dz) {
            for (int dx = 0; dx < width; ++dx) {
                this.initChunkSeed(dx + x, dz + z);
                dest[dx + dz * width] = Biome.getIdForBiome(this.allowedBiomes[this.nextInt(this.allowedBiomes.length)]);
            }
        }
        return dest;
    }
}

