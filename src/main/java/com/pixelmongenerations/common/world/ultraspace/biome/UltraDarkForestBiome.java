/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.ultraspace.biome;

import com.pixelmongenerations.common.world.ultraspace.feature.WorldGenStoneSpike;
import com.pixelmongenerations.common.world.ultraspace.feature.WorldGenUltraDarkTree;
import java.util.Random;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.feature.WorldGenAbstractTree;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class UltraDarkForestBiome
extends Biome {
    protected static final WorldGenUltraDarkTree OBSIDIAN_TREE = new WorldGenUltraDarkTree(false, false);
    private static final IBlockState MAGMA = Blocks.MAGMA.getDefaultState();
    private static final IBlockState OBSIDIAN = Blocks.OBSIDIAN.getDefaultState();
    private final WorldGenStoneSpike stoneBoulder = new WorldGenStoneSpike();

    public UltraDarkForestBiome(Biome.BiomeProperties properties) {
        super(properties);
        this.decorator.treesPerChunk = 5;
        this.topBlock = Blocks.GRASS.getDefaultState();
        this.fillerBlock = Blocks.DIRT.getDefaultState();
    }

    @Override
    public WorldGenAbstractTree getRandomTreeFeature(Random rand) {
        return OBSIDIAN_TREE;
    }

    @Override
    public void decorate(World worldIn, Random rand, BlockPos pos) {
        if (rand.nextInt(50) <= 10) {
            int j = rand.nextInt(16) + 10;
            int k = rand.nextInt(16) + 10;
            this.stoneBoulder.generate(worldIn, rand, worldIn.getHeight(pos.add(j, 0, k)));
        }
        super.decorate(worldIn, rand, pos);
    }

    @Override
    public Class<? extends Biome> getBiomeClass() {
        return UltraDarkForestBiome.class;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public int getGrassColorAtPos(BlockPos pos) {
        return 5269605;
    }
}

