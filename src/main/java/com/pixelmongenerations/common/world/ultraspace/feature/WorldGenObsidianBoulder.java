/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.ultraspace.feature;

import java.util.Random;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenTrees;

public class WorldGenObsidianBoulder
extends WorldGenTrees {
    private final IBlockState leavesMetadata;
    private final IBlockState woodMetadata;

    public WorldGenObsidianBoulder(IBlockState p_i46450_1_, IBlockState p_i46450_2_) {
        super(false);
        this.woodMetadata = p_i46450_1_;
        this.leavesMetadata = p_i46450_2_;
    }

    @Override
    public boolean generate(World worldIn, Random rand, BlockPos position) {
        Random random = new Random();
        IBlockState iblockstate = worldIn.getBlockState(position);
        while ((iblockstate.getBlock().isAir(iblockstate, worldIn, position) || iblockstate.getBlock().isLeaves(iblockstate, worldIn, position)) && position.getY() > 0) {
            position = position.down();
            iblockstate = worldIn.getBlockState(position);
        }
        IBlockState state = worldIn.getBlockState(position);
        position = position.up();
        for (int i = position.getY(); i <= position.getY() + 2; ++i) {
            int j = i - position.getY();
            int k = 2 - j;
            for (int l = position.getX() - k; l <= position.getX() + k; ++l) {
                int i1 = l - position.getX();
                for (int j1 = position.getZ() - k; j1 <= position.getZ() + k; ++j1) {
                    BlockPos blockpos;
                    int k1 = j1 - position.getZ();
                    if (Math.abs(i1) == k && Math.abs(k1) == k && rand.nextInt(2) == 0 || !(state = worldIn.getBlockState(blockpos = new BlockPos(l, i, j1))).getBlock().canBeReplacedByLeaves(state, worldIn, blockpos)) continue;
                    if (random.nextInt(5) == 0) {
                        if (this.inWater(worldIn, blockpos)) continue;
                        this.setBlockAndNotifyAdequately(worldIn, blockpos, this.leavesMetadata);
                        continue;
                    }
                    if (this.inWater(worldIn, position)) continue;
                    this.setBlockAndNotifyAdequately(worldIn, position, this.woodMetadata);
                }
            }
        }
        return true;
    }

    public boolean inWater(World world, BlockPos pos) {
        IBlockState block = world.getBlockState(pos.down());
        return block.getBlock() == Blocks.WATER ? true : (world.getBlockState(pos.down().down()).getBlock() == Blocks.WATER ? true : world.getBlockState(pos.down().down().down()).getBlock() == Blocks.WATER);
    }
}

