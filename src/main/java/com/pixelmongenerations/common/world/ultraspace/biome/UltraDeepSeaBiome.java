/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.ultraspace.biome;

import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeOcean;

public class UltraDeepSeaBiome
extends BiomeOcean {
    public UltraDeepSeaBiome(Biome.BiomeProperties properties) {
        super(properties);
    }
}

