/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.ultraspace.biome;

import java.util.Random;
import net.minecraft.block.BlockFlower;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.monster.EntitySlime;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.ChunkPrimer;
import net.minecraft.world.gen.feature.WorldGenAbstractTree;
import net.minecraft.world.gen.feature.WorldGenBigMushroom;
import net.minecraft.world.gen.feature.WorldGenFossils;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.terraingen.BiomeEvent;
import net.minecraftforge.event.terraingen.DecorateBiomeEvent;
import net.minecraftforge.event.terraingen.TerrainGen;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class UltraSwampBiome
extends Biome {
    protected static final IBlockState WATER_LILY = Blocks.WATERLILY.getDefaultState();

    public UltraSwampBiome(Biome.BiomeProperties properties) {
        super(properties);
        this.decorator.treesPerChunk = 2;
        this.decorator.flowersPerChunk = 1;
        this.decorator.deadBushPerChunk = 1;
        this.decorator.mushroomsPerChunk = 8;
        this.decorator.reedsPerChunk = 10;
        this.decorator.clayPerChunk = 1;
        this.decorator.waterlilyPerChunk = 4;
        this.decorator.sandPatchesPerChunk = 0;
        this.decorator.gravelPatchesPerChunk = 0;
        this.decorator.grassPerChunk = 5;
        this.topBlock = Blocks.MYCELIUM.getDefaultState();
        this.spawnableMonsterList.add(new Biome.SpawnListEntry(EntitySlime.class, 1, 1, 1));
    }

    @Override
    public WorldGenAbstractTree getRandomTreeFeature(Random rand) {
        return SWAMP_FEATURE;
    }

    @Override
    public BlockFlower.EnumFlowerType pickRandomFlower(Random rand, BlockPos pos) {
        return BlockFlower.EnumFlowerType.BLUE_ORCHID;
    }

    @Override
    public int getWaterColorMultiplier() {
        BiomeEvent.GetWaterColor event = new BiomeEvent.GetWaterColor(this, 10682515);
        MinecraftForge.EVENT_BUS.post(event);
        return event.getNewColor();
    }

    @Override
    public void genTerrainBlocks(World worldIn, Random rand, ChunkPrimer chunkPrimerIn, int x, int z, double noiseVal) {
        double d0 = GRASS_COLOR_NOISE.getValue((double)x * 0.25, (double)z * 0.25);
        if (d0 > 0.0) {
            int i = x & 0xF;
            int j = z & 0xF;
            for (int k = 255; k >= 0; --k) {
                if (chunkPrimerIn.getBlockState(j, k, i).getMaterial() == Material.AIR) continue;
                if (k != 62 || chunkPrimerIn.getBlockState(j, k, i).getBlock() == Blocks.WATER) break;
                chunkPrimerIn.setBlockState(j, k, i, WATER);
                if (!(d0 < 0.12)) break;
                chunkPrimerIn.setBlockState(j, k + 1, i, WATER_LILY);
                break;
            }
        }
        this.generateBiomeTerrain(worldIn, rand, chunkPrimerIn, x, z, noiseVal);
    }

    @Override
    public void decorate(World worldIn, Random rand, BlockPos pos) {
        this.addMushrooms(worldIn, rand, pos);
        super.decorate(worldIn, rand, pos);
        if (TerrainGen.decorate(worldIn, rand, new ChunkPos(pos), DecorateBiomeEvent.Decorate.EventType.FOSSIL) && rand.nextInt(64) == 0) {
            new WorldGenFossils().generate(worldIn, rand, pos);
        }
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public int getGrassColorAtPos(BlockPos pos) {
        double d0 = GRASS_COLOR_NOISE.getValue((double)pos.getX() * 0.0225, (double)pos.getZ() * 0.0225);
        return this.getModdedBiomeGrassColor(d0 < -0.1 ? 5011004 : 6975545);
    }

    @Override
    public void addDefaultFlowers() {
        this.addFlower(Blocks.RED_FLOWER.getDefaultState().withProperty(Blocks.RED_FLOWER.getTypeProperty(), BlockFlower.EnumFlowerType.BLUE_ORCHID), 10);
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public int getFoliageColorAtPos(BlockPos pos) {
        double d0 = 0.9f;
        double d1 = 0.9f;
        return this.getModdedBiomeFoliageColor(9087);
    }

    public void addMushrooms(World p_185379_1_, Random p_185379_2_, BlockPos p_185379_3_) {
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
                int k = i * 4 + 1 + 8 + p_185379_2_.nextInt(3);
                int l = j * 4 + 1 + 8 + p_185379_2_.nextInt(3);
                BlockPos blockpos = p_185379_1_.getHeight(p_185379_3_.add(k, 0, l));
                if (p_185379_2_.nextInt(20) == 0 && TerrainGen.decorate(p_185379_1_, p_185379_2_, new ChunkPos(p_185379_3_), blockpos, DecorateBiomeEvent.Decorate.EventType.BIG_SHROOM)) {
                    WorldGenBigMushroom worldgenbigmushroom = new WorldGenBigMushroom();
                    worldgenbigmushroom.generate(p_185379_1_, p_185379_2_, blockpos);
                    continue;
                }
                if (!TerrainGen.decorate(p_185379_1_, p_185379_2_, new ChunkPos(p_185379_3_), blockpos, DecorateBiomeEvent.Decorate.EventType.TREE)) continue;
                WorldGenAbstractTree worldgenabstracttree = this.getRandomTreeFeature(p_185379_2_);
                worldgenabstracttree.setDecorationDefaults();
                if (!worldgenabstracttree.generate(p_185379_1_, p_185379_2_, blockpos)) continue;
                worldgenabstracttree.generateSaplings(p_185379_1_, p_185379_2_, blockpos);
            }
        }
    }
}

