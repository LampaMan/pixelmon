/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.ultraspace.biome;

import com.pixelmongenerations.common.world.ultraspace.feature.WorldGenDesertSpike;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import java.util.Random;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;

public class UltraDesertBiome
extends Biome {
    private final WorldGenDesertSpike iceSpike = new WorldGenDesertSpike();
    Random random = new Random();

    public UltraDesertBiome(Biome.BiomeProperties properties) {
        super(properties);
        this.spawnableCreatureList.clear();
        this.topBlock = PixelmonBlocks.ultraSand.getDefaultState();
        this.fillerBlock = PixelmonBlocks.ultraSandstone.getDefaultState();
        this.decorator.treesPerChunk = 10;
        this.decorator.deadBushPerChunk = 2;
        this.decorator.reedsPerChunk = -999;
        this.decorator.cactiPerChunk = -999;
        this.spawnableCreatureList.clear();
    }

    @Override
    public void decorate(World worldIn, Random rand, BlockPos pos) {
        if (this.random.nextInt(100) <= 10) {
            for (int i = 0; i < 3; ++i) {
                int j = rand.nextInt(16) + 8;
                int k = rand.nextInt(16) + 8;
                this.iceSpike.generate(worldIn, rand, worldIn.getHeight(pos.add(j, 0, k)));
            }
        }
        super.decorate(worldIn, rand, pos);
    }
}

