/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.ultraspace;

import com.pixelmongenerations.common.world.ModWorldGen;
import com.pixelmongenerations.common.world.ultraspace.IUltraWorld;
import com.pixelmongenerations.common.world.ultraspace.UltraSpaceBiomeProvider;
import com.pixelmongenerations.common.world.ultraspace.UltraSpaceChunkGenerator;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.DimensionType;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.biome.BiomeProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class UltraSpaceWorldProvider
extends WorldProvider
implements IUltraWorld {
    @Override
    public void init() {
        this.biomeProvider = new UltraSpaceBiomeProvider(this.world.getWorldInfo());
        this.hasSkyLight = true;
        this.setWorldTime(13000L);
    }

    @Override
    public DimensionType getDimensionType() {
        return ModWorldGen.ULTRA_SPACE_DIM_TYPE;
    }

    @Override
    public String getSaveFolder() {
        return "ultraspace";
    }

    @Override
    public BiomeProvider getBiomeProvider() {
        return this.biomeProvider;
    }

    @Override
    public IChunkGenerator createChunkGenerator() {
        return new UltraSpaceChunkGenerator(this.world, this.world.getWorldInfo().getGeneratorOptions());
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public Vec3d getSkyColor(Entity cameraEntity, float partialTicks) {
        Vec3d color = new Vec3d(0.6, 0.0, 0.8);
        return color;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public Vec3d getFogColor(float p_76562_1_, float p_76562_2_) {
        return new Vec3d(0.8, 0.0, 0.8);
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public double getVoidFogYFactor() {
        return 0.0;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public boolean doesXZShowFog(int x, int z) {
        return true;
    }

    @Override
    public boolean canRespawnHere() {
        return false;
    }

    @Override
    public float getGravity() {
        return 0.062f;
    }
}

