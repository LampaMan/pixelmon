/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world;

import com.pixelmongenerations.common.world.ultraspace.UltraSpaceWorldProvider;
import com.pixelmongenerations.common.world.ultraspace.UltraSpaceWorldType;
import net.minecraft.world.DimensionType;
import net.minecraft.world.WorldType;
import net.minecraftforge.common.DimensionManager;

public class ModWorldGen {
    public static final int ULTRA_SPACE_DIM_ID = 24;
    public static String ULTRA_SPACE_NAME = "ultraspace";
    public static final WorldType ULTRA_SPACE_WORLD_TYPE = new UltraSpaceWorldType();
    public static final DimensionType ULTRA_SPACE_DIM_TYPE = DimensionType.register(ULTRA_SPACE_NAME, "_" + ULTRA_SPACE_NAME, 24, UltraSpaceWorldProvider.class, true);

    public static void registerDimension() {
        DimensionManager.registerDimension(24, ULTRA_SPACE_DIM_TYPE);
    }
}

