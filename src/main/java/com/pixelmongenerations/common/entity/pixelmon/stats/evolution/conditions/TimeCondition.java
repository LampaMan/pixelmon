/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.Evolution;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;
import java.util.ArrayList;

public class TimeCondition
extends EvoCondition {
    public EnumTime time;

    public TimeCondition() {
    }

    public TimeCondition(boolean day) {
        this.time = day ? EnumTime.Day : EnumTime.Night;
    }

    public TimeCondition(EnumTime time) {
        this.time = time;
    }

    @Override
    public boolean passes(EntityPixelmon pokemon) {
        long worldTime = pokemon.getEntityWorld().getWorldTime();
        ArrayList<Evolution> evos = pokemon.getEvolutions(Evolution.class);
        boolean hasDuskEvo = false;
        boolean hasDawnEvo = false;
        for (Evolution evo : evos) {
            for (EvoCondition condition : evo.conditions) {
                if (!(condition instanceof TimeCondition)) continue;
                TimeCondition timeCondition = (TimeCondition)condition;
                if (timeCondition.time == EnumTime.Dawn) {
                    hasDawnEvo = true;
                    continue;
                }
                if (timeCondition.time != EnumTime.Dusk) continue;
                hasDuskEvo = true;
            }
        }
        switch (this.time) {
            case Dawn: {
                return this.isDawn(worldTime);
            }
            case Dusk: {
                return this.isDusk(worldTime);
            }
            case Day: {
                if (hasDuskEvo) {
                    return pokemon.getEntityWorld().isDaytime() && !this.isDusk(worldTime);
                }
                return pokemon.getEntityWorld().isDaytime();
            }
            case Night: {
                if (hasDawnEvo) {
                    return !pokemon.getEntityWorld().isDaytime() && !this.isDawn(worldTime);
                }
                return !pokemon.getEntityWorld().isDaytime();
            }
        }
        return false;
    }

    private boolean isDusk(long worldTime) {
        return worldTime >= 11000L && worldTime < 13000L;
    }

    private boolean isDawn(long worldTime) {
        return worldTime >= 23000L || worldTime < 1000L;
    }

    public static enum EnumTime {
        Day,
        Dawn,
        Dusk,
        Night;


        public static EnumTime getTime(String split) {
            for (EnumTime enumTime : EnumTime.values()) {
                if (!enumTime.name().equalsIgnoreCase(split)) continue;
                return enumTime;
            }
            return Day;
        }
    }
}

