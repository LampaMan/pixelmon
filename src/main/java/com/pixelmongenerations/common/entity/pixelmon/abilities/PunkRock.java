/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class PunkRock
extends AbilityBase {
    @Override
    public int modifyDamageUser(int damage, PixelmonWrapper user, PixelmonWrapper pokemon, Attack a) {
        if (PunkRock.isSoundMove(a)) {
            damage += (int)((double)damage * 0.3);
        }
        return damage;
    }

    @Override
    public int modifyDamageTarget(int damage, PixelmonWrapper user, PixelmonWrapper pokemon, Attack a) {
        if (PunkRock.isSoundMove(a)) {
            damage /= 2;
        }
        return damage;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public static boolean isSoundMove(Attack a) {
        if (a == null) return false;
        if (!a.isAttack("Boomburst", "Bug Buzz", "Chatter", "Clanging Scales", "Clangorous Soulblaze", "Confide", "Disarming Voice", "Echoed Voice", "Grass Whistle", "Growl", "Heal Bell", "Hyper Voice", "Metal Sound", "Noble Roar", "Parting Shot", "Perish Song", "Relic Song", "Roar", "Round", "Screech", "Sing", "Snarl", "Snore", "Sparkling Aria", "Supersonic", "Uproar", "Overdrive")) return false;
        return true;
    }
}

