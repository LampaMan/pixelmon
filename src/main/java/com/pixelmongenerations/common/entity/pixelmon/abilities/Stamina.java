/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class Stamina
extends AbilityBase {
    @Override
    public void tookDamageTargetAfterMove(PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (target.getBattleStats().statCanBeRaised(StatsType.Defence)) {
            this.sendActivatedMessage(target);
            target.getBattleStats().modifyStat(1, StatsType.Defence);
        }
    }
}

