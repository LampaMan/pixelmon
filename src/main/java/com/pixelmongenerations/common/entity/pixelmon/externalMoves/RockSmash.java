/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.externalMoves;

import com.pixelmongenerations.api.spawning.archetypes.spawners.TriggerSpawner;
import com.pixelmongenerations.common.block.BlockFossil;
import com.pixelmongenerations.common.block.MultiBlock;
import com.pixelmongenerations.common.block.spawning.BlockSpawningHandler;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.ExternalMoveBase;
import com.pixelmongenerations.core.enums.battle.EnumBattleStartTypes;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.util.PixelBlockSnapshot;
import javax.swing.Timer;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.GameType;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.BlockEvent;

public class RockSmash
extends ExternalMoveBase {
    public static TriggerSpawner ROCK_SMASH_SPAWNER = null;

    public RockSmash() {
        super("rocksmash", "Rock Smash");
    }

    @Override
    public boolean execute(EntityPixelmon user, RayTraceResult target, int moveIndex) {
        if (target.typeOfHit == RayTraceResult.Type.BLOCK) {
            World world = user.world;
            BlockPos pos = target.getBlockPos();
            IBlockState state = world.getBlockState(pos);
            if (state.getMaterial() != Material.ROCK || state.getBlock() == Blocks.BEDROCK) {
                return false;
            }
            if (state.getBlock() instanceof MultiBlock || state.getBlock() instanceof BlockFossil) {
                return false;
            }
            if (((EntityPlayerMP)user.getOwner()).interactionManager.getGameType() == GameType.ADVENTURE) {
                PixelBlockSnapshot snapshot = new PixelBlockSnapshot(world, pos, state);
                world.setBlockToAir(pos);
                Timer timer = new Timer(7000, e -> snapshot.restoreToLocation(world, pos, true, true));
                timer.setRepeats(false);
                timer.start();
            } else {
                if (MinecraftForge.EVENT_BUS.post(new BlockEvent.BreakEvent(world, pos, state, (EntityPlayerMP)user.getOwner()))) {
                    return false;
                }
                if (((EntityPlayerMP)user.getOwner()).interactionManager.tryHarvestBlock(pos)) {
                    state.getBlock().dropBlockAsItem(world, pos, state, 3);
                }
            }
            BlockSpawningHandler.getInstance().performBattleStartCheck(world, pos, user.getOwner(), user, "", EnumBattleStartTypes.ROCKSMASH);
            return true;
        }
        return false;
    }

    @Override
    public int getCooldown(EntityPixelmon user) {
        return 800 - user.stats.Speed;
    }

    @Override
    public int getCooldown(PixelmonData user) {
        return 800 - user.Speed;
    }

    @Override
    public boolean isDestructive() {
        return true;
    }
}

