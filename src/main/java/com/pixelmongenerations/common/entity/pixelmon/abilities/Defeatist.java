/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class Defeatist
extends AbilityBase {
    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        if (user.getHealthPercent() <= 50.0f) {
            int[] arrn = stats;
            int n = StatsType.Attack.getStatIndex();
            arrn[n] = arrn[n] / 2;
            int[] arrn2 = stats;
            int n2 = StatsType.SpecialAttack.getStatIndex();
            arrn2[n2] = arrn2[n2] / 2;
        }
        return stats;
    }
}

