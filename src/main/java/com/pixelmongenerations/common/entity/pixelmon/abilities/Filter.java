/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class Filter
extends AbilityBase {
    @Override
    public int modifyDamageTarget(int damage, PixelmonWrapper user, PixelmonWrapper pokemon, Attack a) {
        double effectiveness = a.getTypeEffectiveness(user, pokemon);
        if (effectiveness == 2.0 || effectiveness == 4.0) {
            damage = (int)((double)damage * 0.75);
        }
        return damage;
    }
}

