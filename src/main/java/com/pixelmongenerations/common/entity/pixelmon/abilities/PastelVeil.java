/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.PreventStatDrop;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class PastelVeil
extends PreventStatDrop {
    public PastelVeil() {
        super("pixelmon.abilities.pastelveil");
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.bc.rules.battleType.numPokemon != 1) {
            for (PixelmonWrapper ally : newPokemon.getTeamPokemonExcludeSelf()) {
                if (!ally.hasStatus(StatusType.Poison, StatusType.PoisonBadly)) continue;
                ally.removeStatus(StatusType.Poison);
                ally.removeStatus(StatusType.PoisonBadly);
                ally.bc.sendToAll("pixelmon.abilities.pastelveil", newPokemon.getNickname());
                break;
            }
        }
    }

    @Override
    public boolean allowsStatusTeammate(StatusType status, PixelmonWrapper pokemon, PixelmonWrapper target, PixelmonWrapper user) {
        if (status == StatusType.Poison || status == StatusType.PoisonBadly) {
            if (user != target && user.attack != null && user.attack.getAttackCategory() == AttackCategory.Status) {
                user.bc.sendToAll("pixelmon.abilities.pastelveil", pokemon.getNickname());
            }
            return false;
        }
        return true;
    }
}

