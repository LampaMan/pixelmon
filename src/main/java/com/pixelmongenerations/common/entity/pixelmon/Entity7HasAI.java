/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon;

import com.pixelmongenerations.common.block.IPokemonOwner;
import com.pixelmongenerations.common.entity.pixelmon.Entity6CanBattle;
import com.pixelmongenerations.common.entity.pixelmon.EnumAggression;
import com.pixelmongenerations.common.entity.pixelmon.helpers.AIHelper;
import com.pixelmongenerations.common.entity.pixelmon.stats.Aggression;
import com.pixelmongenerations.core.database.SpawnLocation;
import java.util.HashMap;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public abstract class Entity7HasAI
extends Entity6CanBattle {
    private int lastOwnerX;
    private int lastOwnerY;
    private int lastOwnerZ;
    private BlockPos idleSpot;
    public IPokemonOwner blockOwner = null;
    public EnumAggression aggression;
    public int aggressionTimer = 0;
    protected AIHelper aiHelper;
    public int moveIndex = -1;
    public int targetX;
    public int targetY;
    public int targetZ;
    public EnumFacing targetSide;

    public Entity7HasAI(World par1World) {
        super(par1World);
    }

    @Override
    protected void init(String name) {
        super.init(name);
        this.lastOwnerZ = 0;
        this.lastOwnerY = 0;
        this.lastOwnerX = 0;
        this.idleSpot = null;
        this.giveAggression();
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        super.readEntityFromNBT(nbt);
        this.aggression = EnumAggression.getAggression(nbt.getInteger("Aggression"));
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound nbt) {
        super.writeEntityToNBT(nbt);
        if (this.aggression == null) {
            this.aggression = EnumAggression.passive;
        }
        nbt.setInteger("Aggression", this.aggression.index);
    }

    @Override
    public void getNBTTags(HashMap<String, Class> tags) {
        super.getNBTTags(tags);
        tags.put("Aggression", Integer.class);
    }

    public EnumAggression getAggression() {
        return this.aggression == null ? EnumAggression.passive : this.aggression;
    }

    public void setAggression(EnumAggression aggression) {
        if (aggression == null) {
            aggression = EnumAggression.passive;
        }
        this.aggression = aggression;
    }

    public void giveAggression() {
        if (this.getOwner() != null) {
            this.aggression = EnumAggression.passive;
            return;
        }
        int r = this.rand.nextInt(100) + 1;
        Aggression a = this.baseStats.aggression;
        if (this.aggression == null) {
            if (a == null) {
                this.aggression = EnumAggression.passive;
                return;
            }
            this.aggression = r < a.timid ? EnumAggression.timid : (r < a.timid + a.passive ? EnumAggression.passive : (r < a.timid + a.passive + a.aggressive ? EnumAggression.aggressive : EnumAggression.passive));
        }
    }

    public AIHelper getAIHelper() {
        if (this.aiHelper == null) {
            this.aiHelper = new AIHelper(this, this.tasks);
        }
        return this.aiHelper;
    }

    @Override
    public void onEntityUpdate() {
        super.onEntityUpdate();
        if (this.aiHelper == null && this.baseStats != null) {
            this.resetAI();
        }
        if (this.aggressionTimer > 0 && this.battleController == null) {
            --this.aggressionTimer;
        }
    }

    public void resetAI() {
        this.aiHelper = new AIHelper(this, this.tasks);
    }

    @Override
    public void travel(float strafe, float up, float forward) {
        SpawnLocation location = this.getSpawnLocation();
        if (location == SpawnLocation.AirPersistent) {
            this.motionY = 0.0;
        }
        super.travel(strafe, up, forward);
        if (location == SpawnLocation.AirPersistent) {
            this.motionY = 0.0;
        }
        if (location == SpawnLocation.Air && this.isFlying) {
            this.motionY -= 0.01;
        }
    }

    public void setIdleSpot(BlockPos coords) {
        this.idleSpot = coords;
    }

    public BlockPos getIdleSpot() {
        return this.idleSpot;
    }

    public void updateOwnerCoords() {
        this.lastOwnerX = (int)(this.getOwner().posX + 0.5);
        this.lastOwnerY = (int)(this.getOwner().posY + 0.5);
        this.lastOwnerZ = (int)(this.getOwner().posZ + 0.5);
    }

    public int getLastOwnerX() {
        return this.lastOwnerX;
    }

    public int getLastOwnerY() {
        return this.lastOwnerY;
    }

    public int getLastOwnerZ() {
        return this.lastOwnerZ;
    }

    @Override
    public void setAttackTarget(EntityLivingBase entity) {
        this.moveIndex = -1;
        super.setAttackTarget(entity);
    }

    public void setAttackTarget(EntityLivingBase entity, int moveIndex) {
        this.setAttackTarget(entity);
        this.moveIndex = moveIndex;
    }

    public void setBlockTarget(int x, int y, int z, EnumFacing side, int moveIndex) {
        this.targetX = x;
        this.targetY = y;
        this.targetZ = z;
        this.targetSide = side;
        this.moveIndex = moveIndex;
    }
}

