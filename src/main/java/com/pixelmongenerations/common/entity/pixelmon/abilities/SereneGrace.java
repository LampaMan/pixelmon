/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class SereneGrace
extends AbilityBase {
    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (!user.hasStatus(StatusType.WaterPledge)) {
            a.getAttackBase().effects.stream().filter(EffectBase::isChance).forEach(effect -> effect.changeChance(2));
        }
        return new int[]{power, accuracy};
    }
}

