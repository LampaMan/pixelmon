/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class Prankster
extends AbilityBase {
    @Override
    public float modifyPriority(PixelmonWrapper pokemon, float priority) {
        if (pokemon.attack.getAttackCategory() == AttackCategory.Status) {
            priority += 1.0f;
        }
        return priority;
    }

    @Override
    public boolean allowsAttack(PixelmonWrapper user, PixelmonWrapper target, Attack attack) {
        if (target.type.contains((Object)EnumType.Dark) && attack.getAttackBase().getAttackCategory() == AttackCategory.Status) {
            if (attack.isAttack("Aromatherapy", "Captivate", "Cotton Spore", "Growl", "Heal Bell", "Heal Block", "Imprison", "Leer", "Light Screen", "Quick Guard", "Reflect", "Safe Guard", "Sweet Scent", "Tail Wind")) {
                return false;
            }
            return attack.isAttack("Imprison", "Spikes", "Stealth Rock", "Sticky Web", "Toxic Spikes", "Sharp Steel", "Court Change", "Electric Terrain", "Fairy Lock", "Flower Shield", "Grassy Terrain", "Gravity", "Hail", "Haze", "Ion Deluge", "Magic Room", "Misty Terrain", "Mud Sport", "Psychic Terrain", "Rain Dance", "Sandstorm", "Shadow Half", "Shadow Shed", "Shadow Sky", "Sunny Day", "Teatime", "Trick Room", "Water Sport", "Wonder Room");
        }
        return true;
    }

    @Override
    public void allowsIncomingAttackMessage(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        user.bc.sendToAll("pixelmon.abilities.pranksterdark", user.getNickname());
    }

    @Override
    public boolean needNewInstance() {
        return true;
    }
}

