/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.ZAttackBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class Normalize
extends AbilityBase {
    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        block3: {
            block2: {
                if (!(a.getAttackBase() instanceof ZAttackBase) || a.getAttackCategory() == AttackCategory.Status) break block2;
                if (a.isAttack("Hidden Power", "Judgment", "Natural Gift", "Techno Blast", "Weather Ball")) break block3;
            }
            a.overrideType(EnumType.Normal);
        }
        return new int[]{power, accuracy};
    }
}

