/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Sandstorm;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;

public class SandSpit
extends AbilityBase {
    @Override
    public void tookDamageTarget(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (target.hasHeldItem() && target.getHeldItem() == PixelmonItemsHeld.smoothRock) {
            Sandstorm sandstorm = new Sandstorm(8);
            sandstorm.setStartTurns(user);
            target.bc.globalStatusController.addGlobalStatus(sandstorm);
            target.bc.sendToAll("pixelmon.abilities.sandspit", target.getNickname());
        } else {
            Sandstorm sandstorm = new Sandstorm();
            sandstorm.setStartTurns(target);
            target.bc.globalStatusController.addGlobalStatus(sandstorm);
            target.bc.sendToAll("pixelmon.abilities.sandspit", target.getNickname());
        }
    }
}

