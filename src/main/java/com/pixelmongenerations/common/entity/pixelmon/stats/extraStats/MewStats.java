/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.extraStats;

import com.pixelmongenerations.common.entity.pixelmon.stats.ExtraStats;
import net.minecraft.nbt.NBTTagCompound;

public class MewStats
extends ExtraStats {
    public int numCloned = 0;
    public static final int MAX_CLONES = 3;

    @Override
    public void writeToNBT(NBTTagCompound nbt) {
        nbt.setByte("NumCloned", (byte)this.numCloned);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        this.numCloned = nbt.getInteger("NumCloned");
    }
}

