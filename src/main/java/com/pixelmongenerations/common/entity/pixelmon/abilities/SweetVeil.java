/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class SweetVeil
extends AbilityBase {
    @Override
    public boolean allowsStatusTeammate(StatusType status, PixelmonWrapper pokemon, PixelmonWrapper target, PixelmonWrapper user) {
        if (status == StatusType.Sleep || status == StatusType.Yawn) {
            if (user != target && user.attack != null && user.attack.getAttackCategory() == AttackCategory.Status) {
                user.bc.sendToAll("pixelmon.abilities.sweetveil", pokemon.getNickname(), target.getNickname());
            }
            return false;
        }
        return true;
    }
}

