/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Sandstorm;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;

public class SandForce
extends AbilityBase {
    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (user.bc != null && user.bc.globalStatusController.getWeather() instanceof Sandstorm && (a.getAttackBase().attackType.equals((Object)EnumType.Rock) || a.getAttackBase().attackType.equals((Object)EnumType.Ground) || a.getAttackBase().attackType.equals((Object)EnumType.Steel))) {
            power = (int)((double)power * 1.3);
        }
        return new int[]{power, accuracy};
    }
}

