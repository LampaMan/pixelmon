/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Sunny;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;

public class Chlorophyll
extends AbilityBase {
    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        if (user.bc.globalStatusController.getWeather() instanceof Sunny && user.getHeldItem() != PixelmonItemsHeld.utilityUmbrella) {
            int[] arrn = stats;
            int n = StatsType.Speed.getStatIndex();
            arrn[n] = arrn[n] * 2;
        }
        return stats;
    }
}

