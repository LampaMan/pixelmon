/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;

public class HighAltitudeCondition
extends EvoCondition {
    public float minAltitude = 127.0f;

    public HighAltitudeCondition() {
    }

    public HighAltitudeCondition(float minAltitude) {
        this.minAltitude = minAltitude;
    }

    @Override
    public boolean passes(EntityPixelmon pokemon) {
        return pokemon.getPositionVector().y >= (double)this.minAltitude;
    }
}

