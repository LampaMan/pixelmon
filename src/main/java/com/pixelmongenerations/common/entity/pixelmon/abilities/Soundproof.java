/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class Soundproof
extends AbilityBase {
    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        if (Soundproof.isSoundMove(a)) {
            pokemon.bc.sendToAll("pixelmon.abilities.soundproof", pokemon.getNickname());
            return false;
        }
        return true;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public static boolean isSoundMove(Attack a) {
        if (a == null) return false;
        if (!a.isAttack("Boomburst", "Bug Buzz", "Chatter", "Clanging Scales", "Clangorous Soulblaze", "Confide", "Disarming Voice", "Echoed Voice", "Grass Whistle", "Growl", "Heal Bell", "Hyper Voice", "Metal Sound", "Noble Roar", "Parting Shot", "Perish Song", "Relic Song", "Roar", "Round", "Screech", "Sing", "Snarl", "Snore", "Sparkling Aria", "Supersonic", "Uproar", "Overdrive", "Eerie Spell")) return false;
        return true;
    }
}

