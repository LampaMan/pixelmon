/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.textures;

import com.pixelmongenerations.common.entity.pixelmon.textures.IEnumSpecialTexture;

public enum EnumCryogonalTextures implements IEnumSpecialTexture
{
    Radioactive(2, "Radioactive", true);

    private int id;
    private String name;
    private boolean glow;

    private EnumCryogonalTextures(int id, String name, boolean glow) {
        this.id = id;
        this.name = name;
        this.glow = glow;
    }

    @Override
    public boolean hasTexutre() {
        return true;
    }

    @Override
    public String getTexture() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public String getProperName() {
        return this.name;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public boolean hasGlow() {
        return this.glow;
    }
}

