/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class FakemonBirdBrain
extends AbilityBase {
    @Override
    public void onAttackUsed(PixelmonWrapper pixelmonWrapper, Attack attack) {
        Attack randomAttack;
        do {
            randomAttack = new Attack(RandomHelper.getRandomNumberBetween(1, 767));
        } while (randomAttack.isAttack("After You", "Assist", "Bestow", "Chatter", "Copycat", "Counter", "Covet", "Destiny Bond", "Detect", "Endure", "Feint", "Focus Punch", "Follow Me", "Freeze Shock", "Helping Hand", "Ice Burn", "Me First", "Metronome", "Mimic", "Mirror Coat", "Mirror Move", "Nature Power", "Protect", "Quash", "Quick Guard", "Rage Powder", "Relic Song", "Secret Sword", "Sketch", "Sleep Talk", "Snarl", "Snatch", "Snore", "Struggle", "Switcheroo", "Techno Blast", "Thief", "Transform", "Trick", "V-create", "Wide Guard", "Max Airstream", "Max Darkness", "Max Flare", "Max Flutterby", "Max Geyser", "Max Guard", "Max Hailstorm", "Max Knuckle", "Max Lightning", "Max Mindstorm", "Max Ooze", "Max Overgroth", "Max Phantasm", "Max Quake", "Max Rockfall", "Max Starfall", "Max Steelspike", "Max Wyrmwind"));
        pixelmonWrapper.getMoveset().replaceMove(pixelmonWrapper.attack.toString(), randomAttack);
        pixelmonWrapper.update(EnumUpdateType.Moveset);
        pixelmonWrapper.bc.sendToAll("pixelmon.abilities.birdbrain", pixelmonWrapper.getNickname(), randomAttack.getAttackBase().getLocalizedName());
    }
}

