/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class NeutralizingGas
extends AbilityBase {
    @Override
    public void applyRepeatedEffect(PixelmonWrapper newPokemon) {
        newPokemon.bc.sendToAll("pixelmon.abilities.neutralizinggas", newPokemon.getNickname());
    }
}

