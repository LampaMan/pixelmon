/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class Sturdy
extends AbilityBase {
    @Override
    public int modifyDamageIncludeFixed(int damage, PixelmonWrapper user, PixelmonWrapper pokemon, Attack a) {
        if (pokemon != null && user != null && user != pokemon && damage >= pokemon.getHealth() && pokemon.getHealth() == pokemon.getMaxHealth()) {
            if (user.bc != null) {
                user.bc.sendToAll("pixelmon.abilities.sturdy", pokemon.getNickname());
            }
            return pokemon.getHealth() - 1;
        }
        return damage;
    }

    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        return !a.isAttack("Fissure", "Guillotine", "Horn Drill", "Sheer Cold");
    }

    @Override
    public void allowsIncomingAttackMessage(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        if (user.bc != null) {
            user.bc.sendToAll("pixelmon.abilities.sturdy2", pokemon.getNickname());
        }
    }
}

