/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class Synchronize
extends AbilityBase {
    @Override
    public void onStatusAdded(StatusBase status, PixelmonWrapper user, PixelmonWrapper opponent) {
        if (user != opponent && StatusType.isPrimaryStatus(status.type) && status.type != StatusType.Freeze && status.type != StatusType.Sleep) {
            if (!status.isImmune(opponent) && opponent.addStatus(status, opponent)) {
                opponent.bc.sendToAll("pixelmon.entities.syncpassed", opponent.getNickname());
            } else {
                opponent.bc.sendToAll("pixelmon.battletext.noeffect", opponent.getNickname());
            }
        }
    }
}

