/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class ShieldDust
extends AbilityBase {
    @Override
    public int[] modifyPowerAndAccuracyTarget(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        a.getAttackBase().effects.stream().filter(effect -> effect.isChance() && !ShieldDust.ignoreAbility(user, target)).forEach(effect -> effect.changeChance(0));
        return new int[]{power, accuracy};
    }
}

