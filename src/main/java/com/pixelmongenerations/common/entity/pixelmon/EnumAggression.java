/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon;

public enum EnumAggression {
    timid(0),
    passive(1),
    aggressive(2);

    public int index;

    private EnumAggression(int i) {
        this.index = i;
    }

    public static EnumAggression getAggression(int index) {
        for (EnumAggression a : EnumAggression.values()) {
            if (a.index != index) continue;
            return a;
        }
        return passive;
    }
}

