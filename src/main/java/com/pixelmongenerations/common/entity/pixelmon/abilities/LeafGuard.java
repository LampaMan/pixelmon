/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Sunny;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class LeafGuard
extends AbilityBase {
    @Override
    public boolean allowsStatus(StatusType status, PixelmonWrapper pokemon, PixelmonWrapper user) {
        if (user.bc != null && user.bc.globalStatusController.getWeather() instanceof Sunny && pokemon.getHeldItem() != PixelmonItemsHeld.utilityUmbrella && (StatusType.isPrimaryStatus(status) || status.equals((Object)StatusType.Yawn))) {
            if (user != pokemon && user.attack != null && user.attack.getAttackCategory() == AttackCategory.Status) {
                user.bc.sendToAll("pixelmon.abilities.leafguard", pokemon.getNickname());
            }
            return false;
        }
        return true;
    }
}

