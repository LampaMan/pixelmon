/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Rush;

public class SlushRush
extends Rush {
    public SlushRush() {
        super(StatusType.Hail);
    }
}

