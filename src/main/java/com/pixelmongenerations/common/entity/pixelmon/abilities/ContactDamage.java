/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.LongReach;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;
import com.pixelmongenerations.common.item.heldItems.ItemProtectivePads;

public abstract class ContactDamage
extends AbilityBase {
    private String langString;

    protected ContactDamage(String langString) {
        this.langString = langString;
    }

    @Override
    public void applyEffectOnContactTarget(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.isAlive() && !(user.getBattleAbility() instanceof MagicGuard)) {
            if (user.hasHeldItem() && user.getHeldItem() instanceof ItemProtectivePads) {
                user.bc.sendToAll("pixelmon.effect.protectivepads", user.getNickname());
                return;
            }
            if (user.getBattleAbility() instanceof LongReach) {
                return;
            }
            user.bc.sendToAll(this.langString, target.getNickname(), user.getNickname());
            user.doBattleDamage(target, user.getPercentMaxHealth(12.5f), DamageTypeEnum.ABILITY);
        }
    }
}

