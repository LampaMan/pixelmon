/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Weather;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class WeatherTrio
extends AbilityBase {
    public Weather weather;

    public WeatherTrio(Weather weather) {
        this.weather = weather;
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.bc == null || newPokemon.bc.globalStatusController == null || this.weather == null || this.weather.equals(newPokemon.bc.globalStatusController.getWeather())) {
            return;
        }
        newPokemon.bc.globalStatusController.addGlobalStatus(this.weather);
        this.sendActivatedMessage(newPokemon);
    }

    @Override
    public void applySwitchOutEffect(PixelmonWrapper oldPokemon) {
        if (this.weather.equals(oldPokemon.bc.globalStatusController.getWeather())) {
            oldPokemon.bc.globalStatusController.removeGlobalStatus(this.weather);
        }
    }
}

