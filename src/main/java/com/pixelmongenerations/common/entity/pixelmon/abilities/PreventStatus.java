/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class PreventStatus
extends AbilityBase {
    protected StatusType[] preventedStatuses;
    protected String immuneText;
    protected String cureText;

    public PreventStatus(String immuneText, String cureText, StatusType ... preventedStatuses) {
        this.preventedStatuses = preventedStatuses;
        this.immuneText = immuneText;
        this.cureText = cureText;
    }

    @Override
    public boolean allowsStatus(StatusType status, PixelmonWrapper pokemon, PixelmonWrapper user) {
        if (status.isStatus(this.preventedStatuses)) {
            if (user != pokemon && user.attack != null && user.attack.getAttackCategory() == AttackCategory.Status) {
                user.bc.sendToAll(this.immuneText, pokemon.getNickname());
            }
            return false;
        }
        return true;
    }

    @Override
    public void onStatusAdded(StatusBase status, PixelmonWrapper user, PixelmonWrapper opponent) {
        if (status.type.isStatus(this.preventedStatuses)) {
            user.removeStatus(status, false);
            user.bc.sendToAll(this.cureText, user.getNickname());
        }
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper pokemon) {
        for (int i = 0; i < pokemon.getStatusSize(); ++i) {
            StatusBase status = pokemon.getStatus(i);
            if (!status.type.isStatus(this.preventedStatuses)) continue;
            pokemon.removeStatus(i);
            pokemon.bc.sendToAll(this.cureText, pokemon.getNickname());
            return;
        }
    }
}

