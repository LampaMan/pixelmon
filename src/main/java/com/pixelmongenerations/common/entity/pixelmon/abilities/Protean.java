/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.HiddenPower;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.ArrayList;

public class Protean
extends AbilityBase {
    @Override
    public void startMove(PixelmonWrapper user) {
        ArrayList<EnumType> newType = new ArrayList<EnumType>(2);
        EnumType changeType = user.attack.getAttackBase().attackType;
        if (user.attack.isAttack("Hidden Power")) {
            changeType = HiddenPower.getHiddenPowerType(user.getStats().IVs);
        }
        if (changeType == EnumType.Mystery) {
            changeType = EnumType.Normal;
        }
        newType.add(changeType);
        newType.add(null);
        if (!newType.equals(user.type)) {
            user.bc.sendToAll("pixelmon.abilities.protean", user.getNickname(), changeType.getLocalizedName());
            user.setTempType(newType);
        }
    }
}

