/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.BattleBond;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Comatose;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Disguise;
import com.pixelmongenerations.common.entity.pixelmon.abilities.FakemonRevealed;
import com.pixelmongenerations.common.entity.pixelmon.abilities.FlowerGift;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Forecast;
import com.pixelmongenerations.common.entity.pixelmon.abilities.IceFace;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Illusion;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Imposter;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Multitype;
import com.pixelmongenerations.common.entity.pixelmon.abilities.PowerConstruct;
import com.pixelmongenerations.common.entity.pixelmon.abilities.PowerOfAlchemy;
import com.pixelmongenerations.common.entity.pixelmon.abilities.RKSSystem;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Receiver;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Schooling;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ShieldsDown;
import com.pixelmongenerations.common.entity.pixelmon.abilities.StanceChange;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Trace;
import com.pixelmongenerations.common.entity.pixelmon.abilities.WonderGuard;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ZenMode;

public class CopyOnFaint
extends AbilityBase {
    @Override
    public void onPokemonFaintOther(PixelmonWrapper user, PixelmonWrapper pokemon) {
        AbilityBase ab = pokemon.getBattleAbility();
        if (!(!pokemon.isSameTeam(user) || ab instanceof PowerOfAlchemy || ab instanceof Receiver || ab instanceof Trace || ab instanceof Forecast || ab instanceof FlowerGift || ab instanceof Multitype || ab instanceof Illusion || ab instanceof WonderGuard || ab instanceof ZenMode || ab instanceof Imposter || ab instanceof StanceChange || ab instanceof PowerConstruct || ab instanceof Schooling || ab instanceof Comatose || ab instanceof ShieldsDown || ab instanceof Disguise || ab instanceof RKSSystem || ab instanceof BattleBond || ab instanceof IceFace || ab instanceof FakemonRevealed)) {
            user.setTempAbility(pokemon.getBattleAbility());
        }
    }
}

