/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.helpers.animation;

public class IncrementingVariable {
    public float value = 0.0f;
    public float increment;
    public float limit;

    public IncrementingVariable(float increment, float limit) {
        this.increment = increment;
        this.limit = limit;
    }

    public void tick() {
        this.value += this.increment;
        if (this.value >= this.limit) {
            this.value = 0.0f;
        }
    }
}

