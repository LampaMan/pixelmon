/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.EnumType;

public class MotorDrive
extends AbilityBase {
    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        if (a.getAttackBase().attackType == EnumType.Electric) {
            pokemon.bc.sendToAll("pixelmon.abilities.motordrive", pokemon.getNickname());
            if (pokemon.getBattleStats().modifyStat(1, StatsType.Speed)) {
                a.moveResult.weightMod -= 25.0f;
            }
            return false;
        }
        return true;
    }
}

