/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class WonderGuard
extends AbilityBase {
    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        return a.getAttackCategory() == AttackCategory.Status || a.getAttackBase().attackType == EnumType.Mystery || a.getTypeEffectiveness(user, pokemon) >= 2.0;
    }

    @Override
    public void allowsIncomingAttackMessage(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        pokemon.bc.sendToAll("pixelmon.abilities.wonderguard", pokemon.getNickname());
    }
}

