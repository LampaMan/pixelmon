/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.Evolution;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;
import com.pixelmongenerations.core.enums.EnumSpecies;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class InteractEvolution
extends Evolution {
    public Item item = null;

    public InteractEvolution(EnumSpecies from, PokemonSpec to, Item item, EvoCondition ... conditions) {
        super(from, to, conditions);
        this.item = item;
    }

    public boolean canEvolve(EntityPixelmon pokemon, ItemStack stack) {
        return (this.item == null || stack.getItem() == this.item) && super.canEvolve(pokemon);
    }

    public boolean canEvolve(EntityPlayerMP player, EntityPixelmon pokemon, ItemStack stack) {
        return (this.item == null || stack.getItem() == this.item) && super.canEvolve(pokemon);
    }
}

