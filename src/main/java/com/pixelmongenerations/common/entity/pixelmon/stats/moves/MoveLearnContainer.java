/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.moves;

import java.util.Arrays;

public class MoveLearnContainer {
    private int[] pokemonId;
    private int attackIndex;

    public MoveLearnContainer(int[] pokemonId, int attackIndex) {
        this.pokemonId = pokemonId;
        this.attackIndex = attackIndex;
    }

    public boolean isMoveLearn(int[] pokemonId, int attackIndex) {
        return Arrays.equals(this.pokemonId, pokemonId) && this.attackIndex == attackIndex;
    }
}

