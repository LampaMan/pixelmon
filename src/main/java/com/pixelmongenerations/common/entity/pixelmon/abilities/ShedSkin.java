/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class ShedSkin
extends AbilityBase {
    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        if (RandomHelper.getRandomChance(30) && pokemon.hasPrimaryStatus()) {
            this.sendActivatedMessage(pokemon);
            pokemon.removePrimaryStatus();
        }
    }
}

