/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.links;

import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.NoStatus;
import com.pixelmongenerations.common.battle.status.StatusPersist;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.Entity4Textures;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ComingSoon;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.ExtraStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.FriendShip;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.entity.pixelmon.stats.Level;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.Stats;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pixelmon.textures.IEnumSpecialTexture;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.database.DatabaseAbilities;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.enums.EnumMark;
import com.pixelmongenerations.core.enums.EnumNature;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.PixelmonStatsData;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelmonMethods;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class NBTLink
extends PokemonLink {
    private NBTTagCompound nbt;
    private PlayerStorage storage = null;

    public NBTLink(NBTTagCompound nbt) {
        this.nbt = nbt;
    }

    public NBTLink(NBTTagCompound nbt, PlayerStorage storage) {
        this(nbt);
        this.storage = storage;
    }

    @Override
    public BaseStats getBaseStats() {
        return Entity3HasStats.getBaseStats(this.nbt.getString("Name"), this.nbt.getInteger("Variant")).get();
    }

    @Override
    public Stats getStats() {
        Stats stats = new Stats();
        stats.readFromNBT(this.nbt);
        return stats;
    }

    @Override
    public ItemHeld getHeldItem() {
        ItemStack stack = ItemHeld.readHeldItemFromNBT(this.nbt);
        return ItemHeld.getItemHeld(stack);
    }

    @Override
    public void setHeldItem(ItemStack item) {
        ItemHeld.writeHeldItemToNBT(this.nbt, item);
    }

    @Override
    public int getHealth() {
        return (int)this.nbt.getFloat("Health");
    }

    @Override
    public int getMaxHealth() {
        return this.getStats().HP;
    }

    @Override
    public void setHealth(int health) {
        this.nbt.setFloat("Health", health);
        this.nbt.setBoolean("IsFainted", health <= 0);
    }

    @Override
    public int getLevel() {
        return this.nbt.getInteger("Level");
    }

    @Override
    public void setLevel(int level) {
        this.nbt.setInteger("Level", level);
    }

    @Override
    public int getDynamaxLevel() {
        return this.nbt.getInteger("DynamaxLevel");
    }

    @Override
    public void setDynamaxLevel(int level) {
        this.nbt.setInteger("DynamaxLevel", level);
    }

    @Override
    public int getExp() {
        return this.nbt.getInteger("EXP");
    }

    @Override
    public void setExp(int experience) {
        this.nbt.setInteger("EXP", experience);
    }

    @Override
    public FriendShip getFriendship() {
        FriendShip friendship = new FriendShip(this);
        friendship.readFromNBT(this.nbt);
        return friendship;
    }

    @Override
    public boolean doesLevel() {
        return this.nbt.getBoolean("DoesLevel");
    }

    @Override
    public String getRealNickname() {
        String nickname = this.nbt.getString("Nickname");
        if (nickname != null && !nickname.isEmpty()) {
            return nickname;
        }
        return super.getRealNickname();
    }

    @Override
    public EntityPlayerMP getPlayerOwner() {
        if (this.storage == null) {
            return null;
        }
        return this.storage.getPlayer();
    }

    @Override
    public BattleControllerBase getBattleController() {
        try {
            EntityPlayerMP player = this.storage.getPlayer();
            if (player != null) {
                return BattleRegistry.getBattle(player);
            }
        }
        catch (Exception exception) {
            // empty catch block
        }
        return null;
    }

    @Override
    public Moveset getMoveset() {
        Moveset moveset = new Moveset(this);
        moveset.readFromNBT(this.nbt);
        return moveset;
    }

    @Override
    public int[] getPokemonID() {
        return PixelmonMethods.getID(this.nbt);
    }

    @Override
    public EntityPixelmon getEntity() {
        EntityPlayerMP owner = this.getPlayerOwner();
        if (owner == null) {
            return null;
        }
        return this.storage.getPokemon(this.getPokemonID(), owner.world);
    }

    @Override
    public void setScale(float scale) {
    }

    @Override
    public World getWorld() {
        return null;
    }

    @Override
    public Gender getGender() {
        return Gender.getGender(this.nbt.getShort("Gender"));
    }

    @Override
    public BlockPos getPos() {
        return new BlockPos(0, 0, 0);
    }

    @Override
    public Optional<PlayerStorage> getStorage() {
        return Optional.empty();
    }

    @Override
    public void update(EnumUpdateType ... updateTypes) {
        if (this.storage != null) {
            this.storage.updateClient(this.nbt, updateTypes);
        }
    }

    @Override
    public void updateStats() {
        if (this.storage != null) {
            this.storage.updateClient(this.nbt, EnumUpdateType.HP, EnumUpdateType.Stats);
        }
    }

    @Override
    public void updateLevelUp(PixelmonStatsData stats) {
        this.updateStats();
    }

    @Override
    public void sendMessage(String langKey, Object ... data) {
        EntityPlayerMP player;
        if (this.storage != null && (player = this.storage.getPlayer()) != null) {
            ChatHandler.sendChat(player, langKey, data);
        }
    }

    @Override
    public String getNickname() {
        String nickname = this.nbt.getString("Nickname");
        if (!PixelmonConfig.allowNicknames || nickname.isEmpty()) {
            return Entity1Base.getLocalizedName(this.nbt.getString("Name"));
        }
        return nickname;
    }

    @Override
    public String getOriginalTrainer() {
        return this.nbt.getString("originalTrainer");
    }

    @Override
    public String getOriginalTrainerUUID() {
        return this.nbt.getString("originalTrainerUUID");
    }

    @Override
    public String getRealTextureNoCheck(PixelmonWrapper pw) {
        EnumSpecies pokemon = pw.getSpecies();
        short specialTexture = this.nbt.getShort("specialTexture");
        IEnumSpecialTexture enumm = pokemon.getSpecialTexture(pokemon.getFormEnum(pw.getForm()), specialTexture);
        String special = enumm.hasTexutre() ? enumm.getTexture() : this.nbt.getString("CustomTexture");
        return Entity4Textures.getTextureFor(pokemon, pokemon.getFormEnum(pw.getForm()), this.getGender(), special, this.nbt.getBoolean("IsShiny")).toString();
    }

    @Override
    public boolean removeStatuses(StatusType ... statuses) {
        StatusPersist status = StatusPersist.readStatusFromNBT(this.nbt);
        if (status == NoStatus.noStatus) {
            return false;
        }
        for (StatusType type : statuses) {
            if (status.type != type) continue;
            this.nbt.removeTag("Status");
            return true;
        }
        return false;
    }

    @Override
    public EnumNature getNature() {
        return EnumNature.getNatureFromIndex(this.nbt.getShort("Nature"));
    }

    @Override
    public EnumNature getPseudoNature() {
        return EnumNature.getNatureFromIndex(this.nbt.getShort("PseudoNature"));
    }

    @Override
    public int getExpToNextLevel() {
        return this.nbt.getInteger("EXPToNextLevel");
    }

    @Override
    public StatusPersist getPrimaryStatus() {
        return StatusPersist.readStatusFromNBT(this.nbt);
    }

    @Override
    public AbilityBase getAbility() {
        String abilityName = this.nbt.getString("Ability");
        return AbilityBase.getAbility(abilityName).orElse(ComingSoon.noAbility);
    }

    @Override
    public List<EnumType> getType() {
        EnumType type1;
        ArrayList<EnumType> type = new ArrayList<EnumType>();
        if (this.nbt.hasKey("primaryType") && (type1 = EnumType.parseOrNull(this.nbt.getShort("primaryType"))) != null) {
            type.add(type1);
            if (this.nbt.hasKey("secondaryType")) {
                short secondType = this.nbt.getShort("secondaryType");
                EnumType type2 = secondType == -1 ? null : EnumType.parseOrNull(secondType);
                EnumType enumType = type2;
                if (type2 != null) {
                    type.add(type2);
                }
            }
            return type;
        }
        BaseStats baseStats = this.getBaseStats();
        type.add(baseStats.type1);
        if (baseStats.type2 != null) {
            type.add(baseStats.type2);
        }
        return type;
    }

    @Override
    public int getForm() {
        return this.nbt.getInteger("Variant");
    }

    @Override
    public boolean isShiny() {
        return this.nbt.getBoolean("IsShiny");
    }

    @Override
    public boolean isEgg() {
        return this.nbt.getBoolean("isEgg");
    }

    @Override
    public int getEggCycles() {
        return this.nbt.getInteger("eggCycles");
    }

    @Override
    public EnumGrowth getGrowth() {
        return EnumGrowth.getGrowthFromIndex(this.nbt.getShort("Growth"));
    }

    @Override
    public EnumPokeball getCaughtBall() {
        return EnumPokeball.getFromIndex(this.nbt.getInteger("CaughtBall"));
    }

    @Override
    public int getPartyPosition() {
        return this.nbt.getInteger("PixelmonOrder");
    }

    @Override
    public ItemStack getHeldItemStack() {
        return ItemHeld.readHeldItemFromNBT(this.nbt);
    }

    @Override
    public boolean hasOwner() {
        return true;
    }

    @Override
    public EnumBossMode getBossMode() {
        return EnumBossMode.getMode(this.nbt.getShort("BossMode"));
    }

    @Override
    public int getSpecialTexture() {
        return this.nbt.getShort("specialTexture");
    }

    @Override
    public boolean isInRanch() {
        return this.nbt.getBoolean("isInRanch");
    }

    @Override
    public int[] getEggMoves() {
        return this.nbt.getIntArray("EggMoves");
    }

    @Override
    public Level getLevelContainer() {
        return new Level(this);
    }

    @Override
    public NBTTagCompound getNBT() {
        return this.nbt;
    }

    @Override
    public boolean isTotem() {
        return this.nbt.getBoolean("Totem");
    }

    @Override
    public String getCustomTexture() {
        return this.nbt.getString("CustomTexture");
    }

    @Override
    public int getPokeRus() {
        return this.nbt.getInteger("PokeRus");
    }

    @Override
    public boolean hasGmaxFactor() {
        return this.nbt.getBoolean("gmaxfactor");
    }

    @Override
    public void setShiny(boolean shiny) {
        this.nbt.setBoolean("IsShiny", shiny);
    }

    @Override
    public EnumMark getMark() {
        return this.nbt.hasKey("Mark") ? EnumMark.values()[this.nbt.getInteger("Mark")] : EnumMark.None;
    }

    @Override
    public void setMark(EnumMark mark) {
        this.nbt.setInteger("Mark", mark.ordinal());
    }

    @Override
    public ExtraStats getExtraStats() {
        ExtraStats extraStats = ExtraStats.getExtraStats(this.nbt.getString("Name"));
        if (extraStats != null) {
            extraStats.readFromNBT(this.nbt);
        }
        return extraStats;
    }

    @Override
    public IEnumForm getFormEnum() {
        return Gender.mfModels.contains((Object)this.getSpecies()) ? this.getGender() : this.getSpecies().getFormEnum(this.getForm());
    }

    @Override
    public void setForm(int form) {
        this.nbt.setInteger("Variant", form);
    }

    @Override
    public void setForm(IEnumForm form) {
        this.nbt.setInteger("Variant", form.getForm());
    }

    @Override
    public void setNature(EnumNature nature) {
        this.nbt.setInteger("Nature", nature.index);
    }

    @Override
    public void setPseudoNature(EnumNature nature) {
        this.nbt.setInteger("PseudoNature", nature.index);
    }

    @Override
    public void setGender(Gender gender) {
        this.nbt.setShort("Gender", (short)gender.ordinal());
    }

    @Override
    public void setAbility(String ability) {
        this.nbt.setString("Ability", ability);
        this.nbt.setInteger("AbilitySlot", DatabaseAbilities.getSlotForAbility(this.getBaseStats().id, ability));
    }

    @Override
    public void setCustomTexture(String customTexture) {
        this.nbt.setString("CustomTexture", customTexture);
    }

    @Override
    public void setGrowth(EnumGrowth growth) {
        this.nbt.setShort("Growth", (short)growth.index);
    }

    @Override
    public void setCaughtBall(EnumPokeball ball) {
        this.nbt.setShort("CaughtBall", (short)ball.getIndex());
    }

    @Override
    public void setBossMode(EnumBossMode mode) {
        this.nbt.setShort("BossMode", (short)mode.index);
    }

    @Override
    public void setSpecialTexture(int special) {
        this.nbt.setShort("specialTexture", (short)special);
    }

    @Override
    public void setTotem(boolean totem) {
        this.nbt.setBoolean("Totem", totem);
    }

    @Override
    public void setParticleId(String particleId) {
        this.nbt.setString("ParticleId", particleId);
    }

    @Override
    public void setStats(Stats stats) {
        stats.writeToNBT(this.nbt);
    }
}

