/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;

public class GaleWings
extends AbilityBase {
    @Override
    public float modifyPriority(PixelmonWrapper pokemon, float priority) {
        if (pokemon.getHealth() == pokemon.getMaxHealth() && pokemon.attack.getAttackBase().attackType == EnumType.Flying) {
            priority += 1.0f;
        }
        return priority;
    }
}

