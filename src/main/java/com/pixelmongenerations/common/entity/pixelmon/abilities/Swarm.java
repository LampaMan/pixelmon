/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.entity.pixelmon.abilities.LowHPTypeBoost;
import com.pixelmongenerations.core.enums.EnumType;

public class Swarm
extends LowHPTypeBoost {
    public Swarm() {
        super(EnumType.Bug);
    }
}

