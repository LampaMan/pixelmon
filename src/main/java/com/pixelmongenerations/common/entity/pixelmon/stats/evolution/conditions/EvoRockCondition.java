/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions;

import com.pixelmongenerations.common.block.BlockEvolutionRock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityEvolutionRock;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;
import com.pixelmongenerations.core.enums.EnumEvolutionRock;

public class EvoRockCondition
extends EvoCondition {
    public EnumEvolutionRock evoRock;
    public int maxRangeSquared = 100;

    public EvoRockCondition() {
    }

    public EvoRockCondition(EnumEvolutionRock evoRock, int maxRangeSquared) {
        this.evoRock = evoRock;
        this.maxRangeSquared = maxRangeSquared;
    }

    @Override
    public boolean passes(EntityPixelmon pokemon) {
        return pokemon.getEntityWorld().loadedTileEntityList.stream().anyMatch(te -> te instanceof TileEntityEvolutionRock && te.getPos().distanceSq(pokemon.getPosition()) < (double)this.maxRangeSquared && ((BlockEvolutionRock)te.getBlockType()).rockType == this.evoRock);
    }
}

