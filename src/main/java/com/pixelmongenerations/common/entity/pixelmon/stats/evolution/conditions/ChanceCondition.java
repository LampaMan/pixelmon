/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class ChanceCondition
extends EvoCondition {
    public float chance = 50.0f;

    public ChanceCondition() {
    }

    public ChanceCondition(float chance) {
        this.chance = chance;
    }

    @Override
    public boolean passes(EntityPixelmon pokemon) {
        return RandomHelper.getRandomChance(this.chance);
    }
}

