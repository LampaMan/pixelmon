/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;

public class FlashFire
extends AbilityBase {
    boolean activated = false;

    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        if (a.getAttackBase().attackType == EnumType.Fire) {
            if (!this.activated) {
                pokemon.bc.sendToAll("pixelmon.abilities.flashfire", pokemon.getNickname());
                a.moveResult.weightMod -= 25.0f;
                this.activated = true;
            } else {
                pokemon.bc.sendToAll("pixelmon.abilities.flashfire2", pokemon.getNickname());
            }
            return false;
        }
        return true;
    }

    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (this.activated && a.getAttackBase().attackType == EnumType.Fire) {
            return new int[]{(int)((double)power * 1.5), accuracy};
        }
        return new int[]{power, accuracy};
    }
}

