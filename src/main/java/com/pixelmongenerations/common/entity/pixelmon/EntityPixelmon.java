/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon;

import com.pixelmongenerations.api.events.PixelmonUpdateEvent;
import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.block.spawning.TileEntityPixelmonSpawner;
import com.pixelmongenerations.common.entity.npcs.registry.DropItemRegistry;
import com.pixelmongenerations.common.entity.pixelmon.Entity10CanBreed;
import com.pixelmongenerations.common.entity.pixelmon.EnumAggression;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.common.entity.pixelmon.helpers.AIHelper;
import com.pixelmongenerations.common.entity.pixelmon.helpers.EvolutionQuery;
import com.pixelmongenerations.common.entity.pixelmon.particleEffects.ParticleEffects;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.Evolution;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types.InteractEvolution;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types.LevelingEvolution;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types.MilceryEvolution;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types.NatureEvolution;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types.TradeEvolution;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.helper.WorldHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.pathfinding.PathNavigateGround;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeEventFactory;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityPixelmon
extends Entity10CanBreed {
    public static final DataParameter<String> dwName = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.STRING);
    public static final DataParameter<String> dwNickname = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.STRING);
    public static final DataParameter<Integer> dwPokemonID1 = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Integer> dwPokemonID2 = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Integer> dwExp = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<String> dwTrainerName = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.STRING);
    public static final DataParameter<Integer> dwTextures = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<String> dwCustomTexture = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.STRING);
    public static final DataParameter<Integer> dwScale = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Integer> dwMaxHP = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Integer> dwBossMode = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Integer> dwNature = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Integer> dwPseudoNature = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Integer> dwGrowth = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Integer> dwNumInteractions = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Integer> dwForm = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Integer> dwGender = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Integer> dwLevel = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Integer> dwDynamaxLevel = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Integer> dwTransformation = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Integer> dwSpawnLocation = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Integer> dwNumBreedingLevels = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Boolean> dwShiny = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.BOOLEAN);
    public static final DataParameter<String> dwParticle = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.STRING);
    public static final DataParameter<String> dwColorTint = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.STRING);
    public static final DataParameter<Boolean> dwColorStrobe = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.BOOLEAN);
    public static final DataParameter<Boolean> dwParticleColorStrobe = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.BOOLEAN);
    public static final DataParameter<Integer> dwParticleTint = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Integer> dwParticleTintAlpha = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Boolean> dwTotem = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.BOOLEAN);
    public static final DataParameter<Integer> dwPokeRus = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Integer> dwPokeRusTimer = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Boolean> dwGmaxFactor = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.BOOLEAN);
    public static final DataParameter<Boolean> dwDynamaxed = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.BOOLEAN);
    public static final DataParameter<Integer> dwMark = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public boolean playerOwned = false;
    public List<NBTTagCompound> embeddedPokemon = new ArrayList<NBTTagCompound>();
    public int legendaryTicks = -1;
    public int despawnCounter = -1;
    public static int TICKSPERSECOND = 20;
    public static int intMinTicksToDespawn = 15 * TICKSPERSECOND;
    public static int intMaxTicksToDespawn = 180 * TICKSPERSECOND;
    public boolean getTimings = false;
    public long startTime = 0L;
    public static ArrayList<IInteraction> interactionList = new ArrayList();
    int despawnTick = 0;
    public boolean canMove = true;
    public boolean stopRender = false;
    public TileEntityPixelmonSpawner spawner = null;
    public ArrayList<UUID> cameraCapturedPlayers = new ArrayList();
    public static boolean freeze = false;

    public EntityPixelmon(World world) {
        super(world);
        if (world != null && world.isRemote) {
            EntityPixelmon.setRenderDistanceWeight(PixelmonConfig.renderDistanceWeight);
        }
        this.targetTasks.addTask(1, new EntityAIHurtByTarget((EntityCreature)this, false, new Class[0]));
        ((PathNavigateGround)this.getNavigator()).setCanSwim(false);
    }

    @Override
    public void init(String name) {
        super.init(name);
    }

    @Override
    public boolean isCreatureType(EnumCreatureType type, boolean forSpawnCount) {
        if (type == EnumCreatureType.WATER_CREATURE && this.getSpawnLocation() == SpawnLocation.Water) {
            return true;
        }
        return type == EnumCreatureType.CREATURE;
    }

    @Override
    public void onDeath(DamageSource cause) {
        if (!this.world.isRemote) {
            super.onDeath(cause);
            if (this.getOwner() != null) {
                if (this.battleController == null) {
                    this.isFainted = true;
                }
                this.setHealth(0.0f);
                this.catchInPokeball();
            } else {
                if (cause.getTrueSource() instanceof EntityPlayerMP && PixelmonConfig.canPokemonBeHit) {
                    ArrayList<ItemStack> items = DropItemRegistry.getDropsForPokemon(this);
                    for (ItemStack stack : items) {
                        DropItemHelper.dropItemOnGround(this.getPositionVector(), (EntityPlayerMP)cause.getTrueSource(), stack, false, false);
                    }
                }
                this.setDead();
            }
        }
    }

    @Override
    public boolean processInteract(EntityPlayer player, EnumHand hand) {
        ItemStack itemstack;
        if (player instanceof EntityPlayerMP && hand == EnumHand.MAIN_HAND && (itemstack = player.getHeldItem(hand)) != ItemStack.EMPTY) {
            for (IInteraction i : interactionList) {
                if (!i.processInteract(this, player, hand, itemstack)) continue;
                return true;
            }
        }
        return super.processInteract(player, hand);
    }

    public void catchInPokeball() {
        this.isInBall = true;
        if (this.getFormEnum().isTemporary()) {
            this.setForm(this.getFormEnum().getDefaultFromTemporary().getForm(), true);
        }
        if (this.getSpecies() == EnumSpecies.Rayquaza && this.getForm() > 0) {
            this.setForm(0);
        }
        this.unloadEntity();
    }

    public void releaseFromPokeball() {
        Optional<PlayerStorage> optstorage;
        if (this.hasOwner()) {
            this.aggression = EnumAggression.passive;
        }
        this.isDead = false;
        try {
            this.world.spawnEntity(this);
        }
        catch (IllegalStateException illegalStateException) {
            // empty catch block
        }
        this.isInBall = false;
        if (!this.world.isRemote && this.hasOwner() && (optstorage = this.getStorage()).isPresent()) {
            optstorage.get().setInWorld(this, true);
        }
    }

    public void clearAttackTarget() {
        this.setRevengeTarget(null);
        this.setAttackTarget(null);
    }

    @Override
    public boolean getCanSpawnHere() {
        AxisAlignedBB aabb = this.getCollisionBoundingBox();
        BlockPos.MutableBlockPos pos = new BlockPos.MutableBlockPos();
        int wDepth = (int)Math.floor(aabb.minX);
        while ((double)wDepth < Math.ceil(aabb.maxX)) {
            int y = (int)Math.floor(aabb.minY);
            while ((double)y < Math.ceil(aabb.maxY)) {
                int z = (int)Math.floor(aabb.minZ);
                while ((double)z < Math.ceil(aabb.maxZ)) {
                    if (this.world.getBlockState(pos.setPos(wDepth, y, z)).getMaterial().isSolid()) {
                        return false;
                    }
                    ++z;
                }
                ++y;
            }
            ++wDepth;
        }
        if (this.getSpawnLocation() == SpawnLocation.Water) {
            if (this.getSwimmingParameters() == null) {
                this.baseStats.swimmingParameters = EntityPixelmon.getBaseStats((String)"Magikarp").get().swimmingParameters;
            }
            if ((wDepth = WorldHelper.getWaterDepth(this.getPosition(), this.world)) > this.getSwimmingParameters().depthRangeStart && wDepth < this.getSwimmingParameters().depthRangeEnd) {
                return true;
            }
            double prevPosY = this.posY;
            this.posY -= (double)(this.getSwimmingParameters().depthRangeStart + this.rand.nextInt(this.getSwimmingParameters().depthRangeEnd - this.getSwimmingParameters().depthRangeStart));
            wDepth = WorldHelper.getWaterDepth(this.getPosition(), this.world);
            if (wDepth > this.getSwimmingParameters().depthRangeStart && wDepth < this.getSwimmingParameters().depthRangeEnd) {
                this.posY = prevPosY;
                return false;
            }
            return true;
        }
        return true;
    }

    public boolean embed(EntityPixelmon p) {
        NBTTagCompound nbt = new NBTTagCompound();
        p.writeEntityToNBT(nbt);
        return this.embeddedPokemon.add(nbt);
    }

    public boolean embed(NBTTagCompound p) {
        return this.embeddedPokemon.add(p);
    }

    @Override
    public void updateLeashedState() {
    }

    @Override
    public boolean canDespawn() {
        return this.legendaryTicks <= 0 && super.canDespawn();
    }

    @Override
    public EntityLivingBase getOwner() {
        return !this.hasOwner() ? null : super.getOwner();
    }

    @Override
    protected void despawnEntity() {
        Event.Result result;
        if (this.isNoDespawnRequired()) {
            this.ticksExisted = 0;
        } else if ((this.entityAge & 0x1F) == 31 && (result = ForgeEventFactory.canEntityDespawn(this)) != Event.Result.DEFAULT) {
            if (result == Event.Result.DENY) {
                this.entityAge = 0;
            } else {
                this.setDead();
            }
        } else {
            EntityPlayer entityplayer = this.world.getClosestPlayerToEntity(this, -1.0);
            if (entityplayer != null) {
                double d0 = entityplayer.posX - this.posX;
                double d2 = entityplayer.posZ - this.posZ;
                double d3 = d0 * d0 + d2 * d2;
                if (this.canDespawn() && d3 > 16384.0) {
                    this.setDead();
                }
                if (d3 <= 2000.0) {
                    this.entityAge = 0;
                }
            }
            if (this.entityAge > 600 && this.rand.nextInt(500) == 0 && this.canDespawn()) {
                this.setDead();
            }
        }
    }

    @Override
    public void onUpdate() {
        if (this.particleEffects == null) {
            if (!this.isShiny() && !this.getParticleId().isEmpty()) {
                String particle = this.getParticleId();
                if (particle.equalsIgnoreCase("shiny") || particle.equalsIgnoreCase("ultrashiny")) {
                    this.setParticleId("");
                    this.particleEffects = ParticleEffects.getParticleEffects(this);
                }
            } else if (!this.getParticleId().isEmpty()) {
                this.particleEffects = ParticleEffects.getParticleEffects(this);
            } else if (this.isShiny()) {
                this.setParticleId("shiny");
                this.particleEffects = ParticleEffects.getParticleEffects(this);
            }
        }
        try {
            if (freeze) {
                return;
            }
            if (MinecraftForge.EVENT_BUS.post(new PixelmonUpdateEvent(this, TickEvent.Phase.START))) {
                return;
            }
            if (this.posY < 0.0 && !this.world.isRemote) {
                if (this.battleController == null) {
                    this.setDead();
                } else {
                    this.posY = 0.0;
                    this.motionZ = 0.0;
                    this.motionY = 0.0;
                    this.motionX = 0.0;
                }
            }
            if ((this.hasNPCTrainer || this.trainer != null) && this.battleController == null) {
                this.setDead();
            }
            if (this.canDespawn && !this.world.isRemote) {
                this.despawnTick = (this.despawnTick + 1) % 60;
                if (this.despawnTick == 0) {
                    if (this.legendaryTicks > 0 || this.battleController != null && this.battleController.containsParticipantType(PlayerParticipant.class) || this.getOwner() != null || this.baseStats == null || this.blockOwner != null) {
                        return;
                    }
                    if (this.playersNearby() && this.despawnCounter != 0) {
                        this.despawnCounter = (int)(Math.random() * (double)(intMaxTicksToDespawn - intMinTicksToDespawn) + (double)intMinTicksToDespawn);
                    } else {
                        if (this.battleController != null) {
                            this.battleController.endBattleWithoutXP();
                        }
                        this.setDead();
                    }
                }
                this.checkForRarityDespawn();
                if (this.legendaryTicks >= 0 && this.battleController == null) {
                    --this.legendaryTicks;
                    if (this.legendaryTicks == 0) {
                        this.setDead();
                    }
                }
            }
            if (this.playerOwned && this.getOwner() == null) {
                this.setDead();
            }
            super.onUpdate();
            MinecraftForge.EVENT_BUS.post(new PixelmonUpdateEvent(this, TickEvent.Phase.END));
        }
        catch (Exception var2) {
            Pixelmon.LOGGER.error("Error in ticking Pixelmon entity.");
            var2.printStackTrace();
        }
    }

    private void checkForRarityDespawn() {
        if (!(this.legendaryTicks > 0 || this.battleController != null && this.battleController.containsParticipantType(PlayerParticipant.class) || this.getOwner() != null || this.baseStats == null || this.blockOwner != null)) {
            if (this.despawnCounter > 0) {
                --this.despawnCounter;
            } else if (this.despawnCounter == 0) {
                if (!this.playersNearby()) {
                    if (this.battleController != null) {
                        this.battleController.endBattleWithoutXP();
                    }
                    this.setDead();
                }
            } else {
                this.despawnCounter = (int)(Math.random() * (double)(intMaxTicksToDespawn - intMinTicksToDespawn) + (double)intMinTicksToDespawn);
            }
        }
    }

    private boolean playersNearby() {
        for (int i = 0; i < this.world.playerEntities.size(); ++i) {
            EntityPlayer player = this.world.playerEntities.get(i);
            double distancex = player.posX - this.posX;
            double distancey = player.posY - this.posY;
            double distancez = player.posZ - this.posZ;
            double distancesquared = distancex * distancex + distancey * distancey + distancez * distancez;
            if (distancesquared >= (double)(PixelmonConfig.despawnRadius * PixelmonConfig.despawnRadius)) continue;
            return true;
        }
        return false;
    }

    @Override
    public boolean writeToNBTOptional(NBTTagCompound par1nbtTagCompound) {
        return !this.isDead && !this.hasOwner() && this.blockOwner == null && (!this.canDespawn() || PixelmonConfig.writeEntitiesToWorld) && super.writeToNBTOptional(par1nbtTagCompound);
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound nbt) {
        super.writeEntityToNBT(nbt);
        if (this.getOwner() != null) {
            nbt.setUniqueId("pixelmonOwnerUUID", this.getOwnerId());
        }
        if (this.getSpawnLocation() == null) {
            this.setSpawnLocation(SpawnLocation.Land);
        }
        NBTTagList embeddedP = new NBTTagList();
        for (NBTTagCompound n : this.embeddedPokemon) {
            embeddedP.appendTag(n);
        }
        nbt.setTag("embeddedPokemon", embeddedP);
        nbt.setInteger("pixelmonType", this.getSpawnLocation().ordinal());
        if (this.legendaryTicks > 0) {
            nbt.setInteger("legendaryTicks", this.legendaryTicks);
            nbt.setLong("legendaryTime", this.world.getTotalWorldTime());
        }
        nbt.setBoolean("Converted", true);
        nbt.setBoolean("Totem", this.isTotem());
    }

    @Override
    public void getNBTTags(HashMap<String, Class> tags) {
        super.getNBTTags(tags);
        tags.put("pixelmonOwnerUUID", String.class);
        tags.put("pixelmonType", Integer.class);
        tags.put("Converted", Boolean.class);
        tags.put("embeddedPokemon", NBTBase.class);
        tags.put("Totem", Boolean.class);
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        try {
            super.readEntityFromNBT(nbt);
            if (nbt.hasKey("pixelmonOwnerUUID")) {
                this.setOwnerId(nbt.getUniqueId("pixelmonOwnerUUID"));
            }
            if (nbt.hasKey("embeddedPokemon")) {
                NBTTagList embeddedP = (NBTTagList)nbt.getTag("embeddedPokemon");
                this.embeddedPokemon = new ArrayList<NBTTagCompound>();
                for (int i = 0; i < embeddedP.tagCount(); ++i) {
                    this.embeddedPokemon.add(embeddedP.getCompoundTagAt(i));
                }
            }
            float h = this.getHealth();
            this.level.readFromNBT(nbt);
            this.setHealth(h);
            if (nbt.hasKey("pixelmonType")) {
                this.setSpawnLocation(SpawnLocation.getFromIndex(nbt.getInteger("pixelmonType")));
            } else if (this.baseStats.spawnLocations[0] == SpawnLocation.Land) {
                this.setSpawnLocation(SpawnLocation.Land);
            } else {
                this.setSpawnLocation(SpawnLocation.Water);
            }
            this.aiHelper = new AIHelper(this, this.tasks);
            if (nbt.hasKey("legendaryTicks")) {
                this.legendaryTicks = nbt.getInteger("legendaryTicks");
                long lastTime = nbt.getLong("legendaryTime");
                this.legendaryTicks = (int)((long)this.legendaryTicks - (this.world.getTotalWorldTime() - lastTime));
                if (this.legendaryTicks <= 0) {
                    this.setDead();
                }
            }
            if (nbt.hasKey("Totem")) {
                this.setTotem(nbt.getBoolean("Totem"));
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void unloadEntity() {
        super.unloadEntity();
        this.world.removeEntity(this);
        this.clearAttackTarget();
    }

    @Override
    public EntityAgeable createChild(EntityAgeable var1) {
        return null;
    }

    public EnumSpecies[] getPreEvolutions() {
        return this.baseStats.preEvolutions;
    }

    @Override
    public boolean canBeLeashedTo(EntityPlayer player) {
        return this.getOwner() == player;
    }

    public void startEvolution(Evolution evolution) {
        Optional<PlayerStorage> optstorage = this.getStorage();
        if (optstorage.isPresent()) {
            optstorage.get().guiOpened = true;
            new EvolutionQuery(this, evolution);
        }
    }

    public boolean isLoaded() {
        return this.isLoaded(false);
    }

    public boolean isLoaded(boolean checkChunk) {
        boolean isLoaded = true;
        if (checkChunk) {
            isLoaded = this.world.isAreaLoaded(this.getPosition(), 1);
        }
        if (isLoaded) {
            isLoaded = this.world.getEntityByID(this.getEntityId()) != null;
        }
        return isLoaded;
    }

    public <T extends Evolution> ArrayList<T> getEvolutions(Class<T> type) {
        ArrayList<T> evolutions = new ArrayList<>();
        for (Evolution evo : this.baseStats.evolutions) {
            if (evo == null || !type.isInstance(evo)) continue;
            evolutions.add((T) evo);
        }
        return evolutions;
    }

    public boolean testTradeEvolution(EnumSpecies with) {
        if (this.isEgg) {
            return false;
        }
        ArrayList<TradeEvolution> tradeEvolutions = this.getEvolutions(TradeEvolution.class);
        for (TradeEvolution tradeEvolution : tradeEvolutions) {
            if (!tradeEvolution.canEvolve(this, with)) continue;
            return tradeEvolution.doEvolution(this);
        }
        return false;
    }

    public boolean testLevelEvolution(int level) {
        ArrayList<LevelingEvolution> levelingEvolutions = this.getEvolutions(LevelingEvolution.class);
        for (LevelingEvolution levelingEvolution : levelingEvolutions) {
            if (!levelingEvolution.canEvolve(this, level)) continue;
            return levelingEvolution.doEvolution(this);
        }
        return false;
    }

    public boolean testNatureEvolution(int level) {
        ArrayList<NatureEvolution> natureEvolutions = this.getEvolutions(NatureEvolution.class);
        for (NatureEvolution natureEvolution : natureEvolutions) {
            if (!natureEvolution.canEvolve(this, level)) continue;
            return natureEvolution.doEvolution(this);
        }
        return false;
    }

    public boolean testInteractEvolution(ItemStack stack) {
        ArrayList<InteractEvolution> interactingEvolutions = this.getEvolutions(InteractEvolution.class);
        for (InteractEvolution interactingEvolution : interactingEvolutions) {
            if (!interactingEvolution.canEvolve(this, stack)) continue;
            if (interactingEvolution instanceof MilceryEvolution) {
                return interactingEvolution.doEvolution(stack, this);
            }
            return interactingEvolution.doEvolution(this);
        }
        return false;
    }

    public void setSpawnerParent(TileEntityPixelmonSpawner spawner) {
        this.spawner = spawner;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public boolean isInRangeToRender3d(double x, double y, double z) {
        double modifier = this.getEntityBoundingBox().getAverageEdgeLength();
        if (modifier < 0.3) {
            modifier = 0.3;
        }
        return this.getDistance(x, y, z) < modifier * PixelmonConfig.pokemonRenderDistance * EntityPixelmon.getRenderDistanceWeight();
    }

    public void playPixelmonSound() {
        if (this.baseStats.hasSoundForGender(this.gender)) {
            this.playLivingSound();
        }
    }

    public boolean isPokemon(String ... matches) {
        String[] var2 = matches;
        int var3 = matches.length;
        for (int var4 = 0; var4 < var3; ++var4) {
            String match = var2[var4];
            if (!this.getPokemonName().equalsIgnoreCase(match)) continue;
            return true;
        }
        return false;
    }

    @Deprecated
    public String getRealNickname() {
        return super.getNickname();
    }

    public void setTotem(boolean result) {
        if (result) {
            this.level.setLevel(100);
            this.dataManager.set(dwTotem, true);
            this.setGrowth(EnumGrowth.Ordinary);
            this.setColorTint("#9c7d06");
        }
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getAttributeMap().registerAttribute(SharedMonsterAttributes.ATTACK_DAMAGE);
        this.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(6.0);
    }

    @Override
    public boolean attackEntityAsMob(Entity entityIn) {
        boolean flag = entityIn.attackEntityFrom(DamageSource.causeMobDamage(this), 10.0f);
        if (flag) {
            this.applyEnchantments(this, entityIn);
        }
        return flag;
    }

    public static class AIHurtByTarget
    extends EntityAIHurtByTarget {
        public AIHurtByTarget(EntityCreature creatureIn, boolean entityCallsForHelpIn, Class<?> ... excludedReinforcementTypes) {
            super(creatureIn, entityCallsForHelpIn, excludedReinforcementTypes);
        }

        @Override
        public void startExecuting() {
            super.startExecuting();
        }

        @Override
        protected void setEntityAttackTarget(EntityCreature creatureIn, EntityLivingBase entityLivingBaseIn) {
            if (creatureIn instanceof EntityPixelmon) {
                super.setEntityAttackTarget(creatureIn, entityLivingBaseIn);
            }
        }
    }

    public static class AIMeleeAttack
    extends EntityAIAttackMelee {
        public AIMeleeAttack(EntityCreature creature, double speedIn, boolean useLongMemory) {
            super(creature, speedIn, useLongMemory);
        }

        @Override
        protected void checkAndPerformAttack(EntityLivingBase enemy, double distToEnemySqr) {
            double d0 = this.getAttackReachSqr(enemy);
            if (distToEnemySqr <= d0 && this.attackTick <= 0) {
                this.attackTick = 20;
                this.attacker.attackEntityAsMob(enemy);
            } else if (distToEnemySqr <= d0 * 2.0) {
                if (this.attackTick <= 0) {
                    this.attackTick = 20;
                }
            } else {
                this.attackTick = 20;
            }
        }

        @Override
        protected double getAttackReachSqr(EntityLivingBase attackTarget) {
            return 4.0f + attackTarget.width;
        }
    }

    public static class AIAttackPlayer
    extends EntityAINearestAttackableTarget<EntityPlayer> {
        public AIAttackPlayer(EntityCreature creature, Class<EntityPlayer> classTarget, boolean checkSight) {
            super(creature, classTarget, checkSight);
        }

        @Override
        public boolean shouldExecute() {
            return true;
        }

        @Override
        protected double getTargetDistance() {
            return super.getTargetDistance() * 0.5;
        }
    }

    public static class AIPassiveMobAttack
    extends EntityAIAttackMelee {
        public AIPassiveMobAttack(EntityCreature creature, double speedIn, boolean useLongMemory) {
            super(creature, speedIn, useLongMemory);
        }

        @Override
        protected void checkAndPerformAttack(EntityLivingBase entity, double distanceIn) {
            double reach = this.getAttackReachSqr(entity);
            if (distanceIn <= reach && this.attackTick <= 0) {
                this.attackTick = 20;
                this.attacker.swingArm(EnumHand.MAIN_HAND);
            }
        }
    }
}

