/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import java.util.ArrayList;

public class Triage
extends AbilityBase {
    @Override
    public float modifyPriority(PixelmonWrapper pokemon, float priority) {
        if (this.moveIsHealingMove(pokemon.attack)) {
            priority += 3.0f;
        }
        return priority;
    }

    public boolean moveIsHealingMove(Attack sAttack) {
        ArrayList<String> attacks = new ArrayList<String>();
        attacks.add("draining kiss");
        attacks.add("floral healing");
        attacks.add("giga drain");
        attacks.add("rest");
        attacks.add("synthesis");
        attacks.add("absorb");
        attacks.add("drain punch");
        attacks.add("dreameater");
        attacks.add("heal order");
        attacks.add("heal pulse");
        attacks.add("healing wish");
        attacks.add("horn leech");
        attacks.add("leech life");
        attacks.add("lunar dance");
        attacks.add("mega drain");
        attacks.add("milk drink");
        attacks.add("moonlight");
        attacks.add("morning sun");
        attacks.add("oblivion wing");
        attacks.add("parabolic charge");
        attacks.add("purify");
        attacks.add("recover");
        attacks.add("roost");
        attacks.add("shore up");
        attacks.add("slack off");
        attacks.add("softboiled");
        attacks.add("strength sap");
        attacks.add("swallow");
        attacks.add("wish");
        for (String attack : attacks) {
            if (!sAttack.isAttack(attack)) continue;
            return true;
        }
        return false;
    }
}

