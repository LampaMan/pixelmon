/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Transformed;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Illusion;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.clientStorage.Add;
import net.minecraft.entity.player.EntityPlayerMP;

public class Imposter
extends AbilityBase {
    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.bc.simulateMode) {
            return;
        }
        PixelmonWrapper target = newPokemon.bc.getOppositePokemon(newPokemon);
        if (target.getBattleAbility() instanceof Illusion && ((Illusion)target.getBattleAbility()).disguisedPokemon != null) {
            return;
        }
        if (!target.hasStatus(StatusType.Substitute, StatusType.Transformed)) {
            newPokemon.bc.sendToAll("pixelmon.abilities.imposter", newPokemon.getNickname(), target.getNickname());
            newPokemon.addStatus(new Transformed(newPokemon, target), target);
            newPokemon.pokemon.getDataManager().set(EntityPixelmon.dwTransformation, -1);
            PixelmonData newData = new PixelmonData(newPokemon.pokemon);
            newData.inBattle = true;
            newData.order = newPokemon.getPartyPosition();
            EntityPlayerMP player = newPokemon.getPlayerOwner();
            if (player != null) {
                Pixelmon.NETWORK.sendTo(new Add(newData, true), player);
            }
            newPokemon.update(EnumUpdateType.Moveset);
            newPokemon.update(EnumUpdateType.Stats);
            newPokemon.bc.spectators.forEach(spectator -> spectator.sendMessage(new Add(newData, true, true)));
        }
    }
}

