/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;
import java.util.ArrayList;
import net.minecraft.world.biome.Biome;

public class BiomeCondition
extends EvoCondition {
    public ArrayList<Biome> biomes = new ArrayList();

    public BiomeCondition() {
    }

    public BiomeCondition(Biome ... biomes) {
        for (Biome biome : biomes) {
            this.biomes.add(biome);
        }
    }

    public BiomeCondition(ArrayList<Biome> biomes) {
        this.biomes = biomes;
    }

    @Override
    public boolean passes(EntityPixelmon pokemon) {
        return this.biomes.contains(pokemon.getEntityWorld().getBiome(pokemon.getPosition()));
    }
}

