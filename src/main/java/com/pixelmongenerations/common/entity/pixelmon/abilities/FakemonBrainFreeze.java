/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Freeze;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class FakemonBrainFreeze
extends AbilityBase {
    @Override
    public boolean allowsStatus(StatusType status, PixelmonWrapper pokemon, PixelmonWrapper user) {
        if (user != pokemon && user.attack != null && user.attack.getAttackCategory() == AttackCategory.Status && (StatusType.isPrimaryStatus(status) || status.equals((Object)StatusType.Yawn))) {
            user.bc.sendToAll("pixelmon.abilities.fakemonbrainfreeze", user.getNickname());
            user.addStatus(new Freeze(), user);
        }
        return false;
    }
}

