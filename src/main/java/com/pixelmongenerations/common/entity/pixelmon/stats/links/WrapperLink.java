/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.links;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusPersist;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.ExtraStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.FriendShip;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.entity.pixelmon.stats.Level;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.Stats;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.enums.EnumMark;
import com.pixelmongenerations.core.enums.EnumNature;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.PixelmonStatsData;
import com.pixelmongenerations.core.network.packetHandlers.LevelUp;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.List;
import java.util.Optional;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class WrapperLink
extends PokemonLink {
    private PixelmonWrapper pixelmon;
    private PokemonLink innerLink;

    public WrapperLink(PixelmonWrapper pixelmon) {
        this.pixelmon = pixelmon;
        this.innerLink = pixelmon.getInnerLink();
    }

    @Override
    public BaseStats getBaseStats() {
        return this.pixelmon.getBaseStats();
    }

    @Override
    public Stats getStats() {
        return this.pixelmon.getStats();
    }

    @Override
    public ItemHeld getHeldItem() {
        return this.pixelmon.getHeldItem();
    }

    @Override
    public void setHeldItem(ItemStack item) {
        this.pixelmon.setHeldItem(item);
    }

    @Override
    public int getHealth() {
        return this.pixelmon.getHealth();
    }

    @Override
    public int getMaxHealth() {
        return this.pixelmon.getMaxHealth();
    }

    @Override
    public void setHealth(int health) {
        int currentHealth = this.getHealth();
        if (health > currentHealth) {
            this.pixelmon.healEntityBy(health - currentHealth);
        } else if (health < currentHealth) {
            this.pixelmon.doBattleDamage(this.pixelmon, currentHealth - health, DamageTypeEnum.SELF);
        }
    }

    @Override
    public void setHealthDirect(int health) {
        this.pixelmon.setHealth(health);
    }

    @Override
    public int getLevel() {
        return this.pixelmon.getLevelNum();
    }

    @Override
    public void setLevel(int level) {
        this.pixelmon.setLevelNum(level);
    }

    @Override
    public int getDynamaxLevel() {
        return this.pixelmon.getDynamaxLevel();
    }

    @Override
    public void setDynamaxLevel(int level) {
        this.pixelmon.setDynamaxLevel(level);
    }

    @Override
    public int getExp() {
        return this.pixelmon.getExp();
    }

    @Override
    public void setExp(int experience) {
        this.pixelmon.setExp(experience);
    }

    @Override
    public FriendShip getFriendship() {
        return this.pixelmon.getFriendship();
    }

    @Override
    public boolean doesLevel() {
        return this.pixelmon.doesLevel();
    }

    @Override
    public EntityPlayerMP getPlayerOwner() {
        return this.pixelmon.getPlayerOwner();
    }

    @Override
    public String getRealNickname() {
        return this.pixelmon.getNickname();
    }

    @Override
    public BattleControllerBase getBattleController() {
        return this.pixelmon.bc;
    }

    @Override
    public Moveset getMoveset() {
        return this.pixelmon.getMoveset();
    }

    @Override
    public int[] getPokemonID() {
        return this.pixelmon.getPokemonID();
    }

    @Override
    public EntityPixelmon getEntity() {
        if (this.pixelmon.pokemon != null) {
            return this.pixelmon.pokemon;
        }
        BattleParticipant participant = this.pixelmon.getParticipant();
        PlayerStorage storage = participant.getStorage();
        if (storage == null) {
            return null;
        }
        return storage.sendOut(this.getPokemonID(), participant.getWorld());
    }

    @Override
    public void setScale(float scale) {
    }

    @Override
    public World getWorld() {
        return this.pixelmon.getParticipant().getWorld();
    }

    @Override
    public Gender getGender() {
        return this.pixelmon.getGender();
    }

    @Override
    public BlockPos getPos() {
        EntityLivingBase entity = this.pixelmon.pokemon;
        if (this.pixelmon.pokemon == null) {
            entity = this.pixelmon.getParticipant().getEntity();
        }
        return entity.getPosition();
    }

    @Override
    public Optional<PlayerStorage> getStorage() {
        return Optional.of(this.pixelmon.getParticipant().getStorage());
    }

    @Override
    public void update(EnumUpdateType ... updateTypes) {
        this.pixelmon.update(updateTypes);
    }

    @Override
    public void updateStats() {
        this.pixelmon.getStats().setLevelStats(this.pixelmon.getNature(), this.pixelmon.getPseudoNature(), this.pixelmon.getBaseStats(), this.pixelmon.getLevelNum());
        this.pixelmon.update(EnumUpdateType.HP, EnumUpdateType.Stats);
    }

    @Override
    public void updateLevelUp(PixelmonStatsData stats) {
        EntityPlayerMP owner = this.pixelmon.getPlayerOwner();
        if (owner != null) {
            PixelmonStatsData stats2 = PixelmonStatsData.createPacket(this);
            Pixelmon.NETWORK.sendTo(new LevelUp(this.pixelmon.nbt, this.getLevel(), stats, stats2), owner);
            this.pixelmon.updateHPIncrease();
        }
    }

    @Override
    public void sendMessage(String langKey, Object ... data) {
        this.pixelmon.bc.sendToAll(langKey, data);
    }

    @Override
    public String getNickname() {
        return this.pixelmon.getNickname();
    }

    @Override
    public String getOriginalTrainer() {
        return this.pixelmon.getOriginalTrainer();
    }

    @Override
    public String getOriginalTrainerUUID() {
        return this.pixelmon.getOriginalTrainerUUID();
    }

    @Override
    public String getRealTextureNoCheck(PixelmonWrapper pw) {
        return this.pixelmon.getRealTextureNoCheck();
    }

    @Override
    public boolean removeStatuses(StatusType ... statuses) {
        return this.pixelmon.removeStatuses(statuses);
    }

    @Override
    public EnumNature getNature() {
        return this.pixelmon.getNature();
    }

    @Override
    public EnumNature getPseudoNature() {
        return this.pixelmon.getPseudoNature();
    }

    @Override
    public int getExpToNextLevel() {
        return this.pixelmon.getLevel().expToNextLevel;
    }

    @Override
    public StatusPersist getPrimaryStatus() {
        return this.pixelmon.getPrimaryStatus();
    }

    @Override
    public AbilityBase getAbility() {
        return this.pixelmon.getAbility();
    }

    @Override
    public List<EnumType> getType() {
        return this.pixelmon.type;
    }

    @Override
    public int getForm() {
        return this.pixelmon.getForm();
    }

    @Override
    public boolean isShiny() {
        return this.innerLink.isShiny();
    }

    @Override
    public boolean isEgg() {
        return false;
    }

    @Override
    public int getEggCycles() {
        return 0;
    }

    @Override
    public EnumGrowth getGrowth() {
        return this.innerLink.getGrowth();
    }

    @Override
    public EnumPokeball getCaughtBall() {
        return this.innerLink.getCaughtBall();
    }

    @Override
    public int getPartyPosition() {
        return this.pixelmon.getPartyPosition();
    }

    @Override
    public ItemStack getHeldItemStack() {
        return this.innerLink.getHeldItemStack();
    }

    @Override
    public boolean hasOwner() {
        return this.pixelmon.getParticipant().getType() != ParticipantType.WildPokemon;
    }

    @Override
    public EnumBossMode getBossMode() {
        return this.innerLink.getBossMode();
    }

    @Override
    public int getSpecialTexture() {
        return this.innerLink.getSpecialTexture();
    }

    @Override
    public boolean isInRanch() {
        return false;
    }

    @Override
    public int[] getEggMoves() {
        return this.innerLink.getEggMoves();
    }

    @Override
    public Level getLevelContainer() {
        return this.pixelmon.getLevel();
    }

    public PixelmonWrapper getPokemon() {
        return this.pixelmon;
    }

    @Override
    public NBTTagCompound getNBT() {
        return this.innerLink.getNBT();
    }

    @Override
    public boolean isTotem() {
        return this.innerLink.isTotem();
    }

    @Override
    public String getCustomTexture() {
        return this.getPokemon().pokemon.getCustomTexture();
    }

    @Override
    public int getPokeRus() {
        return this.getPokemon().getPokeRus();
    }

    @Override
    public boolean hasGmaxFactor() {
        return this.innerLink.hasGmaxFactor();
    }

    @Override
    public void setShiny(boolean shiny) {
        this.getPokemon().pokemon.setShiny(true);
    }

    public PixelmonWrapper getWrapper() {
        return this.pixelmon;
    }

    @Override
    public EnumMark getMark() {
        EnumMark mark = this.innerLink.getMark();
        return mark;
    }

    @Override
    public void setMark(EnumMark mark) {
        this.getPokemon().pokemon.setMark(mark);
    }

    @Override
    public ExtraStats getExtraStats() {
        return this.getPokemon().pokemon.extraStats;
    }

    @Override
    public IEnumForm getFormEnum() {
        return this.innerLink.getFormEnum();
    }

    @Override
    public void setForm(int form) {
        this.pixelmon.setForm(form);
    }

    @Override
    public void setForm(IEnumForm form) {
        this.pixelmon.setForm(form.getForm());
    }

    @Override
    public void setNature(EnumNature nature) {
        this.getPokemon().pokemon.setNature(nature);
    }

    @Override
    public void setPseudoNature(EnumNature nature) {
        this.getPokemon().pokemon.setPseudoNature(nature);
    }

    @Override
    public void setGender(Gender gender) {
        this.getPokemon().pokemon.setGender(gender);
    }

    @Override
    public void setAbility(String ability) {
        this.getPokemon().pokemon.setAbility(ability);
        this.getPokemon().pokemon.giveAbilitySlot();
    }

    @Override
    public void setCustomTexture(String customTexture) {
        this.getPokemon().pokemon.setCustomSpecialTexture(customTexture);
    }

    @Override
    public void setGrowth(EnumGrowth growth) {
        this.getPokemon().pokemon.setGrowth(growth);
    }

    @Override
    public void setCaughtBall(EnumPokeball ball) {
        this.getPokemon().pokemon.caughtBall = ball;
    }

    @Override
    public void setBossMode(EnumBossMode mode) {
        this.getPokemon().pokemon.setBoss(mode);
    }

    @Override
    public void setSpecialTexture(int special) {
        this.getPokemon().pokemon.setSpecialTexture(special);
    }

    @Override
    public void setTotem(boolean totem) {
        this.getPokemon().pokemon.setTotem(totem);
    }

    @Override
    public void setParticleId(String particleId) {
        this.getPokemon().pokemon.setParticleId(particleId);
    }

    @Override
    public void setStats(Stats stats) {
        this.getPokemon().pokemon.stats = stats;
    }
}

