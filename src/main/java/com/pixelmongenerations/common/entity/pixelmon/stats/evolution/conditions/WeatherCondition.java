/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions;

import com.pixelmongenerations.api.world.WeatherType;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;

public class WeatherCondition
extends EvoCondition {
    WeatherType weather = WeatherType.RAIN;

    public WeatherCondition() {
    }

    public WeatherCondition(WeatherType weather) {
        this.weather = weather;
    }

    @Override
    public boolean passes(EntityPixelmon pokemon) {
        return WeatherType.get(pokemon.getEntityWorld()) == this.weather || this.weather == WeatherType.RAIN && WeatherType.get(pokemon.getEntityWorld()) == WeatherType.STORM;
    }
}

