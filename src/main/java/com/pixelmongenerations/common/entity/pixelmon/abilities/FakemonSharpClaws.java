/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import java.util.Arrays;

public class FakemonSharpClaws
extends AbilityBase {
    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        String[] punchMoves = new String[]{"Air Cutter", "Air Slash", "Cross Poison", "Crush Claw", "Cut", "Dragon Claw", "False Swipe", "Fury Cutter", "Fury Swipes", "Leaf Blade", "Metal Claw", "Night Slash", "Psycho Cut", "Razor Leaf", "Razor Wind", "Scratch", "Secret Sword", "Shadow Claw", "Slash", "X-Scissor"};
        if (Arrays.asList(punchMoves).contains(a.getAttackBase().getUnlocalizedName())) {
            power = (int)((double)power * 1.2);
        }
        return new int[]{power, accuracy};
    }
}

