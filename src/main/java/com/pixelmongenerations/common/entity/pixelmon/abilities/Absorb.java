/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;

public class Absorb
extends AbilityBase {
    private EnumType type;
    private String langString;

    protected Absorb(EnumType type, String langString) {
        this.type = type;
        this.langString = langString;
    }

    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        if (a.getType() == this.type) {
            if (!pokemon.hasStatus(StatusType.HealBlock)) {
                if (pokemon.hasFullHealth()) {
                    pokemon.bc.sendToAll(this.langString + "2", pokemon.getNickname());
                } else {
                    int healAmount = pokemon.getPercentMaxHealth(25.0f);
                    float healPercent = pokemon.getHealPercent(healAmount);
                    pokemon.healEntityBy(healAmount);
                    pokemon.bc.sendToAll(this.langString, pokemon.getNickname());
                    a.moveResult.weightMod -= healPercent;
                }
                return false;
            }
        }
        return true;
    }
}

