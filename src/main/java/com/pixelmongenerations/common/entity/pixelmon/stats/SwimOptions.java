/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.JsonObject
 */
package com.pixelmongenerations.common.entity.pixelmon.stats;

import com.google.gson.JsonObject;

public class SwimOptions {
    public int depthRangeStart = 0;
    public int depthRangeEnd = 5;
    public float swimSpeed = 1.0f;
    public float decayRate = 0.99f;
    public int refreshRate = 100;

    public SwimOptions(int depthMin, int depthMax, double swimSpeed, double decayRate, int refreshRate) {
        this.depthRangeStart = depthMin;
        this.depthRangeEnd = depthMax;
        this.swimSpeed = (float)swimSpeed;
        this.decayRate = (float)decayRate;
        this.refreshRate = refreshRate;
    }

    public SwimOptions(JsonObject jsonObject) {
        if (jsonObject.has("depthmin")) {
            this.depthRangeStart = jsonObject.get("depthmin").getAsInt();
        }
        if (jsonObject.has("depthmax")) {
            this.depthRangeEnd = jsonObject.get("depthmax").getAsInt();
        }
        if (jsonObject.has("swimspeed")) {
            this.swimSpeed = jsonObject.get("swimspeed").getAsFloat();
        }
        if (jsonObject.has("decayrate")) {
            this.decayRate = jsonObject.get("decayrate").getAsFloat();
        }
        if (jsonObject.has("refreshrate")) {
            this.refreshRate = jsonObject.get("refreshrate").getAsInt();
        }
    }
}

