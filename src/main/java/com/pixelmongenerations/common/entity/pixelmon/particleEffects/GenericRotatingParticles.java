/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.particleEffects;

import com.pixelmongenerations.common.entity.pixelmon.Entity4Textures;
import com.pixelmongenerations.common.entity.pixelmon.particleEffects.ParticleEffects;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.awt.Color;
import net.minecraft.util.math.Vec3d;

public class GenericRotatingParticles
extends ParticleEffects {
    private String color = "";
    private int counter = 0;
    private int index = 0;

    public GenericRotatingParticles(Entity4Textures pixelmon) {
        super(pixelmon);
    }

    @Override
    public void onUpdate() {
        if (this.pixelmon.getParticleId().isEmpty()) {
            return;
        }
        double pokeScale = this.pixelmon.height * this.pixelmon.getPixelmonScale() * this.pixelmon.getScaleFactor();
        int delay = (int)(6.0 / pokeScale);
        if (this.counter <= 0) {
            this.counter = delay;
            int pokeParticleCount = Math.max((int)(5.0 * pokeScale), 1);
            this.index = this.index + 1 >= pokeParticleCount ? 0 : this.index + 1;
            double radius = 0.9 * pokeScale;
            Vec3d pos = this.getCirclePosition(this.pixelmon.getPositionVector(), radius, pokeParticleCount, this.index);
            int particleTint = this.pixelmon.hasParticleColorStrobe() ? this.getStrobeColor(this.pixelmon).getRGB() : this.pixelmon.getParticleTint();
            ClientProxy.spawnParticle(this.pixelmon.getParticleId(), this.pixelmon.world, pos.x, pos.y + pokeScale, pos.z, particleTint, this.pixelmon.getParticleTintAlpha());
            return;
        }
        --this.counter;
    }

    private Color getStrobeColor(Entity4Textures pixelmon) {
        try {
            String[] tint = (this.color.isEmpty() ? "255/0/0" : this.color).split("/");
            int r = Integer.parseInt(tint[0]);
            int g = Integer.parseInt(tint[1]);
            int b = Integer.parseInt(tint[2]);
            int increment = 8;
            int min = 75;
            if (r > 75 && b <= 75) {
                r -= 8;
                g += 8;
            }
            if (g > 75 && r <= 75) {
                g -= 8;
                b += 8;
            }
            if (b > 75 && g <= 75) {
                r += 8;
                b -= 8;
            }
            if (r < 75) {
                r = 75;
            }
            if (g < 75) {
                g = 75;
            }
            if (b < 75) {
                b = 75;
            }
            this.color = String.format("%s/%s/%s", r, g, b);
            return new Color((float)r / 255.0f, (float)g / 255.0f, (float)b / 255.0f);
        }
        catch (Exception exception) {
            this.color = "255/0/0";
            return new Color(255, 0, 0);
        }
    }

    public Vec3d getCirclePosition(Vec3d center, double radius, int amount, int index) {
        double increment = Math.PI * 2 / (double)amount;
        double angle = (double)index * increment;
        double x = center.x + radius * Math.cos(angle);
        double z = center.z + radius * Math.sin(angle);
        return new Vec3d(x, center.y, z);
    }
}

