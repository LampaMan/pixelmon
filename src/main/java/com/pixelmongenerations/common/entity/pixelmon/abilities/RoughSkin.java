/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.entity.pixelmon.abilities.ContactDamage;

public class RoughSkin
extends ContactDamage {
    public RoughSkin() {
        super("pixelmon.abilities.roughskin");
    }
}

