/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.externalMoves;

import com.pixelmongenerations.api.events.ExternalMoveEvent;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.ExternalMoveBase;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.common.MinecraftForge;

public class Teleport
extends ExternalMoveBase {
    public Teleport() {
        super("teleport", "Teleport");
    }

    public Teleport(String moveName, String ... moves) {
        super(moveName, moves);
    }

    @Override
    public boolean isTargetted() {
        return false;
    }

    @Override
    public boolean execute(EntityPixelmon user, RayTraceResult target, int moveIndex) {
        Optional<PlayerStorage> storage;
        if (user.hasOwner() && (storage = user.getStorage()).isPresent()) {
            ExternalMoveEvent.TeleportFlyMoveEvent teleportEvent = new ExternalMoveEvent.TeleportFlyMoveEvent((EntityPlayerMP)user.getOwner(), user, this, target, storage.get().teleportPos);
            if (MinecraftForge.EVENT_BUS.post(teleportEvent)) {
                return false;
            }
            teleportEvent.getDestination().teleport((EntityPlayerMP)user.getOwner());
            return true;
        }
        return false;
    }

    @Override
    public int getCooldown(EntityPixelmon user) {
        return 20 + (int)Math.max(0.0f, (300.0f - (float)user.stats.Speed) / 50.0f * 20.0f);
    }

    @Override
    public int getCooldown(PixelmonData user) {
        return 20 + (int)Math.max(0.0f, (300.0f - (float)user.Speed) / 50.0f * 20.0f);
    }

    @Override
    public boolean isDestructive() {
        return false;
    }
}

