/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class Truant
extends AbilityBase {
    public boolean canMove = true;

    @Override
    public boolean canAttackThisTurn(PixelmonWrapper user, Attack a) {
        if (user.hasStatus(StatusType.Sleep)) {
            this.canMove = true;
            return true;
        }
        if (this.canMove) {
            this.canMove = false;
            return true;
        }
        this.canMove = true;
        user.bc.sendToAll("pixelmon.abilities.truant", user.getNickname());
        return false;
    }

    @Override
    public void applySwitchOutEffect(PixelmonWrapper pokemon) {
        this.canMove = true;
    }

    @Override
    public boolean needNewInstance() {
        return true;
    }
}

