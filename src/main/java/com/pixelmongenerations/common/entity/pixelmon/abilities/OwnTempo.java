/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.PreventStatus;

public class OwnTempo
extends PreventStatus {
    public OwnTempo() {
        super("pixelmon.abilities.owntempo", "pixelmon.abilities.owntempocure", StatusType.Confusion);
    }
}

