/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.Evolution;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;
import com.pixelmongenerations.core.enums.EnumSpecies;

public class LevelingEvolution
extends Evolution {
    public int level = 1;

    public LevelingEvolution(EnumSpecies from, PokemonSpec to, int level, EvoCondition ... conditions) {
        super(from, to, conditions);
        this.level = level;
    }

    public boolean canEvolve(EntityPixelmon pokemon, int level) {
        return level >= this.level && super.canEvolve(pokemon);
    }
}

