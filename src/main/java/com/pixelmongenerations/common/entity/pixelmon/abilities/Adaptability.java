/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class Adaptability
extends AbilityBase {
    @Override
    public double modifyStab(double stab) {
        if (stab == 1.5) {
            return 2.0;
        }
        return 1.0;
    }
}

