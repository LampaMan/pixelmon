/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.status.GrassyTerrain;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Surge;

public class GrassySurge
extends Surge {
    public GrassySurge() {
        super(new GrassyTerrain());
    }
}

