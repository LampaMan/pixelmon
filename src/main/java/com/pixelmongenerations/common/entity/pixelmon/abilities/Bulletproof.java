/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class Bulletproof
extends AbilityBase {
    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper var1, PixelmonWrapper var2, Attack var3) {
        return !var3.isAttack("Acid Spray", "Aura Sphere", "Beak Blast", "Bullet Seed", "Electro Ball", "Energy Ball", "Focus Blast", "Gyro Ball", "Mist Ball", "Octazooka", "Pollen Puff", "Pyro Ball", "Rock Blast", "Rock Wrecker", "Searing Shot", "Seed Bomb", "Shadow Ball", "Sludge Bomb", "Weather Ball", "Zap Cannon");
    }

    @Override
    public void allowsIncomingAttackMessage(PixelmonWrapper var1, PixelmonWrapper var2, Attack var3) {
        var1.bc.sendToAll("pixelmon.abilities.bulletproof", var1.getNickname());
    }
}

