/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.extraStats;

import com.pixelmongenerations.common.entity.pixelmon.stats.ExtraStats;
import com.pixelmongenerations.core.config.PixelmonConfig;
import net.minecraft.nbt.NBTTagCompound;

public class LakeTrioStats
extends ExtraStats {
    public int numEnchanted = 0;
    public static final int MAX_ENCHANTED = PixelmonConfig.lakeTrioMaxEnchants;

    @Override
    public void writeToNBT(NBTTagCompound nbt) {
        nbt.setByte("NumEnchanted", (byte)this.numEnchanted);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        this.numEnchanted = nbt.getInteger("NumEnchanted");
    }
}

