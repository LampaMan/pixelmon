/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats;

import com.pixelmongenerations.common.entity.pixelmon.stats.extraStats.LakeTrioStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.extraStats.LightTrioStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.extraStats.MeloettaStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.extraStats.MewStats;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.function.Supplier;
import net.minecraft.nbt.NBTTagCompound;

public abstract class ExtraStats {
    static EnumMap<EnumSpecies, Supplier<ExtraStats>> statsList = new EnumMap(EnumSpecies.class);

    public abstract void writeToNBT(NBTTagCompound var1);

    public abstract void readFromNBT(NBTTagCompound var1);

    public static ExtraStats getExtraStats(String name) {
        return EnumSpecies.getFromName(name).map(statsList::get).map(Supplier::get).orElse(null);
    }

    public static void getNBTTags(HashMap<String, Class> tags) {
        tags.put("NumCloned", Short.class);
        tags.put("NumEnchanted", Short.class);
        tags.put("NumWormholes", Short.class);
    }

    static {
        statsList.put(EnumSpecies.Mew, MewStats::new);
        statsList.put(EnumSpecies.Azelf, LakeTrioStats::new);
        statsList.put(EnumSpecies.Uxie, LakeTrioStats::new);
        statsList.put(EnumSpecies.Mesprit, LakeTrioStats::new);
        statsList.put(EnumSpecies.Lunala, LightTrioStats::new);
        statsList.put(EnumSpecies.Solgaleo, LightTrioStats::new);
        statsList.put(EnumSpecies.Necrozma, LightTrioStats::new);
        statsList.put(EnumSpecies.Meloetta, MeloettaStats::new);
    }
}

