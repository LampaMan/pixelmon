/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class AsOne
extends AbilityBase {
    @Override
    public void tookDamageUser(int damage, PixelmonWrapper user, PixelmonWrapper pokemon, Attack a) {
        if (user.getForm() == 1) {
            if (pokemon.isFainted() && !a.getAttackBase().getMakesContact()) {
                this.sendActivatedMessage(user);
                user.getBattleStats().modifyStat(1, StatsType.Attack);
            }
        } else if (user.getForm() == 2 && pokemon.isFainted() && !a.getAttackBase().getMakesContact()) {
            this.sendActivatedMessage(user);
            user.getBattleStats().modifyStat(1, StatsType.SpecialAttack);
        }
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        newPokemon.bc.sendToAll("pixelmon.abilities.asone", newPokemon.getNickname());
    }

    @Override
    public boolean canBeNeutralized() {
        return true;
    }
}

