/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.ItemHeld;

public class Unburden
extends AbilityBase {
    private boolean itemUsed = false;

    @Override
    public void onItemConsumed(PixelmonWrapper pokemon, PixelmonWrapper consumer, ItemHeld heldItem) {
        if (pokemon == consumer && !pokemon.bc.simulateMode) {
            this.itemUsed = true;
        }
    }

    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        if (this.itemUsed) {
            int index = StatsType.Speed.getStatIndex();
            int[] arrn = stats;
            int n = index;
            arrn[n] = arrn[n] * 2;
        }
        return stats;
    }

    @Override
    public void applySwitchOutEffect(PixelmonWrapper oldPokemon) {
        int[] stats;
        this.itemUsed = false;
        int[] arrn = stats = oldPokemon.getBattleStats().getBattleStats();
        int n = StatsType.Speed.getStatIndex();
        arrn[n] = arrn[n] / 2;
        oldPokemon.getBattleStats().setStatsForTurn(stats);
    }

    @Override
    public boolean needNewInstance() {
        return true;
    }
}

