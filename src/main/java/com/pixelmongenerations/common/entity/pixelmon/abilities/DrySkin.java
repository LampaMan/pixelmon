/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Rainy;
import com.pixelmongenerations.common.battle.status.Sunny;
import com.pixelmongenerations.common.battle.status.Weather;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.enums.EnumType;

public class DrySkin
extends AbilityBase {
    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        if (pokemon.bc == null) {
            return;
        }
        Weather weather = pokemon.bc.globalStatusController.getWeather();
        if (weather instanceof Rainy && !pokemon.hasFullHealth() && pokemon.getHeldItem() != PixelmonItemsHeld.utilityUmbrella) {
            pokemon.bc.sendToAll("pixelmon.abilities.dryskinrain", pokemon.getNickname());
            pokemon.healByPercent(12.5f);
        }
        if (weather instanceof Sunny && pokemon.isAlive() && pokemon.getHeldItem() != PixelmonItemsHeld.utilityUmbrella) {
            pokemon.bc.sendToAll("pixelmon.abilities.dryskinsun", pokemon.getNickname());
            pokemon.doBattleDamage(pokemon, pokemon.getPercentMaxHealth(12.5f), DamageTypeEnum.ABILITY);
        }
    }

    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        if (a.getAttackBase().attackType == EnumType.Water) {
            if (pokemon.hasFullHealth()) {
                user.bc.sendToAll("pixelmon.abilities.dryskinnegate", pokemon.getNickname());
            } else {
                int healAmount = pokemon.getPercentMaxHealth(25.0f);
                pokemon.healEntityBy(healAmount);
                user.bc.sendToAll("pixelmon.abilities.dryskinrain", pokemon.getNickname());
                a.moveResult.weightMod -= pokemon.getHealPercent(healAmount);
            }
            return false;
        }
        return true;
    }

    @Override
    public int modifyDamageTarget(int damage, PixelmonWrapper user, PixelmonWrapper pokemon, Attack a) {
        if (a.getAttackBase().attackType == EnumType.Fire) {
            return (int)((double)damage * 1.25);
        }
        return damage;
    }
}

