/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.drops;

import com.pixelmongenerations.common.entity.pixelmon.stats.FlyingOptions;
import com.pixelmongenerations.common.entity.pixelmon.stats.SwimOptions;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.List;

public class BossInfo {
    public EnumSpecies pokemon;
    public SpawnLocation spawnLocation;
    public FlyingOptions flyingParameters;
    public SwimOptions swimmingParameters;
    public List<Integer> dimensions;

    public BossInfo(EnumSpecies pokemon, SpawnLocation spawnLocation) {
        this.pokemon = pokemon;
        this.spawnLocation = spawnLocation;
    }

    public BossInfo(EnumSpecies pokemon, SpawnLocation spawnLocation, FlyingOptions flyingParamters) {
        this(pokemon, spawnLocation);
        this.flyingParameters = flyingParamters;
    }

    public BossInfo(EnumSpecies pokemon, SpawnLocation spawnLocation, SwimOptions swimmingParamters) {
        this(pokemon, spawnLocation);
        this.swimmingParameters = swimmingParamters;
    }

    public BossInfo withDimensions(List<Integer> dimensions) {
        this.dimensions = dimensions;
        return this;
    }

    public BossInfo withFlyParams(FlyingOptions flyingParameters) {
        this.flyingParameters = flyingParameters;
        return this;
    }

    public BossInfo withSwimParams(SwimOptions swimmingParameters) {
        this.swimmingParameters = swimmingParameters;
        this.flyingParameters = null;
        return this;
    }
}

