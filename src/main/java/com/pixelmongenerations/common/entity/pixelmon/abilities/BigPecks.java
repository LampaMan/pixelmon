/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class BigPecks
extends AbilityBase {
    @Override
    public boolean allowsStatChange(PixelmonWrapper pokemon, PixelmonWrapper user, StatsEffect e) {
        if (e.getStatsType() == StatsType.Defence && !e.getUser()) {
            if (!Attack.dealsDamage(user.attack)) {
                user.bc.sendToAll("pixelmon.abilities.bigpecks", pokemon.getNickname());
            }
            return false;
        }
        return true;
    }
}

