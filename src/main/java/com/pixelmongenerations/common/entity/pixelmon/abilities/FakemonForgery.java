/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.entity.pixelmon.abilities.Ate;
import com.pixelmongenerations.core.enums.EnumType;

public class FakemonForgery
extends Ate {
    public FakemonForgery() {
        super(EnumType.Steel);
    }
}

