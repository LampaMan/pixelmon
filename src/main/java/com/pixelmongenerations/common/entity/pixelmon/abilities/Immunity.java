/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.PreventStatus;

public class Immunity
extends PreventStatus {
    public Immunity() {
        super("pixelmon.abilities.immunity", "pixelmon.abilities.immunitycure", StatusType.Poison, StatusType.PoisonBadly);
    }
}

