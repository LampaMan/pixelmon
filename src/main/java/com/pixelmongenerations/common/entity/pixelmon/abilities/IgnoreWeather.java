/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public abstract class IgnoreWeather
extends AbilityBase {
    private String switchInMessage;

    public IgnoreWeather(String switchInMessage) {
        this.switchInMessage = switchInMessage;
    }

    @Override
    public boolean ignoreWeather() {
        return true;
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        newPokemon.bc.sendToAll(this.switchInMessage, newPokemon.getNickname());
        newPokemon.bc.globalStatusController.triggerWeatherChange(null);
    }

    @Override
    public void applySwitchOutEffect(PixelmonWrapper oldPokemon) {
        for (PixelmonWrapper pw : oldPokemon.bc.getActiveUnfaintedPokemon()) {
            if (pw == oldPokemon || !pw.getBattleAbility().ignoreWeather()) continue;
            return;
        }
        oldPokemon.bc.globalStatusController.triggerWeatherChange(oldPokemon.bc.globalStatusController.getWeather());
    }
}

