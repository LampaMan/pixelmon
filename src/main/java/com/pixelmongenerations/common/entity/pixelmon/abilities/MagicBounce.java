/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.MagicCoat;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class MagicBounce
extends AbilityBase {
    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        if (AbilityBase.ignoreAbility(user, pokemon)) {
            return true;
        }
        return !MagicCoat.reflectMove(a, pokemon, user, "pixelmon.abilities.magicbounce");
    }
}

