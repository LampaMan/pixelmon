/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.moves;

import com.pixelmongenerations.common.entity.pixelmon.stats.moves.MoveLearnContainer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayer;

public class LearnMoveStorage {
    private HashMap<UUID, ArrayList<MoveLearnContainer>> storage;
    private static LearnMoveStorage INSTANCE;

    public static LearnMoveStorage getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new LearnMoveStorage();
            LearnMoveStorage.INSTANCE.storage = new HashMap();
        }
        return INSTANCE;
    }

    public void addPlayerMove(UUID playerId, int[] pokemonId, int attackIndex) {
        ArrayList<MoveLearnContainer> moves = this.storage.get(playerId);
        if (moves == null) {
            moves = new ArrayList();
        }
        moves.add(new MoveLearnContainer(pokemonId, attackIndex));
        this.storage.put(playerId, moves);
    }

    public boolean hasMove(EntityPlayer player, int[] pokemonId, int attackIndex) {
        UUID uniqueId = player.getUniqueID();
        ArrayList<MoveLearnContainer> moves = this.storage.get(uniqueId);
        if (moves == null) {
            return false;
        }
        boolean result = moves.removeIf(moveContainer -> moveContainer.isMoveLearn(pokemonId, attackIndex));
        if (moves.isEmpty()) {
            this.storage.remove(uniqueId);
        } else {
            this.storage.put(uniqueId, moves);
        }
        return result;
    }
}

