/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.PropellerTail;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Stalwart;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.EnumType;

public abstract class Redirect
extends AbilityBase {
    EnumType type;
    String langImmune;
    String langRedirect;
    private boolean propellertail = false;

    public Redirect(EnumType type, String langImmune, String langRedirect) {
        this.type = type;
        this.langImmune = langImmune;
        this.langRedirect = langRedirect;
    }

    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        if (user.getAbility() instanceof PropellerTail || user.getAbility() instanceof Stalwart) {
            user.bc.sendToAll("pixelmon.status.propellertail", user.getNickname());
            this.propellertail = true;
            return true;
        }
        if (a.getAttackBase().attackType == this.type) {
            if (a.getTypeEffectiveness(user, pokemon) == 0.0) {
                pokemon.bc.sendToAll("pixelmon.battletext.noeffect", pokemon.getNickname());
                return false;
            }
            pokemon.bc.sendToAll(this.langImmune, pokemon.getNickname());
            if (pokemon.getBattleStats().modifyStat(1, StatsType.SpecialAttack)) {
                a.moveResult.weightMod -= 25.0f;
            }
            return false;
        }
        return true;
    }

    @Override
    public boolean redirectAttack(PixelmonWrapper user, PixelmonWrapper targetAlly, Attack attack) {
        if (attack.getAttackBase().attackType == this.type) {
            if (targetAlly.isFainted()) {
                return false;
            }
            if (this.propellertail) {
                return false;
            }
            targetAlly.bc.sendToAll(this.langRedirect, targetAlly.getNickname());
            return true;
        }
        return false;
    }
}

