/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.entity.pixelmon.abilities.LowHPTypeBoost;
import com.pixelmongenerations.core.enums.EnumType;

public class Torrent
extends LowHPTypeBoost {
    public Torrent() {
        super(EnumType.Water);
    }
}

