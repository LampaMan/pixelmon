/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.drops;

import com.pixelmongenerations.common.entity.pixelmon.drops.DroppedItem;
import java.util.ArrayList;
import java.util.UUID;
import net.minecraft.util.math.Vec3d;

public class DropItemQuery {
    public Vec3d position;
    public UUID playerUUID;
    public ArrayList<DroppedItem> droppedItemList;

    public DropItemQuery(Vec3d position, UUID playerUUID, ArrayList<DroppedItem> droppedItemList) {
        this.position = position;
        this.playerUUID = playerUUID;
        this.droppedItemList = droppedItemList;
    }
}

