/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.entity.pokeballs.EntityPokeBall;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.enums.EnumNature;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Matcher;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.passive.EntityTameable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;

public abstract class Entity1Base
extends EntityTameable {
    private EnumSpecies species;
    public EnumPokeball caughtBall;
    public EntityPokeBall hitByPokeball = null;
    @Deprecated
    public Gender gender;
    public boolean isInBall = false;
    public boolean isFainted = false;
    public boolean isInitialised = false;
    public String originalTrainer = "";
    public String originalTrainerUUID = "";
    public boolean canDespawn = true;
    protected int entityAge = 0;
    protected EnumBossMode serverBossMode;

    public Entity1Base(World par1World) {
        super(par1World);
        this.dataManager.register(EntityPixelmon.dwName, "");
        this.dataManager.register(EntityPixelmon.dwNickname, "");
        this.dataManager.register(EntityPixelmon.dwPokemonID1, -1);
        this.dataManager.register(EntityPixelmon.dwPokemonID2, -1);
        this.dataManager.register(EntityPixelmon.dwBossMode, -1);
        this.dataManager.register(EntityPixelmon.dwNature, -1);
        this.dataManager.register(EntityPixelmon.dwPseudoNature, -1);
        this.dataManager.register(EntityPixelmon.dwGrowth, -1);
        this.dataManager.register(EntityPixelmon.dwSpawnLocation, -1);
    }

    protected void init(String name) {
        this.setName(name);
        if (this.getBossMode() == null) {
            this.setBoss(EnumBossMode.NotBoss);
        }
        if (this.getNature() == null) {
            this.setNature(EnumNature.getRandomNature());
        }
        if (this.getGrowth() == null) {
            this.setGrowth(EnumGrowth.getRandomGrowth());
        }
        if (this.getPseudoNature() == null || !this.getEntityData().getBoolean("Minted")) {
            this.setPseudoNature(this.getNature());
        }
    }

    public EnumSpecies getSpecies() {
        return this.species;
    }

    public boolean isPokemon(EnumSpecies ... species) {
        return this.species.isPokemon(species);
    }

    public EnumBossMode getBossMode() {
        if (!this.world.isRemote) {
            return this.serverBossMode;
        }
        return EnumBossMode.getMode(this.dataManager.get(EntityPixelmon.dwBossMode));
    }

    public void setBoss(EnumBossMode mode) {
        this.serverBossMode = mode;
        this.dataManager.set(EntityPixelmon.dwBossMode, mode.index);
    }

    public boolean isBossPokemon() {
        return this.getBossMode().isBossPokemon();
    }

    public boolean isLegendary() {
        return this.species.isLegendary();
    }

    public int[] getPokemonId() {
        return new int[]{this.dataManager.get(EntityPixelmon.dwPokemonID1), this.dataManager.get(EntityPixelmon.dwPokemonID2)};
    }

    public void setPokemonId(int[] id) {
        this.dataManager.set(EntityPixelmon.dwPokemonID1, id[0]);
        this.dataManager.set(EntityPixelmon.dwPokemonID2, id[1]);
    }

    public EnumNature getNature() {
        return EnumNature.getNatureFromIndex(this.dataManager.get(EntityPixelmon.dwNature));
    }

    public EnumNature getPseudoNature() {
        return EnumNature.getNatureFromIndex(this.dataManager.get(EntityPixelmon.dwPseudoNature));
    }

    public void setNature(EnumNature nature) {
        if (nature != null) {
            this.dataManager.set(EntityPixelmon.dwNature, nature.index);
            this.dataManager.set(EntityPixelmon.dwPseudoNature, nature.index);
            this.getEntityData().setBoolean("Minted", false);
        }
    }

    public void setPseudoNature(EnumNature pseudonature) {
        if (pseudonature != null) {
            this.dataManager.set(EntityPixelmon.dwPseudoNature, pseudonature.index);
        }
    }

    public EnumGrowth getGrowth() {
        return EnumGrowth.getGrowthFromIndex(this.dataManager.get(EntityPixelmon.dwGrowth));
    }

    public void setGrowth(EnumGrowth growth) {
        this.dataManager.set(EntityPixelmon.dwGrowth, growth.index);
    }

    public void setGender(Gender gender) {
        this.gender = gender;
        this.dataManager.set(EntityPixelmon.dwGender, gender.ordinal());
    }

    public Gender getGender() {
        int gender = this.dataManager.get(EntityPixelmon.dwGender);
        return Gender.getGender((short)gender);
    }

    public int getPokeRus() {
        int pokerus = this.dataManager.get(EntityPixelmon.dwPokeRus);
        return pokerus;
    }

    public void setPokeRus(int pokerus) {
        this.dataManager.set(EntityPixelmon.dwPokeRus, pokerus);
    }

    public int getPokeRusTimer() {
        int pokerusTimer = this.dataManager.get(EntityPixelmon.dwPokeRusTimer);
        return pokerusTimer;
    }

    public void setPokeRusTimer(int pokerusTimer) {
        this.dataManager.set(EntityPixelmon.dwPokeRusTimer, pokerusTimer);
    }

    public SpawnLocation getSpawnLocation() {
        return SpawnLocation.getFromIndex(this.dataManager.get(EntityPixelmon.dwSpawnLocation));
    }

    public void setSpawnLocation(SpawnLocation spawnLocation) {
        if (spawnLocation == null) {
            spawnLocation = SpawnLocation.Land;
        }
        this.dataManager.set(EntityPixelmon.dwSpawnLocation, spawnLocation.ordinal());
    }

    public boolean hasOwner() {
        return this.getOwnerId() != null;
    }

    public boolean belongsTo(EntityPlayer player) {
        return this.getOwner() == player;
    }

    public void update(EnumUpdateType ... types) {
        Optional<PlayerStorage> storage;
        if (this.isInitialised && (storage = this.getStorage()).isPresent()) {
            storage.get().updateAndSendToClient((EntityPixelmon)this, types);
        }
    }

    public Optional<PlayerStorage> getStorage() {
        EntityPlayerMP owner = PixelmonStorage.pokeBallManager.getPlayerFromUUID(this.world.getMinecraftServer(), this.getOwnerId());
        return PixelmonStorage.pokeBallManager.getPlayerStorage(owner);
    }

    @Override
    public String getName() {
        return this.getLocalizedName();
    }

    @Override
    public boolean hasCustomName() {
        return false;
    }

    public void setName(String name) {
        this.dataManager.set(EntityPixelmon.dwName, name);
        this.species = EnumSpecies.getFromName(name).orElse(null);
    }

    public String getPokemonName() {
        return this.dataManager.get(EntityPixelmon.dwName);
    }

    public String getNickname() {
        String nickname = this.dataManager.get(EntityPixelmon.dwNickname);
        if (!PixelmonConfig.allowNicknames || nickname.isEmpty()) {
            return this.getLocalizedName();
        }
        return nickname;
    }

    public String getEscapedNickname() {
        return Matcher.quoteReplacement(this.getNickname());
    }

    public boolean hasNickname() {
        return !this.dataManager.get(EntityPixelmon.dwNickname).equals("");
    }

    public void setNickname(String nickname) {
        this.dataManager.set(EntityPixelmon.dwNickname, nickname);
    }

    public String getLocalizedName() {
        return Entity1Base.getLocalizedName(this.getPokemonName());
    }

    public String getLocalizedDescription() {
        return Entity1Base.getLocalizedDescription(this.getPokemonName());
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox() {
        return this.getEntityBoundingBox();
    }

    @Override
    public AxisAlignedBB getCollisionBox(Entity par1Entity) {
        return null;
    }

    @Override
    public void addVelocity(double par1, double par3, double par5) {
        if (this.canBePushed()) {
            super.addVelocity(par1, par3, par5);
        }
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.55);
    }

    @Override
    public boolean isEntityInsideOpaqueBlock() {
        if (super.isEntityInsideOpaqueBlock()) {
            if (this.world.getBlockState(this.getPosition().add(1, 0, 0)).getBlock().getMaterial(this.world.getBlockState(this.getPosition().add(1, 0, 0))) == Material.AIR) {
                this.setPosition(this.posX + 1.0, this.posY, this.posZ);
            }
            if (this.world.getBlockState(this.getPosition().add(-1, 0, 0)).getBlock().getMaterial(this.world.getBlockState(this.getPosition().add(-1, 0, 0))) == Material.AIR) {
                this.setPosition(this.posX - 1.0, this.posY, this.posZ);
            }
            if (this.world.getBlockState(this.getPosition().add(0, 1, 0)).getBlock().getMaterial(this.world.getBlockState(this.getPosition().add(0, 1, 0))) == Material.AIR) {
                this.setPosition(this.posX, this.posY + 1.0, this.posZ);
            }
            if (this.world.getBlockState(this.getPosition().add(0, -1, 0)).getBlock().getMaterial(this.world.getBlockState(this.getPosition().add(0, -1, 0))) == Material.AIR) {
                this.setPosition(this.posX, this.posY - 1.0, this.posZ);
            }
            if (this.world.getBlockState(this.getPosition().add(0, 0, 1)).getBlock().getMaterial(this.world.getBlockState(this.getPosition().add(0, 0, 1))) == Material.AIR) {
                this.setPosition(this.posX, this.posY, this.posZ + 1.0);
            }
            if (this.world.getBlockState(this.getPosition().add(0, 0, -1)).getBlock().getMaterial(this.world.getBlockState(this.getPosition().add(0, 0, -1))) == Material.AIR) {
                this.setPosition(this.posX, this.posY, this.posZ - 1.0);
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean canBePushed() {
        return true;
    }

    @Override
    public int getGrowingAge() {
        return 0;
    }

    @Override
    protected boolean canDespawn() {
        return true;
    }

    @Override
    public void setDead() {
        Optional<PlayerStorage> storage;
        if (this.getOwner() != null && !this.world.isRemote && (storage = this.getStorage()).isPresent()) {
            storage.get().retrieve((EntityPixelmon)this);
        }
        super.setDead();
    }

    @Override
    public void onUpdate() {
        ++this.entityAge;
        super.onUpdate();
        if (!this.isInitialised) {
            this.init(this.getPokemonName());
            this.isInitialised = true;
        }
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound nbt) {
        super.writeEntityToNBT(nbt);
        nbt.setInteger("pixelmonID1", this.dataManager.get(EntityPixelmon.dwPokemonID1));
        nbt.setInteger("pixelmonID2", this.dataManager.get(EntityPixelmon.dwPokemonID2));
        nbt.setString("Name", this.dataManager.get(EntityPixelmon.dwName));
        nbt.setString("Nickname", this.dataManager.get(EntityPixelmon.dwNickname));
        if (this.caughtBall != null) {
            nbt.setInteger("CaughtBall", this.caughtBall.ordinal());
        }
        nbt.setShort("Gender", (short)this.getGender().ordinal());
        nbt.setInteger("PokeRus", this.getPokeRus());
        nbt.setInteger("PokeRusTimer", this.getPokeRusTimer());
        nbt.setBoolean("IsInBall", this.isInBall);
        nbt.setBoolean("IsFainted", this.isFainted);
        nbt.setShort("BossMode", (short)this.dataManager.get(EntityPixelmon.dwBossMode).intValue());
        nbt.setShort("Nature", (short)this.dataManager.get(EntityPixelmon.dwNature).intValue());
        nbt.setShort("PseudoNature", (short)this.dataManager.get(EntityPixelmon.dwPseudoNature).intValue());
        nbt.setShort("Growth", (short)this.dataManager.get(EntityPixelmon.dwGrowth).intValue());
        nbt.setString("originalTrainer", this.originalTrainer);
        nbt.setString("originalTrainerUUID", this.originalTrainerUUID);
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        super.readEntityFromNBT(nbt);
        if (!nbt.hasKey("pixelmonID1")) {
            if (this.getOwner() != null) {
                this.onKillCommand();
            }
            nbt.setInteger("pixelmonID1", -1);
            nbt.setInteger("pixelmonID2", -1);
        }
        this.dataManager.set(EntityPixelmon.dwPokemonID1, nbt.getInteger("pixelmonID1"));
        this.dataManager.set(EntityPixelmon.dwPokemonID2, nbt.getInteger("pixelmonID2"));
        this.setName(nbt.getString("Name"));
        if (!this.isInitialised) {
            this.init(this.getPokemonName());
        }
        this.isInitialised = true;
        if (nbt.hasKey("Nickname")) {
            this.dataManager.set(EntityPixelmon.dwNickname, nbt.getString("Nickname"));
        }
        if (nbt.hasKey("CaughtBall")) {
            this.caughtBall = EnumPokeball.getFromIndex(nbt.getInteger("CaughtBall"));
        }
        if (nbt.hasKey("IsMale")) {
            this.setGender(nbt.getBoolean("IsMale") ? Gender.Male : Gender.Female);
        }
        this.setGender(Gender.getGender(nbt.getShort("Gender")));
        if (this instanceof EntityPixelmon && ((EntityPixelmon)this).baseStats.malePercent < 0) {
            this.setGender(Gender.None);
        }
        if (nbt.hasKey("PokeRus")) {
            this.setPokeRus(nbt.getInteger("PokeRus"));
        } else {
            this.setPokeRus(0);
        }
        if (nbt.hasKey("PokeRusTimer")) {
            this.setPokeRusTimer(nbt.getInteger("PokeRusTimer"));
        } else {
            this.setPokeRusTimer(0);
        }
        this.isInBall = nbt.getBoolean("IsInBall");
        this.isFainted = nbt.getBoolean("IsFainted");
        if (nbt.hasKey("BossMode")) {
            this.setBoss(EnumBossMode.getMode(nbt.getShort("BossMode")));
        } else {
            this.setBoss(EnumBossMode.NotBoss);
        }
        if (nbt.hasKey("Nature")) {
            this.dataManager.set(EntityPixelmon.dwNature, Integer.valueOf(nbt.getShort("Nature")));
        } else {
            this.setNature(EnumNature.getRandomNature());
        }
        if (nbt.hasKey("PseudoNature")) {
            this.dataManager.set(EntityPixelmon.dwPseudoNature, Integer.valueOf(nbt.getShort("PseudoNature")));
        } else {
            this.dataManager.set(EntityPixelmon.dwNature, Integer.valueOf(nbt.getShort("Nature")));
        }
        if (nbt.hasKey("Growth")) {
            this.dataManager.set(EntityPixelmon.dwGrowth, Integer.valueOf(nbt.getShort("Growth")));
        } else {
            this.setGrowth(EnumGrowth.Ordinary);
        }
        this.originalTrainer = nbt.hasKey("originalTrainer") ? nbt.getString("originalTrainer") : (this.hasOwner() ? this.getOwner().getDisplayName().getFormattedText() : "Statue");
        this.originalTrainerUUID = nbt.hasKey("originalTrainerUUID") ? nbt.getString("originalTrainerUUID") : (this.hasOwner() && this.getOwner() != null ? this.getOwner().getUniqueID().toString() : UUID.randomUUID().toString());
    }

    public void getNBTTags(HashMap<String, Class> tags) {
        tags.put("pixelmonID1", Integer.class);
        tags.put("pixelmonID2", Integer.class);
        tags.put("Name", String.class);
        tags.put("Nickname", String.class);
        tags.put("CaughtBall", Integer.class);
        tags.put("Gender", Short.class);
        tags.put("PokeRus", Integer.class);
        tags.put("PokeRusTimer", Integer.class);
        tags.put("IsFainted", Boolean.class);
        tags.put("BossMode", Short.class);
        tags.put("Nature", Short.class);
        tags.put("PseudoNature", Short.class);
        tags.put("Growth", Short.class);
        tags.put("originalTrainer", String.class);
        tags.put("PixelmonOrder", Integer.class);
        tags.put("BoxNumber", Integer.class);
        tags.put("originalTrainerUUID", String.class);
    }

    public static String getLocalizedDescription(String name) {
        return I18n.translateToLocal("pixelmon." + name.toLowerCase() + ".description");
    }

    public static String getLocalizedName(String name) {
        return I18n.translateToLocal("pixelmon." + name.toLowerCase() + ".name");
    }

    public static String getLocalizedName(EnumSpecies pokemon) {
        return Entity1Base.getLocalizedName(pokemon.name);
    }
}

