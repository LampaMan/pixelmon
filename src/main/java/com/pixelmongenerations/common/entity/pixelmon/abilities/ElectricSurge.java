/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.status.ElectricTerrain;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Surge;

public class ElectricSurge
extends Surge {
    public ElectricSurge() {
        super(new ElectricTerrain());
    }
}

