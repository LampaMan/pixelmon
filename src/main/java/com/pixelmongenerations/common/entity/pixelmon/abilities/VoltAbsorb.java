/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.entity.pixelmon.abilities.Absorb;
import com.pixelmongenerations.core.enums.EnumType;

public class VoltAbsorb
extends Absorb {
    public VoltAbsorb() {
        super(EnumType.Electric, "pixelmon.abilities.voltabsorb");
    }
}

