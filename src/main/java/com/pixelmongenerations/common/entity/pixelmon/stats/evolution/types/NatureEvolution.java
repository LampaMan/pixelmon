/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types;

import com.pixelmongenerations.api.def.NatureDef;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.Evolution;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.ArrayList;

public class NatureEvolution
extends Evolution {
    public int level = 1;
    public ArrayList<NatureDef> natureEvolve;

    public NatureEvolution(EnumSpecies from, PokemonSpec to, int level, ArrayList<NatureDef> natureEvolve, EvoCondition ... conditions) {
        super(from, to, conditions);
        this.level = level;
        this.natureEvolve = natureEvolve;
    }

    public boolean canEvolve(EntityPixelmon pokemon, int level) {
        return level >= this.level && super.canEvolve(pokemon);
    }

    @Override
    public boolean doEvolution(EntityPixelmon pokemon) {
        for (NatureDef def : this.natureEvolve) {
            if (!def.natures.contains((Object)pokemon.getNature())) continue;
            this.setForm(def.form);
            break;
        }
        return super.doEvolution(pokemon);
    }
}

