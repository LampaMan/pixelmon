/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class QuickDraw
extends AbilityBase {
    @Override
    public float modifyPriority(PixelmonWrapper pokemon, float priority) {
        if (RandomHelper.getRandomChance(30)) {
            pokemon.bc.sendToAll("pixelmon.abilities.quickdraw", pokemon.getNickname());
            priority += 0.2f;
        }
        return priority;
    }
}

