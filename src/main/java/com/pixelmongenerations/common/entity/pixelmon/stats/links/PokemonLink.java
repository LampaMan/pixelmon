/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.links;

import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusPersist;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.npcs.registry.PokemonForm;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.ExtraStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.FriendShip;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.entity.pixelmon.stats.Level;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.Stats;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.enums.EnumMark;
import com.pixelmongenerations.core.enums.EnumNature;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.PixelmonStatsData;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.List;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public abstract class PokemonLink {
    public abstract BaseStats getBaseStats();

    public abstract Stats getStats();

    public abstract void setStats(Stats var1);

    public abstract ItemHeld getHeldItem();

    public abstract void setHeldItem(ItemStack var1);

    public abstract int getHealth();

    public abstract int getMaxHealth();

    public abstract void setHealth(int var1);

    public void setHealthDirect(int health) {
        this.setHealth(health);
    }

    public abstract int getLevel();

    public abstract void setLevel(int var1);

    public abstract int getDynamaxLevel();

    public abstract void setDynamaxLevel(int var1);

    public abstract int getExp();

    public abstract void setExp(int var1);

    public abstract FriendShip getFriendship();

    public abstract boolean doesLevel();

    public String getRealNickname() {
        return this.getNickname();
    }

    public abstract EntityPlayerMP getPlayerOwner();

    public abstract BattleControllerBase getBattleController();

    public abstract Moveset getMoveset();

    public abstract int[] getPokemonID();

    public abstract EntityPixelmon getEntity();

    public abstract void setScale(float var1);

    public abstract World getWorld();

    public abstract Gender getGender();

    public abstract void setGender(Gender var1);

    public abstract BlockPos getPos();

    public abstract Optional<PlayerStorage> getStorage();

    public abstract void update(EnumUpdateType ... var1);

    public abstract void updateStats();

    public abstract void updateLevelUp(PixelmonStatsData var1);

    public abstract void sendMessage(String var1, Object ... var2);

    public abstract String getNickname();

    public abstract String getOriginalTrainer();

    public abstract String getOriginalTrainerUUID();

    public abstract String getRealTextureNoCheck(PixelmonWrapper var1);

    public abstract boolean removeStatuses(StatusType ... var1);

    public abstract EnumNature getNature();

    public abstract void setNature(EnumNature var1);

    public abstract EnumNature getPseudoNature();

    public abstract void setPseudoNature(EnumNature var1);

    public abstract int getPokeRus();

    public abstract boolean hasGmaxFactor();

    public abstract int getExpToNextLevel();

    public abstract StatusPersist getPrimaryStatus();

    public abstract AbilityBase getAbility();

    public abstract void setAbility(String var1);

    public abstract List<EnumType> getType();

    public abstract int getForm();

    public abstract IEnumForm getFormEnum();

    public abstract void setForm(int var1);

    public abstract void setForm(IEnumForm var1);

    public boolean isFainted() {
        return this.getHealth() <= 0;
    }

    public abstract boolean isShiny();

    public abstract void setShiny(boolean var1);

    public abstract boolean isEgg();

    public abstract int getEggCycles();

    public abstract EnumGrowth getGrowth();

    public abstract void setGrowth(EnumGrowth var1);

    public abstract EnumPokeball getCaughtBall();

    public abstract void setCaughtBall(EnumPokeball var1);

    public abstract int getPartyPosition();

    public abstract ItemStack getHeldItemStack();

    public abstract boolean hasOwner();

    public abstract EnumBossMode getBossMode();

    public abstract void setBossMode(EnumBossMode var1);

    public abstract int getSpecialTexture();

    public abstract void setSpecialTexture(int var1);

    public abstract boolean isInRanch();

    public abstract int[] getEggMoves();

    public abstract Level getLevelContainer();

    public abstract NBTTagCompound getNBT();

    public abstract boolean isTotem();

    public abstract void setTotem(boolean var1);

    public EnumSpecies getSpecies() {
        BaseStats stats = this.getBaseStats();
        return stats == null ? EnumSpecies.Bulbasaur : stats.pokemon;
    }

    public PokemonForm getPokemonForm() {
        return new PokemonForm(this.getSpecies(), this.getForm());
    }

    public String toString() {
        return this.getNickname();
    }

    public int getCatchRate() {
        return this.getBaseStats().catchRate;
    }

    public abstract String getCustomTexture();

    public abstract void setCustomTexture(String var1);

    public abstract EnumMark getMark();

    public abstract void setMark(EnumMark var1);

    public abstract ExtraStats getExtraStats();

    public abstract void setParticleId(String var1);
}

