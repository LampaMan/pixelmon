/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.TerrainExamine;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.status.NoStatus;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusPersist;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.pixelmon.Entity5Rideable;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.EntityLink;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumBurmy;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.packetHandlers.battles.gui.StatusPacket;
import java.util.ArrayList;
import java.util.HashMap;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;

public abstract class Entity6CanBattle
extends Entity5Rideable {
    public StatusPersist status = NoStatus.noStatus;
    private Moveset moveset = new Moveset(this);
    public BattleControllerBase battleController;
    protected NPCTrainer trainer;
    private PixelmonWrapper pixelmonWrapper;
    public boolean hasNPCTrainer;

    public Entity6CanBattle(World par1World) {
        super(par1World);
        this.dataManager.register(EntityPixelmon.dwTrainerName, "");
    }

    @Override
    public boolean canDespawn() {
        if (this.battleController != null) {
            return false;
        }
        return super.canDespawn();
    }

    public void loadMoveset() {
        this.moveset = Moveset.loadMoveset(new EntityLink(this));
    }

    public void loadMoveset(String ... moves) {
        this.loadMoveset();
        int j = 0;
        for (String move : moves) {
            Attack attack = DatabaseMoves.getAttack(move);
            if (this.moveset.add(attack)) continue;
            this.moveset.set(j++, attack);
        }
    }

    public void setMoveset(Moveset moveset) {
        this.moveset = moveset;
    }

    public Moveset getMoveset() {
        return this.moveset;
    }

    public void endBattle() {
        if (this.baseStats.pokemon == EnumSpecies.Burmy) {
            this.setForm(EnumBurmy.getFromType(TerrainExamine.getTerrain(this.pixelmonWrapper)).ordinal());
        }
        this.battleController = null;
        this.pixelmonWrapper = null;
    }

    public void setTrainer(NPCTrainer trainer) {
        this.trainer = trainer;
        this.dataManager.set(EntityPixelmon.dwTrainerName, trainer.getName());
    }

    public NPCTrainer getTrainer() {
        return this.trainer;
    }

    public String getTrainerName() {
        return this.dataManager.get(EntityPixelmon.dwTrainerName);
    }

    @Override
    public boolean attackEntityFrom(DamageSource source, float amount) {
        if (!this.world.isRemote) {
            if (this.getBossMode() != EnumBossMode.NotBoss && source.damageType.equals("mob") && !(source.getImmediateSource() instanceof EntityPixelmon)) {
                return false;
            }
            if (source.damageType.equals("player") || source.damageType.equals("arrow")) {
                if (!PixelmonConfig.canPokemonBeHit) {
                    return false;
                }
                amount *= 3.0f;
            }
            if (!(this.battleController == null && !this.isEvolving() || source != DamageSource.CACTUS && source != DamageSource.DROWN && source != DamageSource.FALL && source != DamageSource.IN_FIRE && source != DamageSource.IN_WALL && source != DamageSource.LAVA && source != DamageSource.ON_FIRE && source != DamageSource.FALLING_BLOCK && source != DamageSource.ANVIL)) {
                return false;
            }
            boolean flag = super.attackEntityFrom(source, amount);
            if (this.battleController == null) {
                this.updateHealth();
            }
            if (this.getHealth() <= 0.0f) {
                this.onDeath(source);
            }
            Entity entity = source.getImmediateSource();
            if (this.battleController == null && this.getOwner() != null) {
                this.update(EnumUpdateType.HP);
            }
            if (this.isValidTarget(entity)) {
                this.setAttackTarget((EntityLiving)entity);
            }
            return flag;
        }
        return false;
    }

    public ArrayList<Attack> getAttacksAtLevel(int level) {
        return DatabaseMoves.getAttacksAtLevel(this, level);
    }

    public boolean learnsAttackAtLevel(int level) {
        return DatabaseMoves.learnsAttackAtLevel(this, level);
    }

    protected boolean isValidTarget(Entity entity) {
        return entity instanceof EntityPixelmon;
    }

    public boolean hasStatus() {
        return this.status != NoStatus.noStatus;
    }

    public boolean removeStatus(StatusType s) {
        if (this.status.type == s) {
            EntityLivingBase owner = this.getOwner();
            if (owner != null) {
                ChatHandler.sendChat(owner, this.status.getCureMessage(), this.getNickname());
            }
            this.clearStatus();
            return true;
        }
        return false;
    }

    public boolean removeStatuses(StatusType ... statuses) {
        for (StatusType status : statuses) {
            if (!this.removeStatus(status)) continue;
            return true;
        }
        return false;
    }

    public boolean hasStatus(StatusType ... statuses) {
        for (StatusType status : statuses) {
            if (status != this.status.type) continue;
            return true;
        }
        return false;
    }

    public void clearStatus() {
        this.status = NoStatus.noStatus;
    }

    public void removeStatus(StatusBase e) {
        if (this.status.type == e.type) {
            this.clearStatus();
        }
    }

    public void sendStatusPacket(int statusID) {
        if (this.battleController != null && !this.battleController.simulateMode) {
            int[] pokemonID = this.getPokemonId();
            this.battleController.participants.stream().filter(p -> p.getType() == ParticipantType.Player).forEach(p -> Pixelmon.NETWORK.sendTo(new StatusPacket(pokemonID, statusID), ((PlayerParticipant)p).player));
            this.battleController.spectators.forEach(spectator -> spectator.sendMessage(new StatusPacket(pokemonID, statusID)));
        }
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound nbt) {
        super.writeEntityToNBT(nbt);
        this.moveset.writeToNBT(nbt);
        if (nbt.hasKey("StatusCount")) {
            int numStatuses = nbt.getShort("StatusCount");
            for (int i = 0; i < numStatuses; ++i) {
                nbt.removeTag("Status" + i);
            }
            nbt.removeTag("StatusCount");
        }
        if (this.hasStatus()) {
            this.status.writeToNBT(nbt);
        } else {
            nbt.removeTag("Status");
        }
        if (this.trainer != null) {
            nbt.setBoolean("HasNPCTrainer", true);
        }
    }

    @Override
    public void getNBTTags(HashMap<String, Class> tags) {
        super.getNBTTags(tags);
        this.moveset.getNBTTags(tags);
        tags.put("Status", Integer.class);
        tags.put("StatusCount", Integer.class);
        tags.put("StatusSleepTurns", Integer.class);
        tags.put("HasNPCTrainer", Boolean.class);
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        super.readEntityFromNBT(nbt);
        this.moveset.readFromNBT(nbt);
        this.status = StatusPersist.readStatusFromNBT(nbt);
        if (nbt.hasKey("HasNPCTrainer")) {
            this.hasNPCTrainer = nbt.getBoolean("HasNPCTrainer");
        }
    }

    @Override
    public void setHealth(float par1) {
        super.setHealth(par1);
        if (this.battleController != null) {
            this.battleController.updatePokemonHealth((EntityPixelmon)this);
        }
    }

    public BattleParticipant getParticipant() {
        if (this.battleController == null) {
            return null;
        }
        for (BattleParticipant p : this.battleController.participants) {
            for (PixelmonWrapper pw : p.controlledPokemon) {
                if (pw.pokemon != this) continue;
                return p;
            }
        }
        return null;
    }

    public PixelmonWrapper getPixelmonWrapper() {
        if (this.battleController == null) {
            return null;
        }
        if (this.pixelmonWrapper != null && this.pixelmonWrapper.bc != null) {
            return this.pixelmonWrapper;
        }
        for (BattleParticipant bp : this.battleController.participants) {
            if (bp.controlledPokemon == null) continue;
            for (PixelmonWrapper pw : bp.controlledPokemon) {
                if (pw.pokemon != this) continue;
                this.pixelmonWrapper = pw;
                return pw;
            }
        }
        return null;
    }

    public void setPixelmonWrapper(PixelmonWrapper newWrapper) {
        this.pixelmonWrapper = newWrapper;
    }

    public int getPercentMaxHealth(float percent) {
        return Entity6CanBattle.getPercentMaxHealth(percent, this.getMaxHealth());
    }

    public static int getPercentMaxHealth(float percent, float maxHealth) {
        return Math.max(1, (int)(maxHealth * percent / 100.0f));
    }

    public boolean hasFullHealth() {
        return this.getHealth() >= this.getMaxHealth();
    }

    public void healEntityBy(int i) {
        if ((float)i + this.getHealth() > this.getMaxHealth()) {
            i = (int)(this.getMaxHealth() - this.getHealth());
        }
        if (i != 0) {
            this.setHealth(this.getHealth() + (float)i);
        }
    }

    public void healByPercent(float percent) {
        this.healEntityBy(this.getPercentMaxHealth(percent));
    }

    @Override
    public int getPartyPosition() {
        PixelmonWrapper pw;
        int partyPosition = super.getPartyPosition();
        if (partyPosition == -1 && (pw = this.getPixelmonWrapper()) != null) {
            partyPosition = pw.getPartyPosition();
        }
        return partyPosition;
    }
}

