/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class Dancer
extends AbilityBase {
    boolean faster = false;

    @Override
    public void startMove(PixelmonWrapper user) {
        PixelmonWrapper target = user.bc.getOppositePokemon(user);
        for (int i = user.bc.turn + 1; i < user.bc.turnList.size(); ++i) {
            if (user.bc.turnList.get(i) != target) continue;
            this.faster = true;
            return;
        }
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        PixelmonWrapper target = pokemon.bc.getOppositePokemon(pokemon);
        if (this.faster && target.lastAttack != null) {
            if (target.attack.isAttack("Feather Dance", "Fiery Dance", "Dragon Dance", "Lunar Dance", "Petal Dance", "Revelation Dance", "Quiver Dance", "Swords Dance", "Teeter Dance") && !pokemon.attack.equals(pokemon.choiceLocked)) {
                if (!pokemon.hasStatus(StatusType.Taunt) && pokemon.getHeldItem().getHeldItemType() != EnumHeldItems.assaultVest) {
                    pokemon.bc.sendToAll("pixelmon.abilities.dancercopy", pokemon.getNickname());
                    pokemon.useTempAttack(target.attack, false);
                    if (target.attack.isAttack("Petal Dance")) {
                        pokemon.removeStatus(StatusType.Charge);
                        pokemon.removeStatus(StatusType.Confusion);
                        pokemon.attack = null;
                    }
                    return;
                }
            }
        }
    }

    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        PixelmonWrapper target = user.bc.getOppositePokemon(user);
        if (!this.faster && target.lastAttack != null) {
            if (target.attack.isAttack("Feather Dance", "Fiery Dance", "Dragon Dance", "Lunar Dance", "Petal Dance", "Revelation Dance", "Quiver Dance", "Swords Dance", "Teeter Dance") && !user.attack.equals(user.choiceLocked)) {
                if (!user.hasStatus(StatusType.Taunt) && user.getHeldItem().getHeldItemType() != EnumHeldItems.assaultVest) {
                    user.bc.sendToAll("pixelmon.abilities.dancercopy", user.getNickname());
                    if (target.attack.isAttack("Petal Dance")) {
                        Attack chosenAttack = user.attack;
                        user.useTempAttack(target.attack, false);
                        user.removeStatus(StatusType.MultiTurn);
                        user.removeStatus(StatusType.Confusion);
                        target.lastAttack = null;
                        user.attack = chosenAttack;
                    } else if (!this.faster && target.lastAttack != null) {
                        Attack chosenAttack = user.attack;
                        Attack copiedAttack = target.lastAttack;
                        user.useTempAttack(copiedAttack, false);
                        user.useTempAttack(chosenAttack, false);
                        target.lastAttack = null;
                    }
                }
            }
        }
        return stats;
    }
}

