/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MirrorArmor;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class PreventStatDrop
extends AbilityBase {
    private String langKey;

    public PreventStatDrop(String langKey) {
        this.langKey = langKey;
    }

    @Override
    public boolean allowsStatChange(PixelmonWrapper pokemon, PixelmonWrapper user, StatsEffect e) {
        if (e.getUser() || e.value > 0) {
            return true;
        }
        if (pokemon.getBattleAbility() instanceof MirrorArmor) {
            if (e.getStatsType() == StatsType.Attack) {
                user.bc.sendToAll(this.langKey, pokemon.getNickname());
                user.getBattleStats().modifyStat(-1, StatsType.Attack, user, true, true);
            }
            if (e.getStatsType() == StatsType.Defence) {
                user.bc.sendToAll(this.langKey, pokemon.getNickname());
                user.getBattleStats().modifyStat(-1, StatsType.Defence, user, true, true);
            }
            if (e.getStatsType() == StatsType.SpecialAttack) {
                user.bc.sendToAll(this.langKey, pokemon.getNickname());
                user.getBattleStats().modifyStat(-1, StatsType.SpecialAttack, user, true, true);
            }
            if (e.getStatsType() == StatsType.SpecialDefence) {
                user.bc.sendToAll(this.langKey, pokemon.getNickname());
                user.getBattleStats().modifyStat(-1, StatsType.SpecialDefence, user, true, true);
            }
            if (e.getStatsType() == StatsType.HP) {
                user.bc.sendToAll(this.langKey, pokemon.getNickname());
                user.getBattleStats().modifyStat(-1, StatsType.HP, user, true, true);
            }
            if (e.getStatsType() == StatsType.Speed) {
                user.bc.sendToAll(this.langKey, pokemon.getNickname());
                user.getBattleStats().modifyStat(-1, StatsType.Speed, user, true, true);
            }
            if (e.getStatsType() == StatsType.Accuracy) {
                user.bc.sendToAll(this.langKey, pokemon.getNickname());
                user.getBattleStats().modifyStat(-1, StatsType.Accuracy, user, true, true);
            }
            if (e.getStatsType() == StatsType.Evasion) {
                user.bc.sendToAll(this.langKey, pokemon.getNickname());
                user.getBattleStats().modifyStat(-1, StatsType.Evasion, user, true, true);
            }
            return false;
        }
        if (!Attack.dealsDamage(user.attack)) {
            user.bc.sendToAll(this.langKey, pokemon.getNickname());
        }
        return false;
    }
}

