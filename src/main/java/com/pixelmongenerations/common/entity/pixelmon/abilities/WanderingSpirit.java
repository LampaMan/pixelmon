/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AsOne;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Disguise;
import com.pixelmongenerations.common.entity.pixelmon.abilities.FakemonRevealed;
import com.pixelmongenerations.common.entity.pixelmon.abilities.FlowerGift;
import com.pixelmongenerations.common.entity.pixelmon.abilities.GulpMissile;
import com.pixelmongenerations.common.entity.pixelmon.abilities.IceFace;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Imposter;
import com.pixelmongenerations.common.entity.pixelmon.abilities.LongReach;
import com.pixelmongenerations.common.entity.pixelmon.abilities.RKSSystem;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Receiver;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Schooling;
import com.pixelmongenerations.common.entity.pixelmon.abilities.StanceChange;
import com.pixelmongenerations.common.entity.pixelmon.abilities.WonderGuard;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ZenMode;
import com.pixelmongenerations.common.item.heldItems.ItemProtectivePads;

public class WanderingSpirit
extends AbilityBase {
    @Override
    public void applyEffectOnContactTarget(PixelmonWrapper user, PixelmonWrapper target) {
        AbilityBase userAbility = user.getBattleAbility(false);
        if (userAbility instanceof LongReach || userAbility instanceof AsOne || userAbility instanceof Disguise || userAbility instanceof IceFace || userAbility instanceof FakemonRevealed || userAbility instanceof FlowerGift || userAbility instanceof GulpMissile || userAbility instanceof IceFace || userAbility instanceof Imposter || userAbility instanceof Receiver || userAbility instanceof RKSSystem || userAbility instanceof Schooling || userAbility instanceof StanceChange || userAbility instanceof WonderGuard || userAbility instanceof ZenMode) {
            return;
        }
        if (user.hasHeldItem() && user.getHeldItem() instanceof ItemProtectivePads) {
            user.bc.sendToAll("pixelmon.effect.protectivepads", user.getNickname());
            return;
        }
        this.sendActivatedMessage(target);
        AbilityBase originalAbility = target.getAbility();
        target.setTempAbility(user.getAbility());
        user.setTempAbility(originalAbility);
    }
}

