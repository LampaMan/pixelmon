/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.HashMap;

public class Forewarn
extends AbilityBase {
    public static HashMap<String, Integer> exceptionMoves = new HashMap();

    public Forewarn() {
        if (exceptionMoves.isEmpty()) {
            exceptionMoves.put("Crush Grip", 80);
            exceptionMoves.put("Dragon Rage", 80);
            exceptionMoves.put("Endeavor", 80);
            exceptionMoves.put("Flail", 80);
            exceptionMoves.put("Frustration", 80);
            exceptionMoves.put("Grass Knot", 80);
            exceptionMoves.put("Gyro Ball", 80);
            exceptionMoves.put("Hidden Power", 80);
            exceptionMoves.put("Low Kick", 80);
            exceptionMoves.put("Natural Gift", 80);
            exceptionMoves.put("Night Shade", 80);
            exceptionMoves.put("Psywave", 80);
            exceptionMoves.put("Return", 80);
            exceptionMoves.put("Reversal", 80);
            exceptionMoves.put("Seismic Toss", 80);
            exceptionMoves.put("Sonic Boom", 80);
            exceptionMoves.put("Trump Card", 80);
            exceptionMoves.put("Wring Out", 80);
            exceptionMoves.put("Counter", 120);
            exceptionMoves.put("Metal Burst", 120);
            exceptionMoves.put("Mirror Coat", 120);
            exceptionMoves.put("Eruption", 150);
            exceptionMoves.put("Water Spout", 150);
            exceptionMoves.put("Fissure", 160);
            exceptionMoves.put("Guillotine", 160);
            exceptionMoves.put("Horn Drill", 160);
            exceptionMoves.put("Sheer Cold", 160);
        }
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        ArrayList<Attack> strongestAttacks = new ArrayList<Attack>();
        int highestPower = 0;
        ArrayList<PixelmonWrapper> opponents = newPokemon.bc.getOpponentPokemon(newPokemon.getParticipant());
        for (PixelmonWrapper opponent : opponents) {
            Moveset moveset = opponent.getMoveset();
            for (Attack a : moveset.attacks) {
                if (a == null) continue;
                int basePower = this.getAttackBasePower(a);
                if (basePower > highestPower) {
                    strongestAttacks.clear();
                    highestPower = basePower;
                }
                if (basePower != highestPower) continue;
                strongestAttacks.add(a);
            }
        }
        int r = RandomHelper.getRandomNumberBetween(0, strongestAttacks.size() - 1);
        Attack warnedAttack = (Attack)strongestAttacks.get(r);
        newPokemon.bc.sendToAll("pixelmon.abilities.forewarn", newPokemon.getNickname(), warnedAttack.getAttackBase().getLocalizedName());
    }

    private int getAttackBasePower(Attack a) {
        if (a == null) {
            return 0;
        }
        String attackName = a.getAttackBase().getUnlocalizedName();
        if (exceptionMoves.containsKey(attackName)) {
            return exceptionMoves.get(attackName);
        }
        int basePower = a.getAttackBase().basePower;
        if (basePower <= 0) {
            basePower = 1;
        }
        return basePower;
    }
}

