/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.entity.pixelmon.abilities.ContactDamage;

public class IronBarbs
extends ContactDamage {
    public IronBarbs() {
        super("pixelmon.abilities.ironbarbs");
    }
}

