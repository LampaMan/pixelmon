/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;

public class LevelCondition
extends EvoCondition {
    int level;

    public LevelCondition() {
    }

    public LevelCondition(int level) {
        this.level = level;
    }

    @Override
    public boolean passes(EntityPixelmon pokemon) {
        return pokemon.getLvl().getLevel() >= this.level;
    }
}

