/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Infatuated;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class FakemonLoveSong
extends AbilityBase {
    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (FakemonLoveSong.isSoundMove(a) && RandomHelper.getRandomChance(25) && Infatuated.infatuate(target, user, false)) {
            user.bc.sendToAll("pixelmon.abilities.lovesong", target.getNickname(), user.getNickname());
            return new int[]{power, accuracy};
        }
        return new int[]{power, accuracy};
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public static boolean isSoundMove(Attack a) {
        if (a == null) return false;
        if (!a.isAttack("Boomburst", "Bug Buzz", "Chatter", "Clanging Scales", "Clangorous Soulblaze", "Confide", "Disarming Voice", "Echoed Voice", "Grass Whistle", "Growl", "Heal Bell", "Hyper Voice", "Metal Sound", "Noble Roar", "Parting Shot", "Perish Song", "Relic Song", "Roar", "Round", "Screech", "Sing", "Snarl", "Snore", "Sparkling Aria", "Supersonic", "Uproar", "Overdrive")) return false;
        return true;
    }
}

