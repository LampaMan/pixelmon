/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Sunny;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class Harvest
extends AbilityBase {
    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        ItemHeld consumedItem = pokemon.getConsumedItem();
        if (consumedItem.isBerry() && !pokemon.hasHeldItem() && (pokemon.bc.globalStatusController.getWeather() instanceof Sunny && pokemon.getHeldItem() != PixelmonItemsHeld.utilityUmbrella || RandomHelper.getRandomChance(0.5f))) {
            pokemon.setHeldItem(consumedItem);
            pokemon.setConsumedItem(null);
            pokemon.bc.sendToAll("pixelmon.abilities.harvest", pokemon.getNickname(), consumedItem.getLocalizedName());
        }
    }
}

