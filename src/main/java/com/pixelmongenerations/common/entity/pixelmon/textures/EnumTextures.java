/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.textures;

import com.pixelmongenerations.common.entity.pixelmon.textures.IEnumSpecialTexture;

public enum EnumTextures implements IEnumSpecialTexture
{
    None(0),
    Halloween(1),
    Shadow(1),
    Classic(10);

    private int id;

    private EnumTextures(int id) {
        this.id = id;
    }

    @Override
    public boolean hasTexutre() {
        return !this.hasShinyVariant();
    }

    @Override
    public String getTexture() {
        return this.hasTexutre() ? "-" + this.name().toLowerCase() : "";
    }

    @Override
    public String getProperName() {
        return this.hasTexutre() ? this.name() : "";
    }

    @Override
    public boolean hasShinyVariant() {
        return this == None;
    }

    @Override
    public int getId() {
        return this.id;
    }
}

