/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats;

public class Aggression {
    public int timid;
    public int passive;
    public int aggressive;

    public Aggression(int pcTimid, int pcAgg, String pixelmonName) {
        this.timid = pcTimid;
        this.passive = 100 - pcTimid - pcAgg;
        this.aggressive = pcAgg;
    }
}

