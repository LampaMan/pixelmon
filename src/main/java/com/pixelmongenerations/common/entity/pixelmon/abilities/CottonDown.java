/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class CottonDown
extends AbilityBase {
    @Override
    public void applyEffectOnContactTarget(PixelmonWrapper user, PixelmonWrapper target) {
        this.sendActivatedMessage(user);
        user.getBattleStats().modifyStat(-1, StatsType.Speed, target, false);
    }
}

