/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.common.entity.pixelmon.drops;

import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.network.ChatHandler;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class DroppedItem {
    public ItemStack itemStack;
    public EnumBossMode rarity = EnumBossMode.NotBoss;
    public int id;

    public DroppedItem(ItemStack item, int id) {
        this.itemStack = item;
        this.id = id;
    }

    public DroppedItem(ItemStack item, int id, EnumBossMode rarity) {
        this(item, id);
        this.rarity = rarity;
    }

    public DroppedItem copy() {
        return DroppedItem.of(this.itemStack.copy(), this.rarity);
    }

    public void toBytes(ByteBuf buffer) {
        ByteBufUtils.writeItemStack(buffer, this.itemStack);
        buffer.writeInt(this.rarity.ordinal());
        buffer.writeInt(this.id);
    }

    public static DroppedItem fromBytes(ByteBuf buffer) {
        ItemStack itemStack = ByteBufUtils.readItemStack(buffer);
        EnumBossMode rarity = EnumBossMode.values()[buffer.readInt()];
        int id = buffer.readInt();
        return new DroppedItem(itemStack, id, rarity);
    }

    public void giveItem(EntityPlayerMP player) {
        String itemName = this.itemStack.getItem().getItemStackDisplayName(this.itemStack);
        if (DropItemHelper.giveItemStackToPlayer(player, this.itemStack)) {
            ChatHandler.sendFormattedChat(player, TextFormatting.GREEN, "pixelmon.drops.receivedrop", itemName);
        }
    }

    public void drop(Vec3d position, EntityPlayerMP player) {
        DropItemHelper.dropItemOnGround(position, player, this.itemStack, this.rarity != EnumBossMode.NotBoss, false);
    }

    public static DroppedItem of(ItemStack itemStack) {
        return new DroppedItem(itemStack, 1);
    }

    public static DroppedItem of(Item item) {
        return DroppedItem.of(new ItemStack(item));
    }

    public static DroppedItem of(Item item, int amount) {
        return DroppedItem.of(new ItemStack(item, amount));
    }

    public static DroppedItem of(ItemStack itemStack, EnumBossMode rarity) {
        return new DroppedItem(itemStack, 0, rarity);
    }
}

