/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.HiddenPower;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.ArrayList;

public class Anticipation
extends AbilityBase {
    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        ArrayList<PixelmonWrapper> opponents = newPokemon.getOpponentPokemon();
        for (PixelmonWrapper opponent : opponents) {
            for (Attack a : opponent.getMoveset().attacks) {
                if (a == null) continue;
                float effectiveness = EnumType.getTotalEffectiveness(newPokemon.type, a.isAttack("Hidden Power") ? HiddenPower.getHiddenPowerType(opponent.getStats().IVs) : a.getAttackBase().attackType, newPokemon.bc.rules.hasClause("inverse"));
                if (effectiveness < 2.0f) {
                    if (!a.isAttack("Fissure", "Guillotine", "Horn Drill", "Sheer Cold")) continue;
                }
                newPokemon.bc.sendToAll("pixelmon.abilities.anticipation", newPokemon.getNickname());
                return;
            }
        }
    }
}

