/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class Disguise
extends AbilityBase {
    private boolean canProtect = true;

    @Override
    public int modifyDamageIncludeFixed(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (this.canProtect && damage > 0) {
            if (!target.bc.simulateMode) {
                if (target.hasType(EnumType.Bug, EnumType.Dark, EnumType.Dragon, EnumType.Electric, EnumType.Fighting, EnumType.Fire, EnumType.Flying, EnumType.Grass, EnumType.Ground, EnumType.Ice, EnumType.Mystery, EnumType.Normal, EnumType.Poison, EnumType.Psychic, EnumType.Rock, EnumType.Steel, EnumType.Water)) {
                    target.setTempType(target.getInitialType());
                    target.addedType = null;
                }
                this.canProtect = false;
                if (target.getUsableHeldItem().getHeldItemType() == EnumHeldItems.airBalloon) {
                    user.bc.sendToAll("pixelmon.helditems.airballoonpop", target.getNickname());
                    target.removeHeldItem();
                }
                target.bc.sendToAll("pixelmon.abilities.disguise", target.getNickname());
                target.doBattleDamage(target, target.getPercentMaxHealth(12.5f), DamageTypeEnum.ABILITY);
            }
            return 0;
        }
        return damage;
    }

    public boolean getProtected() {
        return this.canProtect;
    }

    @Override
    public boolean canBeNeutralized() {
        return true;
    }
}

