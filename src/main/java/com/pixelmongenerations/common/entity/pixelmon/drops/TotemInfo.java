/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.drops;

import com.pixelmongenerations.common.entity.pixelmon.stats.FlyingOptions;
import com.pixelmongenerations.common.entity.pixelmon.stats.SwimOptions;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.List;

public class TotemInfo {
    public EnumSpecies pokemon;
    public Integer form;
    public SpawnLocation spawnLocation;
    public FlyingOptions flyingParameters;
    public SwimOptions swimmingParameters;
    public List<Integer> dimensions;

    public TotemInfo(EnumSpecies pokemon, SpawnLocation spawnLocation) {
        this.pokemon = pokemon;
        this.spawnLocation = spawnLocation;
    }

    public TotemInfo(EnumSpecies pokemon, SpawnLocation spawnLocation, FlyingOptions flyingParamters) {
        this(pokemon, spawnLocation);
        this.flyingParameters = flyingParamters;
    }

    public TotemInfo(EnumSpecies pokemon, SpawnLocation spawnLocation, SwimOptions swimmingParamters) {
        this(pokemon, spawnLocation);
        this.swimmingParameters = swimmingParamters;
    }
}

