/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.textures;

import com.pixelmongenerations.common.entity.pixelmon.textures.IEnumSpecialTexture;

public enum EnumGoodraLineTextures implements IEnumSpecialTexture
{
    ChocolateCherry(1, "Chocolate Cherry");

    private int id;
    private String name;

    private EnumGoodraLineTextures(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean hasTexutre() {
        return true;
    }

    @Override
    public String getTexture() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public String getProperName() {
        return this.name;
    }

    @Override
    public int getId() {
        return this.id;
    }
}

