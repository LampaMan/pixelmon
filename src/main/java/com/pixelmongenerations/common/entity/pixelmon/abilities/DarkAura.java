/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Aura;
import com.pixelmongenerations.core.enums.EnumType;

public class DarkAura
extends Aura {
    public DarkAura() {
        super(EnumType.Dark, StatusType.DarkAura);
    }
}

