/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.externalMoves;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.ExternalMoveBase;
import com.pixelmongenerations.core.network.PixelmonData;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.biome.Biome;

public class Lightning
extends ExternalMoveBase {
    public Lightning() {
        super("lightning", "Thunderbolt", "Bolt Strike", "Thunder", "Spark", "Discharge", "Fusion Bolt", "Thunder Shock");
    }

    @Override
    public boolean execute(EntityPixelmon user, RayTraceResult target, int moveIndex) {
        WorldServer world = (WorldServer)user.world;
        BlockPos pos = target.getBlockPos();
        if (target.typeOfHit == RayTraceResult.Type.ENTITY) {
            pos = target.entityHit.getPosition();
        }
        if (this.canLightningStrike(world, pos)) {
            world.addWeatherEffect(new EntityLightningBolt(world, pos.getX(), pos.getY(), pos.getZ(), false));
            return true;
        }
        return false;
    }

    private boolean canLightningStrike(World world, BlockPos strikePosition) {
        Biome Biome2 = world.getBiome(strikePosition);
        return !Biome2.getEnableSnow() && !world.canSnowAt(strikePosition, false) && Biome2.canRain();
    }

    @Override
    public int getCooldown(EntityPixelmon user) {
        return 500 - user.stats.Speed;
    }

    @Override
    public int getCooldown(PixelmonData pixelmonData) {
        return 500 - pixelmonData.Speed;
    }

    @Override
    public boolean isDestructive() {
        return true;
    }

    @Override
    public double getTargetDistance() {
        return 7.0;
    }
}

