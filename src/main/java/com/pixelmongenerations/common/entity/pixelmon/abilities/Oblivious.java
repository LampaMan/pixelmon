/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.PreventStatus;

public class Oblivious
extends PreventStatus {
    public Oblivious() {
        super("pixelmon.abilities.oblivious", "pixelmon.abilities.obliviouscure", StatusType.Infatuated, StatusType.Taunt);
    }

    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        if (a.isAttack("Captivate") && !AbilityBase.ignoreAbility(user, pokemon)) {
            user.bc.sendToAll(this.immuneText, user.getNickname());
            return false;
        }
        return true;
    }
}

