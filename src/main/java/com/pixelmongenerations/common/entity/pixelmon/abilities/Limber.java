/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.PreventStatus;

public class Limber
extends PreventStatus {
    public Limber() {
        super("pixelmon.abilities.limber", "pixelmon.abilities.limbercure", StatusType.Paralysis);
    }
}

