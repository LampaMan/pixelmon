/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;

public class FakemonPoptartCannon
extends AbilityBase {
    @Override
    public float modifyPriority(PixelmonWrapper pokemon, float priority) {
        if (pokemon.attack.getType() == EnumType.Flying && pokemon.attack.baseAttack.basePower > 1) {
            priority += 2.0f;
        }
        return priority;
    }
}

