/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class GorillaTactics
extends AbilityBase {
    @Override
    public void onAttackUsed(PixelmonWrapper user, Attack attack) {
        if (!user.bc.simulateMode) {
            if (user.attack.isAttack("Transform")) {
                return;
            }
            if (user.isDynamaxed()) {
                user.choiceSwapped = false;
                user.choiceLocked = null;
                return;
            }
            user.choiceLocked = attack;
        }
    }

    @Override
    public int[] modifyStatsItem(PixelmonWrapper user, int[] stats) {
        if (user.isDynamaxed()) {
            return stats;
        }
        int n = StatsType.Attack.getStatIndex();
        stats[n] = (int)((double)stats[n] * 1.5);
        return stats;
    }

    @Override
    public void applyRepeatedEffectItem(PixelmonWrapper pokemon) {
        Moveset moveset = pokemon.getMoveset();
        if (pokemon.isDynamaxed()) {
            return;
        }
        for (Attack currentMove : moveset) {
            if (pokemon.choiceLocked == null || currentMove.equals(pokemon.choiceLocked)) continue;
            currentMove.setDisabled(true, pokemon);
        }
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper user) {
        if (!user.bc.simulateMode && !user.choiceSwapped) {
            user.choiceLocked = null;
        }
        user.choiceSwapped = false;
    }

    @Override
    public void applySwitchOutEffect(PixelmonWrapper user) {
        if (!user.bc.simulateMode && !user.choiceSwapped) {
            user.choiceLocked = null;
        }
    }
}

