/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Absorb;
import com.pixelmongenerations.core.enums.EnumType;

public class WaterAbsorb
extends Absorb {
    public WaterAbsorb() {
        super(EnumType.Water, "pixelmon.abilities.waterabsorb");
    }

    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        if (a.isAttack("Dive")) {
            if (!user.hasStatus(StatusType.Submerged)) {
                return true;
            }
        }
        return super.allowsIncomingAttack(pokemon, user, a);
    }
}

