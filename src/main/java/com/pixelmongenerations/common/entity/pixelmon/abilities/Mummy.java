/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AsOne;
import com.pixelmongenerations.common.entity.pixelmon.abilities.BattleBond;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Comatose;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Disguise;
import com.pixelmongenerations.common.entity.pixelmon.abilities.FakemonRevealed;
import com.pixelmongenerations.common.entity.pixelmon.abilities.IceFace;
import com.pixelmongenerations.common.entity.pixelmon.abilities.LongReach;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Multitype;
import com.pixelmongenerations.common.entity.pixelmon.abilities.RKSSystem;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Schooling;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ShieldsDown;
import com.pixelmongenerations.common.entity.pixelmon.abilities.StanceChange;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ZenMode;
import com.pixelmongenerations.common.item.heldItems.ItemProtectivePads;
import net.minecraft.util.text.translation.I18n;

public class Mummy
extends AbilityBase {
    @Override
    public void tookDamageTarget(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        AbilityBase userAbility = user.getBattleAbility(false);
        if (!(!a.getAttackBase().getMakesContact() || userAbility instanceof LongReach || userAbility instanceof Mummy || userAbility instanceof AsOne || userAbility instanceof Multitype || userAbility instanceof ZenMode || userAbility instanceof StanceChange || userAbility instanceof Schooling || userAbility instanceof BattleBond || userAbility instanceof ShieldsDown || userAbility instanceof RKSSystem || userAbility instanceof Disguise || userAbility instanceof IceFace || userAbility instanceof FakemonRevealed || userAbility instanceof Comatose)) {
            if (user.hasHeldItem() && user.getHeldItem() instanceof ItemProtectivePads) {
                user.bc.sendToAll("pixelmon.effect.protectivepads", user.getNickname());
                return;
            }
            user.bc.sendToAll("pixelmon.effect.entrainment", user.getNickname(), userAbility.getLocalizedName(), I18n.translateToLocal("ability.mummy.name"));
            user.setTempAbility(this);
        }
    }
}

