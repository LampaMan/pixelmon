/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.externalMoves;

import com.pixelmongenerations.api.events.ExternalMoveEvent;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.ExternalMoveBase;
import com.pixelmongenerations.core.network.PixelmonData;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.common.MinecraftForge;

public class HealOther
extends ExternalMoveBase {
    public HealOther() {
        super("heal", "Soft-Boiled", "Milk Drink");
    }

    @Override
    public boolean execute(EntityPixelmon user, RayTraceResult target, int moveIndex) {
        if (target.typeOfHit == RayTraceResult.Type.ENTITY && target.entityHit instanceof EntityPixelmon) {
            EntityPixelmon targetPokemon = (EntityPixelmon)target.entityHit;
            if (targetPokemon.battleController != null) {
                return false;
            }
            if (targetPokemon.getHealth() >= (float)targetPokemon.stats.HP) {
                return false;
            }
            float healAmount = (float)user.stats.HP / 5.0f;
            ExternalMoveEvent.HealOtherMoveEvent healOtherEvent = new ExternalMoveEvent.HealOtherMoveEvent((EntityPlayerMP)user.getOwner(), user, this, target, healAmount);
            if (MinecraftForge.EVENT_BUS.post(healOtherEvent)) {
                return false;
            }
            healAmount = healOtherEvent.getHealAmount();
            targetPokemon.heal(healAmount);
            user.setHealth(user.getHealth() - healOtherEvent.getDamageAmount());
            return true;
        }
        return false;
    }

    @Override
    public int getCooldown(EntityPixelmon user) {
        return 300;
    }

    @Override
    public int getCooldown(PixelmonData pixelmonData) {
        return 300;
    }

    @Override
    public boolean isDestructive() {
        return false;
    }

    @Override
    public boolean targetsBlocks() {
        return false;
    }
}

