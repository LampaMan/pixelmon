/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class NaturalCure
extends AbilityBase {
    @Override
    public void applySwitchOutEffect(PixelmonWrapper pw) {
        pw.clearStatus();
    }

    @Override
    public void applyEndOfBattleEffect(PixelmonWrapper p) {
        p.clearStatus();
    }
}

