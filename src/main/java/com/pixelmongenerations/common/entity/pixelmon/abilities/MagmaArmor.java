/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.PreventStatus;

public class MagmaArmor
extends PreventStatus {
    public MagmaArmor() {
        super("pixelmon.abilities.magmaarmorcure", "pixelmon.abilities.magmaarmorcure", StatusType.Freeze);
    }
}

