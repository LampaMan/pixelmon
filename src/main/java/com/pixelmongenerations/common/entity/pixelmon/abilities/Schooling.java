/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.forms.EnumWishiwashi;

public class Schooling
extends AbilityBase {
    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        int damage = user.getLevelNum();
        if (damage >= 20) {
            if (user.getForm() == EnumWishiwashi.School.getForm()) {
                if (user.getHealthPercent() <= 25.0f) {
                    user.setForm(EnumWishiwashi.Solo.getForm());
                }
            } else if (user.getForm() == EnumWishiwashi.Solo.getForm() && user.getHealthPercent() >= 25.0f) {
                user.setForm(EnumWishiwashi.School.getForm());
            }
        }
        return stats;
    }

    @Override
    public void startMove(PixelmonWrapper user) {
        if (user.getHealthPercent() <= 25.0f && user.getLevelNum() >= 20) {
            user.bc.sendToAll("pixelmon.abilities.noschooling", user.getNickname());
        }
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.getHealthPercent() >= 25.0f && newPokemon.getLevelNum() >= 20) {
            newPokemon.bc.sendToAll("pixelmon.abilities.schooling", newPokemon.getNickname());
            newPokemon.setForm(EnumWishiwashi.School.getForm());
        }
        if (newPokemon.getLevelNum() <= 19 && newPokemon.getForm() != 0) {
            newPokemon.bc.sendToAll("pixelmon.abilities.noschooling", newPokemon.getNickname());
            newPokemon.setForm(EnumWishiwashi.Solo.getForm());
        }
    }

    @Override
    public void applyEndOfBattleEffect(PixelmonWrapper pokemon) {
        pokemon.setForm(EnumWishiwashi.Solo.getForm());
    }

    @Override
    public boolean canBeNeutralized() {
        return true;
    }
}

