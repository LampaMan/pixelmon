/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import java.util.Arrays;

public class IronFist
extends AbilityBase {
    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        String[] punchMoves = new String[]{"Bullet Punch", "Comet Punch", "Dizzy Punch", "Drain Punch", "DynamicPunch", "Fire Punch", "Focus Punch", "Hammer Arm", "Ice Punch", "Mach Punch", "Mega Punch", "Meteor Mash", "Shadow Punch", "Sky Uppercut", "Thunder Punch"};
        if (Arrays.asList(punchMoves).contains(a.getAttackBase().getUnlocalizedName())) {
            power = (int)((double)power * 1.2);
        }
        return new int[]{power, accuracy};
    }
}

