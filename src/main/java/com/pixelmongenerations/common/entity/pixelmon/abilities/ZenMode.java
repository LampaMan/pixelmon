/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.forms.EnumDarmanitan;
import com.pixelmongenerations.core.enums.forms.EnumForms;

public class ZenMode
extends AbilityBase {
    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        if (user.getForm() == EnumDarmanitan.Normal.getForm()) {
            if (user.getHealthPercent() <= 50.0f) {
                user.setForm(EnumDarmanitan.Zen.getForm());
                user.bc.sendToAll("pixelmon.abilities.zenmode", user.getNickname());
                user.bc.modifyStats();
            }
        } else if (user.getForm() == EnumDarmanitan.Zen.getForm()) {
            if (user.getHealthPercent() >= 50.0f) {
                user.setForm(EnumDarmanitan.Normal.getForm());
                user.bc.modifyStats();
            }
        } else if (user.getForm() == EnumForms.Galarian.getForm()) {
            if (user.getHealthPercent() <= 50.0f) {
                user.setForm(EnumDarmanitan.GalarZen.getForm());
                user.bc.sendToAll("pixelmon.abilities.zenmode", user.getNickname());
                user.bc.modifyStats();
            }
        } else if (user.getForm() == EnumDarmanitan.GalarZen.getForm() && user.getHealthPercent() >= 50.0f) {
            user.setForm(EnumForms.Galarian.getForm());
            user.bc.modifyStats();
        }
        return stats;
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.getHealthPercent() <= 50.0f) {
            newPokemon.bc.sendToAll("pixelmon.abilities.zenmode", newPokemon.getNickname());
        }
    }
}

