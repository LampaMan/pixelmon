/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.api.spawning.conditions.WorldTime;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types.InteractEvolution;
import com.pixelmongenerations.common.item.ItemEvolutionStone;
import com.pixelmongenerations.core.enums.EnumEvolutionStone;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumAlcremie;
import java.util.ArrayList;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class MilceryEvolution
extends InteractEvolution {
    public MilceryEvolution(EnumSpecies from, PokemonSpec to, Item item, EvoCondition[] conditions) {
        super(from, to, item, conditions);
    }

    @Override
    public boolean canEvolve(EntityPixelmon pokemon, ItemStack stack) {
        return (this.item == null || stack.getItem() == this.item) && super.canEvolve(pokemon);
    }

    @Override
    public boolean doEvolution(ItemStack itemStack, EntityPixelmon pokemon) {
        EntityPlayerMP player = (EntityPlayerMP)pokemon.getOwner();
        int ticks = (int)(pokemon.world.getWorldTime() % 24000L);
        ArrayList<WorldTime> times = WorldTime.getCurrent(ticks);
        times.removeAll(Lists.newArrayList((Object[])new WorldTime[]{WorldTime.MORNING, WorldTime.MIDDAY, WorldTime.AFTERNOON, WorldTime.MIDNIGHT}));
        if (times.isEmpty()) {
            return false;
        }
        if (!(itemStack.getItem() instanceof ItemEvolutionStone)) {
            return false;
        }
        EnumEvolutionStone evoStone = ((ItemEvolutionStone)itemStack.getItem()).getType();
        String formName = evoStone.name().toLowerCase().replaceAll("sweet", "");
        if (pokemon.getNickname().equalsIgnoreCase("jeb_")) {
            formName = formName + "-rainbowswirl";
        } else {
            switch (times.get(0)) {
                case DAWN: {
                    formName = formName + (player.isSneaking() ? "-rubyswirl" : "-vanillacream");
                    break;
                }
                case DAY: {
                    formName = formName + (player.isSneaking() ? "-caramelswirl" : "-rubycream");
                    break;
                }
                case DUSK: {
                    formName = formName + (player.isSneaking() ? "-lemoncream" : "-matchacream");
                    break;
                }
                case NIGHT: {
                    formName = formName + (player.isSneaking() ? "-saltedcream" : "-mintcream");
                    break;
                }
            }
        }
        EnumAlcremie form = EnumAlcremie.getFromName(formName);
        if (form == null) {
            return false;
        }
        this.setForm(form.getForm());
        return super.doEvolution(pokemon);
    }
}

