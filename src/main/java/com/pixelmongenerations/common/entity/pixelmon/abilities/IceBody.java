/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Hail;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class IceBody
extends AbilityBase {
    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        if (pokemon.getHealth() < pokemon.getMaxHealth() && pokemon.bc.globalStatusController.getWeather() instanceof Hail) {
            int par1 = (int)((float)pokemon.getMaxHealth() * 0.0625f);
            pokemon.healEntityBy(par1);
            pokemon.bc.sendToAll("pixelmon.abilities.icebody", pokemon.getNickname());
        }
    }
}

