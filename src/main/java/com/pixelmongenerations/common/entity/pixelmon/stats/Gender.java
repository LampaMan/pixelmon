/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Sets
 */
package com.pixelmongenerations.common.entity.pixelmon.stats;

import com.google.common.collect.Sets;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import java.util.Set;

public enum Gender implements IEnumForm {
    Male,
    Female,
    None;

    public String name;
    public static final Set<EnumSpecies> mfModels;

    @Override
    public String getSpriteSuffix() {
        return this == None ? "" : "-" + this.name().toLowerCase();
    }

    @Override
    public String getFormSuffix() {
        return this == None ? "" : this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    @Override
    public String getProperName() {
        return this == None ? "" : this.name();
    }

    public boolean isCompatible(Gender otherGender) {
        return this == Male && otherGender == Female || this == Female && otherGender == Male;
    }

    public static Gender getGender(short ord) {
        for (Gender g : Gender.values()) {
            if (g.ordinal() != ord) continue;
            return g;
        }
        return Male;
    }

    public static Gender getGender(String name) {
        for (Gender gender : Gender.values()) {
            if (!gender.name().toLowerCase().startsWith(name.toLowerCase())) continue;
            return gender;
        }
        return null;
    }

    public static Gender getGenderFromStartingString(String name) {
        for (Gender gender : Gender.values()) {
            if (gender.name == null) {
                gender.name = gender.name().toLowerCase();
            }
            if (!gender.name.startsWith(name)) continue;
            return gender;
        }
        return null;
    }

    static {
        mfModels = Sets.newHashSet(EnumSpecies.Unfezant, EnumSpecies.Frillish, EnumSpecies.Jellicent, EnumSpecies.Pyroar);
    }
}

