/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats;

import com.pixelmongenerations.api.def.EvolutionDef;
import com.pixelmongenerations.api.def.MoveContainer;
import com.pixelmongenerations.common.entity.pixelmon.stats.Aggression;
import com.pixelmongenerations.common.entity.pixelmon.stats.EVsStore;
import com.pixelmongenerations.common.entity.pixelmon.stats.FlyingOptions;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.entity.pixelmon.stats.Rarity;
import com.pixelmongenerations.common.entity.pixelmon.stats.RidingOptions;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.entity.pixelmon.stats.SwimOptions;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.Evolution;
import com.pixelmongenerations.core.database.DatabaseStats;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumEggGroup;
import com.pixelmongenerations.core.enums.EnumExperienceGroup;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.util.PixelSounds;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.minecraft.util.SoundEvent;

public class BaseStats {
    public String pixelmonName;
    public int spDef;
    public int spAtt;
    public int speed;
    public int defence;
    public int attack;
    public int hp;
    public int catchRate;
    public int malePercent;
    public int nationalPokedexNumber;
    public int spawnLevel;
    public int spawnLevelRange;
    public int baseExp;
    public int baseFriendship;
    public Rarity rarity;
    public EnumType type1;
    public EnumType type2;
    public float height;
    public float width;
    public float length;
    public float giScale;
    public boolean canFly;
    public boolean isRideable;
    public EnumSpecies[] preEvolutions;
    public EnumExperienceGroup experienceGroup;
    public Aggression aggression;
    public SwimOptions swimmingParameters;
    public FlyingOptions flyingParameters;
    public SpawnLocation[] spawnLocations;
    public Integer[] biomeIDs;
    public EVsStore evGain;
    public boolean canSurf;
    public RidingOptions ridingOffsets;
    public int maxGroupSize;
    public int minGroupSize;
    public float hoverHeight = 0.0f;
    public boolean doesHover = false;
    public int id;
    public int baseFormID;
    public float weight;
    public EnumSpecies pokemon;
    public transient Evolution[] evolutions;
    public String[] abilities;
    public EnumEggGroup[] eggGroups;
    public Integer eggCycles;
    public transient long lastSpawn = -1L;
    public int form = -1;
    private transient HashMap<SoundType, ArrayList<SoundEvent>> sounds = new HashMap();
    private transient boolean[] soundRegistered = new boolean[]{false, false, false};
    public EvolutionDef[] evolutionDefs;
    public MoveContainer moves;
    public static HashMap<EnumSpecies, String> hiddenAbilities = new HashMap();

    public BaseStats(String name) {
        this.pixelmonName = name;
    }

    public void init() {
        this.sounds = new HashMap();
        this.soundRegistered = new boolean[]{false, false, false};
        this.lastSpawn = -1L;
        if (this.abilities != null && this.abilities.length == 3 && this.abilities[2] != null && !this.abilities[2].isEmpty()) {
            hiddenAbilities.put(this.pokemon, this.abilities[2].toLowerCase());
        }
    }

    public void postInit() {
        try {
            DatabaseStats.getPixelmonEvolutions(this);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addSound(SoundType type, SoundEvent sound) {
        if (!this.soundRegistered[type.ordinal()]) {
            ArrayList<SoundEvent> list = this.sounds.get((Object)type);
            if (list == null) {
                list = new ArrayList();
            }
            list.add(sound);
            this.sounds.put(type, list);
            int id = this.nationalPokedexNumber * 3 + 1000;
            if (type == SoundType.Male) {
                ++id;
            } else if (type == SoundType.Female) {
                id += 2;
            }
            this.soundRegistered[type.ordinal()] = true;
        }
    }

    public SoundEvent getSound(SoundType type) {
        ArrayList<SoundEvent> list = this.sounds.get((Object)type);
        if (list == null) {
            return null;
        }
        return RandomHelper.getRandomElementFromList(list);
    }

    private boolean hasSound(SoundType type) {
        return this.sounds.containsKey((Object)type);
    }

    public boolean hasSoundForGender(Gender gender) {
        return this.sounds.containsKey((Object)(gender == Gender.Male ? SoundType.Male : SoundType.Female)) || this.sounds.containsKey((Object)SoundType.Neutral);
    }

    public SoundEvent getSoundForGender(Gender gender) {
        return this.getSoundForGender(gender, null);
    }

    public SoundEvent getSoundForGender(Gender gender, String nickName) {
        SoundEvent sound;
        if (nickName != null && (sound = PixelSounds.lookForCreatorSound(this.pokemon, nickName)) != null) {
            return sound;
        }
        if (gender == Gender.Male) {
            if (this.hasSound(SoundType.Male)) {
                return this.getSound(SoundType.Male);
            }
        } else if (gender == Gender.Female && this.hasSound(SoundType.Female)) {
            return this.getSound(SoundType.Female);
        }
        if (this.hasSound(SoundType.Neutral)) {
            return this.getSound(SoundType.Neutral);
        }
        return null;
    }

    public int get(StatsType stat) {
        switch (stat) {
            case Attack: {
                return this.attack;
            }
            case Defence: {
                return this.defence;
            }
            case HP: {
                return this.hp;
            }
            case SpecialAttack: {
                return this.spAtt;
            }
            case SpecialDefence: {
                return this.spDef;
            }
            case Speed: {
                return this.speed;
            }
        }
        return -1;
    }

    public boolean hasEggGroup(EnumEggGroup eggGroupTarget) {
        for (EnumEggGroup eggGroup : this.eggGroups) {
            if (eggGroup != eggGroupTarget) continue;
            return true;
        }
        return false;
    }

    public List<EnumType> getTypeList() {
        ArrayList<EnumType> list = new ArrayList<EnumType>();
        list.add(this.type1);
        if (this.type2 != null) {
            list.add(this.type2);
        }
        return list;
    }

    public static enum SoundType {
        Neutral,
        Male,
        Female;

    }
}

