/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats;

import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.Arrays;
import java.util.HashMap;
import net.minecraft.nbt.NBTTagCompound;

public class IVStore {
    public int HP;
    public int Attack;
    public int Defence;
    public int SpAtt;
    public int SpDef;
    public int Speed;
    public static final int MAX_IVS = 31;

    public IVStore() {
    }

    public IVStore(int[] ivs) {
        this.HP = ivs[0];
        this.Attack = ivs[1];
        this.Defence = ivs[2];
        this.SpAtt = ivs[3];
        this.SpDef = ivs[4];
        this.Speed = ivs[5];
    }

    public static IVStore createNewIVs() {
        IVStore iv = new IVStore();
        iv.SpDef = RandomHelper.getRandomNumberBetween(0, 31);
        iv.SpAtt = RandomHelper.getRandomNumberBetween(0, 31);
        iv.Speed = RandomHelper.getRandomNumberBetween(0, 31);
        iv.Defence = RandomHelper.getRandomNumberBetween(0, 31);
        iv.Attack = RandomHelper.getRandomNumberBetween(0, 31);
        iv.HP = RandomHelper.getRandomNumberBetween(0, 31);
        return iv;
    }

    public static IVStore createNewIVs3Perfect() {
        int[] ivs = new int[6];
        int[] maxIVs = RandomHelper.getRandomDistinctNumbersBetween(0, 5, 3);
        Arrays.sort(maxIVs);
        int maxIVCounter = 0;
        for (int i = 0; i < 6; ++i) {
            if (maxIVs[maxIVCounter] == i) {
                ivs[i] = 31;
                if (maxIVCounter >= maxIVs.length - 1) continue;
                ++maxIVCounter;
                continue;
            }
            ivs[i] = RandomHelper.getRandomNumberBetween(0, 31);
        }
        return new IVStore(ivs);
    }

    public static IVStore createPercentIVs(double percent) {
        if (percent >= 1.0) {
            return new IVStore(new int[]{31, 31, 31, 31, 31, 31});
        }
        int[] ivs = new int[6];
        int totalIVs = (int)Math.floor(186.0 * percent);
        block0: while (totalIVs > 0) {
            for (int i = 0; i < 6; ++i) {
                if (ivs[i] < 31) {
                    int ivRemainder = 31 - ivs[i];
                    int max = totalIVs >= 31 ? 31 : totalIVs;
                    max = ivRemainder < max ? ivRemainder : max;
                    int randomNumber = RandomHelper.getRandomNumberBetween(1, max);
                    int n = i;
                    ivs[n] = ivs[n] + randomNumber;
                    totalIVs -= randomNumber;
                }
                if (totalIVs <= 0) continue block0;
            }
        }
        return new IVStore(ivs);
    }

    public void writeToNBT(NBTTagCompound var1) {
        var1.setInteger("IVHP", this.HP);
        var1.setInteger("IVAttack", this.Attack);
        var1.setInteger("IVDefence", this.Defence);
        var1.setInteger("IVSpAtt", this.SpAtt);
        var1.setInteger("IVSpDef", this.SpDef);
        var1.setInteger("IVSpeed", this.Speed);
    }

    public void readFromNBT(NBTTagCompound var1) {
        this.HP = var1.getInteger("IVHP");
        this.Attack = var1.getInteger("IVAttack");
        this.Defence = var1.getInteger("IVDefence");
        this.SpAtt = var1.getInteger("IVSpAtt");
        this.SpDef = var1.getInteger("IVSpDef");
        this.Speed = var1.getInteger("IVSpeed");
    }

    public void copyIVs(IVStore ivs) {
        this.HP = ivs.HP;
        this.Attack = ivs.Attack;
        this.Defence = ivs.Defence;
        this.SpAtt = ivs.SpAtt;
        this.SpDef = ivs.SpDef;
        this.Speed = ivs.Speed;
    }

    public int get(StatsType stat) {
        return stat.getIV(this);
    }

    public void addIVs(StatsType stat, int amount) {
        stat.setIV(this, stat.getIV(this) + amount);
    }

    public static void getNBTTags(HashMap<String, Class> tags) {
        tags.put("IVHP", Integer.class);
        tags.put("IVAttack", Integer.class);
        tags.put("IVDefence", Integer.class);
        tags.put("IVSpAtt", Integer.class);
        tags.put("IVSpDef", Integer.class);
        tags.put("IVSpeed", Integer.class);
    }

    public int[] getArray() {
        return new int[]{this.HP, this.Attack, this.Defence, this.SpAtt, this.SpDef, this.Speed};
    }

    public void maximizeIVs() {
        this.Speed = 31;
        this.SpDef = 31;
        this.SpAtt = 31;
        this.Defence = 31;
        this.Attack = 31;
        this.HP = 31;
    }

    public void maxIV(StatsType stat) {
        stat.setIV(this, 31);
    }

    public boolean isMaxed() {
        for (int iv : this.getArray()) {
            if (iv == 31) continue;
            return false;
        }
        return true;
    }

    public boolean hasIVS() {
        int[] IVS = this.getArray();
        boolean band = true;
        int cont = 0;
        for (int i = 0; i < 6; ++i) {
            if (IVS[i] != 0) continue;
            ++cont;
        }
        if (cont == 6) {
            band = false;
        }
        return band;
    }

    public boolean isMaxed(StatsType stats) {
        return this.get(stats) == 31;
    }

    public int getTotal() {
        return this.HP + this.Attack + this.Defence + this.SpAtt + this.SpDef + this.Speed;
    }

    public double getPercent() {
        double totalIVs = this.getTotal();
        return totalIVs / 186.0 * 100.0;
    }

    public String getPercentString(int decimalPlaces) {
        return String.format("%." + decimalPlaces + "f", this.getPercent());
    }
}

