/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.textures;

import com.pixelmongenerations.common.entity.pixelmon.textures.IEnumSpecialTexture;

public enum EnumKrabbyTextures implements IEnumSpecialTexture
{
    Cowboy(1),
    Pink(2),
    Police(3),
    Classic(4);

    private int id;

    private EnumKrabbyTextures(int id) {
        this.id = id;
    }

    @Override
    public boolean hasTexutre() {
        return true;
    }

    @Override
    public String getTexture() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public String getProperName() {
        return this.name();
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public boolean hasShinyVariant() {
        return this == Classic;
    }
}

