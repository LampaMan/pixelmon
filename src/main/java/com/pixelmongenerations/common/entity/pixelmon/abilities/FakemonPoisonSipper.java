/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.entity.pixelmon.abilities.Redirect;
import com.pixelmongenerations.core.enums.EnumType;

public class FakemonPoisonSipper
extends Redirect {
    public FakemonPoisonSipper() {
        super(EnumType.Poison, "pixelmon.abilities.poisonsipper", "pixelmon.abilities.poisonsipperredirect");
    }
}

