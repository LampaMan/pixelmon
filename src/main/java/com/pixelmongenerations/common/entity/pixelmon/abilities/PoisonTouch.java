/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Poison;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.LongReach;
import com.pixelmongenerations.common.item.heldItems.ItemProtectivePads;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class PoisonTouch
extends AbilityBase {
    @Override
    public void applyEffectOnContactUser(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.hasHeldItem() && user.getHeldItem() instanceof ItemProtectivePads) {
            user.bc.sendToAll("pixelmon.effect.protectivepads", user.getNickname());
            return;
        }
        if (user.getBattleAbility() instanceof LongReach) {
            return;
        }
        if (RandomHelper.getRandomChance(0.3f) && Poison.poison(user, target, null, false)) {
            user.bc.sendToAll("pixelmon.abilities.poisontouch", user.getNickname(), target.getNickname());
        }
    }
}

