/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.drops;

import com.pixelmongenerations.common.entity.npcs.registry.DropItemRegistry;
import com.pixelmongenerations.common.entity.pixelmon.Entity8HoldsItems;
import com.pixelmongenerations.common.entity.pixelmon.drops.TotemInfo;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.minecraft.item.ItemStack;

public class TotemDrops {
    public ArrayList<TotemInfo> totemPokes;
    public ArrayList<String> anyDropPool;
    public HashMap<EnumSpecies, ArrayList<String>> pokeDropPool;
    public HashMap<EnumType, ArrayList<String>> typeDropPool;

    public ArrayList<ItemStack> getDropsFor(Entity8HoldsItems pixelmon) {
        String item;
        ArrayList<ItemStack> drops = new ArrayList<ItemStack>();
        boolean hadPokeDrop = false;
        if (this.pokeDropPool.containsKey((Object)pixelmon.getSpecies())) {
            hadPokeDrop = true;
            String item2 = (String)RandomHelper.getRandomElementFromList((List)this.pokeDropPool.get((Object)pixelmon.getSpecies()));
            drops.add(DropItemRegistry.parseItem(item2, String.format("totemdrops.json {pool=species, species=%s}", pixelmon.getSpecies().name)));
        }
        float chance = 0.5f;
        if (this.typeDropPool.containsKey(pixelmon.type.get(0)) && (!hadPokeDrop || RandomHelper.getRandomChance(0.5))) {
            chance = 0.3f;
            item = (String)RandomHelper.getRandomElementFromList((List)this.typeDropPool.get(pixelmon.type.get(0)));
            drops.add(DropItemRegistry.parseItem(item, String.format("totemdrops.json {pool=type, type=%s}", ((EnumType)((Object)pixelmon.type.get(0))).getLocalizedName())));
        }
        if (RandomHelper.getRandomChance(chance)) {
            item = RandomHelper.getRandomElementFromList(this.anyDropPool);
            drops.add(DropItemRegistry.parseItem(item, "totemdrops.json {pool=any}"));
        }
        return drops;
    }
}

