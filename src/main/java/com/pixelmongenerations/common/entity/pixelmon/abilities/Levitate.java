/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class Levitate
extends AbilityBase {
    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        if (pokemon.isGrounded()) {
            return true;
        }
        if (a.getAttackBase().attackType != EnumType.Ground) return true;
        if (a.getAttackCategory() == AttackCategory.Status) return true;
        if (!a.isAttack("Thousand Arrows")) return false;
        return true;
    }

    @Override
    public void allowsIncomingAttackMessage(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        pokemon.bc.sendToAll("pixelmon.abilities.levitate", pokemon.getNickname());
    }
}

