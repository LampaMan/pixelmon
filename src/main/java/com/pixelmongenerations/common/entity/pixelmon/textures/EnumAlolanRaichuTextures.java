/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.textures;

import com.pixelmongenerations.common.entity.pixelmon.textures.IEnumSpecialTexture;

public enum EnumAlolanRaichuTextures implements IEnumSpecialTexture
{
    Thunderclap(1, "Thunderclap"),
    PoolParty(2, "Pool Party");

    private int id;
    private String name;

    private EnumAlolanRaichuTextures(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean hasTexutre() {
        return true;
    }

    @Override
    public String getTexture() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public String getProperName() {
        return this.name;
    }

    @Override
    public boolean hasShinyVariant() {
        return true;
    }

    @Override
    public int getId() {
        return this.id;
    }
}

