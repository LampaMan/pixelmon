/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import java.util.ArrayList;

public class ScreenCleaner
extends AbilityBase {
    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        ArrayList<PixelmonWrapper> opponents = newPokemon.bc.getOpponentPokemon(newPokemon.getParticipant());
        for (PixelmonWrapper opponent : opponents) {
            if (opponent.removeTeamStatus(StatusType.LightScreen)) {
                newPokemon.bc.sendToAll("pixelmon.status.lightscreenoff", opponent.getNickname());
            }
            if (opponent.removeTeamStatus(StatusType.Reflect)) {
                newPokemon.bc.sendToAll("pixelmon.status.reflectoff", opponent.getNickname());
            }
            if (!opponent.removeTeamStatus(StatusType.AuroraVeil)) continue;
            newPokemon.bc.sendToAll("pixelmon.status.auroraveiloff", opponent.getNickname());
        }
        if (newPokemon.removeTeamStatus(StatusType.LightScreen)) {
            newPokemon.bc.sendToAll("pixelmon.status.lightscreenoff", newPokemon.getNickname());
        }
        if (newPokemon.removeTeamStatus(StatusType.Reflect)) {
            newPokemon.bc.sendToAll("pixelmon.status.reflectoff", newPokemon.getNickname());
        }
        if (newPokemon.removeTeamStatus(StatusType.AuroraVeil)) {
            newPokemon.bc.sendToAll("pixelmon.effect.auroraveiloff", newPokemon.getNickname());
        }
    }
}

