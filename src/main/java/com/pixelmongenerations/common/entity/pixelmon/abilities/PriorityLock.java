/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.GaleWings;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Prankster;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Triage;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import java.util.ArrayList;

public class PriorityLock
extends AbilityBase {
    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        if (this.hasPriorityUpAbility(user)) {
            if (user.getBattleAbility() instanceof Prankster && user.attack.getAttackCategory() != AttackCategory.Status) {
                return true;
            }
            user.bc.sendToAll("pixelmon.battletext.noeffect", pokemon.getNickname());
            return false;
        }
        return user.attack.getAttackBase().getPriority() <= 0;
    }

    public boolean hasPriorityUpAbility(PixelmonWrapper user) {
        ArrayList<Class<?>> abilities = new ArrayList<Class<?>>();
        abilities.add(Prankster.class);
        abilities.add(GaleWings.class);
        abilities.add(Triage.class);
        for (Class class_ : abilities) {
            if (!user.getAbility().isAbility(class_)) continue;
            return true;
        }
        return false;
    }
}

