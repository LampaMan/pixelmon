/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class Neuroforce
extends AbilityBase {
    @Override
    public int[] modifyPowerAndAccuracyTeammate(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (a.getTypeEffectiveness(user, target) >= 2.0) {
            return new int[]{(int)((double)power * 1.25), accuracy};
        }
        return new int[]{power, accuracy};
    }
}

