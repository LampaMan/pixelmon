/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.TrainerParticipant;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SheerForce;
import com.pixelmongenerations.common.item.heldItems.ItemBerryRestoreHP;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.network.packetHandlers.battles.EnforcedSwitch;

public class EmergencyExit
extends AbilityBase {
    @Override
    public void tookDamageTargetAfterMove(PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (user.bc.simulateMode) {
            return;
        }
        float berryHealPercent = 0.0f;
        if (ItemBerryRestoreHP.canEatBerry(target)) {
            if (target.getHeldItem() == PixelmonItemsHeld.sitrusBerry) {
                berryHealPercent = 0.25f;
            }
            if (target.getHeldItem() == PixelmonItemsHeld.figyBerry || target.getHeldItem() == PixelmonItemsHeld.wikiBerry || target.getHeldItem() == PixelmonItemsHeld.magoBerry || target.getHeldItem() == PixelmonItemsHeld.aguavBerry || target.getHeldItem() == PixelmonItemsHeld.iapapaBerry) {
                berryHealPercent = 0.5f;
            }
        }
        float berryHeal = (float)target.getMaxHealth() * berryHealPercent;
        if ((float)target.getHealth() + berryHeal <= (float)target.getPercentMaxHealth(50.0f) && target.isAlive() && (!(user.getBattleAbility() instanceof SheerForce) || a.getAttackBase().hasSecondaryEffect())) {
            if (!target.getParticipant().hasMorePokemonReserve()) {
                return;
            }
            target.nextSwitchIsMove = true;
            BattleParticipant targetParticipant = target.getParticipant();
            if (targetParticipant.getActiveUnfaintedPokemon().size() <= 0) {
                target.nextSwitchIsMove = false;
                return;
            }
            if (targetParticipant instanceof TrainerParticipant) {
                target.setUpSwitchMove();
                target.bc.switchPokemon(target.getPokemonID(), target.getBattleAI().getNextSwitch(target), true);
            } else if (targetParticipant instanceof PlayerParticipant) {
                if (!target.bc.simulateMode) {
                    target.setUpSwitchMove();
                    Pixelmon.NETWORK.sendTo(new EnforcedSwitch(target.bc.getPositionOfPokemon(target, target.getParticipant())), target.getPlayerOwner());
                }
            } else {
                target.nextSwitchIsMove = false;
            }
        }
    }
}

