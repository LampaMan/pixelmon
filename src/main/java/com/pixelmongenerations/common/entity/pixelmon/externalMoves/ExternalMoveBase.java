/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.externalMoves;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.AttackBase;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.network.PixelmonData;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class ExternalMoveBase {
    protected boolean targetted = true;
    protected AttackBase[] attacks;
    private String iconName;

    public ExternalMoveBase(String iconName, String ... attacks) {
        if (attacks != null) {
            this.attacks = Attack.getAttacks(attacks);
        }
        this.iconName = iconName;
    }

    @SideOnly(value=Side.CLIENT)
    public ResourceLocation getIcon() {
        return new ResourceLocation("pixelmon:textures/gui/overlay/externalMoves/" + this.iconName + ".png");
    }

    public boolean isTargetted() {
        return this.targetted;
    }

    public boolean matches(Attack a) {
        for (AttackBase base : this.attacks) {
            if (a.getAttackBase() != base) continue;
            return true;
        }
        return false;
    }

    public boolean matches(int attackIndex) {
        for (AttackBase base : this.attacks) {
            if (base != null && attackIndex != base.attackIndex) continue;
            return true;
        }
        return false;
    }

    public abstract boolean execute(EntityPixelmon var1, RayTraceResult var2, int var3);

    public abstract int getCooldown(EntityPixelmon var1);

    public abstract int getCooldown(PixelmonData var1);

    public double getTargetDistance() {
        return 3.0;
    }

    public boolean targetsBlocks() {
        return true;
    }

    public abstract boolean isDestructive();
}

