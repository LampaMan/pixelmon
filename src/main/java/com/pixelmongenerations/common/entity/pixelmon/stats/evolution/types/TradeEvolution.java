/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.Evolution;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;
import com.pixelmongenerations.core.enums.EnumSpecies;

public class TradeEvolution
extends Evolution {
    public EnumSpecies with = null;

    public TradeEvolution(EnumSpecies from, PokemonSpec to, EnumSpecies with, EvoCondition ... conditions) {
        super(from, to, conditions);
        this.with = with;
    }

    public boolean canEvolve(EntityPixelmon pokemon, EnumSpecies with) {
        return (this.with == null || this.with == with) && super.canEvolve(pokemon);
    }
}

