/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.forms.EnumAegislash;

public class StanceChange
extends AbilityBase {
    @Override
    public void startMove(PixelmonWrapper user) {
        if (user.attack.movePower > 0 && user.getForm() == 0) {
            user.setForm(EnumAegislash.Blade.getForm());
            user.bc.sendToAll("pixelmon.abilities.stancechangetoblade", user.getNickname());
            user.bc.modifyStats();
        }
    }

    @Override
    public boolean canBeNeutralized() {
        return true;
    }
}

