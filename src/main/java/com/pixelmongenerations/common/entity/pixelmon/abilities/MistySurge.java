/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.status.MistyTerrain;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Surge;

public class MistySurge
extends Surge {
    public MistySurge() {
        super(new MistyTerrain());
    }
}

