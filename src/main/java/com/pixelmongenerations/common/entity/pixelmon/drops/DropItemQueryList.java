/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.drops;

import com.pixelmongenerations.api.events.DropEvent;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.pixelmon.Entity8HoldsItems;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemQuery;
import com.pixelmongenerations.common.entity.pixelmon.drops.DroppedItem;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.packetHandlers.itemDrops.ItemDropMode;
import com.pixelmongenerations.core.network.packetHandlers.itemDrops.ItemDropPacket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.MinecraftForge;

public class DropItemQueryList {
    public static List<DropItemQuery> queryList = Collections.synchronizedList(new ArrayList());

    public static void register(Entity8HoldsItems pixelmon, ArrayList<DroppedItem> givenDrops, EntityPlayerMP player) {
        DropEvent event = new DropEvent(player, pixelmon, pixelmon.isTotem() ? ItemDropMode.Totem : (pixelmon.isBossPokemon() ? ItemDropMode.Boss : ItemDropMode.NormalPokemon), givenDrops);
        if (MinecraftForge.EVENT_BUS.post(event)) {
            return;
        }
        givenDrops = new ArrayList<DroppedItem>((Collection<DroppedItem>)event.getDrops());
        for (int i = 0; i < givenDrops.size(); ++i) {
            givenDrops.get((int)i).id = i;
        }
        DropItemQuery diq = new DropItemQuery(new Vec3d(pixelmon.posX, pixelmon.posY, pixelmon.posZ), player.getUniqueID(), givenDrops);
        queryList.add(diq);
        if (pixelmon.isBossPokemon() || pixelmon.isTotem()) {
            Pixelmon.NETWORK.sendTo(new ItemDropPacket(pixelmon.isBossPokemon() ? ItemDropMode.Boss : ItemDropMode.Totem, givenDrops), player);
        } else {
            Pixelmon.NETWORK.sendTo(new ItemDropPacket(ItemDropMode.NormalPokemon, ChatHandler.getMessage("gui.guiItemDrops.beatPokemon", pixelmon.getNickname()), givenDrops), player);
        }
    }

    public static void register(NPCTrainer npc, ArrayList<DroppedItem> givenDrops, EntityPlayerMP player) {
        DropEvent event = new DropEvent(player, npc, npc.isGymLeader ? ItemDropMode.Other : ItemDropMode.NormalTrainer, givenDrops);
        if (MinecraftForge.EVENT_BUS.post(event)) {
            return;
        }
        givenDrops = new ArrayList<DroppedItem>((Collection<DroppedItem>)event.getDrops());
        for (int i = 0; i < givenDrops.size(); ++i) {
            givenDrops.get((int)i).id = i;
        }
        DropItemQuery diq = new DropItemQuery(new Vec3d(npc.posX, npc.posY, npc.posZ), player.getUniqueID(), givenDrops);
        queryList.add(diq);
        ItemDropPacket p = null;
        p = npc.isGymLeader ? new ItemDropPacket(ChatHandler.getMessage("pixelmon.entitytrainer.dropitemstitle", npc.getName(player.language)), givenDrops) : new ItemDropPacket(ItemDropMode.NormalTrainer, givenDrops);
        Pixelmon.NETWORK.sendTo(p, player);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void takeAllItems(EntityPlayerMP player) {
        List<DropItemQuery> list;
        List<DropItemQuery> list2 = list = queryList;
        synchronized (list2) {
            for (int i = 0; i < queryList.size(); ++i) {
                DropItemQuery query = queryList.get(i);
                if (!query.playerUUID.equals(player.getUniqueID())) continue;
                for (DroppedItem droppedItem : query.droppedItemList) {
                    droppedItem.giveItem(player);
                }
            }
        }
        DropItemQueryList.removeQuery(player);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void dropAllItems(EntityPlayerMP player) {
        List<DropItemQuery> list;
        List<DropItemQuery> list2 = list = queryList;
        synchronized (list2) {
            for (int i = 0; i < queryList.size(); ++i) {
                if (!DropItemQueryList.queryList.get((int)i).playerUUID.equals(player.getUniqueID())) continue;
                Vec3d position = DropItemQueryList.queryList.get((int)i).position;
                for (DroppedItem droppedItem : DropItemQueryList.queryList.get((int)i).droppedItemList) {
                    droppedItem.drop(position, player);
                }
            }
        }
        DropItemQueryList.removeQuery(player);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void takeItem(EntityPlayerMP player, int itemID) {
        List<DropItemQuery> list;
        List<DropItemQuery> list2 = list = queryList;
        synchronized (list2) {
            for (int i = 0; i < queryList.size(); ++i) {
                DropItemQuery query = queryList.get(i);
                if (!query.playerUUID.equals(player.getUniqueID())) continue;
                for (int j = 0; j < query.droppedItemList.size(); ++j) {
                    DroppedItem droppedItem = query.droppedItemList.get(j);
                    if (droppedItem.id != itemID) continue;
                    droppedItem.giveItem(player);
                    query.droppedItemList.remove(j);
                    if (query.droppedItemList.isEmpty()) {
                        DropItemQueryList.removeQuery(player);
                    }
                    return;
                }
            }
        }
    }

    private static void giveSingleItem(EntityPlayerMP player, DroppedItem droppedItem) {
        String itemName = droppedItem.itemStack.getItem().getItemStackDisplayName(droppedItem.itemStack);
        if (DropItemHelper.giveItemStackToPlayer(player, droppedItem.itemStack)) {
            ChatHandler.sendFormattedChat(player, TextFormatting.GREEN, "pixelmon.drops.receivedrop", itemName);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void removeQuery(EntityPlayerMP player) {
        List<DropItemQuery> list;
        List<DropItemQuery> list2 = list = queryList;
        synchronized (list2) {
            for (int i = 0; i < queryList.size(); ++i) {
                if (!DropItemQueryList.queryList.get((int)i).playerUUID.equals(player.getUniqueID())) continue;
                queryList.remove(i);
                --i;
            }
        }
    }

    public static boolean hasInitialVowel(String strName) {
        return (strName = strName.toLowerCase()).toLowerCase().startsWith("a") || strName.startsWith("e") || strName.startsWith("i") || strName.startsWith("o") || strName.startsWith("u");
    }
}

