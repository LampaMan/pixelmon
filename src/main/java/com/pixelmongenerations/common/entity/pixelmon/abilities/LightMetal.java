/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class LightMetal
extends AbilityBase {
    @Override
    public float modifyWeight(float initWeight) {
        return initWeight / 2.0f;
    }
}

