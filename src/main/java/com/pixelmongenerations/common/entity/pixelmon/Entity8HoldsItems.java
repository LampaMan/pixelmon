/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon;

import com.pixelmongenerations.api.events.HeldItemChangeEvent;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.entity.pixelmon.Entity7HasAI;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.common.entity.pixelmon.drops.EnumDropHandler;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.interactions.custom.PixelmonInteraction;
import java.util.HashMap;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public abstract class Entity8HoldsItems
extends Entity7HasAI {
    public ItemStack heldItem;
    private DropItemHelper dropItemHelper = new DropItemHelper(this);
    private PixelmonInteraction interaction;
    public int numInteractions;
    boolean dropped = false;
    private int count;

    public Entity8HoldsItems(World par1World) {
        super(par1World);
        this.dataManager.register(EntityPixelmon.dwNumInteractions, 0);
    }

    @Override
    protected void init(String name) {
        super.init(name);
        this.interaction = PixelmonInteraction.getInteraction(this);
    }

    public int getNumInteractions() {
        return this.dataManager.get(EntityPixelmon.dwNumInteractions);
    }

    @Override
    public boolean processInteract(EntityPlayer player, EnumHand hand) {
        if (this.interaction != null && this.interaction.processInteract((EntityPixelmon)this, player, hand, player.getHeldItem(hand))) {
            return true;
        }
        return super.processInteract(player, hand);
    }

    @Override
    public void onUpdate() {
        super.onUpdate();
        if (this.interaction != null) {
            this.interaction.tick();
        }
    }

    public void setHeldItem(ItemStack newHeldItem) {
        if (newHeldItem == null || this.getPokemonId() == null || this == null) {
            return;
        }
        if (this.hasOwner()) {
            HeldItemChangeEvent event = new HeldItemChangeEvent(this.getOwner(), Optional.of((EntityPixelmon)this), Optional.empty(), newHeldItem);
            if (MinecraftForge.EVENT_BUS.post(event)) {
                return;
            }
            this.heldItem = event.getItem() == null ? ItemStack.EMPTY : event.getItem();
        } else {
            this.heldItem = newHeldItem;
        }
    }

    @Override
    public ItemStack getHeldItemMainhand() {
        if (this.heldItem == null) {
            return ItemStack.EMPTY;
        }
        return this.heldItem;
    }

    public ItemHeld getItemHeld() {
        return ItemHeld.getItemHeld(this.getHeldItemMainhand());
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound nbt) {
        super.writeEntityToNBT(nbt);
        ItemHeld.writeHeldItemToNBT(nbt, this.heldItem);
        if (this.interaction != null) {
            this.interaction.writeEntityToNBT(nbt);
        }
    }

    @Override
    public void getNBTTags(HashMap<String, Class> tags) {
        super.getNBTTags(tags);
        tags.put("NumInteractions", Short.class);
        tags.put("InteractionCount", Short.class);
        tags.put("HeldItemName", String.class);
        tags.put("HeldItemStack", ItemStack.class);
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        super.readEntityFromNBT(nbt);
        this.heldItem = ItemHeld.readHeldItemFromNBT(nbt);
        if (this.interaction != null) {
            this.interaction.readEntityFromNBT(nbt);
        }
    }

    @Override
    public void onDeath(DamageSource cause) {
        if (cause.getImmediateSource() instanceof EntityPixelmon && !this.world.isRemote) {
            EntityPixelmon pixelmon = (EntityPixelmon)cause.getImmediateSource();
            if (pixelmon.getOwner() != null && !this.dropped && !pixelmon.battleController.isHordeBattle()) {
                EnumDropHandler.dropFrom(this, (EntityPlayerMP)pixelmon.getOwner());
                this.dropped = true;
            } else if (pixelmon.battleController != null && !this.dropped) {
                if (pixelmon.battleController.isHordeBattle() && this.isWildPokemon()) {
                    this.dropped = true;
                } else {
                    pixelmon.battleController.participants.stream().filter(p -> p instanceof PlayerParticipant).forEach(p -> {
                        EnumDropHandler.dropFrom(this, ((PlayerParticipant)p).player);
                        this.dropped = true;
                    });
                }
            }
        }
        super.onDeath(cause);
    }

    @Override
    protected Item getDropItem() {
        return this.dropItemHelper.getDropItem();
    }

    public DropItemHelper getDropHelper() {
        return this.dropItemHelper;
    }

    public boolean isWildPokemon() {
        return !this.hasOwner() && this.getTrainer() == null;
    }
}

