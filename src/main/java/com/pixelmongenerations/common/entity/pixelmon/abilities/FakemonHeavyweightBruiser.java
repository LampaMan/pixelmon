/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class FakemonHeavyweightBruiser
extends AbilityBase {
    int turn = 0;

    @Override
    public void startMove(PixelmonWrapper user) {
        ++this.turn;
    }

    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (this.turn <= 1) {
            power = (int)((double)power * 3.0);
        } else if (this.turn == 2) {
            power = (int)((double)power * 1.5);
        } else if (this.turn == 3) {
            power = (int)((double)power * 0.75);
        } else if (this.turn == 4) {
            power = (int)((double)power * 0.375);
        } else if (this.turn == 5) {
            power = (int)((double)power * 0.1875);
        } else if (this.turn >= 6) {
            power = (int)((double)power * 0.0);
        }
        return new int[]{power, accuracy};
    }
}

