/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class MarvelScale
extends AbilityBase {
    @Override
    public int[] modifyStatsCancellable(PixelmonWrapper user, int[] stats) {
        if (user.hasPrimaryStatus()) {
            int[] arrn = stats;
            int n = StatsType.Defence.getStatIndex();
            arrn[n] = (int)((double)arrn[n] * 1.5);
        }
        return stats;
    }
}

