/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.heldItems.NoItem;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import net.minecraft.item.ItemStack;

public class HoneyGather
extends AbilityBase {
    private ItemHeld consumedItem = NoItem.noItem;
    private PixelmonWrapper consumer;

    @Override
    public void onItemConsumed(PixelmonWrapper pokemon, PixelmonWrapper consumer, ItemHeld heldItem) {
        if (pokemon != consumer && !pokemon.bc.simulateMode) {
            this.consumedItem = heldItem;
            this.consumer = consumer;
        }
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        if (pokemon != this.consumer && !pokemon.bc.simulateMode) {
            if (!pokemon.hasHeldItem() && this.consumedItem != NoItem.noItem && this.consumer != null && this.consumer.getConsumedItem() != NoItem.noItem && pokemon.bc.getActivePokemon().contains(this.consumer)) {
                pokemon.setHeldItem(this.consumedItem);
                this.consumer.setConsumedItem(null);
                pokemon.bc.sendToAll("pixelmon.abilities.honeygather", pokemon.getNickname(), this.consumedItem.getLocalizedName());
                pokemon.enableReturnHeldItem();
            }
            this.consumedItem = null;
            this.consumer = null;
        }
    }

    @Override
    public boolean needNewInstance() {
        return true;
    }

    public static void pickupHoney(PlayerParticipant player) {
        for (PixelmonWrapper pw : player.allPokemon) {
            if (!(pw.getAbility() instanceof HoneyGather) || pw.hasHeldItem() || !RandomHelper.getRandomChance((float)HoneyGather.getChanceBasedOnLevel(pw.getLevelNum()) / 100.0f)) continue;
            ItemStack foundItem = null;
            foundItem = new ItemStack(PixelmonItems.honey, 1);
            ItemStack itemCopy = foundItem.copy();
            itemCopy.setCount(1);
            player.player.inventory.addItemStackToInventory(itemCopy);
        }
    }

    public static int getChanceBasedOnLevel(int level) {
        int chance = 0;
        if (level < 10) {
            chance = 5;
        } else if (level >= 11 && level <= 20) {
            chance = 10;
        } else if (level >= 21 && level <= 30) {
            chance = 15;
        } else if (level >= 31 && level <= 40) {
            chance = 20;
        } else if (level >= 41 && level <= 50) {
            chance = 25;
        } else if (level >= 51 && level <= 60) {
            chance = 30;
        } else if (level >= 61 && level <= 70) {
            chance = 35;
        } else if (level >= 71 && level <= 80) {
            chance = 40;
        } else if (level >= 81 && level <= 90) {
            chance = 45;
        } else if (level >= 91 && level <= 100) {
            chance = 50;
        } else if (level > 100) {
            chance = 50;
        }
        return chance;
    }
}

