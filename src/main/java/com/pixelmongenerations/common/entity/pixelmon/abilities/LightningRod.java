/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.entity.pixelmon.abilities.Redirect;
import com.pixelmongenerations.core.enums.EnumType;

public class LightningRod
extends Redirect {
    public LightningRod() {
        super(EnumType.Electric, "pixelmon.abilities.lightningrod", "pixelmon.abilities.lightningrodredirect");
    }
}

