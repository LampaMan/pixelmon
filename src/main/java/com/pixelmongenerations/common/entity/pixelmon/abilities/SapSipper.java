/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn.MultiTurnSpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.EnumType;

public class SapSipper
extends AbilityBase {
    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        if (a.getAttackBase().attackType == EnumType.Grass) {
            if (a.isAttack("Solar Beam")) {
                for (int l = 0; l < a.getAttackBase().effects.size(); ++l) {
                    if (!(a.getAttackBase().effects.get(l) instanceof MultiTurnSpecialAttackBase) || !((MultiTurnSpecialAttackBase)a.getAttackBase().effects.get(l)).isCharging(user, pokemon)) continue;
                    return true;
                }
            }
            pokemon.bc.sendToAll("pixelmon.abilities.sapsipper", pokemon.getNickname());
            pokemon.getBattleStats().modifyStat(1, StatsType.Attack);
            return false;
        }
        return true;
    }
}

