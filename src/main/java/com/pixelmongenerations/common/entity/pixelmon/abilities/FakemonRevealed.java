/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class FakemonRevealed
extends AbilityBase {
    @Override
    public void startMove(PixelmonWrapper user) {
        if (user.attack.getAttackCategory() == AttackCategory.Physical && user.getForm() == 28) {
            user.setForm(29);
            user.bc.sendToAll("pixelmon.abilities.fakemonrevealed", user.getNickname());
            user.bc.modifyStats();
        } else if (user.attack.getAttackCategory() != AttackCategory.Physical && user.getForm() == 29) {
            user.setForm(28);
            user.bc.sendToAll("pixelmon.abilities.fakemonrevealed", user.getNickname());
            user.bc.modifyStats();
        }
    }

    @Override
    public boolean canBeNeutralized() {
        return true;
    }
}

