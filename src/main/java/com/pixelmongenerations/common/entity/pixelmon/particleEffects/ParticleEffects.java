/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.particleEffects;

import com.pixelmongenerations.common.entity.pixelmon.Entity4Textures;
import com.pixelmongenerations.common.entity.pixelmon.particleEffects.DiglettParticles;
import com.pixelmongenerations.common.entity.pixelmon.particleEffects.FlameParticles;
import com.pixelmongenerations.common.entity.pixelmon.particleEffects.GastlyParticles;
import com.pixelmongenerations.common.entity.pixelmon.particleEffects.GenericRotatingParticles;
import com.pixelmongenerations.common.entity.pixelmon.particleEffects.KoffingParticles;
import com.pixelmongenerations.common.entity.pixelmon.particleEffects.WeezingParticles;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.Random;

public abstract class ParticleEffects {
    Random rand = RandomHelper.rand;
    Entity4Textures pixelmon;

    public ParticleEffects(Entity4Textures pixelmon) {
        this.pixelmon = pixelmon;
    }

    public abstract void onUpdate();

    public static ParticleEffects[] getParticleEffects(Entity4Textures pixelmon) {
        ArrayList<ParticleEffects> effects = new ArrayList<ParticleEffects>();
        if (pixelmon.getPokemonName().equalsIgnoreCase("Weezing")) {
            effects.add(new WeezingParticles(pixelmon));
        } else if (pixelmon.getPokemonName().equalsIgnoreCase("Koffing")) {
            effects.add(new KoffingParticles(pixelmon));
        } else if (pixelmon.getPokemonName().equalsIgnoreCase("Diglett") || pixelmon.getPokemonName().equalsIgnoreCase("Dugtrio")) {
            effects.add(new DiglettParticles(pixelmon));
        } else if (pixelmon.getPokemonName().equalsIgnoreCase("Gastly")) {
            effects.add(new GastlyParticles(pixelmon));
        } else if (pixelmon.getPokemonName().equalsIgnoreCase("Charmander") && pixelmon.getSpecialTextureIndex() == 2) {
            effects.add(new FlameParticles(pixelmon, 0.55f, 0.52f, 2));
        }
        if (pixelmon.isShiny() || !pixelmon.getParticleId().isEmpty()) {
            effects.add(new GenericRotatingParticles(pixelmon));
        }
        return !effects.isEmpty() ? effects.toArray(new ParticleEffects[effects.size()]) : null;
    }
}

