/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.textures;

import com.pixelmongenerations.common.entity.pixelmon.textures.IEnumSpecialTexture;

public enum EnumEeveeTextures implements IEnumSpecialTexture
{
    Galaxy(1),
    PaleHat(2),
    LightHat(3),
    DarkHat(4),
    Bowtie(5),
    Classic(6);

    private int id;

    private EnumEeveeTextures(int id) {
        this.id = id;
    }

    @Override
    public boolean hasTexutre() {
        return true;
    }

    @Override
    public String getTexture() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public String getProperName() {
        return this.name();
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public boolean hasShinyVariant() {
        return this == Classic;
    }
}

