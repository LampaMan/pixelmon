/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.OHKO;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class NoGuard
extends AbilityBase {
    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        for (EffectBase effect : a.getAttackBase().effects) {
            if (!(effect instanceof OHKO)) continue;
            return new int[]{power, accuracy};
        }
        return new int[]{power, -2};
    }

    @Override
    public int[] modifyPowerAndAccuracyTarget(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        return new int[]{power, -2};
    }
}

