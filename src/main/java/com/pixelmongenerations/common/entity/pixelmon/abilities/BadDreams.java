/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class BadDreams
extends AbilityBase {
    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        pw.bc.getAdjacentPokemon(pw).stream().filter(pokemon -> pokemon.hasStatus(StatusType.Sleep)).forEach(pokemon -> {
            pw.bc.sendToAll("pixelmon.abilities.baddreams", pokemon.getNickname(), pw.getNickname());
            pokemon.doBattleDamage(pw, pokemon.getPercentMaxHealth(12.5f), DamageTypeEnum.ABILITY);
        });
    }
}

