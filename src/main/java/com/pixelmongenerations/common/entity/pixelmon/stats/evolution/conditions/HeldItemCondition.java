/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;
import com.pixelmongenerations.common.item.ItemHeld;

public class HeldItemCondition
extends EvoCondition {
    public final ItemHeld item;

    public HeldItemCondition(ItemHeld item) {
        this.item = item;
    }

    @Override
    public boolean passes(EntityPixelmon pokemon) {
        return pokemon.getItemHeld() == this.item;
    }
}

