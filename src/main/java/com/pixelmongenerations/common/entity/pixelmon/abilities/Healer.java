/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;

public class Healer
extends AbilityBase {
    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        ArrayList<PixelmonWrapper> allies = pokemon.getTeamPokemonExcludeSelf();
        for (PixelmonWrapper ally : allies) {
            if (!RandomHelper.getRandomChance(30) || !ally.hasPrimaryStatus()) continue;
            pokemon.bc.sendToAll(pokemon.getNickname(), this.getLocalizedName());
            ally.removePrimaryStatus();
        }
    }
}

