/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;

public class MoveCondition
extends EvoCondition {
    public int attackIndex = 1;

    public MoveCondition() {
    }

    public MoveCondition(int attackIndex) {
        this.attackIndex = attackIndex;
    }

    @Override
    public boolean passes(EntityPixelmon pokemon) {
        for (Attack move : pokemon.getMoveset().attacks) {
            if (move == null || move.getAttackBase().attackIndex != this.attackIndex) continue;
            return true;
        }
        return false;
    }
}

