/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Rainy;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;

public class RainDish
extends AbilityBase {
    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        if (pokemon.bc.globalStatusController.getWeather() instanceof Rainy && !pokemon.hasFullHealth()) {
            if (pokemon.getHeldItem() == PixelmonItemsHeld.utilityUmbrella) {
                return;
            }
            pokemon.bc.sendToAll("pixelmon.abilities.raindish", pokemon.getNickname());
            pokemon.healByPercent(6.25f);
        }
    }
}

