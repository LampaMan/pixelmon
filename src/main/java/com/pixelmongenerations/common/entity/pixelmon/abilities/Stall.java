/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class Stall
extends AbilityBase {
    @Override
    public float modifyPriority(PixelmonWrapper pokemon, float priority) {
        return priority - 0.1f;
    }
}

