/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.textures;

import com.pixelmongenerations.common.entity.pixelmon.textures.IEnumSpecialTexture;

public enum EnumJirachiTextures implements IEnumSpecialTexture
{
    Hana(2);

    private int id;

    private EnumJirachiTextures(int id) {
        this.id = id;
    }

    @Override
    public boolean hasTexutre() {
        return true;
    }

    @Override
    public String getTexture() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public String getProperName() {
        return this.name();
    }

    @Override
    public int getId() {
        return this.id;
    }
}

