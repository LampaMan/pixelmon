/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.externalMoves;

import com.pixelmongenerations.api.events.ExternalMoveEvent;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.ExternalMoveBase;
import com.pixelmongenerations.core.network.PixelmonData;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.GameType;
import net.minecraftforge.common.MinecraftForge;

public class Explode
extends ExternalMoveBase {
    public Explode() {
        super("explosion", "SelfDestruct", "Explosion");
    }

    @Override
    public boolean execute(EntityPixelmon user, RayTraceResult target, int moveIndex) {
        float explosionPower = 2.0f + user.getHealth() / 100.0f;
        ExternalMoveEvent.ExplodeMoveEvent explodeEvent = new ExternalMoveEvent.ExplodeMoveEvent((EntityPlayerMP)user.getOwner(), user, this, target, explosionPower);
        if (MinecraftForge.EVENT_BUS.post(explodeEvent) || (explosionPower = explodeEvent.getExplosionPower()) == 0.0f) {
            return false;
        }
        if (((EntityPlayerMP)user.getOwner()).interactionManager.getGameType() != GameType.ADVENTURE) {
            user.world.createExplosion(user, user.posX, user.posY, user.posZ, explosionPower, true);
        }
        user.isFainted = true;
        user.setHealth(0.0f);
        user.setDead();
        return true;
    }

    @Override
    public int getCooldown(EntityPixelmon user) {
        return 1600;
    }

    @Override
    public int getCooldown(PixelmonData pixelmonData) {
        return 1600;
    }

    @Override
    public boolean isDestructive() {
        return true;
    }
}

