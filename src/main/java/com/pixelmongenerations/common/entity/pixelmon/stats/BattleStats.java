/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Competitive;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Contrary;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Defiant;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Simple;
import com.pixelmongenerations.common.entity.pixelmon.stats.Stats;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class BattleStats {
    private PixelmonWrapper pixelmon;
    public int accuracy;
    public int evasion;
    public int attackModifier;
    public int defenceModifier;
    public int specialAttackModifier;
    public int specialDefenceModifier;
    public int speedModifier;
    public int attackStat;
    public int defenceStat;
    public int specialAttackStat;
    public int specialDefenceStat;
    public int speedStat;
    private int critStage;
    private int[] stages = new int[7];

    public BattleStats(PixelmonWrapper pixelmon) {
        this.pixelmon = pixelmon;
        this.clearBattleStats(true);
    }

    public BattleStats(BattleStats original) {
        this.copyStats(original);
    }

    public int getAccuracy() {
        return this.accuracy;
    }

    public int getEvasion() {
        return this.evasion;
    }

    public double getAttackModifier() {
        return this.attackModifier;
    }

    public int getDefenceModifier() {
        return this.defenceModifier;
    }

    public double getSpecialAttackModifier() {
        return this.specialAttackModifier;
    }

    public int getSpecialDefenceModifier() {
        return this.specialDefenceModifier;
    }

    public double getSpeedModifier() {
        return this.speedModifier;
    }

    public int getAccuracyStage() {
        return this.stages[StatsType.Accuracy.getStatIndex()];
    }

    public int getEvasionStage() {
        return this.stages[StatsType.Evasion.getStatIndex()];
    }

    public int getSpeedStage() {
        return this.stages[StatsType.Speed.getStatIndex()];
    }

    public int getAttackStage() {
        return this.stages[StatsType.Attack.getStatIndex()];
    }

    public int[] getStages() {
        return this.stages;
    }

    public boolean modifyStat(int amount, StatsType stat) {
        return this.modifyStat(new int[]{amount}, new StatsType[]{stat}, this.pixelmon, false);
    }

    public boolean modifyStat(int amount, StatsType ... stats) {
        int[] amounts = new int[stats.length];
        for (int i = 0; i < stats.length; ++i) {
            amounts[i] = amount;
        }
        return this.modifyStat(amounts, stats, this.pixelmon, false);
    }

    public boolean modifyStat(int[] amounts, StatsType[] stats) {
        return this.modifyStat(amounts, stats, this.pixelmon, false);
    }

    public boolean modifyStat(int amount, StatsType[] stats, PixelmonWrapper user) {
        int[] amounts = new int[stats.length];
        for (int i = 0; i < stats.length; ++i) {
            amounts[i] = amount;
        }
        return this.modifyStat(amounts, stats, user, false);
    }

    public boolean modifyStat(int amount, StatsType stat, PixelmonWrapper user, boolean isAttack) {
        return this.modifyStat(new int[]{amount}, new StatsType[]{stat}, user, isAttack, true, false);
    }

    public boolean modifyStat(int amount, StatsType stat, PixelmonWrapper user, boolean isAttack, boolean shouldMessage) {
        return this.modifyStat(new int[]{amount}, new StatsType[]{stat}, user, isAttack, shouldMessage, false);
    }

    public boolean modifyStat(int[] amounts, StatsType[] stats, PixelmonWrapper user, boolean isAttack) {
        return this.modifyStat(amounts, stats, user, isAttack, true, false);
    }

    public boolean modifyStat(int[] amounts, StatsType[] stats, PixelmonWrapper user, boolean isAttack, boolean shouldMessage, boolean isZEffect) {
        if (this.pixelmon.isFainted()) {
            return false;
        }
        boolean anySucceeded = false;
        for (int i = 0; i < stats.length; ++i) {
            int amount = amounts[i];
            StatsType stat = stats[i];
            if (!isZEffect) {
                AbilityBase thisAbility = this.pixelmon.getBattleAbility(user);
                if (thisAbility instanceof Contrary) {
                    amount *= -1;
                } else if (thisAbility instanceof Simple) {
                    amount *= 2;
                }
            }
            boolean succeeded = false;
            if (amount > 0) {
                succeeded = this.increaseStat(amount, stat, user, isAttack);
            } else if (amount < 0) {
                succeeded = this.decreaseStat(amount * -1, stat, user, isAttack, shouldMessage);
            }
            if (!succeeded) continue;
            anySucceeded = true;
        }
        if (!isAttack) {
            this.pixelmon.getUsableHeldItem().onStatModified(this.pixelmon);
        }
        return anySucceeded;
    }

    public boolean increaseCritStage(int value) {
        if (this.critStage >= value) {
            return false;
        }
        if (!this.pixelmon.bc.simulateMode) {
            this.critStage = value;
        }
        this.pixelmon.bc.sendToAll("pixelmon.effect.critincreased", this.pixelmon.getNickname());
        return true;
    }

    public int getCritStage() {
        return this.critStage;
    }

    public int GetAccOrEva(double stage) {
        if (stage > 6.0) {
            stage = 6.0;
        } else if (stage < -6.0) {
            stage = -6.0;
        }
        return stage < 1.0 ? (int)Math.round(3.0 / (Math.abs(stage) + 3.0) * 100.0) : (int)Math.round((Math.abs(stage) + 3.0) / 3.0 * 100.0);
    }

    private int GetStat(double stage) {
        if (stage > 6.0) {
            stage = 6.0;
        } else if (stage < -6.0) {
            stage = -6.0;
        }
        return stage < 1.0 ? (int)Math.round(2.0 / (Math.abs(stage) + 2.0) * 100.0) : (int)Math.round((Math.abs(stage) + 2.0) / 2.0 * 100.0);
    }

    public void increaseStats(int amount, List<StatsType> stats, PixelmonWrapper user, boolean isAttack) {
        for (StatsType statsType : stats) {
            this.increaseStat(amount, statsType, user, isAttack);
        }
    }

    public boolean increaseStat(int amount, StatsType stat, PixelmonWrapper user, boolean isAttack) {
        return this.increaseStat(amount, stat, user, isAttack, true);
    }

    public boolean increaseStat(int amount, StatsType stat, PixelmonWrapper user, boolean isAttack, boolean shouldMessage) {
        if (amount < 0) {
            return this.decreaseStat(Math.abs(amount), stat, user, isAttack);
        }
        int stageIndex = stat.getStatIndex();
        if (stageIndex != -1) {
            int currentStage = this.stages[stageIndex];
            if (currentStage == 6) {
                if (!isAttack) {
                    this.getStatFailureMessage(stat, true);
                }
                return false;
            }
            if ((currentStage += Math.abs(amount)) > 6) {
                amount -= currentStage - 6;
                currentStage = 6;
            }
            if (!this.pixelmon.bc.simulateMode) {
                this.stages[stageIndex] = currentStage;
            }
            if (stat != StatsType.Accuracy && stat != StatsType.Evasion) {
                int newValue = this.GetStat(currentStage);
                this.changeStat(stat, newValue);
            }
            String statMessage = "pixelmon.effect." + this.getStatStringLang(stat);
            statMessage = statMessage + "increased";
            if (amount == 2) {
                statMessage = statMessage + "2";
            } else if (amount >= 3) {
                statMessage = statMessage + "3";
            }
            if (shouldMessage) {
                this.pixelmon.bc.sendToAll(statMessage, this.pixelmon.getNickname());
            }
            this.pixelmon.getHeldItem().onStatIncrease(user, this.pixelmon, amount, stat);
            return true;
        }
        return false;
    }

    public boolean decreaseStat(int amount, StatsType stat, PixelmonWrapper user, boolean isAttack) {
        return this.decreaseStat(amount, stat, user, isAttack, true);
    }

    public boolean decreaseStat(int amount, StatsType stat, PixelmonWrapper user, boolean isAttack, boolean shouldMessage) {
        int stageIndex = stat.getStatIndex();
        if (stageIndex != -1) {
            StatsEffect effect = new StatsEffect(stat, amount * -1, user == this.pixelmon);
            if (this.pixelmon.getBattleAbility(user).allowsStatChange(this.pixelmon, user, effect)) {
                StatusBase status;
                Iterator<StatusBase> iterator = this.pixelmon.getStatuses().iterator();
                do {
                    if (iterator.hasNext()) continue;
                    int currentStage = this.stages[stageIndex];
                    if (currentStage == -6) {
                        if (!isAttack) {
                            this.getStatFailureMessage(stat, false);
                        }
                        return false;
                    }
                    if ((currentStage -= Math.abs(amount)) < -6) {
                        amount -= -6 - currentStage;
                        currentStage = -6;
                    }
                    if (!this.pixelmon.bc.simulateMode) {
                        this.stages[stageIndex] = currentStage;
                    }
                    if (stat != StatsType.Accuracy && stat != StatsType.Evasion) {
                        int newValue = this.GetStat(currentStage);
                        this.changeStat(stat, newValue);
                    }
                    String statMessage = "pixelmon.effect." + this.getStatStringLang(stat);
                    statMessage = statMessage + "decreased";
                    if (amount == 2) {
                        statMessage = statMessage + "2";
                    } else if (amount >= 3) {
                        statMessage = statMessage + "3";
                    }
                    if (shouldMessage) {
                        this.pixelmon.bc.sendToAll(statMessage, this.pixelmon.getNickname());
                    }
                    if (user != this.pixelmon && this.pixelmon.bc.sendMessages) {
                        AbilityBase thisAbility = this.pixelmon.getBattleAbility();
                        if (thisAbility instanceof Defiant) {
                            thisAbility.sendActivatedMessage(this.pixelmon);
                            this.modifyStat(2, StatsType.Attack);
                        } else if (thisAbility instanceof Competitive) {
                            thisAbility.sendActivatedMessage(this.pixelmon);
                            this.modifyStat(2, StatsType.SpecialAttack);
                        }
                    }
                    this.pixelmon.getHeldItem().onStatDecrease(user, this.pixelmon, amount, stat);
                    return true;
                } while ((status = iterator.next()).allowsStatChange(this.pixelmon, user, effect));
            }
        }
        return false;
    }

    public void changeStat(StatsType stat, int value) {
        if (!this.pixelmon.bc.simulateMode) {
            switch (stat) {
                case Accuracy: {
                    this.accuracy = value;
                    break;
                }
                case Evasion: {
                    this.evasion = value;
                    break;
                }
                case Attack: {
                    this.attackModifier = value;
                    break;
                }
                case Defence: {
                    this.defenceModifier = value;
                    break;
                }
                case SpecialAttack: {
                    this.specialAttackModifier = value;
                    break;
                }
                case SpecialDefence: {
                    this.specialDefenceModifier = value;
                    break;
                }
                case Speed: {
                    this.speedModifier = value;
                }
            }
        }
    }

    public int getStatFromEnum(StatsType stat) {
        switch (stat) {
            case Accuracy: {
                return this.accuracy;
            }
            case Evasion: {
                return this.evasion;
            }
            case Attack: {
                return this.attackStat;
            }
            case Defence: {
                return this.defenceStat;
            }
            case SpecialAttack: {
                return this.specialAttackStat;
            }
            case SpecialDefence: {
                return this.specialDefenceStat;
            }
            case Speed: {
                return this.speedStat;
            }
        }
        return 0;
    }

    public int getStatModFromEnum(StatsType stat) {
        switch (stat) {
            case Accuracy: {
                return this.accuracy;
            }
            case Evasion: {
                return this.evasion;
            }
            case Attack: {
                return this.attackModifier;
            }
            case Defence: {
                return this.defenceModifier;
            }
            case SpecialAttack: {
                return this.specialAttackModifier;
            }
            case SpecialDefence: {
                return this.specialDefenceModifier;
            }
            case Speed: {
                return this.speedModifier;
            }
        }
        return 0;
    }

    public StatsType getStageEnum(int index) {
        switch (index) {
            case 0: {
                return StatsType.Accuracy;
            }
            case 1: {
                return StatsType.Evasion;
            }
            case 2: {
                return StatsType.Attack;
            }
            case 3: {
                return StatsType.Defence;
            }
            case 4: {
                return StatsType.SpecialAttack;
            }
            case 5: {
                return StatsType.SpecialDefence;
            }
            case 6: {
                return StatsType.Speed;
            }
        }
        return StatsType.None;
    }

    public int getStage(StatsType stat) {
        return this.stages[stat.getStatIndex()];
    }

    public void setStage(StatsType stat, int value) {
        if (!this.pixelmon.bc.simulateMode) {
            this.stages[stat.getStatIndex()] = value;
        }
    }

    public void swapStats(BattleStats otherPokemon, StatsType ... stats) {
        if (!this.pixelmon.bc.simulateMode) {
            for (StatsType stat : stats) {
                int index = stat.getStatIndex();
                int tempStat = this.getStatModFromEnum(stat);
                int tempStage = this.stages[index];
                this.changeStat(stat, otherPokemon.getStatModFromEnum(stat));
                this.stages[index] = otherPokemon.getStage(stat);
                otherPokemon.changeStat(stat, tempStat);
                otherPokemon.setStage(stat, tempStage);
            }
        }
    }

    public void reverseStats() {
        if (!this.pixelmon.bc.simulateMode) {
            for (StatsType stat : StatsType.values()) {
                int statIndex = stat.getStatIndex();
                if (statIndex == -1) continue;
                this.stages[statIndex] = -this.stages[statIndex];
                if (stat == StatsType.Accuracy || stat == StatsType.Evasion) continue;
                int newValue = this.GetStat(this.stages[statIndex]);
                this.changeStat(stat, newValue);
            }
        }
    }

    public void resetStat(int index) {
        if (!this.pixelmon.bc.simulateMode) {
            this.changeStat(this.getStageEnum(index), 100);
            this.stages[index] = 0;
        }
    }

    public void resetStat(StatsType stat) {
        if (!this.pixelmon.bc.simulateMode) {
            this.changeStat(stat, 100);
            this.stages[stat.getStatIndex()] = 0;
        }
    }

    public void clearBattleStats() {
        this.clearBattleStats(false);
    }

    public void clearBattleStats(boolean init) {
        if (init || !this.pixelmon.bc.simulateMode) {
            this.evasion = 100;
            this.accuracy = 100;
            this.speedModifier = 100;
            this.specialDefenceModifier = 100;
            this.specialAttackModifier = 100;
            this.defenceModifier = 100;
            this.attackModifier = 100;
            this.critStage = 0;
            this.stages = new int[7];
        }
    }

    public void clearBattleStatsNoCrit() {
        if (!this.pixelmon.bc.simulateMode) {
            this.evasion = 100;
            this.accuracy = 100;
            this.speedModifier = 100;
            this.specialDefenceModifier = 100;
            this.specialAttackModifier = 100;
            this.defenceModifier = 100;
            this.attackModifier = 100;
            this.stages = new int[7];
        }
    }

    public void copyStats(BattleStats battleStats) {
        if (this.pixelmon == null || !this.pixelmon.bc.simulateMode) {
            this.speedModifier = battleStats.speedModifier;
            this.attackModifier = battleStats.attackModifier;
            this.defenceModifier = battleStats.defenceModifier;
            this.specialAttackModifier = battleStats.specialAttackModifier;
            this.specialDefenceModifier = battleStats.specialDefenceModifier;
            this.evasion = battleStats.evasion;
            this.accuracy = battleStats.accuracy;
            System.arraycopy(battleStats.stages, 0, this.stages, 0, this.stages.length);
        }
    }

    public void copyStatsTransform(BattleStats battleStats) {
        if (this.pixelmon == null || !this.pixelmon.bc.simulateMode) {
            this.attackStat = battleStats.attackStat;
            this.defenceStat = battleStats.defenceStat;
            this.specialAttackStat = battleStats.specialAttackStat;
            this.specialDefenceStat = battleStats.specialDefenceStat;
            this.speedStat = battleStats.speedStat;
            this.speedModifier = battleStats.speedModifier;
            this.attackModifier = battleStats.attackModifier;
            this.defenceModifier = battleStats.defenceModifier;
            this.specialAttackModifier = battleStats.specialAttackModifier;
            this.specialDefenceModifier = battleStats.specialDefenceModifier;
            this.evasion = battleStats.evasion;
            this.accuracy = battleStats.accuracy;
            System.arraycopy(battleStats.stages, 0, this.stages, 0, this.stages.length);
        }
    }

    public boolean isStatModified() {
        for (int stage : this.stages) {
            if (stage == 0) continue;
            return true;
        }
        return false;
    }

    public int getSumIncreases() {
        int increases = 0;
        for (int stage : this.stages) {
            if (stage <= 0) continue;
            increases += stage;
        }
        return increases;
    }

    public int getSumDecreases() {
        int decreases = 0;
        for (int stage : this.stages) {
            if (stage >= 0) continue;
            decreases -= stage;
        }
        return decreases;
    }

    public int getSumStages() {
        int stageMods = 0;
        for (int stage : this.stages) {
            stageMods += stage;
        }
        return stageMods;
    }

    public boolean statCanBeRaised() {
        for (int stage : this.stages) {
            if (stage >= 6) continue;
            return true;
        }
        return false;
    }

    public boolean statCanBeRaised(StatsType stat) {
        return this.stages[stat.getStatIndex()] < 6;
    }

    public ArrayList<Integer> getPossibleStatIncreases() {
        ArrayList<Integer> possibleStats = new ArrayList<Integer>();
        for (int i = 0; i < this.stages.length; ++i) {
            if (this.stages[i] >= 6) continue;
            possibleStats.add(i);
        }
        return possibleStats;
    }

    public boolean raiseRandomStat(int amount) {
        if (!this.statCanBeRaised()) {
            return false;
        }
        if (this.pixelmon.bc.simulateMode) {
            return true;
        }
        ArrayList<Integer> possibleStats = this.getPossibleStatIncreases();
        StatsType stat = this.getStageEnum(RandomHelper.getRandomNumberBetween(0, possibleStats.size() - 1));
        return this.modifyStat(amount, stat);
    }

    public int[] getBattleStats() {
        int[] stats = new int[7];
        stats[StatsType.Attack.getStatIndex()] = this.attackStat;
        stats[StatsType.Defence.getStatIndex()] = this.defenceStat;
        stats[StatsType.SpecialAttack.getStatIndex()] = this.specialAttackStat;
        stats[StatsType.SpecialDefence.getStatIndex()] = this.specialDefenceStat;
        stats[StatsType.Speed.getStatIndex()] = this.speedStat;
        stats[StatsType.Accuracy.getStatIndex()] = this.accuracy;
        stats[StatsType.Evasion.getStatIndex()] = this.evasion;
        return stats;
    }

    public int[] getBaseBattleStats() {
        return this.getBaseBattleStats(false);
    }

    public int[] getBaseBattleStats(boolean excludeStatusStats) {
        int[] stats = new int[7];
        Stats statContainer = this.pixelmon.getStats();
        statContainer.setLevelStats(this.pixelmon.getNature(), this.pixelmon.getPseudoNature(), this.pixelmon.getBaseStats(), this.pixelmon.getLevelNum());
        stats[StatsType.Attack.getStatIndex()] = statContainer.Attack;
        stats[StatsType.Defence.getStatIndex()] = statContainer.Defence;
        stats[StatsType.SpecialAttack.getStatIndex()] = statContainer.SpecialAttack;
        stats[StatsType.SpecialDefence.getStatIndex()] = statContainer.SpecialDefence;
        stats[StatsType.Speed.getStatIndex()] = statContainer.Speed;
        stats[StatsType.Accuracy.getStatIndex()] = 100;
        stats[StatsType.Evasion.getStatIndex()] = 100;
        if (excludeStatusStats) {
            for (StatusBase statusBase : this.pixelmon.getStatuses()) {
                stats = statusBase.modifyBaseStats(this.pixelmon, stats);
            }
            for (GlobalStatusBase globalStatusBase : this.pixelmon.bc.globalStatusController.getGlobalStatuses()) {
                stats = globalStatusBase.modifyBaseStats(this.pixelmon, stats);
            }
        }
        return stats;
    }

    public Map<StatsType, Integer> getRawBaseBattleStats() {
        final Stats statContainer = this.pixelmon.getStats();
        return new HashMap<StatsType, Integer>(){
            {
                this.put(StatsType.Attack, statContainer.Attack);
                this.put(StatsType.Defence, statContainer.Defence);
                this.put(StatsType.SpecialAttack, statContainer.SpecialAttack);
                this.put(StatsType.SpecialDefence, statContainer.SpecialDefence);
                this.put(StatsType.Speed, statContainer.Speed);
            }
        };
    }

    public void setStatsForTurn(int[] stats) {
        this.attackStat = stats[StatsType.Attack.getStatIndex()];
        this.defenceStat = stats[StatsType.Defence.getStatIndex()];
        this.specialAttackStat = stats[StatsType.SpecialAttack.getStatIndex()];
        this.specialDefenceStat = stats[StatsType.SpecialDefence.getStatIndex()];
        this.speedStat = stats[StatsType.Speed.getStatIndex()];
        this.evasion = stats[StatsType.Evasion.getStatIndex()];
        this.accuracy = stats[StatsType.Accuracy.getStatIndex()];
    }

    public String getStatStringLang(StatsType stat) {
        switch (stat) {
            case Accuracy: {
                return "accuracy";
            }
            case Evasion: {
                return "evasion";
            }
            case Attack: {
                return "attack";
            }
            case Defence: {
                return "defense";
            }
            case SpecialAttack: {
                return "spatk";
            }
            case SpecialDefence: {
                return "spdef";
            }
            case Speed: {
                return "speed";
            }
        }
        return "";
    }

    public void getStatFailureMessage(StatsType stat, boolean increase) {
        String failtext = "pixelmon.effect." + this.getStatStringLang(stat) + "too";
        failtext = increase ? failtext + "high" : failtext + "low";
        this.pixelmon.bc.sendToAll(failtext, this.pixelmon.getNickname());
    }

    public int getStatWithMod(StatsType stat) {
        switch (stat) {
            case Accuracy: {
                return this.accuracy;
            }
            case Evasion: {
                return this.evasion;
            }
            case Attack: {
                return Math.max(1, (int)((double)(this.attackStat * this.attackModifier) * 0.01));
            }
            case Defence: {
                return Math.max(1, (int)((double)(this.defenceStat * this.defenceModifier) * 0.01));
            }
            case SpecialAttack: {
                return Math.max(1, (int)((double)(this.specialAttackStat * this.specialAttackModifier) * 0.01));
            }
            case SpecialDefence: {
                return Math.max(1, (int)((double)(this.specialDefenceStat * this.specialDefenceModifier) * 0.01));
            }
            case Speed: {
                return Math.max(1, (int)((double)(this.speedStat * this.speedModifier) * 0.01));
            }
        }
        return 1;
    }

    public StatsType getHighestStat() {
        StatsType highestStat = StatsType.Attack;
        int statValue = -1;
        Map<StatsType, Integer> rawBaseBattleStats = this.getRawBaseBattleStats();
        for (Map.Entry<StatsType, Integer> entry : rawBaseBattleStats.entrySet()) {
            if (entry.getValue() <= statValue) continue;
            statValue = entry.getValue();
            highestStat = entry.getKey();
        }
        return highestStat;
    }
}

