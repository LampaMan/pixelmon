/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;

public class InnardsOut
extends AbilityBase {
    @Override
    public void tookDamageTarget(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (target.isFainted() && !(user.getBattleAbility() instanceof MagicGuard)) {
            user.doBattleDamage(target, damage, DamageTypeEnum.ABILITY);
            user.bc.sendToAll("pixelmon.abilities.innardsout", target.getNickname(), user.getNickname());
        }
    }
}

