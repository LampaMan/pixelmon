/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Paralysis;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class GulpMissile
extends AbilityBase {
    private boolean isArrokuda = false;
    private boolean isPikachu = false;

    @Override
    public void tookDamageUser(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (a.isAttack("Surf", "Dive")) {
            if (user.getHealthPercent() >= 50.0f) {
                user.bc.sendToAll("pixelmon.abilities.gulparrokuda", user.getNickname());
                this.isArrokuda = true;
            } else {
                user.bc.sendToAll("pixelmon.abilities.gulppikachu", user.getNickname());
                this.isPikachu = true;
            }
        }
    }

    @Override
    public void tookDamageTarget(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (this.isArrokuda) {
            if (!(user.getAbility() instanceof MagicGuard)) {
                user.doBattleDamage(target, user.getPercentMaxHealth(25.0f), DamageTypeEnum.ABILITY);
            }
            user.getBattleStats().modifyStat(-1, StatsType.Defence, user, false);
            target.bc.sendToAll("pixelmon.abilities.attackarrokuda", target.getNickname());
            this.isArrokuda = false;
        } else if (this.isPikachu) {
            if (!(user.getAbility() instanceof MagicGuard)) {
                user.doBattleDamage(target, user.getPercentMaxHealth(25.0f), DamageTypeEnum.ABILITY);
            }
            user.bc.sendToAll("pixelmon.abilities.attackpikachu", target.getNickname());
            user.addStatus(new Paralysis(), target);
            this.isPikachu = false;
        }
    }
}

