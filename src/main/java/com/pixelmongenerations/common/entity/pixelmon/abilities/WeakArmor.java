/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class WeakArmor
extends AbilityBase {
    @Override
    public void tookDamageTarget(int damage, PixelmonWrapper user, PixelmonWrapper opponent, Attack a) {
        if (a.getAttackCategory() == AttackCategory.Physical && opponent.isAlive()) {
            this.sendActivatedMessage(opponent);
            opponent.getBattleStats().modifyStat(new int[]{-1, 1}, new StatsType[]{StatsType.Defence, StatsType.Speed});
        }
    }
}

