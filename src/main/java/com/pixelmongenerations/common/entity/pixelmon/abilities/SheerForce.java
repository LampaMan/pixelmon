/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class SheerForce
extends AbilityBase {
    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (user.isDynamaxed()) {
            return new int[]{power, accuracy};
        }
        boolean powerModified = false;
        for (EffectBase effect : a.getAttackBase().effects) {
            if (!effect.isChance()) continue;
            effect.changeChance(0);
            if (powerModified) continue;
            power = (int)((double)power * 1.3);
            powerModified = true;
        }
        return new int[]{power, accuracy};
    }
}

