/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.vecmath.Vector3f
 */
package com.pixelmongenerations.common.entity.pixelmon;

import com.pixelmongenerations.api.events.RidePokemonEvent;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.pixelmon.Entity10CanBreed;
import com.pixelmongenerations.common.entity.pixelmon.Entity4Textures;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.helpers.AIHelper;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.RidingOptions;
import com.pixelmongenerations.common.item.IncreaseEV;
import com.pixelmongenerations.common.item.ItemAbilityCapsule;
import com.pixelmongenerations.common.item.ItemAbilityPatch;
import com.pixelmongenerations.common.item.ItemCurry;
import com.pixelmongenerations.common.item.ItemElixir;
import com.pixelmongenerations.common.item.ItemEther;
import com.pixelmongenerations.common.item.ItemEvolutionStone;
import com.pixelmongenerations.common.item.ItemFeather;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.ItemLevelCandy;
import com.pixelmongenerations.common.item.ItemMedicine;
import com.pixelmongenerations.common.item.ItemPokeMint;
import com.pixelmongenerations.common.item.ItemPokePuff;
import com.pixelmongenerations.common.item.ItemTM;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumSpecies;
import javax.vecmath.Vector3f;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSaddle;
import net.minecraft.item.ItemShears;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.MinecraftForge;

public abstract class Entity5Rideable
extends Entity4Textures {
    public int ridingPlayerVertical;
    public boolean isFlying = false;
    private int initAir = 0;
    private boolean ridingInitialised = false;
    private float debugOffsetX = 0.0f;
    private float debugOffsetY = 0.0f;
    private float debugOffsetZ = 0.0f;

    public Entity5Rideable(World par1World) {
        super(par1World);
    }

    @Override
    public void fall(float distance, float damageMultiplier) {
        if (this.baseStats != null && this.baseStats.canFly) {
            return;
        }
        if (this.isBeingRidden() && distance < 4.0f) {
            return;
        }
        super.fall(distance, damageMultiplier);
    }

    @Override
    public boolean processInteract(EntityPlayer player, EnumHand hand) {
        if (player.world.isRemote || player.isRiding() || !PixelmonConfig.allowRiding || ((Entity10CanBreed)this).isInRanchBlock || !this.baseStats.isRideable || !this.belongsTo(player) || PixelmonConfig.haveHM && !this.hasHM()) {
            return super.processInteract(player, hand);
        }
        if (hand == EnumHand.MAIN_HAND) {
            Item item = player.getHeldItem(hand).getItem();
            if (item instanceof ItemMedicine || item instanceof ItemAbilityCapsule || item instanceof ItemAbilityPatch || item == PixelmonItems.nSolarizer || item == PixelmonItems.nLunarizer || item == PixelmonItems.mewtwoArmor || item == PixelmonItems.bottleCap | item instanceof ItemCurry || item instanceof ItemEvolutionStone || item instanceof ItemElixir || item instanceof ItemEther || item instanceof ItemFeather || item instanceof ItemHeld || item instanceof ItemLevelCandy || item instanceof ItemPokeMint || item instanceof ItemPokePuff || item == PixelmonItems.reinsOfUnity || item instanceof ItemSaddle || item instanceof ItemTM || item instanceof ItemShears || item instanceof IncreaseEV) {
                return false;
            }
            boolean allowFly = !MinecraftForge.EVENT_BUS.post(new RidePokemonEvent((EntityPlayerMP)player, (EntityPixelmon)this));
            if (!allowFly) {
                return super.processInteract(player, hand);
            }
            player.startRiding(this);
            return true;
        }
        return super.processInteract(player, hand);
    }

    public boolean hasHM() {
        if (!this.baseStats.canFly && !this.baseStats.canSurf) {
            return true;
        }
        Moveset moves = ((EntityPixelmon)this).getMoveset();
        for (Attack move : moves) {
            if (!move.isAttack("Fly") || !this.baseStats.canFly) {
                if (!move.isAttack("Surf") || !this.baseStats.canSurf) continue;
            }
            return true;
        }
        return false;
    }

    @Override
    public double getMountedYOffset() {
        return (double)this.height * 0.9;
    }

    @Override
    public void travel(float strafe, float up, float forward) {
        this.stepHeight = 0.5f;
        if (this.baseStats == null || this.getPassengers().size() == 0) {
            if (this.baseStats != null && !this.baseStats.canFly && this.getSpawnLocation() == SpawnLocation.Water && this.isInWater()) {
                float f4 = 0.9f;
                float f5 = 0.02f;
                float f6 = EnchantmentHelper.getDepthStriderModifier(this);
                if (f6 > 3.0f) {
                    f6 = 3.0f;
                }
                if (!this.onGround) {
                    f6 *= 0.5f;
                }
                if (f6 > 0.0f) {
                    f4 += (0.54600006f - f4) * f6 / 3.0f;
                    f5 += (this.getAIMoveSpeed() * 1.0f - f5) * f6 / 3.0f;
                }
                this.moveRelative(strafe, 0.0f, forward, f5);
                this.move(MoverType.SELF, this.motionX, this.motionY, this.motionZ);
                this.motionX *= (double)f4;
                this.motionY *= (double)0.8f;
                this.motionZ *= (double)f4;
                this.motionY -= 0.02;
                if (this.collidedHorizontally && this.isOffsetPositionInLiquid(this.motionX, this.motionY + (double)0.6f, this.motionZ)) {
                    this.motionY = 0.3f;
                }
            } else {
                super.travel(strafe, up, forward);
            }
            return;
        }
        if (this.getPassengers().size() > 0 && this.getControllingPassenger() instanceof EntityLivingBase) {
            EntityLivingBase rider = (EntityLivingBase)this.getControllingPassenger();
            if (PixelmonConfig.enablePointToSteer) {
                this.prevRotationYaw = this.rotationYaw = this.getControllingPassenger().rotationYaw;
            }
            strafe = rider.moveStrafing * 0.5f * PixelmonServerConfig.ridingSpeedMultiplier;
            this.rotationYaw -= strafe * 10.0f;
            strafe = 0.0f;
            this.rotationPitch = this.getControllingPassenger().rotationPitch * 0.5f;
            this.rotationYawHead = this.renderYawOffset = this.rotationYaw;
            forward = rider.moveForward * PixelmonServerConfig.ridingSpeedMultiplier;
            this.moveMounted(strafe, up, forward);
            if (PixelmonConfig.enablePointToSteer) {
                rider.prevRenderYawOffset = rider.renderYawOffset = this.renderYawOffset;
                rider.prevRotationYaw = rider.rotationYaw = this.rotationYaw;
            }
            this.prevLimbSwingAmount = this.limbSwingAmount;
            double var9 = this.posX - this.prevPosX;
            double var12 = this.posZ - this.prevPosZ;
            float var11 = MathHelper.sqrt(var9 * var9 + var12 * var12) * 4.0f;
            if (var11 > 1.0f) {
                var11 = 1.0f;
            }
            this.limbSwingAmount += (var11 - this.limbSwingAmount) * 0.4f;
            this.limbSwing += this.limbSwingAmount;
            if (this.world.isRemote) {
                this.onGround = !this.world.isAirBlock(new BlockPos(MathHelper.floor(this.posX), MathHelper.floor(MathHelper.floor(this.getEntityBoundingBox().minY) - 1), MathHelper.floor(this.posZ)));
            }
        }
    }

    private void moveMounted(float strafe, float up, float forward) {
        if (this.getControllingPassenger() != null && this.baseStats.canSurf && this.inWater) {
            double var9 = this.posY;
            this.moveRelative(strafe, 0.0f, forward, this.jumpMovementFactor);
            this.move(MoverType.SELF, this.motionX, this.motionY, this.motionZ);
            if (this.collidedHorizontally && this.isOffsetPositionInLiquid(this.motionX, this.motionY + (double)0.6f - this.posY + var9, this.motionZ)) {
                this.motionY = 0.3f;
            }
            this.motionY *= (double)0.98f;
            this.motionX *= (double)0.96f;
            this.motionZ *= (double)0.96f;
            return;
        }
        if (this.baseStats.canFly && !this.isInWater() && !this.isInLava()) {
            float var3 = 0.91f;
            if (this.onGround) {
                var3 = 0.54600006f;
                IBlockState state = this.world.getBlockState(new BlockPos(MathHelper.floor(this.posX), MathHelper.floor(this.getEntityBoundingBox().minY) - 1, MathHelper.floor(this.posZ)));
                if (state.getMaterial() != Material.AIR) {
                    var3 = state.getBlock().slipperiness * 0.91f;
                }
            }
            float var8 = 0.16277136f / (var3 * var3 * var3);
            float var5 = this.getRideSpeed();
            var5 = this.onGround ? (var5 = var5 * (0.6f * var8)) : (var5 = var5 * 0.8f);
            this.moveRelative(strafe, 0.0f, forward, var5);
            this.move(MoverType.SELF, this.motionX, this.motionY, this.motionZ);
            var3 = 0.91f;
            if (this.onGround) {
                var3 = 0.54600006f;
                IBlockState state = this.world.getBlockState(new BlockPos(MathHelper.floor(this.posX), MathHelper.floor(this.getCollisionBoundingBox().minY) - 1, MathHelper.floor(this.posZ)));
                if (state.getMaterial() != Material.AIR) {
                    var3 = state.getBlock().slipperiness * 0.91f;
                }
            }
            double d = !this.world.isRemote && !this.world.isBlockLoaded(new BlockPos((int)this.posX, 0, (int)this.posZ)) ? (this.posY > 0.0 ? (this.motionY = this.motionY - 0.01) : 0.0) : (this.motionY = this.motionY - 0.02);
            this.motionY = d;
            this.motionY *= (double)0.98f;
            this.motionX *= (double)var3;
            this.motionZ *= (double)var3;
            return;
        }
        this.moveEntityRidden(strafe, forward);
        super.travel(strafe, up, forward);
    }

    private void moveEntityRidden(float strafe, float forward) {
        if (this.isInWater()) {
            double d0 = this.posY;
            this.moveRelative(strafe, 0.0f, forward, 0.05f);
            this.move(MoverType.SELF, this.motionX, this.motionY, this.motionZ);
            this.motionX *= (double)0.8f;
            this.motionY *= (double)0.8f;
            this.motionZ *= (double)0.8f;
            this.motionY -= 0.019;
            if (this.collidedHorizontally && this.isOffsetPositionInLiquid(this.motionX, this.motionY + (double)0.6f - this.posY + d0, this.motionZ)) {
                this.motionY = 0.3f;
            }
        } else if (this.isInLava()) {
            double d0 = this.posY;
            this.moveRelative(strafe, 0.0f, forward, 0.02f);
            this.move(MoverType.SELF, this.motionX, this.motionY, this.motionZ);
            this.motionX *= 0.6;
            this.motionY *= 0.6;
            this.motionZ *= 0.6;
            this.motionY -= 0.02;
            if (this.collidedHorizontally && this.isOffsetPositionInLiquid(this.motionX, this.motionY + (double)0.6f - this.posY + d0, this.motionZ)) {
                this.motionY = 0.3f;
            }
        } else {
            float sh;
            if (SpawnLocation.containsOnly(this.baseStats.spawnLocations, SpawnLocation.Water)) {
                return;
            }
            float f2 = 0.91f;
            if (this.onGround) {
                f2 = 0.54600006f;
                IBlockState state = this.world.getBlockState(new BlockPos(MathHelper.floor(this.posX), MathHelper.floor(this.getCollisionBoundingBox().minY) - 1, MathHelper.floor(this.posZ)));
                if (state.getMaterial() != Material.AIR) {
                    f2 = state.getBlock().slipperiness * 0.91f;
                }
            }
            float f3 = 0.16277136f / (f2 * f2 * f2);
            float f4 = this.getRideSpeed();
            if (this.onGround) {
                f4 *= f3 * 2.8f;
            }
            this.moveRelative(strafe, 0.0f, forward, f4);
            f2 = 0.91f;
            if (this.onGround) {
                f2 = 0.54600006f;
                IBlockState state = this.world.getBlockState(new BlockPos(MathHelper.floor(this.posX), MathHelper.floor(this.getCollisionBoundingBox().minY) - 1, MathHelper.floor(this.posZ)));
                if (state.getMaterial() != Material.AIR) {
                    f2 = state.getBlock().slipperiness * 0.91f;
                }
            }
            this.stepHeight = sh = (float)Math.ceil(this.baseStats.height * this.getScaleFactor() / 5.0f);
            this.move(MoverType.PLAYER, this.motionX, this.motionY, this.motionZ);
            double d = this.world.isRemote && (!this.world.isBlockLoaded(new BlockPos((int)this.posX, 0, (int)this.posZ)) || this.world instanceof WorldServer && !((WorldServer)this.world).getChunkProvider().chunkExists(this.chunkCoordX, this.chunkCoordZ)) ? (this.posY > 0.0 ? -0.1 : 0.0) : (this.motionY = this.motionY - 0.04);
            this.motionY = d;
            this.motionY *= (double)0.98f;
            this.motionX *= (double)f2;
            this.motionZ *= (double)f2;
        }
    }

    private float getRideSpeed() {
        return 0.07f + 0.05f * (float)this.stats.Speed / 500.0f;
    }

    @Override
    public void onLivingUpdate() {
        if (this.getControllingPassenger() != null && this.baseStats != null) {
            if (this.baseStats.canSurf) {
                this.getControllingPassenger().setAir(this.initAir);
            }
            float f = this.moveForward = !this.getControllingPassenger().isInWater() && this.baseStats.canSurf ? 0.0f : (this.moveForward = this.moveForward * 0.4f);
            if ((double)this.moveForward > -0.1 && (double)this.moveForward < 0.1) {
                this.moveForward = 0.0f;
            }
            if (!this.isInWater() && this.ridingPlayerVertical > 0) {
                if (this.onGround) {
                    this.jump();
                    this.motionY *= (double)(1.0f + (float)this.stats.Speed / 500.0f);
                } else if (this.baseStats.canFly) {
                    this.motionY += (double)(0.04f + 0.06f * (float)this.stats.Speed / 500.0f);
                    this.isFlying = true;
                }
            } else if (this.isInWater()) {
                if (this.baseStats.canFly && !this.baseStats.canSurf) {
                    this.getControllingPassenger().dismountRidingEntity();
                }
                if (this.ridingPlayerVertical > 0) {
                    this.motionY = this.baseStats.canSurf ? (this.motionY = this.motionY + (double)0.03f) : (this.motionY = this.motionY + (double)0.1f);
                } else if (this.ridingPlayerVertical < 0) {
                    this.motionY -= (double)0.03f;
                }
            }
            this.ridingPlayerVertical = 0;
            if (this.onGround && this.isFlying) {
                this.isFlying = false;
            }
        }
        if (this.getOwner() == null) {
            if (this.world.getClosestPlayerToEntity(this, 24.0) != null) {
                if (this.ticksExisted % PixelmonConfig.tickRate24Radius == 0) {
                    super.onLivingUpdate();
                }
            } else if (this.world.getClosestPlayerToEntity(this, 48.0) != null) {
                if (this.ticksExisted % PixelmonConfig.tickRate48Radius == 0) {
                    super.onLivingUpdate();
                }
            } else if (this.ticksExisted % PixelmonConfig.tickRateHigherRadius == 0) {
                super.onLivingUpdate();
            }
        } else {
            super.onLivingUpdate();
        }
    }

    @Override
    public void onUpdate() {
        super.onUpdate();
        if (this.getControllingPassenger() != null && !this.ridingInitialised) {
            this.tasks.taskEntries.clear();
            this.initAir = this.getControllingPassenger().getAir();
            this.ridingInitialised = true;
        } else if (this.getControllingPassenger() == null && this.ridingInitialised) {
            this.ridingInitialised = false;
            ((EntityPixelmon)this).aiHelper = new AIHelper((EntityPixelmon)this, this.tasks);
        }
    }

    @Override
    public void updatePassenger(Entity passenger) {
        this.debugOffsetX = 0.0f;
        this.debugOffsetY = 0.0f;
        this.debugOffsetZ = 0.0f;
        if (this.baseStats != null) {
            try {
                Vector3f offsets;
                if (this.baseStats.ridingOffsets == null) {
                    this.baseStats.ridingOffsets = new RidingOptions();
                }
                Vector3f vector3f = offsets = this.baseStats.ridingOffsets.standing != null ? this.baseStats.ridingOffsets.standing : new Vector3f();
                if (this.baseStats.ridingOffsets.moving != null && (this.flyingDelayCounter >= flyingDelayLimit || this.getUsingRidingSpecialConditions())) {
                    offsets = this.baseStats.ridingOffsets.moving;
                }
                float zOffset = 0.0f;
                float yOffset = 0.0f;
                if (this.isPokemon(EnumSpecies.Zapdos) && this.getSpecialTexture().equals("-classic")) {
                    yOffset = 1.0f;
                    zOffset = -0.5f;
                }
                Vec3d vec = new Vec3d((this.debugOffsetX + offsets.x) * this.getPixelmonScale() * this.getScaleFactor(), (offsets.y + this.height + this.debugOffsetY + yOffset) * this.getPixelmonScale() * this.getScaleFactor(), (this.debugOffsetZ + offsets.z + zOffset) * this.getPixelmonScale() * this.getScaleFactor());
                vec = vec.rotateYaw(-1.0f * this.renderYawOffset * (float)Math.PI / 180.0f);
                passenger.setPosition(this.posX + vec.x, this.posY + vec.y, this.posZ + vec.z);
            }
            catch (Exception e) {
                passenger.dismountRidingEntity();
            }
        }
    }

    private boolean getUsingRidingSpecialConditions() {
        return (Math.abs(this.motionX) > 0.01 || Math.abs(this.motionZ) > 0.01) && (this.baseStats.pokemon == EnumSpecies.Gyarados ? this.isInWater() : this.baseStats.pokemon == EnumSpecies.Onix);
    }

    @Override
    public boolean shouldDismountInWater(Entity rider) {
        return false;
    }

    public void unloadEntity() {
        if (this.getControllingPassenger() != null) {
            this.getControllingPassenger().dismountRidingEntity();
            ((EntityPixelmon)this).aiHelper = new AIHelper((EntityPixelmon)this, this.tasks);
        }
    }

    @Override
    public Entity getControllingPassenger() {
        return this.getPassengers().isEmpty() ? null : this.getPassengers().get(0);
    }
}

