/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Hail;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.enums.forms.EnumEiscue;

public class IceFace
extends AbilityBase {
    private boolean canProtect = true;

    @Override
    public int modifyDamageIncludeFixed(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (this.canProtect && damage > 0 && a.getAttackCategory() == AttackCategory.Physical && target.getSpecies() == EnumSpecies.Eiscue && target.getForm() == EnumEiscue.Ice.getForm()) {
            if (!target.bc.simulateMode) {
                this.canProtect = false;
                target.setForm(EnumEiscue.Noice.getForm());
                target.bc.sendToAll("pixelmon.abilities.iceface.noice", target.getNickname());
            }
            return 0;
        }
        return damage;
    }

    public boolean getProtected() {
        return this.canProtect;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        if (pokemon.bc.globalStatusController.getWeatherIgnoreAbility() instanceof Hail && pokemon.getSpecies() == EnumSpecies.Eiscue && pokemon.getForm() == EnumEiscue.Noice.getForm()) {
            pokemon.setForm(EnumEiscue.Ice.getForm());
            this.canProtect = true;
            pokemon.bc.sendToAll("pixelmon.abilities.iceface.ice", pokemon.getNickname());
        }
    }

    @Override
    public void applyEndOfBattleEffect(PixelmonWrapper pokemon) {
        if (pokemon.getSpecies() == EnumSpecies.Eiscue) {
            pokemon.setForm(EnumEiscue.Ice.getForm());
        }
    }
}

