/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.externalMoves;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.enums.EnumType;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.WeightedRandom;

public class ForageItem
extends WeightedRandom.Item {
    private Block[] blocks;
    private ItemStack item;
    private EnumType[] types;
    private Material material;

    public ForageItem(int itemWeightIn) {
        super(itemWeightIn);
    }

    public ForageItem(ItemStack item, Block block, int itemWeightIn, EnumType ... types) {
        super(itemWeightIn);
        this.blocks = new Block[]{block};
        this.types = types;
        this.item = item;
    }

    public ForageItem(Item item, Block block, int itemWeightIn, EnumType ... types) {
        this(new ItemStack(item), block, itemWeightIn, types);
    }

    public ForageItem(Item item, Block[] blocks, int itemWeightIn, EnumType ... types) {
        super(itemWeightIn);
        this.blocks = blocks;
        this.types = types;
        this.item = new ItemStack(item);
    }

    public ForageItem(Item item, Material material, int itemWeightIn, EnumType ... types) {
        super(itemWeightIn);
        this.material = material;
        this.types = types;
        this.item = new ItemStack(item);
    }

    public ForageItem(ItemStack item, Material material, int itemWeightIn, EnumType ... types) {
        super(itemWeightIn);
        this.material = material;
        this.types = types;
        this.item = item;
    }

    public boolean shouldAdd(EntityPixelmon user, Block block) {
        if (this.types != null && !this.hasType(user.baseStats.type1)) {
            if (user.baseStats.type2 != null) {
                if (!this.hasType(user.baseStats.type2)) {
                    return false;
                }
            } else {
                return false;
            }
        }
        if (this.blocks != null && !this.hasBlock(block)) {
            return false;
        }
        return this.material == null || this.material == block.getMaterial(block.getDefaultState());
    }

    private boolean hasBlock(Block block) {
        for (Block b : this.blocks) {
            if (b != block) continue;
            return true;
        }
        return false;
    }

    private boolean hasType(EnumType type) {
        for (EnumType t : this.types) {
            if (t != type) continue;
            return true;
        }
        return false;
    }

    public ItemStack getItem() {
        return this.item;
    }
}

