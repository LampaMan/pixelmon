/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Rainy;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;

public class Hydration
extends AbilityBase {
    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        if (pokemon.bc.globalStatusController.getWeather() instanceof Rainy && pokemon.hasPrimaryStatus() && pokemon.getHeldItem() != PixelmonItemsHeld.utilityUmbrella) {
            this.sendActivatedMessage(pokemon);
            pokemon.removePrimaryStatus();
        }
    }
}

