/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.particleEffects;

import com.pixelmongenerations.common.entity.pixelmon.Entity4Textures;
import com.pixelmongenerations.common.entity.pixelmon.particleEffects.ParticleEffects;
import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.enums.EnumPixelmonParticles;
import com.pixelmongenerations.core.proxy.ClientProxy;
import net.minecraft.util.math.MathHelper;

public class GastlyParticles
extends ParticleEffects {
    public GastlyParticles(Entity4Textures pixelmon) {
        super(pixelmon);
    }

    @Override
    public void onUpdate() {
        float var2 = 0.6f;
        float var4 = this.rand.nextFloat() * (float)Math.PI * 2.0f;
        float var42 = this.rand.nextFloat() * (float)Math.PI * 2.0f;
        float var5 = this.rand.nextFloat() * 1.0f + 0.5f;
        float var52 = this.rand.nextFloat() * 1.0f + 0.5f;
        float var6 = MathHelper.sin(var4) * var2 * 0.7f * var5;
        float var62 = MathHelper.sin(var42) * var2 * 0.7f * var52;
        float var7 = MathHelper.cos(var4) * var2 * 0.5f * var5;
        float var72 = MathHelper.cos(var42) * var2 * 0.5f * var52;
        float var8 = this.rand.nextFloat() * var2 * 1.2f;
        float var82 = this.rand.nextFloat() * var2 * 1.2f;
        if (this.pixelmon.getGrowth() == EnumGrowth.Enormous) {
            int i;
            for (i = 0; i < 20; ++i) {
                ClientProxy.spawnParticle(EnumPixelmonParticles.Gastly, this.pixelmon.world, this.pixelmon.posX + (double)var6 + (double)0.2f, this.pixelmon.posY + 2.0 + (double)var8, this.pixelmon.posZ + (double)var7 + (double)0.4f, this.pixelmon.isShiny());
            }
            for (i = 0; i < 20; ++i) {
                ClientProxy.spawnParticle(EnumPixelmonParticles.Gastly, this.pixelmon.world, this.pixelmon.posX + (double)var62 + (double)0.2f, this.pixelmon.posY + 2.0 + (double)var82, this.pixelmon.posZ + (double)var72 - (double)0.6f, this.pixelmon.isShiny());
            }
        } else if (this.pixelmon.getGrowth() == EnumGrowth.Huge) {
            int i;
            for (i = 0; i < 20; ++i) {
                ClientProxy.spawnParticle(EnumPixelmonParticles.Gastly, this.pixelmon.world, this.pixelmon.posX + (double)var6 + (double)0.2f, this.pixelmon.posY + 1.5 + (double)var8, this.pixelmon.posZ + (double)var7 + (double)0.4f, this.pixelmon.isShiny());
            }
            for (i = 0; i < 20; ++i) {
                ClientProxy.spawnParticle(EnumPixelmonParticles.Gastly, this.pixelmon.world, this.pixelmon.posX + (double)var62 + (double)0.2f, this.pixelmon.posY + 1.5 + (double)var82, this.pixelmon.posZ + (double)var72 - (double)0.6f, this.pixelmon.isShiny());
            }
        } else if (this.pixelmon.getGrowth() == EnumGrowth.Pygmy) {
            for (int i = 0; i < 20; ++i) {
                ClientProxy.spawnParticle(EnumPixelmonParticles.Gastly, this.pixelmon.world, this.pixelmon.posX + (double)var6 + (double)0.2f, this.pixelmon.posY + (double)var8 + 0.6, this.pixelmon.posZ + (double)var7 + (double)0.4f, this.pixelmon.isShiny());
            }
        } else {
            int i;
            for (i = 0; i < 20; ++i) {
                ClientProxy.spawnParticle(EnumPixelmonParticles.Gastly, this.pixelmon.world, this.pixelmon.posX + (double)var6 + (double)0.2f, this.pixelmon.posY + (double)1.2f + (double)var8, this.pixelmon.posZ + (double)var7 + (double)0.4f, this.pixelmon.isShiny());
            }
            for (i = 0; i < 20; ++i) {
                ClientProxy.spawnParticle(EnumPixelmonParticles.Gastly, this.pixelmon.world, this.pixelmon.posX + (double)var62 + (double)0.2f, this.pixelmon.posY + (double)1.2f + (double)var82, this.pixelmon.posZ + (double)var72 - (double)0.6f, this.pixelmon.isShiny());
            }
        }
    }
}

