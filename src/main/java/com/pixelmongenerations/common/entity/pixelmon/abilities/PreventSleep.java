/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.PreventStatus;

public class PreventSleep
extends PreventStatus {
    public PreventSleep(String immuneText, String cureText) {
        super(immuneText, cureText, StatusType.Sleep);
    }

    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        if (a.isAttack("Yawn")) {
            user.bc.sendToAll(this.immuneText, pokemon.getNickname());
            return false;
        }
        return true;
    }
}

