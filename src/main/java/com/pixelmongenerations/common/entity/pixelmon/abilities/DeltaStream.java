/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StrongWinds;
import com.pixelmongenerations.common.entity.pixelmon.abilities.WeatherTrio;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.stream.Stream;

public class DeltaStream
extends WeatherTrio {
    public DeltaStream() {
        super(new StrongWinds());
    }

    @Override
    public int modifyDamageTarget(int damage, PixelmonWrapper user, PixelmonWrapper pokemon, Attack a) {
        double effectiveness = a.getTypeEffectiveness(user, pokemon);
        if (effectiveness == 4.0) {
            if (this.isAny(user.attack.getAttackBase().attackType, EnumType.Electric, EnumType.Ice, EnumType.Rock) && pokemon.type.contains((Object)EnumType.Flying)) {
                damage = (int)((double)damage * 0.5);
                user.bc.sendToAll("pixelmon.effect.strongwindsweaken", new Object[0]);
            }
        }
        if (effectiveness == 2.0) {
            if (this.isAny(user.attack.getAttackBase().attackType, EnumType.Electric, EnumType.Ice, EnumType.Rock) && pokemon.type.contains((Object)EnumType.Flying)) {
                damage = (int)((double)damage * 0.5);
                user.bc.sendToAll("pixelmon.effect.strongwindsweaken", new Object[0]);
            }
        }
        if (effectiveness == 1.0) {
            if (this.isAny(user.attack.getAttackBase().attackType, EnumType.Electric, EnumType.Ice, EnumType.Rock) && pokemon.type.contains((Object)EnumType.Flying)) {
                damage = (int)((double)damage * 0.5);
                user.bc.sendToAll("pixelmon.effect.strongwindsweaken", new Object[0]);
            }
        }
        return damage;
    }

    private boolean isAny(EnumType type, EnumType ... types) {
        return Stream.of(types).anyMatch(t -> type == t);
    }
}

