/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class SpeedBoost
extends AbilityBase {
    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        if (!pokemon.switchedThisTurn) {
            this.sendActivatedMessage(pokemon);
            pokemon.getBattleStats().modifyStat(1, StatsType.Speed);
        }
    }
}

