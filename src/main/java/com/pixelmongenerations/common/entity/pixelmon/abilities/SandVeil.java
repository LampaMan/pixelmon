/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Sandstorm;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class SandVeil
extends AbilityBase {
    @Override
    public int[] modifyStatsCancellable(PixelmonWrapper user, int[] stats) {
        if (user.bc.globalStatusController.getWeather() instanceof Sandstorm) {
            int[] arrn = stats;
            int n = StatsType.Evasion.getStatIndex();
            arrn[n] = (int)((double)arrn[n] * 1.25);
        }
        return stats;
    }
}

