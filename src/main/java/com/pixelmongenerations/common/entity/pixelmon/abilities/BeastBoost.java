/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.google.common.collect.Lists;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.GuardSplit;
import com.pixelmongenerations.common.battle.status.PowerSplit;
import com.pixelmongenerations.common.battle.status.PowerTrick;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.WonderRoom;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Mummy;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class BeastBoost
extends AbilityBase {
    @Override
    public void tookDamageUser(int damage, PixelmonWrapper user, PixelmonWrapper pokemon, Attack a) {
        if (!(!pokemon.isFainted() || a.getAttackBase().getMakesContact() && pokemon.getBattleAbility() instanceof Mummy)) {
            this.sendActivatedMessage(user);
            int[] stats = user.getBattleStats().getBaseBattleStats();
            for (StatusBase statusBase : user.getStatuses()) {
                if (!(statusBase instanceof PowerSplit) && !(statusBase instanceof GuardSplit) && !(statusBase instanceof PowerTrick)) continue;
                stats = statusBase.modifyBaseStats(user, stats);
            }
            for (GlobalStatusBase globalStatusBase : user.bc.globalStatusController.getGlobalStatuses()) {
                if (!(globalStatusBase instanceof WonderRoom)) continue;
                globalStatusBase.modifyBaseStats(user, stats);
            }
            StatsType highestType = null;
            int highestAmount = 0;
            for (StatsType type : Lists.newArrayList(StatsType.Attack, StatsType.Defence, StatsType.SpecialAttack, StatsType.SpecialDefence, StatsType.Speed)) {
                if (highestType != null && stats[type.getStatIndex()] <= highestAmount) continue;
                highestType = type;
                highestAmount = stats[type.getStatIndex()];
            }
            if (user.getBattleStats().statCanBeRaised(highestType)) {
                user.getBattleStats().modifyStat(1, highestType);
            }
        }
    }
}

