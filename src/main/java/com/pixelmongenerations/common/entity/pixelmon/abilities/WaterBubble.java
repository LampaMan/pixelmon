/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;

public class WaterBubble
extends AbilityBase {
    @Override
    public int modifyDamageTarget(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (a.getAttackBase().attackType == EnumType.Fire) {
            return damage / 2;
        }
        return damage;
    }

    @Override
    public int modifyDamageUser(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (a.getAttackBase().attackType == EnumType.Water) {
            return damage * 2;
        }
        return damage;
    }

    @Override
    public boolean allowsStatus(StatusType status, PixelmonWrapper pokemon, PixelmonWrapper user) {
        return status != StatusType.Burn;
    }
}

