/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;

public class StatRatioCondition
extends EvoCondition {
    public StatsType stat1;
    public StatsType stat2;
    public float ratio = 1.0f;

    public StatRatioCondition() {
    }

    public StatRatioCondition(StatsType stat1, StatsType stat2, float ratio) {
        this.stat1 = stat1;
        this.stat2 = stat2;
        this.ratio = ratio;
    }

    @Override
    public boolean passes(EntityPixelmon pokemon) {
        return 1.0f * (float)pokemon.stats.get(this.stat1) / (float)pokemon.stats.get(this.stat2) > this.ratio;
    }
}

