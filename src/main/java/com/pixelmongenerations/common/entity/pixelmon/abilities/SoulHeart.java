/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class SoulHeart
extends AbilityBase {
    @Override
    public void onPokemonFaintOther(PixelmonWrapper user, PixelmonWrapper pokemon) {
        user.getBattleStats().modifyStat(1, StatsType.SpecialAttack);
        user.getBattleAbility().sendActivatedMessage(user);
    }
}

