/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.entity.pixelmon.abilities.PreventSleep;

public class Insomnia
extends PreventSleep {
    public Insomnia() {
        super("pixelmon.abilities.insomnia", "pixelmon.abilities.insomniacure");
    }
}

