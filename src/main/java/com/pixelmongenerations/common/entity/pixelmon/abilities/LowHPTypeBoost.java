/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;

public class LowHPTypeBoost
extends AbilityBase {
    private EnumType type;

    protected LowHPTypeBoost(EnumType type) {
        this.type = type;
    }

    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (user.getHealthPercent() <= 33.333332f && a.getAttackBase().attackType == this.type) {
            power = (int)((double)power * 1.5);
        }
        return new int[]{power, accuracy};
    }
}

