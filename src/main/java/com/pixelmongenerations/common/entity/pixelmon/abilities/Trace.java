/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AsOne;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Comatose;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ComingSoon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Disguise;
import com.pixelmongenerations.common.entity.pixelmon.abilities.FakemonRevealed;
import com.pixelmongenerations.common.entity.pixelmon.abilities.FlowerGift;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Forecast;
import com.pixelmongenerations.common.entity.pixelmon.abilities.GulpMissile;
import com.pixelmongenerations.common.entity.pixelmon.abilities.HungerSwitch;
import com.pixelmongenerations.common.entity.pixelmon.abilities.IceFace;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Illusion;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Imposter;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Mimicry;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Multitype;
import com.pixelmongenerations.common.entity.pixelmon.abilities.NeutralizingGas;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Schooling;
import com.pixelmongenerations.common.entity.pixelmon.abilities.StanceChange;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ZenMode;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;

public class Trace
extends AbilityBase {
    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        ArrayList<PixelmonWrapper> opponents = newPokemon.getOpponentPokemon();
        while (!opponents.isEmpty()) {
            PixelmonWrapper opponent = RandomHelper.getRandomElementFromList(opponents);
            AbilityBase targetAbility = opponent.getBattleAbility(false);
            if (this.canTrace(targetAbility)) {
                newPokemon.bc.sendToAll("pixelmon.abilities.trace", newPokemon.getNickname(), opponent.getNickname(), targetAbility.getLocalizedName());
                newPokemon.setTempAbility(targetAbility);
                newPokemon.bc.modifyStats();
                return;
            }
            opponents.remove(opponent);
        }
    }

    private boolean canTrace(AbilityBase ability) {
        return !(ability instanceof ComingSoon) && !(ability instanceof AsOne) && !(ability instanceof Trace) && !(ability instanceof Multitype) && !(ability instanceof Illusion) && !(ability instanceof FlowerGift) && !(ability instanceof Imposter) && !(ability instanceof StanceChange) && !(ability instanceof Comatose) && !(ability instanceof Disguise) && !(ability instanceof Forecast) && !(ability instanceof Schooling) && !(ability instanceof ZenMode) && !(ability instanceof GulpMissile) && !(ability instanceof IceFace) && !(ability instanceof FakemonRevealed) && !(ability instanceof HungerSwitch) && !(ability instanceof NeutralizingGas) && !(ability instanceof Mimicry);
    }
}

