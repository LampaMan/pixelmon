/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SheerForce;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.ArrayList;

public class ColorChange
extends AbilityBase {
    @Override
    public void tookDamageTargetAfterMove(PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if ((!(user.getBattleAbility() instanceof SheerForce) || a.getAttackBase().hasSecondaryEffect()) && target.isAlive()) {
            ArrayList<EnumType> newType = new ArrayList<EnumType>(2);
            EnumType changeType = a.getAttackBase().attackType;
            if (changeType == EnumType.Mystery) {
                changeType = EnumType.Normal;
            }
            newType.add(changeType);
            newType.add(null);
            if (!target.type.equals(newType)) {
                PixelmonWrapper targetWrapper = target;
                targetWrapper.bc.sendToAll("pixelmon.abilities.colourchange", new Object[]{target.getNickname(), a.getAttackBase().attackType});
                targetWrapper.setTempType(newType);
            }
        }
    }
}

