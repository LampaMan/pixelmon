/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity;

import com.pixelmongenerations.api.DropQueryFactory;
import com.pixelmongenerations.api.HexColor;
import com.pixelmongenerations.common.entity.pixelmon.drops.DroppedItem;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.util.PixelSounds;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityDynamaxEnergy
extends EntityLiving {
    private int ticks = 0;

    public EntityDynamaxEnergy(World world) {
        super(world);
        this.setSize(1.0f, 255.0f);
    }

    @Override
    protected boolean processInteract(EntityPlayer player, EnumHand hand) {
        if (!this.world.isRemote) {
            DropQueryFactory.newInstance().customTitle(new TextComponentTranslation(HexColor.formatHex("You found a #f73995Max Powder!"), new Object[0])).addDrop(DroppedItem.of(PixelmonItems.maxPowder)).addTarget((EntityPlayerMP)player).send();
        }
        this.world.playSound(null, this.getPosition(), PixelSounds.zygardeCell, SoundCategory.BLOCKS, 0.5f, 1.0f);
        this.setDead();
        return true;
    }

    @Override
    public boolean attackEntityFrom(DamageSource source, float par2) {
        return false;
    }

    @Override
    public boolean canBePushed() {
        return false;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public void onUpdate() {
        this.setRotation(0.0f, 0.0f);
        ++this.ticks;
        if (this.ticks > 2) {
            BlockPos loc2 = this.getPosition();
            Vec3d loc = new Vec3d(loc2.getX(), loc2.getY(), loc2.getZ()).add(0.6, 0.0, 0.6);
            int radius = 1;
            for (double y = 0.0; y <= 22.0; y += 0.2) {
                int n = this.ticks;
                double x = (double)radius * Math.cos(y) * (Math.cos(this.ticksExisted) * (double)n) / 4.0;
                double z = (double)radius * Math.sin(y) * (Math.sin(this.ticksExisted) * (double)n) / 4.0;
                Pixelmon.PROXY.spawnParticle(EnumParticleTypes.REDSTONE.getParticleID(), (float)(loc.x + x), (float)(loc.y + y), (float)(loc.z + z), 1.0, 0.0, 1.0, 0, 0, 0);
            }
            this.ticks = 0;
        }
    }
}

