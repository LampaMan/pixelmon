/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.base.Optional
 */
package com.pixelmongenerations.common.entity.pokeballs;

import com.pixelmongenerations.api.events.CaptureEvent;
import com.pixelmongenerations.api.events.PokeballEffectEvent;
import com.pixelmongenerations.client.models.pokeballs.ModelPokeballs;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.helpers.animation.IncrementingVariable;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.EntityLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.WrapperLink;
import com.pixelmongenerations.common.entity.pokeballs.EnumPokeBallMode;
import com.pixelmongenerations.common.entity.pokeballs.PokeballTypeHelper;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonItemsPokeballs;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelSounds;
import com.pixelmongenerations.core.util.PixelmonPlayerUtils;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.item.Item;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class EntityPokeBall
extends EntityThrowable {
    public static final DataParameter<Integer> dwPokeballType = EntityDataManager.createKey(EntityPokeBall.class, DataSerializers.VARINT);
    public static final DataParameter<Boolean> dwIsWaiting = EntityDataManager.createKey(EntityPokeBall.class, DataSerializers.BOOLEAN);
    public static final DataParameter<Boolean> dwIsOnGround = EntityDataManager.createKey(EntityPokeBall.class, DataSerializers.BOOLEAN);
    public static final DataParameter<Boolean> dwCriticalCapture = EntityDataManager.createKey(EntityPokeBall.class, DataSerializers.BOOLEAN);
    public static final DataParameter<Float> dwInitialYaw = EntityDataManager.createKey(EntityPokeBall.class, DataSerializers.FLOAT);
    public static final DataParameter<String> dwAnimation = EntityDataManager.createKey(EntityPokeBall.class, DataSerializers.STRING);
    public static final DataParameter<Integer> dwId = EntityDataManager.createKey(EntityPokeBall.class, DataSerializers.VARINT);
    public static final DataParameter<Float> dwInitialPitch = EntityDataManager.createKey(EntityPokeBall.class, DataSerializers.FLOAT);
    public static final DataParameter<Integer> dwMode = EntityDataManager.createKey(EntityPokeBall.class, DataSerializers.VARINT);
    protected static final DataParameter<com.google.common.base.Optional<UUID>> dwOwner = EntityDataManager.createKey(EntityPokeBall.class, DataSerializers.OPTIONAL_UNIQUE_ID);
    protected static final DataParameter<Integer> dwPokeId1 = EntityDataManager.createKey(EntityPokeBall.class, DataSerializers.VARINT);
    protected static final DataParameter<Integer> dwPokeId2 = EntityDataManager.createKey(EntityPokeBall.class, DataSerializers.VARINT);
    protected final int ticksPerShake = 25;
    public int waitTimer;
    public static float scale = 0.0033333334f;
    protected EntityLivingBase thrower;
    public EntityPixelmon pixelmon;
    protected float endRotationYaw = 0.0f;
    public boolean dropItem;
    protected boolean canCatch = false;
    public float openAngle = 0.0f;
    String name = null;
    Vec3d initPos;
    Vec3d diff;
    float initialScale;
    public IncrementingVariable inc;
    public boolean firstContact = true;
    public boolean pausing = false;
    public String lastAnim = "";
    public int shakeCount = 0;
    private boolean startCapture = true;
    private static final String[] ANIMS = new String[]{"shakeLeft", "shakeRight"};
    private boolean capture1 = false;
    private boolean capture2 = false;
    int numShakes = 0;
    ModelPokeballs model = null;

    public EntityPokeBall(World world) {
        super(world);
        this.dataManager.register(dwPokeballType, EnumPokeball.PokeBall.getIndex());
        this.dataManager.register(dwIsWaiting, false);
        this.dataManager.register(dwIsOnGround, false);
        this.dataManager.register(dwInitialYaw, Float.valueOf(0.0f));
        this.dataManager.register(dwInitialPitch, Float.valueOf(0.0f));
        this.dataManager.register(dwAnimation, "idle");
        this.dataManager.register(dwId, 0);
        this.dataManager.register(dwMode, 0);
        this.dataManager.register(dwOwner, com.google.common.base.Optional.absent());
        this.dataManager.register(dwPokeId1, 0);
        this.dataManager.register(dwPokeId2, 0);
        this.dataManager.register(dwCriticalCapture, false);
    }

    public EntityPokeBall(EnumPokeball type, World world, EntityLivingBase thrower, EnumPokeBallMode mode) {
        super(world, thrower);
        this.dataManager.register(dwPokeballType, type.getIndex());
        this.dataManager.register(dwIsWaiting, false);
        this.dataManager.register(dwIsOnGround, false);
        this.dataManager.register(dwInitialYaw, Float.valueOf(0.0f));
        this.dataManager.register(dwInitialPitch, Float.valueOf(0.0f));
        this.dataManager.register(dwAnimation, "idle");
        this.dataManager.register(dwId, 0);
        this.dataManager.register(dwMode, mode.ordinal());
        this.dataManager.register(dwOwner, com.google.common.base.Optional.absent());
        this.dataManager.register(dwPokeId1, 0);
        this.dataManager.register(dwPokeId2, 0);
        this.dataManager.register(dwCriticalCapture, false);
    }

    @Override
    public String getName() {
        if (this.name == null) {
            this.name = PixelmonItemsPokeballs.getItemFromEnum(this.getType()).getLocalizedName();
        }
        return this.name;
    }

    @Override
    protected void onImpact(RayTraceResult movingobjectposition) {
    }

    public Item breakBall() {
        return PixelmonItemsPokeballs.getLidFromEnum(this.getType());
    }

    @Override
    public void onUpdate() {
        super.onUpdate();
        if (this.world.isRemote && this.model != null) {
            if (this.inc == null) {
                this.inc = new IncrementingVariable(1.0f, 2.14748365E9f);
            }
            this.inc.tick();
            this.model.doAnimation(this);
        }
        if (this.getMode() == EnumPokeBallMode.FULL && this.getIsWaiting()) {
            this.rotationYaw = -1.0f * this.getInitialYaw();
            if (this.waitTimer > 0) {
                --this.waitTimer;
            }
            if (this.firstContact) {
                this.motionY = 0.3;
                this.firstContact = false;
            } else if (this.motionY <= 0.0 && !this.pausing) {
                this.waitTimer = 2;
                this.pausing = true;
            }
            if (this.waitTimer > 0) {
                this.motionY = 0.0;
            } else if (this.pausing && this.waitTimer == 0) {
                EntityPixelmon pixelmon;
                PlayerStorage storage;
                Optional<PlayerStorage> optstorage;
                if (!this.getAnimation().equalsIgnoreCase("bounceClose")) {
                    this.motionY = 0.2;
                    this.motionX = 0.2 * (double)((float)Math.cos((double)this.getInitialYaw() * Math.PI / 180.0 - 1.5707963267948966));
                    this.motionZ = 0.2 * (double)((float)Math.sin((double)this.getInitialYaw() * Math.PI / 180.0 - 1.5707963267948966));
                    this.setAnimation("bounceClose");
                    if (this.world.isRemote) {
                        this.world.playSound(this.posX, this.posY, this.posZ, PixelSounds.pokeballClose, SoundCategory.NEUTRAL, 0.1f, 1.0f, true);
                        this.world.playSound(this.posX, this.posY, this.posZ, PixelSounds.pokeballRelease, SoundCategory.NEUTRAL, 0.2f, 1.0f, true);
                    }
                }
                if (!this.world.isRemote && (optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)this.thrower)).isPresent() && !(storage = optstorage.get()).isInWorld(this.getPokeId()) && (pixelmon = storage.sendOut(this.getPokeId(), this.world)) != null) {
                    pixelmon.setLocationAndAngles(this.posX, this.posY, this.posZ, this.rotationYaw, 0.0f);
                    pixelmon.releaseFromPokeball();
                }
                --this.waitTimer;
            }
        }
        if ((this.getMode() == EnumPokeBallMode.EMPTY || this.getMode() == EnumPokeBallMode.BATTLE && !this.getIsOnGround()) && this.getIsWaiting()) {
            if (this.waitTimer > 0) {
                --this.waitTimer;
            }
            if (this.firstContact) {
                this.motionY = 0.3;
                this.firstContact = false;
            } else if (this.motionY <= 0.0 && !this.pausing) {
                this.waitTimer = 25;
                this.pausing = true;
                if (!this.world.isRemote) {
                    this.initialScale = this.pixelmon.getPixelmonScale();
                    this.initPos = new Vec3d(this.pixelmon.posX, this.pixelmon.posY, this.pixelmon.posZ);
                    Vec3d current = new Vec3d(this.posX, this.posY, this.posZ);
                    current.subtract(this.initPos);
                    this.diff = current;
                }
            }
            if (this.waitTimer > 0) {
                this.motionY = 0.0;
                if (this.waitTimer < 20 && !this.world.isRemote) {
                    if (this.startCapture) {
                        this.world.playSound(this.posX, this.posY, this.posZ, PixelSounds.pokeballCapture, SoundCategory.NEUTRAL, 0.2f, 1.0f, true);
                        this.startCapture = false;
                    }
                    this.pixelmon.setPixelmonScale(this.initialScale * (float)Math.pow(0.5, (20 - this.waitTimer) / 5));
                    this.moveCloser((float)Math.pow(0.5, (20 - this.waitTimer) / 5));
                }
                if (this.waitTimer == 1 && !this.world.isRemote) {
                    this.pixelmon.unloadEntity();
                    this.setAnimation("bounceClose");
                    this.world.playSound(this.posX, this.posY, this.posZ, PixelSounds.pokeballClose, SoundCategory.NEUTRAL, 0.1f, 1.0f, true);
                }
            }
        }
        if (!this.getIsWaiting() && !this.getIsOnGround()) {
            this.rotationYaw += 50.0f;
        } else if (this.getIsOnGround()) {
            this.motionY = 0.0;
            if (this.getMode() == EnumPokeBallMode.EMPTY || this.getMode() == EnumPokeBallMode.BATTLE && this.world.isRemote) {
                if (this.waitTimer > 0) {
                    --this.waitTimer;
                }
                if (this.waitTimer <= 0 && this.shakeCount < Math.abs(this.getId())) {
                    ++this.shakeCount;
                    this.chooseAnimation();
                    if (this.inc != null) {
                        this.inc.value = 0.0f;
                    }
                    this.waitTimer = 25;
                }
            }
        }
    }

    @Override
    public boolean writeToNBTOptional(NBTTagCompound compound) {
        return false;
    }

    private void chooseAnimation() {
        this.setAnimation(ANIMS[RandomHelper.getRandomNumberBetween(0, ANIMS.length - 1)]);
    }

    protected void moveCloser(float percent) {
        Vec3d newVec = this.initPos.add(this.diff.x * (double)(1.0f - percent), this.diff.y * (double)(1.0f - percent), this.diff.z * (double)(1.0f - percent));
        this.pixelmon.posX = newVec.x;
        this.pixelmon.lastTickPosX = newVec.x;
        this.pixelmon.posY = newVec.y;
        this.pixelmon.lastTickPosY = newVec.y;
        this.pixelmon.posZ = newVec.z;
        this.pixelmon.lastTickPosZ = newVec.z;
    }

    @Override
    public void onEntityUpdate() {
        if (!this.world.isRemote && this.posY < 0.0) {
            this.onKillCommand();
        } else if (this.getIsOnGround()) {
            this.motionY = 0.0;
            this.motionX = 0.0;
            this.motionZ = 0.0;
        }
    }

    public void premierFlash() {
        if (this.getType() == EnumPokeball.PremierBall) {
            this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX + 0.1, this.posY + 0.05, this.posZ, 255.0, 250.0, 250.0, new int[0]);
        }
    }

    public void onCaptureAttemptEffect() {
        if (!MinecraftForge.EVENT_BUS.post(new PokeballEffectEvent.StartCaptureEffect(this)) && !this.capture2 && this.pausing) {
            if (this.getType() == EnumPokeball.PremierBall) {
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX, this.posY + 0.4, this.posZ, 255.0, 250.0, 250.0, new int[0]);
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX, this.posY - 0.4, this.posZ, 255.0, 250.0, 250.0, new int[0]);
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX + 0.2, this.posY + 0.2, this.posZ, 255.0, 250.0, 250.0, new int[0]);
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX - 0.2, this.posY + 0.2, this.posZ, 255.0, 250.0, 250.0, new int[0]);
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX + 0.2, this.posY - 0.2, this.posZ, 255.0, 250.0, 250.0, new int[0]);
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX - 0.2, this.posY - 0.2, this.posZ, 255.0, 250.0, 250.0, new int[0]);
            }
            if (this.getType() == EnumPokeball.MasterBall) {
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX, this.posY + 0.4, this.posZ, 148.0, 0.0, 211.0, new int[0]);
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX, this.posY - 0.4, this.posZ, 148.0, 0.0, 211.0, new int[0]);
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX + 0.2, this.posY + 0.2, this.posZ, 148.0, 0.0, 211.0, new int[0]);
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX - 0.2, this.posY + 0.2, this.posZ, 148.0, 0.0, 211.0, new int[0]);
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX + 0.2, this.posY - 0.2, this.posZ, 148.0, 0.0, 211.0, new int[0]);
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX - 0.2, this.posY - 0.2, this.posZ, 148.0, 0.0, 211.0, new int[0]);
            }
            this.capture2 = true;
        }
    }

    public void successfulCaptureEffect() {
        if (!MinecraftForge.EVENT_BUS.post(new PokeballEffectEvent.SuccessfullCaptureEffect(this)) && !this.capture1) {
            this.world.playSound(this.posX, this.posY, this.posZ, PixelSounds.pokeballCaptureSuccess, SoundCategory.NEUTRAL, 0.07f, 1.0f, false);
            if (this.getType() == EnumPokeball.PremierBall) {
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX, this.posY + 0.8, this.posZ, 255.0, 250.0, 250.0, new int[0]);
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX + 0.5, this.posY + 0.6, this.posZ, 255.0, 250.0, 250.0, new int[0]);
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX - 0.5, this.posY + 0.6, this.posZ, 255.0, 250.0, 250.0, new int[0]);
            } else if (this.isCriticalCapture()) {
                ClientProxy.spawnParticle("shiny", this.world, this.posX, this.posY + 0.8, this.posZ, 0xFFFFFF, 255);
                ClientProxy.spawnParticle("shiny", this.world, this.posX + 0.5, this.posY + 0.6, this.posZ, 0xFFFFFF, 255);
                ClientProxy.spawnParticle("shiny", this.world, this.posX - 0.5, this.posY + 0.6, this.posZ, 0xFFFFFF, 255);
            } else {
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX, this.posY, this.posZ, 255.0, 255.0, 0.0, new int[0]);
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX + 0.5, this.posY, this.posZ, 255.0, 255.0, 0.0, new int[0]);
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX - 0.5, this.posY, this.posZ, 255.0, 255.0, 0.0, new int[0]);
            }
            this.capture1 = true;
        }
    }

    public void releaseEffect() {
        if (!MinecraftForge.EVENT_BUS.post(new PokeballEffectEvent.SentOutEffect(this)) && !this.capture1) {
            if (this.getType() == EnumPokeball.PremierBall) {
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX, this.posY + 0.4, this.posZ, 0.0, 0.0, 0.0, new int[0]);
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX, this.posY - 0.4, this.posZ, 0.0, 0.0, 0.0, new int[0]);
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX + 0.2, this.posY + 0.2, this.posZ, 0.0, 0.0, 0.0, new int[0]);
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX - 0.2, this.posY + 0.2, this.posZ, 0.0, 0.0, 0.0, new int[0]);
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX + 0.2, this.posY - 0.2, this.posZ, 0.0, 0.0, 0.0, new int[0]);
                this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX - 0.2, this.posY - 0.2, this.posZ, 0.0, 0.0, 0.0, new int[0]);
            }
            this.capture1 = true;
        }
    }

    protected void doCaptureCalc(EntityPixelmon pokemon) {
        PokemonLink link = pokemon.getPixelmonWrapper() != null ? new WrapperLink(pokemon.getPixelmonWrapper()) : new EntityLink(pokemon);
        int catchRate = PokeballTypeHelper.modifyCaptureRate(this.getType(), link.getSpecies(), link.getCatchRate());
        double ballBonus = PokeballTypeHelper.getBallBonus(this.getType(), this.thrower, link, this.getMode());
        CaptureEvent.StartCaptureEvent event = new CaptureEvent.StartCaptureEvent((EntityPlayerMP)this.getThrower(), pokemon, this, catchRate, ballBonus);
        if (!MinecraftForge.EVENT_BUS.post(event) && link.getBossMode() == EnumBossMode.NotBoss && link.getPlayerOwner() == null && link.getLevel() <= PixelmonServerConfig.maxLevel) {
            if (this.getType() != EnumPokeball.MasterBall && this.getType() != EnumPokeball.ParkBall) {
                int maxShakes = 4;
                int passedShakes = 0;
                catchRate = event.getCatchRate();
                ballBonus = event.getBallBonus();
                if (catchRate > 0) {
                    float hpMax = link.getMaxHealth();
                    float hpCurrent = link.getHealth();
                    PixelmonWrapper pw = pokemon.getPixelmonWrapper();
                    double bonusStatus = 1.0;
                    if (pw != null) {
                        hpCurrent = pw.getHealth();
                        hpMax = pw.getMaxHealth();
                        bonusStatus = this.hasStatusBonus(pw);
                    }
                    EntityPlayerMP player = (EntityPlayerMP)this.getThrower();
                    double a = (double)((3.0f * hpMax - 2.0f * hpCurrent) * (float)catchRate) * ballBonus / (double)(3.0f * hpMax) * bonusStatus;
                    int c = (int)(a * EntityPokeBall.getSpeciesCaughtMultiplier(player)) / 6;
                    if (PixelmonPlayerUtils.hasItem(player, itemStack -> itemStack.getItem() == PixelmonItems.catchingCharm)) {
                        c *= 3;
                    }
                    if (RandomHelper.getRandomNumberBetween(0, 255) < c) {
                        a = Math.cbrt(a);
                        maxShakes = 1;
                        this.setCriticalCapture(true);
                    }
                    double b = (double)Math.round(Math.pow(255.0 / a, 0.25) * 4096.0) / 4096.0;
                    if ((b = Math.floor(65536.0 / b)) != 0.0) {
                        if (a >= 255.0) {
                            this.canCatch = true;
                            this.numShakes = 4;
                            return;
                        }
                        for (int i = 0; i < maxShakes; ++i) {
                            int roll = RandomHelper.rand.nextInt(65536);
                            if ((double)roll > b) continue;
                            ++passedShakes;
                        }
                    }
                }
                this.canCatch = passedShakes == maxShakes;
                this.numShakes = passedShakes;
            } else {
                this.canCatch = true;
                this.numShakes = 4;
            }
        } else {
            this.canCatch = false;
            this.numShakes = 0;
        }
    }

    public EnumPokeball getType() {
        return EnumPokeball.getFromIndex(this.dataManager.get(dwPokeballType));
    }

    protected void setIsWaiting(boolean value) {
        this.dataManager.set(dwIsWaiting, value);
    }

    public boolean getIsWaiting() {
        return this.dataManager.get(dwIsWaiting);
    }

    protected void setIsOnGround(boolean value) {
        this.dataManager.set(dwIsOnGround, value);
    }

    protected boolean getIsOnGround() {
        return this.dataManager.get(dwIsOnGround);
    }

    public void setAnimation(String animation) {
        this.dataManager.set(dwAnimation, animation);
    }

    public String getAnimation() {
        return this.dataManager.get(dwAnimation);
    }

    public int getId() {
        return this.dataManager.get(dwId);
    }

    public void setId(int id) {
        this.dataManager.set(dwId, id);
    }

    public boolean isCriticalCapture() {
        return this.dataManager.get(dwCriticalCapture);
    }

    public void setCriticalCapture(boolean criticalCapture) {
        this.dataManager.set(dwCriticalCapture, criticalCapture);
    }

    public float getInitialYaw() {
        return this.dataManager.get(dwInitialYaw).floatValue();
    }

    public void setInitialYaw(float yaw) {
        this.dataManager.set(dwInitialYaw, Float.valueOf(yaw));
    }

    public float getInitialPitch() {
        return this.dataManager.get(dwInitialPitch).floatValue();
    }

    public void setInitialPitch(float pitch) {
        this.dataManager.set(dwInitialPitch, Float.valueOf(pitch));
    }

    public EnumPokeBallMode getMode() {
        return EnumPokeBallMode.getFromOrdinal(this.dataManager.get(dwMode));
    }

    public ModelPokeballs getModel() {
        if (this.model == null) {
            this.model = this.getType().getModel();
        }
        return this.model;
    }

    public UUID getOwnerId() {
        return (UUID)this.dataManager.get(dwOwner).orNull();
    }

    public void setOwnerId(UUID ownerUuid) {
        this.dataManager.set(dwOwner, com.google.common.base.Optional.of(ownerUuid));
    }

    public int[] getPokeId() {
        return new int[]{this.dataManager.get(dwPokeId1), this.dataManager.get(dwPokeId2)};
    }

    private double hasStatusBonus(PixelmonWrapper pixelmon) {
        if (pixelmon.hasStatus(StatusType.Sleep, StatusType.Freeze)) {
            return 2.5;
        }
        if (pixelmon.hasStatus(StatusType.Paralysis, StatusType.Poison, StatusType.PoisonBadly, StatusType.Burn)) {
            return 1.5;
        }
        return 1.0;
    }

    public void setPokeId(int[] pokeId) {
        this.dataManager.set(dwPokeId1, pokeId[0]);
        this.dataManager.set(dwPokeId2, pokeId[1]);
    }

    public static double getSpeciesCaughtMultiplier(EntityPlayerMP player) {
        Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
        if (storageOpt.isPresent()) {
            int dexSize = storageOpt.get().pokedex.countCaught();
            if (dexSize > 600) {
                return 2.5;
            }
            if (dexSize >= 451) {
                return 2.0;
            }
            if (dexSize >= 301) {
                return 1.5;
            }
            if (dexSize >= 151) {
                return 1.0;
            }
            if (dexSize >= 31) {
                return 0.5;
            }
        }
        return 0.0;
    }
}

