/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pokeballs.captures;

import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pokeballs.EnumPokeBallMode;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureBase;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import net.minecraft.entity.player.EntityPlayer;

public class CaptureLureBall
extends CaptureBase {
    public CaptureLureBall() {
        super(EnumPokeball.LureBall);
    }

    @Override
    public double getBallBonus(EnumPokeball type, EntityPlayer thrower, PokemonLink px, EnumPokeBallMode mode) {
        return px.getBattleController() != null && px.getBattleController().wasFishing ? 3.0 : 1.0;
    }
}

