/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pokeballs.captures;

import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pokeballs.EnumPokeBallMode;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureBase;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import net.minecraft.entity.player.EntityPlayer;

public class CaptureLevelBall
extends CaptureBase {
    public CaptureLevelBall() {
        super(EnumPokeball.LevelBall);
    }

    @Override
    public double getBallBonus(EnumPokeball type, EntityPlayer thrower, PokemonLink px, EnumPokeBallMode mode) {
        double ballBonus = this.pokeball.getBallBonus();
        if (mode == EnumPokeBallMode.BATTLE) {
            BattleControllerBase bc = BattleRegistry.getBattle(thrower);
            if (bc == null) {
                return ballBonus;
            }
            int ownerPokemonLevel = 0;
            for (BattleParticipant p : bc.participants) {
                if (p.getEntity() != thrower) continue;
                for (PixelmonWrapper pw : p.controlledPokemon) {
                    ownerPokemonLevel = Math.max(ownerPokemonLevel, pw.getLevelNum());
                }
            }
            int targetLevel = px.getLevel();
            if (ownerPokemonLevel > 4 * targetLevel) {
                ballBonus = 8.0;
            } else if (ownerPokemonLevel > 2 * targetLevel) {
                ballBonus = 4.0;
            } else if (ownerPokemonLevel > targetLevel) {
                ballBonus = 2.0;
            }
        }
        return ballBonus;
    }
}

