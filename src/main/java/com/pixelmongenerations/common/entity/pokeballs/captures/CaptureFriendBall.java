/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pokeballs.captures;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pokeballs.EnumPokeBallMode;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureBase;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.network.EnumUpdateType;
import net.minecraft.entity.player.EntityPlayer;

public class CaptureFriendBall
extends CaptureBase {
    public CaptureFriendBall() {
        super(EnumPokeball.FriendBall);
    }

    @Override
    public double getBallBonus(EnumPokeball type, EntityPlayer thrower, PokemonLink px, EnumPokeBallMode mode) {
        return type.getBallBonus();
    }

    @Override
    public void doAfterEffect(EnumPokeball type, EntityPixelmon p) {
        p.friendship.setFriendship(200);
        p.update(EnumUpdateType.Friendship);
    }
}

