/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pokeballs.captures;

import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pokeballs.EnumPokeBallMode;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureBase;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;

public class CaptureDuskBall
extends CaptureBase {
    public CaptureDuskBall() {
        super(EnumPokeball.DuskBall);
    }

    @Override
    public double getBallBonus(EnumPokeball type, EntityPlayer thrower, PokemonLink px, EnumPokeBallMode mode) {
        if (thrower.posY <= 62.0 && !thrower.world.isRainingAt(new BlockPos(MathHelper.floor(thrower.posX), MathHelper.floor(thrower.posY), MathHelper.floor(thrower.posZ))) && thrower.world.getLightBrightness(new BlockPos(MathHelper.floor(thrower.posX), MathHelper.floor(thrower.posY), MathHelper.floor(thrower.posZ))) <= 14.0f || !thrower.world.isDaytime()) {
            return 3.5;
        }
        return 1.0;
    }
}

