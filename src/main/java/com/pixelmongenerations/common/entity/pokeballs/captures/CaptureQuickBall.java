/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pokeballs.captures;

import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pokeballs.EnumPokeBallMode;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureBase;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import net.minecraft.entity.player.EntityPlayer;

public class CaptureQuickBall
extends CaptureBase {
    public CaptureQuickBall() {
        super(EnumPokeball.QuickBall);
    }

    @Override
    public double getBallBonus(EnumPokeball type, EntityPlayer thrower, PokemonLink px, EnumPokeBallMode mode) {
        if (mode == EnumPokeBallMode.BATTLE && px.getBattleController() != null && px.getBattleController().battleTurn == 0) {
            return 5.0;
        }
        return type.getBallBonus();
    }
}

