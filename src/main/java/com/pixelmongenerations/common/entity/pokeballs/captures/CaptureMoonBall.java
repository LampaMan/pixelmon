/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pokeballs.captures;

import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pokeballs.EnumPokeBallMode;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureBase;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import net.minecraft.entity.player.EntityPlayer;

public class CaptureMoonBall
extends CaptureBase {
    public CaptureMoonBall() {
        super(EnumPokeball.MoonBall);
    }

    @Override
    public double getBallBonus(EnumPokeball type, EntityPlayer thrower, PokemonLink px, EnumPokeBallMode mode) {
        if (px.getSpecies().isPokemon(EnumSpecies.Nidoranfemale, EnumSpecies.Nidorina, EnumSpecies.Nidoqueen, EnumSpecies.Nidoranmale, EnumSpecies.Nidorino, EnumSpecies.Nidoking, EnumSpecies.Cleffa, EnumSpecies.Clefairy, EnumSpecies.Clefable, EnumSpecies.Igglybuff, EnumSpecies.Jigglypuff, EnumSpecies.Wigglytuff, EnumSpecies.Skitty, EnumSpecies.Delcatty)) {
            return 4.0;
        }
        return 1.0;
    }
}

