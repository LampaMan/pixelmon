/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pokeballs.captures;

import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pokeballs.EnumPokeBallMode;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureBase;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import net.minecraft.entity.player.EntityPlayer;

public class CaptureLoveBall
extends CaptureBase {
    public CaptureLoveBall() {
        super(EnumPokeball.LoveBall);
    }

    @Override
    public double getBallBonus(EnumPokeball type, EntityPlayer thrower, PokemonLink px, EnumPokeBallMode mode) {
        double ballBonus = type.getBallBonus();
        if (mode == EnumPokeBallMode.BATTLE) {
            BattleControllerBase bc = BattleRegistry.getBattle(thrower);
            if (bc == null) {
                return ballBonus;
            }
            for (BattleParticipant p : bc.participants) {
                if (p.getEntity() != thrower) continue;
                for (PixelmonWrapper pw : p.controlledPokemon) {
                    if (!px.getSpecies().name.equals(pw.getPokemonName()) || px.getGender() != pw.getGender()) continue;
                    return 8.0;
                }
            }
        }
        return ballBonus;
    }
}

