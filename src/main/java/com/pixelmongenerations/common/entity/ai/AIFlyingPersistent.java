/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.ai;

import com.pixelmongenerations.common.entity.pixelmon.Entity7HasAI;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;

public class AIFlyingPersistent
extends EntityAIBase {
    Entity7HasAI pixelmon;
    int ticksToChangeDirection = 0;
    int ticksToChangeSpeed = 0;
    float movespeed = 1.0f;
    int directionChange = 1000;
    int speedChange = 500;
    boolean lastChangeDirection;

    public AIFlyingPersistent(Entity7HasAI entity) {
        if (entity.getFlyingParameters() != null) {
            this.movespeed = 1.0f * entity.getFlyingParameters().flySpeedModifier;
            this.directionChange = entity.getFlyingParameters().flyRefreshRateXZ;
            this.speedChange = entity.getFlyingParameters().flyRefreshRateSpeed;
        }
        this.pixelmon = entity;
    }

    @Override
    public boolean shouldExecute() {
        return true;
    }

    @Override
    public boolean shouldContinueExecuting() {
        this.pixelmon.renderYawOffset += (-((float)Math.atan2(this.pixelmon.motionX, this.pixelmon.motionZ)) * 180.0f / (float)Math.PI - this.pixelmon.renderYawOffset) * 0.1f;
        return true;
    }

    @Override
    public void updateTask() {
        if (this.pixelmon.battleController != null) {
            super.updateTask();
            return;
        }
        --this.ticksToChangeDirection;
        --this.ticksToChangeSpeed;
        boolean useLastChangeDirection = false;
        RayTraceResult mop = this.pixelmon.world.rayTraceBlocks(new Vec3d(this.pixelmon.posX, this.pixelmon.getEntityBoundingBox().minY, this.pixelmon.posZ), new Vec3d(this.pixelmon.posX + this.pixelmon.motionX * 100.0, this.pixelmon.getEntityBoundingBox().minY, this.pixelmon.posZ + this.pixelmon.motionZ * 100.0));
        if (mop == null) {
            mop = this.pixelmon.world.rayTraceBlocks(new Vec3d(this.pixelmon.posX, this.pixelmon.getEntityBoundingBox().maxY, this.pixelmon.posZ), new Vec3d(this.pixelmon.posX + this.pixelmon.motionX * 100.0, this.pixelmon.getEntityBoundingBox().maxY, this.pixelmon.posZ + this.pixelmon.motionZ * 100.0));
        }
        if (mop != null) {
            useLastChangeDirection = true;
            this.ticksToChangeDirection = 0;
        }
        if (this.ticksToChangeDirection <= 0) {
            this.pickDirection(useLastChangeDirection);
            this.ticksToChangeDirection = 25 + this.pixelmon.getRNG().nextInt(this.directionChange);
        }
        if (this.ticksToChangeSpeed <= 0) {
            this.pickSpeed();
            this.ticksToChangeSpeed = 50 + this.pixelmon.getRNG().nextInt(this.speedChange);
        }
        this.pixelmon.travel(0.0f, 0.0f, this.movespeed);
        super.updateTask();
    }

    public void pickDirection(boolean useLastChangeDirection) {
        double rotAmt;
        if (useLastChangeDirection) {
            rotAmt = (double)this.pixelmon.getRNG().nextInt(5) + 5.0;
            if (this.lastChangeDirection) {
                rotAmt *= -1.0;
            }
            this.pixelmon.getAttackTarget();
        } else {
            rotAmt = (double)this.pixelmon.getRNG().nextInt(40) - 20.0;
            this.lastChangeDirection = rotAmt > 0.0;
        }
        this.pixelmon.rotationYaw = (float)((double)this.pixelmon.rotationYaw + rotAmt);
    }

    public void pickSpeed() {
        this.movespeed = this.pixelmon.getRNG().nextFloat() * 0.7f + 0.5f;
    }
}

