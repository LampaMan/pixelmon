/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.ai;

import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import net.minecraft.block.Block;
import net.minecraft.block.BlockCrops;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.ai.EntityAIMoveToBlock;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class AIHarvestFarmLand
extends EntityAIMoveToBlock {
    private final EntityNPC npc;
    private boolean hasFarmItem;
    private boolean wantsToReapStuff;
    private int currentTask;

    public AIHarvestFarmLand(EntityNPC theVillagerIn, double speedIn) {
        super(theVillagerIn, speedIn, 16);
        this.npc = theVillagerIn;
    }

    @Override
    public boolean shouldExecute() {
        if (this.runDelay <= 0) {
            if (!this.npc.world.getGameRules().getBoolean("mobGriefing")) {
                return false;
            }
            this.currentTask = -1;
            this.hasFarmItem = this.npc.isFarmItemInInventory();
            this.wantsToReapStuff = this.npc.hasItemToPlant();
        }
        return super.shouldExecute();
    }

    @Override
    public boolean shouldContinueExecuting() {
        return this.currentTask >= 0 && super.shouldContinueExecuting();
    }

    @Override
    public void updateTask() {
        super.updateTask();
        this.npc.getLookHelper().setLookPosition((double)this.destinationBlock.getX() + 0.5, (double)this.destinationBlock.getY() + 1.0, (double)this.destinationBlock.getZ() + 0.5, 10.0f, this.npc.getVerticalFaceSpeed());
        if (this.getIsAboveDestination()) {
            World world = this.npc.world;
            BlockPos blockpos = this.destinationBlock.up();
            IBlockState iblockstate = world.getBlockState(blockpos);
            Block block = iblockstate.getBlock();
            if (this.currentTask == 0 && block instanceof BlockCrops && ((BlockCrops)block).isMaxAge(iblockstate)) {
                world.destroyBlock(blockpos, true);
            } else if (this.currentTask == 1 && iblockstate.getMaterial() == Material.AIR) {
                InventoryBasic inventorybasic = this.npc.getNPCInventory();
                for (int i = 0; i < inventorybasic.getSizeInventory(); ++i) {
                    ItemStack itemstack = inventorybasic.getStackInSlot(i);
                    boolean flag = false;
                    if (!itemstack.isEmpty()) {
                        if (itemstack.getItem() == Items.WHEAT_SEEDS) {
                            world.setBlockState(blockpos, Blocks.WHEAT.getDefaultState(), 3);
                            flag = true;
                        } else if (itemstack.getItem() == Items.POTATO) {
                            world.setBlockState(blockpos, Blocks.POTATOES.getDefaultState(), 3);
                            flag = true;
                        } else if (itemstack.getItem() == Items.CARROT) {
                            world.setBlockState(blockpos, Blocks.CARROTS.getDefaultState(), 3);
                            flag = true;
                        }
                    }
                    if (!flag) continue;
                    itemstack.setCount(itemstack.getCount() - 1);
                    if (itemstack.getCount() > 0) break;
                    inventorybasic.setInventorySlotContents(i, ItemStack.EMPTY);
                    break;
                }
            }
            this.currentTask = -1;
            this.runDelay = 10;
        }
    }

    @Override
    protected boolean shouldMoveTo(World worldIn, BlockPos pos) {
        Block block = worldIn.getBlockState(pos).getBlock();
        if (block == Blocks.FARMLAND) {
            IBlockState iblockstate = worldIn.getBlockState(pos.up());
            block = iblockstate.getBlock();
            if (block instanceof BlockCrops && ((BlockCrops)block).isMaxAge(iblockstate) && this.wantsToReapStuff && (this.currentTask == 0 || this.currentTask < 0)) {
                this.currentTask = 0;
                return true;
            }
            if (iblockstate.getMaterial() == Material.AIR && this.hasFarmItem && (this.currentTask == 1 || this.currentTask < 0)) {
                this.currentTask = 1;
                return true;
            }
        }
        return false;
    }
}

