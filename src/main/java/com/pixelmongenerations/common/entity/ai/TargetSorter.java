/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.ai;

import com.pixelmongenerations.common.entity.ai.AITargetNearest;
import java.util.Comparator;
import net.minecraft.entity.Entity;

public class TargetSorter
implements Comparator<Entity> {
    private Entity theEntity;
    final AITargetNearest targetNearest;

    public TargetSorter(AITargetNearest targetNearest, Entity entity) {
        this.targetNearest = targetNearest;
        this.theEntity = entity;
    }

    @Override
    public int compare(Entity firstEntity, Entity secondEntity) {
        double distanceFirst = this.theEntity.getDistanceSq(firstEntity);
        double distanceSecond = this.theEntity.getDistanceSq(secondEntity);
        return distanceFirst < distanceSecond ? -1 : (distanceFirst > distanceSecond ? 1 : 0);
    }
}

