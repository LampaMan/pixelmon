/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.ai;

import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.storage.playerData.ExternalMoveData;
import java.util.Optional;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;

public class AIMoveTowardsBlock
extends EntityAIBase {
    private EntityPixelmon pixelmon;
    private double movePosX;
    private double movePosY;
    private double movePosZ;

    public AIMoveTowardsBlock(EntityPixelmon pixelmon) {
        this.pixelmon = pixelmon;
        this.setMutexBits(3);
    }

    @Override
    public boolean shouldExecute() {
        if (this.pixelmon.moveIndex == -1) {
            return false;
        }
        if (this.pixelmon.battleController != null) {
            return false;
        }
        if (this.pixelmon.getOwner() != null && BattleRegistry.getBattle((EntityPlayerMP)this.pixelmon.getOwner()) != null) {
            this.pixelmon.setAttackTarget(null);
            return false;
        }
        this.movePosX = this.pixelmon.targetX;
        this.movePosY = this.pixelmon.targetY;
        this.movePosZ = this.pixelmon.targetZ;
        Optional<PlayerStorage> optstorage = this.pixelmon.getStorage();
        if (optstorage.isPresent()) {
            ExternalMoveData move;
            PlayerStorage storage = optstorage.get();
            if (PixelmonConfig.allowExternalMoves && optstorage.get().moves != null) {
                move = storage.moves.get(this.pixelmon.getPokemonId(), this.pixelmon.moveIndex);
                if (move == null) {
                    return false;
                }
            } else {
                return false;
            }
            if (this.pixelmon.getDistance(this.pixelmon.targetX, this.pixelmon.targetY, this.pixelmon.targetZ) < move.getTargetDistance()) {
                RayTraceResult objPos = new RayTraceResult(new Vec3d(this.movePosX, this.movePosY, this.movePosZ), this.pixelmon.targetSide, new BlockPos(this.movePosX, this.movePosY, this.movePosZ));
                move.execute((EntityPlayerMP)this.pixelmon.getOwner(), this.pixelmon, objPos);
                storage.moves.set(this.pixelmon.getPokemonId(), this.pixelmon.moveIndex, move);
                this.pixelmon.moveIndex = -1;
                this.pixelmon.setAttackTarget(null);
                this.movePosX = this.pixelmon.posX;
                this.movePosY = this.pixelmon.posY;
                this.movePosZ = this.pixelmon.posZ;
                this.pixelmon.getNavigator().tryMoveToXYZ(this.movePosX, this.movePosY, this.movePosZ, (float)this.pixelmon.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getAttributeValue());
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean shouldContinueExecuting() {
        return false;
    }

    @Override
    public void startExecuting() {
        this.pixelmon.getNavigator().tryMoveToXYZ(this.movePosX, this.movePosY, this.movePosZ, (float)this.pixelmon.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getAttributeValue());
    }
}

