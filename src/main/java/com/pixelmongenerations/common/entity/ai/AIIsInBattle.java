/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.ai;

import com.pixelmongenerations.common.entity.pixelmon.Entity7HasAI;
import net.minecraft.entity.ai.EntityAIBase;

public class AIIsInBattle
extends EntityAIBase {
    Entity7HasAI pixelmon;

    public AIIsInBattle(Entity7HasAI entity7HasAI) {
        this.setMutexBits(1);
        this.pixelmon = entity7HasAI;
    }

    @Override
    public boolean shouldExecute() {
        return this.pixelmon.battleController != null;
    }

    @Override
    public boolean shouldContinueExecuting() {
        return this.pixelmon.battleController != null;
    }
}

