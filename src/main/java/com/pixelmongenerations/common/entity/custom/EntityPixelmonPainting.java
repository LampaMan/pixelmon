/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.custom;

import com.pixelmongenerations.core.config.PixelmonItems;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityHanging;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class EntityPixelmonPainting
extends EntityHanging {
    public static final DataParameter<ItemStack> ITEMSTACK = EntityDataManager.createKey(EntityPixelmonPainting.class, DataSerializers.ITEM_STACK);

    public EntityPixelmonPainting(World worldIn) {
        super(worldIn);
    }

    public EntityPixelmonPainting(World worldIn, BlockPos pos, EnumFacing facing) {
        super(worldIn, pos);
        this.updateFacingWithBoundingBox(facing);
    }

    @Override
    protected void entityInit() {
        super.entityInit();
        this.getDataManager().register(ITEMSTACK, ItemStack.EMPTY);
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound tagCompound) {
        super.writeEntityToNBT(tagCompound);
        if (this.getDisplayedItem() != null) {
            tagCompound.setTag("Item", this.getDisplayedItem().writeToNBT(new NBTTagCompound()));
        }
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound tagCompund) {
        super.readEntityFromNBT(tagCompund);
        NBTTagCompound nbttagcompound1 = tagCompund.getCompoundTag("Item");
        if (!nbttagcompound1.isEmpty()) {
            this.setDisplayedItemWithUpdate(new ItemStack(nbttagcompound1), false);
        }
    }

    @Override
    public boolean processInitialInteract(EntityPlayer player, EnumHand hand) {
        if (this.world.isRemote) {
            return true;
        }
        ItemStack stack = player.getHeldItem(hand);
        if (this.getDisplayedItem() == null && !stack.isEmpty() && stack.getItem() == PixelmonItems.itemPixelmonSprite) {
            this.setDisplayedItem(stack);
            stack.shrink(1);
            if (!player.capabilities.isCreativeMode && stack.getCount() <= 0) {
                player.setHeldItem(hand, ItemStack.EMPTY);
            }
        }
        return true;
    }

    @Override
    public void onBroken(Entity brokenEntity) {
        this.dropItemOrSelf(brokenEntity, true);
    }

    public void dropItemOrSelf(Entity p_146065_1_, boolean p_146065_2_) {
        if (this.world.getGameRules().getBoolean("doTileDrops")) {
            ItemStack itemstack = this.getDisplayedItem();
            if (p_146065_1_ instanceof EntityPlayer) {
                EntityPlayer entityplayer = (EntityPlayer)p_146065_1_;
                if (entityplayer.capabilities.isCreativeMode) {
                    return;
                }
            }
            if (p_146065_2_) {
                this.entityDropItem(new ItemStack(PixelmonItems.pixelmonPaintingItem), 0.0f);
            }
            if (itemstack != null) {
                itemstack = itemstack.copy();
                this.entityDropItem(itemstack, 0.0f);
            }
        }
    }

    @Override
    public void updateBoundingBox() {
        if (this.world.isRemote) {
            this.facingDirection = EnumFacing.fromAngle(this.rotationYaw);
        }
        if (this.facingDirection != null) {
            double x = this.posX = (double)this.hangingPosition.getX() + 0.5;
            double y = this.posY = (double)this.hangingPosition.getY() + 0.5;
            double z = this.posZ = (double)this.hangingPosition.getZ() + 0.5;
            if (this.facingDirection == EnumFacing.NORTH) {
                this.setEntityBoundingBox(new AxisAlignedBB(x - 1.5, y - 1.5, z + 0.3, x + 0.5, y + 0.5, z + 0.5));
            } else if (this.facingDirection == EnumFacing.SOUTH) {
                this.setEntityBoundingBox(new AxisAlignedBB(x - 0.5, y - 1.5, z - 0.3, x + 1.5, y + 0.5, z - 0.5));
            } else if (this.facingDirection == EnumFacing.EAST) {
                this.setEntityBoundingBox(new AxisAlignedBB(x - 0.3, y - 1.5, z - 1.5, x - 0.5, y + 0.5, z + 0.5));
            } else {
                this.setEntityBoundingBox(new AxisAlignedBB(x + 0.3, y - 1.5, z - 0.5, x + 0.5, y + 0.5, z + 1.5));
            }
        }
    }

    @Override
    public int getWidthPixels() {
        return 32;
    }

    @Override
    public int getHeightPixels() {
        return 32;
    }

    @Override
    public boolean onValidSurface() {
        return true;
    }

    private void setDisplayedItemWithUpdate(ItemStack stack, boolean p_174864_2_) {
        if (stack != null) {
            stack = stack.copy();
            stack.setCount(1);
        }
        this.getDataManager().set(ITEMSTACK, stack);
        if (p_174864_2_ && this.hangingPosition != null) {
            this.world.updateComparatorOutputLevel(this.hangingPosition, Blocks.AIR);
        }
    }

    public void setDisplayedItem(ItemStack stack) {
        this.setDisplayedItemWithUpdate(stack, true);
    }

    public ItemStack getDisplayedItem() {
        return this.dataManager.get(ITEMSTACK) != ItemStack.EMPTY ? this.getDataManager().get(ITEMSTACK) : null;
    }

    @Override
    public void playPlaceSound() {
    }
}

