/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.MoverType;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;

public class EntityChairMount
extends Entity {
    public EntityChairMount(World world) {
        super(world);
        this.setSize(0.1f, 0.1f);
    }

    @Override
    public double getMountedYOffset() {
        return 0.5;
    }

    @Override
    protected void entityInit() {
    }

    @Override
    public void onEntityUpdate() {
        if (this.posY < -1.0 || this.world != null && !this.world.isRemote && this.getPassengers().size() != 1) {
            this.isDead = true;
        }
    }

    @Override
    public boolean isEntityInvulnerable(DamageSource source) {
        return source != DamageSource.OUT_OF_WORLD;
    }

    @Override
    public boolean isInvisible() {
        return true;
    }

    @Override
    public void move(MoverType type, double p_70091_1_, double p_70091_3_, double p_70091_5_) {
    }

    @Override
    protected void readEntityFromNBT(NBTTagCompound p_70037_1_) {
    }

    @Override
    protected void writeEntityToNBT(NBTTagCompound p_70014_1_) {
    }

    @Override
    public boolean canBeCollidedWith() {
        return false;
    }

    @Override
    public boolean canBePushed() {
        return false;
    }

    @Override
    public void fall(float p_70069_1_, float mod) {
    }

    @Override
    protected void removePassenger(Entity passenger) {
        super.removePassenger(passenger);
        if (!passenger.isDead && passenger.getPosition().distanceSq(this.posX, this.posY, this.posZ) < 5.0) {
            passenger.move(MoverType.SELF, 0.0, 1.0, 0.0);
        }
    }
}

