/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.npcs.registry;

public class TrainerChat {
    public String opening;
    public String win;
    public String lose;

    public TrainerChat(String opening, String win, String lose) {
        this.opening = opening;
        this.win = win;
        this.lose = lose;
    }
}

