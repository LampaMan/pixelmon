/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.entity.npcs;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.dialogue.Choice;
import com.pixelmongenerations.api.dialogue.Dialogue;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumFurfrou;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Arrays;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;

public class NPCGroomer
extends EntityNPC {
    public NPCGroomer(World world) {
        super(world);
        this.npcLocation = SpawnLocation.LandVillager;
    }

    @Override
    public String getDisplayText() {
        return "";
    }

    @Override
    public String getTexture() {
        return "pixelmon:textures/steve/groomer.png";
    }

    @Override
    public boolean interactWithNPC(EntityPlayer player, EnumHand hand) {
        Optional<PlayerStorage> oStorage;
        if (player instanceof EntityPlayerMP && (oStorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player)).isPresent()) {
            PlayerStorage storage = oStorage.get();
            boolean cutHair = false;
            for (int i = 0; i < 6; ++i) {
                NBTTagCompound partyNBT;
                if (storage.getList()[i] == null || !(partyNBT = storage.getList()[i]).getString("Name").equals(EnumSpecies.Furfrou.name)) continue;
                cutHair = true;
                this.buildDialogue(storage, i, partyNBT.getInteger("Variant"));
                break;
            }
            if (!cutHair) {
                player.sendMessage(new TextComponentTranslation("pixelmon.groomer.nocut", new Object[0]));
            }
        }
        return true;
    }

    private void buildDialogue(PlayerStorage storage, int slot, int form) {
        Dialogue.DialogueBuilder dialogue = Dialogue.builder();
        dialogue.column(3);
        dialogue.setName(I18n.translateToLocal("pixelmon.gui.furfrou_hairstyles"));
        dialogue.setText(I18n.translateToLocal("pixelmon.gui.furfrou_grooming"));
        Arrays.stream(EnumFurfrou.values()).filter(a -> a.getForm() != form).map(f -> new Choice(f.getProperName(), event -> event.getPlayer().getServer().addScheduledTask(() -> {
            EntityPixelmon pixelmon = storage.sendOutFromPosition(slot, storage.getPlayer().getEntityWorld());
            NBTTagCompound partyNBT = storage.getList()[slot];
            byte newForm = f.getForm();
            storage.recallAllPokemon();
            pixelmon.setForm(newForm);
            storage.updateAndSendToClient(pixelmon, EnumUpdateType.Stats, EnumUpdateType.Nickname, EnumUpdateType.Ability, EnumUpdateType.HeldItem, EnumUpdateType.HP, EnumUpdateType.Moveset, EnumUpdateType.Friendship);
            storage.updateNBT(pixelmon);
            storage.sendUpdatedList();
            pixelmon.unloadEntity();
            ChatHandler.sendChat(storage.getPlayer(), "pixelmon.abilities.changeform", partyNBT.getString("Name"));
            storage.getPlayer().playSound(SoundEvents.ENTITY_SHEEP_SHEAR, 1.0f, 1.0f);
        }))).forEach(dialogue::addChoice);
        Dialogue.setPlayerDialogueData(storage.getPlayer(), Lists.newArrayList(dialogue.build()), true);
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        super.readEntityFromNBT(nbt);
        this.initDefaultAI();
    }
}

