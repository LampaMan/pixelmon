/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.common.entity.npcs.registry;

import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumBurmy;
import com.pixelmongenerations.core.enums.forms.EnumCastform;
import com.pixelmongenerations.core.enums.forms.EnumDeoxys;
import com.pixelmongenerations.core.enums.forms.EnumGastrodon;
import com.pixelmongenerations.core.enums.forms.EnumShellos;
import com.pixelmongenerations.core.enums.forms.EnumUnown;
import com.pixelmongenerations.core.enums.forms.EnumWormadam;
import com.pixelmongenerations.core.util.IEncodeable;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.client.resources.I18n;
import net.minecraft.nbt.NBTTagCompound;

public class PokemonForm
implements IEncodeable {
    public EnumSpecies pokemon;
    public int form;
    public Gender gender;

    public PokemonForm(EnumSpecies pokemon) {
        this.pokemon = pokemon;
        this.form = -1;
        this.gender = Gender.Male;
    }

    public PokemonForm(EnumSpecies pokemon, int form) {
        this(pokemon);
        this.form = form;
        this.gender = Gender.Male;
    }

    public PokemonForm(EnumSpecies pokemon, int form, Gender gender) {
        this(pokemon, form);
        this.gender = gender;
    }

    public PokemonForm(NBTTagCompound nbt) {
        this(EnumSpecies.getFromName(nbt.getString("Name")).get(), nbt.getInteger("Variant"), Gender.getGender(nbt.getShort("Gender")));
    }

    public PokemonForm(ByteBuf buffer) {
        this.decodeInto(buffer);
    }

    public PokemonForm copy() {
        return new PokemonForm(this.pokemon, this.form, this.gender);
    }

    public static PokemonForm[] convertEnumArray(EnumSpecies ... pokemon) {
        PokemonForm[] pokemonForms = new PokemonForm[pokemon.length];
        for (int i = 0; i < pokemon.length; ++i) {
            pokemonForms[i] = new PokemonForm(pokemon[i]);
        }
        return pokemonForms;
    }

    @Override
    public void encodeInto(ByteBuf buffer) {
        buffer.writeInt(this.pokemon.ordinal());
        buffer.writeInt(this.form);
        buffer.writeShort(this.gender.ordinal());
    }

    @Override
    public void decodeInto(ByteBuf buffer) {
        this.pokemon = EnumSpecies.getFromOrdinal(buffer.readInt());
        this.form = buffer.readInt();
        this.gender = Gender.getGender(buffer.readShort());
    }

    public int hashCode() {
        int prime = 31;
        int result = 1;
        result = 31 * result + this.form;
        result = 31 * result + (this.pokemon == null ? 0 : this.pokemon.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        PokemonForm other = (PokemonForm)obj;
        if (this.form != other.form) {
            return false;
        }
        if (this.pokemon != other.pokemon) {
            return false;
        }
        return this.gender == other.gender;
    }

    public static Optional<PokemonForm> getFromName(String name) {
        String[] hyphenSplit;
        switch (name) {
            case "Farfetch'd": {
                return Optional.of(new PokemonForm(EnumSpecies.Farfetchd));
            }
            case "Ho-Oh": {
                return Optional.of(new PokemonForm(EnumSpecies.Hooh));
            }
            case "Mime Jr.": {
                return Optional.of(new PokemonForm(EnumSpecies.MimeJr));
            }
            case "Mr. Mime": {
                return Optional.of(new PokemonForm(EnumSpecies.MrMime));
            }
            case "Nidoran\u2640": {
                return Optional.of(new PokemonForm(EnumSpecies.Nidoranfemale));
            }
            case "Nidoran\u2642": {
                return Optional.of(new PokemonForm(EnumSpecies.Nidoranmale));
            }
            case "Porygon-Z": {
                return Optional.of(new PokemonForm(EnumSpecies.PorygonZ));
            }
        }
        Optional<EnumSpecies> pokemon = Optional.ofNullable(EnumSpecies.getFromNameAnyCase(name));
        if (pokemon.isPresent()) {
            return Optional.of(new PokemonForm(pokemon.get()));
        }
        String megaPrefix = "Mega ";
        if (name.startsWith(megaPrefix)) {
            EnumSpecies enumPokemon;
            String nameEnd = name.substring(megaPrefix.length());
            if (nameEnd.equals("Charizard X")) {
                return Optional.of(new PokemonForm(EnumSpecies.Charizard, 1));
            }
            if (nameEnd.equals("Charizard Y")) {
                return Optional.of(new PokemonForm(EnumSpecies.Charizard, 2));
            }
            pokemon = EnumSpecies.getFromName(nameEnd);
            if (pokemon.isPresent() && (enumPokemon = pokemon.get()) != EnumSpecies.Charizard && enumPokemon.hasMega()) {
                return Optional.of(new PokemonForm(enumPokemon, 1));
            }
        } else if (name.contains("-") && (hyphenSplit = name.split("-")).length == 2) {
            pokemon = EnumSpecies.getFromName(hyphenSplit[0]);
            String formName = hyphenSplit[1];
            if (pokemon.isPresent()) {
                EnumSpecies enumPokemon = pokemon.get();
                int formIndex = EnumSpecies.getFormFromName(pokemon.get(), formName);
                try {
                    if (formIndex != -1) {
                        return Optional.of(new PokemonForm(enumPokemon, formIndex));
                    }
                }
                catch (NullPointerException nullPointerException) {
                    // empty catch block
                }
            }
        }
        return Optional.empty();
    }

    public String toString() {
        return this.pokemon.name + (this.form > 0 ? Integer.valueOf(this.form) : "");
    }

    public String getLocalizedName() {
        String baseName = Entity1Base.getLocalizedName(this.pokemon);
        if (this.form == -1) {
            return baseName;
        }
        if (this.form > 0 && this.pokemon.hasMega()) {
            if (this.pokemon == EnumSpecies.Charizard) {
                baseName = baseName + " " + (this.form == 2 ? "Y" : "X");
            }
            return I18n.format("pixelmon.mega.name", baseName);
        }
        String formName = "";
        switch (this.pokemon) {
            case Burmy: {
                formName = EnumBurmy.getFromIndex(this.form).name();
                break;
            }
            case Castform: {
                formName = EnumCastform.getFromIndex(this.form).name();
                break;
            }
            case Deoxys: {
                formName = EnumDeoxys.getFromIndex(this.form).name();
                break;
            }
            case Gastrodon: {
                formName = EnumGastrodon.getFromIndex(this.form).name();
                break;
            }
            case Shellos: {
                formName = EnumShellos.getFromIndex(this.form).name();
                break;
            }
            case Unown: {
                formName = EnumUnown.getFromIndex(this.form).name();
                break;
            }
            case Wormadam: {
                formName = EnumWormadam.getFromIndex(this.form).name();
            }
        }
        String formTranslate = I18n.format("pixelmon.form." + formName.toLowerCase() + ".name", new Object[0]);
        if (formTranslate.startsWith("pixelmon.")) {
            formTranslate = formName;
        }
        return String.format("%s-%s", baseName, formTranslate);
    }
}

