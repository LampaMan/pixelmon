/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.npcs.registry;

import com.pixelmongenerations.common.entity.npcs.registry.BaseTrainer;
import com.pixelmongenerations.common.entity.npcs.registry.ITrainerData;
import com.pixelmongenerations.common.entity.npcs.registry.PokemonForm;
import com.pixelmongenerations.common.entity.npcs.registry.TrainerChat;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;

public class TrainerData
implements ITrainerData {
    public BaseTrainer trainerType;
    int minLevel;
    int maxLevel;
    int minPartyPokemon;
    int maxPartyPokemon;
    public int winnings;
    ArrayList<PokemonForm> pokemonList = new ArrayList(6);
    ArrayList<String> names = new ArrayList();
    ArrayList<String> textures = new ArrayList();
    ArrayList<TrainerChat> chat = new ArrayList();
    public String id;

    public TrainerData(String id) {
        this.id = id;
    }

    void addName(String name) {
        this.names.add(name);
    }

    void addTexture(String texture) {
        this.textures.add(texture);
    }

    public void addPokemon(PokemonForm poke) {
        this.pokemonList.add(poke);
    }

    public void addChat(String opening, String win, String lose) {
        this.chat.add(new TrainerChat(opening, win, lose));
    }

    public ArrayList<PokemonForm> getRandomParty() {
        int numPokemon = RandomHelper.getRandomNumberBetween(this.minPartyPokemon, this.maxPartyPokemon);
        ArrayList<PokemonForm> list = new ArrayList<PokemonForm>();
        for (int i = 0; i < numPokemon; ++i) {
            list.add(RandomHelper.getRandomElementFromList(this.pokemonList));
        }
        return list;
    }

    public int getRandomName() {
        return RandomHelper.getRandomNumberBetween(0, this.names.size() - 1);
    }

    public int getRandomChat() {
        return RandomHelper.getRandomNumberBetween(0, this.chat.size() - 1);
    }

    public int getRandomLevel() {
        return RandomHelper.getRandomNumberBetween(this.minLevel, this.maxLevel);
    }

    @Override
    public String getName(int index) {
        if (index >= this.names.size() || index < 0) {
            return this.names.get(0);
        }
        return this.names.get(index);
    }

    @Override
    public TrainerChat getChat(int index) {
        if (index >= this.chat.size() || index < 0) {
            return this.chat.get(0);
        }
        return this.chat.get(index);
    }
}

