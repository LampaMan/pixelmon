/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.common.entity.npcs.registry;

import com.pixelmongenerations.common.entity.npcs.registry.BaseShopItem;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.entity.npcs.registry.ShopItem;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import io.netty.buffer.ByteBuf;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class ShopItemWithVariation {
    private float variation;
    private ShopItem shopItem;

    public ShopItemWithVariation(ShopItem shopItem, float variation) {
        this.shopItem = shopItem;
        this.variation = variation;
    }

    public ShopItemWithVariation(ShopItem item) {
        this(item, item.canPriceVary() ? ShopItemWithVariation.getVariation() : 1.0f);
    }

    private static float getVariation() {
        float rand = RandomHelper.getRandomNumberBetween(0.0f, 1.0f);
        float variation = 1.0f;
        if ((double)rand > 0.75) {
            variation = 1.1f;
        } else if ((double)rand < 0.25) {
            variation = 0.9f;
        }
        return variation;
    }

    public void writeToNBT(NBTTagList list) {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setString("ItemName", this.shopItem.getBaseItem().id);
        tag.setFloat("ItemVar", this.variation);
        list.appendTag(tag);
    }

    public static ShopItemWithVariation getFromNBT(String npcIndex, NBTTagCompound tag) {
        ShopItem item = ServerNPCRegistry.shopkeepers.getItem(npcIndex, tag.getString("ItemName"));
        if (item == null) {
            return null;
        }
        return new ShopItemWithVariation(item, tag.getFloat("ItemVar"));
    }

    public void writeToBuffer(ByteBuf buffer) {
        ByteBufUtils.writeUTF8String(buffer, this.shopItem.getBaseItem().id);
        ByteBufUtils.writeItemStack(buffer, this.shopItem.getBaseItem().itemStack);
        buffer.writeInt((int)((float)this.getBaseShopItem().buy * this.variation));
        buffer.writeInt((int)((float)this.getBaseShopItem().sell * this.variation));
    }

    public ItemStack getItem() {
        return this.shopItem.getItem();
    }

    public BaseShopItem getBaseShopItem() {
        return this.shopItem.getBaseItem();
    }

    public boolean canSell() {
        return this.shopItem.getBaseItem().sell != -1;
    }

    public int getBuyCost() {
        return (int)((float)this.getBaseShopItem().buy * this.variation);
    }

    public int getSellCost() {
        return (int)((float)this.getBaseShopItem().sell * this.variation);
    }
}

