/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.JsonArray
 *  com.google.gson.JsonObject
 *  com.google.gson.JsonParser
 */
package com.pixelmongenerations.common.entity.npcs.registry;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.pixelmongenerations.common.entity.npcs.registry.BaseTrainer;
import com.pixelmongenerations.common.entity.npcs.registry.GeneralNPCData;
import com.pixelmongenerations.common.entity.npcs.registry.GymNPCData;
import com.pixelmongenerations.common.entity.npcs.registry.ITrainerData;
import com.pixelmongenerations.common.entity.npcs.registry.LanguageNotFoundException;
import com.pixelmongenerations.common.entity.npcs.registry.NPCRarities;
import com.pixelmongenerations.common.entity.npcs.registry.NPCRegistryData;
import com.pixelmongenerations.common.entity.npcs.registry.NPCRegistryShopkeepers;
import com.pixelmongenerations.common.entity.npcs.registry.NPCRegistryTrainers;
import com.pixelmongenerations.common.entity.npcs.registry.NPCRegistryVillagers;
import com.pixelmongenerations.common.entity.npcs.registry.ShopkeeperData;
import com.pixelmongenerations.common.entity.npcs.registry.TrainerData;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumNPCType;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.minecraft.util.JsonUtils;

public class ServerNPCRegistry {
    public static String en_us = "en_us";
    public static HashMap<String, NPCRegistryData> data = new HashMap();
    public static NPCRegistryTrainers trainers = new NPCRegistryTrainers();
    public static NPCRegistryVillagers villagers = new NPCRegistryVillagers();
    public static NPCRegistryShopkeepers shopkeepers = new NPCRegistryShopkeepers();
    public static NPCRarities rarities = new NPCRarities();

    public static void registerNPCS(String langCode) throws Exception {
        String name;
        JsonObject jsonelement1;
        int i;
        JsonArray jsonarray;
        Pixelmon.LOGGER.info("Registering NPCs.");
        String path = Pixelmon.modDirectory + "/pixelmon/npcs/";
        File npcsDir = new File(path);
        String lowerLangCode = langCode.toLowerCase();
        NPCRegistryData thisData = null;
        thisData = data.containsKey(lowerLangCode) ? data.get(lowerLangCode) : new NPCRegistryData();
        InputStream istream = !PixelmonConfig.useExternalJSONFilesNPCs ? ServerNPCRegistry.class.getResourceAsStream("/assets/pixelmon/npcs/npcs.json") : new FileInputStream(new File(npcsDir, "npcs.json"));
        JsonObject json = new JsonParser().parse((Reader)new InputStreamReader(istream, StandardCharsets.UTF_8)).getAsJsonObject();
        if (json.has("villagers")) {
            jsonarray = JsonUtils.getJsonArray(json, "villagers");
            for (i = 0; i < jsonarray.size(); ++i) {
                try {
                    jsonelement1 = jsonarray.get(i).getAsJsonObject();
                    name = jsonelement1.get("name").getAsString();
                    villagers.loadVillager(thisData, name, lowerLangCode);
                    continue;
                }
                catch (Exception e) {
                    ServerNPCRegistry.printLoadError(e);
                }
            }
        }
        if (json.has("shopKeepers")) {
            jsonarray = JsonUtils.getJsonArray(json, "shopKeepers");
            for (i = 0; i < jsonarray.size(); ++i) {
                try {
                    jsonelement1 = jsonarray.get(i).getAsJsonObject();
                    name = jsonelement1.get("name").getAsString();
                    shopkeepers.loadShopkeeper(thisData, name, lowerLangCode);
                    continue;
                }
                catch (Exception e) {
                    ServerNPCRegistry.printLoadError(e);
                }
            }
        }
        if (json.has("trainers")) {
            String name2;
            JsonObject object = JsonUtils.getJsonObject(json.get("trainers"), "trainers");
            JsonArray typesArray = JsonUtils.getJsonArray(object, "types");
            BaseTrainer._index = 0;
            for (int i2 = 0; i2 < typesArray.size(); ++i2) {
                JsonObject jsonelement12 = typesArray.get(i2).getAsJsonObject();
                name2 = jsonelement12.get("name").getAsString();
                BaseTrainer trainer = new BaseTrainer(name2);
                JsonArray texturesArray = JsonUtils.getJsonArray(jsonelement12, "textures");
                for (int j = 0; j < texturesArray.size(); ++j) {
                    String texture = texturesArray.get(j).getAsString();
                    trainer.addTexture(texture);
                }
                int rarity = 20;
                if (!jsonelement12.has("rarity")) {
                    if (!name2.equals("Steve")) {
                        Pixelmon.LOGGER.warn("Trainer " + name2 + " is missing rarity data!");
                    }
                } else {
                    rarity = jsonelement12.get("rarity").getAsInt();
                }
                trainer.rarity = rarity;
                thisData.trainerTypes.add(trainer);
                if (trainer.name.equals("Steve")) {
                    NPCRegistryTrainers.Steve = trainer;
                }
                if (!trainer.name.equals("Alex")) continue;
                NPCRegistryTrainers.Alex = trainer;
            }
            data.put(lowerLangCode, thisData);
            JsonArray listArray = JsonUtils.getJsonArray(object, "list");
            for (int i3 = 0; i3 < listArray.size(); ++i3) {
                name2 = "";
                try {
                    JsonObject jsonelement13 = listArray.get(i3).getAsJsonObject();
                    name2 = jsonelement13.get("name").getAsString();
                    trainers.loadTrainer(thisData, name2, lowerLangCode);
                    continue;
                }
                catch (LanguageNotFoundException e) {
                    Pixelmon.LOGGER.info("Missing NPC Trainer of type " + name2 + " for language " + lowerLangCode + ".");
                    trainers.loadTrainer(thisData, name2, en_us);
                    continue;
                }
                catch (Exception e) {
                    ServerNPCRegistry.printLoadError(e);
                }
            }
        }
        if (json.has("gymnpcs")) {
            jsonarray = JsonUtils.getJsonArray(json, "gymnpcs");
            for (i = 0; i < jsonarray.size(); ++i) {
                try {
                    jsonelement1 = jsonarray.get(i).getAsJsonObject();
                    String name3 = jsonelement1.get("name").getAsString();
                    ServerNPCRegistry.loadGymNPC(thisData, name3, lowerLangCode);
                    continue;
                }
                catch (Exception e) {
                    ServerNPCRegistry.printLoadError(e);
                }
            }
        }
        if (json.has("rarities")) {
            rarities.loadFromJson(json);
        }
        if (thisData.npcs.size() == 0 && thisData.trainers.size() == 0) {
            throw new LanguageNotFoundException();
        }
        data.put(lowerLangCode, thisData);
    }

    private static void printLoadError(Exception e) {
        Pixelmon.LOGGER.error(e.getMessage());
        e.printStackTrace();
    }

    private static void loadGymNPC(NPCRegistryData thisData, String name, String langCode) throws Exception {
        try {
            JsonObject jsonelement1;
            int i;
            JsonArray jsonarray;
            String path = Pixelmon.modDirectory + "/pixelmon/npcs/gyms/";
            File vDir = new File(path);
            InputStream istream = null;
            if (!PixelmonConfig.useExternalJSONFilesNPCs) {
                istream = ServerNPCRegistry.class.getResourceAsStream("/assets/pixelmon/npcs/gyms/" + name + "_" + langCode.toLowerCase() + ".json");
            } else {
                File file = new File(vDir, name + "_" + langCode.toLowerCase() + ".json");
                if (file.exists()) {
                    istream = new FileInputStream(file);
                }
            }
            if (istream == null) {
                if (langCode.equals("en_us")) {
                    throw new Exception("Error in Gym NPC:" + name + "_" + langCode.toLowerCase());
                }
                return;
            }
            GymNPCData data = new GymNPCData(name);
            JsonObject json = new JsonParser().parse((Reader)new InputStreamReader(istream, StandardCharsets.UTF_8)).getAsJsonObject();
            data.type = EnumNPCType.getFromString(json.get("npctype").getAsString());
            if (json.has("winnings")) {
                data.winnings = json.get("winnings").getAsInt();
            }
            if (json.has("skins")) {
                jsonarray = JsonUtils.getJsonArray(json, "skins");
                for (i = 0; i < jsonarray.size(); ++i) {
                    jsonelement1 = jsonarray.get(i).getAsJsonObject();
                    String skin = jsonelement1.get("filename").getAsString();
                    data.addTexture(skin);
                }
            }
            if (json.has("names")) {
                jsonarray = JsonUtils.getJsonArray(json, "names");
                for (i = 0; i < jsonarray.size(); ++i) {
                    jsonelement1 = jsonarray.get(i).getAsJsonObject();
                    String npcname = jsonelement1.get("name").getAsString();
                    data.addName(npcname);
                }
            }
            if (json.has("chat")) {
                if (data.type == EnumNPCType.ChattingNPC) {
                    jsonarray = JsonUtils.getJsonArray(json, "chat");
                    for (i = 0; i < jsonarray.size(); ++i) {
                        jsonelement1 = jsonarray.get(i).getAsJsonObject();
                        JsonArray lines = jsonelement1.get("lines").getAsJsonArray();
                        ArrayList<String> lineList = new ArrayList<String>();
                        for (int j = 0; j < lines.size(); ++j) {
                            JsonObject object = lines.get(j).getAsJsonObject();
                            String text = object.get("text").getAsString();
                            lineList.add(text);
                        }
                        String[] lineArray = new String[lineList.size()];
                        lineArray = lineList.toArray(lineArray);
                        data.addChat(lineArray);
                    }
                } else if (data.type == EnumNPCType.Trainer) {
                    jsonarray = JsonUtils.getJsonArray(json, "chat");
                    for (i = 0; i < jsonarray.size(); ++i) {
                        jsonelement1 = jsonarray.get(i).getAsJsonObject();
                        String opening = jsonelement1.get("opening").getAsString();
                        String win = jsonelement1.get("win").getAsString();
                        String lose = jsonelement1.get("lose").getAsString();
                        data.addChat(opening, win, lose);
                    }
                }
            }
            thisData.gymnpcs.put(data.id, data);
        }
        catch (Exception e) {
            Pixelmon.LOGGER.error("Error in Gym NPC: " + name + "_" + langCode.toLowerCase());
            ServerNPCRegistry.printLoadError(e);
        }
    }

    public static GymNPCData getGymMember(String id) {
        GymNPCData npc = null;
        npc = ServerNPCRegistry.data.get((Object)ServerNPCRegistry.en_us).gymnpcs.get(id);
        return npc;
    }

    public static String getRandomName() {
        List<TrainerData> list = null;
        while (list == null) {
            NPCRegistryData npcData = data.get(en_us);
            BaseTrainer trainer = RandomHelper.getRandomElementFromList(npcData.trainerTypes);
            list = npcData.trainers.get(trainer);
        }
        TrainerData td = (TrainerData)RandomHelper.getRandomElementFromList(list);
        return RandomHelper.getRandomElementFromList(td.names);
    }

    public static GymNPCData getTranslatedGymMemberData(String langCode, String id) {
        GymNPCData npc;
        String lowerLangCode = langCode.toLowerCase();
        if (!data.containsKey(lowerLangCode)) {
            try {
                ServerNPCRegistry.registerNPCS(lowerLangCode);
            }
            catch (LanguageNotFoundException e2) {
                data.put(lowerLangCode, data.get(en_us));
            }
            catch (Exception exception) {
                // empty catch block
            }
        }
        if ((npc = ServerNPCRegistry.data.get((Object)langCode.toLowerCase()).gymnpcs.get(id)) == null) {
            npc = ServerNPCRegistry.data.get((Object)ServerNPCRegistry.en_us).gymnpcs.get(id);
        }
        return npc;
    }

    public static String getTranslatedGymMemberName(String langCode, String id, int index) {
        ArrayList<String> names = ServerNPCRegistry.getTranslatedGymMemberData((String)langCode.toLowerCase(), (String)id).names;
        if (index >= names.size()) {
            index = 0;
        }
        return names.get(index);
    }

    public static String[] getTranslatedGymMemberChat(String langCode, String id, int index) {
        ArrayList<String[]> chat = ServerNPCRegistry.getTranslatedGymMemberData((String)langCode.toLowerCase(), (String)id).chat;
        if (index >= chat.size()) {
            index = 0;
        }
        return chat.get(index);
    }

    static void extractNpcsDir(File npcsDir) {
        JsonArray skArray;
        File villagersDir;
        int i;
        String filename;
        InputStream istream = ServerNPCRegistry.class.getResourceAsStream("/assets/pixelmon/npcs/index.json");
        JsonObject json = new JsonParser().parse((Reader)new InputStreamReader(istream, StandardCharsets.UTF_8)).getAsJsonObject();
        if (json.has("base")) {
            JsonArray baseArray = JsonUtils.getJsonArray(json, "base");
            for (int i2 = 0; i2 < baseArray.size(); ++i2) {
                String filename2 = baseArray.get(i2).getAsString() + ".json";
                ServerNPCRegistry.extractFile("/assets/pixelmon/npcs/" + filename2, npcsDir, filename2);
            }
        }
        if (json.has("shopKeepers")) {
            File skDir = new File(npcsDir, "shopKeepers");
            skDir.mkdir();
            JsonArray skArray2 = JsonUtils.getJsonArray(json, "shopKeepers");
            for (int i3 = 0; i3 < skArray2.size(); ++i3) {
                filename = skArray2.get(i3).getAsString() + ".json";
                ServerNPCRegistry.extractFile("/assets/pixelmon/npcs/shopKeepers/" + filename, skDir, filename);
            }
        }
        if (json.has("trainers")) {
            File trainersDir = new File(npcsDir, "trainers");
            trainersDir.mkdir();
            JsonArray skArray3 = JsonUtils.getJsonArray(json, "trainers");
            for (i = 0; i < skArray3.size(); ++i) {
                filename = skArray3.get(i).getAsString() + ".json";
                ServerNPCRegistry.extractFile("/assets/pixelmon/npcs/trainers/" + filename, trainersDir, filename);
            }
        }
        if (json.has("villagers")) {
            villagersDir = new File(npcsDir, "villagers");
            villagersDir.mkdir();
            skArray = JsonUtils.getJsonArray(json, "villagers");
            for (i = 0; i < skArray.size(); ++i) {
                filename = skArray.get(i).getAsString() + ".json";
                ServerNPCRegistry.extractFile("/assets/pixelmon/npcs/villagers/" + filename, villagersDir, filename);
            }
        }
        if (json.has("gyms")) {
            villagersDir = new File(npcsDir, "gyms");
            villagersDir.mkdir();
            skArray = JsonUtils.getJsonArray(json, "gyms");
            for (i = 0; i < skArray.size(); ++i) {
                filename = skArray.get(i).getAsString() + ".json";
                ServerNPCRegistry.extractFile("/assets/pixelmon/npcs/gyms/" + filename, villagersDir, filename);
            }
        }
    }

    public static void extractFile(String resourceName, File npcsDir, String filename) {
        try {
            File file = new File(npcsDir, filename);
            if (!file.exists()) {
                int nBytes;
                InputStream link = ServerNPCRegistry.class.getResourceAsStream(resourceName);
                BufferedInputStream in = new BufferedInputStream(link);
                BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file));
                byte[] buffer = new byte[2048];
                while ((nBytes = in.read(buffer)) > 0) {
                    out.write(buffer, 0, nBytes);
                }
                out.flush();
                out.close();
                in.close();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ITrainerData getTranslatedData(String langCode, BaseTrainer baseTrainer, String id) {
        ITrainerData data = null;
        data = trainers.getTranslatedData(langCode, baseTrainer, id);
        if (data == null) {
            data = ServerNPCRegistry.getTranslatedGymMemberData(langCode, id);
        }
        return data;
    }

    public static ArrayList<GeneralNPCData> getEnglishNPCs() {
        return ServerNPCRegistry.data.get((Object)ServerNPCRegistry.en_us).npcs;
    }

    public static ArrayList<ShopkeeperData> getEnglishShopkeepers() {
        return ServerNPCRegistry.data.get((Object)ServerNPCRegistry.en_us).shopkeepers;
    }
}

