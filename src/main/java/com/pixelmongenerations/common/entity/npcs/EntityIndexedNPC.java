/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.npcs;

import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import java.util.ArrayList;
import java.util.Arrays;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public abstract class EntityIndexedNPC
extends EntityNPC {
    protected int nameIndex;
    protected int chatIndex;
    protected String npcIndex = "NPC";

    public EntityIndexedNPC(World world) {
        super(world);
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound nbt) {
        super.writeEntityToNBT(nbt);
        nbt.setString("TrainerIndex", this.npcIndex);
        nbt.setInteger("ChatIndex", this.chatIndex);
        nbt.setInteger("NameIndex", this.nameIndex);
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        super.readEntityFromNBT(nbt);
        this.npcIndex = nbt.getString("TrainerIndex");
        this.chatIndex = nbt.getInteger("ChatIndex");
        this.nameIndex = nbt.getInteger("NameIndex");
        if (this.getId() == -1) {
            this.setId(idIndex++);
        }
    }

    @Override
    public String getTexture() {
        return "pixelmon:textures/steve/" + this.getCustomSteveTexture();
    }

    @Override
    public String getDisplayText() {
        return "";
    }

    @Override
    public void addVelocity(double par1, double par3, double par5) {
    }

    @Override
    public boolean interactWithNPC(EntityPlayer player, EnumHand hand) {
        return false;
    }

    public ArrayList<String> getChat(String langCode) {
        int index = this.chatIndex;
        return new ArrayList<String>(Arrays.asList(ServerNPCRegistry.villagers.getTranslatedChat(langCode, this.npcIndex, index)));
    }

    public String getName(String langCode) {
        int index = this.nameIndex;
        return ServerNPCRegistry.villagers.getTranslatedName(langCode, this.npcIndex, index);
    }
}

