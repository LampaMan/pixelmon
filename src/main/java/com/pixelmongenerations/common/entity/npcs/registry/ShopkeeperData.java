/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.npcs.registry;

import com.pixelmongenerations.common.entity.npcs.registry.ShopItem;
import com.pixelmongenerations.common.entity.npcs.registry.ShopItemWithVariation;
import com.pixelmongenerations.common.entity.npcs.registry.ShopkeeperChat;
import com.pixelmongenerations.core.enums.EnumShopKeeperType;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;

public class ShopkeeperData {
    public String id;
    public EnumShopKeeperType type;
    ArrayList<String> textures = new ArrayList();
    ArrayList<String> names = new ArrayList();
    ArrayList<ShopkeeperChat> chat = new ArrayList();
    ArrayList<ShopItem> items = new ArrayList();
    ArrayList<String> biomes = new ArrayList();

    public ShopkeeperData(String name) {
        this.id = name;
    }

    public void addTexture(String texture) {
        this.textures.add(texture);
    }

    void addName(String name) {
        this.names.add(name);
    }

    public void addChat(String hello, String goodbye) {
        this.chat.add(new ShopkeeperChat(hello, goodbye));
    }

    public void addItem(ShopItem item) {
        this.items.add(item);
    }

    public int getRandomChatIndex() {
        return RandomHelper.getRandomNumberBetween(0, this.chat.size() - 1);
    }

    public int getRandomNameIndex() {
        return RandomHelper.getRandomNumberBetween(0, this.names.size() - 1);
    }

    public String getRandomTexture() {
        return RandomHelper.getRandomElementFromList(this.textures);
    }

    public ArrayList<ShopItemWithVariation> getItemList() {
        ArrayList<ShopItemWithVariation> itemList = new ArrayList<ShopItemWithVariation>();
        for (ShopItem item : this.items) {
            if (item.getRarity() == 1.0f) {
                itemList.add(new ShopItemWithVariation(item));
                continue;
            }
            if (!RandomHelper.getRandomChance(item.getRarity())) continue;
            itemList.add(new ShopItemWithVariation(item));
        }
        return itemList;
    }

    public ShopItem getItem(String itemID) {
        for (ShopItem item : this.items) {
            if (!item.getBaseItem().id.equals(itemID)) continue;
            return item;
        }
        return null;
    }

    public int getNextNameIndex(int nameIndex) {
        if (nameIndex < this.names.size() - 1) {
            return nameIndex + 1;
        }
        return 0;
    }

    public String getNextTexture(String texture) {
        for (int i = 0; i < this.textures.size(); ++i) {
            if (!this.textures.get(i).equals(texture)) continue;
            if (i < this.textures.size() - 1) {
                return this.textures.get(i + 1);
            }
            return this.textures.get(0);
        }
        return this.textures.get(0);
    }

    public int countNames() {
        return this.names.size();
    }

    public ArrayList<String> getTextures() {
        return this.textures;
    }

    public void addBiome(String biome) {
        this.biomes.add(biome);
    }

    public ArrayList<String> getBiomes() {
        return this.biomes;
    }
}

