/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.entity.npcs;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.ItemNPCEditor;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class NPCUltra extends EntityNPC {

    public static List<EnumSpecies> QUEST_POKEMON = Lists.newArrayList(EnumSpecies.Nihilego, EnumSpecies.Buzzwole, EnumSpecies.Pheromosa, EnumSpecies.Xurkitree, EnumSpecies.Celesteela, EnumSpecies.Kartana, EnumSpecies.Guzzlord, EnumSpecies.Stakataka, EnumSpecies.Blacephalon);

    public NPCUltra(World world) {
        super(world);
        this.npcLocation = SpawnLocation.LandVillager;
    }

    @Override
    public String getDisplayText() {
        return "";
    }

    @Override
    public String getTexture() {
        return "pixelmon:textures/steve/ultra.png";
    }

    @Override
    public boolean interactWithNPC(EntityPlayer player, EnumHand hand) {
        Optional<PlayerStorage> oStorage;
        if (player.getHeldItem(hand).getItem() instanceof ItemNPCEditor) {
            this.setDead();
        }
        if (player instanceof EntityPlayerMP && (oStorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player)).isPresent()) {
            PlayerStorage storage = oStorage.get();
            if (!storage.playerData.hasObtainedPoipole()) {
                ArrayList<String> missingPokemon = new ArrayList<String>();
                for (EnumSpecies species : QUEST_POKEMON) {
                    if (storage.pokedex.hasCaught(species.getNationalPokedexInteger())) continue;
                    missingPokemon.add(species.getPokemonName());
                }
                if (missingPokemon.isEmpty()) {
                    PokemonSpec spec = PokemonSpec.from(EnumSpecies.Poipole);
                    EntityPixelmon pixelmon = spec.create(FMLCommonHandler.instance().getMinecraftServerInstance().getEntityWorld());
                    storage.addToParty(pixelmon);
                    storage.playerData.setObtainedPoipole(true);
                    player.sendMessage(new TextComponentTranslation("pixelmon.poipole.obtained", new Object[0]));
                } else {
                    player.sendMessage(new TextComponentTranslation("pixelmon.poipole.notobtained", String.join((CharSequence)", ", missingPokemon)));
                }
            }
        }
        return true;
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        super.readEntityFromNBT(nbt);
        this.initDefaultAI();
    }
}

