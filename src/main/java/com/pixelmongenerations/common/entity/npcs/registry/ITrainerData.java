/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.npcs.registry;

import com.pixelmongenerations.common.entity.npcs.registry.TrainerChat;

public interface ITrainerData {
    public TrainerChat getChat(int var1);

    public String getName(int var1);
}

