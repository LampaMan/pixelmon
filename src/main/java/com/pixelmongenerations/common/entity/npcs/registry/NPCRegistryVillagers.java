/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.JsonArray
 *  com.google.gson.JsonObject
 *  com.google.gson.JsonParser
 */
package com.pixelmongenerations.common.entity.npcs.registry;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.pixelmongenerations.common.entity.npcs.registry.GeneralNPCData;
import com.pixelmongenerations.common.entity.npcs.registry.LanguageNotFoundException;
import com.pixelmongenerations.common.entity.npcs.registry.NPCRegistryData;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import net.minecraft.util.JsonUtils;

public class NPCRegistryVillagers {
    void loadVillager(NPCRegistryData thisData, String name, String langCode) throws Exception {
        try {
            JsonObject jsonelement1;
            int i;
            JsonArray jsonarray;
            String path = Pixelmon.modDirectory + "/pixelmon/npcs/villagers/";
            File vDir = new File(path);
            InputStream istream = null;
            if (!PixelmonConfig.useExternalJSONFilesNPCs) {
                istream = ServerNPCRegistry.class.getResourceAsStream("/assets/pixelmon/npcs/villagers/" + name + "_" + langCode.toLowerCase() + ".json");
            } else {
                File file = new File(vDir, name + "_" + langCode.toLowerCase() + ".json");
                if (file.exists()) {
                    istream = new FileInputStream(file);
                }
            }
            if (istream == null) {
                if (langCode.equals("en_us")) {
                    throw new Exception("Error in villager " + name + "_" + langCode.toLowerCase());
                }
                return;
            }
            GeneralNPCData data = new GeneralNPCData(name);
            InputStreamReader reader = new InputStreamReader(istream, StandardCharsets.UTF_8);
            JsonObject json = new JsonParser().parse((Reader)reader).getAsJsonObject();
            if (json.has("skins")) {
                jsonarray = JsonUtils.getJsonArray(json, "skins");
                for (i = 0; i < jsonarray.size(); ++i) {
                    jsonelement1 = jsonarray.get(i).getAsJsonObject();
                    String skin = jsonelement1.get("filename").getAsString();
                    data.addTexture(skin);
                }
            }
            if (json.has("names")) {
                jsonarray = JsonUtils.getJsonArray(json, "names");
                for (i = 0; i < jsonarray.size(); ++i) {
                    jsonelement1 = jsonarray.get(i).getAsJsonObject();
                    String npcname = jsonelement1.get("name").getAsString();
                    data.addName(npcname);
                }
            }
            if (json.has("chat")) {
                jsonarray = JsonUtils.getJsonArray(json, "chat");
                for (i = 0; i < jsonarray.size(); ++i) {
                    jsonelement1 = jsonarray.get(i).getAsJsonObject();
                    JsonArray lines = jsonelement1.get("lines").getAsJsonArray();
                    ArrayList<String> lineList = new ArrayList<String>();
                    for (int j = 0; j < lines.size(); ++j) {
                        JsonObject object = lines.get(j).getAsJsonObject();
                        String text = object.get("text").getAsString();
                        lineList.add(text);
                    }
                    String[] lineArray = new String[lineList.size()];
                    lineArray = lineList.toArray(lineArray);
                    data.addChat(lineArray);
                }
            }
            thisData.npcs.add(data);
        }
        catch (Exception e) {
            throw new Exception("Error in villager " + name + "_" + langCode.toLowerCase(), e);
        }
    }

    public GeneralNPCData getNext(String index) {
        ArrayList<GeneralNPCData> npcListUs = ServerNPCRegistry.getEnglishNPCs();
        if (npcListUs.isEmpty()) {
            return null;
        }
        for (int i = 0; i < npcListUs.size(); ++i) {
            if (!npcListUs.get((int)i).id.equals(index)) continue;
            return npcListUs.get((i + 1) % npcListUs.size());
        }
        return npcListUs.get(0);
    }

    public GeneralNPCData getData(String id) {
        ArrayList<GeneralNPCData> npcListUs = ServerNPCRegistry.getEnglishNPCs();
        if (npcListUs.isEmpty()) {
            return null;
        }
        for (GeneralNPCData data : npcListUs) {
            if (!id.equals(data.id)) continue;
            return data;
        }
        return null;
    }

    public GeneralNPCData getRandom() {
        return RandomHelper.getRandomElementFromList(ServerNPCRegistry.getEnglishNPCs());
    }

    public GeneralNPCData getTranslatedData(String langCode, String id) {
        NPCRegistryData translatedData;
        if (!ServerNPCRegistry.data.containsKey(langCode.toLowerCase())) {
            try {
                ServerNPCRegistry.registerNPCS(langCode.toLowerCase());
            }
            catch (LanguageNotFoundException e2) {
                ServerNPCRegistry.data.put(langCode.toLowerCase(), ServerNPCRegistry.data.get(ServerNPCRegistry.en_us));
            }
            catch (Exception exception) {
                // empty catch block
            }
        }
        if ((translatedData = ServerNPCRegistry.data.get(langCode)) != null) {
            for (GeneralNPCData npc : translatedData.npcs) {
                if (!npc.id.equals(id)) continue;
                return npc;
            }
        }
        for (GeneralNPCData npc : ServerNPCRegistry.getEnglishNPCs()) {
            if (!npc.id.equals(id)) continue;
            return npc;
        }
        return null;
    }

    public String getTranslatedName(String langCode, String id, int index) {
        ArrayList<String> names = this.getTranslatedData((String)langCode.toLowerCase(), (String)id).names;
        if (index >= names.size()) {
            index = 0;
        }
        return names.get(index);
    }

    public String[] getTranslatedChat(String langCode, String id, int index) {
        ArrayList<String[]> chat = this.getTranslatedData((String)langCode.toLowerCase(), (String)id).chat;
        if (index >= chat.size()) {
            index = 0;
        }
        return chat.get(index);
    }
}

