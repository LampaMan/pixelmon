/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.npcs.registry;

import com.pixelmongenerations.common.entity.npcs.registry.ITrainerData;
import com.pixelmongenerations.common.entity.npcs.registry.TrainerChat;
import com.pixelmongenerations.core.enums.EnumNPCType;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;

public class GymNPCData
implements ITrainerData {
    public String id;
    public EnumNPCType type;
    ArrayList<String> names = new ArrayList();
    public ArrayList<String> textures = new ArrayList();
    ArrayList<String[]> chat = new ArrayList();
    ArrayList<TrainerChat> trainerChat = new ArrayList();
    public int winnings;

    public GymNPCData(String id) {
        this.id = id;
    }

    void addName(String name) {
        this.names.add(name);
    }

    void addTexture(String texture) {
        this.textures.add(texture);
    }

    void addChat(String ... lines) {
        if (this.type == EnumNPCType.ChattingNPC) {
            this.chat.add(lines);
        } else if (this.type == EnumNPCType.Trainer && lines.length == 3) {
            this.trainerChat.add(new TrainerChat(lines[0], lines[1], lines[2]));
        }
    }

    public int getRandomTextureIndex() {
        return RandomHelper.getRandomNumberBetween(0, this.textures.size() - 1);
    }

    public String getRandomTexture() {
        return RandomHelper.getRandomElementFromList(this.textures);
    }

    public int getRandomChatIndex() {
        return RandomHelper.getRandomNumberBetween(0, this.chat.size() - 1);
    }

    public int getRandomTrainerChatIndex() {
        return RandomHelper.getRandomNumberBetween(0, this.trainerChat.size() - 1);
    }

    public int getRandomNameIndex() {
        return RandomHelper.getRandomNumberBetween(0, this.names.size() - 1);
    }

    @Override
    public String getName(int index) {
        if (index >= this.names.size() || index < 0) {
            return this.names.get(0);
        }
        return this.names.get(index);
    }

    @Override
    public TrainerChat getChat(int index) {
        if (index >= this.trainerChat.size() || index < 0) {
            return this.trainerChat.get(0);
        }
        return this.trainerChat.get(index);
    }
}

