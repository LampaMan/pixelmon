/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.npcs.registry;

import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;

public class GeneralNPCData {
    public String id;
    ArrayList<String> names = new ArrayList();
    ArrayList<String> textures = new ArrayList();
    ArrayList<String[]> chat = new ArrayList();

    public GeneralNPCData(String id) {
        this.id = id;
    }

    void addName(String name) {
        this.names.add(name);
    }

    void addTexture(String texture) {
        this.textures.add(texture);
    }

    void addChat(String ... lines) {
        this.chat.add(lines);
    }

    public String getRandomTexture() {
        return RandomHelper.getRandomElementFromList(this.textures);
    }

    public ArrayList<String> getTextures() {
        return this.textures;
    }

    public int getRandomChatIndex() {
        return RandomHelper.getRandomNumberBetween(0, this.chat.size() - 1);
    }

    public int getRandomNameIndex() {
        return RandomHelper.getRandomNumberBetween(0, this.names.size() - 1);
    }
}

