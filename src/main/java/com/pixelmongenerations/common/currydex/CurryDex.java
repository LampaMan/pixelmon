/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableSet
 *  com.google.common.collect.Streams
 */
package com.pixelmongenerations.common.currydex;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Streams;
import com.pixelmongenerations.api.events.CurryEvent;
import com.pixelmongenerations.common.capabilities.curry.CurryData;
import com.pixelmongenerations.common.currydex.CurryDexEntry;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.core.enums.EnumCurryType;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.lang.invoke.LambdaMetafactory;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.common.MinecraftForge;

public class CurryDex {
    private static final Comparator<CurryDexEntry> comparator = Comparator.comparingInt((CurryDexEntry a) -> a.type.ordinal()).thenComparingInt(a -> a.flavor.ordinal());
    List<CurryDexEntry> entries = new ArrayList<CurryDexEntry>();

    public CurryDex() {
    }

    public CurryDex(List<CurryDexEntry> entries) {
        this.entries = entries;
        this.sort();
    }

    public static void add(EntityPlayer player, CurryData data) {
        Optional<PlayerStorage> storage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
        if (data != null && storage.isPresent()) {
            CurryDexEntry dexEntry = new CurryDexEntry();
            dexEntry.setInstant(Instant.now());
            dexEntry.type = data.getCurryType();
            dexEntry.flavor = data.getFlavor();
            dexEntry.rating = data.getRating();
            dexEntry.biome = player.world.getBiome(player.getPosition()).getRegistryName();
            dexEntry.pokemonName = Stream.of(storage.get().partyPokemon).filter(Objects::nonNull).filter(a -> a.hasKey("Nickname")).map(a -> a.getString("Nickname")).map(Entity1Base::getLocalizedName).findFirst().orElse("n/a");
            dexEntry.pos = player.getPosition();
            CurryEvent.AddEntry event = new CurryEvent.AddEntry(player, data, dexEntry);
            if (!MinecraftForge.EVENT_BUS.post(event)) {
                storage.get().curryDex.add(event.getEntry());
            }
        }
    }

    public void add(CurryDexEntry entry) {
        if (this.entries.stream().noneMatch(a -> comparator.compare(entry, (CurryDexEntry)a) == 0) || entry.type == EnumCurryType.Gigantamax && this.entries.stream().noneMatch(a -> a.type == EnumCurryType.Gigantamax)) {
            this.entries.add(entry);
            this.sort();
        }
    }

    private void sort() {
        this.entries.sort(comparator);
    }

    public NBTTagList toTag() {
        return (NBTTagList)this.entries.stream().map(CurryDexEntry::toNbt).collect(NbtListCollector.toNbtList());
    }

    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setTag("CurryDex", this.toTag());
        return compound;
    }

    public static CurryDex fromNbt(NBTTagCompound compound) {
        if (compound.hasKey("CurryDex")) {
            return CurryDex.fromNbt(compound.getTagList("CurryDex", 10));
        }
        return new CurryDex();
    }

    public static CurryDex fromNbt(NBTTagList compound) {
        return new CurryDex(Streams.stream(compound.iterator()).filter(NBTTagCompound.class::isInstance).map(NBTTagCompound.class::cast).map(CurryDexEntry::fromNbt).collect(Collectors.toList()));
    }

    private void clear() {
        this.entries.clear();
    }

    public void sendToPlayer(EntityPlayerMP e) {
    }

    public CurryDexEntry getEntry(int id) {
        return this.entries.get(id);
    }

    public List<CurryDexEntry> getEntries() {
        return this.entries;
    }

    public void setToRead() {
        this.entries.forEach(entry -> {
            entry.newEntry = false;
        });
    }

    public static class NbtListCollector<T extends NBTBase>
    implements Collector<T, NBTTagList, NBTTagList> {
        public static <T extends NBTBase> NbtListCollector<T> toNbtList() {
            return new NbtListCollector<T>();
        }

        @Override
        public Supplier<NBTTagList> supplier() {
            return NBTTagList::new;
        }

        @Override
        public BiConsumer<NBTTagList, T> accumulator() {
            return (list, tag) -> list.appendTag(tag);
        }

        @Override
        public BinaryOperator<NBTTagList> combiner() {
            return (a, b) -> {
                NBTTagList tag = new NBTTagList();
                a.forEach(tag::appendTag);
                b.forEach(tag::appendTag);
                return tag;
            };
        }

        @Override
        public Function<NBTTagList, NBTTagList> finisher() {
            return Function.identity();
        }

        @Override
        public Set<Collector.Characteristics> characteristics() {
            return ImmutableSet.of();
        }
    }
}

