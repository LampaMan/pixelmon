/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.Gson
 *  net.minecraft.launchwrapper.Launch
 *  org.apache.logging.log4j.LogManager
 *  org.apache.logging.log4j.Logger
 */
package com.pixelmongenerations.core;

import com.google.gson.Gson;
import com.pixelmongenerations.api.Tuple;
import com.pixelmongenerations.api.drops.PixelmonDrops;
import com.pixelmongenerations.api.drops.PokemonDrop;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.achievement.PixelmonAchievements;
import com.pixelmongenerations.common.achievement.PixelmonAdvancements;
import com.pixelmongenerations.common.cosmetic.CosmeticData;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.common.entity.pixelmon.drops.DroppedItem;
import com.pixelmongenerations.common.spawning.PixelmonBiomeDictionary;
import com.pixelmongenerations.common.spawning.PixelmonSpawning;
import com.pixelmongenerations.common.world.ModWorldGen;
import com.pixelmongenerations.core.PixelmonPermissions;
import com.pixelmongenerations.core.command.AdjustBankBalance;
import com.pixelmongenerations.core.command.BankTransfer;
import com.pixelmongenerations.core.command.Battle;
import com.pixelmongenerations.core.command.Battle2;
import com.pixelmongenerations.core.command.BlockSnapShot;
import com.pixelmongenerations.core.command.Breed;
import com.pixelmongenerations.core.command.CheckSpawns;
import com.pixelmongenerations.core.command.CommandSchematic;
import com.pixelmongenerations.core.command.Debug;
import com.pixelmongenerations.core.command.DeepStorageCmd;
import com.pixelmongenerations.core.command.DynamaxBand;
import com.pixelmongenerations.core.command.EndBattle;
import com.pixelmongenerations.core.command.FallingStar;
import com.pixelmongenerations.core.command.FillDex;
import com.pixelmongenerations.core.command.Freeze;
import com.pixelmongenerations.core.command.GetBiomeData;
import com.pixelmongenerations.core.command.GiveBackground;
import com.pixelmongenerations.core.command.GiveBerry;
import com.pixelmongenerations.core.command.GiveCosmetic;
import com.pixelmongenerations.core.command.GiveCustomIcon;
import com.pixelmongenerations.core.command.GivePixelSprite;
import com.pixelmongenerations.core.command.GiveServerItem;
import com.pixelmongenerations.core.command.Heal;
import com.pixelmongenerations.core.command.MaxEnergyBeam;
import com.pixelmongenerations.core.command.MegaRing;
import com.pixelmongenerations.core.command.PixelTP;
import com.pixelmongenerations.core.command.PokeGive;
import com.pixelmongenerations.core.command.PokeGiveEgg;
import com.pixelmongenerations.core.command.PokeKill;
import com.pixelmongenerations.core.command.PokeParticle;
import com.pixelmongenerations.core.command.PokeParticleTint;
import com.pixelmongenerations.core.command.PokeRetrieve;
import com.pixelmongenerations.core.command.PokeTint;
import com.pixelmongenerations.core.command.PrintStorage;
import com.pixelmongenerations.core.command.Reload;
import com.pixelmongenerations.core.command.Save;
import com.pixelmongenerations.core.command.SetParty;
import com.pixelmongenerations.core.command.ShinyCharm;
import com.pixelmongenerations.core.command.Spawn;
import com.pixelmongenerations.core.command.Spawning;
import com.pixelmongenerations.core.command.Spectate;
import com.pixelmongenerations.core.command.Stats;
import com.pixelmongenerations.core.command.StatsReset;
import com.pixelmongenerations.core.command.Struc;
import com.pixelmongenerations.core.command.TPToBattleDim;
import com.pixelmongenerations.core.command.Teach;
import com.pixelmongenerations.core.command.TierShow;
import com.pixelmongenerations.core.command.Unlock;
import com.pixelmongenerations.core.command.WarpPlate;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonPotions;
import com.pixelmongenerations.core.data.moves.MoveLoader;
import com.pixelmongenerations.core.data.pokemon.PokemonLoader;
import com.pixelmongenerations.core.economy.AccountHolderImpl;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumRole;
import com.pixelmongenerations.core.event.ForgeListener;
import com.pixelmongenerations.core.event.LootTableInjector;
import com.pixelmongenerations.core.event.PixelmonListener;
import com.pixelmongenerations.core.event.achievements.AchievementListener;
import com.pixelmongenerations.core.handler.RoleHandler;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.PacketRegistry;
import com.pixelmongenerations.core.proxy.CommonProxy;
import com.pixelmongenerations.core.storage.AsyncStorageWrapper;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.util.PixelSounds;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.function.Supplier;
import javax.net.ssl.HttpsURLConnection;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.launchwrapper.Launch;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppedEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(
        modid=Pixelmon.MODID,
        name=Pixelmon.NAME,
        version=Pixelmon.VERSION,
//        dependencies="required-after:librarianlib",
        guiFactory="com.pixelmongenerations.client.gui.factory.GuiFactoryPixelmon")
public class Pixelmon {

    public static final Logger LOGGER = LogManager.getLogger("PixelmonGenerations");
    public static final String MODID = "pixelmon";
    public static final String NAME = "Pixelmon";
    public static final String VERSION = "8.6.2";

    @Mod.Instance(value="pixelmon")
    public static Pixelmon INSTANCE;
    @SidedProxy(clientSide="com.pixelmongenerations.core.proxy.ClientProxy", serverSide="com.pixelmongenerations.core.proxy.CommonProxy")
    public static CommonProxy PROXY;
    public static SimpleNetworkWrapper NETWORK;
    public static PixelmonAdvancements ADVANCEMENTS;
    public static File modDirectory;
    public static boolean devEnvironment;
    private RoleHandler roleHandler;
    public static Class defaultAccountHolder;
    public static Function<UUID, CosmeticData> cosmeticDataFactory;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        devEnvironment = (Boolean)Launch.blackboard.get("fml.deobfuscatedEnvironment");
        LOGGER.info("Loading Pixelmon Generations version " + VERSION + (devEnvironment ? " (DEV)" : ""));
        if (event.getSide() == Side.CLIENT && PixelmonConfig.enableRichPresence) {
            try {
                Class<?> tmp = Class.forName("com.pixelmongenerations.client.util.PixelmonDRPC");
                tmp.getMethod("setup").invoke(null);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.queryGenerationsRoles();
        modDirectory = new File(event.getModConfigurationDirectory().getParent());
        NETWORK = NetworkRegistry.INSTANCE.newSimpleChannel(MODID);
        PacketRegistry.registerPackets();
        PixelmonBiomeDictionary.setupCustomBiomeTags();
        PixelmonConfig.init(new File("./config/pixelmon.hocon"));
        PokemonSpec.registerDefaultExtraSpecs();
        MinecraftForge.EVENT_BUS.register(new AchievementListener());
        MinecraftForge.EVENT_BUS.register(new PixelmonListener());
        MinecraftForge.EVENT_BUS.register(new ForgeListener());
        MinecraftForge.EVENT_BUS.register(new PixelmonPotions());
        MinecraftForge.EVENT_BUS.register(new LootTableInjector());
        MinecraftForge.EVENT_BUS.register(new PixelmonDrops());
        ADVANCEMENTS = new PixelmonAdvancements();
        PixelmonAchievements.SetupAchievements();
        ModWorldGen.registerDimension();
        PROXY.registerKeyBindings();
        PROXY.removeDungeonMobs();
        PROXY.preInit();
        MoveLoader.loadMoves();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        PokemonLoader.loadPokemon();
        PixelmonPermissions.registerPermissions();
        NetworkRegistry.INSTANCE.registerGuiHandler(INSTANCE, PROXY);
        PROXY.registerInteractions();
        PROXY.registerRenderers();
        PROXY.init();
        PROXY.registerTickHandlers();
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        PROXY.postInitClient();
        PROXY.postInitServer();
        if (!PixelmonConfig.allowNonPixelmonMobs) {
            ForgeRegistries.BIOMES.forEach(biome -> {
                for (EnumCreatureType type : EnumCreatureType.values()) {
                    biome.getSpawnableList(type).clear();
                }
            });
        }
        if (PixelmonConfig.expCharmMaxChance == 0) {
            Supplier<Float> chanceSupplier = () -> Float.valueOf(PixelmonConfig.expCharmMaxChance);
            PokemonDrop.createBuilder().drop(Tuple.of(chanceSupplier, new DroppedItem(new ItemStack(PixelmonItems.expCharm), 1){

                @Override
                public void drop(Vec3d position, EntityPlayerMP player) {
                    if (player.getServer() != null) {
                        ChatHandler.sendMessageToAllPlayers(player.getServer(), "[" + (Object)((Object)TextFormatting.DARK_PURPLE) + Pixelmon.NAME + (Object)((Object)TextFormatting.WHITE) + "] " + (Object)((Object)TextFormatting.GREEN) + player.getName() + " has found the rare " + (Object)((Object)TextFormatting.BLUE) + "Exp. Charm!");
                    }
                    DropItemHelper.dropItemOnGround(position, player, this.itemStack, this.rarity != EnumBossMode.NotBoss, false);
                }
            })).build();
        }
    }

    @Mod.EventHandler
    public void onServerStart(FMLServerStartingEvent event) {
        PixelSounds.linkPixelmonSounds();
        event.registerServerCommand(new AdjustBankBalance());
        event.registerServerCommand(new BankTransfer());
        event.registerServerCommand(new Battle());
        event.registerServerCommand(new Battle2());
        event.registerServerCommand(new BlockSnapShot());
        event.registerServerCommand(new Breed());
        event.registerServerCommand(new CheckSpawns());
        event.registerServerCommand(new DeepStorageCmd());
        event.registerServerCommand(new EndBattle());
        event.registerServerCommand(new Freeze());
        event.registerServerCommand(new Heal());
        event.registerServerCommand(new PokeGive());
        event.registerServerCommand(new PokeGiveEgg());
        event.registerServerCommand(new SetParty());
        event.registerServerCommand(new Spawn());
        event.registerServerCommand(new Spawning());
        event.registerServerCommand(new Spectate());
        event.registerServerCommand(new Stats());
        event.registerServerCommand(new StatsReset());
        event.registerServerCommand(new Struc());
        event.registerServerCommand(new Teach());
        event.registerServerCommand(new TierShow());
        event.registerServerCommand(new Unlock());
        event.registerServerCommand(new WarpPlate());
        event.registerServerCommand(new GivePixelSprite());
        event.registerServerCommand(new GiveCustomIcon());
        event.registerServerCommand(new GiveCosmetic());
        event.registerServerCommand(new Save());
        event.registerServerCommand(new PokeTint());
        event.registerServerCommand(new PokeParticle());
        event.registerServerCommand(new PokeParticleTint());
        event.registerServerCommand(new PrintStorage());
        event.registerServerCommand(new Reload());
        event.registerServerCommand(new CommandSchematic());
        event.registerServerCommand(new PokeKill());
        event.registerServerCommand(new PokeRetrieve());
        event.registerServerCommand(new FillDex());
        event.registerServerCommand(new MegaRing());
        event.registerServerCommand(new ShinyCharm());
        event.registerServerCommand(new GiveBerry());
        event.registerServerCommand(new FallingStar());
        event.registerServerCommand(new MaxEnergyBeam());
        event.registerServerCommand(new GiveBackground());
        event.registerServerCommand(new GiveServerItem());
        if (PixelmonConfig.allowDynamax) {
            event.registerServerCommand(new DynamaxBand());
        }
        if (devEnvironment && event.getSide().isClient()) {
            event.registerServerCommand(new Debug());
            event.registerServerCommand(new GetBiomeData());
            event.registerServerCommand(new TPToBattleDim());
            event.registerServerCommand(new PixelTP());
        }
        PixelmonSpawning.startTrackingSpawner();
    }

    @Mod.EventHandler
    public void onServerStopping(FMLServerStoppingEvent event) {
        if (FMLCommonHandler.instance().getSide() == Side.CLIENT) {
            PixelmonStorage.pokeBallManager.clearStorage();
        }
    }

    @Mod.EventHandler
    public void onServerStopped(FMLServerStoppedEvent event) {
        if (FMLCommonHandler.instance().getSide() == Side.SERVER && PixelmonConfig.useAsyncSaving) {
            ((AsyncStorageWrapper)PixelmonStorage.storageAdapter).flush();
            LOGGER.info("Saved all remaining PC & Party data.");
        }
    }

    public Optional<EnumRole> getPlayerRole(String uuid) {
        EnumRole role;
        block5: {
            block4: {
                if (this.roleHandler == null) {
                    return Optional.empty();
                }
                role = this.roleHandler.roles.get(uuid);
                if (role == null) break block4;
                if (role != EnumRole.None) break block5;
            }
            return Optional.empty();
        }
        return Optional.of(role);
    }

    private void queryGenerationsRoles() {
        CompletableFuture.runAsync(new Runnable(){

            @Override
            public void run() {
                try {
                    URL url = new URL("https://pixelmongenerations.com/apis/roles.json");
                    HttpsURLConnection connection = (HttpsURLConnection)url.openConnection();
                    connection.setRequestMethod("GET");
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
                    connection.connect();
                    BufferedReader json = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    Pixelmon.this.roleHandler = (RoleHandler)new Gson().fromJson((Reader)json, RoleHandler.class);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    static {
        defaultAccountHolder = AccountHolderImpl.class;
        cosmeticDataFactory = uuid -> new CosmeticData((UUID)uuid);
    }
}

