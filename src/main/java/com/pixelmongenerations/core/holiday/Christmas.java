/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.holiday;

import com.pixelmongenerations.api.holiday.Holiday;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.time.LocalDate;

public class Christmas
extends Holiday {
    public Christmas() {
        super("christmas", new EnumSpecies[0]);
    }

    public static boolean isValidDay(int day) {
        LocalDate date = LocalDate.now();
        int currentMonth = date.getMonthValue();
        int currentDay = date.getDayOfMonth();
        day = 13 + day;
        return currentMonth == 12 && day >= 13 && day <= currentDay;
    }

    @Override
    public boolean isActive() {
        return this.isWithinDate(12, 13, 24);
    }
}

