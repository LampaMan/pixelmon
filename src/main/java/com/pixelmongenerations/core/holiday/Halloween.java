/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.holiday;

import com.pixelmongenerations.api.holiday.Holiday;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;

public class Halloween
extends Holiday {
    public Halloween() {
        super("halloween", EnumSpecies.Gastly, EnumSpecies.Haunter, EnumSpecies.Gengar);
    }

    @Override
    public void onWildPokemonDefeated(EntityPlayerMP player, PixelmonWrapper pokemonWrapper) {
        Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
        if (!storageOpt.isPresent()) {
            return;
        }
        PlayerStorage storage = storageOpt.get();
        int day = this.getDay();
        if (!storage.playerData.hasRecievedCandy(day)) {
            ChatHandler.sendChat(player, "halloween.received", pokemonWrapper.getPokemonName());
            player.inventory.addItemStackToInventory(new ItemStack(PixelmonItems.candy));
            storage.playerData.setRecievedCandy(day);
        }
    }

    @Override
    public boolean isActive() {
        return this.isWithinDate(10, 1, 31);
    }
}

