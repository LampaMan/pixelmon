/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.holiday;

import com.pixelmongenerations.api.holiday.Holiday;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumSummerTextures;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import net.minecraft.entity.player.EntityPlayerMP;

public class Summer
extends Holiday {
    public Summer() {
        super("summer", EnumSpecies.Sunflora, EnumSpecies.Machamp, EnumSpecies.Lapras, EnumSpecies.Snorunt);
    }

    @Override
    public boolean isActive() {
        return this.isWithinDate(7, 1, 31);
    }

    @Override
    public void onWildPokemonSpawn(EntityPlayerMP player, PokemonSpec pokemonSpec) {
        if (pokemonSpec.specialTexture == 0 && RandomHelper.getRandomNumberBetween(1, 100) == 1) {
            pokemonSpec.specialTexture = EnumSummerTextures.Summer.getId();
        }
    }
}

