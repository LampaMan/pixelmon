/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.core.holiday;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.holiday.Holiday;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumTextures;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;

public class ChristmasTwo
extends Holiday {
    private static ArrayList<String> PARTICLES = Lists.newArrayList("snowflake1", "snowflake2", "snowflake3", "snowflake4", "snowflake5", "snowflake6", "snowflake7", "snowflake8", "snowflake9");
    private static ArrayList<EnumSpecies> WINTER_POKEMON = Lists.newArrayList(EnumSpecies.Abomasnow, EnumSpecies.Baltoy, EnumSpecies.Marill, EnumSpecies.Cinccino, EnumSpecies.Crustle, EnumSpecies.Drampa, EnumSpecies.Gogoat, EnumSpecies.Sableye);

    public ChristmasTwo() {
        super("christmastwo", new EnumSpecies[0]);
    }

    @Override
    public void onWildPokemonSpawn(EntityPlayerMP player, PokemonSpec spec) {
        EnumSpecies species;
        if (spec.shiny.booleanValue() && RandomHelper.getRandomNumberBetween(1, 16) == 1) {
            spec.setParticleId(PARTICLES.get(RandomHelper.rand.nextInt(PARTICLES.size())));
            String biomeName = player.world.getBiome((BlockPos)player.getPosition()).biomeName;
            player.world.getMinecraftServer().getPlayerList().sendMessage(new TextComponentTranslation("chat.type.announcement", (Object)((Object)TextFormatting.LIGHT_PURPLE) + "Pixelmon" + (Object)((Object)TextFormatting.RESET), (Object)((Object)TextFormatting.GREEN) + I18n.translateToLocalFormatted("spawn.eventmessage", spec.name, biomeName)));
        }
        if (WINTER_POKEMON.contains((Object)(species = spec.getSpecies())) && RandomHelper.getRandomNumberBetween(1, 512) == 1) {
            spec.specialTexture = EnumTextures.Halloween.getId();
            String biomeName = player.world.getBiome((BlockPos)player.getPosition()).biomeName;
            player.world.getMinecraftServer().getPlayerList().sendMessage(new TextComponentTranslation("chat.type.announcement", (Object)((Object)TextFormatting.LIGHT_PURPLE) + "Pixelmon" + (Object)((Object)TextFormatting.RESET), (Object)((Object)TextFormatting.GREEN) + I18n.translateToLocalFormatted("spawn.eventmessage", spec.name, biomeName)));
        }
    }

    @Override
    public boolean isActive() {
        return this.isWithinDate(12, 25, 31);
    }
}

