/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.holiday;

import com.pixelmongenerations.api.holiday.Holiday;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumCNYTextures;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import net.minecraft.entity.player.EntityPlayerMP;

public class ChineseNewYears
extends Holiday {
    public ChineseNewYears() {
        super("chinesenewyears", EnumSpecies.Bagon, EnumSpecies.Bunnelby, EnumSpecies.Deerling, EnumSpecies.Diggersby, EnumSpecies.Dunsparce, EnumSpecies.Luxio, EnumSpecies.Luxray, EnumSpecies.Mudbray, EnumSpecies.Mudsdale, EnumSpecies.Salamence, EnumSpecies.Shelgon, EnumSpecies.Shinx, EnumSpecies.Tauros);
    }

    @Override
    public boolean isActive() {
        return this.isWithinDate(2, 1, 7);
    }

    @Override
    public void onWildPokemonSpawn(EntityPlayerMP player, PokemonSpec pokemonSpec) {
        if (pokemonSpec.specialTexture == 0 && RandomHelper.getRandomNumberBetween(1, 100) == 1) {
            pokemonSpec.specialTexture = EnumCNYTextures.CNY.getId();
        }
    }
}

