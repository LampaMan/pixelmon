/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import com.pixelmongenerations.core.enums.EnumBossMode;
import net.minecraft.util.text.translation.I18n;

public enum EnumStatueTextureType {
    OriginalTexture,
    Shiny,
    Stone,
    Gold,
    Bronze,
    Silver,
    BossGreen(EnumBossMode.Uncommon),
    BossYellow(EnumBossMode.Rare),
    BossRed(EnumBossMode.Legendary),
    BossOrange(EnumBossMode.Ultimate),
    Prismarine;

    public EnumBossMode bossMode;

    private EnumStatueTextureType() {
        this(EnumBossMode.NotBoss);
    }

    private EnumStatueTextureType(EnumBossMode bossMode) {
        this.bossMode = bossMode;
    }

    public static EnumStatueTextureType getFromOrdinal(int value) {
        if (value >= EnumStatueTextureType.values().length) {
            return BossRed;
        }
        for (EnumStatueTextureType t : EnumStatueTextureType.values()) {
            if (t.ordinal() != value) continue;
            return t;
        }
        return null;
    }

    public EnumStatueTextureType getNextType(EnumStatueTextureType t) {
        int index = t.ordinal();
        index = index >= EnumStatueTextureType.values().length - 1 ? 0 : ++index;
        return EnumStatueTextureType.getFromOrdinal(index);
    }

    public static EnumStatueTextureType getFromString(String name) {
        for (EnumStatueTextureType t : EnumStatueTextureType.values()) {
            if (!t.toString().equalsIgnoreCase(name)) continue;
            return t;
        }
        return null;
    }

    public String getLocalizedName() {
        return I18n.translateToLocal("enum.statuetex." + this.toString().toLowerCase());
    }
}

