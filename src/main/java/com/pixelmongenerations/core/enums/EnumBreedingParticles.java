/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import net.minecraft.util.ResourceLocation;

public enum EnumBreedingParticles {
    grey(1, new ResourceLocation("pixelmon:textures/particles/heartGrey.png")),
    purple(2, new ResourceLocation("pixelmon:textures/particles/heartPurple.png")),
    blue(3, new ResourceLocation("pixelmon:textures/particles/heartBlue.png")),
    yellow(4, new ResourceLocation("pixelmon:textures/particles/heartYellow.png")),
    red(5, new ResourceLocation("pixelmon:textures/particles/heartRed.png"));

    public int index;
    public ResourceLocation location;

    private EnumBreedingParticles(int index, ResourceLocation location) {
        this.index = index;
        this.location = location;
    }

    public static EnumBreedingParticles getFromIndex(int index) {
        for (EnumBreedingParticles e : EnumBreedingParticles.values()) {
            if (e.index != index) continue;
            return e;
        }
        return grey;
    }
}

