/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.battle;

public enum BattleResults {
    VICTORY,
    DEFEAT,
    DRAW,
    FLEE;

}

