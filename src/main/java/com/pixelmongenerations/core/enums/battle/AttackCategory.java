/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.battle;

import com.google.gson.annotations.SerializedName;
import net.minecraft.client.resources.I18n;

public enum AttackCategory {
    @SerializedName("0") Physical("attack.category.physical"),
    @SerializedName("1") Special("attack.category.special"),
    @SerializedName("2") Status("attack.category.status");

    private final String langKey;

    private AttackCategory(String langKey) {
        this.langKey = langKey;
    }

    public String getLocalizedName() {
        return I18n.format(this.langKey, new Object[0]);
    }

    public int getIndex() {
        return this.ordinal();
    }
}

