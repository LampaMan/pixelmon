/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.battle;

public enum BattleMode {
    Waiting,
    MainMenu,
    ChoosePokemon,
    ChooseBag,
    UseBag,
    ChooseAttack,
    ApplyToPokemon,
    YesNoReplaceMove,
    YesNoForfeit,
    EnforcedSwitch,
    LevelUp,
    ReplaceAttack,
    ReplaceAttackExternal,
    ChooseTargets,
    ChooseRelearnMove,
    ChooseTutor,
    ChooseEther,
    MegaEvolution,
    UltraBurst,
    ChoosePPUp,
    Dynamax;

}

