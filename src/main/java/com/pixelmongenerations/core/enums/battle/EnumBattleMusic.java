/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.battle;

import java.util.Random;
import net.minecraft.util.math.MathHelper;

public enum EnumBattleMusic {
    Intense,
    Boss,
    Hurry,
    Focus,
    Calm,
    Optimistic,
    Fierce,
    Bit,
    Ultra,
    Legendary,
    None;


    public static EnumBattleMusic selectRandomBattleRegular() {
        int id = MathHelper.getInt(new Random(), 0, 6);
        return EnumBattleMusic.values()[id];
    }
}

