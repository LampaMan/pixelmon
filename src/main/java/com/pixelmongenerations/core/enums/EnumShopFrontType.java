/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import com.pixelmongenerations.client.models.smd.ValveStudioModelLoader;
import net.minecraft.util.ResourceLocation;

public enum EnumShopFrontType {
    SHELF_1(1.8, false),
    SHELF_2(1.8, false),
    ROUND_1(1.2, false),
    ROUND_2(1.2, false),
    CASE_1(1.1, true),
    CASE_2(1.1, true);

    private double height;
    private boolean glass;
    private ValveStudioModel model;
    private ValveStudioModel glassModel;

    private EnumShopFrontType(double height, boolean glass) {
        this.height = height;
        this.glass = glass;
    }

    public double getHeight() {
        return this.height;
    }

    public boolean hasGlass() {
        return this.glass;
    }

    public ValveStudioModel getModel() {
        if (this.model == null) {
            String fileName = this.name().toLowerCase() + ".pqc";
            try {
                ResourceLocation rl = new ResourceLocation("pixelmon:models/blocks/shopdisplay/" + fileName);
                if (ValveStudioModelLoader.instance.accepts(rl)) {
                    this.model = (ValveStudioModel)ValveStudioModelLoader.instance.loadModel(rl);
                }
            }
            catch (Exception var2) {
                System.out.println("Could not load the model: " + fileName);
                var2.printStackTrace();
            }
        }
        return this.model;
    }

    public ValveStudioModel getGlassModel() {
        if (this.glassModel == null) {
            try {
                ResourceLocation rl = new ResourceLocation("pixelmon:models/blocks/shopdisplay/case_glass.pqc");
                if (ValveStudioModelLoader.instance.accepts(rl)) {
                    this.glassModel = (ValveStudioModel)ValveStudioModelLoader.instance.loadModel(rl);
                }
            }
            catch (Exception var2) {
                System.out.println("Could not load the model: case_glass");
                var2.printStackTrace();
            }
        }
        return this.glassModel;
    }
}

