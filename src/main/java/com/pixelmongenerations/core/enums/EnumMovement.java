/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

public enum EnumMovement {
    Jump,
    Crouch,
    Back,
    Forward,
    Left,
    Right;


    public static EnumMovement getMovement(int index) {
        try {
            return EnumMovement.values()[index];
        }
        catch (Exception npe) {
            return null;
        }
    }

    public static boolean hasMovement(String name) {
        try {
            return EnumMovement.valueOf(name) != null;
        }
        catch (Exception e) {
            return false;
        }
    }
}

