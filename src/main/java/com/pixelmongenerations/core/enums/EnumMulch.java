/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import java.util.function.IntUnaryOperator;
import net.minecraft.util.IStringSerializable;

public enum EnumMulch implements IStringSerializable
{
    None(0, IntUnaryOperator.identity(), IntUnaryOperator.identity(), IntUnaryOperator.identity(), IntUnaryOperator.identity(), false, ""),
    Growth(0, a -> (int)((double)a * 0.75), a -> (int)((double)a * 0.5), IntUnaryOperator.identity(), IntUnaryOperator.identity(), false, "growth_mulch"),
    Damp(0, a -> (int)((double)a * 1.5), a -> (int)((double)a * 1.5), IntUnaryOperator.identity(), IntUnaryOperator.identity(), false, "damp_mulch"),
    Stable(0, IntUnaryOperator.identity(), IntUnaryOperator.identity(), a -> (int)((double)a * 1.5), IntUnaryOperator.identity(), false, "damp_mulch"),
    Gooey(0, IntUnaryOperator.identity(), IntUnaryOperator.identity(), IntUnaryOperator.identity(), a -> (int)((double)a * 1.5), false, "gooey_mulch"),
    Rich(2, IntUnaryOperator.identity(), IntUnaryOperator.identity(), IntUnaryOperator.identity(), IntUnaryOperator.identity(), false, "rich_mulch"),
    Surprise(0, IntUnaryOperator.identity(), IntUnaryOperator.identity(), IntUnaryOperator.identity(), IntUnaryOperator.identity(), true, "surprise_mulch"),
    Boost(0, IntUnaryOperator.identity(), a -> 4, IntUnaryOperator.identity(), IntUnaryOperator.identity(), false, "boost_mulch"),
    Amaze(2, IntUnaryOperator.identity(), a -> 4, IntUnaryOperator.identity(), IntUnaryOperator.identity(), true, "amaze_mulch");

    private final int baseYieldIncrease;
    private final IntUnaryOperator growthFunction;
    private final IntUnaryOperator soilDryingFunction;
    private final IntUnaryOperator timeTilDeathFunction;
    private final IntUnaryOperator numberRegrowFunction;
    private final boolean increasedMutationChance;

    private EnumMulch(int baseYieldIncrease, IntUnaryOperator growthFunction, IntUnaryOperator soilDryingFunction, IntUnaryOperator timeTilDeathFunction, IntUnaryOperator numberRegrowFunction, boolean increasedMutationChance, String registry_name) {
        this.baseYieldIncrease = baseYieldIncrease;
        this.growthFunction = growthFunction;
        this.soilDryingFunction = soilDryingFunction;
        this.timeTilDeathFunction = timeTilDeathFunction;
        this.numberRegrowFunction = numberRegrowFunction;
        this.increasedMutationChance = increasedMutationChance;
    }

    public int getBaseYieldIncrease() {
        return this.baseYieldIncrease;
    }

    public IntUnaryOperator getSoilDryingFunction() {
        return this.soilDryingFunction;
    }

    public IntUnaryOperator getTimeTilDeathFunction() {
        return this.timeTilDeathFunction;
    }

    public IntUnaryOperator getNumberRegrowFunction() {
        return this.numberRegrowFunction;
    }

    public boolean isIncreasedMutationChance() {
        return this.increasedMutationChance;
    }

    @Override
    public String getName() {
        return this.name().toLowerCase();
    }

    public static EnumMulch getFromOrdinal(int ordinal) {
        try {
            return EnumMulch.values()[ordinal];
        }
        catch (Exception e) {
            return None;
        }
    }

    public int modifyGrowthTime(int growthTime) {
        return this.growthFunction.applyAsInt(growthTime);
    }

    public int modifyUntilDeath(int growthTime) {
        return this.timeTilDeathFunction.applyAsInt(growthTime);
    }

    public int modifyDryoutTime(int dryoutTime) {
        return this.soilDryingFunction.applyAsInt(dryoutTime);
    }

    public int modifyGrowthAmount(int growthAmount) {
        return this.getNumberRegrowFunction().applyAsInt(9);
    }
}

