/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumEiscue implements IEnumForm
{
    Ice,
    Noice;


    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    public static EnumEiscue getFromIndex(int index) {
        try {
            return EnumEiscue.values()[index];
        }
        catch (IndexOutOfBoundsException e) {
            return Ice;
        }
    }

    public static int getNextIndex(int index) {
        if (index < 0 || index >= EnumEiscue.values().length - 1) {
            return 0;
        }
        return index + 1;
    }

    @Override
    public boolean isDefaultForm() {
        return this == Ice;
    }

    @Override
    public boolean isTemporary() {
        return this == Noice;
    }

    @Override
    public String getProperName() {
        return this.name();
    }
}

