/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumKyurem implements IEnumForm
{
    Normal(0, ""),
    Black(1, "black"),
    White(2, "white");

    private final String suffix;
    private final int form;

    private EnumKyurem(int form, String suffix) {
        this.suffix = suffix;
        this.form = form;
    }

    @Override
    public String getSpriteSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public byte getForm() {
        return (byte)this.form;
    }

    @Override
    public boolean isTemporary() {
        return false;
    }

    @Override
    public boolean hasNoForm() {
        return false;
    }

    @Override
    public boolean isDefaultForm() {
        return false;
    }

    public static EnumKyurem getFromPokemon(String name) {
        if (name.equalsIgnoreCase("Zekrom")) {
            return Black;
        }
        return White;
    }

    @Override
    public String getProperName() {
        return this.name();
    }
}

