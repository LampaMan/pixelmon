/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumDeoxys implements IEnumForm
{
    Normal,
    Attack,
    Defense,
    Speed;


    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    public static EnumDeoxys getFromIndex(int index) {
        try {
            return EnumDeoxys.values()[index];
        }
        catch (IndexOutOfBoundsException e) {
            return Normal;
        }
    }

    public static int getNextIndex(int index) {
        if (index < 0 || index >= EnumDeoxys.values().length - 1) {
            return 0;
        }
        return index + 1;
    }

    @Override
    public boolean isDefaultForm() {
        return this == Normal;
    }

    @Override
    public String getProperName() {
        return this.name();
    }
}

