/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumUrshifu implements IEnumForm
{
    Single,
    Rapid;


    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    public static EnumUrshifu getFromIndex(int index) {
        try {
            return EnumUrshifu.values()[index];
        }
        catch (IndexOutOfBoundsException e) {
            return Single;
        }
    }

    public static int getNextIndex(int index) {
        if (index < 0 || index >= EnumUrshifu.values().length - 1) {
            return 0;
        }
        return index + 1;
    }

    @Override
    public boolean isDefaultForm() {
        return this == Single;
    }

    @Override
    public String getProperName() {
        return this.name();
    }
}

