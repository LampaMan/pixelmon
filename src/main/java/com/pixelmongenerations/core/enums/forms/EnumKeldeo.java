/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumKeldeo implements IEnumForm
{
    ORDINARY("Ordinary"),
    RESOLUTE("Resolute");

    private String name;

    private EnumKeldeo(String name) {
        this.name = name;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    @Override
    public boolean isDefaultForm() {
        return this == ORDINARY;
    }

    @Override
    public String getProperName() {
        return this.name;
    }
}

