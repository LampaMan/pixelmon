/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumForms implements IEnumForm
{
    NoForm(0, "", false),
    Mega(1, "mega", true),
    MegaX(1, "mega-x", true),
    MegaY(2, "mega-y", true),
    Alolan(3, "alolan", false),
    Halloween(4, "halloween", false),
    Hat(5, "hat", false),
    Ninja(6, "ninja", false),
    Shadow(7, "shadow", false),
    Special(8, "special", false),
    Event(9, "event", false),
    Galarian(10, "galarian", false);

    private final byte form;
    private final String suffix;
    private final boolean isTemp;

    private EnumForms(int form, String suffix, boolean isTemp) {
        this.form = (byte)form;
        this.suffix = suffix;
        this.isTemp = isTemp;
    }

    @Override
    public String getSpriteSuffix() {
        return this == NoForm ? "" : "-" + this.suffix;
    }

    @Override
    public String getFormSuffix() {
        return this == NoForm ? "" : "-" + this.suffix;
    }

    @Override
    public byte getForm() {
        return this.form;
    }

    @Override
    public boolean isTemporary() {
        return this.isTemp;
    }

    @Override
    public boolean hasNoForm() {
        return true;
    }

    @Override
    public boolean isDefaultForm() {
        return this == NoForm;
    }

    @Override
    public String format(String contents) {
        String properName = this.getProperName();
        if (this == MegaX || this == MegaY) {
            return contents + " " + properName;
        }
        if (!properName.isEmpty()) {
            return properName + " " + contents;
        }
        return contents;
    }

    @Override
    public String getProperName() {
        return this.isDefaultForm() ? "" : (this == MegaX ? "X" : (this == MegaY ? "Y" : this.name()));
    }
}

