/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumToxtricity implements IEnumForm
{
    Amped,
    Lowkey;


    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    public static EnumToxtricity getFromIndex(int index) {
        try {
            return EnumToxtricity.values()[index];
        }
        catch (IndexOutOfBoundsException e) {
            return Amped;
        }
    }

    public static int getNextIndex(int index) {
        if (index < 0 || index >= EnumToxtricity.values().length - 1) {
            return 0;
        }
        return index + 1;
    }

    @Override
    public boolean isDefaultForm() {
        return this == Amped;
    }

    @Override
    public String getProperName() {
        return this.name();
    }
}

