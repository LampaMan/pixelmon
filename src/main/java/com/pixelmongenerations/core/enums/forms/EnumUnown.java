/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumUnown implements IEnumForm
{
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
    Question("?"),
    Exclamation("!");

    private String altName;

    private EnumUnown(String altName) {
        this.altName = altName;
    }

    private EnumUnown() {
        this.altName = this.name();
    }

    @Override
    public String getSpriteSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public String getFormSuffix() {
        return "";
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    public static EnumUnown getFromIndex(int index) {
        try {
            return EnumUnown.values()[index];
        }
        catch (IndexOutOfBoundsException e) {
            return A;
        }
    }

    public static EnumUnown getFromName(String name) {
        for (EnumUnown form : EnumUnown.values()) {
            if (!form.name().equals(name) && !form.altName.equals(name)) continue;
            return form;
        }
        return null;
    }

    @Override
    public String getProperName() {
        return this.altName != null ? this.altName : this.name();
    }
}

