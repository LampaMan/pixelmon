/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumHoopa implements IEnumForm
{
    CONFINED("Confined"),
    UNBOUND("Unbound");

    private String name;

    private EnumHoopa(String name) {
        this.name = name;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    @Override
    public boolean isDefaultForm() {
        return this == CONFINED;
    }

    @Override
    public String getProperName() {
        return this.name;
    }
}

