/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumValentine implements IEnumForm
{
    UNLOVED(""),
    LOVED("Loved");

    private String name;

    private EnumValentine(String name) {
        this.name = name;
    }

    @Override
    public String getFormSuffix() {
        return this == UNLOVED ? "" : "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    @Override
    public boolean isDefaultForm() {
        return this == UNLOVED;
    }

    @Override
    public boolean hasNoForm() {
        return true;
    }

    @Override
    public String getProperName() {
        return this.name;
    }
}

