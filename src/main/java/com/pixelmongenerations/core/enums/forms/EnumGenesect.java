/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumGenesect implements IEnumForm
{
    NORMAL("Normal"),
    BURN("Burn"),
    CHILL("Chill"),
    DOUSE("Douse"),
    SHOCK("Shock");

    private String name;

    private EnumGenesect(String name) {
        this.name = name;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    @Override
    public boolean isDefaultForm() {
        return this == NORMAL;
    }

    @Override
    public String getProperName() {
        return this.name;
    }
}

