/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumGreninja implements IEnumForm
{
    BASE,
    BATTLE_BOND,
    ASH;


    @Override
    public String getFormSuffix() {
        if (this == ASH) {
            return "-" + this.name().toLowerCase();
        }
        return "";
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    @Override
    public boolean isTemporary() {
        return this == ASH;
    }

    @Override
    public IEnumForm getDefaultFromTemporary() {
        return this == ASH || this == BATTLE_BOND ? BATTLE_BOND : BASE;
    }

    @Override
    public boolean isDefaultForm() {
        return this == BASE;
    }

    @Override
    public String getProperName() {
        return this == ASH ? "Ash" : "";
    }
}

