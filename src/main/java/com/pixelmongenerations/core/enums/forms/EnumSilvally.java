/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumSilvally implements IEnumForm
{
    NORMAL(EnumType.Normal),
    GRASS(EnumType.Grass),
    FIRE(EnumType.Fire),
    WATER(EnumType.Water),
    FLYING(EnumType.Flying),
    BUG(EnumType.Bug),
    POISON(EnumType.Poison),
    ELECTRIC(EnumType.Electric),
    PSYCHIC(EnumType.Psychic),
    ROCK(EnumType.Rock),
    GROUND(EnumType.Ground),
    DARK(EnumType.Dark),
    GHOST(EnumType.Ghost),
    STEEL(EnumType.Steel),
    FIGHTING(EnumType.Fighting),
    ICE(EnumType.Ice),
    DRAGON(EnumType.Dragon),
    FAIRY(EnumType.Fairy);

    public final EnumType type;

    private EnumSilvally(EnumType type) {
        this.type = type;
    }

    @Override
    public String getFormSuffix() {
        if (this != NORMAL) {
            return "-" + this.name().toLowerCase();
        }
        return "";
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    public static EnumSilvally getForm(EnumType type) {
        for (EnumSilvally silvally : EnumSilvally.values()) {
            if (silvally.type != type) continue;
            return silvally;
        }
        return NORMAL;
    }

    @Override
    public boolean isDefaultForm() {
        return this == NORMAL;
    }

    @Override
    public String getProperName() {
        return this.type.getName();
    }

    @Override
    public boolean hasNoForm() {
        return false;
    }
}

