/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumZygarde implements IEnumForm
{
    Fifty(0, "fifty"),
    Ten(1, "ten"),
    Complete50(2, "complete"),
    Complete10(3, "complete");

    private final String suffix;
    private final int form;

    private EnumZygarde(int form, String suffix) {
        this.suffix = suffix;
        this.form = form;
    }

    @Override
    public String getSpriteSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public byte getForm() {
        return (byte)this.form;
    }

    @Override
    public boolean isTemporary() {
        return this == Complete10 || this == Complete50;
    }

    public EnumZygarde getComplete() {
        return this == Ten ? Complete10 : (this == Fifty ? Complete50 : this);
    }

    @Override
    public IEnumForm getDefaultFromTemporary() {
        return this == Complete10 ? Ten : (this == Complete50 ? Fifty : this);
    }

    @Override
    public boolean hasNoForm() {
        return false;
    }

    @Override
    public String getProperName() {
        return this.name();
    }
}

