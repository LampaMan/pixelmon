/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumWormadam implements IEnumForm
{
    Plant,
    Sandy,
    Trash;


    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    public static EnumWormadam getFromIndex(int index) {
        try {
            return EnumWormadam.values()[index];
        }
        catch (IndexOutOfBoundsException var2) {
            return Plant;
        }
    }

    @Override
    public boolean hasNoForm() {
        return false;
    }

    @Override
    public String getProperName() {
        return this.name();
    }
}

