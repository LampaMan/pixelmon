/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumNecrozma implements IEnumForm
{
    Normal(0, ""),
    Dusk(1, "dusk"),
    Dawn(2, "dawn"),
    UltraDawn(3, "ultra"),
    UltraDusk(4, "ultra");

    private final String suffix;
    private final int form;

    private EnumNecrozma(int form, String suffix) {
        this.suffix = suffix;
        this.form = form;
    }

    @Override
    public String getSpriteSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public byte getForm() {
        return (byte)this.form;
    }

    @Override
    public boolean isTemporary() {
        return this == UltraDawn || this == UltraDusk;
    }

    @Override
    public boolean hasNoForm() {
        return false;
    }

    @Override
    public boolean isDefaultForm() {
        return this == Normal;
    }

    @Override
    public String getProperName() {
        return this == UltraDawn || this == UltraDusk ? "Ultra" : this.name();
    }

    @Override
    public IEnumForm getDefaultFromTemporary() {
        if (this == UltraDawn) {
            return Dawn;
        }
        if (this == UltraDusk) {
            return Dusk;
        }
        return this;
    }

    public static EnumNecrozma getFromPokemon(String name) {
        if (name.equalsIgnoreCase("Solgaleo")) {
            return Dusk;
        }
        return Dawn;
    }

    public EnumNecrozma getUltra() {
        if (this == Dusk) {
            return UltraDusk;
        }
        if (this == Dawn) {
            return UltraDawn;
        }
        return null;
    }
}

