/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumGastrodon implements IEnumForm
{
    East,
    West;


    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    @Override
    public String getProperName() {
        return this.name();
    }

    public static EnumGastrodon getFromIndex(int index) {
        try {
            return EnumGastrodon.values()[index];
        }
        catch (IndexOutOfBoundsException e) {
            return East;
        }
    }
}

