/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public enum EnumFurfrou implements IEnumForm
{
    Natural,
    Heart,
    Star,
    Diamond,
    Debutante,
    Matron,
    Dandy,
    LaReine,
    Kabuki,
    Pharaoh;

    private final String suffix = this.name().toLowerCase();
    private final int form = this.ordinal();

    public static EnumFurfrou randomGroomed() {
        return EnumFurfrou.values()[Math.max(1, RandomHelper.rand.nextInt(EnumFurfrou.values().length))];
    }

    public EnumFurfrou next(int index) {
        if (index + 1 < EnumFurfrou.values().length) {
            return EnumFurfrou.values()[index + 1];
        }
        return Heart;
    }

    @Override
    public String getSpriteSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public byte getForm() {
        return (byte)this.form;
    }

    @Override
    public boolean isTemporary() {
        return false;
    }

    @Override
    public boolean hasNoForm() {
        return true;
    }

    @Override
    public String getProperName() {
        return this.name();
    }
}

