/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumGroudon implements IEnumForm
{
    Normal(0, "normal", "Normal"),
    Primal(1, "primal", "Primal");

    private final String suffix;
    private final int form;
    private String name;

    private EnumGroudon(int form, String suffix, String name) {
        this.suffix = suffix;
        this.form = form;
        this.name = name;
    }

    @Override
    public String getSpriteSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public byte getForm() {
        return (byte)this.form;
    }

    @Override
    public boolean isDefaultForm() {
        return this == Normal;
    }

    @Override
    public String getProperName() {
        return this.name;
    }

    @Override
    public boolean isTemporary() {
        return this == Primal;
    }

    @Override
    public IEnumForm getDefaultFromTemporary() {
        return Normal;
    }

    @Override
    public boolean hasNoForm() {
        return true;
    }
}

