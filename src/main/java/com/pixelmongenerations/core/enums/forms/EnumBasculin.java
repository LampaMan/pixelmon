/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumBasculin implements IEnumForm
{
    RED("Red"),
    BLUE("Blue");

    private String proper;

    private EnumBasculin(String proper) {
        this.proper = proper;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    @Override
    public boolean hasNoForm() {
        return false;
    }

    @Override
    public String getProperName() {
        return this.proper;
    }
}

