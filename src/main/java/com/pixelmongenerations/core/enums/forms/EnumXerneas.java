/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumXerneas implements IEnumForm
{
    NEUTRAL("Neutral"),
    ACTIVE("Active");

    private String name;

    private EnumXerneas(String name) {
        this.name = name;
    }

    @Override
    public String getSpriteSuffix() {
        return "";
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    @Override
    public boolean hasNoForm() {
        return false;
    }

    @Override
    public boolean isDefaultForm() {
        return this == NEUTRAL;
    }

    @Override
    public String getProperName() {
        return this.name;
    }
}

