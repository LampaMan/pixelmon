/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumAlcremie implements IEnumForm
{
    BerryCaramelSwirl(0, "berry-caramelswirl"),
    BerryLemonCream(1, "berry-lemoncream"),
    BerryMatchaCream(2, "berry-matchacream"),
    BerryMintCream(3, "berry-mintcream"),
    BerryRainbowSwirl(4, "berry-rainbowswirl"),
    BerryRubyCream(5, "berry-rubycream"),
    BerryRubySwirl(6, "berry-rubyswirl"),
    BerrySaltedCream(7, "berry-saltedcream"),
    BerryVanillaCream(8, "berry-vanillacream"),
    CloverCaramelSwirl(9, "clover-caramelswirl"),
    CloverLemonCream(10, "clover-lemoncream"),
    CloverMatchaCream(11, "clover-matchacream"),
    CloverMintCream(12, "clover-mintcream"),
    CloverRainbowSwirl(13, "clover-rainbowswirl"),
    CloverRubyCream(14, "clover-rubycream"),
    CloverRubySwirl(15, "clover-rubyswirl"),
    CloverSaltedCream(16, "clover-saltedcream"),
    CloverVanillaCream(17, "clover-vanillacream"),
    FlowerCaramelSwirl(18, "flower-caramelswirl"),
    FlowerLemonCream(19, "flower-lemoncream"),
    FlowerMatchaCream(20, "flower-matchacream"),
    FlowerMintCream(21, "flower-mintcream"),
    FlowerRainbowSwirl(22, "flower-rainbowswirl"),
    FlowerRubyCream(23, "flower-rubycream"),
    FlowerRubySwirl(24, "flower-rubyswirl"),
    FlowerSaltedCream(25, "flower-saltedcream"),
    FlowerVanillaCream(26, "flower-vanillacream"),
    LoveCaramelSwirl(27, "love-caramelswirl"),
    LoveLemonCream(28, "love-lemoncream"),
    LoveMatchaCream(29, "love-matchacream"),
    LoveMintCream(30, "love-mintcream"),
    LoveRainbowSwirl(31, "love-rainbowswirl"),
    LoveRubyCream(32, "love-rubycream"),
    LoveRubySwirl(33, "love-rubyswirl"),
    LoveSaltedCream(34, "love-saltedcream"),
    LoveVanillaCream(35, "love-vanillacream"),
    RibbonCaramelSwirl(36, "ribbon-caramelswirl"),
    RibbonLemonCream(37, "ribbon-lemoncream"),
    RibbonMatchaCream(38, "ribbon-matchacream"),
    RibbonMintCream(39, "ribbon-mintcream"),
    RibbonRainbowSwirl(40, "ribbon-rainbowswirl"),
    RibbonRubyCream(41, "ribbon-rubycream"),
    RibbonRubySwirl(42, "ribbon-rubyswirl"),
    RibbonSaltedCream(43, "ribbon-saltedcream"),
    RibbonVanillaCream(44, "ribbon-vanillacream"),
    StarCaramelSwirl(45, "star-caramelswirl"),
    StarLemonCream(46, "star-lemoncream"),
    StarMatchaCream(47, "star-matchacream"),
    StarMintCream(48, "star-mintcream"),
    StarRainbowSwirl(49, "star-rainbowswirl"),
    StarRubyCream(50, "star-rubycream"),
    StarRubySwirl(51, "star-rubyswirl"),
    StarSaltedCream(52, "star-saltedcream"),
    StarVanillaCream(53, "star-vanillacream"),
    StrawberryCaramelSwirl(54, "strawberry-caramelswirl"),
    StrawberryLemonCream(55, "strawberry-lemoncream"),
    StrawberryMatchaCream(56, "strawberry-matchacream"),
    StrawberryMintCream(57, "strawberry-mintcream"),
    StrawberryRainbowSwirl(58, "strawberry-rainbowswirl"),
    StrawberryRubyCream(59, "strawberry-rubycream"),
    StrawberryRubySwirl(60, "strawberry-rubyswirl"),
    StrawberrySaltedCream(61, "strawberry-saltedcream"),
    StrawberryVanillaCream(62, "strawberry-vanillacream");

    private final String suffix;
    private final int form;

    private EnumAlcremie(int form, String suffix) {
        this.suffix = suffix;
        this.form = form;
    }

    @Override
    public String getSpriteSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public boolean isDefaultForm() {
        return this == BerryCaramelSwirl;
    }

    @Override
    public byte getForm() {
        return (byte)this.form;
    }

    @Override
    public boolean isTemporary() {
        return false;
    }

    @Override
    public boolean hasNoForm() {
        return false;
    }

    @Override
    public String getProperName() {
        return this.name();
    }

    public static EnumAlcremie getFromName(String suffix) {
        for (EnumAlcremie form : EnumAlcremie.values()) {
            if (!form.suffix.equals(suffix)) continue;
            return form;
        }
        return null;
    }
}

