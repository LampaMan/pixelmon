/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.TerrainExamine;
import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumBurmy implements IEnumForm
{
    Plant,
    Sandy,
    Trash;


    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    public static EnumBurmy getFromIndex(int index) {
        try {
            return EnumBurmy.values()[index];
        }
        catch (IndexOutOfBoundsException e) {
            return Plant;
        }
    }

    public static EnumBurmy getFromType(TerrainExamine.TerrainType type) {
        switch (type) {
            case Grass: {
                return Plant;
            }
            case Cave: 
            case Sand: 
            case Burial: {
                return Sandy;
            }
        }
        return Trash;
    }

    @Override
    public String getProperName() {
        return this.name();
    }
}

