/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumMeowstic implements IEnumForm
{
    Male,
    Female;


    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    @Override
    public String getProperName() {
        return this.name();
    }

    public static EnumMeowstic getFromIndex(int index) {
        try {
            return EnumMeowstic.values()[index];
        }
        catch (IndexOutOfBoundsException e) {
            return Male;
        }
    }
}

