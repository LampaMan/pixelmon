/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumRotom implements IEnumForm
{
    NORMAL(""),
    HEAT("Heat"),
    WASH("Wash"),
    FROST("Frost"),
    FAN("Fan"),
    MOW("Mow");

    private String name;

    private EnumRotom(String name) {
        this.name = name;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    @Override
    public String getProperName() {
        return this.name;
    }

    @Override
    public boolean isDefaultForm() {
        return this == NORMAL;
    }
}

