/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

public enum EnumRotomForm {
    Default(0),
    Heat(1),
    Wash(2),
    Frost(3),
    Fan(4),
    Mow(5);

    private int form;

    private EnumRotomForm(int form) {
        this.form = form;
    }

    public int getForm() {
        return this.form;
    }
}

