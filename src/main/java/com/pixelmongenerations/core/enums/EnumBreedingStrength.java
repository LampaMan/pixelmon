/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

public enum EnumBreedingStrength {
    NONE(0.0f),
    LOW(0.5f),
    MEDIUM(1.0f),
    HIGH(1.5f),
    MAX(2.0f);

    public final float value;

    private EnumBreedingStrength(float breedingStrength) {
        this.value = breedingStrength;
    }

    public static EnumBreedingStrength of(float breedingStrength) {
        for (int i = EnumBreedingStrength.values().length - 1; i >= 0; --i) {
            if (EnumBreedingStrength.values()[i].value > breedingStrength) continue;
            return EnumBreedingStrength.values()[i];
        }
        return NONE;
    }
}

