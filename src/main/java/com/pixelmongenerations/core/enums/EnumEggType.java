/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public enum EnumEggType {
    Base(""),
    Togepi("Togepi");

    private String pokemonName;

    private EnumEggType(String pokemonName) {
        this.pokemonName = pokemonName;
    }

    @SideOnly(value=Side.CLIENT)
    public ResourceLocation getModelTexture() {
        return new ResourceLocation("pixelmon", "textures/pokemon/pokemon-egg/" + (!this.pokemonName.isEmpty() ? this.pokemonName : "regular") + ".png");
    }

    public static EnumEggType getTypeFromPokemon(String name) {
        for (EnumEggType eggType : EnumEggType.values()) {
            if (!eggType.pokemonName.equals(name)) continue;
            return eggType;
        }
        return Base;
    }
}

