/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import com.pixelmongenerations.api.HexColor;
import com.pixelmongenerations.common.cosmetic.EnumCosmetic;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public enum EnumRole {
    Owner("Pixelmon Generations Owner", EnumCosmetic.values()),
    CoOwner("Pixelmon Generations Co-Owner", EnumCosmetic.values()),
    LeadDeveloper((Object)((Object)TextFormatting.YELLOW) + TextFormatting.ITALIC.toString() + "The Best " + HexColor.formatHex("#FCBE72Krabby") + (Object)((Object)TextFormatting.GOLD) + " Gym Leader" + (Object)((Object)TextFormatting.GREEN), EnumCosmetic.values()),
    Developer("Pixelmon Generations Developer", new EnumCosmetic[]{EnumCosmetic.BetaBackpack, EnumCosmetic.BetaCap, EnumCosmetic.BetaSash, EnumCosmetic.DeveloperSash, EnumCosmetic.NitroBackpack, EnumCosmetic.NitroCap, EnumCosmetic.NitroSash}),
    Manager("Pixelmon Generations Manager", EnumCosmetic.values()),
    Staff("A Wild Pixelmon Generations Team Member", new EnumCosmetic[]{EnumCosmetic.BetaBackpack, EnumCosmetic.BetaCap, EnumCosmetic.BetaSash}),
    GenerationsWinner("Pixelmon Generations Champion", new EnumCosmetic[0]),
    GenerationsWinnerStaff("Pixelmon Generations Team Member & Champion", new EnumCosmetic[]{EnumCosmetic.BetaBackpack, EnumCosmetic.BetaCap, EnumCosmetic.BetaSash}),
    SchoolGirl("An Overly Excited School Girl", new EnumCosmetic[0]),
    NitroBooster("Pixelmon Generations Nitro Booster", new EnumCosmetic[]{EnumCosmetic.NitroBackpack, EnumCosmetic.NitroCap, EnumCosmetic.NitroSash}),
    None("", new EnumCosmetic[0]);

    public String name;
    public EnumCosmetic[] cosmetics;

    private EnumRole(String name, EnumCosmetic[] cosmetics) {
        this.name = name;
        this.cosmetics = cosmetics;
    }

    public boolean hasCosmetics() {
        return this.cosmetics.length > 0;
    }

    public EnumCosmetic[] getCosmetics() {
        return this.cosmetics;
    }

    public TextComponentString getJoinMessage(String playerName) {
        return new TextComponentString("[" + (Object)((Object)TextFormatting.DARK_PURPLE) + "Pixelmon" + (Object)((Object)TextFormatting.WHITE) + "] " + (Object)((Object)TextFormatting.GREEN) + this.name + ", " + playerName + " has joined the game!");
    }
}

