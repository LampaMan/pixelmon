/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public enum EnumIncreaseEV {
    HpUp(StatsType.HP, "hpup"),
    Protein(StatsType.Attack, "protein"),
    Iron(StatsType.Defence, "iron"),
    Calcium(StatsType.SpecialAttack, "calcium"),
    Zinc(StatsType.SpecialDefence, "zinc"),
    Carbos(StatsType.Speed, "carbos");

    public String textureName;
    public StatsType statAffected;

    private EnumIncreaseEV(StatsType statAffected, String textureName) {
        this.statAffected = statAffected;
        this.textureName = textureName;
    }

    public static boolean hasIncreaseEV(String name) {
        try {
            return EnumIncreaseEV.valueOf(name) != null;
        }
        catch (Exception e) {
            return false;
        }
    }
}

