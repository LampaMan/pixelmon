/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public enum EnumShinyParticles {
    Base(1, new ResourceLocation("pixelmon:textures/particles/shiny.png")),
    Zubat(2, new ResourceLocation("pixelmon:textures/particles/zubat.png")),
    Spirit(3, new ResourceLocation("pixelmon:textures/particles/spirit.png")),
    Spiderweb(4, new ResourceLocation("pixelmon:textures/particles/spiderweb.png")),
    Spell_Tag(5, new ResourceLocation("pixelmon:textures/particles/spell_tag.png")),
    Jackolantern(6, new ResourceLocation("pixelmon:textures/particles/jackolantern.png")),
    Ghost(7, new ResourceLocation("pixelmon:textures/particles/ghost.png")),
    Krabby(8, new ResourceLocation("pixelmon:textures/particles/krabby.png")),
    OldUltra(9, new ResourceLocation("pixelmon:textures/particles/ultrashiny.png")),
    Ultra(10, new ResourceLocation("pixelmon:textures/particles/ultrashiny.png")),
    Flowers(11, new ResourceLocation("pixelmon:textures/particles/flowers.png")),
    MusicalFlowers(12, new ResourceLocation("pixelmon:textures/particles/musicalflowers.png")),
    PokeHearts(13, new ResourceLocation("pixelmon:textures/particles/pokehearts.png")),
    TwoHearts(14, new ResourceLocation("pixelmon:textures/particles/twohearts.png")),
    Bauble(15, new ResourceLocation("pixelmon:textures/particles/bauble.png")),
    Candycane(16, new ResourceLocation("pixelmon:textures/particles/candycane.png")),
    ChristmasTree(17, new ResourceLocation("pixelmon:textures/particles/christmastree.png")),
    Holly(18, new ResourceLocation("pixelmon:textures/particles/holly.png")),
    Stocking(19, new ResourceLocation("pixelmon:textures/particles/stocking.png")),
    TwinkleLights(20, new ResourceLocation("pixelmon:textures/particles/twinklelights.png")),
    Snowflake1(21, new ResourceLocation("pixelmon:textures/particles/snowflake1.png")),
    Snowflake2(22, new ResourceLocation("pixelmon:textures/particles/snowflake2.png")),
    Snowflake3(23, new ResourceLocation("pixelmon:textures/particles/snowflake3.png")),
    Snowflake4(24, new ResourceLocation("pixelmon:textures/particles/snowflake4.png")),
    Snowflake5(25, new ResourceLocation("pixelmon:textures/particles/snowflake5.png")),
    Snowflake6(26, new ResourceLocation("pixelmon:textures/particles/snowflake6.png")),
    Snowflake7(27, new ResourceLocation("pixelmon:textures/particles/snowflake7.png")),
    Snowflake8(28, new ResourceLocation("pixelmon:textures/particles/snowflake8.png")),
    Snowflake9(29, new ResourceLocation("pixelmon:textures/particles/snowflake9.png")),
    Leaf(30, new ResourceLocation("pixelmon:textures/particles/leaf.png")),
    Shadow(31, new ResourceLocation("pixelmon:textures/particles/shadow.png")),
    Pure(32, new ResourceLocation("pixelmon:textures/particles/pure.png")),
    Petal(33, new ResourceLocation("pixelmon:textures/particles/petal.png"));

    public int index;
    public ResourceLocation location;

    private EnumShinyParticles(int index, ResourceLocation location) {
        this.index = index;
        this.location = location;
    }

    public static EnumShinyParticles getFromIndex(int index) {
        for (EnumShinyParticles e : EnumShinyParticles.values()) {
            if (e.index != index) continue;
            return e;
        }
        return Base;
    }

    public int getID() {
        return this.index;
    }

    @SideOnly(value=Side.CLIENT)
    public ResourceLocation getTexture() {
        return this.location;
    }
}

