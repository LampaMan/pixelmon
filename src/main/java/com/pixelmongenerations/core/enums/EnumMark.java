/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.core.enums;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.spawning.conditions.WorldTime;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumMarkActivity;
import com.pixelmongenerations.core.util.PixelmonPlayerUtils;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.Random;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;

public enum EnumMark {
    None("the Non Existant"),
    Rare("the Recluse"),
    Dawn("the Early Riser"),
    LunchTime("the Peckish"),
    Dusk("the Dozy"),
    SleepyTime("the Sleepy"),
    Sandstorm("the Sandswept"),
    Rainy("the Sodden"),
    Misty("the Mist Drifter"),
    Stormy("the Thunderstruck"),
    Cloudy("the Cloud Watcher"),
    Blizzard("the Shivering"),
    Snowy("the Snow Frolicker"),
    Dry("the Parched"),
    Rowdy("the Rowdy"),
    AbsentMinded("the Spacey"),
    Jittery("the Anxious"),
    Excited("the Giddy"),
    Charismatic("the Radiant"),
    Calmness("the Serene"),
    IntenseMark("the Feisty"),
    ZonedOut("the Daydreamer"),
    Joyful("the Joyful"),
    Angry("the Furious"),
    Smiley("the Beaming"),
    Teary("the Teary-Eyed"),
    Upbeat("the Chipper"),
    Peeved("the Grumpy"),
    Intellectual("the Scholar"),
    Ferocious("the Rampaging"),
    Crafty("the Opportunist"),
    Scowling("the Stern"),
    Kindly("the Kindhearted"),
    Flustered("the Easily Flustered"),
    PumpedUp("the Driven"),
    ZeroEnergy("the Apathetic"),
    Prideful("the Arrogant"),
    Unsure("the Reluctant"),
    Humble("the Humble"),
    Thorny("the Pompous"),
    Vigor("the Lively"),
    Slump("the Worn-Out"),
    Uncommon("the Sociable"),
    Destiny("the Chosen One"),
    Fishing("the Catch of the Day"),
    Curry("the Curry Connoisseur");

    public static ArrayList<EnumMark> RARE_MARKS;
    public static ArrayList<EnumMark> TIME_MARKS;
    public static ArrayList<EnumMark> WEATHER_MARKS;
    public static ArrayList<EnumMark> PERSONALITY_MARKS;
    private String title;

    private EnumMark(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    public ResourceLocation getIconLocation() {
        return new ResourceLocation("pixelmon", "textures/gui/marks/" + this.name().toLowerCase() + ".png");
    }

    public static EnumMark rollMark(EntityPlayerMP player, EntityPixelmon pokemon) {
        return EnumMark.rollMark(player, pokemon, EnumMarkActivity.None);
    }

    public static EnumMark rollMark(EntityPlayerMP player, EntityPixelmon pokemon, EnumMarkActivity activity) {
        int maxValue = PixelmonPlayerUtils.hasItem(player, itemStack -> itemStack.getItem() == PixelmonItems.markCharm) ? 3 : 1;
        Random random = player.world.rand;
        if (activity == EnumMarkActivity.Curry && RandomHelper.getRandomNumberBetween(1, 50) <= maxValue) {
            return Curry;
        }
        if (activity == EnumMarkActivity.Curry) {
            return None;
        }
        if (activity == EnumMarkActivity.Fishing && RandomHelper.getRandomNumberBetween(1, 50) <= maxValue) {
            return Fishing;
        }
        int ticks = (int)(pokemon.world.getWorldTime() % 24000L);
        ArrayList<WorldTime> times = WorldTime.getCurrent(ticks);
        times.removeAll(Lists.newArrayList((Object[])new WorldTime[]{WorldTime.AFTERNOON, WorldTime.MIDNIGHT, WorldTime.DAY}));
        if (times.contains((Object)WorldTime.DAWN) && RandomHelper.getRandomNumberBetween(1, 50) <= maxValue) {
            return Dawn;
        }
        if (times.contains((Object)WorldTime.MIDDAY) && RandomHelper.getRandomNumberBetween(1, 50) <= maxValue) {
            return LunchTime;
        }
        if (times.contains((Object)WorldTime.DUSK) && RandomHelper.getRandomNumberBetween(1, 50) <= maxValue) {
            return Dusk;
        }
        if (times.contains((Object)WorldTime.NIGHT) && RandomHelper.getRandomNumberBetween(1, 50) <= maxValue) {
            return SleepyTime;
        }
        Biome biome = pokemon.world.getBiome(pokemon.getPosition());
        if (biome.getRainfall() == 0.0f && RandomHelper.getRandomNumberBetween(1, 50) <= maxValue) {
            return Math.random() >= 0.5 ? Sandstorm : Dry;
        }
        if (pokemon.world.isRaining() && RandomHelper.getRandomNumberBetween(1, 50) <= maxValue) {
            return Rainy;
        }
        if (times.contains((Object)WorldTime.MORNING) && RandomHelper.getRandomNumberBetween(1, 50) <= maxValue) {
            return Misty;
        }
        if (pokemon.world.isThundering() && RandomHelper.getRandomNumberBetween(1, 50) <= maxValue) {
            return Stormy;
        }
        if (pokemon.world.isRaining() && biome.getRainfall() == 0.0f && RandomHelper.getRandomNumberBetween(1, 50) <= maxValue) {
            return Cloudy;
        }
        if (pokemon.world.isRaining() && biome.isSnowyBiome() && RandomHelper.getRandomNumberBetween(1, 50) <= maxValue) {
            return Math.random() >= 0.5 ? Blizzard : Snowy;
        }
        if (RandomHelper.getRandomNumberBetween(1, 50) <= maxValue) {
            return Uncommon;
        }
        if (RandomHelper.getRandomNumberBetween(1, 100) <= maxValue) {
            return PERSONALITY_MARKS.get(random.nextInt(PERSONALITY_MARKS.size()));
        }
        return None;
    }

    static {
        RARE_MARKS = Lists.newArrayList(Rare);
        TIME_MARKS = Lists.newArrayList(Dawn, LunchTime, Dusk, SleepyTime);
        WEATHER_MARKS = Lists.newArrayList(Sandstorm, Rainy, Misty, Stormy, Cloudy, Blizzard, Snowy, Dry);
        PERSONALITY_MARKS = Lists.newArrayList(Rowdy, AbsentMinded, Jittery, Excited, Charismatic, Calmness, IntenseMark, ZonedOut, Joyful, Angry, Smiley, Teary, Upbeat, Peeved, Intellectual, Ferocious, Crafty, Scowling, Kindly, Flustered, PumpedUp, ZeroEnergy, Prideful, Unsure, Humble, Thorny, Vigor, Slump);
    }
}

