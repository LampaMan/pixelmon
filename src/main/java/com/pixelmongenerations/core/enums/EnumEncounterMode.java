/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import net.minecraft.util.text.translation.I18n;

public enum EnumEncounterMode {
    Once,
    OncePerPlayer,
    OncePerMCDay,
    OncePerDay,
    Unlimited;


    public static EnumEncounterMode getFromIndex(int i) {
        for (EnumEncounterMode e : EnumEncounterMode.values()) {
            if (e.ordinal() != i) continue;
            return e;
        }
        return null;
    }

    public static EnumEncounterMode getNextMode(EnumEncounterMode mode) {
        int index = mode.ordinal();
        index = index == EnumEncounterMode.values().length - 1 ? 0 : ++index;
        for (EnumEncounterMode e : EnumEncounterMode.values()) {
            if (e.ordinal() != index) continue;
            return e;
        }
        return null;
    }

    public static boolean hasEncounterMode(String name) {
        try {
            return EnumEncounterMode.valueOf(name) != null;
        }
        catch (Exception e) {
            return false;
        }
    }

    public String getLocalizedName() {
        return I18n.translateToLocal("enum.trainerEncounter." + this.name().toLowerCase());
    }
}

