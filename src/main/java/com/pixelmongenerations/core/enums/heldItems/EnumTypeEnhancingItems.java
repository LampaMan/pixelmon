/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.heldItems;

public enum EnumTypeEnhancingItems {
    blackBelt,
    blackGlasses,
    charcoal,
    dragonFang,
    hardStone,
    magnet,
    miracleSeed,
    mysticWater,
    neverMeltIce,
    poisonBarb,
    sharpBeak,
    silkScarf,
    silverPowder,
    softSand,
    spellTag,
    twistedSpoon,
    plate,
    orb;

}

