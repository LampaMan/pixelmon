/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.heldItems;

public enum EnumBerryStatIncrease {
    liechiBerry,
    ganlonBerry,
    salacBerry,
    petayaBerry,
    apicotBerry,
    lansatBerry,
    starfBerry;

}

