/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.heldItems;

import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public enum EnumChoiceItems {
    ChoiceBand(StatsType.Attack),
    ChoiceScarf(StatsType.Speed),
    ChoiceSpecs(StatsType.SpecialAttack);

    public StatsType effectType;

    private EnumChoiceItems(StatsType type) {
        this.effectType = type;
    }
}

