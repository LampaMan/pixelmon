/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.heldItems;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.forms.EnumForms;
import com.pixelmongenerations.core.enums.forms.EnumNecrozma;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum EnumZCrystals {
    BuginiumZ(EnumType.Bug),
    DarikniumZ(EnumType.Dark),
    DragoniumZ(EnumType.Dragon),
    ElectriumZ(EnumType.Electric),
    FairiumZ(EnumType.Fairy),
    FightiniumZ(EnumType.Fighting),
    FiriumZ(EnumType.Fire),
    FlyiniumZ(EnumType.Flying),
    GhostiumZ(EnumType.Ghost),
    GrassiumZ(EnumType.Grass),
    GroundiumZ(EnumType.Ground),
    IciumZ(EnumType.Ice),
    NormaliumZ(EnumType.Normal),
    PoisoniumZ(EnumType.Poison),
    PsychiumZ(EnumType.Psychic),
    RockiumZ(EnumType.Rock),
    SteeliumZ(EnumType.Steel),
    WateriumZ(EnumType.Water),
    AloraichiumZ(Collections.singletonList(new PokemonSpec(EnumSpecies.Raichu.name).setForm(EnumForms.Alolan.getForm())), "Raichu", "Thunderbolt"),
    DecidiumZ(Collections.singletonList(new PokemonSpec(EnumSpecies.Decidueye.name)), "Decidueye", "Spirit Shackle"),
    EeviumZ(Collections.singletonList(new PokemonSpec(EnumSpecies.Eevee.name)), "Eevee", "Last Resort"),
    InciniumZ(Collections.singletonList(new PokemonSpec(EnumSpecies.Incineroar.name)), "Incineroar", "Darkest Lariat"),
    KommoniumZ(Collections.singletonList(new PokemonSpec(EnumSpecies.Kommoo.name)), "Kommoo", "Clanging Scales"),
    LunaliumZ(Arrays.asList(new PokemonSpec(EnumSpecies.Lunala.name), new PokemonSpec(EnumSpecies.Necrozma.name).setForm(EnumNecrozma.Dawn.getForm())), "Lunala", "Moongeist Beam"),
    LycaniumZ(Collections.singletonList(new PokemonSpec(EnumSpecies.Lycanroc.name)), "Lycanroc", "Stone Edge"),
    MarshadiumZ(Collections.singletonList(new PokemonSpec(EnumSpecies.Marshadow.name)), "Marshadow", "Spectral Thief"),
    MewniumZ(Collections.singletonList(new PokemonSpec(EnumSpecies.Mew.name)), "Mew", "Psychic"),
    MimikiumZ(Collections.singletonList(new PokemonSpec(EnumSpecies.Mimikyu.name)), "Mimikyu", "Play Rough"),
    PikaniumZ(Collections.singletonList(new PokemonSpec(EnumSpecies.Pikachu.name)), "Pikachu", "Volt Tackle"),
    PikashuniumZ(Collections.singletonList(new PokemonSpec(EnumSpecies.Pikachu.name).setForm(EnumForms.Hat.getForm())), "Pikachu", "Thunderbolt"),
    PrimariumZ(Collections.singletonList(new PokemonSpec(EnumSpecies.Primarina.name)), "Primarina", "Sparkling Aria"),
    SnorliumZ(Collections.singletonList(new PokemonSpec(EnumSpecies.Snorlax.name)), "Snorlax", "Giga Impact"),
    SolganiumZ(Arrays.asList(new PokemonSpec(EnumSpecies.Solgaleo.name), new PokemonSpec(EnumSpecies.Necrozma.name).setForm(EnumNecrozma.Dusk.getForm())), "Solgaleo", "Sunsteel Strike"),
    TapuniumZ(Arrays.asList(new PokemonSpec(EnumSpecies.TapuBulu.name), new PokemonSpec(EnumSpecies.TapuFini.name), new PokemonSpec(EnumSpecies.TapuKoko.name), new PokemonSpec(EnumSpecies.TapuLele.name)), "Tapu", "Nature's Madness"),
    UltranecroziumZ(Arrays.asList(new PokemonSpec(EnumSpecies.Necrozma.name).setForm(EnumNecrozma.UltraDawn.getForm()), new PokemonSpec(EnumSpecies.Necrozma.name).setForm(EnumNecrozma.UltraDusk.getForm())), Arrays.asList(new PokemonSpec(EnumSpecies.Necrozma.name).setForm(EnumNecrozma.Dawn.getForm()), new PokemonSpec(EnumSpecies.Necrozma.name).setForm(EnumNecrozma.Dusk.getForm())), "Necrozma", "Photon Geyser");

    EnumType type;
    List<PokemonSpec> pokemon;
    String requiredAttack;
    private List<PokemonSpec> ultraBurstSpecs = new ArrayList<PokemonSpec>();
    String key;

    private EnumZCrystals(EnumType enumType) {
        this.type = enumType;
        this.key = enumType.name();
        this.pokemon = null;
    }

    private EnumZCrystals(List<PokemonSpec> affectedPokemon, String key, String requiredAttack) {
        this.pokemon = affectedPokemon;
        this.key = key;
        this.requiredAttack = requiredAttack;
    }

    private EnumZCrystals(List<PokemonSpec> affectedPokemon, List<PokemonSpec> ultraBurstSpecs, String key, String requiredAttack) {
        this.pokemon = affectedPokemon;
        this.ultraBurstSpecs = ultraBurstSpecs;
        this.key = key;
        this.requiredAttack = requiredAttack;
    }

    public EnumType getType() {
        return this.type;
    }

    public List<PokemonSpec> getPokemon() {
        return this.pokemon;
    }

    public String getKey() {
        return this.key;
    }

    public String getRequiredAttack() {
        return this.requiredAttack;
    }

    public List<PokemonSpec> getUltraBurstSpecs() {
        return this.ultraBurstSpecs;
    }
}

