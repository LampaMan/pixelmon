/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.heldItems;

public enum EnumBerryStatus {
    cheriBerry,
    chestoBerry,
    pechaBerry,
    rawstBerry,
    aspearBerry,
    persimBerry,
    lumBerry,
    pumkinBerry,
    drashBerry,
    eggantBerry,
    yagoBerry,
    tougaBerry;

}

