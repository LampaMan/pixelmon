/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

public enum EnumShopKeeperType {
    PokemartMain,
    PokemartSecond,
    Spawn;


    public static EnumShopKeeperType getFromString(String string) {
        for (EnumShopKeeperType type : EnumShopKeeperType.values()) {
            if (!type.toString().equalsIgnoreCase(string)) continue;
            return type;
        }
        return null;
    }
}

