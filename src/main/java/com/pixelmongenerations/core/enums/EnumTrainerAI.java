/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import net.minecraft.util.text.translation.I18n;

public enum EnumTrainerAI {
    StandStill,
    StillAndEngage,
    Wander,
    WanderAndEngage;


    public static EnumTrainerAI getFromOrdinal(int ordinal) {
        for (EnumTrainerAI v : EnumTrainerAI.values()) {
            if (v.ordinal() != ordinal) continue;
            return v;
        }
        return null;
    }

    public static EnumTrainerAI getNextMode(EnumTrainerAI mode) {
        int index = mode.ordinal();
        index = index == EnumTrainerAI.values().length - 1 ? 0 : ++index;
        return EnumTrainerAI.getFromOrdinal(index);
    }

    public static boolean hasTrainerAI(String name) {
        try {
            return EnumTrainerAI.valueOf(name) != null;
        }
        catch (Exception e) {
            return false;
        }
    }

    public boolean doesEngage() {
        return this == StillAndEngage || this == WanderAndEngage;
    }

    public String getLocalizedName() {
        return I18n.translateToLocal("enum.trainerAI." + this.name().toLowerCase());
    }
}

