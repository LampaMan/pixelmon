/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

public enum EnumMegaItem {
    Disabled,
    None,
    BraceletORAS;


    public static EnumMegaItem getFromString(String megaItemString) {
        for (EnumMegaItem item : EnumMegaItem.values()) {
            if (!item.toString().equalsIgnoreCase(megaItemString)) continue;
            return item;
        }
        return Disabled;
    }

    public boolean canEvolve() {
        return this != Disabled && this != None;
    }
}

