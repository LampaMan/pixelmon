/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import com.pixelmongenerations.client.models.obj.ObjLoader;
import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import com.pixelmongenerations.client.models.smd.ValveStudioModelLoader;
import com.pixelmongenerations.common.cosmetic.PositionOperation;
import java.util.HashMap;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModel;

public enum EnumCustomModel {
    PillarPlatform("blocks/pillar/pillar_platform.obj"),
    PillarColumn("blocks/pillar/pillar_column.obj"),
    PillarColumnFracturedBottom("blocks/pillar/pillar_column_fractured_bottom.obj"),
    PillarColumnFracturedTop("blocks/pillar/pillar_column_fractured_top.obj"),
    Pokeball("pokeballs/base.pqc"),
    Cherishball("pokeballs/cherishball.pqc"),
    Greatball("pokeballs/greatball.pqc"),
    Heavyball("pokeballs/heavyball.pqc"),
    Masterball("pokeballs/masterball.pqc"),
    Netball("pokeballs/netball.pqc"),
    Timerball("pokeballs/timerball.pqc"),
    Beastball("pokeballs/beastball.pqc"),
    Sash("playeritems/sash/sash.pqc"),
    Fez("playeritems/hats/fez.pqc"),
    Fedora("playeritems/hats/fedora.pqc"),
    TopHat("playeritems/hats/tophat.pqc"),
    Monocle("playeritems/hats/monocle.pqc"),
    TrainerHat("playeritems/hats/trainerhat.pqc"),
    PikaHood("playeritems/hats/pikahood.pqc"),
    MegaBraceletORAS("playeritems/megaitems/megabraceletoras.pqc"),
    MegaBraceletORASStone("playeritems/megaitems/megabraceletorasstone.pqc"),
    DynamaxBand("playeritems/bands/dynamaxband.pqc"),
    Backpack("cosmetics/back/backpack/basic.pqc"),
    CharizardBackpack("cosmetics/back/backpack/charizard.pqc"),
    EeveeBackpack("cosmetics/back/backpack/eevee.pqc"),
    GengarBackpack("cosmetics/back/backpack/gengar.pqc"),
    HoopaBackpack("cosmetics/back/backpack/hoopa.pqc"),
    MegaGengarBackpack("cosmetics/back/backpack/megagengar.pqc"),
    PikachuBackpack("cosmetics/back/backpack/pikachu.pqc"),
    SportyBackpack("cosmetics/back/backpack/sportybackpack.pqc", PositionOperation.translate(0.0f, 2.2f, -0.05f), PositionOperation.scale(0.02f, 0.02f, 0.02f)),
    LeatherBackpack("cosmetics/back/backpack/leatherbackpack.pqc", PositionOperation.translate(0.0f, 2.2f, -0.05f), PositionOperation.scale(0.02f, 0.02f, 0.02f)),
    ScoutBackpack("cosmetics/back/backpack/scoutbackpack.pqc", PositionOperation.translate(0.0f, 2.2f, -0.05f), PositionOperation.scale(0.02f, 0.02f, 0.02f)),
    Cap("cosmetics/head/cap.pqc"),
    Pokepack("cosmetics/back/backpack/pokepack.pqc"),
    StrawHat("cosmetics/head/strawhat.pqc"),
    Crown("cosmetics/head/crown.pqc"),
    DetectiveHat("cosmetics/head/detectivehat.pqc"),
    CowboyHat("cosmetics/head/cowboyhat.pqc", PositionOperation.translate(0.0f, 1.49f, 0.0f)),
    LetsGoCap("cosmetics/head/letsgocap.pqc", PositionOperation.scale(0.038f, 0.04f, 0.045f), PositionOperation.translate(0.0f, -12.0f, 0.35f), PositionOperation.rotate(90.0f, -1.0f, 0.0f, 0.0f)),
    ThickGlasses("cosmetics/face/thickglasses.pqc", PositionOperation.scale(0.015f, 0.015f, 0.015f), PositionOperation.translate(0.0f, -12.0f, -15.3f)),
    Sunglasses("cosmetics/face/sunglasses.pqc", PositionOperation.scale(0.015f, 0.015f, 0.015f), PositionOperation.translate(0.0f, -12.0f, -15.3f)),
    Flowers("cosmetics/head/flower.pqc", PositionOperation.translate(0.2f, -0.45f, -0.27f), PositionOperation.scale(0.01f, 0.01f, 0.01f)),
    AviatorShades("cosmetics/face/aviatorshades.pqc", PositionOperation.scale(0.02f, 0.02f, 0.02f), PositionOperation.translate(0.0f, -8.25f, 16.3f)),
    EggHat("cosmetics/head/egghat.pqc");

    String fileName;
    public IModel theModel;
    public PositionOperation[] operations;
    private boolean initialised = false;
    public static HashMap<String, ResourceLocation> skins;
    public static HashMap<String, IModel> cachedModels;

    private EnumCustomModel(String fileName, PositionOperation ... operations) {
        this.fileName = fileName;
        this.operations = operations;
    }

    public IModel getModel() {
        if (!this.initialised) {
            try {
                ResourceLocation rl = new ResourceLocation("pixelmon:models/" + this.fileName);
                if (ValveStudioModelLoader.instance.accepts(rl)) {
                    this.theModel = ValveStudioModelLoader.instance.loadModel(rl);
                } else if (ObjLoader.accepts(rl)) {
                    this.theModel = ObjLoader.loadModel(rl);
                }
            }
            catch (Exception var2) {
                System.out.println("Could not load the model: " + this.fileName);
                var2.printStackTrace();
            }
            this.initialised = true;
        }
        if (!cachedModels.containsKey(this.name())) {
            cachedModels.put(this.name(), this.theModel instanceof ValveStudioModel ? new ValveStudioModel((ValveStudioModel)this.theModel) : this.theModel);
        }
        return cachedModels.get(this.name());
    }

    public PositionOperation[] getOperaitons() {
        return this.operations;
    }

    public static EnumCustomModel getFromString(String customModel) {
        for (EnumCustomModel enumCustomModel : EnumCustomModel.values()) {
            if (!enumCustomModel.name().equalsIgnoreCase(customModel)) continue;
            return enumCustomModel;
        }
        return null;
    }

    static {
        skins = new HashMap();
        cachedModels = new HashMap();
    }
}

