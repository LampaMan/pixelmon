/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

public enum EnumDynamaxItem {
    Disabled,
    None,
    DynamaxBand;


    public static EnumDynamaxItem getFromString(String shinyItemString) {
        for (EnumDynamaxItem item : EnumDynamaxItem.values()) {
            if (!item.toString().equalsIgnoreCase(shinyItemString)) continue;
            return item;
        }
        return Disabled;
    }

    public boolean canDynamax() {
        return this != Disabled && this != None;
    }
}

