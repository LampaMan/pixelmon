/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

public enum EnumBerryTree {
    Cheri(2, 5, 12, 15),
    Chesto(2, 5, 12, 15),
    Pecha(2, 5, 12, 15),
    Rawst(2, 5, 12, 15),
    Aspear(2, 5, 12, 15),
    Leppa(2, 5, 16, 15),
    Oran(2, 3, 12, 15),
    Persim(2, 5, 16, 15),
    Lum(2, 5, 48, 8),
    Sitrus(2, 5, 32, 7),
    Figy(1, 5, 20, 10),
    Wiki(1, 5, 20, 10),
    Mago(1, 5, 20, 10),
    Aguav(1, 5, 20, 10),
    Iapapa(1, 5, 20, 10),
    Razz(3, 6, 4, 35),
    Bluk(3, 6, 4, 35),
    Nanab(3, 6, 4, 35),
    Wepear(3, 6, 4, 35),
    Pinap(2, 10, 8, 35),
    Pomeg(1, 5, 32, 8),
    Kelpsy(1, 5, 32, 8),
    Qualot(1, 5, 32, 8),
    Hondew(1, 5, 32, 8),
    Grepa(1, 5, 32, 8),
    Tamato(1, 5, 32, 8),
    Cornn(2, 10, 24, 10),
    Magost(2, 10, 24, 10),
    Rabuta(2, 10, 24, 10),
    Nomel(2, 10, 24, 10),
    Spelon(2, 15, 60, 8),
    Pamtre(3, 15, 60, 8),
    Watmel(2, 15, 60, 8),
    Durin(3, 15, 60, 8),
    Belue(2, 15, 60, 8),
    Occa(1, 5, 72, 6),
    Passho(1, 5, 72, 6),
    Wacan(1, 5, 72, 6),
    Rindo(1, 5, 72, 6),
    Yache(1, 5, 72, 6),
    Chople(1, 5, 72, 6),
    Kebia(1, 5, 72, 6),
    Shuca(1, 5, 72, 6),
    Coba(1, 5, 72, 6),
    Payapa(1, 5, 72, 6),
    Tanga(1, 5, 72, 6),
    Charti(1, 5, 72, 6),
    Kasib(1, 5, 72, 6),
    Haban(1, 5, 72, 6),
    Colbur(1, 5, 72, 6),
    Babiri(1, 5, 72, 6),
    Chilan(1, 5, 72, 6),
    Liechi(1, 5, 96, 4),
    Ganlon(1, 5, 96, 4),
    Salac(1, 5, 96, 4),
    Petaya(1, 2, 96, 4),
    Apicot(1, 5, 96, 4),
    Lansat(1, 5, 96, 4),
    Starf(1, 2, 96, 4),
    Enigma(1, 5, 96, 7),
    Micle(1, 5, 96, 7),
    Custap(1, 5, 96, 7),
    Jaboca(1, 5, 96, 7),
    Rowap(1, 5, 96, 7);

    byte minYield;
    byte maxYield;
    byte growthTime;
    byte moistureDrainRate;

    private EnumBerryTree(int minYield, int maxYield, int growthTime, int moistureDrainRate) {
        this.minYield = (byte)minYield;
        this.maxYield = (byte)maxYield;
        this.growthTime = (byte)growthTime;
        this.moistureDrainRate = (byte)moistureDrainRate;
    }

    public byte getMinYield() {
        return this.minYield;
    }

    public byte getMaxYield() {
        return this.maxYield;
    }

    public byte getGrowthTime() {
        return this.growthTime;
    }

    public byte getMoistureDrainRate() {
        return this.moistureDrainRate;
    }
}

