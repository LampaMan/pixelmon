/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.items;

public enum EnumRodType {
    OldRod("oldrod", 170, 32),
    GoodRod("goodrod", 60, 64),
    SuperRod("superrod", 0, 128);

    public String textureName;
    public int rarityThreshold;
    public int maxDamage;

    private EnumRodType(String textureName, int rarityThreshold, int maxDamage) {
        this.textureName = textureName;
        this.rarityThreshold = rarityThreshold;
        this.maxDamage = maxDamage;
    }

    public static boolean hasRodType(String name) {
        try {
            return EnumRodType.valueOf(name) != null;
        }
        catch (Exception e) {
            return false;
        }
    }
}

