/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.items;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.spawning.spawners.EnumWorldState;
import com.pixelmongenerations.common.spawning.spawners.SpawnerBase;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.function.Predicate;
import net.minecraft.world.World;

public enum EnumFlutes {
    Moon("moon", EnumSpecies.Lunala, state -> state.equals(EnumWorldState.dusk) || state.equals(EnumWorldState.night)),
    Sun("sun", EnumSpecies.Solgaleo, state -> state.equals(EnumWorldState.dawn) || state.equals(EnumWorldState.day));

    private final String name;
    private final EnumSpecies pokemon;
    private final Predicate<EnumWorldState> time;

    private EnumFlutes(String name, EnumSpecies pokemon, Predicate<EnumWorldState> time) {
        this.name = name + "_flute";
        this.pokemon = pokemon;
        this.time = time;
    }

    public String getName() {
        return this.name;
    }

    public boolean isPokemonUsable(EntityPixelmon pixelmon) {
        return pixelmon.getSpecies().equals((Object)this.pokemon) || pixelmon.getSpecies().equals((Object)EnumSpecies.Necrozma);
    }

    public boolean isCorrectTime(World world) {
        EnumWorldState worldState = SpawnerBase.getWorldState(world);
        return this.time.test(worldState);
    }
}

