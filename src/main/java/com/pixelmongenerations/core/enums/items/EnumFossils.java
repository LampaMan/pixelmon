/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.items;

import com.pixelmongenerations.client.models.ModelHolder;
import com.pixelmongenerations.client.render.tileEntities.SharedModels;
import com.pixelmongenerations.core.enums.EnumSpecies;
import net.minecraft.util.ResourceLocation;

public enum EnumFossils {
    HELIX(0, 1, EnumSpecies.Omanyte, "helix_fossil", SharedModels.helixFossil),
    DOME(1, 1, EnumSpecies.Kabuto, "dome_fossil", SharedModels.domeFossil),
    OLD_AMBER(2, 1, EnumSpecies.Aerodactyl, "old_amber", SharedModels.oldAmber),
    ROOT(3, 3, EnumSpecies.Lileep, "root_fossil", SharedModels.rootFossil),
    CLAW(4, 3, EnumSpecies.Anorith, "claw_fossil", SharedModels.clawFossil),
    SKULL(5, 4, EnumSpecies.Cranidos, "skull_fossil", SharedModels.skullFossil),
    ARMOR(6, 4, EnumSpecies.Shieldon, "armor_fossil", SharedModels.armorFossil),
    COVER(7, 5, EnumSpecies.Tirtouga, "cover_fossil", SharedModels.coverFossil),
    PLUME(8, 5, EnumSpecies.Archen, "plume_fossil", SharedModels.plumeFossil),
    JAW(9, 6, EnumSpecies.Tyrunt, "jaw_fossil", SharedModels.jawFossil),
    SAIL(10, 6, EnumSpecies.Amaura, "sail_fossil", SharedModels.sailFossil),
    BIRD(11, 8, EnumSpecies.Arctozolt, "bird_fossil", SharedModels.birdFossil, false),
    FISH(12, 8, EnumSpecies.Arctovish, "fish_fossil", SharedModels.fishFossil, false),
    DINO(13, 8, EnumSpecies.Dracovish, "dino_fossil", SharedModels.dinoFossil, false),
    DRAKE(14, 8, EnumSpecies.Dracozolt, "drake_fossil", SharedModels.drakeFossil, false),
    ARCTOZOLT(15, 8, EnumSpecies.Arctozolt, "arctozolt_fossil", SharedModels.arctozoltFossil),
    ARCTOVISH(16, 8, EnumSpecies.Arctovish, "arctovish_fossil", SharedModels.arctovishFossil),
    DRACOVISH(17, 8, EnumSpecies.Dracovish, "dracovish_fossil", SharedModels.dracovishFossil),
    DRACOZOLT(18, 8, EnumSpecies.Dracozolt, "dracozolt_fossil", SharedModels.dracozoltFossil);

    private int index;
    private int generation;
    private EnumSpecies pokemon;
    private String itemName;
    private ResourceLocation texture;
    private ModelHolder model;
    private final boolean machineUsable;

    private EnumFossils(int index, int generation, EnumSpecies pokemon, String itemName, ModelHolder model) {
        this(index, generation, pokemon, itemName, model, true);
    }

    private EnumFossils(int index, int generation, EnumSpecies pokemon, String itemName, ModelHolder model, boolean machineUsable) {
        this.index = index;
        this.generation = generation;
        this.pokemon = pokemon;
        this.itemName = itemName;
        this.texture = new ResourceLocation("pixelmon", "textures/fossils/" + this.name().toLowerCase() + "_fossilmodel.png");
        this.model = model;
        this.machineUsable = machineUsable;
    }

    public int getIndex() {
        return this.index;
    }

    public int getGeneration() {
        return this.generation;
    }

    public EnumSpecies getPokemon() {
        return this.pokemon;
    }

    public String getItemName() {
        return this.itemName;
    }

    public ResourceLocation getTexture() {
        return this.texture;
    }

    public ModelHolder getModel() {
        return this.model;
    }

    public boolean isMachineUsable() {
        return this.machineUsable;
    }
}

