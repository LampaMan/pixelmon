/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.items;

import com.pixelmongenerations.client.models.pokeballs.ModelPokeballs;
import com.pixelmongenerations.common.item.IEnumItem;
import com.pixelmongenerations.common.item.ItemPokeball;
import com.pixelmongenerations.common.item.ItemPokeballDisc;
import com.pixelmongenerations.common.item.ItemPokeballLid;
import com.pixelmongenerations.core.config.PixelmonItemsPokeballs;
import com.pixelmongenerations.core.enums.EnumCustomModel;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;

public enum EnumPokeball implements IEnumItem
{
    PokeBall(0, 1.0, "poke_ball", 5, 15),
    GreatBall(1, 1.5, "great_ball", 2, 35, EnumCustomModel.Greatball),
    UltraBall(2, 2.0, "ultra_ball", 1, 55),
    MasterBall(3, 255.0, "master_ball", 0, 0, EnumCustomModel.Masterball),
    LevelBall(4, 1.0, "level_ball", 2, 35),
    MoonBall(5, 1.0, "moon_ball", 2, 35),
    FriendBall(6, 1.0, "friend_ball", 3, 35),
    LoveBall(7, 1.0, "love_ball", 3, 35),
    SafariBall(8, -1.0, "safari_ball", 3, 35),
    HeavyBall(9, 1.0, "heavy_ball", 3, 35, EnumCustomModel.Heavyball),
    FastBall(10, 1.0, "fast_ball", 3, 35),
    RepeatBall(11, 1.0, "repeat_ball", 3, 35),
    TimerBall(12, 1.0, "timer_ball", 3, 35, EnumCustomModel.Timerball),
    NestBall(13, 1.0, "nest_ball", 3, 35),
    NetBall(14, 1.0, "net_ball", 3, 35, EnumCustomModel.Netball),
    DiveBall(15, 1.0, "dive_ball", 3, 35),
    LuxuryBall(16, 1.0, "luxury_ball", 3, 35),
    HealBall(17, 1.0, "heal_ball", 3, 35),
    DuskBall(18, 1.0, "dusk_ball", 3, 35),
    PremierBall(19, 1.0, "premier_ball", 3, 25),
    SportBall(20, 1.0, "sport_ball", 3, 25),
    QuickBall(21, 1.0, "quick_ball", 3, 35),
    ParkBall(22, 255.0, "park_ball", 0, 0),
    LureBall(23, 1.0, "lure_ball", 3, 35),
    CherishBall(24, 1.0, "cherish_ball", 0, 0, EnumCustomModel.Cherishball),
    GSBall(25, 1.0, "gs_ball", 0, 0),
    BeastBall(26, 1.0, "beast_ball", 0, 0, EnumCustomModel.Beastball),
    DreamBall(27, 1.0, "dream_ball", 0, 0);

    private double ballBonus;
    private int index;
    private String directory;
    private String filenamePrefix;
    public int quantityMade;
    public int breakChance;
    private EnumCustomModel model;
    private String name;

    private EnumPokeball(int index, double ballBonus, String filenamePrefix, int quantityMade, int chanceBreak) {
        this(index, ballBonus, filenamePrefix, quantityMade, chanceBreak, null);
    }

    private EnumPokeball(int index, double ballBonus, String filenamePrefix, int quantityMade, int chanceBreak, EnumCustomModel model) {
        this.ballBonus = ballBonus;
        this.index = index;
        this.filenamePrefix = filenamePrefix;
        this.quantityMade = quantityMade;
        this.breakChance = chanceBreak;
        this.model = model;
    }

    public double getBallBonus() {
        return this.ballBonus;
    }

    public int getIndex() {
        return this.index;
    }

    public int getBreakChance() {
        return this.breakChance;
    }

    public String getTexture() {
        return this.filenamePrefix.replace("_", "") + ".png";
    }

    public String getFlashRedTexture() {
        return this.filenamePrefix.replace("_", "") + "flashing.png";
    }

    public String getFilenamePrefix() {
        return this.filenamePrefix;
    }

    public static EnumPokeball getFromIndex(int index) {
        return index >= 0 && index < EnumPokeball.values().length ? EnumPokeball.values()[index] : PokeBall;
    }

    public ItemPokeball getItem() {
        return PixelmonItemsPokeballs.getItemFromEnum(this);
    }

    public ItemPokeballLid getLid() {
        return PixelmonItemsPokeballs.getLidFromEnum(this);
    }

    public ItemPokeballDisc getDisc() {
        return PixelmonItemsPokeballs.getDiscFromEnum(this);
    }

    @Override
    public Item getItem(int type) {
        switch (type) {
            case 0: {
                return this.getItem();
            }
            case 1: {
                return this.getLid();
            }
            case 2: {
                return this.getDisc();
            }
        }
        return null;
    }

    @Override
    public int numTypes() {
        return 3;
    }

    public static boolean hasPokeball(String pokeball) {
        try {
            return EnumPokeball.valueOf(pokeball) != null;
        }
        catch (Exception var2) {
            return false;
        }
    }

    public String getTextureDirectory() {
        String path = "pixelmon:textures/pokeballs/";
        return this.directory == null ? path : this.directory;
    }

    public ResourceLocation getTextureLocation() {
        return new ResourceLocation(this.getTextureDirectory() + this.getTexture());
    }

    public void setTextureDirectory(String newDirectory) {
        try {
            this.directory = newDirectory.charAt(newDirectory.length() - 1) == '/' ? newDirectory : newDirectory + '/';
        }
        catch (NullPointerException var3) {
            this.directory = null;
        }
    }

    public ModelPokeballs getModel() {
        EnumCustomModel model = this.model != null ? this.model : EnumCustomModel.Pokeball;
        return new ModelPokeballs(model);
    }

    public static EnumPokeball getPokeballFromString(String name) {
        for (EnumPokeball ball : EnumPokeball.values()) {
            if (!ball.filenamePrefix.equalsIgnoreCase(name)) continue;
            return ball;
        }
        return null;
    }

    public static EnumPokeball getPokeballFromStartingString(String name) {
        for (EnumPokeball ball : EnumPokeball.values()) {
            if (ball.name == null) {
                ball.name = ball.name().toLowerCase().replaceAll("ball", "");
            }
            if (!ball.name.startsWith(name)) continue;
            return ball;
        }
        return null;
    }

    public String getLocalizedName() {
        return I18n.translateToLocal("item." + this.filenamePrefix + ".name");
    }
}

