/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.items;

import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public enum EnumJuice {
    Purple(StatsType.HP),
    Red(StatsType.Attack),
    Yellow(StatsType.Defence),
    Blue(StatsType.SpecialAttack),
    Green(StatsType.SpecialDefence),
    Pink(StatsType.Speed);

    private StatsType statType;

    private EnumJuice(StatsType statType) {
        this.statType = statType;
    }

    public StatsType getStatType() {
        return this.statType;
    }

    public String getId() {
        return this.name().toLowerCase() + "_juice";
    }
}

