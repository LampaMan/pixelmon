/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.items;

import net.minecraft.util.text.translation.I18n;

public enum EnumEXPAmount {
    XS(100),
    S(800),
    M(3000),
    L(10000),
    XL(30000);

    public int expAmount;

    private EnumEXPAmount(int expAmount) {
        this.expAmount = expAmount;
    }

    public String getLocalizedName() {
        return I18n.translateToLocal("enum.expcandy." + this.toString().toLowerCase());
    }
}

