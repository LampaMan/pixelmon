/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.Optional;

public enum EnumEggGroup {
    Monster(0),
    Bug(1),
    Flying(2),
    Field(3),
    Fairy(4),
    Grass(5),
    Humanlike(6),
    Mineral(7),
    Amorphous(8),
    Ditto(9),
    Dragon(10),
    Water1(11),
    Water2(12),
    Water3(13),
    Undiscovered(14);

    public int index;

    private EnumEggGroup(int index) {
        this.index = index;
    }

    public static EnumEggGroup getEggGroupFromIndex(int index) {
        for (EnumEggGroup n : EnumEggGroup.values()) {
            if (n.index != index) continue;
            return n;
        }
        return null;
    }

    public static Integer getIndexFromEggGroup(EnumEggGroup group) {
        return group.index;
    }

    public static Integer getIndexFromEggGroupName(String groupName) {
        return EnumEggGroup.getEggGroupFromString((String)groupName).index;
    }

    public Integer getIndex() {
        return this.index;
    }

    public static EnumEggGroup getRandomEggGroup() {
        int rndm = RandomHelper.getRandomNumberBetween(0, 14);
        return EnumEggGroup.getEggGroupFromIndex(rndm);
    }

    public static boolean hasEggGroup(String group) {
        try {
            for (EnumEggGroup n : EnumEggGroup.values()) {
                if (n.index != EnumEggGroup.valueOf((String)group).index) continue;
                return true;
            }
            return false;
        }
        catch (Exception IllegalArgumentException2) {
            return false;
        }
    }

    public static EnumEggGroup getEggGroupFromString(String groupName) {
        for (EnumEggGroup n : EnumEggGroup.values()) {
            if (n.index != EnumEggGroup.valueOf((String)groupName).index) continue;
            return n;
        }
        return null;
    }

    public String getName() {
        return this.name();
    }

    public static EnumEggGroup[] getEggGroups(EnumSpecies pokemon) {
        EnumEggGroup[] arrenumEggGroup;
        Optional<BaseStats> optional = Entity3HasStats.getBaseStats(pokemon);
        if (optional.isPresent()) {
            arrenumEggGroup = optional.get().eggGroups;
        } else {
            EnumEggGroup[] arrenumEggGroup2 = new EnumEggGroup[1];
            arrenumEggGroup = arrenumEggGroup2;
            arrenumEggGroup2[0] = Undiscovered;
        }
        return arrenumEggGroup;
    }

    public static EnumEggGroup getRandomEggGroup(EnumSpecies pokemon) {
        EnumEggGroup[] groups = EnumEggGroup.getEggGroups(pokemon);
        return RandomHelper.getRandomElementFromArray(groups);
    }
}

