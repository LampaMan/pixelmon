/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import java.util.function.Function;
import java.util.function.Predicate;

public enum EnumCatchCombo {
    TierZero(streak -> streak <= 10, charm -> Float.valueOf(charm == false ? PixelmonConfig.shinyRate : PixelmonConfig.shinyCharmRate)),
    TierOne(streak -> streak >= 11 && streak <= 20, charm -> Float.valueOf(charm == false ? PixelmonConfig.catchComboTier1 : PixelmonConfig.catchComboTier1Charm)),
    TierTwo(streak -> streak >= 21 && streak <= 30, charm -> Float.valueOf(charm == false ? PixelmonConfig.catchComboTier2 : PixelmonConfig.catchComboTier2Charm)),
    TierThree(streak -> streak >= 31, charm -> Float.valueOf(charm == false ? PixelmonConfig.catchComboTier3 : PixelmonConfig.catchComboTier3Charm));

    public Predicate<Integer> comboCondition;
    public Function<Boolean, Float> comboFunction;

    private EnumCatchCombo(Predicate<Integer> comboCondition, Function<Boolean, Float> comboFunction) {
        this.comboCondition = comboCondition;
        this.comboFunction = comboFunction;
    }

    public static EnumCatchCombo getCombo(int streak) {
        for (EnumCatchCombo combo : EnumCatchCombo.values()) {
            if (!combo.comboCondition.test(streak)) continue;
            return combo;
        }
        Pixelmon.LOGGER.info("Shouldn't be making it here in EnumCatchCombo");
        return TierZero;
    }

    public float getShinyRate(boolean hasCharm) {
        return this.comboFunction.apply(hasCharm).floatValue();
    }
}

