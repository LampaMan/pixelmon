/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

public enum ZygardeMachineAction {
    createTen,
    createFifty,
    createOneHundred,
    combineTen,
    combineTenWith90,
    combineFiftyWith50;


    public static ZygardeMachineAction getZygardeMachineActionFromIndex(int index) {
        try {
            return ZygardeMachineAction.values()[index];
        }
        catch (Exception npe) {
            return null;
        }
    }
}

