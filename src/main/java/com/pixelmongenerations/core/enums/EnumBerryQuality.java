/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import net.minecraft.util.text.TextFormatting;

public enum EnumBerryQuality {
    Poor(TextFormatting.DARK_RED),
    Normal(TextFormatting.RED),
    Good(TextFormatting.WHITE),
    Better(TextFormatting.DARK_GREEN),
    Best(TextFormatting.GREEN);

    private final TextFormatting formatting;

    private EnumBerryQuality(TextFormatting formatting) {
        this.formatting = formatting;
    }

    public static EnumBerryQuality getFromIndex(int index) {
        try {
            return EnumBerryQuality.values()[index];
        }
        catch (Exception npe) {
            return Poor;
        }
    }

    public int getIndex() {
        return this.ordinal();
    }

    public String getName() {
        return (Object)((Object)this.formatting) + this.name();
    }
}

