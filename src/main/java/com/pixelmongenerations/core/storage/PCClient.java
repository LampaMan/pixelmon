/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.pcServer.SwapPokemon;
import com.pixelmongenerations.core.network.packetHandlers.pcServer.TrashPokemon;
import com.pixelmongenerations.core.storage.PCClientStorage;
import com.pixelmongenerations.core.storage.PCPos;

public class PCClient {
    public void deletePokemon(int box, int pos) {
        this.storePokemonAtPos(null, box, pos);
    }

    public void deletePokemon() {
        PixelmonData p = this.getSelected();
        PCPos pos = this.getPos(p);
        this.deletePokemon(pos.box, pos.pos);
        Pixelmon.NETWORK.sendToServer(new TrashPokemon(pos.box, pos.pos));
    }

    public void swapPokemon(int firstBox, int firstPos, int secondBox, int secondPos) {
        PixelmonData pkt1 = this.getPokemonAtPos(firstBox, firstPos);
        PixelmonData pkt2 = this.getPokemonAtPos(secondBox, secondPos);
        this.storePokemonAtPos(pkt1, secondBox, secondPos);
        this.storePokemonAtPos(pkt2, firstBox, firstPos);
        if (ServerStorageDisplay.countNonEgg() >= 1) {
            Pixelmon.NETWORK.sendToServer(new SwapPokemon(firstBox, firstPos, secondBox, secondPos));
        } else {
            this.storePokemonAtPos(pkt1, firstBox, firstPos);
            this.storePokemonAtPos(pkt2, secondBox, secondPos);
        }
    }

    public PixelmonData getPokemonAtPos(int box, int pos) {
        if (box == -1) {
            if (pos < 6) {
                return ServerStorageDisplay.getPokemon()[pos];
            }
            return null;
        }
        return PCClientStorage.getFromBox(box, pos);
    }

    public void storePokemonAtPos(PixelmonData pkt, int box, int pos) {
        if (pkt != null) {
            pkt.order = pos;
            if (box != -1) {
                pkt.boxNumber = box;
            }
        }
        if (box == -1) {
            ServerStorageDisplay.changePokemon(pos, pkt);
        } else {
            PCClientStorage.changePokemon(box, pos, pkt);
        }
    }

    public boolean hasOneInParty() {
        return ServerStorageDisplay.countNonEgg() == 1;
    }

    public int numSelected() {
        int num = 0;
        PixelmonData[] party = ServerStorageDisplay.getPokemon();
        for (int i = 0; i < 6; ++i) {
            PixelmonData current = party[i];
            if (current == null || !current.selected) continue;
            ++num;
        }
        return num += PCClientStorage.numSelected();
    }

    public void swapPokemonWithSelected(PixelmonData p) {
        PixelmonData p2 = this.getSelected();
        PCPos pos1 = this.getPos(p);
        PCPos pos2 = this.getPos(p2);
        if (pos1 == null || pos2 == null) {
            return;
        }
        this.swapPokemon(pos1.box, pos1.pos, pos2.box, pos2.pos);
        if (p2 != null) {
            p2.selected = false;
        }
    }

    public PCPos getPos(PixelmonData p) {
        PixelmonData[] party = ServerStorageDisplay.getPokemon();
        for (int i = 0; i < 6; ++i) {
            if (party[i] != p) continue;
            return new PCPos(-1, i);
        }
        return PCClientStorage.getPos(p);
    }

    public PixelmonData getSelected() {
        PixelmonData[] party = ServerStorageDisplay.getPokemon();
        for (int i = 0; i < 6; ++i) {
            PixelmonData current = party[i];
            if (current == null || !current.selected) continue;
            return current;
        }
        return PCClientStorage.getSelected();
    }

    public void swapPositionWithSelected(PCPos pos1) {
        PixelmonData p2 = this.getSelected();
        PCPos pos2 = this.getPos(p2);
        this.swapPokemon(pos1.box, pos1.pos, pos2.box, pos2.pos);
        if (p2 != null) {
            p2.selected = false;
        }
    }

    public void unselectAll() {
        PixelmonData[] party = ServerStorageDisplay.getPokemon();
        for (int i = 0; i < 6; ++i) {
            PixelmonData current = party[i];
            if (current == null) continue;
            current.selected = false;
        }
    }

    public PixelmonData getByID(int[] id) {
        return PCClientStorage.getByID(id);
    }
}

