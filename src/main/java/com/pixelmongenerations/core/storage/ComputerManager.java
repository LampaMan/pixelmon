/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage;

import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.storage.AsyncStorageWrapper;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ComputerManager {
    private ArrayList<PlayerComputerStorage> playerComputerList = new ArrayList();
    private int lastSaveTick = 0;
    public static Map<UUID, Map<Integer, String>> boxNamesMain;
    public static Map<Integer, String> boxNames;

    public PlayerComputerStorage getPlayerStorage(EntityPlayerMP owner) {
        for (PlayerComputerStorage p : this.playerComputerList) {
            if (!p.getPlayerID().equals(owner.getUniqueID())) continue;
            return p;
        }
        this.loadPlayer(owner);
        return this.getPlayerStorage(owner);
    }

    public EntityPlayerMP getPlayerFromUUID(UUID uuid) {
        for (PlayerComputerStorage p : this.playerComputerList) {
            EntityPlayerMP player = p.getPlayer();
            if (player == null || !player.getUniqueID().equals(uuid)) continue;
            return player;
        }
        return null;
    }

    public PlayerComputerStorage getPlayerStorageFromUUID(World world, UUID ownerUUID) {
        EntityPlayerMP player = this.getPlayerFromUUID(ownerUUID);
        if (player == null) {
            player = (EntityPlayerMP)world.getPlayerEntityByUUID(ownerUUID);
        }
        if (player != null) {
            return this.getPlayerStorage(player);
        }
        return this.getPlayerStorageOffline(world.getMinecraftServer(), ownerUUID);
    }

    public PlayerComputerStorage getPlayerStorageOffline(MinecraftServer server, UUID ownerUUID) {
        NBTTagCompound compound = PixelmonStorage.storageAdapter.readComputerData(ownerUUID);
        if (compound != null) {
            PlayerComputerStorage p = new PlayerComputerStorage(server, ownerUUID);
            p.readFromNBT(compound);
            return p;
        }
        return null;
    }

    public void refreshComputerStorage(EntityPlayerMP player) {
        PlayerComputerStorage store = null;
        Iterator<PlayerComputerStorage> i = this.playerComputerList.iterator();
        while (i.hasNext()) {
            PlayerComputerStorage next = i.next();
            if (!next.getPlayerID().equals(player.getUniqueID())) continue;
            store = next;
            i.remove();
            break;
        }
        if (store == null && PixelmonStorage.storageAdapter instanceof AsyncStorageWrapper) {
            store = ((AsyncStorageWrapper)PixelmonStorage.storageAdapter).getQueuedComputerStorage(player.getUniqueID());
        }
        if (store != null) {
            NBTTagCompound compound = new NBTTagCompound();
            store.writeToNBT(compound);
            store = new PlayerComputerStorage(player);
            store.readFromNBT(compound);
            this.playerComputerList.add(store);
        }
    }

    private void loadPlayer(EntityPlayerMP player) {
        PlayerComputerStorage p = new PlayerComputerStorage(player);
        NBTTagCompound compound = PixelmonStorage.storageAdapter.readComputerData(player.getUniqueID());
        if (compound != null) {
            p.readFromNBT(compound);
        }
        this.playerComputerList.add(p);
    }

    public void saveAll() {
        for (int i = 0; i < this.playerComputerList.size(); ++i) {
            this.savePlayer(this.playerComputerList.get(i));
            if (!this.playerComputerList.get(i).isOffline()) continue;
            this.playerComputerList.remove(i);
            --i;
        }
    }

    public void savePlayer(PlayerComputerStorage storage) {
        PixelmonStorage.storageAdapter.saveComputerStorage(storage);
    }

    @SubscribeEvent
    public void onWorldSave(WorldEvent.Save event) {
        int currentTick = event.getWorld().getMinecraftServer().getTickCounter();
        if (PixelmonConfig.dataSaveOnWorldSave && this.lastSaveTick != currentTick) {
            this.lastSaveTick = currentTick;
            this.saveAll();
        }
    }
}

