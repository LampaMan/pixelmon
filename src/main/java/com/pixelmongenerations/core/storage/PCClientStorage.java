/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage;

import com.pixelmongenerations.api.pc.ClientBackground;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.storage.PCPos;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import com.pixelmongenerations.core.util.PixelmonMethods;
import java.util.ArrayList;
import java.util.List;

public class PCClientStorage {
    private static int lastBoxOpen = 0;
    private static PixelmonData[][] store = new PixelmonData[PlayerComputerStorage.boxCount][30];
    private static String[] names = new String[PlayerComputerStorage.boxCount];
    private static String[] backgrounds = new String[PlayerComputerStorage.boxCount];
    private static List<ClientBackground> backgroundList = new ArrayList<ClientBackground>();

    public static void refreshStore() {
        store = new PixelmonData[PlayerComputerStorage.boxCount][30];
        names = new String[PlayerComputerStorage.boxCount];
        backgrounds = new String[PlayerComputerStorage.boxCount];
    }

    public static void addToList(PixelmonData p) {
        PCClientStorage.store[p.boxNumber][p.order] = p;
    }

    public static void clearList() {
        for (int i = 0; i < PlayerComputerStorage.boxCount; ++i) {
            for (int j = 0; j < 30; ++j) {
                PCClientStorage.store[i][j] = null;
            }
        }
    }

    public static PixelmonData getFromBox(int i, int j) {
        return store[i][j];
    }

    public static PixelmonData getPixelmonDataFromID(int[] id) {
        for (int i = 0; i < PlayerComputerStorage.boxCount; ++i) {
            for (int j = 0; j < 30; ++j) {
                if (store[i][j] == null || !PixelmonMethods.isIDSame(PCClientStorage.store[i][j].pokemonID, id)) continue;
                return store[i][j];
            }
        }
        return null;
    }

    public static void removeFromList(int box, int pos) {
        PCClientStorage.store[box][pos] = null;
    }

    public static void changePokemon(int box, int pos, PixelmonData pkt) {
        PCClientStorage.store[box][pos] = pkt;
    }

    public static int numSelected() {
        int num = 0;
        for (int i = 0; i < PlayerComputerStorage.boxCount; ++i) {
            for (int j = 0; j < 30; ++j) {
                if (store[i][j] == null || !PCClientStorage.store[i][j].selected) continue;
                ++num;
            }
        }
        return num;
    }

    public static PixelmonData getSelected() {
        for (int i = 0; i < PlayerComputerStorage.boxCount; ++i) {
            for (int j = 0; j < 30; ++j) {
                if (store[i][j] == null || !PCClientStorage.store[i][j].selected) continue;
                return store[i][j];
            }
        }
        return null;
    }

    public static PCPos getPos(PixelmonData p) {
        for (int i = 0; i < PlayerComputerStorage.boxCount; ++i) {
            for (int j = 0; j < 30; ++j) {
                if (store[i][j] != p) continue;
                return new PCPos(i, j);
            }
        }
        return null;
    }

    public static PixelmonData getByID(int[] id) {
        for (int i = 0; i < PlayerComputerStorage.boxCount; ++i) {
            for (int j = 0; j < 30; ++j) {
                if (store[i][j] == null || !PixelmonMethods.isIDSame(PCClientStorage.store[i][j].pokemonID, id)) continue;
                return store[i][j];
            }
        }
        return null;
    }

    public static void setLastBoxOpen(int lastBoxOpen) {
        PCClientStorage.lastBoxOpen = lastBoxOpen;
    }

    public static int getLastBoxOpen() {
        return lastBoxOpen;
    }

    public static String getBoxName(int box) {
        return names[box];
    }

    public static void setBoxName(int box, String name) {
        PCClientStorage.names[box] = name;
    }

    public static String getBoxBackground(int box) {
        return backgrounds[box];
    }

    public static void setBoxBackground(int box, String background) {
        PCClientStorage.backgrounds[box] = background;
    }

    public static void setBackgroundList(List<ClientBackground> backgrounds) {
        backgroundList = backgrounds;
    }

    public static ClientBackground getBackgroundById(String id) {
        for (ClientBackground background : backgroundList) {
            if (!background.getId().equals(id)) continue;
            return background;
        }
        return null;
    }

    public static ClientBackground getBackgroundFromIndex(int index) {
        if (index >= backgroundList.size() || index < 0) {
            return backgroundList.get(0);
        }
        for (int i = 0; i < backgroundList.size(); ++i) {
            ClientBackground background = backgroundList.get(i);
            if (i != index) continue;
            return background;
        }
        return backgroundList.get(0);
    }

    public static int getIndexFromBackgroundId(String id) {
        for (int i = 0; i < backgroundList.size(); ++i) {
            ClientBackground background = backgroundList.get(i);
            if (!background.getId().equals(id)) continue;
            return i;
        }
        return 0;
    }

    public static int getBackgroundCount() {
        return backgroundList.size();
    }
}

