/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage;

import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.storage.AsyncStorageWrapper;
import com.pixelmongenerations.core.storage.ComputerManager;
import com.pixelmongenerations.core.storage.FileStorage;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.storage.PokeballManager;
import com.pixelmongenerations.core.storage.StorageAdapter;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;

public class PixelmonStorage {
    public static PokeballManager pokeBallManager = new PokeballManager();
    public static ComputerManager computerManager = new ComputerManager();
    public static StorageAdapter storageAdapter = new FileStorage();

    public static void saveAll(World world) {
        pokeBallManager.saveAll(world);
        computerManager.saveAll();
    }

    public static void save(EntityPlayerMP player) {
        Optional<PlayerStorage> optstorage = pokeBallManager.getPlayerStorage(player);
        if (optstorage.isPresent()) {
            pokeBallManager.savePlayer(player.getServer(), optstorage.get());
        }
        PlayerComputerStorage compStorage = computerManager.getPlayerStorage(player);
        computerManager.savePlayer(compStorage);
    }

    static {
        StorageAdapter storageAdapter = PixelmonStorage.storageAdapter;
        if (FMLCommonHandler.instance().getSide() == Side.SERVER && PixelmonConfig.useAsyncSaving) {
            PixelmonStorage.storageAdapter = new AsyncStorageWrapper(PixelmonStorage.storageAdapter);
        }
    }
}

