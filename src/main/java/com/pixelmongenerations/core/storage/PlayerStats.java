/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage;

import java.util.HashMap;
import net.minecraft.nbt.NBTTagCompound;

public class PlayerStats {
    private int wins;
    private int losses;
    private int totalExp;
    private int totalKills;
    private int currentExp;
    private int currentKills;
    private int totalBred;
    private int totalHatched;
    private int totalEvolved;

    public void writeToNBT(NBTTagCompound nbt) {
        nbt.setInteger("Wins", this.wins);
        nbt.setInteger("Losses", this.losses);
        nbt.setInteger("TotalExp", this.totalExp);
        nbt.setInteger("CurrentExp", this.currentExp);
        nbt.setInteger("TotalKills", this.totalKills);
        nbt.setInteger("CurrentKills", this.currentKills);
        nbt.setInteger("TotalBred", this.totalBred);
        nbt.setInteger("TotalHatched", this.totalHatched);
    }

    public void readFromNBT(NBTTagCompound nbt) {
        this.wins = nbt.getInteger("Wins");
        this.losses = nbt.getInteger("Losses");
        this.totalExp = nbt.getInteger("TotalExp");
        this.currentExp = nbt.getInteger("CurrentExp");
        this.totalKills = nbt.getInteger("TotalKills");
        this.currentKills = nbt.getInteger("CurrentKills");
        this.totalBred = nbt.getInteger("TotalBred");
        this.totalHatched = nbt.getInteger("TotalHatched");
    }

    public void addWin() {
        ++this.wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public void addLoss() {
        ++this.losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public void addKill() {
        ++this.totalKills;
        ++this.currentKills;
    }

    public void setCurrentKills(int currentKills) {
        this.currentKills = currentKills;
    }

    public int getCurrentKills() {
        return this.currentKills;
    }

    public int getTotalExp() {
        return this.totalExp;
    }

    public void setTotalExp(int totalExp) {
        this.totalExp = totalExp;
    }

    public int getTotalKills() {
        return this.totalKills;
    }

    public void setTotalKills(int totalKills) {
        this.totalKills = totalKills;
    }

    public int getCurrentExp() {
        return this.currentExp;
    }

    public void setCurrentExp(int currentExp) {
        this.currentExp = currentExp;
    }

    public int getWins() {
        return this.wins;
    }

    public int getLosses() {
        return this.losses;
    }

    public void addExp(int amount) {
        this.totalExp += amount;
        this.currentExp += amount;
    }

    public void setExp(int exp) {
        this.currentExp = exp;
    }

    public void resetWinLoss() {
        this.wins = 0;
        this.losses = 0;
    }

    public int getTotalBred() {
        return this.totalBred;
    }

    public void addToTotalBred() {
        ++this.totalBred;
    }

    public void setTotalBred(int totalBred) {
        this.totalBred = totalBred;
    }

    public int getTotalHatched() {
        return this.totalHatched;
    }

    public void addHatched() {
        ++this.totalHatched;
    }

    public void setTotalHatched(int totalHatched) {
        this.totalHatched = totalHatched;
    }

    public int getTotalEvolved() {
        return this.totalEvolved;
    }

    public void addEvolved() {
        ++this.totalEvolved;
    }

    public void setTotalEvolved(int totalHatched) {
        this.totalEvolved = totalHatched;
    }

    public static void getNBTTags(HashMap<String, Class> tags) {
        tags.put("Wins", Integer.class);
        tags.put("Losses", Integer.class);
        tags.put("TotalExp", Integer.class);
        tags.put("CurrentExp", Integer.class);
        tags.put("TotalKills", Integer.class);
        tags.put("CurrentKills", Integer.class);
        tags.put("TotalBred", Integer.class);
        tags.put("TotalHatched", Integer.class);
    }
}

