/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage;

import com.pixelmongenerations.api.enums.DeleteType;
import com.pixelmongenerations.api.events.PixelmonDeleteEvent;
import com.pixelmongenerations.api.pc.BackgroundRegistry;
import com.pixelmongenerations.api.pc.ClientBackground;
import com.pixelmongenerations.api.pc.ClientBoxInfo;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.pcClientStorage.PCBoxBackground;
import com.pixelmongenerations.core.network.packetHandlers.pcClientStorage.PCBoxName;
import com.pixelmongenerations.core.network.packetHandlers.pcClientStorage.PCInitBackgrounds;
import com.pixelmongenerations.core.network.packetHandlers.pcClientStorage.PCSync;
import com.pixelmongenerations.core.storage.ComputerBox;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.MinecraftForge;

public class PCServer {
    public static void deletePokemon(EntityPlayerMP player, int box, int pos) {
        Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
        if (optstorage.isPresent()) {
            PlayerStorage storage = optstorage.get();
            storage.recallAllPokemon();
        }
        MinecraftForge.EVENT_BUS.post(new PixelmonDeleteEvent(player, PCServer.getPokemonAtPos(player, box, pos), DeleteType.PC));
        PCServer.storePokemonAtPos(player, null, box, pos);
    }

    public static void swapPokemon(EntityPlayerMP player, int firstBox, int firstPos, int secondBox, int secondPos) {
        NBTTagCompound n1 = PCServer.getPokemonAtPos(player, firstBox, firstPos);
        NBTTagCompound n2 = PCServer.getPokemonAtPos(player, secondBox, secondPos);
        PCServer.unloadEntity(player, firstBox, firstPos);
        PCServer.unloadEntity(player, secondBox, secondPos);
        PCServer.storePokemonAtPos(player, n1, secondBox, secondPos);
        PCServer.storePokemonAtPos(player, n2, firstBox, firstPos);
    }

    private static void storePokemonAtPos(EntityPlayerMP player, NBTTagCompound nbt, int box, int pos) {
        if (PixelmonConfig.pcsHealPokemon) {
            try {
                nbt.setFloat("Health", nbt.getInteger("StatsHP"));
                nbt.setBoolean("IsFainted", false);
                int amount = nbt.getInteger("PixelmonNumberMoves");
                for (int i = 0; i < amount; ++i) {
                    nbt.setInteger("PixelmonMovePP" + i, nbt.getInteger("PixelmonMovePPBase" + i));
                }
            }
            catch (NullPointerException amount) {
                // empty catch block
            }
        }
        if (box == -1) {
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
            if (optstorage.isPresent()) {
                PlayerStorage storage = optstorage.get();
                storage.changePokemon(pos, nbt);
            }
        } else {
            PlayerComputerStorage storage = PixelmonStorage.computerManager.getPlayerStorage(player);
            storage.getBox(box).changePokemon(pos, nbt);
            storage.lastBoxOpen = box;
        }
    }

    private static void unloadEntity(EntityPlayerMP player, int box, int pos) {
        Optional<EntityPixelmon> p;
        PlayerStorage storage;
        int[] id;
        Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
        if (optstorage.isPresent() && (id = (storage = optstorage.get()).getIDFromPosition(pos))[0] != -1 && id[1] != -1 && (p = storage.getAlreadyExists(id, player.world)).isPresent()) {
            p.get().unloadEntity();
        }
    }

    public static NBTTagCompound getPokemonAtPos(EntityPlayerMP player, int box, int pos) {
        if (box == -1) {
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
            if (optstorage.isPresent()) {
                PlayerStorage storage = optstorage.get();
                return storage.getList()[pos];
            }
            return null;
        }
        return PixelmonStorage.computerManager.getPlayerStorage(player).getBox(box).getNBTByPosition(pos);
    }

    public static void sendContentsToPlayer(EntityPlayerMP player) {
        PlayerComputerStorage s = PixelmonStorage.computerManager.getPlayerStorage(player);
        ArrayList<ClientBoxInfo> boxInfo = new ArrayList<ClientBoxInfo>();
        ArrayList<PixelmonData> pokemon = new ArrayList<PixelmonData>();
        for (ComputerBox b : s.getBoxList()) {
            boxInfo.add(ClientBoxInfo.of(b.position, b.getName(), b.getBackground()));
            for (NBTTagCompound n : b.getStoredPokemon()) {
                if (n == null) continue;
                PixelmonData p = new PixelmonData(n);
                pokemon.add(p);
            }
        }
        Pixelmon.NETWORK.sendTo(new PCSync(s.lastBoxOpen, boxInfo, pokemon), player);
    }

    public static void setBoxName(EntityPlayerMP player, int box, String name) {
        PlayerComputerStorage storage = PixelmonStorage.computerManager.getPlayerStorage(player);
        storage.getBox(box).setName(name);
        Pixelmon.NETWORK.sendTo(new PCBoxName(box, name), player);
    }

    public static void setBoxBackground(EntityPlayerMP player, int box, String background) {
        PlayerComputerStorage storage = PixelmonStorage.computerManager.getPlayerStorage(player);
        if (storage.getBox(box).setBackground(background)) {
            Pixelmon.NETWORK.sendTo(new PCBoxBackground(box, background), player);
        }
    }

    public static boolean giveBackground(EntityPlayerMP player, String background) {
        PlayerComputerStorage storage = PixelmonStorage.computerManager.getPlayerStorage(player);
        if (storage.addUnlockedBackground(background)) {
            PCServer.refreshBackgrounds(player);
            return true;
        }
        return false;
    }

    public static void refreshBackgrounds(EntityPlayerMP player) {
        Pixelmon.NETWORK.sendTo(new PCInitBackgrounds(BackgroundRegistry.getBackgrounds().stream().map(bg -> ClientBackground.of(bg.getId(), bg.getName(), bg.hasUnlocked(player))).collect(Collectors.toList())), player);
    }
}

