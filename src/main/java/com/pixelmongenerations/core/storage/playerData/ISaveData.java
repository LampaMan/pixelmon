/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage.playerData;

import net.minecraft.nbt.NBTTagCompound;

public interface ISaveData {
    public void writeToNBT(NBTTagCompound var1);

    public void readFromNBT(NBTTagCompound var1);
}

