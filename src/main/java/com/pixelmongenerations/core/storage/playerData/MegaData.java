/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage.playerData;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumMegaItem;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.event.EntityPlayerExtension;
import com.pixelmongenerations.core.network.packetHandlers.clientStorage.UpdateClientPlayerData;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.HashMap;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class MegaData {
    private EnumMegaItem megaItem = EnumMegaItem.Disabled;
    private HashMap<EnumSpecies, int[]> megasObtained = new HashMap();
    private final PlayerStorage parentStorage;
    private boolean canEquip = false;

    public MegaData(PlayerStorage playerStorage) {
        this.parentStorage = playerStorage;
    }

    public void writeToNBT(NBTTagCompound nbt) {
        nbt.setString("MegaItemString", this.megaItem.toString());
        NBTTagList tagList = new NBTTagList();
        for (EnumSpecies pokemon : this.megasObtained.keySet()) {
            NBTTagCompound megaObtainedNBT = new NBTTagCompound();
            megaObtainedNBT.setString("Name", pokemon.toString());
            int[] forms = this.megasObtained.get((Object)pokemon);
            megaObtainedNBT.setIntArray("Variant", forms);
            tagList.appendTag(megaObtainedNBT);
        }
        nbt.setTag("MegasObtained", tagList);
        nbt.setBoolean("CanEquip", this.canEquip);
    }

    public void readFromNBT(NBTTagCompound nbt) {
        if (nbt.hasKey("MegaItemString")) {
            this.setMegaItem(EnumMegaItem.getFromString(nbt.getString("MegaItemString")), false);
        }
        if (nbt.hasKey("MegasObtained")) {
            NBTTagList list = nbt.getTagList("MegasObtained", 10);
            for (int i = 0; i < list.tagCount(); ++i) {
                NBTTagCompound megaInfo = (NBTTagCompound)list.get(i);
                EnumSpecies pokemon = EnumSpecies.get(megaInfo.getString("Name"));
                int[] forms = megaInfo.getIntArray("Variant");
                this.megasObtained.put(pokemon, forms);
            }
        }
        this.canEquip = nbt.getBoolean("CanEquip");
    }

    public static void getNBTTags(HashMap<String, Class> tags) {
        tags.put("MegaItemString", String.class);
        tags.put("MegasObtained", NBTTagList.class);
        tags.put("CanEquip", Boolean.class);
    }

    public boolean isMegaItemObtained(EnumSpecies pokemon, int form) {
        if (this.megasObtained.containsKey((Object)pokemon)) {
            int[] forms;
            for (int f : forms = this.megasObtained.get((Object)pokemon)) {
                if (f != form) continue;
                return true;
            }
        }
        return false;
    }

    public void obtainedItem(EnumSpecies pokemon, int form, EntityPlayerMP player) {
        if (this.megasObtained.containsKey((Object)pokemon)) {
            int[] forms;
            for (int f : forms = this.megasObtained.get((Object)pokemon)) {
                if (f != form) continue;
                return;
            }
            int[] newForms = new int[forms.length + 1];
            for (int i = 0; i < forms.length; ++i) {
                newForms[i] = forms[i];
            }
            newForms[newForms.length - 1] = form;
            this.megasObtained.put(pokemon, newForms);
        } else {
            this.megasObtained.put(pokemon, new int[]{form});
        }
        if (this.megaItem == EnumMegaItem.Disabled) {
            this.setMegaItem(EnumMegaItem.None, true);
        }
    }

    public void setMegaItem(EnumMegaItem megaItem, boolean giveChoice) {
        this.megaItem = megaItem;
        if (this.parentStorage.getPlayer() != null) {
            if (giveChoice) {
                Pixelmon.NETWORK.sendTo(new UpdateClientPlayerData(this.megaItem, true), this.parentStorage.getPlayer());
            }
            EntityPlayerExtension.updatePlayerMegaItem(this.parentStorage.getPlayer(), this.megaItem);
        }
    }

    public boolean canEquipMegaItem() {
        return this.canEquip ? true : !this.megasObtained.isEmpty();
    }

    public void setCanEquipBracelet(boolean canEquip) {
        this.canEquip = canEquip;
    }

    public EnumMegaItem getMegaItem() {
        return this.megaItem;
    }
}

