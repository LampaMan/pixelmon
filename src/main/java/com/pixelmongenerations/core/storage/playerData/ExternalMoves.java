/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage.playerData;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.ExternalMoveBase;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.ExternalMoveRegistry;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumGreninja;
import com.pixelmongenerations.core.enums.forms.EnumNecrozma;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.storage.playerData.ExternalMoveData;
import java.util.ArrayList;
import java.util.HashMap;
import net.minecraft.nbt.NBTTagCompound;

public class ExternalMoves {
    private HashMap<Integer, ExternalMoveData[]> moves = new HashMap();
    private PlayerStorage storage;

    public ExternalMoves(PlayerStorage parent) {
        this.storage = parent;
    }

    public void refresh() {
        for (int i = 0; i < 6; ++i) {
            this.refresh(this.storage.partyPokemon[i]);
        }
    }

    public void refresh(NBTTagCompound nbt) {
        int i;
        int numMoves;
        ArrayList<ExternalMoveData> pokeMoves = new ArrayList<ExternalMoveData>();
        pokeMoves.add(new ExternalMoveData(5, ExternalMoveRegistry.forage));
        String name = nbt.getString("Name");
        EnumSpecies pokemon = EnumSpecies.getFromNameAnyCase(name);
        if (pokemon.hasMega()) {
            if (PixelmonWrapper.hasCompatibleMegaStone(ItemHeld.getItemHeld(ItemHeld.readHeldItemFromNBT(nbt)), pokemon)) {
                pokeMoves.add(new ExternalMoveData(99, ExternalMoveRegistry.megaEvolution));
            } else if (pokemon == EnumSpecies.Rayquaza) {
                numMoves = nbt.getInteger("PixelmonNumberMoves");
                for (i = 0; i < numMoves; ++i) {
                    Attack a = !nbt.hasKey("PixelmonMoveID" + i) && nbt.hasKey("PixelmonMoveName" + i) ? DatabaseMoves.getAttack(nbt.getString("PixelmonMoveName" + i)) : DatabaseMoves.getAttack(nbt.getInteger("PixelmonMoveID" + i));
                    if (!a.isAttack("Dragon Ascent")) continue;
                    pokeMoves.add(new ExternalMoveData(99, ExternalMoveRegistry.megaEvolution));
                }
            }
        } else if (pokemon == EnumSpecies.Greninja && nbt.getInteger("Variant") == EnumGreninja.BATTLE_BOND.getForm()) {
            pokeMoves.add(new ExternalMoveData(99, ExternalMoveRegistry.megaEvolution));
        } else if (pokemon == EnumSpecies.Necrozma && nbt.getInteger("Variant") == EnumNecrozma.Dawn.getForm()) {
            pokeMoves.add(new ExternalMoveData(99, ExternalMoveRegistry.megaEvolution));
        } else if (pokemon == EnumSpecies.Necrozma && nbt.getInteger("Variant") == EnumNecrozma.Dusk.getForm()) {
            pokeMoves.add(new ExternalMoveData(99, ExternalMoveRegistry.megaEvolution));
        } else if (pokemon == EnumSpecies.Kyogre) {
            pokeMoves.add(new ExternalMoveData(99, ExternalMoveRegistry.megaEvolution));
        } else if (pokemon == EnumSpecies.Groudon) {
            pokeMoves.add(new ExternalMoveData(99, ExternalMoveRegistry.megaEvolution));
        }
        numMoves = nbt.getInteger("PixelmonNumberMoves");
        for (i = 0; i < numMoves; ++i) {
            int id = nbt.getInteger("PixelmonMoveID" + i);
            ExternalMoveBase e = ExternalMoveRegistry.getExternalMove(id);
            if (e == null) continue;
            pokeMoves.add(new ExternalMoveData(i, e));
        }
        ExternalMoveData[] list = this.moves.get(nbt.getInteger("pixelmonID2"));
        ExternalMoveData[] base = pokeMoves.toArray(new ExternalMoveData[pokeMoves.size()]);
        if (list != null) {
            for (ExternalMoveData data : base) {
                for (ExternalMoveData oldData : list) {
                    if (data.getBaseExternalMove() != oldData.getBaseExternalMove()) continue;
                    data.timeLastUsed = oldData.timeLastUsed;
                }
            }
        }
        this.moves.put(nbt.getInteger("pixelmonID2"), base);
    }

    public static ExternalMoveData[] load(PixelmonData pdata) {
        int i;
        ArrayList<ExternalMoveData> pokeMoves = new ArrayList<ExternalMoveData>();
        int numMoves = pdata.numMoves;
        pokeMoves.add(new ExternalMoveData(5, ExternalMoveRegistry.forage));
        EnumSpecies pokemon = pdata.getSpecies();
        if (pokemon.hasMega()) {
            if (PixelmonWrapper.hasCompatibleMegaStone(ItemHeld.getItemHeld(pdata.heldItem), pokemon)) {
                pokeMoves.add(new ExternalMoveData(99, ExternalMoveRegistry.megaEvolution));
            } else if (pokemon == EnumSpecies.Rayquaza) {
                for (i = 0; i < numMoves; ++i) {
                    Attack a = pdata.moveset[i].getAttack();
                    if (!a.isAttack("Dragon Ascent")) continue;
                    pokeMoves.add(new ExternalMoveData(99, ExternalMoveRegistry.megaEvolution));
                }
            }
        } else if (pokemon == EnumSpecies.Greninja && pdata.form == EnumGreninja.BATTLE_BOND.getForm()) {
            pokeMoves.add(new ExternalMoveData(99, ExternalMoveRegistry.megaEvolution));
        } else if (pokemon == EnumSpecies.Groudon) {
            pokeMoves.add(new ExternalMoveData(99, ExternalMoveRegistry.megaEvolution));
        } else if (pokemon == EnumSpecies.Kyogre) {
            pokeMoves.add(new ExternalMoveData(99, ExternalMoveRegistry.megaEvolution));
        } else if (pokemon == EnumSpecies.Necrozma && pdata.form == EnumNecrozma.Dawn.getForm()) {
            pokeMoves.add(new ExternalMoveData(99, ExternalMoveRegistry.megaEvolution));
        } else if (pokemon == EnumSpecies.Necrozma && pdata.form == EnumNecrozma.Dusk.getForm()) {
            pokeMoves.add(new ExternalMoveData(99, ExternalMoveRegistry.megaEvolution));
        }
        for (i = 0; i < numMoves; ++i) {
            int id = pdata.moveset[i].attackIndex;
            ExternalMoveBase e = ExternalMoveRegistry.getExternalMove(id);
            if (e == null) continue;
            pokeMoves.add(new ExternalMoveData(i, e));
        }
        return pokeMoves.toArray(new ExternalMoveData[pokeMoves.size()]);
    }

    public ExternalMoveData get(int[] pokemonId, int moveIndex) {
        ExternalMoveData[] list = this.moves.get(pokemonId[1]);
        if (list != null && moveIndex >= 0 && moveIndex < list.length) {
            return list[moveIndex];
        }
        return null;
    }

    public void set(int[] pokemonId, int moveIndex, ExternalMoveData move) {
        ExternalMoveData[] list = this.moves.get(pokemonId[1]);
        if (list != null && moveIndex >= 0 && moveIndex < list.length) {
            list[moveIndex] = move;
        }
        this.moves.put(pokemonId[1], list);
    }
}

