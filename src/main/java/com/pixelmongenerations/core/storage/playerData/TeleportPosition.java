/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage.playerData;

import com.pixelmongenerations.core.storage.playerData.ISaveData;
import java.util.HashMap;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.Vec2f;
import net.minecraft.util.math.Vec3d;

public class TeleportPosition
implements ISaveData {
    private double x;
    private double y;
    private double z;
    private float yaw;
    private float pitch;

    @Override
    public void writeToNBT(NBTTagCompound nbt) {
        nbt.setDouble("tpPosX", this.x);
        nbt.setDouble("tpPosY", this.y);
        nbt.setDouble("tpPosZ", this.z);
        nbt.setFloat("tpRotY", this.yaw);
        nbt.setFloat("tpRotP", this.pitch);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        this.x = nbt.getDouble("tpPosX");
        this.y = nbt.getDouble("tpPosY");
        this.z = nbt.getDouble("tpPosZ");
        this.yaw = nbt.getFloat("tpRotY");
        this.pitch = nbt.getFloat("tpRotP");
    }

    public void store(double posX, double posY, double posZ, float rotationYaw, float rotationPitch) {
        this.x = posX;
        this.y = posY;
        this.z = posZ;
        this.yaw = rotationYaw;
        this.pitch = rotationPitch;
    }

    public void teleport(EntityPlayerMP player) {
        if (this.x != 0.0 && this.y != 0.0 && this.z != 0.0) {
            player.world.playSound(null, player.posX, player.posY, player.posZ, SoundEvents.ENTITY_ENDERMEN_TELEPORT, SoundCategory.NEUTRAL, 1.0f, 1.0f);
            player.connection.setPlayerLocation(this.x, this.y, this.z, this.yaw, this.pitch);
            player.world.playSound(null, player.posX, player.posY, player.posZ, SoundEvents.ENTITY_ENDERMEN_TELEPORT, SoundCategory.NEUTRAL, 1.0f, 1.0f);
        }
    }

    public static void getNBTTags(HashMap<String, Class> tags) {
        tags.put("tpPosX", Double.class);
        tags.put("tpPosY", Double.class);
        tags.put("tpPosZ", Double.class);
        tags.put("tpRotP", Float.class);
        tags.put("tpRotY", Float.class);
    }

    public Vec3d getPosition() {
        return new Vec3d(this.x, this.y, this.z);
    }

    public Vec2f getYawAndPitch() {
        return new Vec2f(this.yaw, this.pitch);
    }
}

