/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage.playerData;

import com.pixelmongenerations.api.events.ExternalMoveEvent;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.ExternalMoveBase;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.ExternalMove;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.common.MinecraftForge;

public class ExternalMoveData {
    public int moveIndex;
    private ExternalMoveBase move;
    public long timeLastUsed;

    public ExternalMoveData(int moveIndex, ExternalMoveBase move) {
        this.moveIndex = moveIndex;
        this.move = move;
    }

    public void execute(EntityPlayerMP user, EntityPixelmon pixelmon, RayTraceResult objPos) {
        ExternalMoveEvent.PreparingMoveEvent externalMoveEvent = new ExternalMoveEvent.PreparingMoveEvent(user, pixelmon, this.move, objPos);
        if (MinecraftForge.EVENT_BUS.post(externalMoveEvent)) {
            return;
        }
        long worldTime = pixelmon.world.getTotalWorldTime();
        this.timeLastUsed = Math.min(this.timeLastUsed, worldTime);
        if (worldTime > this.timeLastUsed + (long)externalMoveEvent.getCooldown() && this.move.execute(pixelmon, externalMoveEvent.getTarget(), this.moveIndex)) {
            this.timeLastUsed = worldTime;
            Pixelmon.NETWORK.sendTo(new ExternalMove(pixelmon.getPokemonId(), this.moveIndex, this.timeLastUsed), user);
        }
    }

    public ExternalMoveBase getBaseExternalMove() {
        return this.move;
    }

    public double getTargetDistance() {
        return this.move.getTargetDistance();
    }
}

