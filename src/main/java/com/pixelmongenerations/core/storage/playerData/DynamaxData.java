/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage.playerData;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumDynamaxItem;
import com.pixelmongenerations.core.event.EntityPlayerExtension;
import com.pixelmongenerations.core.network.packetHandlers.clientStorage.UpdateClientPlayerData;
import com.pixelmongenerations.core.storage.PlayerStorage;
import net.minecraft.nbt.NBTTagCompound;

public class DynamaxData {
    private EnumDynamaxItem dynamaxItem = EnumDynamaxItem.Disabled;
    private final PlayerStorage parentStorage;
    private boolean canEquip = false;

    public DynamaxData(PlayerStorage playerStorage) {
        this.parentStorage = playerStorage;
    }

    public void writeToNBT(NBTTagCompound nbt) {
        nbt.setString("DynamaxItemString", this.dynamaxItem.toString());
        nbt.setBoolean("CanEquipDynamax", this.canEquip);
    }

    public void readFromNBT(NBTTagCompound nbt) {
        if (!PixelmonConfig.allowDynamax) {
            this.canEquip = false;
            this.setDynamaxItem(EnumDynamaxItem.Disabled, false);
        } else {
            this.canEquip = nbt.getBoolean("CanEquipDynamax");
            if (nbt.hasKey("DynamaxItemString") || this.canEquipDynamaxItem()) {
                if (!nbt.hasKey("DynamaxItemString") && this.canEquipDynamaxItem()) {
                    this.setDynamaxItem(EnumDynamaxItem.None, false);
                } else if (this.canEquipDynamaxItem()) {
                    this.setDynamaxItem(EnumDynamaxItem.getFromString(nbt.getString("DynamaxItemString")), false);
                } else {
                    this.setDynamaxItem(EnumDynamaxItem.Disabled, false);
                }
            } else {
                this.setDynamaxItem(EnumDynamaxItem.Disabled, false);
            }
        }
    }

    public void setDynamaxItem(EnumDynamaxItem dynamaxItem, boolean giveChoice) {
        this.dynamaxItem = dynamaxItem;
        if (this.parentStorage.getPlayer() != null) {
            if (giveChoice) {
                Pixelmon.NETWORK.sendTo(new UpdateClientPlayerData(this.dynamaxItem, true), this.parentStorage.getPlayer());
            }
            EntityPlayerExtension.updatePlayerDynamaxItem(this.parentStorage.getPlayer(), this.dynamaxItem);
        }
    }

    public boolean canEquipDynamaxItem() {
        return this.canEquip;
    }

    public void setCanEquipDynamaxBand(boolean canEquip) {
        this.canEquip = canEquip;
    }

    public EnumDynamaxItem getDynamaxItem() {
        return this.dynamaxItem;
    }
}

