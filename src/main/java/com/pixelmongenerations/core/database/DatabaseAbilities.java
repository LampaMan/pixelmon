/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.database;

import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.core.data.pokemon.PokemonRegistry;
import com.pixelmongenerations.core.util.RegexPatterns;

public class DatabaseAbilities {
    public static String[] getAbilities(String pixelmon) {
        BaseStats stats = PokemonRegistry.getBaseFor(pixelmon);
        return stats.abilities;
    }

    public static String[] getAbilities(int id) {
        BaseStats stats = PokemonRegistry.getBaseFor(id);
        return stats.abilities;
    }

    public static Integer getSlotForAbility(int id, String ability) {
        String[] abilities = DatabaseAbilities.getAbilities(id);
        if (abilities != null) {
            for (int i = 0; i < abilities.length; ++i) {
                if (abilities[i] == null || !RegexPatterns.SPACE_SYMBOL.matcher(abilities[i]).replaceAll("").equalsIgnoreCase(RegexPatterns.SPACE_SYMBOL.matcher(ability).replaceAll(""))) continue;
                return i;
            }
        }
        return null;
    }
}

