/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.common.entity.npcs.registry.PokemonForm;
import com.pixelmongenerations.core.enums.EnumSpecies;

public class StarterList {
    public static final int NUM_STARTERS = 24;
    private static PokemonForm[] starterList = new PokemonForm[]{new PokemonForm(EnumSpecies.Bulbasaur), new PokemonForm(EnumSpecies.Squirtle), new PokemonForm(EnumSpecies.Charmander), new PokemonForm(EnumSpecies.Chikorita), new PokemonForm(EnumSpecies.Totodile), new PokemonForm(EnumSpecies.Cyndaquil), new PokemonForm(EnumSpecies.Treecko), new PokemonForm(EnumSpecies.Mudkip), new PokemonForm(EnumSpecies.Torchic), new PokemonForm(EnumSpecies.Turtwig), new PokemonForm(EnumSpecies.Piplup), new PokemonForm(EnumSpecies.Chimchar), new PokemonForm(EnumSpecies.Snivy), new PokemonForm(EnumSpecies.Oshawott), new PokemonForm(EnumSpecies.Tepig), new PokemonForm(EnumSpecies.Chespin), new PokemonForm(EnumSpecies.Froakie), new PokemonForm(EnumSpecies.Fennekin), new PokemonForm(EnumSpecies.Rowlet), new PokemonForm(EnumSpecies.Popplio), new PokemonForm(EnumSpecies.Litten), new PokemonForm(EnumSpecies.Grookey), new PokemonForm(EnumSpecies.Sobble), new PokemonForm(EnumSpecies.Scorbunny)};
    private static EnumSpecies[] starterSpecies = new EnumSpecies[]{EnumSpecies.Bulbasaur, EnumSpecies.Squirtle, EnumSpecies.Charmander, EnumSpecies.Chikorita, EnumSpecies.Totodile, EnumSpecies.Cyndaquil, EnumSpecies.Treecko, EnumSpecies.Mudkip, EnumSpecies.Torchic, EnumSpecies.Turtwig, EnumSpecies.Piplup, EnumSpecies.Chimchar, EnumSpecies.Snivy, EnumSpecies.Oshawott, EnumSpecies.Tepig, EnumSpecies.Chespin, EnumSpecies.Froakie, EnumSpecies.Fennekin, EnumSpecies.Rowlet, EnumSpecies.Popplio, EnumSpecies.Litten, EnumSpecies.Grookey, EnumSpecies.Sobble, EnumSpecies.Scorbunny};

    public static void setStarterList(PokemonForm[] newStarterList) {
        starterList = newStarterList;
        for (int i = 0; i < starterList.length; ++i) {
            StarterList.starterSpecies[i] = starterList[i] == null ? null : StarterList.starterList[i].pokemon;
        }
    }

    public static PokemonForm[] getStarterList() {
        return starterList;
    }
}

