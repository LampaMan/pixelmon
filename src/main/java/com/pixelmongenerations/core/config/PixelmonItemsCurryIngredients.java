/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.common.item.CurryIngredient;
import com.pixelmongenerations.core.enums.EnumCurryType;
import net.minecraft.item.Item;

public class PixelmonItemsCurryIngredients {
    public static Item sausages;
    public static Item bobsFoodTin;
    public static Item bachsFoodTin;
    public static Item tinOfBeans;
    public static Item bread;
    public static Item pasta;
    public static Item mixedMushrooms;
    public static Item smokedPokeTail;
    public static Item largeLeek;
    public static Item fancyApple;
    public static Item brittleBones;
    public static Item packofPotatoes;
    public static Item pungentRoot;
    public static Item saladMix;
    public static Item friedFood;
    public static Item boiledEgg;
    public static Item fruitBunch;
    public static Item moomooCheese;
    public static Item spiceMix;
    public static Item freshCream;
    public static Item packagedCurry;
    public static Item coconutMilk;
    public static Item instantNoodles;
    public static Item prepackagedBurger;
    public static Item gigantamix;

    public static void load() {
        sausages = new CurryIngredient("sausages", EnumCurryType.Sausage);
        bobsFoodTin = new CurryIngredient("bobs_food_tin", EnumCurryType.Juicy);
        bachsFoodTin = new CurryIngredient("bachs_food_tin", EnumCurryType.Rich);
        tinOfBeans = new CurryIngredient("tin_of_beans", EnumCurryType.BeanMedley);
        bread = new CurryIngredient("bread", EnumCurryType.Toast);
        pasta = new CurryIngredient("pasta", EnumCurryType.Pasta);
        mixedMushrooms = new CurryIngredient("mixed_mushrooms", EnumCurryType.MushroomMedley);
        smokedPokeTail = new CurryIngredient("smoked_poke_tail", EnumCurryType.SmokedTail);
        largeLeek = new CurryIngredient("large_leek", EnumCurryType.Leek);
        fancyApple = new CurryIngredient("fancy_apple", EnumCurryType.Apple);
        brittleBones = new CurryIngredient("brittle_bones", EnumCurryType.Bone);
        packofPotatoes = new CurryIngredient("pack_of_potatoes", EnumCurryType.PlentyOfPotato);
        pungentRoot = new CurryIngredient("pungent_root", EnumCurryType.HerbMedley);
        saladMix = new CurryIngredient("salad_mix", EnumCurryType.Salad);
        friedFood = new CurryIngredient("fried_food", EnumCurryType.FriedFood);
        boiledEgg = new CurryIngredient("boiled_egg", EnumCurryType.BoiledEgg);
        fruitBunch = new CurryIngredient("fruit_bunch", EnumCurryType.Tropical);
        moomooCheese = new CurryIngredient("moomoo_cheese", EnumCurryType.CheeseCovered);
        spiceMix = new CurryIngredient("spice_mix", EnumCurryType.Seasoned);
        freshCream = new CurryIngredient("fresh_cream", EnumCurryType.WhippedCream);
        packagedCurry = new CurryIngredient("packaged_curry", EnumCurryType.Decorative);
        coconutMilk = new CurryIngredient("coconut_milk", EnumCurryType.Coconut);
        instantNoodles = new CurryIngredient("instant_noodles", EnumCurryType.InstantNoodle);
        prepackagedBurger = new CurryIngredient("precooked_burger", EnumCurryType.BurgerSteak);
        gigantamix = new CurryIngredient("gigantamix", EnumCurryType.Gigantamax);
    }
}

