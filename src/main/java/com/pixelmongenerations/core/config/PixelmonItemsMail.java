/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.common.item.heldItems.ItemMail;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import java.util.ArrayList;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;

public class PixelmonItemsMail {
    private static String[] mailTypes = new String[]{"air", "bloom", "brick", "bridged", "bridgem", "bridges", "bridget", "bridgev", "bubble", "dream", "fab", "favored", "flame", "glitter", "grass", "greet", "harbor", "heart", "inquiry", "like", "mech", "mosaic", "orange", "reply", "retro", "rsvp", "shadow", "snow", "space", "steel", "thanks", "tropic", "tunnel", "wave", "wood"};
    public static ArrayList<ItemMail> items = new ArrayList(mailTypes.length);

    static void registerMailItems(RegistryEvent.Register<Item> event) {
        for (String name : mailTypes) {
            ItemMail mailItem = new ItemMail(name);
            event.getRegistry().register(mailItem);
            items.add(mailItem);
            PixelmonItemsHeld.getHeldItemList().add(mailItem);
        }
    }

    static void registerRenderers() {
        for (ItemMail itemMail : items) {
            ModelLoader.setCustomModelResourceLocation(itemMail, 0, new ModelResourceLocation(itemMail.getRegistryName(), "inventory"));
            ModelLoader.setCustomModelResourceLocation(itemMail, 1, new ModelResourceLocation(itemMail.getRegistryName().toString() + "_closed", "inventory"));
            ModelBakery.registerItemVariants(itemMail, itemMail.getRegistryName(), new ResourceLocation(itemMail.getRegistryName() + "_closed"));
        }
    }
}

