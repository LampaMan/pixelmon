/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 *  com.google.gson.Gson
 *  com.google.gson.GsonBuilder
 */
package com.pixelmongenerations.core.config;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixelmongenerations.api.spawning.CompositeSpawnCondition;
import com.pixelmongenerations.api.spawning.conditions.SpawnCondition;
import com.pixelmongenerations.api.spawning.util.SpawnConditionTypeAdapter;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Stream;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Biomes;
import net.minecraft.init.Blocks;

public class BetterSpawnerConfig {
    public static BetterSpawnerConfig INSTANCE = null;
    public static final String PATH = "config/pixelmon/BetterSpawnerConfig.json";
    public static final HashMap<String, Long> intervalTimes = new HashMap();
    public CompositeSpawnCondition globalCompositeCondition = new CompositeSpawnCondition();
    public HashMap<String, Integer> intervalSeconds = new HashMap();
    public HashMap<String, ArrayList<String>> blockCategories = new HashMap();
    public HashMap<String, ArrayList<String>> biomeCategories = new HashMap();

    public static void load() {
        try {
            Gson gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(SpawnCondition.class, (Object)new SpawnConditionTypeAdapter()).create();
            File file = new File(PATH);
            file.getParentFile().mkdirs();
            INSTANCE = null;
            try {
                if (file.exists()) {
                    INSTANCE = (BetterSpawnerConfig)gson.fromJson((Reader)new FileReader(PATH), BetterSpawnerConfig.class);
                }
            }
            catch (Exception var5) {
                var5.printStackTrace();
            }
            if (INSTANCE == null) {
                INSTANCE = new BetterSpawnerConfig();
                SpawnCondition condition = new SpawnCondition();
                condition.dimensions = Lists.newArrayList(0, -1, 1, 24);
                BetterSpawnerConfig.INSTANCE.globalCompositeCondition = new CompositeSpawnCondition(Lists.newArrayList(condition), Lists.newArrayList());
                BetterSpawnerConfig.INSTANCE.intervalSeconds.put("legendary", 7200);
                BetterSpawnerConfig.INSTANCE.blockCategories.put("land", Lists.newArrayList(Blocks.HARDENED_CLAY.getRegistryName().toString(), Blocks.STAINED_HARDENED_CLAY.getRegistryName().toString(), Blocks.BLACK_GLAZED_TERRACOTTA.getRegistryName().toString(), Blocks.BLUE_GLAZED_TERRACOTTA.getRegistryName().toString(), Blocks.BROWN_GLAZED_TERRACOTTA.getRegistryName().toString(), Blocks.CYAN_GLAZED_TERRACOTTA.getRegistryName().toString(), Blocks.GRAY_GLAZED_TERRACOTTA.getRegistryName().toString(), Blocks.GREEN_GLAZED_TERRACOTTA.getRegistryName().toString(), Blocks.LIGHT_BLUE_GLAZED_TERRACOTTA.getRegistryName().toString(), Blocks.LIME_GLAZED_TERRACOTTA.getRegistryName().toString(), Blocks.MAGENTA_GLAZED_TERRACOTTA.getRegistryName().toString(), Blocks.ORANGE_GLAZED_TERRACOTTA.getRegistryName().toString(), Blocks.PINK_GLAZED_TERRACOTTA.getRegistryName().toString(), Blocks.PURPLE_GLAZED_TERRACOTTA.getRegistryName().toString(), Blocks.RED_GLAZED_TERRACOTTA.getRegistryName().toString(), Blocks.SILVER_GLAZED_TERRACOTTA.getRegistryName().toString(), Blocks.WHITE_GLAZED_TERRACOTTA.getRegistryName().toString(), Blocks.YELLOW_GLAZED_TERRACOTTA.getRegistryName().toString(), Blocks.GRASS.getRegistryName().toString(), Blocks.DIRT.getRegistryName().toString(), Blocks.STONE.getRegistryName().toString(), Blocks.WATERLILY.getRegistryName().toString(), Blocks.SAND.getRegistryName().toString(), Blocks.GRAVEL.getRegistryName().toString(), Blocks.MYCELIUM.getRegistryName().toString(), Blocks.SNOW.getRegistryName().toString(), Blocks.NETHERRACK.getRegistryName().toString(), Blocks.SOUL_SAND.getRegistryName().toString(), Blocks.ICE.getRegistryName().toString(), Blocks.FROSTED_ICE.getRegistryName().toString(), Blocks.PACKED_ICE.getRegistryName().toString(), Blocks.SANDSTONE.getRegistryName().toString(), Blocks.COBBLESTONE.getRegistryName().toString(), Blocks.MOSSY_COBBLESTONE.getRegistryName().toString(), Blocks.END_STONE.getRegistryName().toString(), "biomesoplenty:ash_block", "biomesoplenty:dirt", "biomesoplenty:white_sand", "biomesoplenty:white_sandstone", "biomesoplenty:dried_sand", "biomesoplenty:hard_ice", "biomesoplenty:mud", "biomesoplenty:grass", "pixelmon:ultra_sand", "pixelmon:ultra_sandstone", "pixelmon:ruins_sand", "pixelmon:copper_junk", "pixelmon:burst_turf"));
                BetterSpawnerConfig.INSTANCE.blockCategories.put("air", Lists.newArrayList(Blocks.AIR.getRegistryName().toString(), Blocks.TALLGRASS.getRegistryName().toString(), Blocks.SNOW_LAYER.getRegistryName().toString(), Blocks.RED_FLOWER.getRegistryName().toString(), Blocks.YELLOW_FLOWER.getRegistryName().toString(), PixelmonBlocks.pixelmonGrassBlock.getRegistryName().toString(), PixelmonBlocks.pokeGrassBlock.getRegistryName().toString(), "biomesoplenty:double_plant", "biomesoplenty:flower_0", "biomesoplenty:flower_1", "biomesoplenty:mushroom", "biomesoplenty:plant_0", "biomesoplenty:plant_1"));
                BetterSpawnerConfig.INSTANCE.blockCategories.put("water", Lists.newArrayList(Blocks.WATER.getRegistryName().toString(), "biomesoplenty:coral", "biomesoplenty:hot_spring_water", "biomesoplenty:seaweed", "biomesoplenty:waterlilly"));
                BetterSpawnerConfig.INSTANCE.blockCategories.put("lava", Lists.newArrayList(Blocks.LAVA.getRegistryName().toString()));
                BetterSpawnerConfig.INSTANCE.blockCategories.put("seesSkyException", Lists.newArrayList(Blocks.LEAVES.getRegistryName().toString(), Blocks.LEAVES2.getRegistryName().toString(), Blocks.NETHERRACK.getRegistryName().toString(), Blocks.BEDROCK.getRegistryName().toString(), Blocks.GLASS.getRegistryName().toString(), Blocks.STAINED_GLASS.getRegistryName().toString(), Blocks.STAINED_GLASS_PANE.getRegistryName().toString(), Blocks.GLASS_PANE.getRegistryName().toString(), PixelmonBlocks.hiddenCube.getRegistryName().toString(), "biomesoplenty:leaves_0", "biomesoplenty:leaves_1", "biomesoplenty:leaves_2", "biomesoplenty:leaves_3", "biomesoplenty:leaves_4", "biomesoplenty:leaves_5"));
                BetterSpawnerConfig.INSTANCE.blockCategories.put("high grass", Lists.newArrayList(Blocks.TALLGRASS.getRegistryName().toString(), PixelmonBlocks.pixelmonGrassBlock.getRegistryName().toString(), PixelmonBlocks.pokeGrassBlock.getRegistryName().toString()));
                BetterSpawnerConfig.INSTANCE.blockCategories.put("sandy", Lists.newArrayList(Blocks.SAND.getRegistryName().toString(), Blocks.CLAY.getRegistryName().toString(), "biomesoplenty:white_sand"));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("oceans", Lists.newArrayList(Biomes.OCEAN.getRegistryName().toString(), Biomes.DEEP_OCEAN.getRegistryName().toString(), Biomes.FROZEN_OCEAN.getRegistryName().toString(), "biomesoplenty:coral_reef", "biomesoplenty:kelp_forest"));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("dry", Lists.newArrayList(Biomes.DESERT.getRegistryName().toString(), Biomes.DESERT_HILLS.getRegistryName().toString(), Biomes.MUTATED_DESERT.getRegistryName().toString(), "biomesoplenty:outback", "biomesoplenty:brushland", "biomesoplenty:volcanic_island", "biomesoplenty:xeric_shrubland", "biomesoplenty:oasis", "biomesoplenty:shrubland"));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("mountainous", Lists.newArrayList(Biomes.EXTREME_HILLS.getRegistryName().toString(), Biomes.EXTREME_HILLS_EDGE.getRegistryName().toString(), Biomes.EXTREME_HILLS_WITH_TREES.getRegistryName().toString(), Biomes.MUTATED_EXTREME_HILLS.getRegistryName().toString(), Biomes.MUTATED_EXTREME_HILLS_WITH_TREES.getRegistryName().toString(), "biomesoplenty:alps", "biomesoplenty:alps_foothills", "biomesoplenty:mountain_foothills", "biomesoplenty:crag", "biomesoplenty:highland", "biomesoplenty:mountain", "biomesoplenty:overgrown_cliffs"));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("cold", Lists.newArrayList(Biomes.COLD_TAIGA.getRegistryName().toString(), Biomes.COLD_TAIGA_HILLS.getRegistryName().toString(), Biomes.COLD_BEACH.getRegistryName().toString(), Biomes.FROZEN_RIVER.getRegistryName().toString(), Biomes.FROZEN_OCEAN.getRegistryName().toString(), Biomes.MUTATED_TAIGA_COLD.getRegistryName().toString(), Biomes.ICE_MOUNTAINS.getRegistryName().toString(), Biomes.ICE_PLAINS.getRegistryName().toString(), Biomes.MUTATED_ICE_FLATS.getRegistryName().toString(), "biomesoplenty:alps", "biomesoplenty:alps_foothills", "biomesoplenty:cold_desert", "biomesoplenty:coniferous_forest", "biomesoplenty:crag", "biomesoplenty:snowy_coniferous_forest", "biomesoplenty:snowy_forest", "biomesoplenty:tundra", "biomesoplenty:glacier", "biomesoplenty:snowy_tundra"));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("cold mountains", Lists.newArrayList(Biomes.ICE_MOUNTAINS.getRegistryName().toString(), Biomes.COLD_TAIGA_HILLS.getRegistryName().toString(), "biomesoplenty:alps", "biomesoplenty:alps_foothills", "biomesoplenty:crag"));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("mesas", Lists.newArrayList(Biomes.MESA.getRegistryName().toString(), Biomes.MESA_CLEAR_ROCK.getRegistryName().toString(), Biomes.MESA_ROCK.getRegistryName().toString(), Biomes.MUTATED_MESA.getRegistryName().toString(), Biomes.MUTATED_MESA_CLEAR_ROCK.getRegistryName().toString(), Biomes.MUTATED_MESA_ROCK.getRegistryName().toString()));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("savannas", Lists.newArrayList(Biomes.SAVANNA.getRegistryName().toString(), Biomes.SAVANNA_PLATEAU.getRegistryName().toString(), Biomes.MUTATED_SAVANNA.getRegistryName().toString(), Biomes.MUTATED_SAVANNA_ROCK.getRegistryName().toString(), "biomesoplenty:brushland", "biomesoplenty:lush_desert", "biomesoplenty:outback", "biomesoplenty:oasis", "biomesoplenty:xeric_shrubland"));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("jungles", Lists.newArrayList(Biomes.JUNGLE.getRegistryName().toString(), Biomes.JUNGLE_EDGE.getRegistryName().toString(), Biomes.JUNGLE_HILLS.getRegistryName().toString(), Biomes.MUTATED_JUNGLE.getRegistryName().toString(), Biomes.MUTATED_JUNGLE_EDGE.getRegistryName().toString(), "biomesoplenty:eucalyptus_forest", "biomesoplenty:bamboo_forest", "biomesoplenty:rainforest", "biomesoplenty:tropical_rainforest", "biomesoplenty:tropical_island"));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("flowery", Lists.newArrayList(Biomes.MUTATED_PLAINS.getRegistryName().toString(), Biomes.MUTATED_FOREST.getRegistryName().toString(), "biomesoplenty:cherry_blossom_grove", "biomesoplenty:flower_island", "biomesoplenty:flower_field", "biomesoplenty:lavender_fields"));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("swamps", Lists.newArrayList(Biomes.SWAMPLAND.getRegistryName().toString(), Biomes.MUTATED_SWAMPLAND.getRegistryName().toString(), "biomesoplenty:bayou", "biomesoplenty:bog", "biomesoplenty:dead_swamp", "biomesoplenty:fen", "biomesoplenty:land_of_lakes", "biomesoplenty:lush_swamp", "biomesoplenty:mangrove", "biomesoplenty:marsh", "biomesoplenty:moor", "biomesoplenty:quagmire", "biomesoplenty:wetland"));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("taigas", Lists.newArrayList(Biomes.TAIGA.getRegistryName().toString(), Biomes.TAIGA_HILLS.getRegistryName().toString(), Biomes.MUTATED_TAIGA.getRegistryName().toString()));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("redwoods", Lists.newArrayList(Biomes.REDWOOD_TAIGA.getRegistryName().toString(), Biomes.REDWOOD_TAIGA_HILLS.getRegistryName().toString(), Biomes.MUTATED_REDWOOD_TAIGA.getRegistryName().toString(), Biomes.MUTATED_REDWOOD_TAIGA_HILLS.getRegistryName().toString(), "biomesoplenty:redwood_forest"));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("birches", Lists.newArrayList(Biomes.BIRCH_FOREST.getRegistryName().toString(), Biomes.BIRCH_FOREST_HILLS.getRegistryName().toString(), Biomes.MUTATED_BIRCH_FOREST.getRegistryName().toString(), Biomes.MUTATED_BIRCH_FOREST_HILLS.getRegistryName().toString()));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("forests", Lists.newArrayList(Biomes.FOREST.getRegistryName().toString(), Biomes.FOREST_HILLS.getRegistryName().toString(), Biomes.MUTATED_FOREST.getRegistryName().toString(), Biomes.ROOFED_FOREST.getRegistryName().toString(), Biomes.MUTATED_ROOFED_FOREST.getRegistryName().toString(), "biomesoplenty:boreal_forest", "biomesoplenty:maple_woods", "biomesoplenty:orchard", "biomesoplenty:seasonal_forest", "biomesoplenty:woodland", "biomesoplenty:land_of_lakes", "biomesoplenty:grove", "biomesoplenty:maple_woods"));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("mushroom", Lists.newArrayList(Biomes.MUSHROOM_ISLAND.getRegistryName().toString(), Biomes.MUSHROOM_ISLAND_SHORE.getRegistryName().toString(), "biomesoplenty:fungi_forest"));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("plains", Lists.newArrayList(Biomes.PLAINS.getRegistryName().toString(), Biomes.MUTATED_PLAINS.getRegistryName().toString(), "biomesoplenty:chaparral", "biomesoplenty:grassland", "biomesoplenty:flower_field", "biomesoplenty:pasture", "biomesoplenty:meadow", "biomesoplenty:grove", "biomesoplenty:prairie", "biomesoplenty:steppe"));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("hills", Lists.newArrayList(Biomes.BIRCH_FOREST_HILLS.getRegistryName().toString(), Biomes.COLD_TAIGA_HILLS.getRegistryName().toString(), Biomes.DESERT_HILLS.getRegistryName().toString(), Biomes.FOREST_HILLS.getRegistryName().toString(), Biomes.JUNGLE_HILLS.getRegistryName().toString(), Biomes.MUTATED_BIRCH_FOREST_HILLS.getRegistryName().toString(), Biomes.REDWOOD_TAIGA_HILLS.getRegistryName().toString(), Biomes.TAIGA_HILLS.getRegistryName().toString(), "biomesoplenty:boreal_forest", "biomesoplenty:grassland", "biomesoplenty:highland", "biomesoplenty:moor"));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("beaches", Lists.newArrayList(Biomes.BEACH.getRegistryName().toString(), Biomes.COLD_BEACH.getRegistryName().toString(), Biomes.STONE_BEACH.getRegistryName().toString(), "biomesoplenty:gravel_beach", "biomesoplenty:white_beach", "biomesoplenty:origin_beach"));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("cold forests", Lists.newArrayList(Biomes.TAIGA.getRegistryName().toString(), Biomes.TAIGA_HILLS.getRegistryName().toString(), Biomes.COLD_TAIGA.getRegistryName().toString(), Biomes.COLD_TAIGA_HILLS.getRegistryName().toString(), Biomes.MUTATED_TAIGA_COLD.getRegistryName().toString(), "biomesoplenty:shield", "biomesoplenty:coniferous_forest", "biomesoplenty:snowy_forest", "biomesoplenty:snowy_coniferous_forest"));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("all forests", Lists.newArrayList(Biomes.FOREST.getRegistryName().toString(), Biomes.FOREST_HILLS.getRegistryName().toString(), Biomes.MUTATED_FOREST.getRegistryName().toString(), Biomes.ROOFED_FOREST.getRegistryName().toString(), Biomes.MUTATED_ROOFED_FOREST.getRegistryName().toString(), Biomes.TAIGA.getRegistryName().toString(), Biomes.TAIGA_HILLS.getRegistryName().toString(), Biomes.MUTATED_TAIGA.getRegistryName().toString(), Biomes.REDWOOD_TAIGA.getRegistryName().toString(), Biomes.REDWOOD_TAIGA_HILLS.getRegistryName().toString(), Biomes.MUTATED_REDWOOD_TAIGA.getRegistryName().toString(), Biomes.MUTATED_REDWOOD_TAIGA_HILLS.getRegistryName().toString(), Biomes.BIRCH_FOREST.getRegistryName().toString(), Biomes.BIRCH_FOREST_HILLS.getRegistryName().toString(), Biomes.MUTATED_BIRCH_FOREST.getRegistryName().toString(), Biomes.MUTATED_BIRCH_FOREST_HILLS.getRegistryName().toString(), "biomesoplenty:boreal_forest", "biomesoplenty:maple_woods", "biomesoplenty:orchard", "biomesoplenty:seasonal_forest", "biomesoplenty:woodland", "biomesoplenty:land_of_lakes", "biomesoplenty:grove", "biomesoplenty:maple_woods"));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("mountainous forests", Lists.newArrayList(Biomes.EXTREME_HILLS_WITH_TREES.getRegistryName().toString(), Biomes.MUTATED_EXTREME_HILLS_WITH_TREES.getRegistryName().toString()));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("nether", Lists.newArrayList(Biomes.HELL.getRegistryName().toString(), "biomesoplenty:corrupted_sands", "biomesoplenty:fungi_forest", "biomesoplenty:phantasmagoric_inferno", "biomesoplenty:visceral_heap", "biomesoplenty:undergarden"));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("mystical", Lists.newArrayList(Biomes.MUSHROOM_ISLAND.getRegistryName().toString(), Biomes.MUSHROOM_ISLAND_SHORE.getRegistryName().toString(), "biomesoplenty:cherry_blossom_grove", "biomesoplenty:flower_island", "biomesoplenty:origin_island", "biomesoplenty:lavender_field", "biomesoplenty:mystic_grove", "biomesoplenty:sacred_springs"));
                BetterSpawnerConfig.INSTANCE.biomeCategories.put("dead", Lists.newArrayList("biomesoplenty:dead_forest", "biomesoplenty:quagmire", "biomesoplenty:dead_swamp", "biomesoplenty:ominous_woods", "biomesoplenty:fen", "biomesoplenty:steppe", "biomesoplenty:tundra", "biomesoplenty:wasteland"));
                if (!file.exists()) {
                    String json = gson.toJson((Object)INSTANCE);
                    PrintWriter pw = new PrintWriter(file);
                    pw.write(json);
                    pw.flush();
                    pw.close();
                } else {
                    Pixelmon.LOGGER.error("Could not parse the BetterSpawnerConfig.json. Check it in a JSON parser. Using default options.");
                }
            }
        }
        catch (Exception var6) {
            var6.printStackTrace();
        }
    }

    public static boolean isInCategory(String category, String targetBiome) {
        ArrayList<String> biomes = BetterSpawnerConfig.INSTANCE.biomeCategories.get(category);
        if (biomes == null) {
            return false;
        }
        for (String biome : biomes) {
            if (!biome.equals(targetBiome)) continue;
            return true;
        }
        return false;
    }

    public static boolean checkInterval(String interval) {
        if (interval == null) {
            return true;
        }
        if (!BetterSpawnerConfig.INSTANCE.intervalSeconds.containsKey(interval)) {
            return true;
        }
        if (BetterSpawnerConfig.INSTANCE.intervalSeconds.get(interval) == -1) {
            return false;
        }
        Long last = intervalTimes.get(interval);
        if (last != null && BetterSpawnerConfig.INSTANCE.intervalSeconds.containsKey(interval)) {
            return last < System.currentTimeMillis();
        }
        return true;
    }

    public static void consumeInterval(String interval) {
        if (BetterSpawnerConfig.INSTANCE.intervalSeconds.containsKey(interval)) {
            intervalTimes.put(interval, System.currentTimeMillis() + (long)(BetterSpawnerConfig.INSTANCE.intervalSeconds.get(interval) * 1000));
        }
    }

    public static ArrayList<String> getLandBlocks() {
        return BetterSpawnerConfig.INSTANCE.blockCategories.get("land");
    }

    public static ArrayList<String> getWaterBlocks() {
        return BetterSpawnerConfig.INSTANCE.blockCategories.get("water");
    }

    public static ArrayList<String> getLavaBlocks() {
        return BetterSpawnerConfig.INSTANCE.blockCategories.get("lava");
    }

    public static ArrayList<String> getAirBlocks() {
        return BetterSpawnerConfig.INSTANCE.blockCategories.get("air");
    }

    public static ArrayList<String> getSeesSkyExceptionBlocks() {
        return BetterSpawnerConfig.INSTANCE.blockCategories.get("seesSkyException");
    }

    public static ArrayList<String> getBlockCategory(String blockCategory) {
        return BetterSpawnerConfig.INSTANCE.blockCategories.containsKey(blockCategory) ? BetterSpawnerConfig.INSTANCE.blockCategories.get(blockCategory) : new ArrayList<String>();
    }

    public static boolean doesBlockSeeSky(IBlockState state) {
        if (state == null) {
            return false;
        }
        return !state.getMaterial().blocksMovement() || BetterSpawnerConfig.getAirBlocks().contains(state.getBlock().getRegistryName().toString()) || BetterSpawnerConfig.getSeesSkyExceptionBlocks().contains(state.getBlock().getRegistryName().toString());
    }

    public static Stream<String> processBiomeIdToStream(String id) {
        return BetterSpawnerConfig.INSTANCE.biomeCategories.getOrDefault(id, Lists.newArrayList(id)).stream();
    }
}

