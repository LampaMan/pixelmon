/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.core.network.PixelmonPotion;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.potion.Potion;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.registries.IForgeRegistry;

@Mod.EventBusSubscriber
public class PixelmonPotions {
    public static PixelmonPotion attract = new PixelmonPotion("potion.attract", false, EnumDyeColor.GREEN);
    public static PixelmonPotion luck = new PixelmonPotion("potion.luck", false, EnumDyeColor.YELLOW);
    public static PixelmonPotion repel = new PixelmonPotion("potion.repel", false, EnumDyeColor.RED);

    @SubscribeEvent
    public static void registerPotions(RegistryEvent.Register<Potion> event) {
        IForgeRegistry<Potion> registry = event.getRegistry();
        registry.register(attract);
        registry.register(luck);
        registry.register(repel);
    }
}

