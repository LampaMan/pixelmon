/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.common.block.generic.GenericOre;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonItems;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraftforge.oredict.OreDictionary;

public class PixelmonOres {
    static void registerOres() {
        OreDictionary.registerOre("oreRuby", PixelmonBlocks.rubyOre);
        OreDictionary.registerOre("oreSapphire", PixelmonBlocks.sapphireOre);
        OreDictionary.registerOre("oreAmethyst", PixelmonBlocks.amethystOre);
        OreDictionary.registerOre("oreCrystal", PixelmonBlocks.crystalOre);
        OreDictionary.registerOre("oreSilicon", PixelmonBlocks.siliconOre);
        OreDictionary.registerOre("oreAluminum", PixelmonBlocks.bauxite);
        OreDictionary.registerOre("gemRuby", PixelmonItems.ruby);
        OreDictionary.registerOre("gemSapphire", PixelmonItems.sapphire);
        OreDictionary.registerOre("gemAmethyst", PixelmonItems.amethyst);
        OreDictionary.registerOre("gemCrystal", PixelmonItems.crystal);
        OreDictionary.registerOre("ingotAluminum", PixelmonItems.aluminiumIngot);
        OreDictionary.registerOre("blockRuby", PixelmonBlocks.rubyBlock);
        OreDictionary.registerOre("blockSapphire", PixelmonBlocks.sapphireBlock);
        OreDictionary.registerOre("blockAmethyst", PixelmonBlocks.amethystBlock);
        OreDictionary.registerOre("blockCrystal", PixelmonBlocks.crystalBlock);
        OreDictionary.registerOre("blockZCrystal", PixelmonBlocks.zCrystalOre);
    }

    static void populateOres() {
        ((GenericOre)PixelmonBlocks.rubyOre).addDrop(new ItemStack(PixelmonItems.ruby, 1));
        ((GenericOre)PixelmonBlocks.sapphireOre).addDrop(new ItemStack(PixelmonItems.sapphire, 1));
        ((GenericOre)PixelmonBlocks.amethystOre).addDrop(new ItemStack(PixelmonItems.amethyst, 1));
        ((GenericOre)PixelmonBlocks.crystalOre).addDrop(new ItemStack(PixelmonItems.crystal, 1));
    }

    static void registerFurnaceRecipes(FurnaceRecipes recipes) {
        recipes.addSmeltingRecipeForBlock(PixelmonBlocks.bauxite, new ItemStack(PixelmonItems.aluminiumIngot), 1.0f);
        recipes.addSmeltingRecipeForBlock(PixelmonBlocks.amethystOre, new ItemStack(PixelmonItems.amethyst), 2.0f);
        recipes.addSmeltingRecipeForBlock(PixelmonBlocks.crystalOre, new ItemStack(PixelmonItems.crystal), 2.0f);
        recipes.addSmeltingRecipeForBlock(PixelmonBlocks.rubyOre, new ItemStack(PixelmonItems.ruby), 2.0f);
        recipes.addSmeltingRecipeForBlock(PixelmonBlocks.sapphireOre, new ItemStack(PixelmonItems.sapphire), 2.0f);
        recipes.addSmeltingRecipeForBlock(PixelmonBlocks.siliconOre, new ItemStack(PixelmonItems.siliconItem), 2.0f);
        recipes.addSmeltingRecipeForBlock(PixelmonBlocks.zCrystalOre, new ItemStack(PixelmonItems.zIngot), 2.0f);
    }
}

