/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.common.item.ItemApricorn;
import com.pixelmongenerations.common.item.ItemApricornCooked;
import com.pixelmongenerations.core.enums.items.EnumApricorns;
import java.lang.reflect.Field;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;

public class PixelmonItemsApricorns {
    public static Item apricornBlack;
    public static Item apricornWhite;
    public static Item apricornPink;
    public static Item apricornGreen;
    public static Item apricornBlue;
    public static Item apricornYellow;
    public static Item apricornRed;
    public static Item apricornBlackCooked;
    public static Item apricornWhiteCooked;
    public static Item apricornPinkCooked;
    public static Item apricornGreenCooked;
    public static Item apricornBlueCooked;
    public static Item apricornYellowCooked;
    public static Item apricornRedCooked;

    public static void load() {
        apricornBlack = new ItemApricorn(EnumApricorns.Black);
        apricornWhite = new ItemApricorn(EnumApricorns.White);
        apricornPink = new ItemApricorn(EnumApricorns.Pink);
        apricornGreen = new ItemApricorn(EnumApricorns.Green);
        apricornBlue = new ItemApricorn(EnumApricorns.Blue);
        apricornYellow = new ItemApricorn(EnumApricorns.Yellow);
        apricornRed = new ItemApricorn(EnumApricorns.Red);
        apricornBlackCooked = new ItemApricornCooked(EnumApricorns.Black);
        apricornWhiteCooked = new ItemApricornCooked(EnumApricorns.White);
        apricornPinkCooked = new ItemApricornCooked(EnumApricorns.Pink);
        apricornGreenCooked = new ItemApricornCooked(EnumApricorns.Green);
        apricornBlueCooked = new ItemApricornCooked(EnumApricorns.Blue);
        apricornYellowCooked = new ItemApricornCooked(EnumApricorns.Yellow);
        apricornRedCooked = new ItemApricornCooked(EnumApricorns.Red);
    }

    public static Item getCookedApricorn(EnumApricorns type) {
        try {
            for (Field field : PixelmonItemsApricorns.class.getFields()) {
                Item item = (Item)field.get(null);
                if (!(item instanceof ItemApricornCooked) || ((ItemApricornCooked)item).apricorn != type) continue;
                return item;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return apricornBlackCooked;
    }

    public static Item getApricorn(EnumApricorns type) {
        try {
            for (Field field : PixelmonItemsApricorns.class.getFields()) {
                Item item = (Item)field.get(null);
                if (!(item instanceof ItemApricorn) || ((ItemApricorn)item).apricorn != type) continue;
                return item;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return apricornBlack;
    }

    static void registerFurnaceRecipes(FurnaceRecipes recipes) {
        recipes.addSmelting(apricornBlack, new ItemStack(apricornBlackCooked), 0.1f);
        recipes.addSmelting(apricornWhite, new ItemStack(apricornWhiteCooked), 0.1f);
        recipes.addSmelting(apricornPink, new ItemStack(apricornPinkCooked), 0.1f);
        recipes.addSmelting(apricornGreen, new ItemStack(apricornGreenCooked), 0.1f);
        recipes.addSmelting(apricornBlue, new ItemStack(apricornBlueCooked), 0.1f);
        recipes.addSmelting(apricornYellow, new ItemStack(apricornYellowCooked), 0.1f);
        recipes.addSmelting(apricornRed, new ItemStack(apricornRedCooked), 0.1f);
    }
}

