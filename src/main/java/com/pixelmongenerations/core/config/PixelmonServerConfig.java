/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.network.packetHandlers.ServerConfigList;

public class PixelmonServerConfig {
    public static int maxLevel = PixelmonConfig.maxLevel;
    public static boolean afkHandlerOn = PixelmonConfig.afkHandlerOn;
    public static int afkTimerActivateSeconds = PixelmonConfig.afkTimerActivateSeconds;
    public static float ridingSpeedMultiplier = PixelmonConfig.ridingSpeedMultiplier;

    public static void updateFromServer(ServerConfigList message) {
        maxLevel = message.maxLevel;
        ridingSpeedMultiplier = message.ridingSpeedMultiplier;
        afkHandlerOn = message.afkHandlerOn;
        afkTimerActivateSeconds = message.afkTimerActivateSeconds;
    }
}

