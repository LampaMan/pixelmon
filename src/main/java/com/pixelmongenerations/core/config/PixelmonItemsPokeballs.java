/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.common.item.ItemPokeball;
import com.pixelmongenerations.common.item.ItemPokeballDisc;
import com.pixelmongenerations.common.item.ItemPokeballLid;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import java.util.ArrayList;
import net.minecraft.item.Item;

public class PixelmonItemsPokeballs {
    public static Item pokeBall;
    public static Item greatBall;
    public static Item ultraBall;
    public static Item masterBall;
    public static Item levelBall;
    public static Item moonBall;
    public static Item friendBall;
    public static Item loveBall;
    public static Item safariBall;
    public static Item heavyBall;
    public static Item fastBall;
    public static Item repeatBall;
    public static Item timerBall;
    public static Item nestBall;
    public static Item netBall;
    public static Item diveBall;
    public static Item luxuryBall;
    public static Item healBall;
    public static Item duskBall;
    public static Item premierBall;
    public static Item sportBall;
    public static Item quickBall;
    public static Item lureBall;
    public static Item parkBall;
    public static Item cherishBall;
    public static Item gsBall;
    public static Item beastBall;
    public static Item dreamBall;
    public static ArrayList<Item> pokeballList;
    public static Item pokeBallLid;
    public static Item greatBallLid;
    public static Item ultraBallLid;
    public static Item levelBallLid;
    public static Item moonBallLid;
    public static Item friendBallLid;
    public static Item loveBallLid;
    public static Item safariBallLid;
    public static Item heavyBallLid;
    public static Item fastBallLid;
    public static Item repeatBallLid;
    public static Item timerBallLid;
    public static Item nestBallLid;
    public static Item netBallLid;
    public static Item diveBallLid;
    public static Item luxuryBallLid;
    public static Item healBallLid;
    public static Item duskBallLid;
    public static Item premierBallLid;
    public static Item sportBallLid;
    public static Item quickBallLid;
    public static Item lureBallLid;
    public static Item pokeBallDisc;
    public static Item greatBallDisc;
    public static Item ultraBallDisc;
    public static Item levelBallDisc;
    public static Item moonBallDisc;
    public static Item friendBallDisc;
    public static Item loveBallDisc;
    public static Item safariBallDisc;
    public static Item heavyBallDisc;
    public static Item fastBallDisc;
    public static Item repeatBallDisc;
    public static Item timerBallDisc;
    public static Item nestBallDisc;
    public static Item netBallDisc;
    public static Item diveBallDisc;
    public static Item luxuryBallDisc;
    public static Item healBallDisc;
    public static Item duskBallDisc;
    public static Item premierBallDisc;
    public static Item sportBallDisc;
    public static Item quickBallDisc;
    public static Item lureBallDisc;
    public static Item ironDisc;
    public static Item ironBase;
    public static Item aluDisc;
    public static Item aluBase;

    public static void load() {
        pokeBall = new ItemPokeball(EnumPokeball.PokeBall);
        ultraBall = new ItemPokeball(EnumPokeball.UltraBall);
        greatBall = new ItemPokeball(EnumPokeball.GreatBall);
        masterBall = new ItemPokeball(EnumPokeball.MasterBall);
        levelBall = new ItemPokeball(EnumPokeball.LevelBall);
        moonBall = new ItemPokeball(EnumPokeball.MoonBall);
        friendBall = new ItemPokeball(EnumPokeball.FriendBall);
        loveBall = new ItemPokeball(EnumPokeball.LoveBall);
        safariBall = new ItemPokeball(EnumPokeball.SafariBall);
        heavyBall = new ItemPokeball(EnumPokeball.HeavyBall);
        fastBall = new ItemPokeball(EnumPokeball.FastBall);
        repeatBall = new ItemPokeball(EnumPokeball.RepeatBall);
        timerBall = new ItemPokeball(EnumPokeball.TimerBall);
        nestBall = new ItemPokeball(EnumPokeball.NestBall);
        netBall = new ItemPokeball(EnumPokeball.NetBall);
        diveBall = new ItemPokeball(EnumPokeball.DiveBall);
        luxuryBall = new ItemPokeball(EnumPokeball.LuxuryBall);
        healBall = new ItemPokeball(EnumPokeball.HealBall);
        duskBall = new ItemPokeball(EnumPokeball.DuskBall);
        premierBall = new ItemPokeball(EnumPokeball.PremierBall);
        sportBall = new ItemPokeball(EnumPokeball.SportBall);
        parkBall = new ItemPokeball(EnumPokeball.ParkBall);
        quickBall = new ItemPokeball(EnumPokeball.QuickBall);
        lureBall = new ItemPokeball(EnumPokeball.LureBall);
        cherishBall = new ItemPokeball(EnumPokeball.CherishBall);
        gsBall = new ItemPokeball(EnumPokeball.GSBall);
        beastBall = new ItemPokeball(EnumPokeball.BeastBall);
        dreamBall = new ItemPokeball(EnumPokeball.DreamBall);
        pokeBallLid = new ItemPokeballLid(EnumPokeball.PokeBall);
        ultraBallLid = new ItemPokeballLid(EnumPokeball.UltraBall);
        greatBallLid = new ItemPokeballLid(EnumPokeball.GreatBall);
        levelBallLid = new ItemPokeballLid(EnumPokeball.LevelBall);
        moonBallLid = new ItemPokeballLid(EnumPokeball.MoonBall);
        friendBallLid = new ItemPokeballLid(EnumPokeball.FriendBall);
        loveBallLid = new ItemPokeballLid(EnumPokeball.LoveBall);
        safariBallLid = new ItemPokeballLid(EnumPokeball.SafariBall);
        heavyBallLid = new ItemPokeballLid(EnumPokeball.HeavyBall);
        fastBallLid = new ItemPokeballLid(EnumPokeball.FastBall);
        repeatBallLid = new ItemPokeballLid(EnumPokeball.RepeatBall);
        timerBallLid = new ItemPokeballLid(EnumPokeball.TimerBall);
        nestBallLid = new ItemPokeballLid(EnumPokeball.NestBall);
        netBallLid = new ItemPokeballLid(EnumPokeball.NetBall);
        diveBallLid = new ItemPokeballLid(EnumPokeball.DiveBall);
        luxuryBallLid = new ItemPokeballLid(EnumPokeball.LuxuryBall);
        healBallLid = new ItemPokeballLid(EnumPokeball.HealBall);
        duskBallLid = new ItemPokeballLid(EnumPokeball.DuskBall);
        premierBallLid = new ItemPokeballLid(EnumPokeball.PremierBall);
        sportBallLid = new ItemPokeballLid(EnumPokeball.SportBall);
        quickBallLid = new ItemPokeballLid(EnumPokeball.QuickBall);
        lureBallLid = new ItemPokeballLid(EnumPokeball.LureBall);
        pokeBallDisc = new ItemPokeballDisc(EnumPokeball.PokeBall);
        ultraBallDisc = new ItemPokeballDisc(EnumPokeball.UltraBall);
        greatBallDisc = new ItemPokeballDisc(EnumPokeball.GreatBall);
        levelBallDisc = new ItemPokeballDisc(EnumPokeball.LevelBall);
        moonBallDisc = new ItemPokeballDisc(EnumPokeball.MoonBall);
        friendBallDisc = new ItemPokeballDisc(EnumPokeball.FriendBall);
        loveBallDisc = new ItemPokeballDisc(EnumPokeball.LoveBall);
        safariBallDisc = new ItemPokeballDisc(EnumPokeball.SafariBall);
        heavyBallDisc = new ItemPokeballDisc(EnumPokeball.HeavyBall);
        fastBallDisc = new ItemPokeballDisc(EnumPokeball.FastBall);
        repeatBallDisc = new ItemPokeballDisc(EnumPokeball.RepeatBall);
        timerBallDisc = new ItemPokeballDisc(EnumPokeball.TimerBall);
        nestBallDisc = new ItemPokeballDisc(EnumPokeball.NestBall);
        netBallDisc = new ItemPokeballDisc(EnumPokeball.NetBall);
        diveBallDisc = new ItemPokeballDisc(EnumPokeball.DiveBall);
        luxuryBallDisc = new ItemPokeballDisc(EnumPokeball.LuxuryBall);
        healBallDisc = new ItemPokeballDisc(EnumPokeball.HealBall);
        duskBallDisc = new ItemPokeballDisc(EnumPokeball.DuskBall);
        premierBallDisc = new ItemPokeballDisc(EnumPokeball.PremierBall);
        sportBallDisc = new ItemPokeballDisc(EnumPokeball.SportBall);
        quickBallDisc = new ItemPokeballDisc(EnumPokeball.QuickBall);
        lureBallDisc = new ItemPokeballDisc(EnumPokeball.LureBall);
        ironBase = new PixelmonItem("iron_base").setCreativeTab(PixelmonCreativeTabs.pokeball);
        ironDisc = new PixelmonItem("iron_disc").setCreativeTab(PixelmonCreativeTabs.pokeball);
        aluBase = new PixelmonItem("aluminum_base").setCreativeTab(PixelmonCreativeTabs.pokeball);
        aluDisc = new PixelmonItem("aluminum_disc").setCreativeTab(PixelmonCreativeTabs.pokeball);
    }

    public static ItemPokeball getItemFromEnum(EnumPokeball type) {
        Item result = null;
        switch (type) {
            case DiveBall: {
                result = diveBall;
                break;
            }
            case DuskBall: {
                result = duskBall;
                break;
            }
            case FastBall: {
                result = fastBall;
                break;
            }
            case FriendBall: {
                result = friendBall;
                break;
            }
            case GreatBall: {
                result = greatBall;
                break;
            }
            case HealBall: {
                result = healBall;
                break;
            }
            case HeavyBall: {
                result = heavyBall;
                break;
            }
            case LevelBall: {
                result = levelBall;
                break;
            }
            case LoveBall: {
                result = loveBall;
                break;
            }
            case LuxuryBall: {
                result = luxuryBall;
                break;
            }
            case MasterBall: {
                result = masterBall;
                break;
            }
            case MoonBall: {
                result = moonBall;
                break;
            }
            case NestBall: {
                result = nestBall;
                break;
            }
            case NetBall: {
                result = netBall;
                break;
            }
            case PokeBall: {
                result = pokeBall;
                break;
            }
            case PremierBall: {
                result = premierBall;
                break;
            }
            case RepeatBall: {
                result = repeatBall;
                break;
            }
            case SafariBall: {
                result = safariBall;
                break;
            }
            case TimerBall: {
                result = timerBall;
                break;
            }
            case UltraBall: {
                result = ultraBall;
                break;
            }
            case SportBall: {
                result = sportBall;
                break;
            }
            case ParkBall: {
                result = parkBall;
                break;
            }
            case LureBall: {
                result = lureBall;
                break;
            }
            case QuickBall: {
                result = quickBall;
                break;
            }
            case CherishBall: {
                result = cherishBall;
                break;
            }
            case GSBall: {
                result = gsBall;
                break;
            }
            case BeastBall: {
                result = beastBall;
                break;
            }
            case DreamBall: {
                result = dreamBall;
                break;
            }
            default: {
                result = pokeBall;
            }
        }
        return (ItemPokeball)result;
    }

    public static ItemPokeballLid getLidFromEnum(EnumPokeball type) {
        Item result = null;
        switch (type) {
            case DiveBall: {
                result = diveBallLid;
                break;
            }
            case DuskBall: {
                result = duskBallLid;
                break;
            }
            case FastBall: {
                result = fastBallLid;
                break;
            }
            case FriendBall: {
                result = friendBallLid;
                break;
            }
            case GreatBall: {
                result = greatBallLid;
                break;
            }
            case HealBall: {
                result = healBallLid;
                break;
            }
            case HeavyBall: {
                result = heavyBallLid;
                break;
            }
            case MasterBall: {
                break;
            }
            case LevelBall: {
                result = levelBallLid;
                break;
            }
            case LoveBall: {
                result = loveBallLid;
                break;
            }
            case LuxuryBall: {
                result = luxuryBallLid;
                break;
            }
            case MoonBall: {
                result = moonBallLid;
                break;
            }
            case NestBall: {
                result = nestBallLid;
                break;
            }
            case NetBall: {
                result = netBallLid;
                break;
            }
            case PokeBall: {
                result = pokeBallLid;
                break;
            }
            case PremierBall: {
                result = premierBallLid;
                break;
            }
            case RepeatBall: {
                result = repeatBallLid;
                break;
            }
            case SafariBall: {
                result = safariBallLid;
                break;
            }
            case TimerBall: {
                result = timerBallLid;
                break;
            }
            case UltraBall: {
                result = ultraBallLid;
                break;
            }
            case SportBall: {
                result = sportBallLid;
                break;
            }
            case ParkBall: {
                break;
            }
            case LureBall: {
                result = lureBallLid;
                break;
            }
            case QuickBall: {
                result = quickBallLid;
            }
        }
        return (ItemPokeballLid)result;
    }

    public static ItemPokeballDisc getDiscFromEnum(EnumPokeball type) {
        Item result = null;
        switch (type) {
            case DiveBall: {
                result = diveBallDisc;
                break;
            }
            case DuskBall: {
                result = duskBallDisc;
                break;
            }
            case FastBall: {
                result = fastBallDisc;
                break;
            }
            case FriendBall: {
                result = friendBallDisc;
                break;
            }
            case GreatBall: {
                result = greatBallDisc;
                break;
            }
            case HealBall: {
                result = healBallDisc;
                break;
            }
            case HeavyBall: {
                result = heavyBallDisc;
                break;
            }
            case LevelBall: {
                result = levelBallDisc;
                break;
            }
            case LoveBall: {
                result = loveBallDisc;
                break;
            }
            case LuxuryBall: {
                result = luxuryBallDisc;
                break;
            }
            case MoonBall: {
                result = moonBallDisc;
                break;
            }
            case NestBall: {
                result = nestBallDisc;
                break;
            }
            case NetBall: {
                result = netBallDisc;
                break;
            }
            case PokeBall: {
                result = pokeBallDisc;
                break;
            }
            case PremierBall: {
                result = premierBallDisc;
                break;
            }
            case RepeatBall: {
                result = repeatBallDisc;
                break;
            }
            case SafariBall: {
                result = safariBallDisc;
                break;
            }
            case TimerBall: {
                result = timerBallDisc;
                break;
            }
            case UltraBall: {
                result = ultraBallDisc;
                break;
            }
            case SportBall: {
                result = sportBallDisc;
                break;
            }
            case LureBall: {
                result = lureBallDisc;
                break;
            }
            case QuickBall: {
                result = quickBallDisc;
                break;
            }
        }
        return (ItemPokeballDisc)result;
    }

    public static ArrayList<Item> initializePokeballList() {
        ArrayList<Item> list = new ArrayList<Item>();
        list.add(pokeBall);
        list.add(ultraBall);
        list.add(greatBall);
        list.add(levelBall);
        list.add(moonBall);
        list.add(friendBall);
        list.add(loveBall);
        list.add(safariBall);
        list.add(heavyBall);
        list.add(fastBall);
        list.add(repeatBall);
        list.add(timerBall);
        list.add(nestBall);
        list.add(netBall);
        list.add(diveBall);
        list.add(luxuryBall);
        list.add(healBall);
        list.add(duskBall);
        list.add(premierBall);
        list.add(sportBall);
        list.add(lureBall);
        list.add(quickBall);
        list.add(cherishBall);
        list.add(beastBall);
        list.add(dreamBall);
        return list;
    }

    public static ArrayList<Item> getPokeballListNoMaster() {
        if (pokeballList == null) {
            pokeballList = PixelmonItemsPokeballs.initializePokeballList();
        }
        return pokeballList;
    }

    public static ArrayList<Item> getPokeballListWithMaster() {
        if (pokeballList == null) {
            pokeballList = PixelmonItemsPokeballs.initializePokeballList();
        }
        ArrayList<Item> temp = pokeballList;
        temp.add(masterBall);
        temp.add(parkBall);
        return temp;
    }
}

