/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.core.config.PixelmonBlocks;
import net.minecraft.block.Block;
import net.minecraft.item.Item;

public class PixelmonRecipes {
    public static void addRecipes() {
    }

    private static void addPlateRecipe(Item plate, Item gem) {
    }

    private static void addGemRecipe(Item gem, Item other) {
    }

    private static void addBrailleBlockRecipe(int damage, String topRow, String middleRow, String bottomRow) {
        Block block = PixelmonBlocks.blockBraille;
        if (damage >= 16) {
            damage -= 16;
            block = PixelmonBlocks.blockBraille2;
        }
        topRow = topRow + " ";
        middleRow = middleRow + "S";
        bottomRow = bottomRow + " ";
    }
}

