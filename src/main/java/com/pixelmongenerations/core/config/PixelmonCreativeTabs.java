/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonItemsApricorns;
import com.pixelmongenerations.core.config.PixelmonItemsBadges;
import com.pixelmongenerations.core.config.PixelmonItemsCurryIngredients;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.config.PixelmonItemsPokeballs;
import com.pixelmongenerations.core.config.PixelmonItemsTMs;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PixelmonCreativeTabs {
    public static final CreativeTabs pokeball = new CreativeTabs("Pokeballs"){

        @SideOnly(value=Side.CLIENT)
        public ItemStack createIcon() {
            return new ItemStack(PixelmonItemsPokeballs.pokeBall);
        }

        public String getTranslationKey() {
            return I18n.translateToLocal("itemGroup.Pokeballs");
        }
    };
    public static final CreativeTabs keyItems = new CreativeTabs("Key Items"){

        @SideOnly(value=Side.CLIENT)
        public ItemStack createIcon() {
            return new ItemStack(PixelmonItems.greenBike);
        }

        public String getTranslationKey() {
            return I18n.translateToLocal("itemGroup.keyitems");
        }
    };
    public static final CreativeTabs legendItems = new CreativeTabs("Legendary Items"){

        @SideOnly(value=Side.CLIENT)
        public ItemStack createIcon() {
            return new ItemStack(PixelmonItems.mysticalOrb);
        }

        public String getTranslationKey() {
            return I18n.translateToLocal("itemGroup.legendaryitems");
        }
    };
    public static final CreativeTabs restoration = new CreativeTabs("Restoration"){

        @SideOnly(value=Side.CLIENT)
        public ItemStack createIcon() {
            return new ItemStack(PixelmonItems.potion);
        }

        public String getTranslationKey() {
            return I18n.translateToLocal("itemGroup.Restoration");
        }
    };
    public static final CreativeTabs natural = new CreativeTabs("Natural"){

        @SideOnly(value=Side.CLIENT)
        public ItemStack createIcon() {
            return new ItemStack(PixelmonItemsApricorns.apricornRed);
        }

        public String getTranslationKey() {
            return I18n.translateToLocal("itemGroup.Natural");
        }
    };
    public static final CreativeTabs held = new CreativeTabs("Held Items"){

        @SideOnly(value=Side.CLIENT)
        public ItemStack createIcon() {
            return new ItemStack(PixelmonItemsHeld.expShare);
        }

        public String getTranslationKey() {
            return I18n.translateToLocal("itemGroup.Held Items");
        }
    };
    public static final CreativeTabs tms = new CreativeTabs("TMs/TRs"){

        @SideOnly(value=Side.CLIENT)
        public ItemStack createIcon() {
            if (!PixelmonItemsTMs.TMs.isEmpty()) {
                return new ItemStack(PixelmonItemsTMs.TMs.get(0));
            }
            if (!PixelmonItemsTMs.TRs.isEmpty()) {
                return new ItemStack(PixelmonItemsTMs.TRs.get(0));
            }
            return new ItemStack(PixelmonItems.orb);
        }
    };
    public static final CreativeTabs utilityBlocks = new CreativeTabs("Utility Blocks"){

        @SideOnly(value=Side.CLIENT)
        public ItemStack createIcon() {
            return new ItemStack(Item.getItemFromBlock(PixelmonBlocks.warpPlate));
        }

        public String getTranslationKey() {
            return I18n.translateToLocal("itemGroup.utilityBlocks");
        }
    };
    public static final CreativeTabs badges = new CreativeTabs("Badges"){

        @SideOnly(value=Side.CLIENT)
        public ItemStack createIcon() {
            return new ItemStack(PixelmonItemsBadges.marshBadge);
        }

        public String getTranslationKey() {
            return I18n.translateToLocal("itemGroup.Badges");
        }
    };
    public static CreativeTabs curryIngredients = new CreativeTabs("curryIngredients"){

        @SideOnly(value=Side.CLIENT)
        public ItemStack createIcon() {
            return new ItemStack(PixelmonItemsCurryIngredients.bobsFoodTin);
        }

        public String getTranslationKey() {
            return I18n.translateToLocal("itemGroup.curryIngredient");
        }
    };
    public static final CreativeTabs tabPokeLoot = new CreativeTabs("tabPokeLoot"){

        @SideOnly(value=Side.CLIENT)
        public ItemStack createIcon() {
            return new ItemStack(PixelmonBlocks.pokeChest);
        }
    };
    public static final CreativeTabs buildingBlocks = new CreativeTabs("tabBuildingBlocks"){

        @SideOnly(value=Side.CLIENT)
        public ItemStack createIcon() {
            return new ItemStack(Item.getItemFromBlock(PixelmonBlocks.templeBrick));
        }

        public String getTranslationKey() {
            return I18n.translateToLocal("itemGroup.BuildingBlocks");
        }
    };
    public static final CreativeTabs decoration = new CreativeTabs("Decoration"){

        @SideOnly(value=Side.CLIENT)
        public ItemStack createIcon() {
            return new ItemStack(Item.getItemFromBlock(PixelmonBlocks.hdtvBlock));
        }

        public String getTranslationKey() {
            return I18n.translateToLocal("itemGroup.Decoration");
        }
    };
    public static final CreativeTabs pokedolls = new CreativeTabs("tabPokedolls"){

        @SideOnly(value=Side.CLIENT)
        public ItemStack createIcon() {
            return new ItemStack(Item.getItemFromBlock(PixelmonBlocks.krabbyPokedoll));
        }

        public String getTranslationKey() {
            return I18n.translateToLocal("itemGroup.Pokedolls");
        }
    };
    public static final CreativeTabs shrines = new CreativeTabs("tabShrines"){

        @SideOnly(value=Side.CLIENT)
        public ItemStack createIcon() {
            return new ItemStack(Item.getItemFromBlock(PixelmonBlocks.timespaceAltar));
        }

        public String getTranslationKey() {
            return I18n.translateToLocal("itemGroup.Shrines");
        }
    };
    public static final CreativeTabs valueables = new CreativeTabs("tabValuables"){

        @SideOnly(value=Side.CLIENT)
        public ItemStack createIcon() {
            return new ItemStack(PixelmonItems.relicVase);
        }

        public String getTranslationKey() {
            return I18n.translateToLocal("itemGroup.Valuables");
        }
    };
}

