/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value=RetentionPolicy.RUNTIME)
@Target(value={ElementType.FIELD})
public @interface Node {
    public String category();

    public String nameOverride() default "";

    public String comment() default "";

    public double minValue() default -2.147483648E9;

    public double maxValue() default 2.147483647E9;

    public Class<?> type() default Void.class;
}

