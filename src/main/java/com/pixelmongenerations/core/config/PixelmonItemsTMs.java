/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.item.ItemTM;
import com.pixelmongenerations.common.item.ItemTR;
import com.pixelmongenerations.core.Pixelmon;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;

public class PixelmonItemsTMs {
    public static List<ItemTM> TMs = new ArrayList<ItemTM>();
    public static List<ItemTM> TRs = new ArrayList<ItemTM>();

    static void load() {
        Pixelmon.LOGGER.info("Loading TM/TRs.");
        ArrayList usedTms = new ArrayList();
        ArrayList usedHms = new ArrayList();
        Attack.getMoves().stream().filter(attack -> attack.trIndex != -1 || attack.tmIndex != -1).forEach(attack -> {
            ItemTM item;
            if (attack.tmIndex != -1 && !usedTms.contains(attack.tmIndex)) {
                item = new ItemTM(attack.getUnlocalizedName(), attack.tmIndex, attack.attackType, false);
                TMs.add(item);
                usedTms.add(attack.tmIndex);
            }
            if (attack.trIndex != -1 && !usedHms.contains(attack.trIndex)) {
                item = new ItemTR(attack.getUnlocalizedName(), attack.trIndex, attack.attackType);
                TRs.add(item);
                usedHms.add(attack.trIndex);
            }
        });
        Pixelmon.LOGGER.info(String.format("Loaded %s TMs and %s TRs", TMs.size(), TRs.size()));
    }

    static void registerItems(RegistryEvent.Register<Item> event) {
        PixelmonItemsTMs.load();
        for (Item item : TMs) {
            event.getRegistry().register(item);
        }
        for (Item item : TRs) {
            event.getRegistry().register(item);
        }
    }

    static void registerRenderers() {
        try {
            for (ItemTM item : TMs) {
                ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation("pixelmon:tm_" + item.moveType.toString()));
            }
            for (ItemTM item : TRs) {
                ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation("pixelmon:tr_" + item.moveType.toString()));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}

