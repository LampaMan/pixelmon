/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.common.item.ItemCoveredFossil;
import com.pixelmongenerations.common.item.ItemFossil;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.items.EnumFossils;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class PixelmonItemsFossils {
    public static Item helixFossil;
    public static Item domeFossil;
    public static Item oldAmber;
    public static Item rootFossil;
    public static Item clawFossil;
    public static Item skullFossil;
    public static Item armorFossil;
    public static Item coverFossil;
    public static Item plumeFossil;
    public static Item jawFossil;
    public static Item sailFossil;
    public static Item birdFossil;
    public static Item fishFossil;
    public static Item dinoFossil;
    public static Item drakeFossil;
    public static Item dracovishFossil;
    public static Item dracozoltFossil;
    public static Item arctovishFossil;
    public static Item arctozoltFossil;
    public static Item helixFossilCovered;
    public static Item domeFossilCovered;
    public static Item oldAmberCovered;
    public static Item rootFossilCovered;
    public static Item clawFossilCovered;
    public static Item skullFossilCovered;
    public static Item armorFossilCovered;
    public static Item coverFossilCovered;
    public static Item plumeFossilCovered;
    public static Item jawFossilCovered;
    public static Item sailFossilCovered;
    public static Item birdFossilCovered;
    public static Item fishFossilCovered;
    public static Item dinoFossilCovered;
    public static Item drakeFossilCovered;
    public static Item fossilMachineTank;
    public static Item fossilMachineDisplay;
    public static Item fossilMachineTop;
    public static Item fossilMachineBase;

    public static void load() {
        helixFossil = new ItemFossil(EnumFossils.HELIX);
        domeFossil = new ItemFossil(EnumFossils.DOME);
        oldAmber = new ItemFossil(EnumFossils.OLD_AMBER);
        rootFossil = new ItemFossil(EnumFossils.ROOT);
        clawFossil = new ItemFossil(EnumFossils.CLAW);
        skullFossil = new ItemFossil(EnumFossils.SKULL);
        armorFossil = new ItemFossil(EnumFossils.ARMOR);
        coverFossil = new ItemFossil(EnumFossils.COVER);
        plumeFossil = new ItemFossil(EnumFossils.PLUME);
        jawFossil = new ItemFossil(EnumFossils.JAW);
        sailFossil = new ItemFossil(EnumFossils.SAIL);
        birdFossil = new ItemFossil(EnumFossils.BIRD);
        fishFossil = new ItemFossil(EnumFossils.FISH);
        dinoFossil = new ItemFossil(EnumFossils.DINO);
        drakeFossil = new ItemFossil(EnumFossils.DRAKE);
        dracovishFossil = new ItemFossil(EnumFossils.DRACOVISH);
        dracozoltFossil = new ItemFossil(EnumFossils.DRACOZOLT);
        arctovishFossil = new ItemFossil(EnumFossils.ARCTOVISH);
        arctozoltFossil = new ItemFossil(EnumFossils.ARCTOZOLT);
        fossilMachineTank = new PixelmonItem("fossil_machine_tank").setCreativeTab(CreativeTabs.MISC);
        fossilMachineDisplay = new PixelmonItem("fossil_machine_display").setCreativeTab(CreativeTabs.MISC);
        fossilMachineTop = new PixelmonItem("fossil_machine_top").setCreativeTab(CreativeTabs.MISC);
        fossilMachineBase = new PixelmonItem("fossil_machine_base").setCreativeTab(CreativeTabs.MISC);
        helixFossilCovered = new ItemCoveredFossil((ItemFossil)helixFossil, EnumFossils.HELIX);
        domeFossilCovered = new ItemCoveredFossil((ItemFossil)domeFossil, EnumFossils.DOME);
        oldAmberCovered = new ItemCoveredFossil((ItemFossil)oldAmber, EnumFossils.OLD_AMBER);
        rootFossilCovered = new ItemCoveredFossil((ItemFossil)rootFossil, EnumFossils.ROOT);
        clawFossilCovered = new ItemCoveredFossil((ItemFossil)clawFossil, EnumFossils.CLAW);
        skullFossilCovered = new ItemCoveredFossil((ItemFossil)skullFossil, EnumFossils.SKULL);
        armorFossilCovered = new ItemCoveredFossil((ItemFossil)armorFossil, EnumFossils.ARMOR);
        coverFossilCovered = new ItemCoveredFossil((ItemFossil)coverFossil, EnumFossils.COVER);
        plumeFossilCovered = new ItemCoveredFossil((ItemFossil)plumeFossil, EnumFossils.PLUME);
        jawFossilCovered = new ItemCoveredFossil((ItemFossil)jawFossil, EnumFossils.JAW);
        sailFossilCovered = new ItemCoveredFossil((ItemFossil)sailFossil, EnumFossils.SAIL);
        birdFossilCovered = new ItemCoveredFossil((ItemFossil)birdFossil, EnumFossils.BIRD);
        fishFossilCovered = new ItemCoveredFossil((ItemFossil)fishFossil, EnumFossils.FISH);
        dinoFossilCovered = new ItemCoveredFossil((ItemFossil)dinoFossil, EnumFossils.DINO);
        drakeFossilCovered = new ItemCoveredFossil((ItemFossil)drakeFossil, EnumFossils.DRAKE);
    }

    public static ItemStack getRandomFossil() {
        ArrayList<ItemCoveredFossil> enabledFossils = new ArrayList<ItemCoveredFossil>(ItemCoveredFossil.fossilList.size());
        for (ItemCoveredFossil fossil : ItemCoveredFossil.fossilList) {
            if (!PixelmonConfig.isGenerationEnabled(fossil.getGeneration())) continue;
            enabledFossils.add(fossil);
        }
        if (enabledFossils.isEmpty()) {
            return new ItemStack(Blocks.COBBLESTONE);
        }
        return new ItemStack((Item)RandomHelper.getRandomElementFromList(enabledFossils));
    }
}

