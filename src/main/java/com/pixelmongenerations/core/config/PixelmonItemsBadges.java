/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.common.item.ItemBadge;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.HashMap;
import net.minecraft.item.Item;

public class PixelmonItemsBadges {
    public static Item balanceBadge;
    public static Item basicBadge;
    public static Item beaconBadge;
    public static Item boulderBadge;
    public static Item cascadeBadge;
    public static Item coalBadge;
    public static Item cobbleBadge;
    public static Item dynamoBadge;
    public static Item earthBadge;
    public static Item featherBadge;
    public static Item fenBadge;
    public static Item fogBadge;
    public static Item forestBadge;
    public static Item glacierBadge;
    public static Item heatBadge;
    public static Item hiveBadge;
    public static Item icicleBadge;
    public static Item knuckleBadge;
    public static Item marshBadge;
    public static Item mindBadge;
    public static Item mineBadge;
    public static Item mineralBadge;
    public static Item plainBadge;
    public static Item rainBadge;
    public static Item rainbowBadge;
    public static Item relicBadge;
    public static Item risingBadge;
    public static Item soulBadge;
    public static Item stoneBadge;
    public static Item stormBadge;
    public static Item thunderBadge;
    public static Item volcanoBadge;
    public static Item zephyrBadge;
    public static Item boltBadge;
    public static Item freezeBadge;
    public static Item insectBadge;
    public static Item jetBadge;
    public static Item legendBadge;
    public static Item quakeBadge;
    public static Item toxicBadge;
    public static Item trioBadge;
    public static Item waveBadge;
    public static Item bugBadge;
    public static Item cliffBadge;
    public static Item rumbleBadge;
    public static Item plantBadge;
    public static Item voltageBadge;
    public static Item fairyBadge;
    public static Item psychicBadge;
    public static Item icebergBadge;
    public static Item spikeShellBadge;
    public static Item seaRubyBadge;
    public static Item jadeStarBadge;
    public static Item coralEyeBadge;
    public static Item freedomBadge;
    public static Item harmonyBadge;
    public static Item patienceBadge;
    public static Item prideBadge;
    public static Item tranquilityBadge;
    public static Item darkBadge;
    public static Item dragonBadge;
    public static Item fairyBadge2;
    public static Item fireBadge;
    public static Item grassBadge;
    public static Item iceBadge;
    public static Item rockBadge;
    public static Item waterBadge;
    public static Item fightingBadge;
    public static Item ghostBadge;
    public static Item shieldBadgeComplete;
    public static Item swordBadgeComplete;
    private static HashMap<EnumType, ArrayList<ItemBadge>> badgeList;

    public static void load() {
        balanceBadge = new ItemBadge("balance_badge");
        beaconBadge = new ItemBadge("beacon_badge");
        boulderBadge = new ItemBadge("boulder_badge");
        cascadeBadge = new ItemBadge("cascade_badge");
        coalBadge = new ItemBadge("coal_badge");
        cobbleBadge = new ItemBadge("cobble_badge");
        dynamoBadge = new ItemBadge("dynamo_badge");
        earthBadge = new ItemBadge("earth_badge");
        featherBadge = new ItemBadge("feather_badge");
        fenBadge = new ItemBadge("fen_badge");
        fogBadge = new ItemBadge("fog_badge");
        forestBadge = new ItemBadge("forest_badge");
        glacierBadge = new ItemBadge("glacier_badge");
        heatBadge = new ItemBadge("heat_badge");
        hiveBadge = new ItemBadge("hive_badge");
        icicleBadge = new ItemBadge("icicle_badge");
        knuckleBadge = new ItemBadge("knuckle_badge");
        marshBadge = new ItemBadge("marsh_badge");
        mindBadge = new ItemBadge("mind_badge");
        mineBadge = new ItemBadge("mine_badge");
        mineralBadge = new ItemBadge("mineral_badge");
        plainBadge = new ItemBadge("plain_badge");
        rainbowBadge = new ItemBadge("rainbow_badge");
        rainBadge = new ItemBadge("rain_badge");
        relicBadge = new ItemBadge("relic_badge");
        risingBadge = new ItemBadge("rising_badge");
        soulBadge = new ItemBadge("soul_badge");
        stoneBadge = new ItemBadge("stone_badge");
        stormBadge = new ItemBadge("storm_badge");
        thunderBadge = new ItemBadge("thunder_badge");
        volcanoBadge = new ItemBadge("volcano_badge");
        zephyrBadge = new ItemBadge("zephyr_badge");
        basicBadge = new ItemBadge("basic_badge");
        boltBadge = new ItemBadge("bolt_badge");
        freezeBadge = new ItemBadge("freeze_badge");
        insectBadge = new ItemBadge("insect_badge");
        jetBadge = new ItemBadge("jet_badge");
        legendBadge = new ItemBadge("legend_badge");
        quakeBadge = new ItemBadge("quake_badge");
        toxicBadge = new ItemBadge("toxic_badge");
        trioBadge = new ItemBadge("trio_badge");
        waveBadge = new ItemBadge("wave_badge");
        bugBadge = new ItemBadge("bug_badge");
        cliffBadge = new ItemBadge("cliff_badge");
        rumbleBadge = new ItemBadge("rumble_badge");
        plantBadge = new ItemBadge("plant_badge");
        voltageBadge = new ItemBadge("voltage_badge");
        fairyBadge = new ItemBadge("fairy_badge");
        psychicBadge = new ItemBadge("psychic_badge");
        icebergBadge = new ItemBadge("iceberg_badge");
        spikeShellBadge = new ItemBadge("spikeshell_badge");
        seaRubyBadge = new ItemBadge("searuby_badge");
        jadeStarBadge = new ItemBadge("jadestar_badge");
        coralEyeBadge = new ItemBadge("coraleye_badge");
        freedomBadge = new ItemBadge("freedom_badge");
        harmonyBadge = new ItemBadge("harmony_badge");
        patienceBadge = new ItemBadge("patience_badge");
        prideBadge = new ItemBadge("pride_badge");
        tranquilityBadge = new ItemBadge("tranquility_badge");
        darkBadge = new ItemBadge("dark_badge");
        dragonBadge = new ItemBadge("dragon_badge");
        fairyBadge2 = new ItemBadge("fairy_badge2");
        fireBadge = new ItemBadge("fire_badge");
        grassBadge = new ItemBadge("grass_badge");
        iceBadge = new ItemBadge("ice_badge");
        rockBadge = new ItemBadge("rock_badge");
        waterBadge = new ItemBadge("water_badge");
        fightingBadge = new ItemBadge("fighting_badge");
        ghostBadge = new ItemBadge("ghost_badge");
        shieldBadgeComplete = new ItemBadge("shield_badge_complete");
        swordBadgeComplete = new ItemBadge("sword_badge_complete");
    }

    public static ArrayList<ItemBadge> getBadgeList(EnumType type) {
        if (badgeList == null) {
            badgeList = new HashMap();
            PixelmonItemsBadges.addToTypeList(boulderBadge, EnumType.Rock);
            PixelmonItemsBadges.addToTypeList(cascadeBadge, EnumType.Water);
            PixelmonItemsBadges.addToTypeList(thunderBadge, EnumType.Electric);
            PixelmonItemsBadges.addToTypeList(rainbowBadge, EnumType.Grass);
            PixelmonItemsBadges.addToTypeList(soulBadge, EnumType.Poison);
            PixelmonItemsBadges.addToTypeList(marshBadge, EnumType.Psychic);
            PixelmonItemsBadges.addToTypeList(volcanoBadge, EnumType.Fire);
            PixelmonItemsBadges.addToTypeList(earthBadge, EnumType.Ground);
            PixelmonItemsBadges.addToTypeList(zephyrBadge, EnumType.Flying);
            PixelmonItemsBadges.addToTypeList(hiveBadge, EnumType.Bug);
            PixelmonItemsBadges.addToTypeList(plainBadge, EnumType.Normal);
            PixelmonItemsBadges.addToTypeList(fogBadge, EnumType.Ghost);
            PixelmonItemsBadges.addToTypeList(stormBadge, EnumType.Fighting);
            PixelmonItemsBadges.addToTypeList(mineralBadge, EnumType.Steel);
            PixelmonItemsBadges.addToTypeList(glacierBadge, EnumType.Ice);
            PixelmonItemsBadges.addToTypeList(risingBadge, EnumType.Dragon);
            PixelmonItemsBadges.addToTypeList(stoneBadge, EnumType.Rock);
            PixelmonItemsBadges.addToTypeList(knuckleBadge, EnumType.Fighting);
            PixelmonItemsBadges.addToTypeList(dynamoBadge, EnumType.Electric);
            PixelmonItemsBadges.addToTypeList(heatBadge, EnumType.Fire);
            PixelmonItemsBadges.addToTypeList(balanceBadge, EnumType.Normal);
            PixelmonItemsBadges.addToTypeList(featherBadge, EnumType.Flying);
            PixelmonItemsBadges.addToTypeList(mindBadge, EnumType.Psychic);
            PixelmonItemsBadges.addToTypeList(rainBadge, EnumType.Water);
            PixelmonItemsBadges.addToTypeList(coalBadge, EnumType.Rock);
            PixelmonItemsBadges.addToTypeList(forestBadge, EnumType.Grass);
            PixelmonItemsBadges.addToTypeList(cobbleBadge, EnumType.Fighting);
            PixelmonItemsBadges.addToTypeList(fenBadge, EnumType.Water);
            PixelmonItemsBadges.addToTypeList(relicBadge, EnumType.Ghost);
            PixelmonItemsBadges.addToTypeList(mineBadge, EnumType.Steel);
            PixelmonItemsBadges.addToTypeList(icicleBadge, EnumType.Ice);
            PixelmonItemsBadges.addToTypeList(beaconBadge, EnumType.Electric);
            PixelmonItemsBadges.addToTypeList(trioBadge, EnumType.Normal);
            PixelmonItemsBadges.addToTypeList(basicBadge, EnumType.Normal);
            PixelmonItemsBadges.addToTypeList(insectBadge, EnumType.Bug);
            PixelmonItemsBadges.addToTypeList(boltBadge, EnumType.Electric);
            PixelmonItemsBadges.addToTypeList(quakeBadge, EnumType.Ground);
            PixelmonItemsBadges.addToTypeList(jetBadge, EnumType.Flying);
            PixelmonItemsBadges.addToTypeList(freezeBadge, EnumType.Ice);
            PixelmonItemsBadges.addToTypeList(legendBadge, EnumType.Dragon);
            PixelmonItemsBadges.addToTypeList(toxicBadge, EnumType.Poison);
            PixelmonItemsBadges.addToTypeList(waveBadge, EnumType.Water);
            PixelmonItemsBadges.addToTypeList(bugBadge, EnumType.Bug);
            PixelmonItemsBadges.addToTypeList(cliffBadge, EnumType.Rock);
            PixelmonItemsBadges.addToTypeList(rumbleBadge, EnumType.Fighting);
            PixelmonItemsBadges.addToTypeList(plantBadge, EnumType.Grass);
            PixelmonItemsBadges.addToTypeList(voltageBadge, EnumType.Electric);
            PixelmonItemsBadges.addToTypeList(fairyBadge, EnumType.Fairy);
            PixelmonItemsBadges.addToTypeList(psychicBadge, EnumType.Psychic);
            PixelmonItemsBadges.addToTypeList(icebergBadge, EnumType.Ice);
            PixelmonItemsBadges.addToTypeList(spikeShellBadge, EnumType.Water);
            PixelmonItemsBadges.addToTypeList(seaRubyBadge, EnumType.Water);
            PixelmonItemsBadges.addToTypeList(jadeStarBadge, EnumType.Water);
            PixelmonItemsBadges.addToTypeList(coralEyeBadge, EnumType.Water);
            PixelmonItemsBadges.addToTypeList(darkBadge, EnumType.Dark);
            PixelmonItemsBadges.addToTypeList(dragonBadge, EnumType.Dragon);
            PixelmonItemsBadges.addToTypeList(fairyBadge2, EnumType.Fairy);
            PixelmonItemsBadges.addToTypeList(fireBadge, EnumType.Fire);
            PixelmonItemsBadges.addToTypeList(grassBadge, EnumType.Grass);
            PixelmonItemsBadges.addToTypeList(iceBadge, EnumType.Ice);
            PixelmonItemsBadges.addToTypeList(rockBadge, EnumType.Rock);
            PixelmonItemsBadges.addToTypeList(waterBadge, EnumType.Water);
            PixelmonItemsBadges.addToTypeList(fightingBadge, EnumType.Fighting);
            PixelmonItemsBadges.addToTypeList(ghostBadge, EnumType.Ghost);
        }
        return badgeList.get(type);
    }

    public static ArrayList<ItemBadge> getBadgeList(EnumType[] types) {
        ArrayList<ItemBadge> totalList = new ArrayList<ItemBadge>();
        for (int var4 = 0; var4 < types.length; ++var4) {
            EnumType type = types[var4];
            ArrayList<ItemBadge> badgeList = PixelmonItemsBadges.getBadgeList(type);
            for (ItemBadge badge : badgeList) {
                totalList.add(badge);
            }
        }
        return totalList;
    }

    public static ItemBadge getRandomBadge(EnumType ... types) {
        ArrayList<ItemBadge> badgeList = PixelmonItemsBadges.getBadgeList(types);
        return RandomHelper.getRandomElementFromList(badgeList);
    }

    private static void addToTypeList(Item badge, EnumType type) {
        ArrayList<ItemBadge> list = badgeList.containsKey(type) ? badgeList.get(type) : new ArrayList<>();
        list.add((ItemBadge)badge);
        badgeList.put(type, list);
    }
}

