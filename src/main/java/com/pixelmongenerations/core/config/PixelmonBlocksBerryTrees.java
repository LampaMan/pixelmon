/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.common.block.BlockBerryTree;
import com.pixelmongenerations.common.item.PixelmonItemBlock;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.enums.EnumBerry;
import java.util.ArrayList;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;

public class PixelmonBlocksBerryTrees {
    public static ArrayList<BlockBerryTree> berryBlocks = new ArrayList();

    static void load() {
        for (EnumBerry berry : EnumBerry.values()) {
            if (!berry.isImplemented) continue;
            berryBlocks.add((BlockBerryTree)new BlockBerryTree(berry).setTranslationKey("berrytree_" + berry.name().toLowerCase()));
        }
    }

    static void registerBlocks(RegistryEvent.Register<Block> event) {
        for (BlockBerryTree block : berryBlocks) {
            PixelmonBlocks.registerBlock(block, PixelmonItemBlock.class, "berrytree_" + block.getBerryType().name().toLowerCase(), new Object[0]);
        }
    }

    public static void registerModels() {
        for (BlockBerryTree block : berryBlocks) {
            ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(block), 0, new ModelResourceLocation("pixelmon:berrytree_" + block.getBerryType().name().toLowerCase(), "inventory"));
        }
    }
}

