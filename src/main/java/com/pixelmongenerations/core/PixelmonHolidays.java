/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core;

import com.pixelmongenerations.api.holiday.Holiday;
import com.pixelmongenerations.core.holiday.ChineseNewYears;
import com.pixelmongenerations.core.holiday.Christmas;
import com.pixelmongenerations.core.holiday.ChristmasTwo;
import com.pixelmongenerations.core.holiday.Halloween;
import com.pixelmongenerations.core.holiday.NewYears;
import com.pixelmongenerations.core.holiday.StartOfSpring;
import com.pixelmongenerations.core.holiday.Summer;
import com.pixelmongenerations.core.holiday.ValentinesDay;
import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Stream;

public class PixelmonHolidays {
    public static final String VALENTINES = "valentines";
    public static final String HALLOWEEN = "halloween";
    public static final String CHRISTMAS = "christmas";
    public static final String SUMMER = "summer";
    public static final String START_OF_SPRING = "startofspring";
    public static final String CHINESE_NEW_YEARS = "chinesenewyears";
    public static final String NEW_YEARS = "newyears";
    public static final String CHRISTMASTWO = "christmastwo";
    private static ArrayList<Holiday> holidays = new ArrayList();

    public static ArrayList<Holiday> getAllHolidays() {
        return holidays;
    }

    public static Stream<Holiday> getActiveHolidays() {
        return holidays.stream().filter(h -> h.isActive());
    }

    public static Optional<Holiday> getHolidayByName(String name) {
        for (Holiday holiday : holidays) {
            if (!holiday.getName().equalsIgnoreCase(name)) continue;
            return Optional.of(holiday);
        }
        return Optional.empty();
    }

    public static void register(Holiday holiday) {
        holidays.add(holiday);
    }

    public static boolean unregister(Holiday holiday) {
        return holidays.remove(holiday);
    }

    static {
        holidays.add(new ValentinesDay());
        holidays.add(new Halloween());
        holidays.add(new Christmas());
        holidays.add(new Summer());
        holidays.add(new StartOfSpring());
        holidays.add(new ChineseNewYears());
        holidays.add(new NewYears());
        holidays.add(new ChristmasTwo());
    }
}

