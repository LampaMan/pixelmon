/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.event;

import com.pixelmongenerations.common.item.armor.GenericArmor;
import com.pixelmongenerations.common.item.armor.armoreffects.IArmorEffect;
import com.pixelmongenerations.core.config.PixelmonItemsTools;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class PlayerFallListener {
    @SubscribeEvent
    public void onPlayerFall(LivingFallEvent event) {
        if (!event.getEntity().world.isRemote && event.getEntity() instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer)event.getEntity();
            if (player.getItemStackFromSlot(EntityEquipmentSlot.FEET) == null) {
                return;
            }
            ItemStack boots = player.getItemStackFromSlot(EntityEquipmentSlot.FEET);
            if (boots != null && boots.getItem() instanceof GenericArmor && ((GenericArmor)boots.getItem()).material == PixelmonItemsTools.DAWNSTONEARMORMAT && IArmorEffect.isWearingFullSet(player, PixelmonItemsTools.DAWNSTONEARMORMAT)) {
                event.setCanceled(true);
            }
        }
    }
}

