/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.event;

import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.event.entity.player.PlayerWakeUpEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class SleepHandler {
    @SubscribeEvent
    public void tickStart(PlayerWakeUpEvent event) {
        EntityPlayer player;
        Optional<PlayerStorage> optstorage;
        if (PixelmonConfig.bedsHealPokemon && !event.wakeImmediately() && !event.updateWorld() && event.shouldSetSpawn() && event.getEntityPlayer() instanceof EntityPlayerMP && (optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)(player = event.getEntityPlayer()))).isPresent()) {
            PlayerStorage storage = optstorage.get();
            storage.healAllPokemon(player.world);
        }
    }
}

