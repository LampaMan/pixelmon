/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.event;

import com.pixelmongenerations.common.item.ItemRepel;
import com.pixelmongenerations.core.network.ChatHandler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class RepelHandler {
    private static HashMap<UUID, Integer> repelExpiries = new HashMap();

    public static void onTick() {
        ArrayList<UUID> uuids = new ArrayList<UUID>(repelExpiries.keySet());
        for (UUID uuid : uuids) {
            int ticks = repelExpiries.get(uuid) - 1;
            if (ticks <= 0) {
                repelExpiries.remove(uuid);
                EntityPlayerMP player = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUUID(uuid);
                if (player == null) continue;
                ChatHandler.sendFormattedChat(player, TextFormatting.GRAY, "item.repel.expire", new Object[0]);
                continue;
            }
            repelExpiries.put(uuid, ticks);
        }
    }

    public static boolean hasRepel(UUID uuid) {
        return repelExpiries.containsKey(uuid);
    }

    public static void applyRepel(UUID uuid, ItemRepel.EnumRepel repel) {
        int ticks = repel.ticks;
        if (RepelHandler.hasRepel(uuid)) {
            ticks += repelExpiries.get(uuid).intValue();
        }
        repelExpiries.put(uuid, ticks);
    }
}

