/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.event;

import com.pixelmongenerations.core.event.PixelmonPlayerTracker;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EntityDeath {
    @SubscribeEvent
    public void onPlayerDeath(LivingDeathEvent event) {
        Entity eventEntity = event.getEntity();
        if (event.getEntity() instanceof EntityPlayerMP) {
            PixelmonPlayerTracker.removePlayer((EntityPlayerMP)eventEntity);
        }
    }
}

