/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.core.event;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.events.PixelmonJukeboxEvent;
import com.pixelmongenerations.api.events.pokemon.MaxFriendshipEvent;
import com.pixelmongenerations.api.pc.BackgroundRegistry;
import com.pixelmongenerations.api.pc.RecipeUnlockedBackground;
import com.pixelmongenerations.client.models.PixelmonModelRegistry;
import com.pixelmongenerations.client.util.PixelmonDRPC;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.block.BlockBerryTree;
import com.pixelmongenerations.common.block.decorative.BenchBlock;
import com.pixelmongenerations.common.block.decorative.CouchBlock;
import com.pixelmongenerations.common.block.enums.EnumBlockPos;
import com.pixelmongenerations.common.block.machines.BlockElevator;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.spawnmethod.BlockMeloettaMusicBox;
import com.pixelmongenerations.common.block.tileEntities.TileEntityBerryTree;
import com.pixelmongenerations.common.block.tileEntities.TileEntityMeloettaMusicBox;
import com.pixelmongenerations.common.block.tileEntities.TileEntityTimespaceAltar;
import com.pixelmongenerations.common.cosmetic.CosmeticData;
import com.pixelmongenerations.common.entity.EntityWishingStar;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.ItemPixelmonRecord;
import com.pixelmongenerations.common.item.ItemZoneWand;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonItemsCurryIngredients;
import com.pixelmongenerations.core.enums.EnumDynamaxItem;
import com.pixelmongenerations.core.enums.EnumMegaItem;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.battle.EnumBattleEndCause;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.packetHandlers.PixelmonServerInfo;
import com.pixelmongenerations.core.network.packetHandlers.UpdateCosmeticData;
import com.pixelmongenerations.core.storage.PCServer;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.function.BiPredicate;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEnchantmentTable;
import net.minecraft.block.BlockJukebox;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemRecord;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.client.event.sound.PlaySoundEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.UseHoeEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ForgeListener {
    private static List<EnumSpecies> fixFormsForPokemon = new ArrayList<EnumSpecies>(){
        {
            this.add(EnumSpecies.Basculin);
            this.add(EnumSpecies.Burmy);
            this.add(EnumSpecies.Castform);
            this.add(EnumSpecies.Cherrim);
            this.add(EnumSpecies.Darmanitan);
            this.add(EnumSpecies.Deoxys);
            this.add(EnumSpecies.Floette);
            this.add(EnumSpecies.Flabebe);
            this.add(EnumSpecies.Florges);
            this.add(EnumSpecies.Gastrodon);
            this.add(EnumSpecies.Genesect);
            this.add(EnumSpecies.Keldeo);
            this.add(EnumSpecies.Meloetta);
            this.add(EnumSpecies.MissingNo);
            this.add(EnumSpecies.Rotom);
            this.add(EnumSpecies.Sawsbuck);
            this.add(EnumSpecies.Servine);
            this.add(EnumSpecies.Shaymin);
            this.add(EnumSpecies.Shellos);
            this.add(EnumSpecies.Wormadam);
            this.add(EnumSpecies.Xerneas);
            this.add(EnumSpecies.Deerling);
            this.add(EnumSpecies.Furfrou);
            this.add(EnumSpecies.Toxtricity);
            this.add(EnumSpecies.Sinistea);
            this.add(EnumSpecies.Polteageist);
            this.add(EnumSpecies.Zygarde);
            this.add(EnumSpecies.Indeedee);
            this.add(EnumSpecies.Mewtwo);
        }
    };
    private static HashMap<String, Item> itemRemaps = new HashMap();

    @SubscribeEvent
    public void onSmack(LivingAttackEvent event) {
        if (event.getSource().getTrueSource() instanceof EntityPlayer) {
            EntityPlayer p = (EntityPlayer)event.getSource().getTrueSource();
            if (event.getEntity() instanceof EntityPixelmon) {
                EntityPixelmon pokemon = (EntityPixelmon)event.getEntity();
                if (pokemon.hasOwner()) {
                    event.setCanceled(true);
                    return;
                }
                if (pokemon.battleController != null) {
                    event.setCanceled(true);
                    return;
                }
                if (PixelmonConfig.canPokemonBeHit && PixelmonConfig.doPokemonAttackPlayers) {
                    pokemon.tasks.taskEntries.clear();
                    pokemon.tasks.addTask(1, new EntityPixelmon.AIMeleeAttack(pokemon, 1.1, true));
                    pokemon.targetTasks.addTask(1, new EntityPixelmon.AIHurtByTarget((EntityCreature)pokemon, false, new Class[0]));
                    pokemon.targetTasks.addTask(2, new EntityPixelmon.AIAttackPlayer((EntityCreature)pokemon, EntityPlayer.class, false));
                    pokemon.setAttackTarget(p);
                }
            }
        }
    }

    @SubscribeEvent
    public void onItemCrafted(PlayerEvent.ItemCraftedEvent event) {
        if (!event.player.world.isRemote) {
            BackgroundRegistry.getBackgrounds().stream().filter(background -> background instanceof RecipeUnlockedBackground).forEach(background -> ((RecipeUnlockedBackground)background).onItemCrafted((EntityPlayerMP)event.player, event.crafting));
        }
    }

    @SubscribeEvent
    public void onPlayerLogin(PlayerEvent.PlayerLoggedInEvent event) {
        EntityPlayer player = event.player;
        String uuid = event.player.getUniqueID().toString();
        if (player instanceof EntityPlayerMP) {
            PCServer.refreshBackgrounds((EntityPlayerMP)player);
        }
        this.broadcastRole(player);
        Pixelmon.NETWORK.sendTo(new PixelmonServerInfo(PixelmonConfig.serverKey), (EntityPlayerMP)player);
        FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers().forEach(listPlayer -> {
            Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)listPlayer);
            if (storageOpt.isPresent()) {
                CosmeticData cosmeticData = storageOpt.get().cosmeticData;
                Pixelmon.NETWORK.sendTo(new UpdateCosmeticData(cosmeticData), (EntityPlayerMP)player);
            }
        });
        BattleControllerBase bc = BattleRegistry.getBattle(player);
        if (bc != null) {
            bc.endBattle(EnumBattleEndCause.FORCE);
            BattleRegistry.deRegisterBattle(bc);
        }
    }

    private void broadcastRole(EntityPlayer player) {
        Pixelmon.INSTANCE.getPlayerRole(player.getUniqueID().toString()).ifPresent(role -> {
            if (player.getServer() == null) {
                return;
            }
            for (EntityPlayer entityPlayer : player.getServer().getPlayerList().getPlayers()) {
                entityPlayer.sendMessage(role.getJoinMessage(player.getDisplayNameString()));
            }
        });
    }

    @SubscribeEvent
    public void onFall(LivingFallEvent event) {
        EntityLivingBase entity = event.getEntityLiving();
        if (entity instanceof EntityWishingStar) {
            entity.setDead();
            entity.world.createExplosion(entity, entity.posX, entity.posY, entity.posZ, 5.0f, false);
            EntityItem wishingStar = new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(PixelmonItems.wishingStar, 1));
            wishingStar.setNoDespawn();
            entity.world.spawnEntity(wishingStar);
        }
    }

    @SubscribeEvent
    public void onPokemonSpawn(EntityJoinWorldEvent event) {
        if (event.getEntity() instanceof EntityPixelmon) {
            EntityPixelmon pokemon = (EntityPixelmon)event.getEntity();
            if (!Entity3HasStats.isAvailableGeneration(pokemon.getPokemonName())) {
                pokemon.setDead();
                event.setCanceled(true);
                return;
            }
            if (pokemon.getTrainer() != null) {
                pokemon.getEntityData().setBoolean("trainerPokemon", true);
            } else if (pokemon.getEntityData().hasKey("trainerPokemon")) {
                pokemon.setDead();
                return;
            }
            if (!event.getWorld().isRemote && pokemon != null && !pokemon.playerOwned && pokemon.getSpecies().equals((Object)EnumSpecies.Eternatus) && pokemon.getForm() == 0) {
                pokemon.setForm(1);
                pokemon.level.setLevel(100);
                pokemon.stats.HP = pokemon.stats.calculateHP(pokemon.baseStats, 100) * 5;
                pokemon.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(pokemon.stats.HP);
                pokemon.setHealth(pokemon.stats.HP);
                pokemon.updateStats();
            }
            if (pokemon.getForm() == -1 && fixFormsForPokemon.contains((Object)pokemon.getSpecies())) {
                pokemon.setForm(0, true);
            }
            if (pokemon.getSpecies() == EnumSpecies.Mewtwo && pokemon.getForm() == 3) {
                pokemon.setForm(0, true);
            }
        }
    }

    @SubscribeEvent
    public void onWorldUnload(WorldEvent.Unload event) {
        for (EntityPlayer player : event.getWorld().playerEntities) {
            BattleControllerBase bc = BattleRegistry.getBattle(player);
            if (bc == null) continue;
            bc.endBattle(EnumBattleEndCause.FORCE);
            BattleRegistry.deRegisterBattle(bc);
        }
    }

    @SubscribeEvent
    public void onNoFormSpawned(EntityJoinWorldEvent event) {
        if (!(event.getEntity() instanceof EntityPixelmon)) {
            return;
        }
        EntityPixelmon pixelmon = (EntityPixelmon)event.getEntity();
        if (pixelmon.getOwner() == null || !pixelmon.hasForms() || pixelmon.getForm() != -1 || PixelmonModelRegistry.getModel(pixelmon.getSpecies(), pixelmon.getFormEnum()) != null) {
            return;
        }
        pixelmon.setForm(Entity3HasStats.getRandomForm(pixelmon.getSpecies()));
        pixelmon.update(EnumUpdateType.Stats);
        PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)pixelmon.getOwner()).get();
        storage.update(pixelmon, EnumUpdateType.Stats);
    }

    @SubscribeEvent
    @SideOnly(value=Side.CLIENT)
    public void onSoundPlay(PlaySoundEvent event) {
        if (event.getSound().getCategory() != SoundCategory.MUSIC) {
            return;
        }
    }

    @SubscribeEvent(priority=EventPriority.HIGHEST)
    @SideOnly(value=Side.CLIENT)
    public void onGuiOpen(GuiOpenEvent event) {
        if (event.getGui() instanceof GuiMainMenu) {
            PixelmonDRPC.sendMainMenuRPC();
        }
    }

    @SubscribeEvent
    public void onBreakEvent(BlockEvent.BreakEvent event) {
        IBlockState state;
        EntityPlayer player = event.getPlayer();
        if (!event.getWorld().isRemote && ItemZoneWand.isValidWand(player)) {
            ItemStack heldStack = player.getHeldItem(EnumHand.MAIN_HAND);
            ItemZoneWand.setPosition(heldStack, ItemZoneWand.SelectPoint.One, event.getPos());
            event.setCanceled(true);
        }
        if ((state = event.getState()).getBlock() instanceof BenchBlock || state.getBlock() instanceof CouchBlock) {
            state.getBlock().onPlayerDestroy(event.getWorld(), event.getPos(), state);
        }
    }

    @SubscribeEvent
    public void onDropsEvent(BlockEvent.HarvestDropsEvent event) {
        EntityPlayer player = event.getHarvester();
        if (player != null && !event.getWorld().isRemote && event.getState() != null) {
            boolean isShears;
            String registryName = event.getState().getBlock().getRegistryName().toString();
            boolean bl = isShears = player.getHeldItemMainhand() != null && player.getHeldItemMainhand().getItem() == Items.SHEARS;
            if (registryName.contains("leaves") && !isShears && !event.isSilkTouching() && RandomHelper.getRandomNumberBetween(0, 99) == 0) {
                ArrayList leafDrops = Lists.newArrayList((Object[])new Item[]{PixelmonItems.sweetApple, PixelmonItems.galaricaTwig, PixelmonItems.tartApple, PixelmonItemsCurryIngredients.fancyApple});
                ItemStack dropStack = new ItemStack((Item)leafDrops.get(RandomHelper.getRandomNumberBetween(0, 3)));
                event.getDrops().add(dropStack);
            } else if (registryName.contains("_mushroom_block") && !event.isSilkTouching() && RandomHelper.getRandomNumberBetween(0, 99) == 0) {
                ItemStack dropStack = new ItemStack(RandomHelper.getRandomNumberBetween(0, 4) == 0 ? PixelmonItems.bigMushroom : PixelmonItems.tinyMushroom);
                event.getDrops().add(dropStack);
            }
            if (registryName.equals("pixelmon:crystal_ore") && PCServer.giveBackground((EntityPlayerMP)player, "box_crystal_caverns")) {
                ChatHandler.sendChat(player, "pixelmon.backgrounds.unlocked", "Crystal Caverns");
            }
        }
    }

    @SubscribeEvent
    public void onJump(LivingEvent.LivingJumpEvent event) {
        if (!(event.getEntity() instanceof EntityPlayerMP)) {
            return;
        }
        Block block = event.getEntity().world.getBlockState(event.getEntity().getPosition().down()).getBlock();
        if (!(block instanceof BlockElevator)) {
            return;
        }
        ((BlockElevator)block).takeElevator(event.getEntity().world, event.getEntity().getPosition().down(), (EntityPlayerMP)event.getEntity(), true);
    }

    @SubscribeEvent
    public void onFriendshipMax(MaxFriendshipEvent event) {
        for (ItemStack item : event.getPlayer().inventory.mainInventory) {
            int dmg;
            if (item.getItem() != PixelmonItems.rainbowWing || (dmg = item.getItemDamage()) >= item.getMaxDamage()) continue;
            item.setItemDamage(dmg + 1);
        }
    }

    @SubscribeEvent
    public void onEntityDamage(LivingAttackEvent event) {
        if ((event.getEntityLiving() instanceof EntityPlayerMP || event.getEntityLiving() instanceof EntityPixelmon) && event.getEntityLiving().dimension == 24 && event.getSource().damageType.equals("fall")) {
            event.setCanceled(true);
        }
    }

    @SubscribeEvent
    public void onItemInteract(PlayerInteractEvent.RightClickItem event) {
        EntityPlayer player = event.getEntityPlayer();
        ItemStack heldStack = event.getItemStack();
        if (player.world.isRemote || heldStack.isEmpty()) {
            return;
        }
        if (heldStack.getItem() == PixelmonItems.unchargedDynamaxBand) {
            TextComponentTranslation msg;
            Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
            if (!storageOpt.isPresent()) {
                return;
            }
            PlayerStorage storage = storageOpt.get();
            if (storage.dynamaxData.canEquipDynamaxItem()) {
                TextComponentTranslation msg2 = new TextComponentTranslation("dynamaxband.alreadyowned", new Object[0]);
                msg2.getStyle().setColor(TextFormatting.GRAY);
                player.sendMessage(msg2);
                return;
            }
            boolean consumedWishingStar = false;
            for (ItemStack itemStack : player.inventory.mainInventory) {
                if (itemStack.isEmpty() || itemStack.getItem() != PixelmonItems.wishingStar) continue;
                itemStack.shrink(1);
                consumedWishingStar = true;
                break;
            }
            if (consumedWishingStar) {
                if (storage.megaData.getMegaItem() == EnumMegaItem.BraceletORAS) {
                    storage.megaData.setMegaItem(EnumMegaItem.None, false);
                }
                storage.dynamaxData.setDynamaxItem(EnumDynamaxItem.DynamaxBand, false);
                storage.dynamaxData.setCanEquipDynamaxBand(true);
                heldStack.shrink(1);
                msg = new TextComponentTranslation("dynamaxband.unlocked", new Object[0]);
                msg.getStyle().setColor(TextFormatting.GRAY);
                player.sendMessage(msg);
            } else {
                msg = new TextComponentTranslation("dynamaxband.needwishingstar", new Object[0]);
                msg.getStyle().setColor(TextFormatting.GRAY);
                player.sendMessage(msg);
            }
        }
    }

    @SubscribeEvent
    public void onBlockInteract(PlayerInteractEvent.RightClickBlock event) {
        EntityPlayer player = event.getEntityPlayer();
        World world = player.getEntityWorld();
        BlockPos blockPos = event.getPos();
        IBlockState blockState = world.getBlockState(blockPos);
        Block block = blockState.getBlock();
        ItemStack itemStack = player.getHeldItem(event.getHand());
        TileEntity tileEntity = world.getTileEntity(blockPos);
        if (!world.isRemote && block instanceof BlockEnchantmentTable && PCServer.giveBackground((EntityPlayerMP)player, "box_mystic_library")) {
            ChatHandler.sendChat(player, "pixelmon.backgrounds.unlocked", "Mystic Library");
        }
        if (!world.isRemote && !itemStack.isEmpty() && block instanceof BlockShrine) {
            TileEntityTimespaceAltar altar;
            Item usedItem = itemStack.getItem();
            String registryId = usedItem.getRegistryName().getPath();
            if (block == PixelmonBlocks.regiceShrine && registryId.startsWith("shattered_ice_key")) {
                ChatHandler.sendChat(player, "pixelmon.shattered_ice_key_1.right_click", new Object[0]);
            }
            if (block == PixelmonBlocks.regirockShrine && registryId.startsWith("crumbled_rock_key")) {
                ChatHandler.sendChat(player, "pixelmon.crumbled_rock_key_1.right_click", new Object[0]);
            }
            if (block == PixelmonBlocks.registeelShrine && registryId.startsWith("rusty_iron_key")) {
                ChatHandler.sendChat(player, "pixelmon.rusty_iron_key_1.right_click", new Object[0]);
            }
            if (block == PixelmonBlocks.regielekiShrine && registryId.startsWith("discharged_eleki_key")) {
                ChatHandler.sendChat(player, "pixelmon.discharged_eleki_key_1.right_click", new Object[0]);
            }
            if (block == PixelmonBlocks.regidragoShrine && registryId.startsWith("fragmented_drago_key")) {
                ChatHandler.sendChat(player, "pixelmon.fragmented_drago_key_1.right_click", new Object[0]);
            }
            if (block == PixelmonBlocks.meloettaMusicBox && registryId.startsWith("shattered_relic_song")) {
                ChatHandler.sendChat(player, "pixelmon.shattered_relic_song_1.right_click", new Object[0]);
            }
            if (block == PixelmonBlocks.timespaceAltar && usedItem == PixelmonItems.redchain && (altar = BlockHelper.getTileEntity(TileEntityTimespaceAltar.class, world, blockPos)) != null && !altar.isSpawning()) {
                ChatHandler.sendChat(player, "pixelmon.red_chain.right_click", new Object[0]);
            }
            if (block == PixelmonBlocks.abundantShrine && usedItem == PixelmonItems.mirror) {
                ChatHandler.sendChat(player, "pixelmon.mirror.right_click", new Object[0]);
            }
            if (block == PixelmonBlocks.tapuShrine && usedItem == PixelmonItems.sparklingShard) {
                ChatHandler.sendChat(player, "pixelmon.sparkling_shard.right_click", new Object[0]);
            }
            if (block == PixelmonBlocks.darkraiAltar && usedItem == PixelmonItems.darkSoul) {
                ChatHandler.sendChat(player, "pixelmon.dark_soul.right_click", new Object[0]);
            }
            if (block == PixelmonBlocks.taoTrioShrine && usedItem == PixelmonItems.dragonSoul) {
                ChatHandler.sendChat(player, "pixelmon.dragon_soul.right_click", new Object[0]);
            }
            if (block == PixelmonBlocks.lugiaShrine && usedItem == PixelmonItems.orb) {
                ChatHandler.sendChat(player, "pixelmon.lugia_shrine.right_click", new Object[0]);
            }
            if (block == PixelmonBlocks.shrineUno && usedItem == PixelmonItems.orb) {
                ChatHandler.sendChat(player, "pixelmon.articuno_shrine.right_click", new Object[0]);
            }
            if (block == PixelmonBlocks.shrineDos && usedItem == PixelmonItems.orb) {
                ChatHandler.sendChat(player, "pixelmon.zapdos_shrine.right_click", new Object[0]);
            }
            if (block == PixelmonBlocks.shrineTres && usedItem == PixelmonItems.orb) {
                ChatHandler.sendChat(player, "pixelmon.moltres_shrine.right_click", new Object[0]);
            }
            if (block == PixelmonBlocks.generationsShrine && usedItem == PixelmonItems.generationsOrb && itemStack.getItemDamage() < 250) {
                ChatHandler.sendChat(player, "pixelmon.generations_shrine.right_click", new Object[0]);
            }
        }
        if (!world.isRemote && block instanceof BlockJukebox) {
            Item item;
            PixelmonJukeboxEvent jukeboxEvent = new PixelmonJukeboxEvent((EntityPlayerMP)player, blockPos);
            if (MinecraftForge.EVENT_BUS.post(jukeboxEvent)) {
                event.setCanceled(true);
                return;
            }
            boolean hasRecord = blockState.getValue(BlockJukebox.HAS_RECORD);
            if (!event.getItemStack().isEmpty() && (item = event.getItemStack().getItem()) instanceof ItemRecord && !hasRecord) {
                if (block instanceof BlockMeloettaMusicBox && item instanceof ItemPixelmonRecord && ((ItemPixelmonRecord)item).canSpawnMeloetta(itemStack) && world.getTileEntity(blockPos) instanceof TileEntityMeloettaMusicBox) {
                    TileEntityMeloettaMusicBox musicBox = (TileEntityMeloettaMusicBox)world.getTileEntity(blockPos);
                    if (musicBox.isSpawningMeloetta()) {
                        return;
                    }
                    ItemPixelmonRecord.setUsed(itemStack, true);
                    musicBox.spawnMeloetta(player, ((ItemPixelmonRecord)itemStack.getItem()).getDuration());
                }
                ((BlockJukebox)block).insertRecord(world, blockPos, blockState, itemStack);
                world.playEvent(null, 1010, blockPos, Item.getIdFromItem(item));
                itemStack.shrink(1);
                player.addStat(StatList.RECORD_PLAYED);
            } else if (tileEntity != null && hasRecord && tileEntity instanceof BlockJukebox.TileEntityJukebox) {
                if (tileEntity instanceof TileEntityMeloettaMusicBox && ((TileEntityMeloettaMusicBox)tileEntity).isSpawningMeloetta()) {
                    return;
                }
                BlockJukebox.TileEntityJukebox jukeboxTileEntity = (BlockJukebox.TileEntityJukebox)tileEntity;
                ItemStack itemstack = jukeboxTileEntity.getRecord();
                if (!itemstack.isEmpty()) {
                    world.playEvent(1010, blockPos, 0);
                    world.playRecord(blockPos, null);
                    jukeboxTileEntity.setRecord(ItemStack.EMPTY);
                    float f = 0.7f;
                    double offX = (double)(world.rand.nextFloat() * 0.7f) + (double)0.15f;
                    double offY = (double)(world.rand.nextFloat() * 0.7f) + 0.06000000238418579 + 0.6;
                    double offZ = (double)(world.rand.nextFloat() * 0.7f) + (double)0.15f;
                    ItemStack copyItemStack = itemstack.copy();
                    EntityItem entityitem = new EntityItem(world, (double)blockPos.getX() + offX, (double)blockPos.getY() + offY, (double)blockPos.getZ() + offZ, copyItemStack);
                    entityitem.setDefaultPickupDelay();
                    world.spawnEntity(entityitem);
                }
                blockState = blockState.withProperty(BlockJukebox.HAS_RECORD, Boolean.FALSE);
                world.setBlockState(blockPos, blockState, 2);
            }
            event.setCancellationResult(EnumActionResult.SUCCESS);
            event.setCanceled(true);
        }
    }

    @SubscribeEvent
    public void onHoeUse(UseHoeEvent event) {
        IBlockState state;
        if (!event.getWorld().isRemote && (state = event.getWorld().getBlockState(event.getPos())).getBlock() instanceof BlockBerryTree) {
            TileEntityBerryTree tileEntityBerryTree;
            BlockPos pos = event.getPos();
            if (state.getValue(BlockBerryTree.BLOCKPOS) == EnumBlockPos.TOP) {
                pos = pos.down();
            }
            if ((tileEntityBerryTree = (TileEntityBerryTree)event.getWorld().getTileEntity(pos)) != null) {
                if (!tileEntityBerryTree.removeWeeds()) {
                    ChatHandler.sendChat(event.getEntityPlayer(), "There are no weeds here", new Object[0]);
                } else {
                    event.setResult(Event.Result.ALLOW);
                }
            }
        }
    }

    public static List<BlockPos> searchForBlock(World world, BlockPos pos, int radius, int amount, BiPredicate<World, BlockPos> block) {
        ArrayList<BlockPos> states = new ArrayList<BlockPos>();
        for (int x = -radius; x < radius; ++x) {
            for (int y = -radius; y < radius; ++y) {
                for (int z = -radius; z < radius; ++z) {
                    BlockPos blockPos = pos.add(x, y, z);
                    if (!block.test(world, blockPos)) continue;
                    states.add(blockPos);
                    if (states.size() != amount) continue;
                    return states;
                }
            }
        }
        return states;
    }

    @SubscribeEvent
    public void fixItemMappings(RegistryEvent.MissingMappings<Item> e) {
        for (RegistryEvent.MissingMappings.Mapping missing : e.getMappings()) {
            Item item = itemRemaps.get(missing.key.toString());
            if (item == null) continue;
            missing.remap(item);
        }
    }

    static {
        itemRemaps.put("pixelmon:xl_exp_candy", PixelmonItems.expCandyXL);
    }
}

