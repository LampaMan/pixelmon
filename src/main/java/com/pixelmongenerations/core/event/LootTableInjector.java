/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableSet
 */
package com.pixelmongenerations.core.event;

import com.google.common.collect.ImmutableSet;
import java.util.Set;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.LootPool;
import net.minecraft.world.storage.loot.LootTable;
import net.minecraft.world.storage.loot.LootTableList;
import net.minecraftforge.event.LootTableLoadEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class LootTableInjector {
    public static final int MAX_POOLS = 3;
    public static final Set<String> LOOT_TABLES = ImmutableSet.of("inject/minecraft/chests/abandoned_mineshaft", "inject/minecraft/chests/desert_pyramid", "inject/minecraft/chests/end_city_treasure", "inject/minecraft/chests/igloo_chest", "inject/minecraft/chests/jungle_temple", "inject/minecraft/chests/nether_bridge", "inject/minecraft/chests/simple_dungeon", "inject/minecraft/chests/spawn_bonus_chest", "inject/minecraft/chests/stronghold_corridor", "inject/minecraft/chests/stronghold_crossing", "inject/minecraft/chests/stronghold_library", "inject/minecraft/chests/village_blacksmith", "inject/minecraft/chests/woodland_mansion", "chests/abundant_shrine", "chests/articuno_shrine", "chests/darkrai_shrine", "chests/groudon_shrine", "chests/kyogre_shrine", "chests/legendary_beasts_shrine", "chests/lugia_shrine", "chests/moltres_shrine", "chests/regice_shrine", "chests/regirock_shrine", "chests/registeel_shrine", "chests/regieleki_shrine", "chests/regidrago_shrine", "chests/tao_trio_shrine", "chests/tapu_shrine", "chests/zapdos_shrine", "lists/arceus_plates", "lists/berries", "lists/candy", "lists/fossils", "lists/gems", "lists/genesect_drives", "lists/mega_stones", "lists/memory_drives", "lists/mints", "lists/money", "lists/pokeballs", "lists/pokedolls", "lists/restoration", "lists/tms", "lists/trs", "lists/type_enhancements", "lists/ultrajunk", "lists/music_discs", "lists/z_crystals");

    public LootTableInjector() {
        for (String path : LOOT_TABLES) {
            LootTableList.register(new ResourceLocation("pixelmon", path));
        }
    }

    @SubscribeEvent
    public void lootTableLoaded(LootTableLoadEvent event) {
        ResourceLocation poolName = event.getName();
        String injectTablePath = "inject/" + poolName.getNamespace() + "/" + poolName.getPath();
        if (!LOOT_TABLES.contains(injectTablePath)) {
            return;
        }
        LootTable injectTable = event.getLootTableManager().getLootTableFromLocation(new ResourceLocation("pixelmon", injectTablePath));
        LootPool pool = injectTable.getPool("inject");
        if (pool != null) {
            event.getTable().addPool(pool);
        }
    }
}

