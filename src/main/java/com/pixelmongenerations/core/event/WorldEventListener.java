/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.event;

import com.pixelmongenerations.api.spawning.AbstractSpawner;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pokeballs.EntityOccupiedPokeball;
import com.pixelmongenerations.common.spawning.PixelmonSpawning;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Optional;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldEventListener;
import net.minecraft.world.World;

public class WorldEventListener
implements IWorldEventListener {
    @Override
    public void onEntityRemoved(Entity entityIn) {
        if (entityIn instanceof EntityPixelmon) {
            EntityPixelmon pixelmon = (EntityPixelmon)entityIn;
            if (pixelmon.hasOwner()) {
                Optional<PlayerStorage> optstorage = pixelmon.getStorage();
                if (optstorage.isPresent()) {
                    PlayerStorage storage = optstorage.get();
                    storage.setInWorld(pixelmon, false);
                }
            } else if (PixelmonSpawning.coordinator != null) {
                for (AbstractSpawner spawner : PixelmonSpawning.coordinator.spawners) {
                    if (!spawner.spawned.remove(entityIn.getUniqueID())) {
                        continue;
                    }
                    break;
                }
            }
        } else if (entityIn instanceof EntityOccupiedPokeball) {
            Optional<PlayerStorage> optstorage;
            EntityLivingBase thrower = ((EntityOccupiedPokeball)entityIn).getThrower();
            if (thrower == null) {
                return;
            }
            if (thrower instanceof EntityPlayerMP && (optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)thrower)).isPresent()) {
                PlayerStorage storage = optstorage.get();
                storage.thrownPokeball = null;
            }
        }
    }

    @Override
    public void notifyBlockUpdate(World worldIn, BlockPos pos, IBlockState oldState, IBlockState newState, int flags) {
    }

    @Override
    public void notifyLightSet(BlockPos pos) {
    }

    @Override
    public void markBlockRangeForRenderUpdate(int x1, int y1, int z1, int x2, int y2, int z2) {
    }

    @Override
    public void playSoundToAllNearExcept(EntityPlayer player, SoundEvent soundIn, SoundCategory category, double x, double y, double z, float volume, float pitch) {
    }

    @Override
    public void playRecord(SoundEvent soundIn, BlockPos pos) {
    }

    @Override
    public void spawnParticle(int particleID, boolean ignoreRange, double xCoord, double yCoord, double zCoord, double xSpeed, double ySpeed, double zSpeed, int ... parameters) {
    }

    @Override
    public void onEntityAdded(Entity entityIn) {
    }

    @Override
    public void broadcastSound(int soundID, BlockPos pos, int data) {
    }

    @Override
    public void playEvent(EntityPlayer player, int type, BlockPos blockPosIn, int data) {
    }

    @Override
    public void sendBlockBreakProgress(int breakerId, BlockPos pos, int progress) {
    }

    @Override
    public void spawnParticle(int id, boolean ignoreRange, boolean p_190570_3_, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed, int ... parameters) {
    }
}

