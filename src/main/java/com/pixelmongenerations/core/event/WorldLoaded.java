/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.event;

import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.event.WorldEventListener;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class WorldLoaded {
    @SubscribeEvent
    public void worldLoaded(WorldEvent.Load loadedArgs) {
        if (!PixelmonConfig.allowNonPixelmonMobs) {
            loadedArgs.getWorld().getGameRules().setOrCreateGameRule("doMobSpawning", "false");
        } else {
            loadedArgs.getWorld().getGameRules().setOrCreateGameRule("doMobSpawning", "true");
        }
        if (!loadedArgs.getWorld().isRemote) {
            loadedArgs.getWorld().addEventListener(new WorldEventListener());
        }
    }
}

