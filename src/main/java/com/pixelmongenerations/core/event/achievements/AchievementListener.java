/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.event.achievements;

import com.pixelmongenerations.common.achievement.PixelmonAchievements;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.common.item.ItemApricornCooked;
import com.pixelmongenerations.common.item.ItemItemFinder;
import com.pixelmongenerations.common.item.ItemPokeball;
import com.pixelmongenerations.common.item.armor.GenericArmor;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonItemsTools;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

public class AchievementListener {
    @SubscribeEvent
    public void onCrafting(PlayerEvent.ItemCraftedEvent event) {
        Item item = event.crafting.getItem();
        EntityPlayer player = event.player;
        if (!player.world.isRemote && (item == Item.getItemFromBlock(PixelmonBlocks.pc) || item == Item.getItemFromBlock(PixelmonBlocks.tradeMachine)) && RandomHelper.getRandomChance(30)) {
            DropItemHelper.giveItemStackToPlayer(player, new ItemStack(PixelmonItems.porygonPieces, 1, RandomHelper.getRandomNumberBetween(1, 4)));
        }
        if (item instanceof ItemItemFinder) {
            player.addStat(PixelmonAchievements.itemFinderChieve, 1);
        } else if (item instanceof ItemPokeball) {
            player.addStat(PixelmonAchievements.pokeballChieve, 1);
        } else if (item instanceof GenericArmor) {
            if (item == PixelmonItemsTools.rubyHelm) {
                player.addStat(PixelmonAchievements.teammagma, 1);
            } else if (item == PixelmonItemsTools.rubyBoots) {
                player.addStat(PixelmonAchievements.teammagma1, 1);
            } else if (item == PixelmonItemsTools.rubyLegs) {
                player.addStat(PixelmonAchievements.teammagma2, 1);
            } else if (item == PixelmonItemsTools.rubyPlate) {
                player.addStat(PixelmonAchievements.teammagma3, 1);
            } else if (item == PixelmonItemsTools.sapphireHelm) {
                player.addStat(PixelmonAchievements.teamaqua, 1);
            } else if (item == PixelmonItemsTools.sapphireBoots) {
                player.addStat(PixelmonAchievements.teamaqua1, 1);
            } else if (item == PixelmonItemsTools.sapphireLegs) {
                player.addStat(PixelmonAchievements.teamaqua2, 1);
            } else if (item == PixelmonItemsTools.sapphirePlate) {
                player.addStat(PixelmonAchievements.teamaqua3, 1);
            } else if (item == PixelmonItemsTools.rocketHelm) {
                player.addStat(PixelmonAchievements.teamrocket, 1);
            } else if (item == PixelmonItemsTools.rocketBoots) {
                player.addStat(PixelmonAchievements.teamrocket1, 1);
            } else if (item == PixelmonItemsTools.rocketLegs) {
                player.addStat(PixelmonAchievements.teamrocket2, 1);
            } else if (item == PixelmonItemsTools.rocketPlate) {
                player.addStat(PixelmonAchievements.teamrocket3, 1);
            } else if (item == PixelmonItemsTools.galacticHelm) {
                player.addStat(PixelmonAchievements.teamgalactic, 1);
            } else if (item == PixelmonItemsTools.galacticBoots) {
                player.addStat(PixelmonAchievements.teamgalactic1, 1);
            } else if (item == PixelmonItemsTools.galacticLegs) {
                player.addStat(PixelmonAchievements.teamgalactic2, 1);
            } else if (item == PixelmonItemsTools.galacticPlate) {
                player.addStat(PixelmonAchievements.teamgalactic3, 1);
            } else if (item == PixelmonItemsTools.plasmaHelm) {
                player.addStat(PixelmonAchievements.teamplasma, 1);
            } else if (item == PixelmonItemsTools.plasmaBoots) {
                player.addStat(PixelmonAchievements.teamplasma1, 1);
            } else if (item == PixelmonItemsTools.plasmaLegs) {
                player.addStat(PixelmonAchievements.teamplasma2, 1);
            } else if (item == PixelmonItemsTools.plasmaPlate) {
                player.addStat(PixelmonAchievements.teamplasma3, 1);
            }
        }
    }

    @SubscribeEvent
    public void onSmelting(PlayerEvent.ItemSmeltedEvent event) {
        EntityPlayer player = event.player;
        if (player instanceof EntityPlayerMP) {
            if (event.smelting.getItem() instanceof ItemApricornCooked) {
                player.addStat(PixelmonAchievements.apricornChieve, 1);
            } else if (event.smelting.getItem() == PixelmonItems.siliconItem) {
                player.addStat(PixelmonAchievements.getSilicon, 1);
            }
        }
    }
}

