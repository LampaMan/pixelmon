/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core;

import com.pixelmongenerations.core.enums.items.EnumFlutes;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.util.PixelMusic;
import java.util.Random;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.MusicTicker;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.GuiWinGame;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.WorldProviderEnd;
import net.minecraft.world.WorldProviderHell;

public class PixelmonMusicTicker
extends MusicTicker {
    private final Minecraft mc;
    private final Random rand;

    public PixelmonMusicTicker(Minecraft minecraft) {
        super(minecraft);
        this.mc = minecraft;
        this.rand = new Random();
    }

    @Override
    public void update() {
        MusicTicker.MusicType musicType = this.getAmbientMusicType();
        if (this.currentMusic != null) {
            if (!musicType.getMusicLocation().getSoundName().equals(this.currentMusic.getSoundLocation())) {
                this.mc.getSoundHandler().stopSound(this.currentMusic);
                this.timeUntilNextMusic = MathHelper.getInt(this.rand, 0, musicType.getMinDelay() / 2);
            }
            if (!this.mc.getSoundHandler().isSoundPlaying(this.currentMusic)) {
                this.currentMusic = null;
                this.timeUntilNextMusic = Math.min(MathHelper.getInt(this.rand, musicType.getMinDelay(), musicType.getMaxDelay()), this.timeUntilNextMusic);
            }
        }
        this.timeUntilNextMusic = Math.min(this.timeUntilNextMusic, musicType.getMaxDelay());
        if (this.currentMusic == null && this.timeUntilNextMusic-- <= 0) {
            this.playMusic(musicType);
        }
    }

    @Override
    public void playMusic(MusicTicker.MusicType requestedMusicType) {
        this.currentMusic = PositionedSoundRecord.getMusicRecord(requestedMusicType.getMusicLocation());
        this.mc.getSoundHandler().playSound(this.currentMusic);
        this.timeUntilNextMusic = Integer.MAX_VALUE;
    }

    public MusicTicker.MusicType getAmbientMusicType() {
        if (this.mc.currentScreen instanceof GuiWinGame) {
            return MusicTicker.MusicType.CREDITS;
        }
        if (this.mc.player != null) {
            MusicTicker.MusicType type = this.mc.world.provider.getMusicType();
            if (ClientProxy.battleManager.isBattling()) {
                type = PixelMusic.getCurrentBattleMusic();
            }
            if (type != null) {
                return type;
            }
            if (this.mc.player.world.provider instanceof WorldProviderHell) {
                return MusicTicker.MusicType.NETHER;
            }
            if (this.mc.player.world.provider instanceof WorldProviderEnd) {
                return this.mc.ingameGUI.getBossOverlay().shouldPlayEndBossMusic() ? MusicTicker.MusicType.END_BOSS : MusicTicker.MusicType.END;
            }
            return EnumFlutes.Sun.isCorrectTime(this.mc.player.world) ? PixelMusic.DAY : PixelMusic.NIGHT;
        }
        return MusicTicker.MusicType.MENU;
    }
}

