/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.apache.logging.log4j.LogManager
 */
package com.pixelmongenerations.core.handler;

import com.pixelmongenerations.core.config.PixelmonConfig;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.common.FMLCommonHandler;
import org.apache.logging.log4j.LogManager;

public class TimeHandler {
    static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    private static String time;
    private static String[] Time;
    private static int timeNow;
    private static long worldTime;

    public static void changeTime() {
        if (PixelmonConfig.useSystemWorldTime) {
            try {
                time = sdf.format(Calendar.getInstance().getTime());
                Time = time.split(":");
                timeNow = Integer.parseInt(Time[0] + Time[1] + Time[2].substring(0, Time[2].length() - 1)) + 18000;
                worldTime = timeNow;
                MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
                if (!server.getWorld((int)0).isRemote) {
                    for (int j = 0; j < server.worlds.length - 1; ++j) {
                        server.getWorld(j).setWorldTime(worldTime);
                    }
                }
            }
            catch (NullPointerException exception) {
                LogManager.getLogger((String)"Pixelmon").error(exception.toString());
            }
        }
    }
}

