/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.reflect.TypeToken
 *  com.google.gson.Gson
 *  com.google.gson.GsonBuilder
 */
package com.pixelmongenerations.core.proxy;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixelmongenerations.common.block.ranch.BreedingConditions;
import com.pixelmongenerations.core.Pixelmon;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.function.BooleanSupplier;
import java.util.function.Supplier;
import net.minecraft.util.ResourceLocation;

public class ConfigBase {
    public static Gson gsonInstance = new GsonBuilder().registerTypeAdapter(ResourceLocation.class, (Object)new ResourceLocation.Serializer()).disableHtmlEscaping().setPrettyPrinting().create();

    public static <T> T register(String name, Supplier<T> defaultValue, BooleanSupplier supplier, TypeToken<T> token) {
        Pixelmon.LOGGER.info("Registering" + name + ".");
        if (supplier.getAsBoolean()) {
            return ConfigBase.imporFrom(name, supplier, defaultValue, token);
        }
        return ConfigBase.retrieveFromAssets(name, supplier, defaultValue, token);
    }

    public static <T> T imporFrom(String name, BooleanSupplier supplier, Supplier<T> defaultValue, TypeToken<T> token) {
        File file = new File("./config/pixelmon/" + name + ".json");
        if (!file.exists()) {
            ConfigBase.retrieveFromAssets(name, supplier, defaultValue, token);
        }
        try {
            return (T)gsonInstance.fromJson((Reader)new FileReader(file), token.getType());
        }
        catch (Exception var6) {
            var6.printStackTrace();
            return defaultValue.get();
        }
    }

    public static <T> T retrieveFromAssets(String name, BooleanSupplier supplier, Supplier<T> defaultValue, TypeToken<T> token) {
        Object t = defaultValue.get();
        try {
            String primaryPath;
            File file;
            InputStream iStream = BreedingConditions.class.getResourceAsStream("/assets/pixelmon/" + name + ".json");
            try {
                t = gsonInstance.fromJson((Reader)new InputStreamReader(iStream), token.getType());
            }
            catch (Exception e) {
                Pixelmon.LOGGER.error("Couldn't load " + name + " JSON:");
                e.printStackTrace();
            }
            if (supplier.getAsBoolean() && !(file = new File(primaryPath = "./config/pixelmon/" + name + ".json")).exists()) {
                file.getParentFile().mkdirs();
                PrintWriter pw = new PrintWriter(file);
                pw.write(gsonInstance.toJson(t));
                pw.flush();
                pw.close();
            }
            try {
                iStream.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return (T) t;
    }
}

