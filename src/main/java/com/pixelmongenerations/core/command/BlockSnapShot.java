/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.common.block.MultiBlock;
import com.pixelmongenerations.common.entity.npcs.NPCChatting;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.pixelmon.EntityStatue;
import com.pixelmongenerations.common.item.PixelmonItemBlock;
import com.pixelmongenerations.common.world.Schematic;
import com.pixelmongenerations.common.world.gen.structure.util.StructureSnapshot;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.CommandChatHandler;
import com.pixelmongenerations.core.util.PixelBlockSnapshot;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import net.minecraft.block.Block;
import net.minecraft.block.BlockDoublePlant;
import net.minecraft.block.BlockRedstoneLight;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.datafix.fixes.TileEntityId;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

public class BlockSnapShot
extends CommandBase {
    private ArrayList<BlockPos> corners = new ArrayList();
    public static NBTTagCompound compound;

    @Override
    public String getName() {
        return "psnapshot";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/" + this.getName() + " <argument>";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (!(sender instanceof EntityPlayer) || args.length == 0) {
            CommandChatHandler.sendChat(sender, this.getUsage(sender), new Object[0]);
            return;
        }
        EntityPlayer player = (EntityPlayer)sender;
        World world = player.world;
        if (args[0].equals("set")) {
            this.corners.add(player.getPosition());
            if (this.corners.size() > 2) {
                this.corners.remove(0);
            }
            CommandChatHandler.sendChat(sender, "pixelmon.command.snapshot.corners", new Object[0]);
            for (BlockPos p : this.corners) {
                CommandChatHandler.sendChat(sender, p.toString(), new Object[0]);
            }
        } else if (args[0].equals("save")) {
            if (this.corners.size() < 2) {
                CommandChatHandler.sendChat(sender, "pixelmon.command.snapshot.nobounds", new Object[0]);
                return;
            }
            compound = new NBTTagCompound();
            BlockPos corner0 = this.corners.get(0);
            BlockPos corner1 = this.corners.get(1);
            int x0 = corner0.getX();
            int x1 = corner1.getX();
            int y0 = corner0.getY();
            int y1 = corner1.getY();
            int z0 = corner0.getZ();
            int z1 = corner1.getZ();
            int minX = Math.min(x0, x1);
            int maxX = Math.max(x0, x1);
            int minY = Math.min(y0, y1);
            int maxY = Math.max(y0, y1);
            int minZ = Math.min(z0, z1);
            int maxZ = Math.max(z0, z1);
            compound = Schematic.createFromWorld(world, new BlockPos(minX, minY, minZ), new BlockPos(maxX, maxY, maxZ)).saveToNBT();
            AxisAlignedBB aabb = new AxisAlignedBB(minX, minY, minZ, maxX, maxY, maxZ);
            List<NPCTrainer> trainers = world.getEntitiesWithinAABB(NPCTrainer.class, aabb);
            for (NPCTrainer nPCTrainer : trainers) {
                Pixelmon.LOGGER.info("Trainer position: " + (nPCTrainer.posX - (double)minX) + " " + (nPCTrainer.posY - (double)minY) + " " + (nPCTrainer.posZ - (double)minZ));
            }
            List<NPCChatting> chattingNPCS = world.getEntitiesWithinAABB(NPCChatting.class, aabb);
            for (NPCChatting npc : chattingNPCS) {
                Pixelmon.LOGGER.info("Chatting NPC position: " + (npc.posX - (double)minX) + " " + (npc.posY - (double)minY) + " " + (npc.posZ - (double)minZ));
            }
            if (args.length > 1) {
                String string = Pixelmon.modDirectory + "/schematics/";
                String filename = string + args[1] + ".schem";
                File saveFile = new File(filename);
                File saveDirPath = new File(string);
                if (!saveDirPath.exists()) {
                    saveDirPath.mkdirs();
                }
                try {
                    FileOutputStream f = new FileOutputStream(saveFile);
                    DataOutputStream s = new DataOutputStream(f);
                    CompressedStreamTools.write(compound, s);
                    s.close();
                    f.close();
                    CommandChatHandler.sendChat(sender, "pixelmon.command.snapshot.savefile", args[1]);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                CommandChatHandler.sendChat(sender, "pixelmon.command.snapshot.save", new Object[0]);
            }
        } else {
            if (args[0].equals("read") && args.length > 1) {
                String filename = Pixelmon.modDirectory + "/schematics/" + args[1] + ".schem";
                File saveFile = new File(filename);
                try {
                    compound = CompressedStreamTools.read(saveFile);
                    CommandChatHandler.sendChat(sender, "pixelmon.command.snapshot.load", args[1]);
                }
                catch (Exception e) {
                    e.printStackTrace();
                    CommandChatHandler.sendFormattedChat(sender, TextFormatting.RED, "pixelmon.command.snapshot.errorread", args[1]);
                    return;
                }
            }
            if (args[0].equals("place")) {
                sender.sendMessage(new TextComponentString("Place command is disabled for the mean time."));
            } else if (args[0].equals("convert")) {
                String baseDirectory = Pixelmon.modDirectory + "/schematics/";
                String baseFile = baseDirectory + args[1];
                String filename = baseFile + ".snapshot";
                File saveFile = new File(filename);
                try {
                    compound = CompressedStreamTools.read(saveFile);
                }
                catch (Exception e) {
                    e.printStackTrace();
                    CommandChatHandler.sendFormattedChat(sender, TextFormatting.RED, "pixelmon.command.snapshot.errorread", args[1]);
                    return;
                }
                StructureSnapshot snapshot = StructureSnapshot.readFromNBT(compound);
                BlockPos base = new BlockPos(compound.getInteger("baseX"), compound.getInteger("baseY"), compound.getInteger("baseZ"));
                Schematic schematic = new Schematic((short)snapshot.getWidth(), (short)snapshot.getHight(), (short)snapshot.getLenght());
                TileEntityId id = new TileEntityId();
                for (int x = 0; x < snapshot.getWidth(); ++x) {
                    for (int y = 0; y < snapshot.getHight(); ++y) {
                        for (int z = 0; z < snapshot.getLenght(); ++z) {
                            PixelBlockSnapshot block = snapshot.getBlockAt(x, y, z);
                            schematic.setBlockState(x, y, z, block.getReplacedBlock());
                            block.world = player.world;
                            TileEntity te = block.getTileEntity();
                            if (te == null) continue;
                            NBTTagCompound tag = id.fixTagCompound(te.serializeNBT());
                            tag.setInteger("x", tag.getInteger("x") - base.getX());
                            tag.setInteger("y", tag.getInteger("y") - base.getY());
                            tag.setInteger("z", tag.getInteger("z") - base.getZ());
                            schematic.tileEntities.add(tag);
                        }
                    }
                }
                schematic.entities = snapshot.getStatues();
                schematic.name = args[0];
                filename = baseFile + "-converted.schem";
                saveFile = new File(filename);
                File saveDirPath = new File(baseDirectory);
                if (!saveDirPath.exists()) {
                    saveDirPath.mkdirs();
                }
                try {
                    FileOutputStream f = new FileOutputStream(saveFile);
                    DataOutputStream s = new DataOutputStream(f);
                    CompressedStreamTools.write(schematic.saveToNBT(), s);
                    s.close();
                    f.close();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                CommandChatHandler.sendFormattedChat(sender, TextFormatting.GREEN, "pixelmon.command.snapshot.convert", args[1]);
            } else {
                CommandChatHandler.sendChat(sender, this.getUsage(sender), new Object[0]);
            }
        }
    }

    private void place(MinecraftServer server, ICommandSender sender, EntityPlayer player, World world) {
        BlockPos newPos;
        PixelBlockSnapshot snapshot;
        int z;
        int y;
        int x;
        if (compound == null) {
            CommandChatHandler.sendChat(sender, "pixelmon.command.snapshot.nosave", new Object[0]);
            return;
        }
        Schematic schematic = Schematic.loadFromNBT(compound);
        int width = schematic.width;
        int height = schematic.height;
        int length = schematic.length;
        BlockPos basePos = player.getPosition();
        int rotation = (int)Math.abs(player.getRotationYawHead()) % 360;
        EnumFacing facing = rotation < 45 ? EnumFacing.NORTH : (rotation < 135 ? EnumFacing.EAST : (rotation < 225 ? EnumFacing.SOUTH : (rotation < 315 ? EnumFacing.WEST : EnumFacing.NORTH)));
        ArrayList<BlockPos> multiblocks = new ArrayList<BlockPos>();
        for (x = 0; x < width; ++x) {
            for (y = 0; y < height; ++y) {
                for (z = 0; z < length; ++z) {
                    snapshot = new PixelBlockSnapshot(world, new BlockPos(x, y, z), schematic.getBlockState(x, y, z));
                    if (snapshot.getReplacedBlock().getMaterial() == Material.CIRCUITS || snapshot.getReplacedBlock().getBlock() instanceof BlockDoublePlant) continue;
                    newPos = this.getPos(x, y, z, basePos, facing);
                    snapshot.restoreToLocationWithRotation(newPos, facing);
                    if (!(world.getBlockState(newPos).getBlock() instanceof MultiBlock)) continue;
                    multiblocks.add(newPos);
                }
            }
        }
        for (x = 0; x < width; ++x) {
            for (y = 0; y < height; ++y) {
                for (z = 0; z < length; ++z) {
                    snapshot = new PixelBlockSnapshot(world, new BlockPos(x, y, z), schematic.getBlockState(x, y, z));
                    if (snapshot.getReplacedBlock().getMaterial() == Material.CIRCUITS) {
                        newPos = this.getPos(x, y, z, basePos, facing);
                        snapshot.restoreToLocationWithRotation(newPos, facing);
                        if (world.getBlockState(newPos).getBlock() instanceof MultiBlock) {
                            multiblocks.add(newPos);
                        }
                        if (!(world.getBlockState(newPos).getBlock() instanceof BlockRedstoneLight)) continue;
                    }
                    if (y + 1 >= height) continue;
                    PixelBlockSnapshot nextSnapshot = new PixelBlockSnapshot(world, new BlockPos(x, y, z), schematic.getBlockState(x, y, z));
                    if (!(snapshot.getReplacedBlock().getBlock() instanceof BlockDoublePlant) || !(nextSnapshot.getReplacedBlock().getBlock() instanceof BlockDoublePlant)) continue;
                    BlockPos newPos2 = this.getPos(x, y, z, basePos, facing);
                    snapshot.restoreToLocationWithRotation(newPos2, facing);
                }
            }
        }
        for (BlockPos newPos3 : multiblocks) {
            IBlockState state = world.getBlockState(newPos3);
            Block block = state.getBlock();
            EnumFacing blockRot = state.getValue(MultiBlock.FACING);
            PixelmonItemBlock.setMultiBlocksWidth(newPos3, blockRot, world, (MultiBlock)block, block, player);
        }
        for (NBTTagCompound tileEntityNBT : schematic.tileEntities) {
            BlockPos schematicPos = new BlockPos(tileEntityNBT.getInteger("x"), tileEntityNBT.getInteger("y"), tileEntityNBT.getInteger("z"));
            BlockPos pos = basePos.add(schematicPos);
            TileEntity tileEntity = world.getTileEntity(pos);
            if (tileEntity != null) {
                String blockTileEntityId;
                String schematicTileEntityId = tileEntityNBT.getString("id");
                if (schematicTileEntityId.equals(blockTileEntityId = TileEntity.getKey(tileEntity.getClass()).toString())) {
                    tileEntity.readFromNBT(tileEntityNBT);
                    tileEntity.setWorld(world);
                    tileEntity.setPos(pos);
                    tileEntity.markDirty();
                    continue;
                }
                throw new RuntimeException("Schematic contained TileEntity " + schematicTileEntityId + " at " + pos + " but the TileEntity of that block (" + world.getBlockState(pos) + ") must be " + blockTileEntityId);
            }
            throw new RuntimeException("Schematic contained TileEntity info at " + pos + " but the block there (" + world.getBlockState(pos) + ") has no TileEntity.");
        }
        for (NBTTagCompound entityNBT : schematic.entities) {
            NBTTagList posNBT = (NBTTagList)entityNBT.getTag("Pos");
            NBTTagList newPosNBT = new NBTTagList();
            newPosNBT.appendTag(new NBTTagDouble(posNBT.getDoubleAt(0) + (double)basePos.getX()));
            newPosNBT.appendTag(new NBTTagDouble(posNBT.getDoubleAt(1) + (double)basePos.getY()));
            newPosNBT.appendTag(new NBTTagDouble(posNBT.getDoubleAt(2) + (double)basePos.getZ()));
            NBTTagCompound adjustedEntityNBT = entityNBT.copy();
            adjustedEntityNBT.setTag("Pos", newPosNBT);
            adjustedEntityNBT.setUniqueId("UUID", UUID.randomUUID());
            EntityStatue statue = (EntityStatue)EntityList.createEntityFromNBT(adjustedEntityNBT, world);
            double posX = statue.posX;
            double posY = statue.posY;
            double posZ = statue.posZ;
            double x2 = 0.0;
            double y2 = 0.0;
            double z2 = 0.0;
            if (facing == EnumFacing.EAST) {
                x2 = posX + (double)basePos.getX();
                y2 = posY + (double)basePos.getY();
                z2 = posZ + (double)basePos.getZ();
            } else if (facing == EnumFacing.SOUTH) {
                x2 = -1.0 * posZ + 1.0 + (double)basePos.getX();
                y2 = posY + (double)basePos.getY();
                z2 = posX + (double)basePos.getZ();
            } else if (facing == EnumFacing.WEST) {
                x2 = -1.0 * posX + 1.0 + (double)basePos.getX();
                y2 = posY + (double)basePos.getY();
                z2 = -1.0 * posZ + 1.0 + (double)basePos.getZ();
            } else if (facing == EnumFacing.NORTH) {
                x2 = posZ + (double)basePos.getX();
                y2 = posY + (double)basePos.getY();
                z2 = -1.0 * posX + 1.0 + (double)basePos.getZ();
            }
            if (facing != EnumFacing.EAST) {
                if (facing == EnumFacing.WEST) {
                    statue.setRotation(statue.getRotation() + 180.0f);
                } else if (facing == EnumFacing.NORTH) {
                    statue.setRotation(statue.getRotation() - 90.0f);
                } else if (facing == EnumFacing.SOUTH) {
                    statue.setRotation(statue.getRotation() + 90.0f);
                }
                statue.prevRotationYaw = statue.rotationYaw;
            }
            statue.setPosition(x2, y2, z2);
            statue.setUniqueId(UUID.randomUUID());
            world.spawnEntity(statue);
        }
        for (NBTTagCompound statueTag : schematic.entities) {
            EntityStatue statue = new EntityStatue(world);
            statue.readFromNBT(statueTag);
            double posX = statue.posX;
            double posY = statue.posY;
            double posZ = statue.posZ;
            double x2 = 0.0;
            double y2 = 0.0;
            double z2 = 0.0;
            if (facing == EnumFacing.EAST) {
                x2 = posX + (double)basePos.getX();
                y2 = posY + (double)basePos.getY();
                z2 = posZ + (double)basePos.getZ();
            } else if (facing == EnumFacing.SOUTH) {
                x2 = -1.0 * posZ + 1.0 + (double)basePos.getX();
                y2 = posY + (double)basePos.getY();
                z2 = posX + (double)basePos.getZ();
            } else if (facing == EnumFacing.WEST) {
                x2 = -1.0 * posX + 1.0 + (double)basePos.getX();
                y2 = posY + (double)basePos.getY();
                z2 = -1.0 * posZ + 1.0 + (double)basePos.getZ();
            } else if (facing == EnumFacing.NORTH) {
                x2 = posZ + (double)basePos.getX();
                y2 = posY + (double)basePos.getY();
                z2 = -1.0 * posX + 1.0 + (double)basePos.getZ();
            }
            if (facing != EnumFacing.EAST) {
                if (facing == EnumFacing.WEST) {
                    statue.setRotation(statue.getRotation() + 180.0f);
                } else if (facing == EnumFacing.NORTH) {
                    statue.setRotation(statue.getRotation() - 90.0f);
                } else if (facing == EnumFacing.SOUTH) {
                    statue.setRotation(statue.getRotation() + 90.0f);
                }
                statue.prevRotationYaw = statue.rotationYaw;
            }
            statue.setPosition(x2, y2, z2);
            statue.setUniqueId(UUID.randomUUID());
            world.spawnEntity(statue);
        }
        CommandChatHandler.sendChat(sender, "pixelmon.command.snapshot.place", new Object[0]);
    }

    private BlockPos getPos(int x, int y, int z, BlockPos basePos, EnumFacing facing) {
        if (facing == EnumFacing.EAST) {
            return new BlockPos(x + basePos.getX(), y + basePos.getY(), z + basePos.getZ());
        }
        if (facing == EnumFacing.SOUTH) {
            return new BlockPos(-1 * z + basePos.getX(), y + basePos.getY(), x + basePos.getZ());
        }
        if (facing == EnumFacing.WEST) {
            return new BlockPos(-1 * x + basePos.getX(), y + basePos.getY(), -1 * z + basePos.getZ());
        }
        if (facing == EnumFacing.NORTH) {
            return new BlockPos(z + basePos.getX(), y + basePos.getY(), -1 * x + basePos.getZ());
        }
        return null;
    }
}

