/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.core.config.PixelmonItems;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.annotation.Nullable;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;

public class GiveServerItem
extends PixelmonCommand {
    public GiveServerItem() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "giveserveritem";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/giveserveritem <model name> <texture name>";
    }

    @Override
    public List<String> getAliases() {
        return Arrays.asList("gsi");
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        if (!(sender instanceof EntityPlayerMP)) {
            return;
        }
        if (args.length == 0 || args.length > 2) {
            sender.sendMessage(new TextComponentString(this.getUsage(sender)));
            return;
        }
        EntityPlayerMP player = (EntityPlayerMP)sender;
        ItemStack itemStack = new ItemStack(PixelmonItems.serverItem);
        NBTTagCompound compound = new NBTTagCompound();
        compound.setString("ServerItemModel", args[0]);
        compound.setString("ServerItemTexture", args[1]);
        itemStack.setTagCompound(compound);
        player.inventory.addItemStackToInventory(itemStack);
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos pos) {
        return Collections.emptyList();
    }
}

