/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.entity.EntityWishingStar;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import net.minecraft.command.CommandException;
import net.minecraft.command.EntitySelector;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;

public class FallingStar
extends PixelmonCommand {
    private Random random = new Random();

    public FallingStar() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "fallingstar";
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/fallingstar <player>";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            this.execute(sender, new String[]{sender.getName()});
        } else if (args.length > 1) {
            for (String player : args) {
                this.execute(sender, new String[]{player});
            }
        } else if (args[0].startsWith("@")) {
            List<EntityPlayerMP> list = EntitySelector.matchEntities(sender, args[0], EntityPlayerMP.class);
            for (EntityPlayerMP p : list) {
                this.execute(sender, new String[]{p.getName()});
            }
        } else {
            EntityPlayerMP player = this.getPlayer(args[0]);
            EntityWishingStar wishingStar = new EntityWishingStar(player.world);
            wishingStar.setPosition(player.getPosition().getX(), player.getPosition().getY() + 20, player.getPosition().getZ());
            player.world.spawnEntity(wishingStar);
            int direction = this.random.nextInt(4);
            if (direction == 0) {
                wishingStar.addVelocity(3.0, 0.0, 0.0);
            } else if (direction == 1) {
                wishingStar.addVelocity(-3.0, 0.0, 0.0);
            } else if (direction == 2) {
                wishingStar.addVelocity(0.0, 0.0, 3.0);
            } else {
                wishingStar.addVelocity(0.0, 0.0, -3.0);
            }
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        return args.length != 1 && args.length != 2 ? Collections.emptyList() : FallingStar.getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames());
    }
}

