/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.core.Pixelmon;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.world.biome.Biome;

public class GetBiomeData
extends PixelmonCommand {
    public GetBiomeData() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "getbiomedata";
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/getbiomedata";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        GetBiomeData.writeToFile("Biome Name, Temperature, Rainfall, RootHeight, HeightVariation, TopBlock, waterColorMultiplier, fillerBlock");
        for (Biome biome : Biome.REGISTRY) {
            String strData = "";
            try {
                strData = strData + biome.getRegistryName().getPath();
            }
            catch (Exception e) {
                strData = strData + "---";
            }
            strData = strData + ",";
            try {
                strData = strData + biome.getDefaultTemperature();
            }
            catch (Exception e) {
                strData = strData + "---";
            }
            strData = strData + ",";
            try {
                strData = strData + biome.getRainfall();
            }
            catch (Exception e) {
                strData = strData + "---";
            }
            strData = strData + ",";
            try {
                strData = strData + biome.getBaseHeight();
            }
            catch (Exception e) {
                strData = strData + "---";
            }
            strData = strData + ",";
            try {
                strData = strData + (biome.getBaseHeight() + biome.getHeightVariation());
            }
            catch (Exception e) {
                strData = strData + "---";
            }
            strData = strData + ",";
            try {
                strData = strData + biome.topBlock.getBlock().getTranslationKey();
            }
            catch (Exception e) {
                strData = strData + "---";
            }
            strData = strData + ",";
            try {
                strData = strData + biome.getWaterColorMultiplier();
            }
            catch (Exception e) {
                strData = strData + "---";
            }
            strData = strData + ",";
            try {
                strData = strData + biome.fillerBlock.getBlock().getLocalizedName();
            }
            catch (Exception e) {
                strData = strData + "---";
            }
            strData = strData + ",";
            GetBiomeData.writeToFile(strData);
        }
    }

    private static String getSaveFile() {
        return Pixelmon.modDirectory + "";
    }

    private static void writeToFile(String strToWrite) {
        File saveDirPath = new File(GetBiomeData.getSaveFile());
        if (!saveDirPath.exists()) {
            saveDirPath.mkdirs();
        }
        try {
            saveDirPath = new File(GetBiomeData.getSaveFile() + "/BiomeData.csv");
            FileWriter fw = new FileWriter(saveDirPath.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.append(strToWrite).append("\n");
            bw.close();
        }
        catch (IOException ioe) {
            System.out.println("[Error] Error saving data file for " + strToWrite);
            ioe.printStackTrace();
        }
    }
}

