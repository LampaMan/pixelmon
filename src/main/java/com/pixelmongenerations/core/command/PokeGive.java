/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.api.enums.ReceiveType;
import com.pixelmongenerations.api.events.PixelmonReceivedEvent;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.achievement.PixelmonAchievements;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Collections;
import java.util.List;
import net.minecraft.command.CommandException;
import net.minecraft.command.EntitySelector;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.MinecraftForge;

public class PokeGive
extends PixelmonCommand {
    public PokeGive() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "pokegive";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/pokegive <player> <pokemon>";
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        block10: {
            if (args.length >= 2) {
                if (args[0].startsWith("@")) {
                    List<EntityPlayerMP> list = EntitySelector.matchEntities(sender, args[0], EntityPlayerMP.class);
                    for (EntityPlayerMP p : list) {
                        args[0] = p.getName();
                        this.execute(sender, args);
                    }
                    return;
                }
                try {
                    EntityPlayerMP player = this.getPlayer(args[0]);
                    if (player == null) {
                        this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
                        return;
                    }
                    PokemonSpec spec = PokemonSpec.from(args);
                    spec.boss = null;
                    EntityPixelmon pokemon = spec.create(player.world);
                    pokemon.caughtBall = pokemon.caughtBall == null ? EnumPokeball.PokeBall : pokemon.caughtBall;
                    pokemon.friendship.initFromCapture();
                    pokemon.originalTrainer = player.getName();
                    pokemon.originalTrainerUUID = player.getUniqueID().toString();
                    pokemon.setOwnerId(player.getUniqueID());
                    PlayerStorage storage = this.getPlayerStorage(player);
                    if (storage != null) {
                        MinecraftForge.EVENT_BUS.post(new PixelmonReceivedEvent(player, ReceiveType.Command, pokemon));
                        if (BattleRegistry.getBattle(player) == null) {
                            storage.addToParty(pokemon);
                        } else {
                            storage.addToPC(pokemon);
                        }
                        PixelmonAchievements.pokedexChieves(player);
                        String playerName = player.getDisplayName().getUnformattedText();
                        String pokemonName = pokemon.getLocalizedName();
                        this.sendMessage(sender, "pixelmon.command.give.givesuccess", playerName, pokemonName);
                        PokeGive.notifyCommandListener(sender, (ICommand)this, 0, "pixelmon.command.give.notifygive", sender.getName(), playerName, pokemonName);
                        pokemon.update(EnumUpdateType.values());
                        break block10;
                    }
                    this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
                }
                catch (NullPointerException npe) {
                    this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.notingame", args[1]);
                }
            } else {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalid", new Object[0]);
                this.sendMessage(sender, TextFormatting.RED, this.getUsage(sender), new Object[0]);
            }
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        if (args.length == 1) {
            return this.tabCompleteUsernames(args);
        }
        if (args.length == 2) {
            return this.tabCompletePokemon(args);
        }
        return Collections.emptyList();
    }
}

