/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableList
 */
package com.pixelmongenerations.core.command;

import com.google.common.collect.ImmutableList;
import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import com.pixelmongenerations.core.storage.deepstorage.DeepStorage;
import com.pixelmongenerations.core.storage.deepstorage.DeepStorageManager;
import java.util.ArrayList;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class DeepStorageCmd
extends PixelmonCommand {
    public DeepStorageCmd() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "deepstorage";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/deepstorage <collect | view [player]>";
    }

    @Override
    protected void execute(ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            this.sendMessage(sender, "Not enough arguments. " + this.getUsage(sender), new Object[0]);
            return;
        }
        if (args[0].toLowerCase().equals("collect")) {
            if (!(sender instanceof EntityPlayerMP)) {
                this.sendMessage(sender, "You have to be a player to collect deep storage Pok\u00e9mon. How else would you have any.", new Object[0]);
                return;
            }
            EntityPlayerMP player = (EntityPlayerMP)sender;
            if (!DeepStorageManager.hasPokemonInDeepStorage(player.getUniqueID())) {
                this.sendMessage(sender, "You don't have any Pok\u00e9mon in deep storage.", new Object[0]);
                return;
            }
            DeepStorage deepStorage = DeepStorageManager.getOrCreateDeepStorage(player.getUniqueID());
            if (deepStorage == null) {
                this.sendMessage(sender, "An unknown problem was encountered while trying to retrieve deep storage.", new Object[0]);
                return;
            }
            PlayerComputerStorage pc = PixelmonStorage.computerManager.getPlayerStorage(player);
            if (pc == null) {
                this.sendMessage(sender, "An unknown problem was encountered loading regular PC storage. Try again.", new Object[0]);
                return;
            }
            ArrayList<NBTTagCompound> retrieved = deepStorage.tryRetrieve();
            ArrayList restored = new ArrayList();
            retrieved.removeIf(nbt -> {
                if (pc.addToComputer((NBTTagCompound)nbt)) {
                    return restored.add(nbt);
                }
                return false;
            });
            if (!retrieved.isEmpty()) {
                this.sendMessage(sender, retrieved.size() + " were unable to be added to the PC due to lack of space. Make room and try again.", new Object[0]);
                retrieved.removeAll(restored);
                for (NBTTagCompound nbt2 : retrieved) {
                    deepStorage.put(nbt2);
                }
            }
            DeepStorageManager.save(player.getUniqueID());
            this.sendMessage(sender, TextFormatting.DARK_GREEN, "Successfully restored " + restored.size() + " Pok\u00e9mon from deep storage!", new Object[0]);
            return;
        }
        if (!args[0].toLowerCase().equals("view")) {
            this.sendMessage(sender, "Unknown argument: " + args[0] + ". " + this.getUsage(sender), new Object[0]);
            return;
        }
        if (args.length == 1 && !(sender instanceof EntityPlayerMP)) {
            this.sendMessage(sender, "To view a deep storage, you need to specify a player or be a player.", new Object[0]);
            this.sendMessage(sender, this.getUsage(sender), new Object[0]);
            return;
        }
        EntityPlayerMP player = null;
        if (sender instanceof EntityPlayerMP) {
            player = (EntityPlayerMP)sender;
        }
        if (args.length > 1 && (player = this.getPlayer(args[1])) == null) {
            this.sendMessage(sender, "Unknown player: " + args[1], new Object[0]);
            return;
        }
        if (!DeepStorageManager.hasPokemonInDeepStorage(player.getUniqueID())) {
            this.sendMessage(sender, "No Pok\u00e9mon in deep storage for " + player.getName(), new Object[0]);
            return;
        }
        DeepStorage deepStorage = DeepStorageManager.getOrCreateDeepStorage(player.getUniqueID());
        ImmutableList<NBTTagCompound> list = deepStorage.getArchivedPokemon();
        String message = "";
        if (list.isEmpty()) {
            message = "No Pok\u00e9mon in deep storage for " + player.getName();
            DeepStorageManager.save(player.getUniqueID());
        } else {
            for (NBTTagCompound nbt3 : list) {
                message = EnumSpecies.hasPokemonAnyCase(nbt3.getString("Name")) ? message + (Object)((Object)TextFormatting.DARK_GREEN) : message + (Object)((Object)TextFormatting.GRAY);
                message = message + nbt3.getString("Name") + (Object)((Object)TextFormatting.WHITE) + ", ";
            }
            message = message.substring(0, message.length() - 2);
        }
        sender.sendMessage(new TextComponentString(message));
    }
}

