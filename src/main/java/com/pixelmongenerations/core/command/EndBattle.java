/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.battle.EnumBattleEndCause;
import com.pixelmongenerations.core.network.packetHandlers.battles.EndSpectate;
import com.pixelmongenerations.core.network.packetHandlers.battles.ExitBattle;
import java.util.Collections;
import java.util.List;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;

public class EndBattle
extends PixelmonCommand {
    static final String OTHER_NODE = "pixelmon.command.admin.endbattle";

    public EndBattle() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "endbattle";
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/endbattle [player]";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) {
        String username = args.length == 0 ? sender.getName() : args[0];
        String string = username;
        if (!sender.getName().equalsIgnoreCase(username) && !sender.canUseCommand(this.getRequiredPermissionLevel(), OTHER_NODE)) {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.endbattle.permissionother", new Object[0]);
            return;
        }
        EntityPlayerMP player = this.getPlayer(username);
        if (player == null) {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
            return;
        }
        BattleControllerBase bc = BattleRegistry.getBattle(player);
        if (bc != null) {
            if (bc.removeSpectator(player)) {
                Pixelmon.NETWORK.sendTo(new EndSpectate(), player);
            } else {
                bc.endBattle(EnumBattleEndCause.FORCE);
                this.sendMessage(sender, TextFormatting.GREEN, "pixelmon.command.endbattle.endbattle", new Object[0]);
                BattleRegistry.deRegisterBattle(bc);
            }
        } else {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.endbattle.notinbattle", player.getName());
            Pixelmon.NETWORK.sendTo(new ExitBattle(), player);
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        if (args.length == 1) {
            return this.tabCompleteUsernames(args);
        }
        return Collections.emptyList();
    }
}

