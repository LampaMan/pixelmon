/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.network.CommandChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.packetHandlers.OpenReplaceMoveScreen;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.RegexPatterns;
import java.util.Collections;
import java.util.List;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;

public class Teach
extends PixelmonCommand {
    public Teach() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "teach";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/teach <player> <slot> <move>";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        block26: {
            if (args.length < 1) {
                CommandChatHandler.sendFormattedChat(sender, TextFormatting.RED, this.getUsage(sender), new Object[0]);
            } else if (args.length == 1) {
                this.execute(sender, new String[]{sender.getName(), "1", args[0]});
            } else if (args.length == 2) {
                this.execute(sender, new String[]{sender.getName(), args[0], args[1]});
            } else if (args.length >= 3) {
                try {
                    EntityPlayerMP player = this.getPlayer(args[0]);
                    if (player == null) {
                        this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
                        return;
                    }
                    String playerName = player.getDisplayNameString();
                    if (BattleRegistry.getBattle(player) != null) {
                        this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.inbattle", new Object[0]);
                        return;
                    }
                    PlayerStorage storage = this.getPlayerStorage(player);
                    if (storage != null) {
                        Attack a;
                        String attackName;
                        NBTTagCompound nbt;
                        block25: {
                            int slot = Integer.parseInt(RegexPatterns.LETTERS.matcher(args[1]).replaceAll(""));
                            if (storage.guiOpened) {
                                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.teach.busy", playerName);
                                return;
                            }
                            if (slot < 1 || slot > 6) {
                                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.teach.slot", new Object[0]);
                                return;
                            }
                            nbt = storage.getList()[slot - 1];
                            if (nbt == null) {
                                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.teach.nothing", playerName);
                                return;
                            }
                            attackName = args[2];
                            for (int i = 3; i < args.length; ++i) {
                                attackName = attackName + ' ' + args[i];
                            }
                            attackName = attackName.replace('_', ' ');
                            a = null;
                            try {
                                int attackIndex = Integer.parseInt(attackName);
                                if (Attack.hasAttack(attackIndex)) {
                                    a = new Attack(attackIndex);
                                }
                            }
                            catch (NumberFormatException e) {
                                if (!Attack.hasAttack(attackName)) break block25;
                                a = new Attack(attackName);
                            }
                        }
                        if (a == null) {
                            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.teach.nomove", attackName);
                            return;
                        }
                        storage.recallAllPokemon();
                        EntityPixelmon p = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(nbt, player.world);
                        if (!p.getMoveset().hasAttack(a)) {
                            if (p.getMoveset().size() >= 4) {
                                EntityPlayerMP playerOwner = (EntityPlayerMP)p.getOwner();
                                if (playerOwner == null) {
                                    playerOwner = player;
                                }
                                Pixelmon.NETWORK.sendTo(new OpenReplaceMoveScreen(playerOwner.getUniqueID(), p.getPokemonId(), a.getAttackBase().attackIndex, 0, 0), playerOwner);
                                this.sendMessage(sender, TextFormatting.GREEN, "pixelmon.command.teach.sentmove", playerName, p.getNickname(), a.getAttackBase().getLocalizedName());
                            } else {
                                p.getMoveset().add(a);
                                p.update(EnumUpdateType.Moveset);
                                this.sendMessage(sender, TextFormatting.GREEN, "pixelmon.stats.learnedmove", p.getNickname(), a.getAttackBase().getLocalizedName());
                            }
                        } else {
                            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.teach.knowsmove", playerName, p.getNickname(), a.getAttackBase().getLocalizedName());
                        }
                        break block26;
                    }
                    this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
                }
                catch (NumberFormatException e) {
                    this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalid", new Object[0]);
                }
            }
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        if (args.length == 1) {
            return this.tabCompleteUsernames(args);
        }
        if (args.length == 2) {
            return Teach.getListOfStringsMatchingLastWord(args, "1", "2", "3", "4", "5", "6");
        }
        return Collections.emptyList();
    }
}

