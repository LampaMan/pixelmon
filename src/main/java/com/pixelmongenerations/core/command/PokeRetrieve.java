/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.core.util.PixelmonMethods;
import java.util.List;
import java.util.Optional;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextFormatting;

public class PokeRetrieve
extends PixelmonCommand {
    public PokeRetrieve() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "pokeretrieve";
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/pokeretrieve <radius>";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        if (sender instanceof EntityPlayerMP) {
            List<EntityPlayerMP> players;
            EntityPlayerMP player = (EntityPlayerMP)sender;
            if (args.length == 1) {
                Optional<Integer> radiusOpt = this.getRadius(args[0]);
                if (!radiusOpt.isPresent()) {
                    this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalid", new Object[0]);
                    this.sendMessage(sender, TextFormatting.RED, this.getUsage(sender), new Object[0]);
                    return;
                }
                Integer radius = radiusOpt.get();
                players = player.world.getEntities(EntityPlayerMP.class, e -> player.getDistanceSq((Entity)e) <= (double)(radius * radius));
            } else {
                players = player.getServer().getPlayerList().getPlayers();
            }
            players.forEach(PixelmonMethods::retrieveAllPokemon);
            this.sendMessage(sender, TextFormatting.GREEN, "pixelmon.command.pokeretrieve.done", new Object[0]);
        } else {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.needtobeplayer", new Object[0]);
        }
    }

    private Optional<Integer> getRadius(String radiusText) {
        try {
            int radius = Integer.parseInt(radiusText);
            return Optional.of(radius);
        }
        catch (NumberFormatException numberFormatException) {
            return Optional.empty();
        }
    }
}

