/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.common.item.heldItems.ItemBerry;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.enums.EnumBerryQuality;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import net.minecraft.command.CommandException;
import net.minecraft.command.EntitySelector;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;

public class GiveBerry
extends PixelmonCommand {
    public GiveBerry() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "giveberry";
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/giveberry <player> <type> <quality> <amount>";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        List<EntityPlayerMP> player = null;
        EnumBerry berry = EnumBerry.Oran;
        EnumBerryQuality quality = EnumBerryQuality.Poor;
        int amount = 1;
        player = args[0].startsWith("@") ? EntitySelector.matchEntities(sender, args[0], EntityPlayerMP.class) : Collections.singletonList(this.getPlayer(args[0]));
        if (args.length == 2) {
            berry = EnumBerry.valueOf(args[1]);
        } else if (args.length == 3) {
            berry = EnumBerry.valueOf(args[1]);
            quality = EnumBerryQuality.valueOf(args[2]);
        } else if (args.length == 4) {
            berry = EnumBerry.valueOf(args[1]);
            quality = EnumBerryQuality.valueOf(args[2]);
            try {
                amount = Integer.decode(args[3]);
            }
            catch (Exception e) {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.berrygive.defaultamount", new Object[0]);
            }
        }
        ItemStack stack = new ItemStack(berry.getBerry(), amount);
        ItemBerry.setQuality(stack, quality);
        player.forEach(p -> DropItemHelper.giveItemStackToPlayer(p, stack));
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        if (args.length == 1) {
            return this.tabCompleteUsernames(args);
        }
        if (args.length == 2) {
            return PixelmonCommand.getListOfStringsMatchingLastWord(args, Arrays.asList(EnumBerry.values()));
        }
        if (args.length == 3) {
            return PixelmonCommand.getListOfStringsMatchingLastWord(args, Arrays.asList(EnumBerryQuality.values()));
        }
        return Collections.emptyList();
    }
}

