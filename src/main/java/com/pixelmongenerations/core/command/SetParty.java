/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.RegexPatterns;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextFormatting;

public class SetParty
extends PixelmonCommand {
    public SetParty() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "setparty";
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/setparty <lvl>";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        try {
            if (args.length < 1 || args.length > 2) {
                return;
            }
            int level = Integer.parseInt(RegexPatterns.LETTERS.matcher(args[0]).replaceAll(""));
            if (level <= 0 || level > PixelmonServerConfig.maxLevel) {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.cheater", new Object[0]);
                return;
            }
            EntityPlayerMP player = this.getPlayer(sender.getName());
            if (BattleRegistry.getBattle(player) != null) {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.inbattle", new Object[0]);
                return;
            }
            PlayerStorage storage = this.getPlayerStorage(player);
            if (storage != null) {
                if (args.length == 2 && args[1].equals("moves")) {
                    storage.setAllToLevelAndChooseMoveset(level);
                } else {
                    storage.setAllToLevel(level);
                }
            } else {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
            }
        }
        catch (NumberFormatException e) {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.lvlerror", new Object[0]);
        }
        catch (Exception e) {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
        }
    }
}

