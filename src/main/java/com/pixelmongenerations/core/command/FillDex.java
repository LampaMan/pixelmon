/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.pokedex.EnumPokedexRegisterStatus;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javax.annotation.Nullable;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class FillDex
extends PixelmonCommand {
    public FillDex() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "filldex";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/filldex <player> <unknown/seen/caught>";
    }

    @Override
    public List<String> getAliases() {
        return Arrays.asList("dexfill");
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        if (!(sender instanceof EntityPlayerMP)) {
            return;
        }
        if (args.length == 0 || args.length > 3) {
            sender.sendMessage(new TextComponentString(this.getUsage(sender)));
            return;
        }
        EntityPlayerMP player = this.getPlayer(sender, args[0]);
        Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
        if (storageOpt.isPresent()) {
            EnumPokedexRegisterStatus status = EnumPokedexRegisterStatus.valueOf(args[1]);
            if (status == null) {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalid", new Object[0]);
                this.sendMessage(sender, TextFormatting.RED, this.getUsage(sender), new Object[0]);
            }
            storageOpt.get().pokedex.fillDex(status);
            storageOpt.get().pokedex.sendToPlayer((EntityPlayerMP)sender);
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos pos) {
        if (args.length == 1) {
            return this.tabCompleteUsernames(args);
        }
        return args.length == 2 ? this.tabCompleteCustomItems(args) : Collections.emptyList();
    }

    public List<String> tabCompleteCustomItems(String[] args) {
        return PixelmonCommand.getListOfStringsMatchingLastWord(args, EnumPokedexRegisterStatus.getNameList());
    }
}

