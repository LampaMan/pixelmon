/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.core.storage.PlayerStats;
import com.pixelmongenerations.core.storage.PlayerStorage;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;

public class Stats
extends PixelmonCommand {
    public Stats() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "pokestats";
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/pokestats <me:player>";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 0;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        EntityPlayerMP target = null;
        if (args.length == 0 || args[0].equalsIgnoreCase("me")) {
            if (sender instanceof EntityPlayerMP) {
                target = (EntityPlayerMP)sender;
                this.sendMessage(sender, I18n.translateToLocal("pixelmon.command.stats.yourstats"), new Object[0]);
            } else {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
            }
        } else {
            target = this.getPlayer(args[0]);
            if (target == null) {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
                return;
            }
            this.sendMessage(sender, target.getDisplayNameString() + I18n.translateToLocal("pixelmon.command.stats.playerstats"), new Object[0]);
        }
        PlayerStorage storage = this.getPlayerStorage(target);
        if (storage != null) {
            PlayerStats stats = storage.stats;
            this.sendMessage(sender, TextFormatting.GREEN, I18n.translateToLocal("pixelmon.command.stats.wins") + " " + stats.getWins(), new Object[0]);
            this.sendMessage(sender, TextFormatting.RED, I18n.translateToLocal("pixelmon.command.stats.losses") + " " + stats.getLosses(), new Object[0]);
        }
    }
}

