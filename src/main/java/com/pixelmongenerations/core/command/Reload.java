/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.battle.rules.clauses.tiers.RulesRegistry;
import com.pixelmongenerations.common.entity.npcs.registry.BaseShopItem;
import com.pixelmongenerations.common.entity.npcs.registry.DropItemRegistry;
import com.pixelmongenerations.common.entity.npcs.registry.NPCRegistryData;
import com.pixelmongenerations.common.entity.npcs.registry.NPCRegistryShopkeepers;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.core.config.PixelmonConfig;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.text.TextFormatting;

public class Reload
extends PixelmonCommand {
    public Reload() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "pokereload";
    }

    @Override
    public String getUsage(ICommandSender commandSender) {
        return "/pokereload";
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        String[] langCodeArray;
        try {
            PixelmonConfig.reload(true);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        HashMap<String, BaseShopItem> oldItems = NPCRegistryShopkeepers.shopItems;
        NPCRegistryShopkeepers.shopItems = new HashMap();
        try {
            ServerNPCRegistry.shopkeepers.registerShopItems();
        }
        catch (Exception e) {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.reload.error.shopitems", new Object[0]);
            NPCRegistryShopkeepers.shopItems = oldItems;
        }
        Set<String> langCodes = ServerNPCRegistry.data.keySet();
        for (String langCode : langCodeArray = langCodes.toArray(new String[langCodes.size()])) {
            NPCRegistryData oldData = ServerNPCRegistry.data.remove(langCode);
            try {
                ServerNPCRegistry.registerNPCS(langCode);
            }
            catch (Exception e) {
                e.printStackTrace();
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.reload.error.npcs", new Object[0]);
                ServerNPCRegistry.data.put(langCode, oldData);
            }
        }
        RulesRegistry.registerRules();
        try {
            DropItemRegistry.registerDropItems();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.reload.error.drops", new Object[0]);
        }
        this.sendMessage(sender, "pixelmon.command.reload.done", new Object[0]);
    }
}

