/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextFormatting;

public class PokeKill
extends PixelmonCommand {
    public PokeKill() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "pokekill";
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/pokekill <radius>";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        if (sender instanceof EntityPlayerMP) {
            EntityPlayerMP player = (EntityPlayerMP)sender;
            if (args.length == 1) {
                Optional<Integer> radiusOpt = this.getRadius(args[0]);
                if (!radiusOpt.isPresent()) {
                    this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalid", new Object[0]);
                    this.sendMessage(sender, TextFormatting.RED, this.getUsage(sender), new Object[0]);
                }
                Integer radius = radiusOpt.get();
                Integer killedPokemon = 0;
                List<EntityPixelmon> nearbyPokemon = player.world.getEntities(EntityPixelmon.class, e -> player.getDistanceSq((Entity)e) <= (double)(radius * radius));
                for (EntityPixelmon pokemon : nearbyPokemon) {
                    if (pokemon.hasOwner()) continue;
                    pokemon.setDead();
                    Integer n = killedPokemon;
                    Integer n2 = killedPokemon = Integer.valueOf(killedPokemon + 1);
                }
                this.sendMessage(sender, TextFormatting.GREEN, "pixelmon.command.pokekill.done", killedPokemon);
            } else {
                AtomicInteger killedPokemon = new AtomicInteger(0);
                player.world.getEntities(EntityPixelmon.class, e -> {
                    e.setDead();
                    killedPokemon.getAndIncrement();
                    return true;
                });
                this.sendMessage(sender, TextFormatting.GREEN, "pixelmon.command.pokekill.done", killedPokemon);
            }
        } else {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.needtobeplayer", new Object[0]);
        }
    }

    private Optional<Integer> getRadius(String radiusText) {
        try {
            int radius = Integer.parseInt(radiusText);
            return Optional.of(radius);
        }
        catch (NumberFormatException numberFormatException) {
            return Optional.empty();
        }
    }
}

