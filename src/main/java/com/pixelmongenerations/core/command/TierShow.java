/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClauseRegistry;
import com.pixelmongenerations.common.battle.rules.clauses.tiers.Tier;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.text.TextFormatting;

public class TierShow
extends PixelmonCommand {
    private static final String COMMAND_NAME = "tiershow";

    public TierShow() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return COMMAND_NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return String.format("/%s <tier>", COMMAND_NAME);
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        if (args.length != 1) {
            this.sendMessage(sender, TextFormatting.RED, this.getUsage(sender), new Object[0]);
            return;
        }
        Tier tier = BattleClauseRegistry.getTierRegistry().getClause(args[0]);
        if (tier == null) {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.tiershow", args[0]);
            return;
        }
        this.sendMessage(sender, tier.getTierDescription(), new Object[0]);
    }
}

