/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.block.tileEntities.TileEntityRanchBase;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.ComputerBox;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.List;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;

public class Unlock
extends PixelmonCommand {
    public Unlock() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "unlock";
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/unlock <player>";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        if (args.length != 1) {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalid", new Object[0]);
            this.sendMessage(sender, TextFormatting.RED, this.getUsage(sender), new Object[0]);
            return;
        }
        EntityPlayerMP player = Unlock.getEntity(sender.getServer(), sender, args[0], EntityPlayerMP.class);
        if (player == null) {
            ChatHandler.sendFormattedChat(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
            return;
        }
        for (EntityPixelmon pixelmon : player.world.getEntities(EntityPixelmon.class, input -> input.isInRanchBlock && input.isOwner(player))) {
            pixelmon.isInRanchBlock = false;
            if (pixelmon.blockOwner != null && pixelmon.blockOwner instanceof TileEntityRanchBase) {
                TileEntityRanchBase te = (TileEntityRanchBase)pixelmon.blockOwner;
                te.removePokemon(player, pixelmon.getPokemonId());
                continue;
            }
            pixelmon.unloadEntity();
        }
        PlayerStorage storage = this.getPlayerStorage(player);
        if (storage != null) {
            boolean flag = false;
            for (NBTTagCompound compound : storage.getList()) {
                if (compound == null || !compound.hasKey("isInRanch") || !compound.getBoolean("isInRanch")) continue;
                compound.setBoolean("isInRanch", false);
                flag = true;
            }
            if (flag) {
                storage.sendUpdatedList();
            }
        }
        PlayerComputerStorage computerStorage = this.getComputerStorage(player);
        for (ComputerBox box : computerStorage.getBoxList()) {
            box.unlockAllPokemon();
        }
        Unlock.notifyCommandListener(sender, (ICommand)this, 0, "pixelmon.command.unlock", player.getName());
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        return this.tabCompleteUsernames(args);
    }

    @Override
    public boolean isUsernameIndex(String[] args, int index) {
        return index == 0;
    }
}

