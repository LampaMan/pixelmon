/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.command.Breed;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.network.CommandChatHandler;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelmonMethods;
import java.awt.Color;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

public class PokeTint
extends PixelmonCommand {
    public PokeTint() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "poketint";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/poketint <username> <party slot #1> <hex color>";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        block17: {
            if (args.length != 3) {
                CommandChatHandler.sendChat(sender, "pixelmon.command.general.invalid", new Object[0]);
                CommandChatHandler.sendChat(sender, this.getUsage(sender), new Object[0]);
                return;
            }
            EntityPlayerMP player = this.getPlayer(sender, args[0]);
            if (player == null) {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
                return;
            }
            PlayerStorage storage = this.getPlayerStorage(player);
            if (storage != null) {
                try {
                    int slot = Integer.parseInt(args[1]);
                    if (slot > 0 && slot < 7) {
                        Optional<EntityPixelmon> outPokemonOpt = storage.getAlreadyExists(storage.getIDFromPosition(slot - 1), player.getServerWorld());
                        Optional<EntityPixelmon> pokemonOpt = PokeTint.getPartyPokemon(player.getServerWorld(), storage, Integer.parseInt(args[1]));
                        if (outPokemonOpt.isPresent() || pokemonOpt.isPresent()) {
                            EntityPixelmon pokemon = outPokemonOpt.isPresent() ? outPokemonOpt.get() : pokemonOpt.get();
                            try {
                                if (args[2].equalsIgnoreCase("strobeon")) {
                                    pokemon.setColorStrobe(true);
                                } else if (args[2].equalsIgnoreCase("strobeoff")) {
                                    pokemon.setColorStrobe(false);
                                } else if (args[2].equalsIgnoreCase("off") || args[2].equalsIgnoreCase("remove") || args[2].equalsIgnoreCase("clear")) {
                                    pokemon.setColorStrobe(false);
                                } else {
                                    Color color = Color.decode(args[2]);
                                    pokemon.setColorTint(args[2]);
                                }
                                PokeTint.updatePartyPokemon(storage, pokemon);
                                this.sendMessage(sender, TextFormatting.GREEN, "Successfully set the pokemons tint.", new Object[0]);
                            }
                            catch (NumberFormatException e) {
                                this.sendMessage(sender, TextFormatting.RED, String.format("%s is not a valid hex color.", args[2]), new Object[0]);
                            }
                        } else {
                            this.sendMessage(sender, TextFormatting.RED, String.format("No pokemon found in this slot.", args[1]), new Object[0]);
                        }
                        break block17;
                    }
                    this.sendMessage(sender, TextFormatting.RED, String.format("Invalid slot number, needs to be 1 to 6.", args[1]), new Object[0]);
                }
                catch (NumberFormatException exception) {
                    this.sendMessage(sender, TextFormatting.RED, String.format("%s is not a valid slot number.", args[1]), new Object[0]);
                }
            } else {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
            }
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        if (args.length == 1) {
            return this.tabCompleteUsernames(args);
        }
        if (args.length == 2) {
            return Breed.getListOfStringsMatchingLastWord(args, "1", "2", "3", "4", "5", "6");
        }
        return Collections.emptyList();
    }

    public static Optional<EntityPixelmon> getPartyPokemon(World world, PlayerStorage storage, int slot) {
        NBTTagCompound tagCompound;
        NBTTagCompound[] team = storage.partyPokemon;
        if (--slot <= 5 && slot >= 0 && (tagCompound = team[slot]) != null) {
            int[] id = PixelmonMethods.getID(tagCompound);
            if (storage.isEgg(id)) {
                return Optional.ofNullable(null);
            }
            EntityPixelmon entity = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(tagCompound, world);
            return Optional.ofNullable(entity);
        }
        return Optional.ofNullable(null);
    }

    public static void updatePartyPokemon(PlayerStorage storage, EntityPixelmon pixelmon) {
        storage.updateNBT(pixelmon);
    }
}

