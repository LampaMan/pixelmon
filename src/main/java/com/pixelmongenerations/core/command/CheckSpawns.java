/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.spawning.AbstractSpawner;
import com.pixelmongenerations.api.spawning.SpawnInfo;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.calculators.ICalculateSpawnLocations;
import com.pixelmongenerations.api.world.BlockCollection;
import com.pixelmongenerations.common.spawning.PixelmonSpawning;
import com.pixelmongenerations.common.spawning.PlayerTrackingSpawner;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class CheckSpawns
extends CommandBase {
    @Override
    public String getName() {
        return "checkspawns";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/checkspawns [specific] [pokemon...]";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(MinecraftServer minecraftServer, ICommandSender sender, String[] args) throws CommandException {
        if (!(sender instanceof EntityPlayerMP)) {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.needtobeplayer", new Object[0]);
        } else if (PixelmonSpawning.coordinator != null && PixelmonSpawning.coordinator.getActive()) {
            boolean regional = args.length == 0 || !args[0].equals("specific");
            ArrayList<String> specificPokemon = new ArrayList<String>();
            if (args.length > 0) {
                for (String arg : args) {
                    if (!EnumSpecies.hasPokemonAnyCase(arg)) continue;
                    specificPokemon.add(I18n.translateToLocal("pixelmon." + EnumSpecies.getFromNameAnyCase((String)arg).name.toLowerCase() + ".name"));
                }
            }
            EntityPlayerMP player = (EntityPlayerMP)sender;
            PlayerTrackingSpawner pspawner = null;
            for (AbstractSpawner abstractSpawner : PixelmonSpawning.coordinator.spawners) {
                if (!(abstractSpawner instanceof PlayerTrackingSpawner) || !((PlayerTrackingSpawner)abstractSpawner).playerUUID.equals(player.getUniqueID())) continue;
                pspawner = (PlayerTrackingSpawner)abstractSpawner;
                break;
            }
            if (pspawner == null) {
                this.sendMessage(sender, TextFormatting.RED, "Error finding associated player tracking spawner.", new Object[0]);
                return;
            }
            PlayerTrackingSpawner spawner = pspawner;
            PixelmonSpawning.coordinator.processor.addProcess(() -> {
                float adjusted;
                BlockPos pos = player.getPosition();
                BlockCollection collection = new BlockCollection(player.getEntityWorld(), pos.getX() - spawner.horizontalSliceRadius, pos.getX() + spawner.horizontalSliceRadius, pos.getY() - spawner.verticalSliceRadius, pos.getY() + spawner.verticalSliceRadius, pos.getZ() - spawner.horizontalSliceRadius, pos.getZ() + spawner.horizontalSliceRadius);
                ICalculateSpawnLocations calculator = spawner.spawnLocationCalculator;
                ArrayList<SpawnInfo> possibleSpawns = new ArrayList<SpawnInfo>();
                float raritySum = 0.0f;
                HashMap<SpawnInfo, Float> adjustedRarities = new HashMap<SpawnInfo, Float>();
                ArrayList<SpawnLocation> spawnLocations = calculator.calculateSpawnableLocations(collection);
                for (SpawnLocation spawnLocation : spawnLocations) {
                    if (!spawnLocation.location.pos.equals(pos) && !regional) continue;
                    ArrayList<SpawnInfo> blockPossibleSpawns = spawner.getSuitableSpawns(spawnLocation);
                    for (SpawnInfo spawnInfo : blockPossibleSpawns) {
                        adjusted = spawnInfo.getAdjustedRarity(spawnLocation);
                        raritySum += adjusted;
                        if (adjustedRarities.containsKey(spawnInfo)) {
                            adjusted += ((Float)adjustedRarities.get(spawnInfo)).floatValue();
                        }
                        adjustedRarities.put(spawnInfo, Float.valueOf(adjusted));
                        if (possibleSpawns.contains(spawnInfo)) continue;
                        possibleSpawns.add(spawnInfo);
                    }
                }
                DecimalFormat df = new DecimalFormat(".####", DecimalFormatSymbols.getInstance(Locale.US));
                DecimalFormat df2 = new DecimalFormat(".##", DecimalFormatSymbols.getInstance(Locale.US));
                HashMap<String, Double> percentages = new HashMap<String, Double>();
                for (SpawnInfo spawnInfo : possibleSpawns) {
                    String label;
                    adjusted = ((Float)adjustedRarities.get(spawnInfo)).floatValue() * 100.0f;
                    double percentage = (double)(adjusted / raritySum) + (percentages.containsKey(label = spawnInfo.toString()) ? (Double)percentages.get(label) : 0.0);
                    if (percentage >= 0.01 && percentage <= 99.99) {
                        percentages.put(label, Double.parseDouble(df2.format(percentage)));
                        continue;
                    }
                    percentages.put(label, Double.parseDouble(df.format(percentage)));
                }
                double greenThreshold = 10.0;
                double yellowThreshold = 2.0;
                double redTreshold = 0.5;
                double lightPurpleThreshold = 0.001;
                ArrayList<Map.Entry<String, Double>> sortedEntries = new ArrayList<>(percentages.entrySet());
                sortedEntries.sort(new Comparator<Map.Entry<String, Double>>(){

                    @Override
                    public int compare(Map.Entry<String, Double> o1, Map.Entry<String, Double> o2) {
                        return o1.getValue().compareTo(o2.getValue());
                    }
                });
                if (regional) {
                    this.sendMessage(sender, TextFormatting.AQUA, "pixelmon.command.checkspawns.spawns.inthisarea", sortedEntries.isEmpty() ? (Object)((Object)TextFormatting.RED) + "0" : "");
                } else {
                    this.sendMessage(sender, TextFormatting.AQUA, "pixelmon.command.checkspawns.spawns.atthisposition", sortedEntries.isEmpty() ? (Object)((Object)TextFormatting.RED) + "0" : "");
                }
                ArrayList<String> messages = new ArrayList<String>();
                for (Map.Entry<String, Double> entry : sortedEntries) {
                    if (!specificPokemon.isEmpty() && !specificPokemon.contains(entry.getKey())) continue;
                    TextFormatting color = TextFormatting.DARK_PURPLE;
                    if ((Double)entry.getValue() > greenThreshold) {
                        color = TextFormatting.GREEN;
                    } else if ((Double)entry.getValue() > yellowThreshold) {
                        color = TextFormatting.YELLOW;
                    } else if ((Double)entry.getValue() > redTreshold) {
                        color = TextFormatting.RED;
                    } else if ((Double)entry.getValue() > lightPurpleThreshold) {
                        color = TextFormatting.LIGHT_PURPLE;
                    }
                    messages.add((Object)((Object)TextFormatting.GOLD) + (String)entry.getKey() + (Object)((Object)TextFormatting.GRAY) + ": " + (Object)((Object)color) + entry.getValue() + "%");
                }
                while (!messages.isEmpty()) {
                    String message = (String)messages.remove(0);
                    int i = 1;
                    while (!messages.isEmpty() && i++ < 3) {
                        message = message + (Object)((Object)TextFormatting.GOLD) + "," + (String)messages.remove(0);
                    }
                    sender.sendMessage(new TextComponentString(message));
                }
            });
        } else {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.checkspawns.betterspawningisoff", new Object[0]);
        }
    }

    public void sendMessage(ICommandSender sender, TextFormatting color, String string, Object ... data) {
        TextComponentTranslation text = new TextComponentTranslation(string, data);
        text.getStyle().setColor(color);
        sender.sendMessage(text);
    }

    public void sendMessage(ICommandSender sender, String string, Object ... data) {
        this.sendMessage(sender, TextFormatting.GRAY, string, data);
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        return args.length == 2 ? this.tabCompletePokemon(args) : Collections.emptyList();
    }

    public List<String> tabCompleteUsernames(String[] args) {
        return CheckSpawns.getListOfStringsMatchingLastWord(args, this.getServer().getOnlinePlayerNames());
    }

    public List<String> tabCompletePokemon(String[] args) {
        return CheckSpawns.getListOfStringsMatchingLastWord(args, EnumSpecies.getNameList());
    }

    public MinecraftServer getServer() {
        return FMLCommonHandler.instance().getMinecraftServerInstance();
    }
}

