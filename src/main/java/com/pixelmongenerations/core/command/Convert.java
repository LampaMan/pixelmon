/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.common.world.Schematic;
import com.pixelmongenerations.common.world.gen.structure.util.StructureSnapshot;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.CommandChatHandler;
import com.pixelmongenerations.core.util.PixelBlockSnapshot;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.datafix.fixes.TileEntityId;
import net.minecraft.util.math.BlockPos;

public class Convert
extends CommandBase {
    @Override
    public String getName() {
        return "convert";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/" + this.getName();
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (!(sender instanceof EntityPlayer)) {
            CommandChatHandler.sendChat(sender, this.getUsage(sender), new Object[0]);
            return;
        }
        EntityPlayer player = (EntityPlayer)sender;
        String filename = Pixelmon.modDirectory + "/snapshots/" + args[0] + ".snapshot";
        File saveFile = new File(filename);
        try {
            NBTTagCompound compound = CompressedStreamTools.read(saveFile);
            StructureSnapshot snapshot = StructureSnapshot.readFromNBT(compound);
            BlockPos base = new BlockPos(compound.getInteger("baseX"), compound.getInteger("baseY"), compound.getInteger("baseZ"));
            Schematic schematic = new Schematic((short)snapshot.getWidth(), (short)snapshot.getHight(), (short)snapshot.getLenght());
            TileEntityId id = new TileEntityId();
            for (int x = 0; x < snapshot.getWidth(); ++x) {
                for (int y = 0; y < snapshot.getHight(); ++y) {
                    for (int z = 0; z < snapshot.getLenght(); ++z) {
                        PixelBlockSnapshot block = snapshot.getBlockAt(x, y, z);
                        schematic.setBlockState(x, y, z, block.getReplacedBlock());
                        block.world = player.world;
                        TileEntity te = block.getTileEntity();
                        if (te == null) continue;
                        NBTTagCompound tag = id.fixTagCompound(te.serializeNBT());
                        tag.setInteger("x", tag.getInteger("x") - base.getX());
                        tag.setInteger("y", tag.getInteger("y") - base.getY());
                        tag.setInteger("z", tag.getInteger("z") - base.getZ());
                        schematic.tileEntities.add(tag);
                    }
                }
            }
            schematic.entities = snapshot.getStatues();
            schematic.name = args[0];
            System.out.println("pallete: " + schematic.palette);
            String baseDirectory = Pixelmon.modDirectory + "/schematics/";
            filename = baseDirectory + args[0] + ".schem";
            saveFile = new File(filename);
            File saveDirPath = new File(baseDirectory);
            if (!saveDirPath.exists()) {
                saveDirPath.mkdirs();
            }
            try {
                FileOutputStream f = new FileOutputStream(saveFile);
                DataOutputStream s = new DataOutputStream(f);
                CompressedStreamTools.write(schematic.saveToNBT(), s);
                s.close();
                f.close();
                CommandChatHandler.sendChat(sender, "pixelmon.command.snapshot.savefile", args[0]);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}

