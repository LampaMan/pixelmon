/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.api.events.SpectateEvent;
import com.pixelmongenerations.client.gui.battles.PixelmonInGui;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.Spectator;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.battles.EndSpectate;
import com.pixelmongenerations.core.network.packetHandlers.battles.SetBattlingPokemon;
import com.pixelmongenerations.core.network.packetHandlers.battles.SetPokemonBattleData;
import com.pixelmongenerations.core.network.packetHandlers.battles.SetPokemonTeamData;
import com.pixelmongenerations.core.network.packetHandlers.battles.StartBattle;
import com.pixelmongenerations.core.network.packetHandlers.battles.StartSpectate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.MinecraftForge;

public class Spectate
extends PixelmonCommand {
    public Spectate() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "spectate";
    }

    @Override
    public String getUsage(ICommandSender arg0) {
        return "/spectate <playerName>";
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        if (!(sender instanceof EntityPlayerMP)) {
            return;
        }
        EntityPlayerMP player = (EntityPlayerMP)sender;
        if (args.length == 0) {
            if (!BattleRegistry.removeSpectator(player)) {
                this.sendMessage(sender, TextFormatting.RED, this.getUsage(sender), new Object[0]);
            }
            Pixelmon.NETWORK.sendTo(new EndSpectate(), player);
        } else if (args.length == 1) {
            BattleControllerBase base;
            EntityPlayerMP target = this.getPlayer(args[0]);
            if (target == null || target.getDisplayName().equals(sender.getDisplayName())) {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.spectate.self", new Object[0]);
                return;
            }
            if (!BattleRegistry.hasBattle()) {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.spectate.nobattle", target.getDisplayNameString());
                return;
            }
            if (BattleRegistry.getBattle(player) != null) {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.inbattle", new Object[0]);
            }
            if ((base = BattleRegistry.getBattle(target)) == null) {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.spectate.nobattle", target.getDisplayNameString());
            } else {
                PlayerParticipant watchedPlayer = base.getPlayer(target.getName());
                if (watchedPlayer != null && !MinecraftForge.EVENT_BUS.post(new SpectateEvent.StartSpectate(player, base, target))) {
                    Pixelmon.NETWORK.sendTo(new StartSpectate(watchedPlayer.player.getUniqueID(), base.rules.battleType), player);
                    Pixelmon.NETWORK.sendTo(new StartBattle(base.battleIndex, base.getBattleType(watchedPlayer), base.rules), player);
                    ArrayList<PixelmonWrapper> teamList = watchedPlayer.getTeamPokemonList();
                    Pixelmon.NETWORK.sendTo(new SetBattlingPokemon(teamList), player);
                    Pixelmon.NETWORK.sendTo(new SetPokemonBattleData(PixelmonInGui.convertToGUI(teamList), false), player);
                    Pixelmon.NETWORK.sendTo(new SetPokemonBattleData(watchedPlayer.getOpponentData(), true), player);
                    if (base.getTeam(watchedPlayer).size() > 1) {
                        Pixelmon.NETWORK.sendTo(new SetPokemonTeamData(watchedPlayer.getAllyData()), player);
                    }
                    base.addSpectator(new Spectator(player, target.getName()));
                }
            }
        } else {
            this.sendMessage(sender, TextFormatting.RED, this.getUsage(sender), new Object[0]);
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        return args.length == 1 ? this.tabCompleteUsernames(args) : Collections.emptyList();
    }
}

