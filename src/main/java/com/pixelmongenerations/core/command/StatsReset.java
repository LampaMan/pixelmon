/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.core.storage.PlayerStats;
import com.pixelmongenerations.core.storage.PlayerStorage;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;

public class StatsReset
extends PixelmonCommand {
    public StatsReset() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "resetpokestats";
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/resetpokestats <player>";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        if (args.length != 1) {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalid", new Object[0]);
            return;
        }
        EntityPlayerMP player = this.getPlayer(args[0]);
        PlayerStorage storage = this.getPlayerStorage(player);
        if (storage != null) {
            PlayerStats stats = storage.stats;
            stats.resetWinLoss();
            this.sendMessage(sender, player.getDisplayName().getUnformattedText() + I18n.translateToLocal("pixelmon.command.statsreset.reset"), new Object[0]);
        } else {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
        }
    }
}

