/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.WildPixelmonParticipant;
import com.pixelmongenerations.core.enums.EnumCurryType;
import com.pixelmongenerations.core.enums.EnumFlavor;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.text.translation.I18n;

public class TPToBattleDim
extends PixelmonCommand {
    BattleControllerBase bc1;
    BattleControllerBase bc2;
    PlayerParticipant playerPart;
    WildPixelmonParticipant wildPart;

    public TPToBattleDim() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "tpbat";
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/tpbat";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        for (EnumCurryType type : EnumCurryType.values()) {
            for (EnumFlavor flavor : EnumFlavor.values()) {
                String name = I18n.translateToLocal("item.curry.name");
                if (type != EnumCurryType.None) {
                    name = type.getLocalizedName() + " " + name;
                }
                if (flavor != EnumFlavor.None) {
                    name = flavor.getLocalizedName() + " " + name;
                }
                System.out.println(name + ": " + name.length());
            }
        }
    }
}

