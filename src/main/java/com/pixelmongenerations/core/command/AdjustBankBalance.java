/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Collections;
import java.util.List;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;

public class AdjustBankBalance
extends PixelmonCommand {
    public AdjustBankBalance() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "givemoney";
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/givemoney <player> <money>";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 1) {
            this.execute(sender, new String[]{sender.getName(), args[0]});
            return;
        }
        if (args.length < 1 || args.length > 3) {
            this.sendMessage(sender, "pixelmon.command.general.invalid", new Object[0]);
            this.sendMessage(sender, this.getUsage(sender), new Object[0]);
            return;
        }
        int amount = 0;
        try {
            amount = Integer.parseInt(args[1]);
        }
        catch (NumberFormatException numberFormatException) {
            // empty catch block
        }
        if (amount == 0) {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalid", new Object[0]);
            this.sendMessage(sender, this.getUsage(sender), new Object[0]);
            return;
        }
        EntityPlayerMP target = this.getPlayer(sender, args[0]);
        if (target == null) {
            throw new PlayerNotFoundException(args[0]);
        }
        PlayerStorage targetStorage = this.getPlayerStorage(target);
        if (targetStorage != null) {
            int beforeCurrency = targetStorage.getCurrency();
            targetStorage.addCurrency(amount);
            int currencyDifference = targetStorage.getCurrency() - beforeCurrency;
            ITextComponent targetName = target.getDisplayName();
            if (currencyDifference == 0) {
                if (amount > 0) {
                    this.sendMessage(sender, "pixelmon.command.givemoney.moneylimit", targetName);
                } else {
                    this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.givemoney.nomoney", targetName);
                }
            } else {
                String currencyString = Integer.toString(currencyDifference);
                AdjustBankBalance.notifyCommandListener(sender, (ICommand)this, 0, "pixelmon.command.givemoney.notifygive", sender.getName(), currencyDifference, targetName);
                this.sendMessage(sender, "pixelmon.command.givemoney.given", currencyString, targetName);
                if (sender != target) {
                    this.sendMessage((ICommandSender)target, TextFormatting.GRAY, "pixelmon.command.givemoney.received", sender.getDisplayName(), currencyString);
                }
            }
        } else {
            this.sendMessage(sender, TextFormatting.GRAY, "pixelmon.command.general.invalidplayer", new Object[0]);
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        return args.length != 1 ? Collections.emptyList() : this.tabCompleteUsernames(args);
    }
}

