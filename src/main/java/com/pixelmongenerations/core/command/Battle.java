/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.battle.BattleFactory;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Collections;
import java.util.List;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;

public class Battle
extends PixelmonCommand {
    private PlayerParticipant player1;
    private PlayerParticipant player2;

    public Battle() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "pokebattle";
    }

    @Override
    public String getUsage(ICommandSender iCommandSender) {
        return "/pokebattle <player1> <player2>";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        if (args.length < 2 || args.length > 3) {
            this.sendMessage(sender, "pixelmon.command.general.invalid", new Object[0]);
            this.sendMessage(sender, this.getUsage(sender), new Object[0]);
            return;
        }
        EntityPlayerMP player1 = this.getPlayer(sender, args[0]);
        if (player1 == null) {
            throw new PlayerNotFoundException(args[0]);
        }
        EntityPlayerMP player2 = this.getPlayer(sender, args[1]);
        if (player2 == null) {
            throw new PlayerNotFoundException(args[1]);
        }
        if (player1 == player2) {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.battle.sameperson", new Object[0]);
            return;
        }
        if (BattleRegistry.getBattle(player1) != null) {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.battle.cannotchallenge", player1.getName());
            return;
        }
        if (BattleRegistry.getBattle(player2) != null) {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.battle.cannotchallenge", player2.getName());
            return;
        }
        if (player1.world != player2.world) {
            Battle.notifyCommandListener(sender, (ICommand)this, 0, "pixelmon.command.battle.dimension", new Object[0]);
            return;
        }
        PlayerStorage storage1 = this.getPlayerStorage(player1);
        PlayerStorage storage2 = this.getPlayerStorage(player2);
        if (storage1 != null && storage2 != null) {
            EntityPixelmon player1FirstPokemon = storage1.getFirstAblePokemon(player1.world);
            this.player1 = new PlayerParticipant(player1, player1FirstPokemon);
            EntityPixelmon player2FirstPokemon = storage2.getFirstAblePokemon(player2.world);
            this.player2 = new PlayerParticipant(player2, player2FirstPokemon);
            EntityPlayerMP invalidPlayer = null;
            if (player1FirstPokemon == null) {
                invalidPlayer = player1;
            } else if (player2FirstPokemon == null) {
                invalidPlayer = player2;
            }
            if (invalidPlayer != null) {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.battle.nopokemon", invalidPlayer.getDisplayNameString());
                return;
            }
            Battle.notifyCommandListener(sender, (ICommand)this, 0, "pixelmon.command.battle.started", player1.getDisplayNameString(), player2.getDisplayNameString());
            BattleFactory.createBattle().team1(this.player1).team2(this.player2).startBattle();
        } else if (PixelmonConfig.printErrors) {
            Pixelmon.LOGGER.error("Error loading player for command /pokebattle: " + args[0] + " " + args[1]);
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        return args.length != 1 && args.length != 2 ? Collections.emptyList() : this.tabCompleteUsernames(args);
    }
}

