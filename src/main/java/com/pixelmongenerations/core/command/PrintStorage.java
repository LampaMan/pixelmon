/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;

public class PrintStorage
extends PixelmonCommand {
    public PrintStorage() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "printstore";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/printstore";
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        PixelmonStorage.pokeBallManager.printStorage();
    }
}

