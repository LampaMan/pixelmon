/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;

public class Freeze
extends PixelmonCommand {
    public Freeze() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "freeze";
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        boolean bl = EntityPixelmon.freeze = !EntityPixelmon.freeze;
        if (EntityPixelmon.freeze) {
            this.sendMessage(sender, "pixelmon.command.freeze.frozen", new Object[0]);
            Freeze.notifyCommandListener(sender, (ICommand)this, 0, "pixelmon.command.freeze.frozen", new Object[0]);
        } else {
            this.sendMessage(sender, "pixelmon.command.freeze.unfrozen", new Object[0]);
            Freeze.notifyCommandListener(sender, (ICommand)this, 0, "pixelmon.command.freeze.unfrozen", new Object[0]);
        }
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/freeze";
    }
}

