/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nonnull
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.enums.ReceiveType;
import com.pixelmongenerations.api.events.PixelmonReceivedEvent;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javax.annotation.Nonnull;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.EntitySelector;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class PokeGiveEgg
extends CommandBase {
    @Override
    public String getName() {
        return "pokegiveegg";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public String getUsage(@Nonnull ICommandSender icommandsender) {
        return "/pokegiveegg <player> <pokemon>";
    }

    @Override
    public void execute(@Nonnull MinecraftServer server, @Nonnull ICommandSender sender, @Nonnull String[] args) throws CommandException {
        block15: {
            if (args.length < 2) {
                this.sendMessage(sender, "pixelmon.command.general.invalid", new Object[0]);
                this.sendMessage(sender, TextFormatting.RED, this.getUsage(sender), new Object[0]);
            } else if (!args[0].startsWith("@")) {
                try {
                    EntityPlayerMP player = PokeGiveEgg.getPlayer(server, sender, args[0]);
                    if (BattleRegistry.getBattle(player) != null) {
                        this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.inbattle", new Object[0]);
                        return;
                    }
                    String name = args[1];
                    Optional<PlayerStorage> oStorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
                    if (oStorage.isPresent()) {
                        PlayerStorage storage = oStorage.get();
                        if (name.equalsIgnoreCase("random")) {
                            String randomName = EnumSpecies.randomPoke((boolean)PixelmonConfig.allowRandomSpawnedEggsToBeLegendary).name;
                            if (EnumSpecies.hasPokemonAnyCase(randomName)) {
                                EntityPixelmon pokemon = (EntityPixelmon)PixelmonEntityList.createEntityByName(randomName, player.world);
                                pokemon.makeEntityIntoEgg();
                                MinecraftForge.EVENT_BUS.post(new PixelmonReceivedEvent(player, ReceiveType.Command, pokemon));
                                storage.addToParty(pokemon);
                                this.sendMessage(sender, "pixelmon.command.give.givesuccessegg", player.getDisplayName().getUnformattedText(), "random");
                                PokeGiveEgg.notifyCommandListener(sender, (ICommand)this, 0, "pixelmon.command.give.notifygiveegg", sender.getName(), player.getDisplayName().getUnformattedText(), "random");
                            } else {
                                this.sendMessage(sender, randomName + " not found.", new Object[0]);
                            }
                        } else if (EnumSpecies.hasPokemon(name)) {
                            EntityPixelmon pokemon = (EntityPixelmon)PixelmonEntityList.createEntityByName(name, player.world);
                            pokemon.makeEntityIntoEgg();
                            storage.addToParty(pokemon);
                            this.sendMessage(sender, "pixelmon.command.give.givesuccessegg", player.getDisplayName().getUnformattedText(), I18n.translateToLocal("pixelmon." + name.toLowerCase() + ".name"));
                            PokeGiveEgg.notifyCommandListener(sender, (ICommand)this, 0, "pixelmon.command.give.notifygiveegg", sender.getName(), player.getDisplayName().getUnformattedText(), I18n.translateToLocal("pixelmon." + name.toLowerCase() + ".name"));
                        } else {
                            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.notingame", name);
                        }
                        break block15;
                    }
                    this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
                }
                catch (NumberFormatException var8) {
                    this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.lvlerror", new Object[0]);
                }
            } else {
                List<EntityPlayerMP> list = EntitySelector.matchEntities(sender, args[0], EntityPlayerMP.class);
                for (EntityPlayerMP p : list) {
                    args[0] = p.getName();
                    this.execute(server, sender, args);
                }
            }
        }
    }

    public void sendMessage(ICommandSender sender, TextFormatting color, String string, Object ... data) {
        TextComponentTranslation text = new TextComponentTranslation(string, data);
        text.getStyle().setColor(color);
        sender.sendMessage(text);
    }

    public void sendMessage(ICommandSender sender, String string, Object ... data) {
        this.sendMessage(sender, TextFormatting.GRAY, string, data);
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        return args.length == 1 ? this.tabCompleteUsernames(args) : (args.length == 2 ? this.tabCompletePokemon(args) : Collections.emptyList());
    }

    public List<String> tabCompleteUsernames(String[] args) {
        return PokeGiveEgg.getListOfStringsMatchingLastWord(args, this.getServer().getOnlinePlayerNames());
    }

    public List<String> tabCompletePokemon(String[] args) {
        return PokeGiveEgg.getListOfStringsMatchingLastWord(args, EnumSpecies.getNameList());
    }

    public MinecraftServer getServer() {
        return FMLCommonHandler.instance().getMinecraftServerInstance();
    }
}

