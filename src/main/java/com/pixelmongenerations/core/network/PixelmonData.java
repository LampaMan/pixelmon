/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.mojang.authlib.GameProfile
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network;

import com.mojang.authlib.GameProfile;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.NoStatus;
import com.pixelmongenerations.common.battle.status.StatusPersist;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.Entity10CanBreed;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Illusion;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.entity.pixelmon.stats.Level;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.Stats;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.EntityLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.NBTLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.item.heldItems.HeldItem;
import com.pixelmongenerations.common.item.heldItems.ItemPlate;
import com.pixelmongenerations.common.item.heldItems.MemoryDrive;
import com.pixelmongenerations.common.item.heldItems.ZCrystal;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.enums.EnumMark;
import com.pixelmongenerations.core.enums.EnumNature;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.PixelmonMovesetData;
import com.pixelmongenerations.core.network.PixelmonUpdateData;
import com.pixelmongenerations.core.storage.playerData.ExternalMoveData;
import com.pixelmongenerations.core.storage.playerData.ExternalMoves;
import com.pixelmongenerations.core.util.RegexPatterns;
import io.netty.buffer.ByteBuf;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Matcher;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class PixelmonData {
    public int[] pokemonID;
    public String name;
    public String nickname;
    public String OT;
    public int lvl;
    public int dynamaxLevel;
    public int hp;
    public int health;
    public int friendship;
    public Gender gender;
    public boolean isFainted;
    public boolean isEgg;
    public int eggCycles;
    public int steps;
    public String eggDescription;
    public int nationalPokedexNumber;
    public boolean selected;
    public String originalTrainerName;
    protected EnumSpecies species;
    public int[] eggMoves;
    public boolean inBattle;
    public EnumType type1;
    public EnumType type2;
    public int order;
    public int numMoves;
    public int HP;
    public int Speed;
    public int Attack;
    public int Defence;
    public int SpecialAttack;
    public int SpecialDefence;
    public int nextLvlXP;
    public int boxNumber;
    public boolean isShiny;
    public boolean hasOwner;
    public boolean doesLevel;
    public boolean canHoldAnItem;
    public ItemStack heldItem;
    public int xp;
    protected int effectCount;
    public EnumBossMode bossMode;
    public EnumNature nature;
    public EnumNature pseudoNature;
    public int pokerus;
    public boolean hasGmaxFactor;
    public EnumGrowth growth;
    public EnumPokeball pokeball;
    public StatusType status;
    public PixelmonMovesetData[] moveset;
    public String ability;
    public short specialTexture;
    public boolean isInRanch;
    public short selectedMoveIndex;
    public int[] evs;
    public int[] ivs;
    public boolean[] caps;
    public int targetId;
    public short form;
    public int numClones;
    public int numEnchanted;
    public int numWormholes;
    public int numAbundantActivations;
    private String description;
    public boolean outside;
    public boolean resetMoves;
    private ExternalMoveData[] externalMoves;
    public EntityPixelmon outsideEntity;
    public String OTUUID;
    public boolean totem;
    public String customTexture;
    public EnumMark mark;

    public boolean hasPerfectIVs() {
        for (int iv : this.ivs) {
            if (iv == 31) continue;
            return false;
        }
        return true;
    }

    public boolean hasBottleCap() {
        for (boolean cap : this.caps) {
            if (!cap) continue;
            return true;
        }
        return false;
    }

    public int getTotalEVs() {
        int total = 0;
        for (int i = 0; i < 6; ++i) {
            total += this.evs[i];
        }
        return total;
    }

    public int getTotalIVs() {
        int total = 0;
        for (int i = 0; i < 6; ++i) {
            total += this.ivs[i];
        }
        return total;
    }

    public double getIVPercent() {
        double totalIVs = this.getTotalIVs();
        return totalIVs / 186.0 * 100.0;
    }

    public double getEVPercent() {
        double totalEVs = this.getTotalEVs();
        return totalEVs / 510.0 * 100.0;
    }

    public int getNationalPokedexNumber() {
        if (this.nationalPokedexNumber == -1) {
            this.nationalPokedexNumber = this.getSpecies().getNationalPokedexInteger();
        }
        return this.nationalPokedexNumber;
    }

    public boolean isGen6Sprite() {
        return this.getNationalPokedexNumber() > 649 && !this.isEgg;
    }

    public EnumSpecies getSpecies() {
        if (this.species == null) {
            this.species = EnumSpecies.getFromNameAnyCase(this.name);
            if (this.species == null) {
                this.species = EnumSpecies.Bulbasaur;
            }
        }
        return this.species;
    }

    public boolean isPokemon(EnumSpecies ... species) {
        if (this.species == null) {
            this.species = this.getSpecies();
        }
        return this.species.isPokemon(species);
    }

    public EnumType getType1() {
        if (this.type1 == null) {
            this.type1 = Entity3HasStats.getBaseStats((String)this.name, (int)this.form).get().type1;
        }
        return this.type1;
    }

    public EnumType getType2() {
        if (this.type2 == null) {
            this.type2 = Entity3HasStats.getBaseStats((String)this.name, (int)this.form).get().type2;
        }
        return this.type2;
    }

    public boolean hasType(EnumType type) {
        return this.getType1() == type || this.getType2() == type;
    }

    public PixelmonData() {
        this.dynamaxLevel = 0;
        this.OT = "";
        this.eggDescription = "";
        this.nationalPokedexNumber = -1;
        this.selected = false;
        this.eggMoves = new int[0];
        this.type1 = null;
        this.type2 = null;
        this.boxNumber = 0;
        this.heldItem = ItemStack.EMPTY;
        this.effectCount = 0;
        this.moveset = new PixelmonMovesetData[4];
        this.isInRanch = false;
        this.evs = new int[6];
        this.ivs = new int[6];
        this.caps = new boolean[6];
        this.targetId = -1;
        this.form = (short)-1;
        this.resetMoves = true;
        this.OTUUID = "";
        this.customTexture = "";
        this.mark = EnumMark.None;
    }

    public PixelmonData(NBTTagCompound p) {
        this(new NBTLink(p));
        int i;
        boolean movesetInvalid = false;
        try {
            for (i = 0; i < this.numMoves; ++i) {
                this.moveset[i] = PixelmonMovesetData.create(p, i);
                if (this.moveset[i] != null) continue;
                movesetInvalid = true;
            }
        }
        catch (NullPointerException e) {
            movesetInvalid = true;
        }
        if (movesetInvalid) {
            this.fixMoveset(p);
            for (i = 0; i < this.numMoves; ++i) {
                this.moveset[i] = PixelmonMovesetData.create(p, i);
            }
        }
        if (p.hasKey("BoxNumber")) {
            this.boxNumber = p.getInteger("BoxNumber");
        }
        if (p.hasKey("NumCloned")) {
            this.numClones = p.getInteger("NumCloned");
        }
        if (p.hasKey("NumEnchanted")) {
            this.numEnchanted = p.getInteger("NumEnchanted");
        }
        if (p.hasKey("NumWormholes")) {
            this.numWormholes = p.getInteger("NumWormholes");
        }
        if (p.hasKey("NumAbundantActivations")) {
            this.numAbundantActivations = p.getInteger("NumAbundantActivations");
        }
        if (p.hasKey("Totem")) {
            this.totem = p.getBoolean("Totem");
        }
        if (p.hasKey("CustomTexture")) {
            this.customTexture = p.getString("CustomTexture");
        }
        if (p.hasKey("Mark")) {
            this.mark = EnumMark.values()[p.getInteger("Mark")];
        }
    }

    private void fixMoveset(NBTTagCompound p) {
        for (int i = 0; i < this.numMoves; ++i) {
            Attack a;
            if (p.hasKey("PixelmonMoveID" + i) || (a = DatabaseMoves.getAttack(p.getString("PixelmonMoveName" + i))) != null) continue;
            p.removeTag("PixelmonMoveName" + i);
            p.removeTag("PixelmonMoveType" + i);
            p.removeTag("PixelmonMovePP" + i);
            p.removeTag("PixelmonMovePPBase" + i);
            for (int j = i + 1; j < this.numMoves; ++j) {
                int newIndex = j - 1;
                p.setInteger("PixelmonMoveID" + newIndex, p.getInteger("PixelmonMoveID" + j));
                p.setString("PixelmonMoveName" + newIndex, p.getString("PixelmonMoveName" + j));
                p.setInteger("PixelmonMovePP" + newIndex, p.getInteger("PixelmonMovePP" + j));
                p.setInteger("PixelmonMovePPBase" + newIndex, p.getInteger("PixelmonMovePPBase" + j));
            }
            --this.numMoves;
        }
        p.setInteger("PixelmonNumberMoves", this.numMoves);
    }

    public PixelmonData(EntityPixelmon p) {
        this(new EntityLink(p));
        this.targetId = p.getAttackTarget() != null ? p.getAttackTarget().getEntityId() : -1;
    }

    public PixelmonData(EntityPixelmon p, boolean realNickname) {
        this(p);
        if (!realNickname) {
            this.nickname = p.getNickname();
            if (p.getAbility() instanceof Illusion && ((Illusion)p.getAbility()).disguisedGender != null) {
                this.gender = ((Illusion)p.getAbility()).disguisedGender;
            }
        }
    }

    public PixelmonData(PokemonLink p) {
        this.OT = "";
        this.eggDescription = "";
        this.nationalPokedexNumber = -1;
        this.selected = false;
        this.eggMoves = new int[0];
        this.type1 = null;
        this.type2 = null;
        this.boxNumber = 0;
        this.heldItem = ItemStack.EMPTY;
        this.effectCount = 0;
        this.moveset = new PixelmonMovesetData[4];
        this.isInRanch = false;
        this.evs = new int[6];
        this.ivs = new int[6];
        this.caps = new boolean[6];
        this.targetId = -1;
        this.form = (short)-1;
        this.resetMoves = true;
        this.pokemonID = p.getPokemonID();
        BaseStats baseStats = p.getBaseStats();
        this.name = baseStats.pokemon.name;
        this.species = null;
        this.nickname = p.getNickname();
        this.lvl = p.getLevel();
        this.nextLvlXP = p.getExpToNextLevel();
        this.dynamaxLevel = p.getDynamaxLevel();
        this.xp = p.getExp();
        Stats stats = p.getStats();
        this.hp = stats.HP;
        this.friendship = p.getFriendship().getFriendship();
        this.health = p.getHealth();
        this.gender = p.getGender();
        this.isFainted = p.isFainted();
        this.isShiny = p.isShiny();
        this.isEgg = p.isEgg();
        this.eggCycles = p.getEggCycles();
        this.eggDescription = Entity10CanBreed.getEggDescription(this.eggCycles);
        this.canHoldAnItem = !this.isEgg;
        this.nature = p.getNature();
        this.pseudoNature = p.getPseudoNature();
        this.pokerus = p.getPokeRus();
        this.hasGmaxFactor = p.hasGmaxFactor();
        this.growth = p.getGrowth();
        this.pokeball = p.getCaughtBall();
        this.order = Math.max(0, p.getPartyPosition());
        Moveset moveset = p.getMoveset();
        this.numMoves = moveset.size();
        for (int i = 0; i < this.numMoves; ++i) {
            this.moveset[i] = PixelmonMovesetData.create(moveset, i);
        }
        this.HP = stats.HP;
        this.Speed = stats.Speed;
        this.Attack = stats.Attack;
        this.Defence = stats.Defence;
        this.SpecialAttack = stats.SpecialAttack;
        this.SpecialDefence = stats.SpecialDefence;
        this.heldItem = p.getHeldItemStack() == null ? ItemStack.EMPTY : p.getHeldItemStack();
        this.hasOwner = p.hasOwner();
        this.doesLevel = p.doesLevel();
        StatusPersist status = p.getPrimaryStatus();
        if (status != NoStatus.noStatus) {
            this.status = status.type;
        }
        this.bossMode = p.getBossMode();
        this.OT = p.getOriginalTrainer();
        this.OTUUID = p.getOriginalTrainerUUID();
        this.ability = RegexPatterns.SPACE_SYMBOL.matcher(p.getAbility().getName()).replaceAll("");
        this.specialTexture = (short)p.getSpecialTexture();
        this.isInRanch = p.isInRanch();
        this.form = (short)p.getForm();
        this.evs[0] = stats.EVs.get(StatsType.HP);
        this.evs[1] = stats.EVs.get(StatsType.Attack);
        this.evs[2] = stats.EVs.get(StatsType.Defence);
        this.evs[3] = stats.EVs.get(StatsType.SpecialAttack);
        this.evs[4] = stats.EVs.get(StatsType.SpecialDefence);
        this.evs[5] = stats.EVs.get(StatsType.Speed);
        this.ivs[0] = stats.IVs.get(StatsType.HP);
        this.ivs[1] = stats.IVs.get(StatsType.Attack);
        this.ivs[2] = stats.IVs.get(StatsType.Defence);
        this.ivs[3] = stats.IVs.get(StatsType.SpecialAttack);
        this.ivs[4] = stats.IVs.get(StatsType.SpecialDefence);
        this.ivs[5] = stats.IVs.get(StatsType.Speed);
        this.caps = stats.getBottleCapIVArray();
        List<EnumType> types = p.getType();
        this.type1 = types.get(0);
        if (types.size() > 1) {
            this.type2 = types.get(1);
        }
        this.eggMoves = p.getEggMoves();
        this.totem = p.isTotem();
        this.inBattle = p.getBattleController() != null;
        try {
            this.customTexture = p.getCustomTexture();
        }
        catch (Exception e) {
            this.customTexture = "null";
        }
        this.mark = p.getMark();
    }

    public PixelmonData(ByteBuf data) {
        this.OT = "";
        this.eggDescription = "";
        this.nationalPokedexNumber = -1;
        this.selected = false;
        this.eggMoves = new int[0];
        this.type1 = null;
        this.type2 = null;
        this.boxNumber = 0;
        this.heldItem = ItemStack.EMPTY;
        this.effectCount = 0;
        this.moveset = new PixelmonMovesetData[4];
        this.isInRanch = false;
        this.evs = new int[6];
        this.ivs = new int[6];
        this.caps = new boolean[6];
        this.targetId = -1;
        this.form = (short)-1;
        this.resetMoves = true;
        this.OTUUID = "";
        this.decodeInto(data);
    }

    public void update(PixelmonUpdateData p) {
        block23: for (EnumUpdateType type : p.updateTypes) {
            switch (type) {
                case HP: {
                    this.hp = p.hp;
                    this.health = p.health;
                    this.isFainted = p.isFainted;
                    continue block23;
                }
                case Stats: {
                    this.lvl = p.lvl;
                    this.nextLvlXP = p.nextLvlXP;
                    this.ability = p.ability;
                    this.xp = p.xp;
                    this.HP = p.HP;
                    this.Speed = p.Speed;
                    this.Attack = p.Attack;
                    this.Defence = p.Defence;
                    this.SpecialAttack = p.SpecialAttack;
                    this.SpecialDefence = p.SpecialDefence;
                    this.form = p.form;
                    this.isShiny = p.isShiny;
                    this.hasGmaxFactor = p.hasGmaxFactor;
                    this.pokeball = p.pokeball;
                    this.gender = p.gender;
                    this.type1 = p.type1;
                    if (p.type2 != null) {
                        this.type2 = p.type2;
                    }
                    this.ivs = p.ivs;
                    this.evs = p.evs;
                    this.caps = p.caps;
                    this.dynamaxLevel = p.dynamaxLevel;
                    continue block23;
                }
                case Nickname: {
                    this.nickname = p.nickname;
                    continue block23;
                }
                case Name: {
                    this.name = p.name;
                    this.species = null;
                    this.nationalPokedexNumber = p.getNationalPokedexNumber();
                    continue block23;
                }
                case Friendship: {
                    this.friendship = p.friendship;
                    continue block23;
                }
                case Moveset: {
                    this.numMoves = p.numMoves;
                    this.moveset = p.moveset;
                    this.resetMoves = true;
                    continue block23;
                }
                case HeldItem: {
                    this.heldItem = p.heldItem == null ? ItemStack.EMPTY : p.heldItem;
                    this.resetMoves = true;
                    continue block23;
                }
                case Status: {
                    this.effectCount = p.effectCount;
                    this.status = p.status;
                    continue block23;
                }
                case CanLevel: {
                    this.doesLevel = p.doesLevel;
                    continue block23;
                }
                case Egg: {
                    this.isEgg = p.isEgg;
                    this.eggCycles = p.eggCycles;
                    this.steps = p.steps;
                    this.eggDescription = Entity10CanBreed.getEggDescription(this.eggCycles);
                    continue block23;
                }
                case Texture: {
                    this.specialTexture = p.specialTexture;
                    this.customTexture = p.customTexture;
                    continue block23;
                }
                case Target: {
                    this.targetId = p.targetId;
                    continue block23;
                }
                case Ability: {
                    this.ability = p.ability;
                    continue block23;
                }
                case Clones: {
                    this.numClones = p.numClones;
                    continue block23;
                }
                case Enchants: {
                    this.numEnchanted = p.numEnchanted;
                    continue block23;
                }
                case Wormholes: {
                    this.numWormholes = p.numWormholes;
                    continue block23;
                }
                case AbundantActivations: {
                    this.numAbundantActivations = p.numAbundantActivations;
                    continue block23;
                }
                case Nature: {
                    this.nature = p.nature;
                    continue block23;
                }
                case PseudoNature: {
                    this.pseudoNature = p.pseudoNature;
                    continue block23;
                }
                case PokeRus: {
                    this.pokerus = p.pokerus;
                    continue block23;
                }
                case Mark: {
                    this.mark = p.mark;
                    continue block23;
                }
            }
        }
    }

    public void updatePokemon(NBTTagCompound p) {
        if (p != null) {
            MinecraftServer server;
            Optional<BaseStats> optional;
            int i;
            if (EnumSpecies.hasPokemon(this.name)) {
                p.setString("Name", this.name);
            }
            p.setString("Nickname", this.nickname);
            int currentLevel = p.getInteger("Level");
            if (currentLevel != this.lvl) {
                p.setInteger("Level", this.lvl);
                p.setInteger("EXP", 0);
            }
            p.setInteger("DynamaxLevel", this.dynamaxLevel);
            p.setShort("Gender", (short)this.gender.ordinal());
            p.setBoolean("IsFainted", this.isFainted);
            p.setBoolean("IsShiny", this.isShiny);
            p.setShort("specialTexture", this.specialTexture);
            p.setShort("Growth", (short)this.growth.index);
            p.setInteger("PixelmonNumberMoves", this.numMoves);
            p.setBoolean("isEgg", this.isEgg);
            p.setInteger("eggCycles", this.eggCycles);
            p.setInteger("steps", this.steps);
            p.setInteger("Friendship", this.friendship);
            for (i = 0; i < this.numMoves; ++i) {
                this.moveset[i].updatePokemon(p, i);
            }
            p.setShort("BossMode", (short)this.bossMode.index);
            p.setInteger("EVHP", this.evs[0]);
            p.setInteger("EVAttack", this.evs[1]);
            p.setInteger("EVDefence", this.evs[2]);
            p.setInteger("EVSpecialAttack", this.evs[3]);
            p.setInteger("EVSpecialDefence", this.evs[4]);
            p.setInteger("EVSpeed", this.evs[5]);
            p.setInteger("IVHP", this.ivs[0]);
            p.setInteger("IVAttack", this.ivs[1]);
            p.setInteger("IVDefence", this.ivs[2]);
            p.setInteger("IVSpAtt", this.ivs[3]);
            p.setInteger("IVSpDef", this.ivs[4]);
            p.setInteger("IVSpeed", this.ivs[5]);
            for (i = 0; i < this.caps.length; ++i) {
                if (!this.caps[i]) continue;
                p.setBoolean("BottleCap" + StatsType.fromPermaIndex(i).name(), true);
            }
            p.setInteger("Nature", this.nature.index);
            p.setInteger("PseudoNature", this.pseudoNature.index);
            p.setInteger("PokeRus", this.pokerus);
            p.setBoolean("gmaxfactor", this.hasGmaxFactor);
            p.setInteger("CaughtBall", this.pokeball.ordinal());
            p.setString("Ability", this.ability);
            if (this.heldItem == null || this.heldItem == ItemStack.EMPTY) {
                if (p.hasKey("HeldItemStack")) {
                    p.removeTag("HeldItemStack");
                }
            } else {
                p.setTag("HeldItemStack", this.heldItem.writeToNBT(new NBTTagCompound()));
            }
            if (this.form != -1) {
                p.setInteger("Variant", this.form);
            }
            if ((optional = Entity3HasStats.getBaseStats(this.name, (int)this.form)).isPresent()) {
                String[] allAbilities = optional.get().abilities;
                if (optional.get().abilities != null) {
                    for (i = 0; i < allAbilities.length; ++i) {
                        if (!this.ability.equals(allAbilities[i])) continue;
                        p.setString("Ability", this.ability);
                        p.setInteger("AbilitySlot", i);
                        break;
                    }
                }
            }
            if (this.numClones > 0) {
                p.setInteger("NumCloned", this.numClones);
            } else {
                p.removeTag("NumCloned");
            }
            if (this.numEnchanted > 0) {
                p.setInteger("NumEnchanted", this.numEnchanted);
            } else {
                p.removeTag("NumEnchanted");
            }
            if (this.numWormholes > 0) {
                p.setInteger("NumWormholes", this.numWormholes);
            } else {
                p.removeTag("NumWormholes");
            }
            if (this.numAbundantActivations > 0) {
                p.setInteger("NumAbundantActivations", this.numAbundantActivations);
            } else {
                p.removeTag("NumAbundantActivations");
            }
            if (this.customTexture != null || !this.customTexture.equals("null")) {
                p.setString("CustomTexture", this.customTexture);
            }
            GameProfile profile = null;
            if (this.OT != null && FMLCommonHandler.instance().getMinecraftServerInstance().isDedicatedServer() && (profile = (server = FMLCommonHandler.instance().getMinecraftServerInstance()).getPlayerProfileCache().getGameProfileForUsername(this.OT)) == null && this.OTUUID != null) {
                profile = server.getPlayerProfileCache().getProfileByUUID(UUID.fromString(this.OTUUID));
            }
            if (profile != null) {
                p.setString("originalTrainer", profile.getName());
                p.setString("originalTrainerUUID", profile.getId().toString());
            }
            if (this.mark != null) {
                p.setInteger("Mark", this.mark.ordinal());
            }
        }
    }

    public void encodeInto(ByteBuf data) {
        data.writeInt(this.pokemonID[0]);
        data.writeInt(this.pokemonID[1]);
        ByteBufUtils.writeUTF8String(data, this.name);
        ByteBufUtils.writeUTF8String(data, this.nickname);
        ByteBufUtils.writeUTF8String(data, this.OT);
        data.writeShort(this.lvl);
        data.writeInt(this.nextLvlXP);
        data.writeShort(this.xp);
        data.writeInt(this.dynamaxLevel);
        data.writeShort(this.hp);
        data.writeShort(this.friendship);
        data.writeShort(this.health);
        data.writeShort(this.gender.ordinal());
        data.writeBoolean(this.isFainted);
        data.writeBoolean(this.hasOwner);
        data.writeShort(this.order);
        data.writeShort(this.numMoves);
        for (int i = 0; i < this.numMoves; ++i) {
            this.moveset[i].writeData(data);
        }
        data.writeShort(this.HP);
        data.writeShort(this.Speed);
        data.writeShort(this.Attack);
        data.writeShort(this.Defence);
        data.writeShort(this.SpecialAttack);
        data.writeShort(this.SpecialDefence);
        data.writeShort(this.boxNumber);
        data.writeBoolean(this.isShiny);
        data.writeShort(this.nature.index);
        data.writeShort(this.pseudoNature.index);
        data.writeInt(this.pokerus);
        data.writeBoolean(this.hasGmaxFactor);
        data.writeShort(this.growth.index);
        data.writeShort(this.bossMode.index);
        if (this.pokeball != null) {
            data.writeShort(this.pokeball.getIndex());
        } else {
            data.writeShort(0);
        }
        data.writeBoolean(this.doesLevel);
        if (this.heldItem != ItemStack.EMPTY && this.heldItem.getItem() != Items.AIR) {
            data.writeBoolean(true);
            ByteBufUtils.writeItemStack(data, this.heldItem);
        } else {
            data.writeBoolean(false);
        }
        if (this.status == null) {
            data.writeShort(-1);
        } else {
            data.writeShort(this.status.ordinal());
        }
        ByteBufUtils.writeUTF8String(data, this.ability);
        data.writeBoolean(this.isEgg);
        data.writeInt(this.eggCycles);
        data.writeInt(this.steps);
        ByteBufUtils.writeUTF8String(data, this.eggDescription);
        data.writeShort((int)this.specialTexture);
        data.writeBoolean(this.isInRanch);
        data.writeShort((int)this.form);
        for (int n : this.evs) {
            data.writeShort(n);
        }
        for (int n : this.ivs) {
            data.writeShort(n);
        }
        for (boolean bl2 : this.caps) {
            data.writeBoolean(bl2);
        }
        data.writeShort(this.type1 == null ? -1 : this.type1.getIndex());
        data.writeShort(this.type2 == null ? -1 : this.type2.getIndex());
        data.writeShort(this.eggMoves.length);
        int[] arrn = this.eggMoves;
        int n = arrn.length;
        int[] arrn2 = arrn;
        int n2 = arrn2.length;
        for (int i = 0; i < n2; ++i) {
            Integer eggMove = arrn2[i];
            data.writeInt(eggMove.intValue());
        }
        data.writeBoolean(this.inBattle);
        data.writeInt(this.numClones);
        data.writeInt(this.numEnchanted);
        data.writeInt(this.numWormholes);
        data.writeInt(this.numAbundantActivations);
        ByteBufUtils.writeUTF8String(data, this.OTUUID);
        data.writeBoolean(this.totem);
        ByteBufUtils.writeUTF8String(data, this.customTexture);
        data.writeInt(this.mark.ordinal());
    }

    public void decodeInto(ByteBuf data) {
        int i;
        short statusIndex;
        this.pokemonID = new int[]{data.readInt(), data.readInt()};
        this.name = ByteBufUtils.readUTF8String(data);
        this.species = EnumSpecies.getFromNameAnyCase(this.name);
        this.nickname = ByteBufUtils.readUTF8String(data);
        this.OT = ByteBufUtils.readUTF8String(data);
        this.lvl = data.readShort();
        this.nextLvlXP = data.readInt();
        this.xp = data.readShort();
        this.dynamaxLevel = data.readInt();
        this.hp = data.readShort();
        this.friendship = data.readShort();
        this.health = data.readShort();
        this.gender = Gender.getGender(data.readShort());
        this.isFainted = data.readBoolean();
        this.hasOwner = data.readBoolean();
        this.order = data.readShort();
        this.numMoves = data.readShort();
        for (int i2 = 0; i2 < this.numMoves; ++i2) {
            this.moveset[i2] = new PixelmonMovesetData();
            this.moveset[i2].readData(data);
        }
        this.HP = data.readShort();
        this.Speed = data.readShort();
        this.Attack = data.readShort();
        this.Defence = data.readShort();
        this.SpecialAttack = data.readShort();
        this.SpecialDefence = data.readShort();
        this.boxNumber = data.readShort();
        this.isShiny = data.readBoolean();
        this.nature = EnumNature.getNatureFromIndex(data.readShort());
        this.pseudoNature = EnumNature.getNatureFromIndex(data.readShort());
        this.pokerus = data.readInt();
        this.hasGmaxFactor = data.readBoolean();
        this.growth = EnumGrowth.getGrowthFromIndex(data.readShort());
        this.bossMode = EnumBossMode.getMode(data.readShort());
        this.pokeball = EnumPokeball.getFromIndex(data.readShort());
        this.doesLevel = data.readBoolean();
        if (data.readBoolean()) {
            this.heldItem = ByteBufUtils.readItemStack(data);
        }
        if ((statusIndex = data.readShort()) > -1) {
            this.status = StatusType.getEffect(statusIndex);
        }
        this.ability = ByteBufUtils.readUTF8String(data);
        this.isEgg = data.readBoolean();
        this.eggCycles = data.readInt();
        this.steps = data.readInt();
        this.eggDescription = ByteBufUtils.readUTF8String(data);
        this.specialTexture = data.readShort();
        this.isInRanch = data.readBoolean();
        this.form = data.readShort();
        for (i = 0; i < this.evs.length; ++i) {
            this.evs[i] = data.readShort();
        }
        for (i = 0; i < this.ivs.length; ++i) {
            this.ivs[i] = data.readShort();
        }
        for (i = 0; i < this.caps.length; ++i) {
            this.caps[i] = data.readBoolean();
        }
        short type1 = data.readShort();
        this.type1 = type1 == -1 ? null : EnumType.parseOrNull(type1);
        short type2 = data.readShort();
        this.type2 = type2 == -1 ? null : EnumType.parseOrNull(type2);
        this.eggMoves = new int[data.readShort()];
        for (int i3 = 0; i3 < this.eggMoves.length; ++i3) {
            this.eggMoves[i3] = data.readInt();
        }
        this.inBattle = data.readBoolean();
        this.numClones = data.readInt();
        this.numEnchanted = data.readInt();
        this.numWormholes = data.readInt();
        this.numAbundantActivations = data.readInt();
        this.OTUUID = ByteBufUtils.readUTF8String(data);
        this.totem = data.readBoolean();
        this.customTexture = ByteBufUtils.readUTF8String(data);
        if (this.customTexture.equals("null")) {
            this.customTexture = null;
        }
        this.mark = EnumMark.values()[data.readInt()];
    }

    public BaseStats getBaseStats() {
        return Entity3HasStats.getBaseStats(this.name, (int)this.form).orElse(null);
    }

    public ExternalMoveData[] getExternalMoves() {
        if (this.resetMoves) {
            ExternalMoveData[] prevExternalMoves = this.externalMoves;
            this.externalMoves = ExternalMoves.load(this);
            if (prevExternalMoves != null) {
                block0: for (ExternalMoveData prevData : prevExternalMoves) {
                    for (ExternalMoveData currentData : this.externalMoves) {
                        if (prevData.moveIndex != currentData.moveIndex) continue;
                        currentData.timeLastUsed = prevData.timeLastUsed;
                        continue block0;
                    }
                }
            }
            this.resetMoves = false;
        }
        return this.externalMoves;
    }

    public float[] getStatusTexture() {
        if (this.status == null) {
            return new float[]{-1.0f, -1.0f};
        }
        return StatusType.getTexturePos(this.status);
    }

    public String getNickname() {
        if (this.isEgg) {
            return Entity1Base.getLocalizedName("Egg");
        }
        if (PixelmonConfig.allowNicknames && this.nickname != null && !this.nickname.isEmpty()) {
            return this.nickname;
        }
        return Entity1Base.getLocalizedName(this.name);
    }

    public String getEscapedNickname() {
        return Matcher.quoteReplacement(this.getNickname());
    }

    public boolean isMegaEvolved() {
        return PixelmonWrapper.isMegaEvolved(this.heldItem, this.getSpecies(), this.form);
    }

    public boolean canMegaEvolve() {
        if (this.getSpecies().equals((Object)EnumSpecies.Rayquaza)) {
            return Arrays.stream(this.moveset).filter(Objects::nonNull).map(PixelmonMovesetData::getAttack).anyMatch(attack -> attack.isAttack("Dragon Ascent", "attack.dragon ascent.name"));
        }
        return PixelmonWrapper.canMegaEvolve(this.heldItem, this.getSpecies(), (int)this.form);
    }

    public boolean canUltraBurst() {
        return PixelmonWrapper.canUltraBurst(HeldItem.getItemHeld(this.heldItem), this.getSpecies(), this.form);
    }

    public float getExpFraction() {
        return Level.getExpFraction(this.xp, this.nextLvlXP);
    }

    public EnumType getTypeFromItem() {
        Item item = this.heldItem.getItem();
        switch (this.species) {
            case Arceus: {
                if (item instanceof ZCrystal) {
                    return ((ZCrystal)item).crystalType.getType();
                }
                if (item instanceof ItemPlate) {
                    return ((ItemPlate)item).getType();
                }
            }
            case Silvally: {
                if (!(item instanceof MemoryDrive)) break;
                return ((MemoryDrive)item).getType();
            }
        }
        return EnumType.Normal;
    }
}

