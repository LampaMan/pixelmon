/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.core.enums.battle.EnumBattleType;
import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import java.util.UUID;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class StartSpectate
implements IMessage {
    private EnumBattleType battleType = EnumBattleType.Single;
    private UUID uuid;

    public StartSpectate() {
    }

    public StartSpectate(UUID spectatedPlayer, EnumBattleType battleType) {
        this.battleType = battleType;
        this.uuid = spectatedPlayer;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.battleType = EnumBattleType.values()[buf.readInt()];
        this.uuid = new UUID(buf.readLong(), buf.readLong());
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.battleType.ordinal());
        buf.writeLong(this.uuid.getMostSignificantBits());
        buf.writeLong(this.uuid.getLeastSignificantBits());
    }

    public static class Handler
    implements IMessageHandler<StartSpectate, IMessage> {
        @Override
        public IMessage onMessage(StartSpectate message, MessageContext ctx) {
            ClientProxy.battleManager.spectatingUUID = message.uuid;
            Minecraft.getMinecraft().addScheduledTask(() -> ClientProxy.battleManager.startSpectate(message.battleType));
            return null;
        }
    }
}

