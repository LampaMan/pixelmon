/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.client.gui.toast.PixelmonFrameType;
import com.pixelmongenerations.core.Pixelmon;
import io.netty.buffer.ByteBuf;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PixelmonCustomToast
implements IMessage {
    public String title;
    public String body;
    public ItemStack itemStack;
    public PixelmonFrameType frameType;

    public PixelmonCustomToast() {
    }

    public PixelmonCustomToast(String title, String body, ItemStack itemStack, PixelmonFrameType frameType) {
        this.title = title;
        this.body = body;
        this.itemStack = itemStack;
        this.frameType = frameType;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, this.title);
        ByteBufUtils.writeUTF8String(buf, this.body);
        ByteBufUtils.writeItemStack(buf, this.itemStack);
        ByteBufUtils.writeUTF8String(buf, this.frameType.getName());
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.title = ByteBufUtils.readUTF8String(buf);
        this.body = ByteBufUtils.readUTF8String(buf);
        this.itemStack = ByteBufUtils.readItemStack(buf);
        this.frameType = PixelmonFrameType.byName(ByteBufUtils.readUTF8String(buf));
    }

    public static class Handler
    implements IMessageHandler<PixelmonCustomToast, IMessage> {
        @Override
        public IMessage onMessage(PixelmonCustomToast message, MessageContext ctx) {
            Pixelmon.PROXY.openToast(message.title, message.body, message.itemStack, message.frameType);
            return null;
        }
    }
}

