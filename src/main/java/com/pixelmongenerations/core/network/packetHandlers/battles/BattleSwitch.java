/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class BattleSwitch
implements IMessage {
    @Override
    public void fromBytes(ByteBuf buf) {
    }

    @Override
    public void toBytes(ByteBuf buf) {
    }

    public static class Handler
    implements IMessageHandler<BattleSwitch, IMessage> {
        @Override
        public IMessage onMessage(BattleSwitch message, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(() -> {
                ClientProxy.battleManager.mode = BattleMode.EnforcedSwitch;
                ClientProxy.battleManager.oldMode = BattleMode.MainMenu;
            });
            return null;
        }
    }
}

