/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.dialogue;

import com.pixelmongenerations.api.dialogue.Dialogue;
import com.pixelmongenerations.client.gui.dialogue.GuiDialogue;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class DialogueNextAction
implements IMessage {
    public DialogueGuiAction action;
    private int numDialogues = 0;
    public ArrayList<Dialogue> newDialogues = null;

    public DialogueNextAction() {
    }

    public DialogueNextAction(DialogueGuiAction action, ArrayList<Dialogue> newDialogues) {
        this.action = action;
        this.newDialogues = newDialogues;
        this.numDialogues = newDialogues == null ? 0 : newDialogues.size();
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.action = DialogueGuiAction.values()[buffer.readInt()];
        if (this.action == DialogueGuiAction.NEW_DIALOGUES || this.action == DialogueGuiAction.INSERT_DIALOGUES) {
            this.newDialogues = new ArrayList();
            this.numDialogues = buffer.readInt();
            while (this.numDialogues-- > 0) {
                this.newDialogues.add(new Dialogue(buffer));
            }
        }
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.action.ordinal());
        buffer.writeInt(this.numDialogues);
        if (this.numDialogues > 0) {
            for (Dialogue dialogue : this.newDialogues) {
                dialogue.writeToBytes(buffer);
            }
        }
    }

    public static enum DialogueGuiAction {
        CLOSE,
        CONTINUE,
        NEW_DIALOGUES,
        INSERT_DIALOGUES;

    }

    public static class Handler
    implements IMessageHandler<DialogueNextAction, IMessage> {
        @Override
        public IMessage onMessage(DialogueNextAction message, MessageContext ctx) {
            GuiScreen openScreen = Minecraft.getMinecraft().currentScreen;
            if (openScreen instanceof GuiDialogue) {
                GuiDialogue dialogueScreen = (GuiDialogue)openScreen;
                if (message.action == DialogueGuiAction.CLOSE) {
                    dialogueScreen.close();
                } else {
                    GuiDialogue.removeImmediateDialogue();
                    if (message.action != DialogueGuiAction.CONTINUE) {
                        ArrayList<Dialogue> dialogues = new ArrayList<Dialogue>(message.newDialogues);
                        if (message.action == DialogueGuiAction.NEW_DIALOGUES) {
                            GuiDialogue.setDialogues(dialogues);
                        } else if (message.action == DialogueGuiAction.INSERT_DIALOGUES) {
                            GuiDialogue.insertDialogues(dialogues);
                        }
                    }
                    dialogueScreen.next();
                }
            }
            return null;
        }
    }
}

