/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.dialogue;

import com.pixelmongenerations.api.dialogue.Dialogue;
import com.pixelmongenerations.client.gui.dialogue.GuiDialogue;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SetDialogueData
implements IMessage {
    private boolean openGui = false;
    private int numDialogues = 0;
    private ArrayList<Dialogue> dialogues = new ArrayList();

    public SetDialogueData() {
    }

    public SetDialogueData(ArrayList<Dialogue> dialogues) {
        if (dialogues != null) {
            this.numDialogues = dialogues.size();
        }
        this.dialogues = dialogues;
    }

    public SetDialogueData(ArrayList<Dialogue> dialogues, boolean openGui) {
        this(dialogues);
        this.openGui = openGui;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.openGui = buffer.readBoolean();
        this.numDialogues = buffer.readInt();
        for (int i = 0; i < this.numDialogues; ++i) {
            this.dialogues.add(new Dialogue(buffer));
        }
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeBoolean(this.openGui);
        buffer.writeInt(this.numDialogues);
        for (Dialogue dialogue : this.dialogues) {
            dialogue.writeToBytes(buffer);
        }
    }

    public static class Handler
    implements IMessageHandler<SetDialogueData, IMessage> {
        @Override
        public IMessage onMessage(SetDialogueData message, MessageContext ctx) {
            GuiDialogue.setDialogues(message.dialogues);
            if (message.openGui) {
                Minecraft.getMinecraft().addScheduledTask(() -> Minecraft.getMinecraft().player.openGui(Pixelmon.INSTANCE, EnumGui.Dialogue.getIndex(), Minecraft.getMinecraft().player.world, 0, 0, 0));
            }
            return null;
        }
    }
}

