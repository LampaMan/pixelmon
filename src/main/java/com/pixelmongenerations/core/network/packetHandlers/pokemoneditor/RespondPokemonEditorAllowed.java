/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pokemoneditor;

import com.pixelmongenerations.common.item.ItemPokemonEditor;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.SetEditedPlayer;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.SetEditingPlayer;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class RespondPokemonEditorAllowed
implements IMessage {
    UUID editingPlayer;
    boolean allowPokemonEditors;

    public RespondPokemonEditorAllowed() {
    }

    public RespondPokemonEditorAllowed(UUID editingPlayer, boolean allowPokemonEditors) {
        this.editingPlayer = editingPlayer;
        this.allowPokemonEditors = allowPokemonEditors;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        PixelmonMethods.toBytesUUID(buf, this.editingPlayer);
        buf.writeBoolean(this.allowPokemonEditors);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.editingPlayer = PixelmonMethods.fromBytesUUID(buf);
        this.allowPokemonEditors = buf.readBoolean();
    }

    public static class Handler
    implements IMessageHandler<RespondPokemonEditorAllowed, IMessage> {
        @Override
        public IMessage onMessage(RespondPokemonEditorAllowed message, MessageContext ctx) {
            MinecraftServer server = ctx.getServerHandler().player.getServer();
            EntityPlayerMP editingPlayer = server.getPlayerList().getPlayerByUUID(message.editingPlayer);
            EntityPlayerMP editedPlayer = ctx.getServerHandler().player;
            if (editingPlayer != null) {
                if (!ItemPokemonEditor.checkPermission(editingPlayer)) {
                    return null;
                }
                if (message.allowPokemonEditors) {
                    Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(editedPlayer);
                    if (optstorage.isPresent()) {
                        PlayerStorage storage = optstorage.get();
                        server.addScheduledTask(() -> {
                            storage.recallAllPokemon();
                            Pixelmon.NETWORK.sendTo(new SetEditingPlayer(editingPlayer.getUniqueID(), editingPlayer.getDisplayNameString()), editedPlayer);
                            editedPlayer.openGui(Pixelmon.INSTANCE, EnumGui.EditedPlayer.ordinal(), editedPlayer.world, 0, 0, 0);
                            Pixelmon.NETWORK.sendTo(new SetEditedPlayer(editedPlayer.getUniqueID(), editedPlayer.getDisplayNameString(), storage.convertToData()), editingPlayer);
                            editingPlayer.openGui(Pixelmon.INSTANCE, EnumGui.PokemonEditor.ordinal(), editingPlayer.world, 0, 0, 0);
                        });
                    }
                } else {
                    ChatHandler.sendChat(editingPlayer, I18n.translateToLocal("gui.pokemoneditor.notallowedplayer"), editedPlayer.getDisplayNameString());
                }
            }
            return null;
        }
    }
}

