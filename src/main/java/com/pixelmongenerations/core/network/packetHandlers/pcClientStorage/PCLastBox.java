/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pcClientStorage;

import com.pixelmongenerations.core.storage.PCClientStorage;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PCLastBox
implements IMessage {
    private int lastBox = 0;

    public PCLastBox() {
    }

    public PCLastBox(int lastBox) {
        this.lastBox = lastBox;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.lastBox = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.lastBox);
    }

    public static class Handler
    implements IMessageHandler<PCLastBox, IMessage> {
        @Override
        public IMessage onMessage(PCLastBox message, MessageContext ctx) {
            PCClientStorage.setLastBoxOpen(message.lastBox);
            return null;
        }
    }
}

