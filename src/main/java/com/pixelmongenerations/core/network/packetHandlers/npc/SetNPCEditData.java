/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.npc;

import com.pixelmongenerations.client.gui.npcEditor.GuiChattingNPCEditor;
import com.pixelmongenerations.client.gui.npcEditor.GuiShopkeeperEditor;
import com.pixelmongenerations.client.gui.npcEditor.GuiTrainerEditor;
import com.pixelmongenerations.core.enums.EnumNPCType;
import com.pixelmongenerations.core.network.SetTrainerData;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SetNPCEditData
implements IMessage {
    SetTrainerData data;
    EnumNPCType npcType;
    ArrayList<String> chatPages;
    String name;
    String json;
    String texture;

    public SetNPCEditData() {
    }

    public SetNPCEditData(SetTrainerData data) {
        this.npcType = EnumNPCType.Trainer;
        this.data = data;
    }

    public SetNPCEditData(String name, ArrayList<String> chatPages) {
        this.name = name;
        this.npcType = EnumNPCType.ChattingNPC;
        this.chatPages = chatPages;
    }

    public SetNPCEditData(String json, String name, String texture) {
        this.json = json;
        this.name = name;
        this.texture = texture;
        this.npcType = EnumNPCType.Shopkeeper;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeShort(this.npcType.ordinal());
        switch (this.npcType) {
            case Trainer: {
                this.data.encodeInto(buffer);
                break;
            }
            case ChattingNPC: {
                ByteBufUtils.writeUTF8String(buffer, this.name);
                buffer.writeShort(this.chatPages.size());
                for (String page : this.chatPages) {
                    ByteBufUtils.writeUTF8String(buffer, page);
                }
                break;
            }
            case Shopkeeper: {
                ByteBufUtils.writeUTF8String(buffer, this.name);
                ByteBufUtils.writeUTF8String(buffer, this.json);
                ByteBufUtils.writeUTF8String(buffer, this.texture);
                break;
            }
        }
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.npcType = EnumNPCType.getFromOrdinal(buffer.readShort());
        switch (this.npcType) {
            case Trainer: {
                this.data = new SetTrainerData();
                this.data.decodeInto(buffer);
                break;
            }
            case ChattingNPC: {
                this.name = ByteBufUtils.readUTF8String(buffer);
                this.chatPages = new ArrayList();
                int numPages = buffer.readShort();
                for (int i = 0; i < numPages; ++i) {
                    this.chatPages.add(ByteBufUtils.readUTF8String(buffer));
                }
                break;
            }
            case Shopkeeper: {
                this.name = ByteBufUtils.readUTF8String(buffer);
                this.json = ByteBufUtils.readUTF8String(buffer);
                this.texture = ByteBufUtils.readUTF8String(buffer);
                break;
            }
        }
    }

    public static class Handler
    implements IMessageHandler<SetNPCEditData, IMessage> {
        @Override
        public IMessage onMessage(SetNPCEditData message, MessageContext ctx) {
            switch (message.npcType) {
                case Trainer: {
                    GuiTrainerEditor.trainerData = message.data;
                    break;
                }
                case ChattingNPC: {
                    GuiChattingNPCEditor.name = message.name;
                    GuiChattingNPCEditor.chatPages = message.chatPages;
                    GuiChattingNPCEditor.chatChanged = true;
                    break;
                }
                case Shopkeeper: {
                    Minecraft mc = Minecraft.getMinecraft();
                    mc.addScheduledTask(() -> {
                        GuiShopkeeperEditor.json = message.json;
                        GuiShopkeeperEditor.name = message.name;
                        if (mc.currentScreen instanceof GuiShopkeeperEditor) {
                            ((GuiShopkeeperEditor)mc.currentScreen).updateShopkeeper(message.texture);
                        }
                    });
                    break;
                }
            }
            return null;
        }
    }
}

