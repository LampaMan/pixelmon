/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.customOverlays;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.client.gui.custom.overlays.CustomNoticeOverlay;
import com.pixelmongenerations.client.gui.custom.overlays.GraphicPlacement;
import com.pixelmongenerations.client.gui.custom.overlays.GraphicType;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.Collection;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class CustomNoticePacket
implements IMessage {
    private boolean enabled = true;
    private Collection<String> lines;
    private GraphicType graphicType = null;
    private GraphicPlacement graphicDisplayType = null;
    private ItemStack itemStack = null;
    private NBTTagCompound pokemonTag;
    private int borderColor = 255;
    private int backgroundColor = 255;

    public CustomNoticePacket() {
    }

    public CustomNoticePacket(Collection<String> lines) {
        this.lines = lines;
    }

    public CustomNoticePacket setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public CustomNoticePacket setText(Collection<String> lines) {
        this.lines = lines;
        return this;
    }

    public CustomNoticePacket setPokemonSprite(PokemonSpec pokemon, GraphicPlacement displayType) {
        this.graphicType = GraphicType.PokemonSprite;
        this.graphicDisplayType = displayType;
        NBTTagCompound compound = new NBTTagCompound();
        pokemon.apply(compound);
        this.pokemonTag = compound;
        return this;
    }

    public CustomNoticePacket setPokemon3D(PokemonSpec pokemon, GraphicPlacement displayType) {
        this.graphicType = GraphicType.Pokemon3D;
        this.graphicDisplayType = displayType;
        NBTTagCompound compound = new NBTTagCompound();
        pokemon.apply(compound);
        this.pokemonTag = compound;
        return this;
    }

    public CustomNoticePacket setItemStack(ItemStack itemStack, GraphicPlacement displayType) {
        this.graphicType = GraphicType.ItemSprite;
        this.graphicDisplayType = displayType;
        this.itemStack = itemStack;
        return this;
    }

    public CustomNoticePacket setItemStack3D(ItemStack itemStack, GraphicPlacement displayType) {
        this.graphicType = GraphicType.Item3D;
        this.graphicDisplayType = displayType;
        this.itemStack = itemStack;
        return this;
    }

    public CustomNoticePacket setBorderColor(int borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public CustomNoticePacket setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
        return this;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.enabled = buf.readBoolean();
        if (!this.enabled) {
            return;
        }
        int lineAmount = buf.readByte();
        this.lines = new ArrayList<String>(lineAmount);
        for (int i = 0; i < lineAmount; ++i) {
            this.lines.add(ByteBufUtils.readUTF8String(buf));
        }
        this.borderColor = buf.readInt();
        this.backgroundColor = buf.readInt();
        if (buf.readBoolean()) {
            this.graphicType = GraphicType.values()[buf.readByte()];
            this.graphicDisplayType = GraphicPlacement.values()[buf.readByte()];
            switch (this.graphicType) {
                case PokemonSprite: 
                case Pokemon3D: {
                    this.pokemonTag = ByteBufUtils.readTag(buf);
                    break;
                }
                case ItemSprite: 
                case Item3D: {
                    this.itemStack = ByteBufUtils.readItemStack(buf);
                }
            }
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeBoolean(this.enabled);
        if (!this.enabled) {
            return;
        }
        buf.writeByte(this.lines.size());
        for (String line : this.lines) {
            ByteBufUtils.writeUTF8String(buf, line);
        }
        buf.writeInt(this.borderColor);
        buf.writeInt(this.backgroundColor);
        buf.writeBoolean(this.graphicType != null);
        if (this.graphicType != null) {
            buf.writeByte(this.graphicType.ordinal());
            buf.writeByte(this.graphicDisplayType.ordinal());
            switch (this.graphicType) {
                case PokemonSprite: 
                case Pokemon3D: {
                    ByteBufUtils.writeTag(buf, this.pokemonTag);
                    break;
                }
                case ItemSprite: 
                case Item3D: {
                    ByteBufUtils.writeItemStack(buf, this.itemStack);
                }
            }
        }
    }

    public static class Handler
    implements IMessageHandler<CustomNoticePacket, IMessage> {
        @Override
        public IMessage onMessage(CustomNoticePacket message, MessageContext ctx) {
            if (!message.enabled) {
                CustomNoticeOverlay.setEnabled(false);
                return null;
            }
            CustomNoticeOverlay.resetNotice();
            CustomNoticeOverlay.populate(message.lines);
            CustomNoticeOverlay.setBorderColor(message.borderColor);
            CustomNoticeOverlay.setBackgroundColor(message.backgroundColor);
            if (message.graphicType != null) {
                switch (message.graphicType) {
                    case PokemonSprite: {
                        CustomNoticeOverlay.setPokemonSprite(message.pokemonTag, message.graphicDisplayType);
                        break;
                    }
                    case Pokemon3D: {
                        CustomNoticeOverlay.setPokemon3D(message.pokemonTag, message.graphicDisplayType);
                        break;
                    }
                    case ItemSprite: {
                        CustomNoticeOverlay.setItemSprite(message.itemStack, message.graphicDisplayType);
                        break;
                    }
                    case Item3D: {
                        CustomNoticeOverlay.setItem3D(message.itemStack, message.graphicDisplayType);
                    }
                }
            }
            CustomNoticeOverlay.setEnabled(true);
            return null;
        }
    }
}

