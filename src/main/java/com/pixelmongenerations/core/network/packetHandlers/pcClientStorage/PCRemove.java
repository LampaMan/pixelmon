/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pcClientStorage;

import com.pixelmongenerations.core.storage.PCClientStorage;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PCRemove
implements IMessage {
    int box;
    int pos;

    public PCRemove() {
    }

    public PCRemove(int box, int pos) {
        this.box = box;
        this.pos = pos;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.box);
        buffer.writeInt(this.pos);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.box = buffer.readInt();
        this.pos = buffer.readInt();
    }

    public static class Handler
    implements IMessageHandler<PCRemove, IMessage> {
        @Override
        public IMessage onMessage(PCRemove message, MessageContext ctx) {
            PCClientStorage.removeFromList(message.box, message.pos);
            return null;
        }
    }
}

