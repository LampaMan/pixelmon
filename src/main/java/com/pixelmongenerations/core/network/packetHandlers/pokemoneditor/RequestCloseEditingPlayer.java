/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pokemoneditor;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.CloseEditingPlayer;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class RequestCloseEditingPlayer
implements IMessage {
    UUID editingPlayer;

    public RequestCloseEditingPlayer() {
    }

    public RequestCloseEditingPlayer(UUID editingPlayer) {
        this.editingPlayer = editingPlayer;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        PixelmonMethods.toBytesUUID(buf, this.editingPlayer);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.editingPlayer = PixelmonMethods.fromBytesUUID(buf);
    }

    public static class Handler
    implements IMessageHandler<RequestCloseEditingPlayer, IMessage> {
        @Override
        public IMessage onMessage(RequestCloseEditingPlayer message, MessageContext ctx) {
            EntityPlayerMP editingPlayer = ctx.getServerHandler().player.getServer().getPlayerList().getPlayerByUUID(message.editingPlayer);
            if (editingPlayer != null) {
                ChatHandler.sendChat(editingPlayer, I18n.translateToLocal("gui.pokemoneditor.cancelediting"), ctx.getServerHandler().player.getDisplayNameString());
                Pixelmon.NETWORK.sendTo(new CloseEditingPlayer(), editingPlayer);
            }
            return null;
        }
    }
}

