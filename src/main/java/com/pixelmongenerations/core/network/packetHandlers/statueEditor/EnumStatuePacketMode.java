/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.network.packetHandlers.statueEditor;

import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.enums.EnumStatueTextureType;

public enum EnumStatuePacketMode {
    SetName,
    SetGrowth,
    SetLabel,
    SetTextureType,
    SetAnimation,
    SetModelStanding,
    SetAnimationFrame,
    SetForm,
    SetSpecial,
    SetCustomSpecial,
    SetAdminPlaced,
    SetAnimated,
    SetGmaxModel;


    public static EnumStatuePacketMode getFromOrdinal(int value) {
        for (EnumStatuePacketMode m : EnumStatuePacketMode.values()) {
            if (m.ordinal() != value) continue;
            return m;
        }
        return null;
    }

    public Object chooseValueForMode(String stringValue, Boolean boolValue, Integer intValue) {
        switch (this) {
            case SetName: 
            case SetAnimation: 
            case SetCustomSpecial: 
            case SetLabel: {
                return stringValue;
            }
            case SetGrowth: {
                return EnumGrowth.growthFromString(stringValue);
            }
            case SetTextureType: {
                return EnumStatueTextureType.getFromString(stringValue);
            }
            case SetAdminPlaced: 
            case SetModelStanding: 
            case SetGmaxModel: 
            case SetAnimated: {
                return boolValue;
            }
            case SetAnimationFrame: 
            case SetSpecial: 
            case SetForm: {
                return intValue;
            }
        }
        return null;
    }
}

