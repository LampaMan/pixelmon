/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.itemDrops;

import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemQueryList;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ServerItemDropPacket
implements IMessage {
    int itemID;
    PacketMode mode;

    public ServerItemDropPacket() {
    }

    public ServerItemDropPacket(PacketMode mode) {
        this.mode = mode;
    }

    public ServerItemDropPacket(int itemID) {
        this.mode = PacketMode.TakeItem;
        this.itemID = itemID;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.mode = PacketMode.values()[buffer.readInt()];
        if (this.mode == PacketMode.TakeItem) {
            this.itemID = buffer.readInt();
        }
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.mode.ordinal());
        if (this.mode == PacketMode.TakeItem) {
            buffer.writeInt(this.itemID);
        }
    }

    public static enum PacketMode {
        DropAllItems,
        TakeAllItems,
        TakeItem;

    }

    public static class Handler
    implements IMessageHandler<ServerItemDropPacket, IMessage> {
        @Override
        public IMessage onMessage(ServerItemDropPacket message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            player.getServer().addScheduledTask(() -> {
                switch (message.mode) {
                    case DropAllItems: {
                        DropItemQueryList.dropAllItems(player);
                        break;
                    }
                    case TakeAllItems: {
                        DropItemQueryList.takeAllItems(player);
                        break;
                    }
                    case TakeItem: {
                        DropItemQueryList.takeItem(player, message.itemID);
                    }
                }
            });
            return null;
        }
    }
}

