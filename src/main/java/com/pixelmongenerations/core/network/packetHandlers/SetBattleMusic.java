/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.core.enums.battle.EnumBattleMusic;
import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SetBattleMusic
implements IMessage {
    private EnumBattleMusic battleSong;

    public SetBattleMusic() {
        this(EnumBattleMusic.None);
    }

    public SetBattleMusic(EnumBattleMusic music) {
        this.battleSong = music;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.battleSong = EnumBattleMusic.values()[buf.readInt()];
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.battleSong.ordinal());
    }

    public static class Handler
    implements IMessageHandler<SetBattleMusic, IMessage> {
        @Override
        public IMessage onMessage(SetBattleMusic message, MessageContext ctx) {
            ClientProxy.battleManager.battleSong = message.battleSong;
            return null;
        }
    }
}

