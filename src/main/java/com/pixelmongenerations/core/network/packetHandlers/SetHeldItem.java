/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.api.events.HeldItemChangeEvent;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.helpers.EvolutionQueryList;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.heldItems.ItemMegaStone;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumGreninja;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SetHeldItem
implements IMessage {
    int[] pokemonID;
    Item item;

    public SetHeldItem() {
    }

    public SetHeldItem(int[] pokemonID) {
        this.pokemonID = pokemonID;
    }

    public void setItem(Item item) {
        if (item == Items.AIR) {
            this.item = null;
        }
        this.item = item;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.pokemonID = new int[]{buffer.readInt(), buffer.readInt()};
        boolean hasItem = buffer.readBoolean();
        this.item = hasItem ? Item.getByNameOrId(ByteBufUtils.readUTF8String(buffer)) : null;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.pokemonID[0]);
        buffer.writeInt(this.pokemonID[1]);
        buffer.writeBoolean(this.item != null);
        if (this.item != null) {
            ByteBufUtils.writeUTF8String(buffer, this.item.getRegistryName().toString());
        }
    }

    public static class Handler
    implements IMessageHandler<SetHeldItem, IMessage> {
        @Override
        public IMessage onMessage(final SetHeldItem message, MessageContext ctx) {
            final EntityPlayerMP player = ctx.getServerHandler().player;
            player.getServer().addScheduledTask(new Runnable(){

                @Override
                public void run() {
                    HeldItemChangeEvent event;
                    InventoryPlayer inventory = player.inventory;
                    Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
                    if (!optstorage.isPresent()) {
                        return;
                    }
                    PlayerStorage storage = optstorage.get();
                    if (storage.isOffline()) {
                        return;
                    }
                    int[] pokemonID = message.pokemonID;
                    NBTTagCompound nbt = storage.getNBT(pokemonID);
                    if (nbt == null) {
                        return;
                    }
                    boolean creative = player.isCreative();
                    ItemStack newHeldItem = ItemStack.EMPTY;
                    ItemStack currentItem = inventory.getItemStack().copy();
                    ItemStack giveItem = null;
                    ItemStack singleItem = null;
                    if (creative) {
                        newHeldItem = message.item == null ? ItemStack.EMPTY : new ItemStack(message.item, 1);
                    } else {
                        ItemStack oldItem = new ItemStack(nbt.getCompoundTag("HeldItemStack"));
                        if (oldItem.isEmpty()) {
                            if (!currentItem.isEmpty()) {
                                singleItem = currentItem.copy();
                                singleItem.setCount(1);
                                newHeldItem = singleItem;
                                currentItem.shrink(1);
                            }
                        } else if (currentItem.isEmpty()) {
                            currentItem = oldItem;
                            newHeldItem = ItemStack.EMPTY;
                        } else if (oldItem.getItem() == currentItem.getItem()) {
                            newHeldItem = ItemStack.EMPTY;
                            currentItem.grow(1);
                        } else {
                            singleItem = currentItem.copy();
                            singleItem.setCount(1);
                            newHeldItem = singleItem;
                            if (currentItem.getCount() <= 1) {
                                currentItem = oldItem;
                            } else {
                                currentItem.shrink(1);
                                giveItem = oldItem;
                            }
                        }
                    }
                    Optional<EntityPixelmon> pixelmonOptional = storage.getAlreadyExists(pokemonID, player.world);
                    if (newHeldItem.isEmpty()) {
                        newHeldItem = ItemStack.EMPTY;
                    }
                    if (MinecraftForge.EVENT_BUS.post(event = new HeldItemChangeEvent(player, pixelmonOptional, Optional.of(nbt), newHeldItem))) {
                        return;
                    }
                    inventory.setItemStack(currentItem);
                    if (giveItem != null && !inventory.addItemStackToInventory(giveItem)) {
                        player.dropItem(giveItem, false);
                    }
                    if (pixelmonOptional.isPresent()) {
                        EntityPixelmon pokemon = pixelmonOptional.get();
                        pokemon.heldItem = event.getItem();
                    }
                    if (newHeldItem.isEmpty()) {
                        EvolutionQueryList.declineQuery(player, pokemonID);
                        nbt.removeTag("HeldItemStack");
                        EntityPixelmon pixelmon = storage.getPokemon(pokemonID, player.world);
                        pixelmon.setHeldItem(ItemStack.EMPTY);
                        storage.updateNBT(pixelmon);
                    } else {
                        nbt.setTag("HeldItemStack", event.getItem().writeToNBT(new NBTTagCompound()));
                    }
                    PixelmonStorage.pokeBallManager.savePlayer(player.getServer(), storage);
                    storage.sendUpdatedList();
                }
            });
            return null;
        }

        public boolean isCorrectMegaItem(EntityPixelmon user, ItemHeld heldItem) {
            PlayerStorage storage = user.getStorage().orElse(null);
            if (storage == null) {
                return false;
            }
            EnumSpecies species = user.getSpecies();
            if (species == EnumSpecies.Kyogre && heldItem == PixelmonItemsHeld.blueOrb || species == EnumSpecies.Groudon && user.getItemHeld() == PixelmonItemsHeld.redOrb) {
                return true;
            }
            if (species == EnumSpecies.Greninja && user.getFormEnum() == EnumGreninja.BATTLE_BOND) {
                return true;
            }
            if (species == EnumSpecies.Rayquaza && EnumSpecies.Rayquaza.hasMega()) {
                return true;
            }
            ItemHeld held = user.getItemHeld();
            if (!(held instanceof ItemMegaStone) || ((ItemMegaStone)held).pokemon != species) {
                return false;
            }
            ItemMegaStone stone = (ItemMegaStone)held;
            return storage.megaData.getMegaItem().canEvolve();
        }
    }
}

