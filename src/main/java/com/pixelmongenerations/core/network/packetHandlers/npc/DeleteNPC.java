/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.npc;

import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.item.ItemNPCEditor;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class DeleteNPC
implements IMessage {
    int trainerId;

    public DeleteNPC() {
    }

    public DeleteNPC(int trainerId) {
        this.trainerId = trainerId;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.trainerId);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.trainerId = buffer.readInt();
    }

    public static class Handler
    implements IMessageHandler<DeleteNPC, IMessage> {
        @Override
        public IMessage onMessage(DeleteNPC message, MessageContext ctx) {
            EntityPlayerMP p = ctx.getServerHandler().player;
            if (!ItemNPCEditor.checkPermission(p)) {
                return null;
            }
            Optional<EntityNPC> npc = EntityNPC.locateNPCServer(p.world, message.trainerId, EntityNPC.class);
            if (npc.isPresent()) {
                npc.get().unloadEntity();
            }
            return null;
        }
    }
}

