/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles.rules;

import com.pixelmongenerations.common.battle.rules.teamselection.TeamSelection;
import com.pixelmongenerations.common.battle.rules.teamselection.TeamSelectionList;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class UnconfirmTeamSelect
implements IMessage {
    int teamSelectID;

    public UnconfirmTeamSelect() {
    }

    public UnconfirmTeamSelect(int teamSelectID) {
        this.teamSelectID = teamSelectID;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.teamSelectID);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.teamSelectID = buf.readInt();
    }

    public static class Handler
    implements IMessageHandler<UnconfirmTeamSelect, IMessage> {
        @Override
        public IMessage onMessage(UnconfirmTeamSelect message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            player.getServer().addScheduledTask(() -> {
                TeamSelection ts = TeamSelectionList.getTeamSelection(message.teamSelectID);
                if (ts == null) {
                    return;
                }
                ts.unregisterTeamSelect(player);
            });
            return null;
        }
    }
}

