/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.client.gui.battles.PixelmonInGui;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SwapPosition
implements IMessage {
    int[] user;
    int[] target;

    public SwapPosition() {
    }

    public SwapPosition(PixelmonWrapper user, PixelmonWrapper target) {
        this.user = user.getPokemonID();
        this.target = target.getPokemonID();
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.user = new int[]{buf.readInt(), buf.readInt()};
        this.target = new int[]{buf.readInt(), buf.readInt()};
    }

    @Override
    public void toBytes(ByteBuf buf) {
        for (int id : this.user) {
            buf.writeInt(id);
        }
        for (int id : this.target) {
            buf.writeInt(id);
        }
    }

    public static class Handler
    implements IMessageHandler<SwapPosition, IMessage> {
        @Override
        public IMessage onMessage(SwapPosition message, MessageContext ctx) {
            PixelmonInGui userPokemon = null;
            PixelmonInGui targetPokemon = null;
            for (PixelmonInGui pokemon : ClientProxy.battleManager.displayedOurPokemon) {
                if (PixelmonMethods.isIDSame(message.user, pokemon.pokemonID)) {
                    userPokemon = pokemon;
                    continue;
                }
                if (!PixelmonMethods.isIDSame(message.target, pokemon.pokemonID)) continue;
                targetPokemon = pokemon;
            }
            for (PixelmonInGui pokemon : ClientProxy.battleManager.displayedEnemyPokemon) {
                if (PixelmonMethods.isIDSame(message.user, pokemon.pokemonID)) {
                    userPokemon = pokemon;
                    continue;
                }
                if (!PixelmonMethods.isIDSame(message.target, pokemon.pokemonID)) continue;
                targetPokemon = pokemon;
            }
            if (userPokemon != null && targetPokemon != null) {
                int userPosition = userPokemon.position;
                userPokemon.position = targetPokemon.position;
                targetPokemon.position = userPosition;
            }
            return null;
        }
    }
}

