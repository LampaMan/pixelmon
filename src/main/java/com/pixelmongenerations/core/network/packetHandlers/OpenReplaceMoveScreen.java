/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.client.gui.GuiEvolve;
import com.pixelmongenerations.client.gui.battles.AttackData;
import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.common.entity.pixelmon.stats.moves.LearnMoveStorage;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import java.util.UUID;
import net.minecraft.client.Minecraft;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class OpenReplaceMoveScreen
implements IMessage {
    public static Item tm = null;
    int[] pokemonId;
    int attackId;
    int replaceIndex;
    int level;
    boolean checkEvo;

    public OpenReplaceMoveScreen() {
    }

    public OpenReplaceMoveScreen(UUID uniqueId, int[] pokemonId, int attackId, int replaceIndex, int level) {
        this(uniqueId, pokemonId, attackId, replaceIndex, level, false);
    }

    public OpenReplaceMoveScreen(UUID uniqueId, int[] pokemonId, int attackId, int replaceIndex, int level, boolean checkEvo) {
        this.pokemonId = pokemonId;
        this.attackId = attackId;
        this.replaceIndex = replaceIndex;
        this.level = level;
        this.checkEvo = checkEvo;
        LearnMoveStorage.getInstance().addPlayerMove(uniqueId, pokemonId, attackId);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.pokemonId = new int[]{buffer.readInt(), buffer.readInt()};
        this.attackId = buffer.readInt();
        this.replaceIndex = buffer.readInt();
        this.level = buffer.readInt();
        this.checkEvo = buffer.readBoolean();
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.pokemonId[0]);
        buffer.writeInt(this.pokemonId[1]);
        buffer.writeInt(this.attackId);
        buffer.writeInt(this.replaceIndex);
        buffer.writeInt(this.level);
        buffer.writeBoolean(this.checkEvo);
    }

    public static class Handler
    implements IMessageHandler<OpenReplaceMoveScreen, IMessage> {
        @Override
        public IMessage onMessage(OpenReplaceMoveScreen message, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(() -> this.processData(message));
            return null;
        }

        private void processData(OpenReplaceMoveScreen message) {
            AttackData attackData = new AttackData(message.pokemonId, DatabaseMoves.getAttack(message.attackId), message.level, message.checkEvo);
            if (ClientProxy.battleManager.newAttackList.contains(attackData)) {
                return;
            }
            ClientProxy.battleManager.newAttackList.add(attackData);
            if (!(Minecraft.getMinecraft().currentScreen instanceof GuiBattle) && !(Minecraft.getMinecraft().currentScreen instanceof GuiEvolve)) {
                Minecraft.getMinecraft().player.openGui(Pixelmon.INSTANCE, EnumGui.LearnMove.getIndex(), Pixelmon.PROXY.getClientWorld(), 0, 0, 0);
            }
        }
    }
}

