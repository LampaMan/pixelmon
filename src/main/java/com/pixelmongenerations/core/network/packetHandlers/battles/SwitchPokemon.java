/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SwitchPokemon
implements IMessage {
    int pos;
    int battleControllerIndex;
    int[] switchingPokemonId;
    boolean happensInstantly;

    public SwitchPokemon() {
    }

    public SwitchPokemon(int pos, int battleControllerIndex, int[] switchingPokemonId, boolean happensInstantly) {
        this.pos = pos;
        this.battleControllerIndex = battleControllerIndex;
        this.switchingPokemonId = switchingPokemonId;
        this.happensInstantly = happensInstantly;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.pos = buffer.readInt();
        this.battleControllerIndex = buffer.readInt();
        this.switchingPokemonId = new int[]{buffer.readInt(), buffer.readInt()};
        this.happensInstantly = buffer.readBoolean();
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.pos);
        buffer.writeInt(this.battleControllerIndex);
        buffer.writeInt(this.switchingPokemonId[0]);
        buffer.writeInt(this.switchingPokemonId[1]);
        buffer.writeBoolean(this.happensInstantly);
    }

    public static class Handler
    implements IMessageHandler<SwitchPokemon, IMessage> {
        @Override
        public IMessage onMessage(SwitchPokemon message, MessageContext ctx) {
            ctx.getServerHandler().player.getServer().addScheduledTask(() -> {
                int[] newPokemonId;
                EntityPlayerMP player = ctx.getServerHandler().player;
                BattleControllerBase bc = BattleRegistry.getBattle(message.battleControllerIndex);
                if (bc == null) {
                    return;
                }
                PlayerParticipant p = bc.getPlayer(player);
                if (p == null) {
                    return;
                }
                if (message.pos == -1) {
                    PixelmonWrapper switching = p.getPokemonFromID(message.switchingPokemonId);
                    if (switching == null) {
                        return;
                    }
                    newPokemonId = p.getBattleAI().getNextSwitch(switching);
                } else {
                    if (p.allPokemon.length <= message.pos) {
                        return;
                    }
                    newPokemonId = p.allPokemon[message.pos].getPokemonID();
                }
                bc.switchPokemon(message.switchingPokemonId, newPokemonId, message.happensInstantly);
            });
            return null;
        }
    }
}

