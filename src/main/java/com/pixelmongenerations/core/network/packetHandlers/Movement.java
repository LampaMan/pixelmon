/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.common.entity.pixelmon.Entity5Rideable;
import com.pixelmongenerations.core.enums.EnumMovement;
import com.pixelmongenerations.core.network.packetHandlers.EnumMovementType;
import com.pixelmongenerations.core.util.IMovementHandler;
import io.netty.buffer.ByteBuf;
import java.util.HashMap;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class Movement
implements IMessage {
    public static HashMap<EntityPlayerMP, EnumMovement[]> movementList = new HashMap();
    static IMovementHandler handler;
    EnumMovementType type;
    EnumMovement[] movement;

    public static void registerMovementHandler(IMovementHandler mHandler) {
        handler = mHandler;
    }

    public Movement() {
    }

    public Movement(EnumMovement[] movement, EnumMovementType type) {
        this.movement = movement;
        this.type = type;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.type = EnumMovementType.getFromOrdinal(buffer.readByte());
        int numMovements = buffer.readShort();
        this.movement = new EnumMovement[numMovements];
        for (int i = 0; i < numMovements; ++i) {
            this.movement[i] = EnumMovement.getMovement(buffer.readShort());
        }
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeByte(this.type.ordinal());
        buffer.writeShort(this.movement.length);
        for (EnumMovement aMovement : this.movement) {
            buffer.writeShort(aMovement.ordinal());
        }
    }

    public static class Handler
    implements IMessageHandler<Movement, IMessage> {
        @Override
        public IMessage onMessage(Movement message, MessageContext ctx) {
            if (message.type == EnumMovementType.Riding) {
                EntityPlayerMP player = ctx.getServerHandler().player;
                if (!(player.getRidingEntity() instanceof Entity5Rideable)) {
                    return null;
                }
                Entity5Rideable mount = (Entity5Rideable)player.getRidingEntity();
                for (EnumMovement movementType : message.movement) {
                    if (movementType == EnumMovement.Jump) {
                        ++mount.ridingPlayerVertical;
                        continue;
                    }
                    if (movementType != EnumMovement.Crouch) continue;
                    --mount.ridingPlayerVertical;
                }
            } else if (handler != null) {
                handler.handleMovement(ctx.getServerHandler().player, message.movement);
            }
            return null;
        }
    }
}

