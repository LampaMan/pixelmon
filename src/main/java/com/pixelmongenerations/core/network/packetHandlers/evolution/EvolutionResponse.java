/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.evolution;

import com.pixelmongenerations.common.entity.pixelmon.helpers.EvolutionQueryList;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class EvolutionResponse
implements IMessage {
    boolean accept;
    int[] pokemonID;
    EvolutionResponseMode mode;

    public EvolutionResponse() {
    }

    public EvolutionResponse(int[] pokemonID, boolean accept) {
        this.pokemonID = pokemonID;
        this.accept = accept;
        this.mode = EvolutionResponseMode.Message;
    }

    public EvolutionResponse(int[] pokemonID) {
        this.mode = EvolutionResponseMode.SpawnPokemon;
        this.pokemonID = pokemonID;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.mode = EvolutionResponseMode.values()[buffer.readInt()];
        this.pokemonID = new int[]{buffer.readInt(), buffer.readInt()};
        if (this.mode == EvolutionResponseMode.Message) {
            this.accept = buffer.readBoolean();
        }
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.mode.ordinal());
        buffer.writeInt(this.pokemonID[0]);
        buffer.writeInt(this.pokemonID[1]);
        if (this.mode == EvolutionResponseMode.Message) {
            buffer.writeBoolean(this.accept);
        }
    }

    static enum EvolutionResponseMode {
        Message,
        SpawnPokemon;

    }

    public static class Handler
    implements IMessageHandler<EvolutionResponse, IMessage> {
        @Override
        public IMessage onMessage(EvolutionResponse message, MessageContext ctx) {
            ctx.getServerHandler().player.getServer().addScheduledTask(() -> {
                if (message.mode == EvolutionResponseMode.Message) {
                    if (message.accept) {
                        EvolutionQueryList.acceptQuery(ctx.getServerHandler().player, message.pokemonID);
                    } else {
                        EvolutionQueryList.declineQuery(ctx.getServerHandler().player, message.pokemonID);
                    }
                } else {
                    EvolutionQueryList.spawnPokemon(ctx.getServerHandler().player, message.pokemonID);
                }
            });
            return null;
        }
    }
}

