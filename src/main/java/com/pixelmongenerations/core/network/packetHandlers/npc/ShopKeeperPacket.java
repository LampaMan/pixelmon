/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.npc;

import com.pixelmongenerations.api.events.npc.NPCTransactionEvent;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCShopkeeper;
import com.pixelmongenerations.common.entity.npcs.registry.EnumBuySell;
import com.pixelmongenerations.common.entity.npcs.registry.ShopItemWithVariation;
import com.pixelmongenerations.common.item.ItemPokeball;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonItemsPokeballs;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.network.packetHandlers.clientStorage.UpdateClientPlayerData;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.Optional;
import net.minecraft.crash.CrashReport;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ReportedException;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ShopKeeperPacket
implements IMessage {
    EnumBuySell buySell;
    String itemID;
    int amount;
    int shopKeeperID;

    public ShopKeeperPacket() {
    }

    public ShopKeeperPacket(EnumBuySell buySell, int shopkeeperID, String itemID, int amount) {
        this.shopKeeperID = shopkeeperID;
        this.itemID = itemID;
        this.amount = amount;
        this.buySell = buySell;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.buySell = buf.readBoolean() ? EnumBuySell.Buy : EnumBuySell.Sell;
        this.shopKeeperID = buf.readInt();
        this.itemID = ByteBufUtils.readUTF8String(buf);
        this.amount = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeBoolean(this.buySell == EnumBuySell.Buy);
        buf.writeInt(this.shopKeeperID);
        ByteBufUtils.writeUTF8String(buf, this.itemID);
        buf.writeInt(this.amount);
    }

    public static boolean checkInventoryCanFit(InventoryPlayer inventoryIn, ItemStack itemStackIn) {
        if (!itemStackIn.isEmpty()) {
            try {
                int i;
                do {
                    i = itemStackIn.getCount();
                    itemStackIn.setCount(inventoryIn.storePartialItemStack(itemStackIn));
                } while (itemStackIn.getCount() > 0 && itemStackIn.getCount() < i);
                return itemStackIn.getCount() < i;
            }
            catch (Throwable throwable) {
                CrashReport crashReport = CrashReport.makeCrashReport(throwable, "Adding item to inventory");
                CrashReportCategory crashReportCategory = crashReport.makeCategory("Item being added");
                crashReportCategory.addCrashSection("Item ID", Item.getIdFromItem(itemStackIn.getItem()));
                crashReportCategory.addCrashSection("Item data", itemStackIn.getMetadata());
                crashReportCategory.addCrashSection("Item name", itemStackIn.getDisplayName());
                throw new ReportedException(crashReport);
            }
        }
        return false;
    }

    public static class Handler
    implements IMessageHandler<ShopKeeperPacket, IMessage> {
        @Override
        public IMessage onMessage(ShopKeeperPacket message, MessageContext ctx) {
            block17: {
                if (message.amount <= 0) {
                    return null;
                }
                EntityPlayerMP p = ctx.getServerHandler().player;
                Optional<NPCShopkeeper> npcOptional = EntityNPC.locateNPCServer(p.world, message.shopKeeperID, NPCShopkeeper.class);
                if (!npcOptional.isPresent()) {
                    return null;
                }
                NPCShopkeeper npc = npcOptional.get();
                Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(p);
                if (!optstorage.isPresent()) break block17;
                PlayerStorage storage = optstorage.get();
                if (message.buySell == EnumBuySell.Buy) {
                    ArrayList<ShopItemWithVariation> itemList = npc.getItemList();
                    for (ShopItemWithVariation s : itemList) {
                        int buyCost;
                        long totalCost;
                        if (!s.getBaseShopItem().id.equals(message.itemID) || (totalCost = (long)(buyCost = s.getBuyCost()) * (long)message.amount) < 0L || totalCost > (long)storage.getCurrency()) continue;
                        ItemStack item = s.getItem();
                        ItemStack buyStack = item.copy();
                        int initialAmount = message.amount;
                        buyStack.setCount(message.amount);
                        NPCTransactionEvent.Shopkeeper event = new NPCTransactionEvent.Shopkeeper(p, npc, EnumBuySell.Buy, buyStack, initialAmount, buyCost);
                        if (MinecraftForge.EVENT_BUS.post(event)) {
                            return null;
                        }
                        buyStack = event.getItem();
                        buyCost = event.getItemWorth();
                        totalCost = (long)buyCost * (long)(initialAmount = buyStack.getCount());
                        if (totalCost < 0L || totalCost > (long)storage.getCurrency()) continue;
                        if (ShopKeeperPacket.checkInventoryCanFit(p.inventory, buyStack)) {
                            Item buyItem = buyStack.getItem();
                            if (buyItem instanceof ItemPokeball && ((ItemPokeball)buyItem).type == EnumPokeball.PokeBall && initialAmount >= 10) {
                                ItemStack premierBall = new ItemStack(PixelmonItemsPokeballs.premierBall, 1);
                                ShopKeeperPacket.checkInventoryCanFit(p.inventory, premierBall);
                            }
                            int moneyAmount = buyCost * initialAmount;
                            storage.removeCurrency(moneyAmount);
                            this.updateTransaction(storage, p, npc);
                            return null;
                        }
                        if (initialAmount > buyStack.getCount()) {
                            int moneyAmount = buyCost * (initialAmount - buyStack.getCount());
                            storage.removeCurrency(moneyAmount);
                            this.updateTransaction(storage, p, npc);
                            return null;
                        }
                        Pixelmon.NETWORK.sendTo(new UpdateClientPlayerData(storage.getCurrency()), p);
                    }
                } else {
                    ArrayList<ShopItemWithVariation> sellList = npc.getSellList(p);
                    for (ShopItemWithVariation s : sellList) {
                        if (!s.getBaseShopItem().id.equals(message.itemID)) continue;
                        int count = 0;
                        ItemStack sStack = s.getItem();
                        for (int i = 0; i < p.inventory.mainInventory.size(); ++i) {
                            ItemStack item = p.inventory.mainInventory.get(i);
                            if (!this.areItemsEqual(item, sStack)) continue;
                            count += item.getCount();
                        }
                        if (count < message.amount) continue;
                        int cost = s.getSellCost();
                        NPCTransactionEvent.Shopkeeper event = new NPCTransactionEvent.Shopkeeper(p, npc, EnumBuySell.Sell, sStack, message.amount, cost);
                        if (MinecraftForge.EVENT_BUS.post(event)) {
                            return null;
                        }
                        cost = event.getItemWorth();
                        sStack = event.getItem();
                        count = message.amount;
                        for (int i = 0; i < p.inventory.mainInventory.size(); ++i) {
                            ItemStack item = p.inventory.mainInventory.get(i);
                            if (this.areItemsEqual(item, sStack)) {
                                if (item.getCount() >= count) {
                                    item.setCount(item.getCount() - count);
                                    count = 0;
                                } else {
                                    count -= item.getCount();
                                    item.setCount(0);
                                }
                                if (item.getCount() == 0) {
                                    p.inventory.mainInventory.set(i, ItemStack.EMPTY);
                                }
                            }
                            if (count <= 0) break;
                        }
                        int moneyAmount = event.getItemWorth() * message.amount;
                        storage.addCurrency(moneyAmount);
                        this.updateTransaction(storage, p, npc);
                    }
                }
            }
            return null;
        }

        private void updateTransaction(PlayerStorage storage, EntityPlayerMP p, NPCShopkeeper npc) {
            Pixelmon.NETWORK.sendTo(new UpdateClientPlayerData(storage.getCurrency()), p);
            p.sendContainerToPlayer(p.inventoryContainer);
            npc.sendItemsToPlayer(p);
        }

        private boolean areItemsEqual(ItemStack item1, ItemStack item2) {
            return !item1.isEmpty() && ItemStack.areItemsEqual(item1, item2) && ItemStack.areItemStackTagsEqual(item1, item2) && item1.getItemDamage() == item2.getItemDamage();
        }
    }
}

