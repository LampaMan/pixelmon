/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.customOverlays;

import com.pixelmongenerations.api.ui.Popup;
import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.client.gui.overlay.OverlayType;
import com.pixelmongenerations.client.gui.overlay.PopupOverlay;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class EnqueuePopup
implements IMessage {
    public Popup popup;

    public EnqueuePopup(Popup popup) {
        this.popup = popup;
    }

    public EnqueuePopup() {
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        this.popup.serialize(buffer);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.popup = Popup.unserialize(buffer);
    }

    public static class Handler
    implements IMessageHandler<EnqueuePopup, IMessage> {
        @Override
        public IMessage onMessage(EnqueuePopup message, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(() -> {
                PopupOverlay overlay = (PopupOverlay)GuiPixelmonOverlay.getOverlay(OverlayType.POPUP);
                overlay.enqueuePopup(message.popup);
            });
            return null;
        }
    }
}

