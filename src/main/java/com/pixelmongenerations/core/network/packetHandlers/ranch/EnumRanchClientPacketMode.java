/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.network.packetHandlers.ranch;

public enum EnumRanchClientPacketMode {
    ViewBlock,
    UpgradeBlock;


    public static EnumRanchClientPacketMode getFromOrdinal(int ord) {
        for (EnumRanchClientPacketMode p : EnumRanchClientPacketMode.values()) {
            if (p.ordinal() != ord) continue;
            return p;
        }
        return null;
    }
}

