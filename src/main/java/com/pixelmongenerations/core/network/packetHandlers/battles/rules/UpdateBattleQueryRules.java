/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles.rules;

import com.pixelmongenerations.client.gui.battles.rules.EnumRulesGuiState;
import com.pixelmongenerations.client.gui.battles.rules.GuiBattleRulesPlayer;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class UpdateBattleQueryRules
implements IMessage {
    EnumRulesGuiState state;

    public UpdateBattleQueryRules() {
    }

    public UpdateBattleQueryRules(EnumRulesGuiState state) {
        this.state = state;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.state.ordinal());
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.state = EnumRulesGuiState.getFromOrdinal(buf.readInt());
    }

    public static class Handler
    implements IMessageHandler<UpdateBattleQueryRules, IMessage> {
        @Override
        public IMessage onMessage(UpdateBattleQueryRules message, MessageContext ctx) {
            GuiScreen currentScreen = Minecraft.getMinecraft().currentScreen;
            if (currentScreen instanceof GuiBattleRulesPlayer) {
                GuiBattleRulesPlayer rulesScreen = (GuiBattleRulesPlayer)currentScreen;
                rulesScreen.changeState(message.state);
            }
            return null;
        }
    }
}

