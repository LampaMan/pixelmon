/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

public enum EnumBattleQueryResponse {
    Accept,
    Decline,
    Rules,
    Change,
    None;


    public boolean isAcceptResponse() {
        return this == Accept || this == Rules;
    }

    static EnumBattleQueryResponse getFromOrdinal(int ordinal) {
        EnumBattleQueryResponse[] values = EnumBattleQueryResponse.values();
        if (ordinal >= 0 && ordinal < values.length) {
            return values[ordinal];
        }
        return Decline;
    }
}

