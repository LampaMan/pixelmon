/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles.rules;

import com.pixelmongenerations.common.battle.BattleQuery;
import com.pixelmongenerations.common.battle.rules.BattleRules;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ProposeBattleRules
implements IMessage {
    int queryID;
    BattleRules rules;

    public ProposeBattleRules() {
    }

    public ProposeBattleRules(int queryID, BattleRules rules) {
        this.queryID = queryID;
        this.rules = rules;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.queryID);
        this.rules.encodeInto(buf);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.queryID = buf.readInt();
        this.rules = new BattleRules(buf);
    }

    public static class Handler
    implements IMessageHandler<ProposeBattleRules, IMessage> {
        @Override
        public IMessage onMessage(ProposeBattleRules message, MessageContext ctx) {
            BattleQuery query = BattleQuery.getQuery(message.queryID);
            if (query == null) {
                return null;
            }
            EntityPlayerMP player = ctx.getServerHandler().player;
            query.proposeRules(player, message.rules);
            return null;
        }
    }
}

