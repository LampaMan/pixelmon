/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.npc;

import com.pixelmongenerations.client.gui.npcEditor.GuiTrainerEditor;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.item.ItemNPCEditor;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.ClearTrainerPokemon;
import com.pixelmongenerations.core.network.packetHandlers.npc.StoreTrainerPokemon;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.UpdateEditedPokemon;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class UpdateTrainerPokemon
extends UpdateEditedPokemon {
    int trainerID;

    public UpdateTrainerPokemon() {
    }

    public UpdateTrainerPokemon(PixelmonData data) {
        super(data);
        this.trainerID = GuiTrainerEditor.currentTrainerID;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.trainerID);
        super.toBytes(buffer);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.trainerID = buffer.readInt();
        super.fromBytes(buffer);
    }

    public static class Handler
    implements IMessageHandler<UpdateTrainerPokemon, IMessage> {
        @Override
        public IMessage onMessage(UpdateTrainerPokemon message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            if (!ItemNPCEditor.checkPermission(player)) {
                return null;
            }
            Optional<NPCTrainer> entityNPCOptional = EntityNPC.locateNPCServer(player.world, message.trainerID, NPCTrainer.class);
            if (!entityNPCOptional.isPresent()) {
                return null;
            }
            NPCTrainer t = entityNPCOptional.get();
            PlayerStorage storage = t.getPokemonStorage();
            player.getServer().addScheduledTask(() -> {
                NBTTagCompound tag = UpdateEditedPokemon.updatePokemon(message, player, storage);
                if (tag != null) {
                    t.updateLvl();
                    Pixelmon.NETWORK.sendTo(new ClearTrainerPokemon(), player);
                    for (int i = 0; i < storage.count(); ++i) {
                        Pixelmon.NETWORK.sendTo(new StoreTrainerPokemon(new PixelmonData(storage.getList()[i])), player);
                    }
                }
            });
            return null;
        }
    }
}

