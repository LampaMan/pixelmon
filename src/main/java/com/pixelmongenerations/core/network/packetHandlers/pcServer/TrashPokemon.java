/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pcServer;

import com.pixelmongenerations.core.storage.PCServer;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class TrashPokemon
implements IMessage {
    int box;
    int pos;

    public TrashPokemon() {
    }

    public TrashPokemon(int box, int pos) {
        this.box = box;
        this.pos = pos;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.box);
        buffer.writeInt(this.pos);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.box = buffer.readInt();
        this.pos = buffer.readInt();
    }

    public static class Handler
    implements IMessageHandler<TrashPokemon, IMessage> {
        @Override
        public IMessage onMessage(TrashPokemon message, MessageContext ctx) {
            ctx.getServerHandler().player.getServer().addScheduledTask(() -> PCServer.deletePokemon(ctx.getServerHandler().player, message.box, message.pos));
            return null;
        }
    }
}

