/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles.rules;

import com.pixelmongenerations.core.network.packetHandlers.battles.rules.CheckRulesVersion;
import com.pixelmongenerations.core.network.packetHandlers.battles.rules.DisplayBattleQueryRules;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class CheckRulesVersionChoose
extends CheckRulesVersion<DisplayBattleQueryRules> {
    public CheckRulesVersionChoose() {
    }

    public CheckRulesVersionChoose(int clauseVersion, DisplayBattleQueryRules packet) {
        super(clauseVersion, packet);
    }

    @Override
    protected void readPacket(ByteBuf buf) {
        this.packet = new DisplayBattleQueryRules();
        ((DisplayBattleQueryRules)this.packet).fromBytes(buf);
    }

    @Override
    public void processPacket(MessageContext ctx) {
        new DisplayBattleQueryRules.Handler().onMessage((DisplayBattleQueryRules)this.packet, ctx);
    }

    public static class Handler
    implements IMessageHandler<CheckRulesVersionChoose, IMessage> {
        @Override
        public IMessage onMessage(CheckRulesVersionChoose message, MessageContext ctx) {
            message.onMessage(ctx);
            return null;
        }
    }
}

