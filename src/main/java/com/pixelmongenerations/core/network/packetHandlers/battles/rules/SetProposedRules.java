/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles.rules;

import com.pixelmongenerations.client.gui.battles.rules.GuiBattleRulesPlayer;
import com.pixelmongenerations.common.battle.rules.BattleRules;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SetProposedRules
implements IMessage {
    BattleRules rules;

    public SetProposedRules() {
    }

    public SetProposedRules(BattleRules rules) {
        this.rules = rules;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        this.rules.encodeInto(buf);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.rules = new BattleRules(buf);
    }

    public static class Handler
    implements IMessageHandler<SetProposedRules, IMessage> {
        @Override
        public IMessage onMessage(SetProposedRules message, MessageContext ctx) {
            Minecraft mc = Minecraft.getMinecraft();
            GuiScreen currentScreen = mc.currentScreen;
            if (currentScreen instanceof GuiBattleRulesPlayer) {
                GuiBattleRulesPlayer rulesScreen = (GuiBattleRulesPlayer)currentScreen;
                mc.addScheduledTask(() -> rulesScreen.setRules(message.rules));
            }
            return null;
        }
    }
}

