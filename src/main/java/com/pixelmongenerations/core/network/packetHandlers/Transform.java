/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.common.battle.status.Transformed;
import com.pixelmongenerations.core.enums.EnumSpecies;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class Transform
implements IMessage {
    public int pixelmonID;
    public EnumSpecies transformedModel;
    public String transformedTexture;
    public int transformedForm;

    public Transform() {
    }

    public Transform(int is, EnumSpecies newPokemon, String newTexture, int newForm) {
        this.transformedModel = newPokemon;
        this.transformedTexture = newTexture;
        this.transformedForm = newForm;
        this.pixelmonID = is;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.pixelmonID = buffer.readInt();
        this.transformedModel = EnumSpecies.getFromOrdinal(buffer.readInt());
        this.transformedTexture = ByteBufUtils.readUTF8String(buffer);
        this.transformedForm = buffer.readInt();
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.pixelmonID);
        buffer.writeInt(this.transformedModel.ordinal());
        ByteBufUtils.writeUTF8String(buffer, this.transformedTexture);
        buffer.writeInt(this.transformedForm);
    }

    public static class Handler
    implements IMessageHandler<Transform, IMessage> {
        @Override
        public IMessage onMessage(Transform message, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(() -> Transformed.applyToClientEntity(message));
            return null;
        }
    }
}

