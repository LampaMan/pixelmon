/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.client.gui.battles.GuiAcceptDeny;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.UUID;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class BattleQueryPacket
implements IMessage {
    public UUID opponentUUID;
    public int queryIndex;
    public int[] pokeballs = new int[]{-1, -1, -1, -1, -1, -1};

    public BattleQueryPacket() {
    }

    public BattleQueryPacket(int queryIndex, UUID opponentUUID, PlayerStorage opponentStorage) {
        this.queryIndex = queryIndex;
        this.opponentUUID = opponentUUID;
        for (int i = 0; i < 6; ++i) {
            if (opponentStorage.getList()[i] != null) {
                this.pokeballs[i] = opponentStorage.getList()[i].getInteger("CaughtBall");
                if (!opponentStorage.getList()[i].getBoolean("IsFainted")) continue;
                this.pokeballs[i] = this.pokeballs[i] * -1 - 1;
                continue;
            }
            this.pokeballs[i] = -999;
        }
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.queryIndex = buffer.readInt();
        this.opponentUUID = UUID.fromString(ByteBufUtils.readUTF8String(buffer));
        for (int i = 0; i < 6; ++i) {
            this.pokeballs[i] = buffer.readShort();
        }
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.queryIndex);
        ByteBufUtils.writeUTF8String(buffer, this.opponentUUID.toString());
        for (int i = 0; i < 6; ++i) {
            buffer.writeShort(this.pokeballs[i]);
        }
    }

    public static class Handler
    implements IMessageHandler<BattleQueryPacket, IMessage> {
        @Override
        public IMessage onMessage(BattleQueryPacket message, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(() -> {
                GuiAcceptDeny.opponent = message;
                Minecraft.getMinecraft().player.openGui(Pixelmon.INSTANCE, EnumGui.AcceptDeny.getIndex(), Minecraft.getMinecraft().player.world, message.queryIndex, 0, 0);
            });
            return null;
        }
    }
}

