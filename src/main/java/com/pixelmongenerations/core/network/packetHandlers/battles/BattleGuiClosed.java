/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.core.util.MoveCostList;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class BattleGuiClosed
implements IMessage {
    @Override
    public void fromBytes(ByteBuf buf) {
    }

    @Override
    public void toBytes(ByteBuf buf) {
    }

    public static class Handler
    implements IMessageHandler<BattleGuiClosed, IMessage> {
        @Override
        public IMessage onMessage(BattleGuiClosed message, MessageContext ctx) {
            ctx.getServerHandler().player.getServer().addScheduledTask(() -> this.processMessage(message, ctx));
            return null;
        }

        private void processMessage(BattleGuiClosed message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            BattleControllerBase b = BattleRegistry.getBattle(player);
            if (b != null) {
                BattleRegistry.deRegisterBattle(b);
            }
            MoveCostList.removeEntry(player);
        }
    }
}

