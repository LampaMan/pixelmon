/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class BattleMessage
implements IMessage {
    TextComponentTranslation chat;

    public BattleMessage() {
    }

    public BattleMessage(TextComponentTranslation chat) {
        this.chat = chat;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        PacketBuffer pb = new PacketBuffer(buffer);
        try {
            this.chat = (TextComponentTranslation)ITextComponent.Serializer.jsonToComponent(pb.readString(32767));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        PacketBuffer pb = new PacketBuffer(buffer);
        try {
            pb.writeString(ITextComponent.Serializer.componentToJson(this.chat));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class Handler
    implements IMessageHandler<BattleMessage, IMessage> {
        @Override
        public IMessage onMessage(BattleMessage message, MessageContext ctx) {
            if (message.chat != null) {
                ClientProxy.battleManager.addMessage(message.chat.getUnformattedText());
            }
            return null;
        }
    }
}

