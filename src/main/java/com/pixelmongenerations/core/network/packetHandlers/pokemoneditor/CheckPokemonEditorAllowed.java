/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pokemoneditor;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.RespondPokemonEditorAllowed;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import java.util.UUID;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class CheckPokemonEditorAllowed
implements IMessage {
    UUID editingPlayer;

    public CheckPokemonEditorAllowed() {
    }

    public CheckPokemonEditorAllowed(UUID editingPlayer) {
        this.editingPlayer = editingPlayer;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        PixelmonMethods.toBytesUUID(buf, this.editingPlayer);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.editingPlayer = PixelmonMethods.fromBytesUUID(buf);
    }

    public static class Handler
    implements IMessageHandler<CheckPokemonEditorAllowed, IMessage> {
        @Override
        public IMessage onMessage(CheckPokemonEditorAllowed message, MessageContext ctx) {
            Pixelmon.NETWORK.sendToServer(new RespondPokemonEditorAllowed(message.editingPlayer, PixelmonConfig.allowPokemonEditors));
            return null;
        }
    }
}

