/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.statueEditor;

import com.pixelmongenerations.api.events.StatueEvent;
import com.pixelmongenerations.common.entity.pixelmon.EntityStatue;
import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.enums.EnumStatueTextureType;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.packetHandlers.statueEditor.EnumStatuePacketMode;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.Entity;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.server.permission.PermissionAPI;

public class StatuePacketServer
implements IMessage {
    EnumStatuePacketMode mode;
    int[] id;
    String name;
    boolean data;
    int intData;

    public StatuePacketServer() {
    }

    public StatuePacketServer(EnumStatuePacketMode mode, int[] id, String name) {
        this.mode = mode;
        this.id = id;
        this.name = name;
    }

    public StatuePacketServer(EnumStatuePacketMode mode, int[] id, boolean data) {
        this.mode = mode;
        this.id = id;
        this.data = data;
    }

    public StatuePacketServer(EnumStatuePacketMode mode, int[] id, int intData) {
        this.mode = mode;
        this.id = id;
        this.intData = intData;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.mode = EnumStatuePacketMode.getFromOrdinal(buf.readShort());
        this.id = new int[]{buf.readInt(), buf.readInt()};
        switch (this.mode) {
            case SetGrowth: 
            case SetLabel: 
            case SetTextureType: 
            case SetAnimation: 
            case SetCustomSpecial: 
            case SetName: {
                this.name = ByteBufUtils.readUTF8String(buf);
                break;
            }
            case SetAdminPlaced: 
            case SetModelStanding: 
            case SetGmaxModel: 
            case SetAnimated: {
                this.data = buf.readBoolean();
                break;
            }
            case SetAnimationFrame: 
            case SetSpecial: 
            case SetForm: {
                this.intData = buf.readInt();
            }
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeShort((int)((short)this.mode.ordinal()));
        buf.writeInt(this.id[0]);
        buf.writeInt(this.id[1]);
        switch (this.mode) {
            case SetGrowth: 
            case SetLabel: 
            case SetTextureType: 
            case SetAnimation: 
            case SetCustomSpecial: 
            case SetName: {
                ByteBufUtils.writeUTF8String(buf, this.name);
                break;
            }
            case SetAdminPlaced: 
            case SetModelStanding: 
            case SetGmaxModel: 
            case SetAnimated: {
                buf.writeBoolean(this.data);
                break;
            }
            case SetAnimationFrame: 
            case SetSpecial: 
            case SetForm: {
                buf.writeInt(this.intData);
            }
        }
    }

    public static class Handler
    implements IMessageHandler<StatuePacketServer, IMessage> {
        @Override
        public IMessage onMessage(StatuePacketServer message, MessageContext ctx) {
            ctx.getServerHandler().player.getServer().addScheduledTask(() -> {
                EntityStatue statue = null;
                for (int i = 0; i < ctx.getServerHandler().player.world.loadedEntityList.size(); ++i) {
                    Entity e = ctx.getServerHandler().player.world.loadedEntityList.get(i);
                    if (!(e instanceof EntityStatue) || !PixelmonMethods.isIDSame(((EntityStatue)e).getPokemonId(), message.id)) continue;
                    statue = (EntityStatue)e;
                    break;
                }
                if (statue == null) {
                    return;
                }
                Object value = message.mode.chooseValueForMode(message.name, message.data, message.intData);
                StatueEvent.ModifyStatue modifyEvent = new StatueEvent.ModifyStatue(ctx.getServerHandler().player, statue, message.mode, value);
                if (MinecraftForge.EVENT_BUS.post(modifyEvent)) {
                    statue.update(EnumUpdateType.values());
                    ctx.getServerHandler().player.closeScreen();
                    return;
                }
                value = modifyEvent.getValue();
                switch (message.mode) {
                    case SetName: {
                        statue.init((String)value);
                        break;
                    }
                    case SetGrowth: {
                        statue.setGrowth((EnumGrowth)((Object)((Object)value)));
                        break;
                    }
                    case SetLabel: {
                        statue.setLabel((String)value);
                        break;
                    }
                    case SetTextureType: {
                        statue.setTextureType((EnumStatueTextureType)((Object)((Object)value)));
                        break;
                    }
                    case SetAnimation: {
                        statue.setAnimation((String)value);
                        break;
                    }
                    case SetModelStanding: {
                        statue.setIsFlying((Boolean)value);
                        break;
                    }
                    case SetAnimationFrame: {
                        statue.setAnimationFrame((Integer)value);
                        break;
                    }
                    case SetForm: {
                        statue.setForm((Integer)value, false);
                        break;
                    }
                    case SetSpecial: {
                        statue.setSpecialTextureId((Integer)value);
                        break;
                    }
                    case SetAdminPlaced: {
                        if (!PermissionAPI.hasPermission(ctx.getServerHandler().player, "pixelmon.admin.admin_statue")) break;
                        statue.setAdminPlaced((Boolean)value);
                        break;
                    }
                    case SetCustomSpecial: {
                        if (!PermissionAPI.hasPermission(ctx.getServerHandler().player, "pixelmon.admin.admin_statue")) break;
                        statue.setCustomSpecial((String)value);
                        break;
                    }
                    case SetAnimated: {
                        statue.setAnimated((Boolean)value);
                        break;
                    }
                    case SetGmaxModel: {
                        statue.setGmaxModel((Boolean)value);
                    }
                }
            });
            return null;
        }
    }
}

