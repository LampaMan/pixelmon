/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.npc;

import com.pixelmongenerations.client.gui.npc.GuiTutor;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.npcs.NPCTutor;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class LoadTutorData
implements IMessage {
    ArrayList<Attack> attackList;
    ArrayList<ArrayList<ItemStack>> costs;

    public LoadTutorData() {
    }

    public LoadTutorData(NPCTutor tutor) {
        this.attackList = tutor.attackList;
        this.costs = tutor.costs;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.attackList = new ArrayList();
        this.costs = new ArrayList();
        NPCTutor.decode(buf, this.attackList, this.costs);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        NPCTutor.encode(buf, this.attackList, this.costs);
    }

    public static class Handler
    implements IMessageHandler<LoadTutorData, IMessage> {
        @Override
        public IMessage onMessage(LoadTutorData message, MessageContext ctx) {
            GuiTutor.attackList = message.attackList;
            GuiTutor.costs = message.costs;
            return null;
        }
    }
}

