/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.network.packetHandlers.chooseMoveset;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.util.PixelmonMethods;
import java.util.ArrayList;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;

public class ChoosingMovesetData {
    EntityPlayerMP player;
    public ArrayList<NBTTagCompound> pokemonList;

    public ChoosingMovesetData(EntityPlayerMP player, ArrayList<NBTTagCompound> pokemonList) {
        this.player = player;
        this.pokemonList = pokemonList;
    }

    public void next() {
        NBTTagCompound nbt = this.pokemonList.get(0);
        int[] id = PixelmonMethods.getID(nbt);
        this.player.openGui(Pixelmon.INSTANCE, EnumGui.ChooseMoveset.getIndex(), this.player.world, id[0], id[1], 0);
        this.pokemonList.remove(0);
    }
}

