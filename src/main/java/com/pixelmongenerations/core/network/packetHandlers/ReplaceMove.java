/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.EntityLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.NBTLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.WrapperLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.moves.LearnMoveStorage;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.MoveCostList;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ReplaceMove
implements IMessage {
    private int[] pokemonId;
    private int attackId;
    private int replaceIndex;
    private boolean checkEvo;

    public ReplaceMove() {
    }

    public ReplaceMove(int[] pokemonId, int attackId, int replaceIndex, boolean checkEvo) {
        this.pokemonId = pokemonId;
        this.attackId = attackId;
        this.replaceIndex = replaceIndex;
        this.checkEvo = checkEvo;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.pokemonId = new int[]{buffer.readInt(), buffer.readInt()};
        this.attackId = buffer.readInt();
        this.replaceIndex = buffer.readInt();
        this.checkEvo = buffer.readBoolean();
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.pokemonId[0]);
        buffer.writeInt(this.pokemonId[1]);
        buffer.writeInt(this.attackId);
        buffer.writeInt(this.replaceIndex);
        buffer.writeBoolean(this.checkEvo);
    }

    public static class Handler
    implements IMessageHandler<ReplaceMove, IMessage> {
        @Override
        public IMessage onMessage(ReplaceMove message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            player.getServer().addScheduledTask(() -> {
                Optional<PlayerStorage> optstorage;
                PlayerParticipant participant;
                if (!MoveCostList.checkEntry(player)) {
                    ChatHandler.sendChat(player, "pixelmon.npc.cantpay", new Object[0]);
                    return;
                }
                Attack a = new Attack(message.attackId);
                PokemonLink pokemon = null;
                BattleControllerBase bc = BattleRegistry.getBattle(player);
                PixelmonWrapper pw = null;
                NBTTagCompound nbt = null;
                if (bc != null && (participant = bc.getPlayer(player)) != null && (pw = participant.getPokemonFromParty(message.pokemonId)) != null) {
                    pokemon = new WrapperLink(pw);
                }
                if (pokemon == null && (optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player)).isPresent()) {
                    PlayerStorage storage = optstorage.get();
                    EntityPixelmon p = storage.getAlreadyExists(message.pokemonId, player.world).orElse(null);
                    if (p == null) {
                        nbt = storage.getNBT(message.pokemonId);
                        if (nbt == null) {
                            return;
                        }
                        pokemon = new NBTLink(nbt, storage);
                    } else {
                        pokemon = new EntityLink(p);
                    }
                }
                if (pokemon != null) {
                    TextComponentTranslation chatMessage;
                    if (!DatabaseMoves.CanLearnAttack(DatabaseMoves.getPokemonID(pokemon.getSpecies(), pokemon.getBaseStats(), pokemon.getForm(), false), a.getAttackBase().getUnlocalizedName())) {
                        chatMessage = ChatHandler.getMessage("replacemove.invalid", pokemon.getRealNickname(), a.getAttackBase().getLocalizedName());
                        if (bc == null || bc.battleEnded) {
                            ChatHandler.sendChat(player, chatMessage);
                        } else {
                            bc.sendToPlayer(player, chatMessage);
                        }
                    } else {
                        if (!LearnMoveStorage.getInstance().hasMove(player, message.pokemonId, message.attackId)) {
                            chatMessage = ChatHandler.getMessage("replacemove.invalidcheat", pokemon.getRealNickname(), a.getAttackBase().getLocalizedName());
                            if (bc == null || bc.battleEnded) {
                                ChatHandler.sendChat(player, chatMessage);
                            } else {
                                bc.sendToPlayer(player, chatMessage);
                            }
                            return;
                        }
                        if (message.replaceIndex > -1) {
                            Moveset moveset = pokemon.getMoveset();
                            TextComponentTranslation chatMessage2 = ChatHandler.getMessage("replacemove.replace", pokemon.getRealNickname(), moveset.get(message.replaceIndex).getAttackBase().getLocalizedName(), a.getAttackBase().getLocalizedName());
                            moveset.set(message.replaceIndex, a);
                            if (bc == null || bc.battleEnded) {
                                ChatHandler.sendChat(player, chatMessage2);
                            } else {
                                bc.sendToPlayer(player, chatMessage2);
                            }
                            if (bc == null && nbt != null) {
                                moveset.writeToNBT(nbt);
                            } else if (pw != null && bc.battleEnded) {
                                pw.writeToNBT();
                            }
                            pokemon.update(EnumUpdateType.Moveset);
                        }
                        if (message.checkEvo) {
                            pokemon.getLevelContainer().tryEvolution();
                        }
                    }
                }
            });
            return null;
        }
    }
}

