/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.common.battle.BattleQuery;
import com.pixelmongenerations.core.network.packetHandlers.battles.EnumBattleQueryResponse;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class AcceptDeclineBattle
implements IMessage {
    EnumBattleQueryResponse response;
    int queryID;

    public AcceptDeclineBattle() {
    }

    public AcceptDeclineBattle(int queryID, EnumBattleQueryResponse response) {
        this.queryID = queryID;
        this.response = response;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.queryID = buffer.readInt();
        this.response = EnumBattleQueryResponse.getFromOrdinal(buffer.readInt());
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.queryID);
        buffer.writeInt(this.response.ordinal());
    }

    public static class Handler
    implements IMessageHandler<AcceptDeclineBattle, IMessage> {
        @Override
        public IMessage onMessage(AcceptDeclineBattle message, MessageContext ctx) {
            ctx.getServerHandler().player.getServerWorld().addScheduledTask(() -> {
                BattleQuery query = BattleQuery.getQuery(message.queryID);
                if (query == null) {
                    return;
                }
                EntityPlayerMP player = ctx.getServerHandler().player;
                switch (message.response) {
                    case Decline: {
                        query.declineQuery(player);
                        break;
                    }
                    case Accept: 
                    case Rules: {
                        query.acceptQuery(player, message.response);
                        break;
                    }
                    case Change: {
                        query.changeRules(player);
                        break;
                    }
                }
            });
            return null;
        }
    }
}

