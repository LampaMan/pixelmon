/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.camera;

import com.pixelmongenerations.api.events.CameraEvent;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.EntityLink;
import com.pixelmongenerations.common.item.ItemPixelmonSprite;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.util.PixelSounds;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ItemCameraPacket
implements IMessage {
    public int entityId;

    public ItemCameraPacket() {
    }

    public ItemCameraPacket(int entityID) {
        this.entityId = entityID;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.entityId = ByteBufUtils.readVarInt(buf, 4);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeVarInt(buf, this.entityId, 4);
    }

    public static class Handler
    implements IMessageHandler<ItemCameraPacket, IMessage> {
        @Override
        public IMessage onMessage(ItemCameraPacket message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            EntityPixelmon pixelmon = (EntityPixelmon)ctx.getServerHandler().player.world.getEntityByID(message.entityId);
            if (pixelmon != null) {
                if (!PixelmonConfig.allowMultiplePhotosOfSamePokemon && pixelmon.cameraCapturedPlayers.contains(player.getUniqueID())) {
                    player.sendMessage(new TextComponentTranslation("camera.error.samepokemon", Entity1Base.getLocalizedName(pixelmon.baseStats.pixelmonName)));
                } else if (player.inventory.clearMatchingItems(PixelmonItems.filmItem, 0, 1, null) == 1) {
                    CameraEvent event;
                    if (!PixelmonConfig.allowMultiplePhotosOfSamePokemon) {
                        pixelmon.cameraCapturedPlayers.add(player.getUniqueID());
                    }
                    if (MinecraftForge.EVENT_BUS.post(event = new CameraEvent(new EntityLink(pixelmon), player))) {
                        if (!PixelmonConfig.allowMultiplePhotosOfSamePokemon) {
                            pixelmon.cameraCapturedPlayers.remove(player.getUniqueID());
                        }
                        return null;
                    }
                    player.inventory.addItemStackToInventory(ItemPixelmonSprite.getPhoto(event.getPokemonLink()));
                    pixelmon.world.playSound(null, player.posX, player.posY, player.posZ, PixelSounds.cameraShutter, SoundCategory.AMBIENT, 1.0f, 1.0f);
                }
            }
            return null;
        }
    }
}

