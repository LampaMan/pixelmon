/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles.rules;

import com.pixelmongenerations.common.battle.rules.clauses.BattleClause;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClauseRegistry;
import com.pixelmongenerations.common.battle.rules.clauses.tiers.Tier;
import com.pixelmongenerations.core.network.packetHandlers.battles.rules.CheckRulesVersion;
import com.pixelmongenerations.core.util.helper.ArrayHelper;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class UpdateClientRules
implements IMessage {
    int clauseVersion = BattleClauseRegistry.getClauseVersion();
    List<BattleClause> customClauses = BattleClauseRegistry.getClauseRegistry().getCustomClauses();
    List<Tier> customTiers = BattleClauseRegistry.getTierRegistry().getCustomClauses();

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.clauseVersion);
        ArrayHelper.encodeList(buf, this.customClauses);
        ArrayHelper.encodeList(buf, this.customTiers);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        int i;
        this.clauseVersion = buf.readInt();
        this.customClauses = new ArrayList<BattleClause>();
        int length = buf.readInt();
        for (i = 0; i < length; ++i) {
            this.customClauses.add(new BattleClause(ByteBufUtils.readUTF8String(buf)).setDescription(ByteBufUtils.readUTF8String(buf)));
        }
        this.customTiers = new ArrayList<Tier>();
        length = buf.readInt();
        for (i = 0; i < length; ++i) {
            this.customTiers.add((Tier)new Tier(ByteBufUtils.readUTF8String(buf)).setDescription(ByteBufUtils.readUTF8String(buf)));
        }
    }

    public static class Handler
    implements IMessageHandler<UpdateClientRules, IMessage> {
        @Override
        public IMessage onMessage(UpdateClientRules message, MessageContext ctx) {
            if (!Minecraft.getMinecraft().isSingleplayer()) {
                BattleClauseRegistry.getClauseRegistry().replaceCustomClauses(message.customClauses, message.clauseVersion);
                BattleClauseRegistry.getTierRegistry().replaceCustomClauses(message.customTiers, message.clauseVersion);
            }
            CheckRulesVersion.processStoredPacket(ctx);
            return null;
        }
    }
}

