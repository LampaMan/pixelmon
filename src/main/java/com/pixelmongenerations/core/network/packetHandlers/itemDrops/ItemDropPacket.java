/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.itemDrops;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.common.entity.pixelmon.drops.DroppedItem;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.network.packetHandlers.battles.BattleGuiClosed;
import com.pixelmongenerations.core.network.packetHandlers.itemDrops.ItemDropMode;
import com.pixelmongenerations.core.network.packetHandlers.itemDrops.ServerItemDropPacket;
import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ItemDropPacket
implements IMessage {
    public DroppedItem[] items;
    public boolean hasCustomTitle = false;
    public ItemDropMode mode;
    public TextComponentTranslation customTitle;

    public ItemDropPacket() {
    }

    public ItemDropPacket(ItemDropMode mode, ArrayList<DroppedItem> givenDrops) {
        this.items = givenDrops.toArray(new DroppedItem[givenDrops.size()]);
        this.mode = mode;
    }

    public ItemDropPacket(ItemDropMode mode, TextComponentTranslation customTitle, ArrayList<DroppedItem> drops) {
        this(mode, drops);
        this.hasCustomTitle = true;
        this.customTitle = customTitle;
    }

    public ItemDropPacket(TextComponentTranslation customTitle, ArrayList<DroppedItem> drops) {
        this(ItemDropMode.Other, customTitle, drops);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.mode = ItemDropMode.values()[buffer.readInt()];
        this.items = new DroppedItem[buffer.readShort()];
        this.hasCustomTitle = buffer.readBoolean();
        int actualLength = this.items.length;
        for (int i = 0; i < this.items.length; ++i) {
            this.items[i] = DroppedItem.fromBytes(buffer);
            if (this.items[i] != null) continue;
            --actualLength;
        }
        if (actualLength != this.items.length) {
            DroppedItem[] newItems = new DroppedItem[actualLength];
            int newItemIndex = 0;
            for (int i = 0; i < this.items.length; ++i) {
                if (this.items[i] == null || newItemIndex >= actualLength) continue;
                newItems[newItemIndex++] = this.items[i];
            }
            this.items = newItems;
        }
        if (this.hasCustomTitle) {
            PacketBuffer pb = new PacketBuffer(buffer);
            try {
                this.customTitle = (TextComponentTranslation)ITextComponent.Serializer.jsonToComponent(pb.readString(32767));
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.mode.ordinal());
        buffer.writeShort(this.items.length);
        buffer.writeBoolean(this.hasCustomTitle);
        for (DroppedItem item : this.items) {
            item.toBytes(buffer);
        }
        if (this.hasCustomTitle) {
            PacketBuffer pb = new PacketBuffer(buffer);
            try {
                pb.writeString(ITextComponent.Serializer.componentToJson(this.customTitle));
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static class Handler
    implements IMessageHandler<ItemDropPacket, IMessage> {
        @Override
        public IMessage onMessage(ItemDropPacket message, MessageContext ctx) {
            Minecraft mc = Minecraft.getMinecraft();
            mc.addScheduledTask(() -> {
                if (PixelmonConfig.useDropGUI || message.mode != ItemDropMode.NormalPokemon) {
                    ServerStorageDisplay.bossDrops = message;
                    if (ClientProxy.battleManager.battleEnded && !ClientProxy.battleManager.hasMoreMessages()) {
                        mc.player.openGui(Pixelmon.INSTANCE, EnumGui.ItemDrops.getIndex(), mc.player.world, 0, 0, 0);
                    }
                } else {
                    Pixelmon.NETWORK.sendToServer(new ServerItemDropPacket(ServerItemDropPacket.PacketMode.TakeAllItems));
                    Pixelmon.NETWORK.sendToServer(new BattleGuiClosed());
                }
            });
            return null;
        }
    }
}

