/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.clientStorage;

import com.pixelmongenerations.core.enums.EnumDynamaxItem;
import com.pixelmongenerations.core.enums.EnumMegaItem;
import com.pixelmongenerations.core.enums.EnumShinyItem;
import com.pixelmongenerations.core.storage.ClientData;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class UpdateClientPlayerData
implements IMessage {
    private ClientDataType type;
    private int playerMoney;
    private EnumMegaItem megaItem;
    private EnumShinyItem shinyItem;
    private EnumDynamaxItem dynamaxItem;
    private boolean giveChoice;

    public UpdateClientPlayerData() {
    }

    public UpdateClientPlayerData(int playerMoney) {
        this.playerMoney = playerMoney;
        this.type = ClientDataType.Currency;
    }

    public UpdateClientPlayerData(EnumMegaItem megaItem, boolean openMegaItemGui) {
        this.megaItem = megaItem;
        this.type = ClientDataType.MegaItem;
        this.giveChoice = openMegaItemGui;
    }

    public UpdateClientPlayerData(EnumShinyItem shinyItem, boolean openShinyItemGui) {
        this.shinyItem = shinyItem;
        this.type = ClientDataType.ShinyItem;
        this.giveChoice = openShinyItemGui;
    }

    public UpdateClientPlayerData(EnumDynamaxItem dynamaxItem, boolean openDynamaxItemGui) {
        this.dynamaxItem = dynamaxItem;
        this.type = ClientDataType.DynamaxItem;
        this.giveChoice = openDynamaxItemGui;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.type = ClientDataType.values()[buf.readInt()];
        switch (this.type) {
            case Currency: {
                this.playerMoney = buf.readInt();
                break;
            }
            case MegaItem: {
                this.megaItem = EnumMegaItem.values()[buf.readInt()];
                this.giveChoice = buf.readBoolean();
                break;
            }
            case ShinyItem: {
                this.shinyItem = EnumShinyItem.values()[buf.readInt()];
                this.giveChoice = buf.readBoolean();
            }
            case DynamaxItem: {
                this.dynamaxItem = EnumDynamaxItem.values()[buf.readInt()];
                this.giveChoice = buf.readBoolean();
            }
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.type.ordinal());
        switch (this.type) {
            case Currency: {
                buf.writeInt(this.playerMoney);
                break;
            }
            case MegaItem: {
                buf.writeInt(this.megaItem.ordinal());
                buf.writeBoolean(this.giveChoice);
                break;
            }
            case ShinyItem: {
                buf.writeInt(this.shinyItem.ordinal());
                buf.writeBoolean(this.giveChoice);
            }
            case DynamaxItem: {
                buf.writeInt(this.dynamaxItem.ordinal());
                buf.writeBoolean(this.giveChoice);
            }
        }
    }

    public static enum ClientDataType {
        Currency,
        MegaItem,
        ShinyItem,
        DynamaxItem;

    }

    public static class Handler
    implements IMessageHandler<UpdateClientPlayerData, IMessage> {
        @Override
        public IMessage onMessage(UpdateClientPlayerData message, MessageContext ctx) {
            switch (message.type) {
                case Currency: {
                    ClientData.playerMoney = message.playerMoney;
                    break;
                }
                case MegaItem: {
                    ClientData.openMegaItemGui = message.giveChoice;
                    break;
                }
                case ShinyItem: {
                    ClientData.openShinyItemGui = message.giveChoice;
                    break;
                }
                case DynamaxItem: {
                    ClientData.openDynamaxItemGui = message.giveChoice;
                }
            }
            return null;
        }
    }
}

