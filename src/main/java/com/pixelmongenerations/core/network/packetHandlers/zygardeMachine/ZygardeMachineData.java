/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.zygardeMachine;

import com.pixelmongenerations.common.block.tileEntities.TileEntityZygardeMachine;
import com.pixelmongenerations.common.item.ItemZygardeCube;
import com.pixelmongenerations.core.enums.ZygardeMachineAction;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ZygardeMachineData
implements IMessage {
    ZygardeMachineAction action = null;
    int x;
    int y;
    int z;

    public ZygardeMachineData() {
    }

    public ZygardeMachineData(int x, int y, int z, ZygardeMachineAction action) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.action = action;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.x);
        buffer.writeInt(this.y);
        buffer.writeInt(this.z);
        buffer.writeShort(this.action.ordinal());
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.x = buffer.readInt();
        this.y = buffer.readInt();
        this.z = buffer.readInt();
        this.action = ZygardeMachineAction.getZygardeMachineActionFromIndex(buffer.readShort());
    }

    public static class Handler
    implements IMessageHandler<ZygardeMachineData, IMessage> {
        @Override
        public IMessage onMessage(ZygardeMachineData message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            Optional<PlayerStorage> oStorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
            if (!oStorage.isPresent()) {
                return null;
            }
            PlayerStorage storage = oStorage.get();
            NBTTagCompound zygarde = null;
            ItemStack item = player.getHeldItemMainhand();
            if (item.getItem() instanceof ItemZygardeCube) {
                for (NBTTagCompound nbt : storage.getList()) {
                    if (nbt == null || !nbt.getString("Name").equals("Zygarde")) continue;
                    int form = nbt.getInteger("Variant");
                    if (form == 0) {
                        zygarde = nbt;
                        continue;
                    }
                    if (form != 1) continue;
                    zygarde = nbt;
                    break;
                }
                switch (message.action) {
                    case createTen: {
                        TileEntityZygardeMachine.createZygardeTen(player, item, storage, message.x, message.y, message.z);
                        break;
                    }
                    case createFifty: {
                        TileEntityZygardeMachine.createZygardeFifty(player, item, storage, message.x, message.y, message.z);
                        break;
                    }
                    case createOneHundred: {
                        TileEntityZygardeMachine.createZygardeOneHundred(player, item, storage, message.x, message.y, message.z);
                    }
                    case combineTen: {
                        TileEntityZygardeMachine.combineTen(player, item, storage, message.x, message.y, message.z);
                    }
                    case combineTenWith90: {
                        TileEntityZygardeMachine.combineTenWithNinety(player, item, storage, message.x, message.y, message.z);
                    }
                    case combineFiftyWith50: {
                        TileEntityZygardeMachine.combineFiftyWith50(player, item, storage, message.x, message.y, message.z);
                    }
                }
            }
            return null;
        }
    }
}

