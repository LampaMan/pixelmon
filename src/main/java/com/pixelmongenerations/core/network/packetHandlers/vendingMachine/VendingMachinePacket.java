/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.vendingMachine;

import com.pixelmongenerations.api.events.npc.NPCTransactionEvent;
import com.pixelmongenerations.common.block.machines.BlockVendingMachine;
import com.pixelmongenerations.common.entity.npcs.registry.ShopItemWithVariation;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.Optional;
import net.minecraft.crash.CrashReport;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ReportedException;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class VendingMachinePacket
implements IMessage {
    String itemID;
    BlockPos pos;

    public VendingMachinePacket() {
    }

    public VendingMachinePacket(BlockPos pos, String itemName) {
        this.pos = pos;
        this.itemID = itemName;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        int x = buf.readInt();
        int y = buf.readInt();
        int z = buf.readInt();
        this.pos = new BlockPos(x, y, z);
        this.itemID = ByteBufUtils.readUTF8String(buf);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.pos.getX());
        buf.writeInt(this.pos.getY());
        buf.writeInt(this.pos.getZ());
        ByteBufUtils.writeUTF8String(buf, this.itemID);
    }

    public static boolean checkInventoryCanFit(InventoryPlayer inventoryIn, ItemStack itemStackIn) {
        InventoryPlayer inventory = new InventoryPlayer(inventoryIn.player);
        if (itemStackIn != null && itemStackIn.getCount() != 0 && itemStackIn.getItem() != null) {
            try {
                int i;
                if (itemStackIn.isItemDamaged()) {
                    int i2 = inventory.getFirstEmptyStack();
                    if (i2 >= 0) {
                        inventory.mainInventory.set(i2, itemStackIn.copy());
                        inventory.mainInventory.get(i2).setAnimationsToGo(5);
                        itemStackIn.setCount(0);
                        return true;
                    }
                    if (inventory.player.capabilities.isCreativeMode) {
                        itemStackIn.setCount(0);
                        return true;
                    }
                    return false;
                }
                do {
                    i = itemStackIn.getCount();
                    itemStackIn.setCount(inventory.storePartialItemStack(itemStackIn));
                } while (itemStackIn.getCount() > 0 && itemStackIn.getCount() < i);
                if (itemStackIn.getCount() == i && inventory.player.capabilities.isCreativeMode) {
                    itemStackIn.setCount(0);
                    return true;
                }
                return itemStackIn.getCount() < i;
            }
            catch (Throwable throwable) {
                CrashReport crashreport = CrashReport.makeCrashReport(throwable, "Adding item to inventory");
                CrashReportCategory crashreportcategory = crashreport.makeCategory("Item being added");
                crashreportcategory.addCrashSection("Item ID", Item.getIdFromItem(itemStackIn.getItem()));
                crashreportcategory.addCrashSection("Item data", itemStackIn.getMetadata());
                crashreportcategory.addCrashSection("Item name", itemStackIn.getDisplayName());
                throw new ReportedException(crashreport);
            }
        }
        return false;
    }

    public static class Handler
    implements IMessageHandler<VendingMachinePacket, IMessage> {
        @Override
        public IMessage onMessage(VendingMachinePacket message, MessageContext ctx) {
            EntityPlayerMP p = ctx.getServerHandler().player;
            BlockVendingMachine vendingMachine = (BlockVendingMachine)p.world.getBlockState(message.pos).getBlock();
            if (vendingMachine == null) {
                return null;
            }
            Optional<PlayerStorage> optStorage = PixelmonStorage.pokeBallManager.getPlayerStorage(p);
            if (optStorage.isPresent()) {
                PlayerStorage storage = optStorage.get();
                ArrayList<ShopItemWithVariation> itemList = ((BlockVendingMachine)PixelmonBlocks.blueVendingMachineBlock).getShop().getItems();
                for (ShopItemWithVariation s : itemList) {
                    if (!s.getBaseShopItem().id.equals(message.itemID)) continue;
                    int cost = s.getBuyCost();
                    if (storage.getCurrency() < cost) continue;
                    ItemStack buyStack = s.getItem().copy();
                    buyStack.setCount(1);
                    NPCTransactionEvent.Vending event = new NPCTransactionEvent.Vending(p, vendingMachine, message.pos, buyStack, cost);
                    if (MinecraftForge.EVENT_BUS.post(event)) {
                        return null;
                    }
                    buyStack = event.getItem();
                    cost = event.getItemWorth();
                    if (storage.getCurrency() < cost || !VendingMachinePacket.checkInventoryCanFit(p.inventory, buyStack)) continue;
                    buyStack.setCount(1);
                    p.inventory.addItemStackToInventory(buyStack);
                    storage.removeCurrency(cost);
                    p.sendContainerToPlayer(p.inventoryContainer);
                    p.closeScreen();
                    return null;
                }
            }
            return null;
        }
    }
}

