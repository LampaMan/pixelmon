/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.trading;

import com.pixelmongenerations.client.gui.ClientTradingManager;
import io.netty.buffer.ByteBuf;
import java.util.UUID;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class RegisterTrader
implements IMessage {
    UUID uuid;

    public RegisterTrader() {
    }

    public RegisterTrader(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        if (this.uuid == null) {
            ByteBufUtils.writeUTF8String(buffer, "");
        } else {
            ByteBufUtils.writeUTF8String(buffer, this.uuid.toString());
        }
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        String uuidString = ByteBufUtils.readUTF8String(buffer);
        this.uuid = uuidString == null || uuidString.isEmpty() ? null : UUID.fromString(uuidString);
    }

    public static class Handler
    implements IMessageHandler<RegisterTrader, IMessage> {
        @Override
        public IMessage onMessage(RegisterTrader message, MessageContext ctx) {
            ClientTradingManager.findTradePartner(message.uuid);
            return null;
        }
    }
}

