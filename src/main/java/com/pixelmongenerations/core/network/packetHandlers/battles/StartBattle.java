/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.rules.BattleRules;
import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class StartBattle
implements IMessage {
    int battleControllerIndex;
    ParticipantType[][] type;
    int afkActivate;
    int afkTurn;
    BattleRules rules;

    public StartBattle() {
    }

    public StartBattle(int battleControllerIndex, ParticipantType[][] type, BattleRules rules) {
        this(battleControllerIndex, type, -1, -1, rules);
    }

    public StartBattle(int battleControllerIndex, ParticipantType[][] type, int afkActivate, int afkTurn, BattleRules rules) {
        this.battleControllerIndex = battleControllerIndex;
        this.type = type;
        this.afkActivate = afkActivate;
        this.afkTurn = afkTurn;
        this.rules = rules;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.battleControllerIndex);
        for (int i = 0; i < 2; ++i) {
            buffer.writeShort(this.type[i].length);
            for (ParticipantType p : this.type[i]) {
                buffer.writeInt(p.ordinal());
            }
        }
        buffer.writeInt(this.afkActivate);
        buffer.writeInt(this.afkTurn);
        this.rules.encodeInto(buffer);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.battleControllerIndex = buffer.readInt();
        this.type = new ParticipantType[2][];
        for (int i = 0; i < 2; ++i) {
            this.type[i] = new ParticipantType[buffer.readShort()];
            for (int j = 0; j < this.type[i].length; ++j) {
                this.type[i][j] = ParticipantType.get(buffer.readInt());
            }
        }
        this.afkActivate = buffer.readInt();
        this.afkTurn = buffer.readInt();
        this.rules = new BattleRules(buffer);
    }

    public static class Handler
    implements IMessageHandler<StartBattle, IMessage> {
        @Override
        public IMessage onMessage(StartBattle message, MessageContext ctx) {
            ClientProxy.battleManager.startBattle(message.battleControllerIndex, message.type, message.afkActivate, message.afkTurn, message.rules);
            return null;
        }
    }
}

