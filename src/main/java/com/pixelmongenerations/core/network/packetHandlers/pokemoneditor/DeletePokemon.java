/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pokemoneditor;

import com.pixelmongenerations.client.gui.pokemoneditor.GuiPokemonEditorParty;
import com.pixelmongenerations.common.item.ItemPokemonEditor;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class DeletePokemon
implements IMessage {
    UUID playerID;
    int slot;

    public DeletePokemon() {
    }

    public DeletePokemon(int position) {
        this.playerID = GuiPokemonEditorParty.editedPlayerUUID;
        this.slot = position;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        PixelmonMethods.toBytesUUID(buf, this.playerID);
        buf.writeInt(this.slot);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.playerID = PixelmonMethods.fromBytesUUID(buf);
        this.slot = buf.readInt();
    }

    public static void deletePokemon(UUID playerID, int slot, MessageContext ctx) {
        EntityPlayerMP player = ctx.getServerHandler().player;
        Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorageFromUUID(player.getServer(), playerID);
        if (optstorage.isPresent()) {
            PlayerStorage storage = optstorage.get();
            storage.removeFromPartyPlayer(slot);
            ItemPokemonEditor.updateSinglePokemon(player, playerID, slot);
        }
    }

    public static class Handler
    implements IMessageHandler<DeletePokemon, IMessage> {
        @Override
        public IMessage onMessage(DeletePokemon message, MessageContext ctx) {
            if (!ItemPokemonEditor.checkPermission(ctx.getServerHandler().player)) {
                return null;
            }
            DeletePokemon.deletePokemon(message.playerID, message.slot, ctx);
            return null;
        }
    }
}

