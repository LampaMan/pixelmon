/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.MoverType;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class RotateEntity
implements IMessage {
    int entityID;
    float yaw;
    float pitch;

    public RotateEntity() {
    }

    public RotateEntity(int entityID, float yaw, float pitch) {
        this.entityID = entityID;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.entityID = buffer.readInt();
        this.yaw = buffer.readFloat();
        this.pitch = buffer.readFloat();
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.entityID);
        buffer.writeFloat(this.yaw);
        buffer.writeFloat(this.pitch);
    }

    public static class Handler
    implements IMessageHandler<RotateEntity, IMessage> {
        @Override
        public IMessage onMessage(RotateEntity message, MessageContext ctx) {
            Entity entity = Minecraft.getMinecraft().world.getEntityByID(message.entityID);
            if (entity != null) {
                entity.setPositionAndRotation(entity.posX, entity.posY, entity.posZ, message.yaw, message.pitch);
                entity.rotationYaw = message.yaw;
                entity.move(MoverType.SELF, 0.1, 0.1, 0.1);
            }
            return null;
        }
    }
}

