/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.client.gui.overlay.OverlayType;
import com.pixelmongenerations.client.gui.overlay.SpectateOverlay;
import com.pixelmongenerations.common.item.ItemPokeball;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.storage.PCClientStorage;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ServerConfigList
implements IMessage {
    int computerBoxCount = PlayerComputerStorage.boxCount = PixelmonConfig.computerBoxes;
    boolean allowCapturingOutsideBattle = ItemPokeball.allowCapturingOutsideBattle = PixelmonConfig.allowCapturingOutsideBattle;
    public int maxLevel = PixelmonConfig.maxLevel;
    public float ridingSpeedMultiplier = PixelmonConfig.ridingSpeedMultiplier;
    public boolean afkHandlerOn = PixelmonConfig.afkHandlerOn;
    public int afkTimerActivateSeconds = PixelmonConfig.afkTimerActivateSeconds;

    @Override
    public void fromBytes(ByteBuf buf) {
        this.computerBoxCount = buf.readInt();
        this.allowCapturingOutsideBattle = buf.readBoolean();
        this.maxLevel = buf.readInt();
        this.ridingSpeedMultiplier = buf.readFloat();
        this.afkHandlerOn = buf.readBoolean();
        this.afkTimerActivateSeconds = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.computerBoxCount);
        buf.writeBoolean(this.allowCapturingOutsideBattle);
        buf.writeInt(this.maxLevel);
        buf.writeFloat(this.ridingSpeedMultiplier);
        buf.writeBoolean(this.afkHandlerOn);
        buf.writeInt(this.afkTimerActivateSeconds);
    }

    public static class Handler
    implements IMessageHandler<ServerConfigList, IMessage> {
        @Override
        public IMessage onMessage(ServerConfigList message, MessageContext ctx) {
            PlayerComputerStorage.boxCount = message.computerBoxCount;
            PCClientStorage.refreshStore();
            ItemPokeball.allowCapturingOutsideBattle = message.allowCapturingOutsideBattle;
            ((SpectateOverlay)GuiPixelmonOverlay.getOverlay(OverlayType.SPECTATE)).hideSpectateMessage(null);
            PixelmonServerConfig.updateFromServer(message);
            return null;
        }
    }
}

