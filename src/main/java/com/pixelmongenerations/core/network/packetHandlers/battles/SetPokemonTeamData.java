/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.client.gui.battles.PixelmonInGui;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SetPokemonTeamData
implements IMessage {
    PixelmonInGui[] data;

    public SetPokemonTeamData() {
    }

    public SetPokemonTeamData(ArrayList<PixelmonWrapper> otherTeamPokemon) {
        this.data = new PixelmonInGui[otherTeamPokemon.size()];
        for (int i = 0; i < otherTeamPokemon.size(); ++i) {
            this.data[i] = new PixelmonInGui(otherTeamPokemon.get(i));
        }
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeShort(this.data.length);
        for (PixelmonInGui pg : this.data) {
            pg.encodeInto(buffer);
        }
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.data = new PixelmonInGui[buffer.readShort()];
        for (int i = 0; i < this.data.length; ++i) {
            this.data[i] = new PixelmonInGui();
            this.data[i].decodeInto(buffer);
        }
    }

    public static class Handler
    implements IMessageHandler<SetPokemonTeamData, IMessage> {
        @Override
        public IMessage onMessage(SetPokemonTeamData message, MessageContext ctx) {
            ClientProxy.battleManager.setTeamData(message.data);
            return null;
        }
    }
}

