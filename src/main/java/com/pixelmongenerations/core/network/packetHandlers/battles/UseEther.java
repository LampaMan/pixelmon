/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.EntityLink;
import com.pixelmongenerations.common.item.ItemEther;
import com.pixelmongenerations.common.item.heldItems.ItemBerryLeppa;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class UseEther
implements IMessage {
    int moveIndex;
    int[] id;

    public UseEther() {
    }

    public UseEther(int moveIndex, int[] id) {
        this.moveIndex = moveIndex;
        this.id = id;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.moveIndex);
        for (int i : this.id) {
            buf.writeInt(i);
        }
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.moveIndex = buf.readInt();
        this.id = new int[]{buf.readInt(), buf.readInt()};
    }

    public static class Handler
    implements IMessageHandler<UseEther, IMessage> {
        @Override
        public IMessage onMessage(UseEther message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            ItemStack itemStack = player.getHeldItemMainhand();
            if (itemStack != null) {
                PlayerStorage storage;
                EntityPixelmon p;
                Attack attack;
                Item item = itemStack.getItem();
                Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
                if (optstorage.isPresent() && (attack = (p = (storage = optstorage.get()).getPokemon(message.id, player.world)).getMoveset().get(message.moveIndex)) != null) {
                    boolean succeeded = false;
                    ItemEther ether = null;
                    if (item instanceof ItemEther) {
                        ether = (ItemEther)item;
                    } else if (item instanceof ItemBerryLeppa) {
                        ether = (ItemEther)PixelmonItems.ether;
                    } else {
                        return null;
                    }
                    succeeded = ether.useEther(new EntityLink(p), message.moveIndex);
                    if (succeeded && !player.capabilities.isCreativeMode) {
                        player.inventory.clearMatchingItems(item, itemStack.getMetadata(), 1, itemStack.getTagCompound());
                    }
                }
            }
            return null;
        }
    }
}

