/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.network.packetHandlers;

public enum EnumMovementType {
    Riding,
    Custom;


    public static EnumMovementType getFromOrdinal(int ordinal) {
        for (EnumMovementType m : EnumMovementType.values()) {
            if (m.ordinal() != ordinal) continue;
            return m;
        }
        return null;
    }
}

