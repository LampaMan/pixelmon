/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.client.gui.battles.battleScreens.Dynamax;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class DynamaxPacket
implements IMessage {
    int[] pokemonID;
    boolean revertDynamax = false;

    public DynamaxPacket() {
    }

    public DynamaxPacket(int[] pokemonID) {
        this.pokemonID = pokemonID;
    }

    public DynamaxPacket(int[] pokemonID, boolean revertDynamax) {
        this.pokemonID = pokemonID;
        this.revertDynamax = revertDynamax;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        PixelmonMethods.toBytesPokemonID(buf, this.pokemonID);
        buf.writeBoolean(this.revertDynamax);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.pokemonID = PixelmonMethods.fromBytesPokemonID(buf);
        this.revertDynamax = buf.readBoolean();
    }

    public static class Handler
    implements IMessageHandler<DynamaxPacket, IMessage> {
        @Override
        public IMessage onMessage(DynamaxPacket message, MessageContext ctx) {
            if (message.revertDynamax) {
                ClientProxy.battleManager.dynamaxing = false;
                ClientProxy.battleManager.dynamax = null;
                ClientProxy.battleManager.hasDynamaxed = true;
            } else {
                ClientProxy.battleManager.dynamax = message.pokemonID;
                ClientProxy.battleManager.mode = BattleMode.Dynamax;
                ClientProxy.battleManager.hasDynamaxed = false;
                Dynamax.selectEntity();
            }
            return null;
        }
    }
}

