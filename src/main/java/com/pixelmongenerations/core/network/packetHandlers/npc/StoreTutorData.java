/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.npc;

import com.pixelmongenerations.client.gui.npc.GuiTutor;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCTutor;
import com.pixelmongenerations.common.item.ItemNPCEditor;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class StoreTutorData
implements IMessage {
    ArrayList<Attack> attackList;
    ArrayList<ArrayList<ItemStack>> costs;
    int tutorID;

    public StoreTutorData() {
    }

    public StoreTutorData(int tutorID) {
        this.attackList = GuiTutor.attackList;
        this.costs = GuiTutor.costs;
        this.tutorID = tutorID;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.attackList = new ArrayList();
        this.costs = new ArrayList();
        NPCTutor.decode(buf, this.attackList, this.costs);
        this.tutorID = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        NPCTutor.encode(buf, this.attackList, this.costs);
        buf.writeInt(this.tutorID);
    }

    public static class Handler
    implements IMessageHandler<StoreTutorData, IMessage> {
        @Override
        public IMessage onMessage(StoreTutorData message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            if (!ItemNPCEditor.checkPermission(player)) {
                return null;
            }
            Optional<NPCTutor> entityNPCOptional = EntityNPC.locateNPCServer(player.world, message.tutorID, NPCTutor.class);
            if (entityNPCOptional.isPresent()) {
                NPCTutor tutor = entityNPCOptional.get();
                tutor.attackList = message.attackList;
                tutor.costs = message.costs;
            }
            return null;
        }
    }
}

