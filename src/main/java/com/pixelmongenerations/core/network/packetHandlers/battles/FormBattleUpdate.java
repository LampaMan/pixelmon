/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.client.gui.battles.PixelmonInGui;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class FormBattleUpdate
implements IMessage {
    int[] id;
    short form;

    public FormBattleUpdate() {
    }

    public FormBattleUpdate(int[] id, int form) {
        this.id = id;
        this.form = (short)form;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        PixelmonMethods.toBytesPokemonID(buf, this.id);
        buf.writeShort((int)this.form);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.id = PixelmonMethods.fromBytesPokemonID(buf);
        this.form = buf.readShort();
    }

    public static class Handler
    implements IMessageHandler<FormBattleUpdate, IMessage> {
        @Override
        public IMessage onMessage(FormBattleUpdate message, MessageContext ctx) {
            PixelmonInGui pokemon = ClientProxy.battleManager.getPokemon(message.id);
            if (pokemon != null && !pokemon.isSwitching) {
                pokemon.form = message.form;
            }
            return null;
        }
    }
}

