/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.npc;

import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.ClearTrainerPokemon;
import com.pixelmongenerations.core.network.packetHandlers.npc.StoreTrainerPokemon;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class DeleteTrainerPokemon
implements IMessage {
    int trainerID;
    int position;

    public DeleteTrainerPokemon() {
    }

    public DeleteTrainerPokemon(int trainerID, int position) {
        this.trainerID = trainerID;
        this.position = position;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.trainerID);
        buffer.writeInt(this.position);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.trainerID = buffer.readInt();
        this.position = buffer.readInt();
    }

    public static void deletePokemon(int trainerID, int position, MessageContext ctx, boolean updateClient) {
        EntityPlayerMP p = ctx.getServerHandler().player;
        Optional<NPCTrainer> entityNPCOptional = EntityNPC.locateNPCServer(p.world, trainerID, NPCTrainer.class);
        if (!entityNPCOptional.isPresent()) {
            return;
        }
        NPCTrainer t = entityNPCOptional.get();
        PlayerStorage storage = t.getPokemonStorage();
        if (storage.count() > 1) {
            storage.removeFromPartyTrainer(position);
        }
        t.updateLvl();
        if (updateClient) {
            Pixelmon.NETWORK.sendTo(new ClearTrainerPokemon(), p);
            NBTTagCompound[] storageList = storage.getList();
            for (int i = 0; i < storage.count(); ++i) {
                Pixelmon.NETWORK.sendTo(new StoreTrainerPokemon(new PixelmonData(storageList[i])), p);
            }
        }
    }

    public static class Handler
    implements IMessageHandler<DeleteTrainerPokemon, IMessage> {
        @Override
        public IMessage onMessage(DeleteTrainerPokemon message, MessageContext ctx) {
            DeleteTrainerPokemon.deletePokemon(message.trainerID, message.position, ctx, true);
            return null;
        }
    }
}

