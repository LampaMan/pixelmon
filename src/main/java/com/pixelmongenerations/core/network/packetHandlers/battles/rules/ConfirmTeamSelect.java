/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles.rules;

import com.pixelmongenerations.common.battle.rules.teamselection.TeamSelection;
import com.pixelmongenerations.common.battle.rules.teamselection.TeamSelectionList;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ConfirmTeamSelect
implements IMessage {
    int teamSelectID;
    int[] selection;
    boolean force;

    public ConfirmTeamSelect() {
    }

    public ConfirmTeamSelect(int teamSelectID, int[] selection, boolean force) {
        this.teamSelectID = teamSelectID;
        this.selection = selection;
        this.force = force;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.teamSelectID);
        for (int s : this.selection) {
            buf.writeInt(s);
        }
        buf.writeBoolean(this.force);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.teamSelectID = buf.readInt();
        this.selection = new int[6];
        for (int i = 0; i < this.selection.length; ++i) {
            this.selection[i] = buf.readInt();
        }
        this.force = buf.readBoolean();
    }

    public static class Handler
    implements IMessageHandler<ConfirmTeamSelect, IMessage> {
        @Override
        public IMessage onMessage(ConfirmTeamSelect message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            player.getServer().addScheduledTask(() -> {
                TeamSelection ts = TeamSelectionList.getTeamSelection(message.teamSelectID);
                if (ts == null) {
                    return;
                }
                ts.registerTeamSelect(player, message.selection, message.force);
            });
            return null;
        }
    }
}

