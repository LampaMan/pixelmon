/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.clientStorage;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.battles.PixelmonInGui;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class Add
implements IMessage {
    PixelmonData data;
    boolean updateBattle = false;
    boolean updateSpectator = false;

    public Add() {
    }

    public Add(PixelmonData data) {
        this(data, false, false);
    }

    public Add(PixelmonData data, boolean updateBattle) {
        this(data, updateBattle, false);
    }

    public Add(PixelmonData data, boolean updateBattle, boolean updateSpectator) {
        this.data = data;
        this.updateBattle = updateBattle;
        this.updateSpectator = updateSpectator;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        this.data.encodeInto(buffer);
        buffer.writeBoolean(this.updateBattle);
        buffer.writeBoolean(this.updateSpectator);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.data = new PixelmonData();
        this.data.decodeInto(buffer);
        this.updateBattle = buffer.readBoolean();
        this.updateSpectator = buffer.readBoolean();
    }

    public static class Handler
    implements IMessageHandler<Add, IMessage> {
        @Override
        public IMessage onMessage(Add message, MessageContext ctx) {
            PixelmonInGui pokemon;
            if (!message.updateSpectator) {
                ServerStorageDisplay.add(message.data);
            }
            if (message.updateBattle && (pokemon = ClientProxy.battleManager.getPokemon(message.data.pokemonID)) != null) {
                float saveHealth = pokemon.health;
                pokemon.set(message.data);
                pokemon.health = saveHealth;
            }
            return null;
        }
    }
}

