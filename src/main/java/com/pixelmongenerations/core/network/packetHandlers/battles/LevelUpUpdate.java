/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.client.gui.battles.PixelmonInGui;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class LevelUpUpdate
implements IMessage {
    public int[] pixelmonID;
    public int level;
    public int currentHP;
    public int maxHP;

    public LevelUpUpdate() {
    }

    public LevelUpUpdate(int[] pixelmonID, int level, int currentHP, int maxHP) {
        this.pixelmonID = pixelmonID;
        this.level = level;
        this.currentHP = currentHP;
        this.maxHP = maxHP;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.pixelmonID = new int[]{buf.readInt(), buf.readInt()};
        this.level = buf.readInt();
        this.currentHP = buf.readInt();
        this.maxHP = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        for (int id : this.pixelmonID) {
            buf.writeInt(id);
        }
        buf.writeInt(this.level);
        buf.writeInt(this.currentHP);
        buf.writeInt(this.maxHP);
    }

    public static class Handler
    implements IMessageHandler<LevelUpUpdate, IMessage> {
        @Override
        public IMessage onMessage(LevelUpUpdate message, MessageContext ctx) {
            for (PixelmonInGui pokemon : ClientProxy.battleManager.displayedOurPokemon) {
                this.updatePokemon(pokemon, message);
            }
            for (PixelmonInGui pokemon : ClientProxy.battleManager.displayedEnemyPokemon) {
                this.updatePokemon(pokemon, message);
            }
            if (ClientProxy.battleManager.displayedAllyPokemon != null) {
                for (PixelmonInGui pokemon : ClientProxy.battleManager.displayedAllyPokemon) {
                    this.updatePokemon(pokemon, message);
                }
            }
            return null;
        }

        private void updatePokemon(PixelmonInGui pokemon, LevelUpUpdate message) {
            if (PixelmonMethods.isIDSame(pokemon.pokemonID, message.pixelmonID)) {
                pokemon.level = message.level;
                pokemon.health = message.currentHP;
                pokemon.maxHealth = message.maxHP;
            }
        }
    }
}

