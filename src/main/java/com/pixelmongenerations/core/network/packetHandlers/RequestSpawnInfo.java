/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableList
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.google.common.collect.ImmutableList;
import com.pixelmongenerations.api.spawning.SpawnSet;
import com.pixelmongenerations.api.spawning.archetypes.entities.pokemon.SpawnInfoPokemon;
import com.pixelmongenerations.api.spawning.util.SetLoader;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.packetHandlers.ResponseSpawnInfo;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import java.util.stream.Collectors;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class RequestSpawnInfo
implements IMessage {
    public int dex;
    public String biomes;
    public String times;

    public RequestSpawnInfo() {
    }

    public RequestSpawnInfo(int dex) {
        this.dex = dex;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.dex = buffer.readInt();
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.dex);
    }

    public static class Handler
    implements IMessageHandler<RequestSpawnInfo, IMessage> {
        @Override
        public IMessage onMessage(RequestSpawnInfo message, MessageContext ctx) {
            Optional<EnumSpecies> pokemonOpt;
            EntityPlayerMP player = ctx.getServerHandler().player;
            Optional<PlayerStorage> storage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
            if (storage.isPresent() && storage.get().pokedex.hasSeen(message.dex) && (pokemonOpt = EnumSpecies.getFromDex(message.dex)).isPresent()) {
                EnumSpecies pokemon = pokemonOpt.get();
                ImmutableList<SpawnSet> spawnSets = SetLoader.getAllSets();
                spawnSets.forEach(s -> s.spawnInfos.forEach(si -> {
                    if (si instanceof SpawnInfoPokemon) {
                        SpawnInfoPokemon sip = (SpawnInfoPokemon)si;
                        if (sip.getPokemonSpec().name.equalsIgnoreCase(pokemon.name)) {
                            String biomes = "";
                            if (sip.condition.biomes != null) {
                                biomes = sip.condition.biomes.stream().map(b -> b.biomeName).collect(Collectors.joining(", "));
                            }
                            String times = "";
                            if (sip.condition.times != null) {
                                times = sip.condition.times.stream().map(t -> t.getDisplayName()).collect(Collectors.joining(", "));
                            }
                            message.biomes = biomes;
                            message.times = times;
                        }
                    }
                }));
                if (message.biomes != null && message.times != null) {
                    return new ResponseSpawnInfo(message.biomes, message.times);
                }
            }
            return null;
        }
    }
}

