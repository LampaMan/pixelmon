/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.common.starter.CustomStarters;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.StarterList;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.network.packetHandlers.PokemonListPacket;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class StarterListPacket
extends PokemonListPacket {
    public boolean isShiny = CustomStarters.shinyStarter;

    public StarterListPacket() {
        super(StarterList.getStarterList());
    }

    public static class Handler
    implements IMessageHandler<StarterListPacket, IMessage> {
        @Override
        public IMessage onMessage(StarterListPacket message, MessageContext ctx) {
            Minecraft mc = Minecraft.getMinecraft();
            mc.addScheduledTask(() -> {
                if (mc.player != null) {
                    ServerStorageDisplay.starterListPacket = message;
                    if (mc.currentScreen == null) {
                        mc.player.openGui(Pixelmon.INSTANCE, EnumGui.ChooseStarter.getIndex(), mc.player.world, 0, 0, 0);
                    }
                }
            });
            return null;
        }
    }
}

