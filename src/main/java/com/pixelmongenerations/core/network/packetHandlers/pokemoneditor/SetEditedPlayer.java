/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pokemoneditor;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiPokemonEditorParty;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import java.util.Arrays;
import java.util.UUID;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SetEditedPlayer
implements IMessage {
    UUID playerID;
    String playerName;
    boolean isSelf;
    PixelmonData[] pokemon;

    public SetEditedPlayer() {
    }

    public SetEditedPlayer(UUID playerID, PixelmonData[] pokemon) {
        this(playerID, "", pokemon);
    }

    public SetEditedPlayer(UUID playerID, String playerName, PixelmonData[] pokemon) {
        this.playerID = playerID;
        this.playerName = playerName;
        this.isSelf = pokemon == null;
        this.pokemon = pokemon;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        PixelmonMethods.toBytesUUID(buf, this.playerID);
        ByteBufUtils.writeUTF8String(buf, this.playerName);
        buf.writeBoolean(this.isSelf);
        if (!this.isSelf) {
            for (PixelmonData data : this.pokemon) {
                if (data == null) {
                    buf.writeBoolean(false);
                    continue;
                }
                buf.writeBoolean(true);
                data.encodeInto(buf);
            }
        }
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.playerID = PixelmonMethods.fromBytesUUID(buf);
        this.playerName = ByteBufUtils.readUTF8String(buf);
        this.isSelf = buf.readBoolean();
        if (!this.isSelf) {
            this.pokemon = new PixelmonData[6];
            for (int i = 0; i < this.pokemon.length; ++i) {
                if (!buf.readBoolean()) continue;
                this.pokemon[i] = new PixelmonData(buf);
            }
        }
    }

    public static class Handler
    implements IMessageHandler<SetEditedPlayer, IMessage> {
        @Override
        public IMessage onMessage(SetEditedPlayer message, MessageContext ctx) {
            GuiPokemonEditorParty.editedPlayerUUID = message.playerID;
            if (!message.playerName.isEmpty()) {
                GuiPokemonEditorParty.editedPlayerName = message.playerName;
            }
            PixelmonData[] pokemon = message.isSelf ? ServerStorageDisplay.getPokemon() : message.pokemon;
            ServerStorageDisplay.editedPokemon = Arrays.asList(pokemon);
            return null;
        }
    }
}

