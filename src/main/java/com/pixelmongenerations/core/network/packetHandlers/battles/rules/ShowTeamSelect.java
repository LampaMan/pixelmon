/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles.rules;

import com.pixelmongenerations.client.gui.battles.rules.GuiTeamSelect;
import com.pixelmongenerations.common.battle.rules.BattleRules;
import com.pixelmongenerations.common.battle.rules.teamselection.TeamSelectPokemon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.util.PixelmonMethods;
import com.pixelmongenerations.core.util.helper.ArrayHelper;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ShowTeamSelect
implements IMessage {
    public int teamSelectID;
    public String[] disabled;
    public List<TeamSelectPokemon> opponentTeam;
    public int opponentSize;
    public int npcID;
    public String npcName = "";
    public UUID opponentUUID;
    BattleRules rules;
    boolean showRules;

    public ShowTeamSelect() {
    }

    private ShowTeamSelect(int teamSelectID, String[] disabled, List<TeamSelectPokemon> opponentTeam, BattleRules rules, boolean showRules) {
        this.teamSelectID = teamSelectID;
        this.disabled = disabled;
        if (rules.teamPreview) {
            this.opponentTeam = opponentTeam;
        } else {
            this.opponentSize = opponentTeam.size();
        }
        this.rules = rules;
        this.showRules = showRules;
    }

    public ShowTeamSelect(int teamSelectID, String[] disabled, List<TeamSelectPokemon> opponentTeam, int npcID, String npcName, BattleRules rules, boolean showRules) {
        this(teamSelectID, disabled, opponentTeam, rules, showRules);
        this.npcID = npcID;
        this.npcName = npcName;
    }

    public ShowTeamSelect(int teamSelectID, String[] disabled, List<TeamSelectPokemon> opponentTeam, UUID opponentUUID, BattleRules rules, boolean showRules) {
        this(teamSelectID, disabled, opponentTeam, rules, showRules);
        this.opponentUUID = opponentUUID;
    }

    public boolean isAllDisabled() {
        for (String s : this.disabled) {
            if (s != null && !s.isEmpty()) continue;
            return false;
        }
        return true;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.teamSelectID);
        ArrayHelper.encodeStringArray(buf, this.disabled);
        buf.writeBoolean(this.rules.teamPreview);
        if (this.rules.teamPreview) {
            ArrayHelper.encodeList(buf, this.opponentTeam);
        } else {
            buf.writeInt(this.opponentSize);
        }
        if (this.opponentUUID == null) {
            buf.writeBoolean(true);
            buf.writeInt(this.npcID);
            ByteBufUtils.writeUTF8String(buf, this.npcName);
        } else {
            buf.writeBoolean(false);
            PixelmonMethods.toBytesUUID(buf, this.opponentUUID);
        }
        this.rules.encodeInto(buf);
        buf.writeBoolean(this.showRules);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.teamSelectID = buf.readInt();
        this.disabled = ArrayHelper.decodeStringArray(buf);
        if (buf.readBoolean()) {
            int length = buf.readInt();
            this.opponentTeam = new ArrayList<TeamSelectPokemon>();
            for (int i = 0; i < length; ++i) {
                this.opponentTeam.add(new TeamSelectPokemon(buf));
            }
        } else {
            this.opponentSize = buf.readInt();
        }
        if (buf.readBoolean()) {
            this.npcID = buf.readInt();
            this.npcName = ByteBufUtils.readUTF8String(buf);
        } else {
            this.opponentUUID = PixelmonMethods.fromBytesUUID(buf);
        }
        this.rules = new BattleRules(buf);
        this.showRules = buf.readBoolean();
    }

    public static class Handler
    implements IMessageHandler<ShowTeamSelect, IMessage> {
        @Override
        public IMessage onMessage(ShowTeamSelect message, MessageContext ctx) {
            ClientProxy.battleManager.rules = message.rules;
            Minecraft mc = Minecraft.getMinecraft();
            EnumGui gui = message.showRules ? EnumGui.BattleRulesFixed : EnumGui.TeamSelect;
            GuiTeamSelect.teamSelectPacket = message;
            mc.addScheduledTask(() -> mc.player.openGui(Pixelmon.INSTANCE, gui.getIndex(), mc.player.world, 0, 0, 0));
            return null;
        }
    }
}

