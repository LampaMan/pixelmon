/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ParticipantReady
implements IMessage {
    public String uuid;
    public int battleIndex;

    public ParticipantReady() {
    }

    public ParticipantReady(int battleControllerIndex, String uuid) {
        this.uuid = uuid;
        this.battleIndex = battleControllerIndex;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.battleIndex = buf.readInt();
        this.uuid = ByteBufUtils.readUTF8String(buf);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.battleIndex);
        ByteBufUtils.writeUTF8String(buf, this.uuid);
    }

    public static class Handler
    implements IMessageHandler<ParticipantReady, IMessage> {
        @Override
        public IMessage onMessage(ParticipantReady message, MessageContext ctx) {
            ctx.getServerHandler().player.getServer().addScheduledTask(() -> {
                BattleControllerBase bc = BattleRegistry.getBattle(message.battleIndex);
                if (bc == null) {
                    return;
                }
                bc.participants.stream().filter(bp -> bp instanceof PlayerParticipant).filter(bp -> ((PlayerParticipant)bp).player.getUniqueID().toString().equals(message.uuid)).forEach(bp -> bc.participantReady((PlayerParticipant)bp));
            });
            return null;
        }
    }
}

