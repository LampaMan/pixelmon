/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.evolution;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.network.packetHandlers.evolution.EvolutionStage;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class EvolvePokemon
implements IMessage {
    int[] pokemonID;
    EvolutionStage stage;

    public EvolvePokemon() {
    }

    public EvolvePokemon(int[] pokemonID, EvolutionStage stage) {
        this.pokemonID = pokemonID;
        this.stage = stage;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.pokemonID = new int[]{buffer.readInt(), buffer.readInt()};
        this.stage = EvolutionStage.values()[buffer.readInt()];
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.pokemonID[0]);
        buffer.writeInt(this.pokemonID[1]);
        buffer.writeInt(this.stage.ordinal());
    }

    public static class Handler
    implements IMessageHandler<EvolvePokemon, IMessage> {
        @Override
        public IMessage onMessage(EvolvePokemon message, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(() -> this.evolvePokemon(message));
            return null;
        }

        private void evolvePokemon(EvolvePokemon message) {
            EntityPixelmon pokemon = GuiHelper.getEntity(message.pokemonID);
            if (pokemon != null) {
                pokemon.setEvolutionAnimationStage(message.stage);
                pokemon.evoAnimTicks = 0;
            }
        }
    }
}

