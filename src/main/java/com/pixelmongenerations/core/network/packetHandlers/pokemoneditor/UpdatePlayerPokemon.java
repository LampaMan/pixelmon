/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pokemoneditor;

import com.pixelmongenerations.client.gui.pokemoneditor.GuiPokemonEditorParty;
import com.pixelmongenerations.common.item.ItemPokemonEditor;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.UpdateEditedPokemon;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class UpdatePlayerPokemon
extends UpdateEditedPokemon {
    UUID playerID;

    public UpdatePlayerPokemon() {
    }

    public UpdatePlayerPokemon(PixelmonData data) {
        super(data);
        this.playerID = GuiPokemonEditorParty.editedPlayerUUID;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        super.toBytes(buffer);
        PixelmonMethods.toBytesUUID(buffer, this.playerID);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        super.fromBytes(buffer);
        this.playerID = PixelmonMethods.fromBytesUUID(buffer);
    }

    public static class Handler
    implements IMessageHandler<UpdatePlayerPokemon, IMessage> {
        @Override
        public IMessage onMessage(UpdatePlayerPokemon message, MessageContext ctx) {
            Optional<PlayerStorage> optstorage;
            EntityPlayerMP sender = ctx.getServerHandler().player;
            if (!ItemPokemonEditor.checkPermission(sender)) {
                return null;
            }
            EntityPlayerMP player = sender.getServer().getPlayerList().getPlayerByUUID(message.playerID);
            if (player != null && (optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player)).isPresent()) {
                PlayerStorage storage = optstorage.get();
                sender.getServer().addScheduledTask(() -> {
                    UpdateEditedPokemon.updatePokemon(message, player, storage);
                    ItemPokemonEditor.updateSinglePokemon(sender, message.playerID, message.data.order);
                    storage.sendUpdatedList();
                });
            }
            return null;
        }
    }
}

