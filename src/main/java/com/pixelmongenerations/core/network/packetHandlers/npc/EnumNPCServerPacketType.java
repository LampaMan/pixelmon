/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.network.packetHandlers.npc;

public enum EnumNPCServerPacketType {
    AI,
    BattleAI,
    BossMode,
    ChooseNPC,
    CustomSteveTexture,
    EncounterMode,
    BattleRules,
    Model,
    Name,
    Pokemon,
    TextureIndex,
    ChatPages,
    Trader,
    Relearner,
    Tutor,
    CycleTexture,
    CycleJson,
    CycleName,
    RefreshItems;

}

