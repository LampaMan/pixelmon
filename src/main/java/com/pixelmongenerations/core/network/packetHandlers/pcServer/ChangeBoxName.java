/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pcServer;

import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.storage.PCServer;
import io.netty.buffer.ByteBuf;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ChangeBoxName
implements IMessage {
    private static String regex = "^[a-zA-Z0-9*$()-_:\\s]+$";
    private static Pattern pattern = Pattern.compile(regex, 8);
    private int box;
    private String name;

    public ChangeBoxName() {
    }

    public ChangeBoxName(int box, String name) {
        this.box = box;
        this.name = name;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.box);
        ByteBufUtils.writeUTF8String(buffer, this.name);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.box = buffer.readInt();
        this.name = ByteBufUtils.readUTF8String(buffer);
    }

    public static class Handler
    implements IMessageHandler<ChangeBoxName, IMessage> {
        @Override
        public IMessage onMessage(ChangeBoxName message, MessageContext ctx) {
            if (message.box < -1 || message.box >= PixelmonConfig.computerBoxes) {
                return null;
            }
            if (message.name.length() > 20) {
                return null;
            }
            Matcher matcher = pattern.matcher(message.name);
            if (!matcher.matches()) {
                return null;
            }
            ctx.getServerHandler().player.getServer().addScheduledTask(() -> PCServer.setBoxName(ctx.getServerHandler().player, message.box, message.name));
            return null;
        }
    }
}

