/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.network.packetHandlers.itemDrops;

public enum ItemDropMode {
    NormalPokemon,
    NormalTrainer,
    Boss,
    Totem,
    Other;

}

