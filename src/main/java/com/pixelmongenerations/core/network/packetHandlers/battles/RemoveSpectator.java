/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import io.netty.buffer.ByteBuf;
import java.util.UUID;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class RemoveSpectator
implements IMessage {
    public int battleControllerIndex;
    public UUID uuid;

    public RemoveSpectator() {
    }

    public RemoveSpectator(int battleControllerIndex, UUID uuid) {
        this.battleControllerIndex = battleControllerIndex;
        this.uuid = uuid;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.battleControllerIndex = buffer.readInt();
        this.uuid = new UUID(buffer.readLong(), buffer.readLong());
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.battleControllerIndex);
        buffer.writeLong(this.uuid.getMostSignificantBits());
        buffer.writeLong(this.uuid.getLeastSignificantBits());
    }

    public static class Handler
    implements IMessageHandler<RemoveSpectator, IMessage> {
        @Override
        public IMessage onMessage(RemoveSpectator message, MessageContext ctx) {
            ctx.getServerHandler().player.getServer().addScheduledTask(() -> this.processMessage(message, ctx));
            return null;
        }

        private void processMessage(RemoveSpectator message, MessageContext ctx) {
            BattleControllerBase bc = BattleRegistry.getBattle(message.battleControllerIndex);
            if (bc != null) {
                bc.removeSpectator(ctx.getServerHandler().player);
            }
        }
    }
}

