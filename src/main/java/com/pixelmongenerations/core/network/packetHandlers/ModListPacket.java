/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.api.events.ReceivedModListEvent;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLModContainer;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.CoreModManager;

public class ModListPacket
implements IMessage {
    public String[] modIds;

    public ModListPacket() {
    }

    public ModListPacket(String[] modIds) {
        this.modIds = modIds;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        int numMods = buffer.readInt();
        this.modIds = new String[numMods];
        for (int i = 0; i < numMods; ++i) {
            this.modIds[i] = ByteBufUtils.readUTF8String(buffer);
        }
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        ArrayList<FMLModContainer> modList = new ArrayList<>();
        List<ModContainer> mods = Loader.instance().getModList();
        modList.addAll(mods.stream().filter(mc -> mc instanceof FMLModContainer).map(mc -> (FMLModContainer)mc).collect(Collectors.toList()));
        buffer.writeInt(modList.size());
        for (FMLModContainer mc2 : modList) {
            ByteBufUtils.writeUTF8String(buffer, mc2.getModId());
        }
        for (String str : CoreModManager.getIgnoredMods()) {
            ByteBufUtils.writeUTF8String(buffer, str);
        }
    }

    public static class Handler
    implements IMessageHandler<ModListPacket, IMessage> {
        @Override
        public IMessage onMessage(ModListPacket message, MessageContext ctx) {
            MinecraftForge.EVENT_BUS.post(new ReceivedModListEvent(ctx.getServerHandler().player, message.modIds));
            return null;
        }
    }
}

