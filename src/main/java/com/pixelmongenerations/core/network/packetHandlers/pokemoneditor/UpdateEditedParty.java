/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pokemoneditor;

import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.UpdateEditedPokemon;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.List;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public abstract class UpdateEditedParty
implements IMessage {
    public List<UpdateEditedPokemon> party = new ArrayList<UpdateEditedPokemon>(6);

    protected UpdateEditedParty() {
    }

    protected UpdateEditedParty(List<PixelmonData> party) {
        for (PixelmonData data : party) {
            this.party.add(data == null ? null : this.createPokemonPacket(data));
        }
    }

    protected abstract UpdateEditedPokemon createPokemonPacket(PixelmonData var1);

    @Override
    public void toBytes(ByteBuf buf) {
        for (UpdateEditedPokemon data : this.party) {
            if (data == null) {
                buf.writeBoolean(false);
                continue;
            }
            buf.writeBoolean(true);
            data.toBytes(buf);
        }
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        for (int i = 0; i < 6; ++i) {
            if (!buf.readBoolean()) continue;
            this.party.add(this.readPokemonData(buf));
        }
    }

    protected abstract UpdateEditedPokemon readPokemonData(ByteBuf var1);
}

