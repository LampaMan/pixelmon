/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.common.entity.npcs.registry.PokemonForm;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.core.enums.EnumSpecies;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public abstract class PokemonListPacket
implements IMessage {
    public PokemonForm[] pokemonList;
    public int[] pokemonListIndex;

    public PokemonListPacket(EnumSpecies ... pokemon) {
        this(pokemon, new int[pokemon.length]);
    }

    public PokemonListPacket(EnumSpecies[] pokemon, int[] forms) {
        PokemonForm[] pokemonForms = new PokemonForm[pokemon.length];
        for (int i = 0; i < pokemon.length; ++i) {
            pokemonForms[i] = new PokemonForm(pokemon[i], forms[i]);
        }
        this.initialize(pokemonForms);
    }

    public PokemonListPacket(PokemonForm[] pokemon) {
        this.initialize(pokemon);
    }

    private void initialize(PokemonForm[] pokemon) {
        this.pokemonList = pokemon;
        this.pokemonListIndex = new int[pokemon.length];
        for (int i = 0; i < this.pokemonList.length; ++i) {
            int id;
            this.pokemonListIndex[i] = this.pokemonList[i] != null ? (Entity3HasStats.isAvailableGeneration(id = Entity3HasStats.getPokedexNumber(this.pokemonList[i].pokemon.name)) ? id : -2) : -1;
        }
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.pokemonList = new PokemonForm[buffer.readShort()];
        this.pokemonListIndex = new int[this.pokemonList.length];
        for (int i = 0; i < this.pokemonList.length; ++i) {
            int index;
            this.pokemonListIndex[i] = index = buffer.readInt();
            if (index < 0) continue;
            EnumSpecies pokemon = EnumSpecies.getFromName(Entity3HasStats.getBaseStats((int)index).pixelmonName).get();
            this.pokemonList[i] = new PokemonForm(pokemon, buffer.readShort());
        }
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeShort(this.pokemonList.length);
        for (int i = 0; i < this.pokemonListIndex.length; ++i) {
            buffer.writeInt(this.pokemonListIndex[i]);
            if (this.pokemonListIndex[i] < 0) continue;
            buffer.writeShort(this.pokemonList[i].form);
        }
    }
}

