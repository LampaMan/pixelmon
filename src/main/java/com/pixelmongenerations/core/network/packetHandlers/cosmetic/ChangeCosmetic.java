/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.cosmetic;

import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ChangeCosmetic
implements IMessage {
    int hashCode;

    public ChangeCosmetic() {
    }

    public ChangeCosmetic(int hashCode) {
        this.hashCode = hashCode;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.hashCode = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.hashCode);
    }

    public static class Handler
    implements IMessageHandler<ChangeCosmetic, IMessage> {
        @Override
        public IMessage onMessage(ChangeCosmetic message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            player.getServerWorld().addScheduledTask(() -> {
                Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
                if (!storageOpt.isPresent()) {
                    return;
                }
                PlayerStorage playerStorage = storageOpt.get();
                playerStorage.cosmeticData.setCosmetic(message.hashCode);
            });
            return null;
        }
    }
}

