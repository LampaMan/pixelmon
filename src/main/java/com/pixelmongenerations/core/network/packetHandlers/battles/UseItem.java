/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class UseItem
implements IMessage {
    public int itemID;

    public UseItem() {
    }

    public UseItem(Item item) {
        this.itemID = Item.getIdFromItem(item);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.itemID = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.itemID);
    }

    public static class Handler
    implements IMessageHandler<UseItem, IMessage> {
        @Override
        public IMessage onMessage(UseItem message, MessageContext ctx) {
            ItemStack item = new ItemStack(Item.getItemById(message.itemID));
            Minecraft.getMinecraft().player.inventory.clearMatchingItems(item.getItem(), item.getMetadata(), 1, item.getTagCompound());
            return null;
        }
    }
}

