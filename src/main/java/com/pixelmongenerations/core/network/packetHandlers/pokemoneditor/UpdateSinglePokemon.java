/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pokemoneditor;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.core.network.PixelmonData;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class UpdateSinglePokemon
implements IMessage {
    int slot;
    PixelmonData pokemon;

    public UpdateSinglePokemon() {
    }

    public UpdateSinglePokemon(int slot, PixelmonData pokemon) {
        this.slot = slot;
        this.pokemon = pokemon;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.slot);
        boolean hasPokemon = this.pokemon != null;
        buf.writeBoolean(hasPokemon);
        if (hasPokemon) {
            this.pokemon.encodeInto(buf);
        }
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.slot = buf.readInt();
        if (buf.readBoolean()) {
            this.pokemon = new PixelmonData(buf);
        }
    }

    public static class Handler
    implements IMessageHandler<UpdateSinglePokemon, IMessage> {
        @Override
        public IMessage onMessage(UpdateSinglePokemon message, MessageContext ctx) {
            ServerStorageDisplay.editedPokemon.set(message.slot, message.pokemon);
            return null;
        }
    }
}

