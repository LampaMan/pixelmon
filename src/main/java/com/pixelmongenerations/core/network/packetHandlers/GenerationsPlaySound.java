/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.client.SoundHelper;
import io.netty.buffer.ByteBuf;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class GenerationsPlaySound
implements IMessage {
    private String songName;

    public GenerationsPlaySound() {
    }

    public GenerationsPlaySound(String songName) {
        this.songName = songName;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.songName = ByteBufUtils.readUTF8String(buf);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, this.songName);
    }

    public static class Handler
    implements IMessageHandler<GenerationsPlaySound, IMessage> {
        @Override
        public IMessage onMessage(GenerationsPlaySound message, MessageContext ctx) {
            SoundHelper.playSound(new ResourceLocation("dynpixelmon:customsound/" + message.songName));
            return null;
        }
    }
}

