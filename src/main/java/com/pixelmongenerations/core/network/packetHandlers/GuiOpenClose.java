/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class GuiOpenClose
implements IMessage {
    private boolean open;

    public GuiOpenClose() {
    }

    public GuiOpenClose(boolean open) {
        this.open = open;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.open = buffer.readBoolean();
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeBoolean(this.open);
    }

    public static class Handler
    implements IMessageHandler<GuiOpenClose, IMessage> {
        @Override
        public IMessage onMessage(GuiOpenClose message, MessageContext ctx) {
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(ctx.getServerHandler().player);
            if (optstorage.isPresent()) {
                PlayerStorage storage = optstorage.get();
                storage.guiOpened = message.open;
            }
            return null;
        }
    }
}

