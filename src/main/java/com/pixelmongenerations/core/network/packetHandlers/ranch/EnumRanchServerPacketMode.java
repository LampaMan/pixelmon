/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.network.packetHandlers.ranch;

public enum EnumRanchServerPacketMode {
    AddPokemon,
    CollectEgg,
    RemovePokemon,
    ExtendRanch;


    public static EnumRanchServerPacketMode getFromOrdinal(int ordinal) {
        for (EnumRanchServerPacketMode p : EnumRanchServerPacketMode.values()) {
            if (p.ordinal() != ordinal) continue;
            return p;
        }
        return null;
    }
}

