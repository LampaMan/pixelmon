/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.client.gui.battles.battleScreens.choosePokemon.EnforcedSwitch;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class FailFleeSwitch
implements IMessage {
    @Override
    public void toBytes(ByteBuf buf) {
    }

    @Override
    public void fromBytes(ByteBuf buf) {
    }

    public static class Handler
    implements IMessageHandler<FailFleeSwitch, IMessage> {
        @Override
        public IMessage onMessage(FailFleeSwitch message, MessageContext ctx) {
            EnforcedSwitch.failFlee = true;
            ClientProxy.battleManager.mode = BattleMode.EnforcedSwitch;
            return null;
        }
    }
}

