/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pcClientStorage;

import com.pixelmongenerations.core.storage.PCClientStorage;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PCBoxBackground
implements IMessage {
    private int box;
    private String background;

    public PCBoxBackground() {
    }

    public PCBoxBackground(int box, String background) {
        this.box = box;
        this.background = background;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.box = buf.readInt();
        this.background = ByteBufUtils.readUTF8String(buf);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.box);
        ByteBufUtils.writeUTF8String(buf, this.background);
    }

    public static class Handler
    implements IMessageHandler<PCBoxBackground, IMessage> {
        @Override
        public IMessage onMessage(PCBoxBackground message, MessageContext ctx) {
            PCClientStorage.setBoxBackground(message.box, message.background);
            return null;
        }
    }
}

