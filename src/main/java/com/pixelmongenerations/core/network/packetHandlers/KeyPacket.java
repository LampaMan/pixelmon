/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.api.events.KeyEvent;
import com.pixelmongenerations.api.events.PixelmonSendOutEvent;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pokeballs.EntityOccupiedPokeball;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.battle.EnumBattleEndCause;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.packetHandlers.EnumPixelmonKeyType;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class KeyPacket
implements IMessage {
    int[] pokemonID;
    int entityID;
    int moveIndex;
    int x;
    int y;
    int z;
    int side;
    EnumPixelmonKeyType mode;
    static long lastThrownTime = -1L;

    public KeyPacket() {
    }

    public KeyPacket(int[] id) {
        this.pokemonID = id;
        this.mode = EnumPixelmonKeyType.SendPokemon;
    }

    public KeyPacket(int[] id, int entityId, EnumPixelmonKeyType mode) {
        this.pokemonID = id;
        this.entityID = entityId;
        this.mode = mode;
    }

    public KeyPacket(int[] id, int moveIndex, int entityId) {
        this.pokemonID = id;
        this.moveIndex = moveIndex;
        this.entityID = entityId;
        this.mode = EnumPixelmonKeyType.ExternalMoveEntity;
    }

    public KeyPacket(int[] id, int moveIndex, BlockPos pos, EnumFacing side) {
        this.pokemonID = id;
        this.moveIndex = moveIndex;
        this.x = pos.getX();
        this.y = pos.getY();
        this.z = pos.getZ();
        this.mode = EnumPixelmonKeyType.ExternalMoveBlock;
        this.side = side.getIndex();
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.mode = EnumPixelmonKeyType.getFromOrdinal(buffer.readByte());
        this.pokemonID = new int[]{buffer.readInt(), buffer.readInt()};
        switch (this.mode) {
            case ExternalMove: 
            case ActionKeyEntity: {
                this.entityID = buffer.readInt();
                break;
            }
            case ExternalMoveEntity: {
                this.entityID = buffer.readInt();
                this.moveIndex = buffer.readInt();
                break;
            }
            case ExternalMoveBlock: {
                this.moveIndex = buffer.readInt();
                this.x = buffer.readInt();
                this.y = buffer.readInt();
                this.z = buffer.readInt();
                this.side = buffer.readInt();
            }
        }
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeByte(this.mode.ordinal());
        buffer.writeInt(this.pokemonID[0]);
        buffer.writeInt(this.pokemonID[1]);
        switch (this.mode) {
            case ExternalMove: 
            case ActionKeyEntity: {
                buffer.writeInt(this.entityID);
                break;
            }
            case ExternalMoveEntity: {
                buffer.writeInt(this.entityID);
                buffer.writeInt(this.moveIndex);
                break;
            }
            case ExternalMoveBlock: {
                buffer.writeInt(this.moveIndex);
                buffer.writeInt(this.x);
                buffer.writeInt(this.y);
                buffer.writeInt(this.z);
                buffer.writeInt(this.side);
            }
        }
    }

    public static class Handler
    implements IMessageHandler<KeyPacket, IMessage> {
        @Override
        public IMessage onMessage(KeyPacket message, MessageContext ctx) {
            EnumPixelmonKeyType mode = message.mode;
            KeyEvent event = new KeyEvent(ctx.getServerHandler().player, mode);
            if (MinecraftForge.EVENT_BUS.post(event)) {
                return null;
            }
            ctx.getServerHandler().player.getServer().addScheduledTask(() -> {
                if (mode == EnumPixelmonKeyType.SendPokemon) {
                    this.sendPokemon(message, ctx);
                    return;
                }
                World world = ctx.getServerHandler().player.world;
                Optional<EntityPixelmon> pixelmonOptional = this.getPokemon(world, message.pokemonID, ctx);
                if (pixelmonOptional.isPresent()) {
                    Optional<PlayerStorage> optstorage;
                    EntityPixelmon p = pixelmonOptional.get();
                    if (mode == EnumPixelmonKeyType.ExternalMove && (optstorage = p.getStorage()).isPresent()) {
                        PlayerStorage storage = optstorage.get();
                        storage.moves.get(message.pokemonID, message.entityID).execute(ctx.getServerHandler().player, p, null);
                    }
                    if (mode.isEntity()) {
                        EntityLivingBase entity = (EntityLivingBase)world.getEntityByID(message.entityID);
                        if (mode == EnumPixelmonKeyType.ActionKeyEntity) {
                            p.setAttackTarget(entity);
                        } else {
                            p.setAttackTarget(entity, message.moveIndex);
                        }
                    } else {
                        p.setBlockTarget(message.x, message.y, message.z, EnumFacing.values()[message.side], message.moveIndex);
                    }
                    if (mode.isAction()) {
                        p.update(EnumUpdateType.Target);
                    }
                }
            });
            return null;
        }

        private Optional<EntityPixelmon> getPokemon(World world, int[] pokemonId, MessageContext ctx) {
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(ctx.getServerHandler().player);
            if (optstorage.isPresent()) {
                PlayerStorage storage = optstorage.get();
                return storage.getAlreadyExists(pokemonId, world);
            }
            return Optional.empty();
        }

        private void sendPokemon(KeyPacket message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            int[] pokemonID = message.pokemonID;
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
            if (optstorage.isPresent()) {
                PlayerStorage storage = optstorage.get();
                NBTTagCompound nbt = storage.getNBT(pokemonID);
                if (nbt == null || nbt.getBoolean("isEgg")) {
                    return;
                }
                PixelmonSendOutEvent sendOutEvent = new PixelmonSendOutEvent(player, nbt);
                if (MinecraftForge.EVENT_BUS.post(sendOutEvent)) {
                    return;
                }
                MinecraftServer server = ctx.getServerHandler().player.getServer();
                server.addScheduledTask(() -> {
                    String nickname;
                    String string = nickname = !PixelmonConfig.allowNicknames || nbt.getString("Nickname").isEmpty() ? Entity1Base.getLocalizedName(nbt.getString("Name").toLowerCase()) : nbt.getString("Nickname");
                    if (storage.isFainted(pokemonID)) {
                        ChatHandler.sendChat(player, "sendpixelmon.cantbattle", new TextComponentTranslation(nickname, new Object[0]));
                    } else if (storage.isInWorld(pokemonID)) {
                        Optional<EntityPixelmon> pixelmonOptional = storage.getAlreadyExists(pokemonID, player.world);
                        if (!pixelmonOptional.isPresent()) {
                            storage.setInWorld(pokemonID, false);
                            return;
                        }
                        EntityPixelmon pixelmon = pixelmonOptional.get();
                        if (pixelmon.battleController != null) {
                            pixelmon.battleController.endBattle(EnumBattleEndCause.FORCE);
                            BattleRegistry.deRegisterBattle(BattleRegistry.getBattle(player));
                            return;
                        }
                        BattleRegistry.deRegisterBattle(BattleRegistry.getBattle(player));
                        if (pixelmon.getControllingPassenger() == player) {
                            player.dismountRidingEntity();
                        }
                        if (pixelmon.getOwner() == null) {
                            pixelmon.unloadEntity();
                        } else if (pixelmon.getOwner() == player) {
                            pixelmon.catchInPokeball();
                            ChatHandler.sendChat(player, "sendpixelmon.retrieved", new TextComponentTranslation(nickname, new Object[0]));
                        }
                    } else {
                        long worldTime = player.world.getWorldTime();
                        if (lastThrownTime - worldTime < 300L && storage.thrownPokeball != null && !storage.thrownPokeball.isDead) {
                            return;
                        }
                        EnumPokeball caughtBall = EnumPokeball.getFromIndex(nbt.getInteger("CaughtBall"));
                        storage.thrownPokeball = new EntityOccupiedPokeball(player.world, player, pokemonID, caughtBall);
                        if (player.world.spawnEntity(storage.thrownPokeball)) {
                            player.world.playSound(null, player.posX, player.posY, player.posZ, SoundEvents.ENTITY_ARROW_SHOOT, SoundCategory.NEUTRAL, 0.5f, 0.4f / (player.world.rand.nextFloat() * 0.4f + 0.8f));
                            ChatHandler.sendChat(player, "sendpixelmon.sentout", new TextComponentTranslation(nickname, new Object[0]));
                            lastThrownTime = worldTime;
                        } else {
                            storage.thrownPokeball = null;
                        }
                    }
                });
            }
        }
    }
}

