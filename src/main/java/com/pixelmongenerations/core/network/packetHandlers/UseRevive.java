/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.common.entity.pixelmon.stats.links.NBTLink;
import com.pixelmongenerations.common.item.ItemRevive;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class UseRevive
implements IMessage {
    int[] id;

    public UseRevive() {
    }

    public UseRevive(int[] id) {
        this.id = id;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        for (int i : this.id) {
            buffer.writeInt(i);
        }
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.id = new int[]{buffer.readInt(), buffer.readInt()};
    }

    public static class Handler
    implements IMessageHandler<UseRevive, IMessage> {
        @Override
        public IMessage onMessage(UseRevive message, MessageContext ctx) {
            Item item;
            EntityPlayerMP player = ctx.getServerHandler().player;
            ItemStack itemStack = player.getHeldItemMainhand();
            if (itemStack != null && (item = itemStack.getItem()) instanceof ItemRevive) {
                PlayerStorage storage;
                NBTTagCompound targetNBT;
                ItemRevive revive = (ItemRevive)item;
                Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
                if (optstorage.isPresent() && (targetNBT = (storage = optstorage.get()).getNBT(message.id)) != null && revive.useMedicine(new NBTLink(targetNBT, storage), 0) && !player.capabilities.isCreativeMode) {
                    player.inventory.clearMatchingItems(item, itemStack.getMetadata(), 1, itemStack.getTagCompound());
                }
            }
            return null;
        }
    }
}

