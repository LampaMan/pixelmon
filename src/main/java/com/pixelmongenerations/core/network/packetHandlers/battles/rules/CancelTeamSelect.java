/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles.rules;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.battles.rules.GuiBattleRulesPlayer;
import com.pixelmongenerations.client.gui.battles.rules.GuiTeamSelect;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class CancelTeamSelect
implements IMessage {
    @Override
    public void toBytes(ByteBuf buf) {
    }

    @Override
    public void fromBytes(ByteBuf buf) {
    }

    public static class Handler
    implements IMessageHandler<CancelTeamSelect, IMessage> {
        @Override
        public IMessage onMessage(CancelTeamSelect message, MessageContext ctx) {
            GuiScreen screen = Minecraft.getMinecraft().currentScreen;
            if (screen instanceof GuiTeamSelect || screen instanceof GuiBattleRulesPlayer) {
                GuiHelper.closeScreen();
            }
            return null;
        }
    }
}

