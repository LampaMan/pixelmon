/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.core.Pixelmon;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ResponseSpawnInfo
implements IMessage {
    public String biomes;
    public String times;

    public ResponseSpawnInfo() {
    }

    public ResponseSpawnInfo(String biomes, String times) {
        this.biomes = biomes;
        this.times = times;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, this.biomes);
        ByteBufUtils.writeUTF8String(buf, this.times);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.biomes = ByteBufUtils.readUTF8String(buf);
        this.times = ByteBufUtils.readUTF8String(buf);
    }

    public static class Handler
    implements IMessageHandler<ResponseSpawnInfo, IMessage> {
        @Override
        public IMessage onMessage(ResponseSpawnInfo message, MessageContext ctx) {
            Pixelmon.PROXY.setPokedexSpawns(message.biomes, message.times);
            return null;
        }
    }
}

