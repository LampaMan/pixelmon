/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.statueEditor;

import com.pixelmongenerations.client.gui.statueEditor.GuiStatueEditor;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class StatuePacketClient
implements IMessage {
    int[] id;

    public StatuePacketClient() {
    }

    public StatuePacketClient(int[] id) {
        this.id = id;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.id = new int[]{buf.readInt(), buf.readInt()};
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.id[0]);
        buf.writeInt(this.id[1]);
    }

    public static class Handler
    implements IMessageHandler<StatuePacketClient, IMessage> {
        @Override
        public IMessage onMessage(StatuePacketClient message, MessageContext ctx) {
            GuiStatueEditor.statueId = message.id;
            return null;
        }
    }
}

