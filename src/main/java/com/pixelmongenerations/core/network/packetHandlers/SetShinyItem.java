/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.core.enums.EnumShinyItem;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SetShinyItem
implements IMessage {
    EnumShinyItem shinyItem;

    public SetShinyItem() {
    }

    public SetShinyItem(EnumShinyItem shinyItem) {
        this.shinyItem = shinyItem;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.shinyItem = EnumShinyItem.values()[buffer.readInt()];
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.shinyItem.ordinal());
    }

    public static class Handler
    implements IMessageHandler<SetShinyItem, IMessage> {
        @Override
        public IMessage onMessage(SetShinyItem message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
            if (storageOpt.isPresent()) {
                if (storageOpt.get().shinyData.canEquipShinyCharm()) {
                    storageOpt.get().shinyData.setShinyItem(message.shinyItem, false);
                } else {
                    storageOpt.get().shinyData.setShinyItem(EnumShinyItem.Disabled, false);
                }
            }
            return null;
        }
    }
}

