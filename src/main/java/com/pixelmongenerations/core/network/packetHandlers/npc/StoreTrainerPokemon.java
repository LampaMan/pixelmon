/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.npc;

import com.pixelmongenerations.client.gui.npcEditor.GuiTrainerEditor;
import com.pixelmongenerations.core.network.PixelmonData;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class StoreTrainerPokemon
implements IMessage {
    PixelmonData data;

    public StoreTrainerPokemon() {
    }

    public StoreTrainerPokemon(PixelmonData data) {
        this.data = data;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        this.data.encodeInto(buffer);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.data = new PixelmonData();
        this.data.decodeInto(buffer);
    }

    public static class Handler
    implements IMessageHandler<StoreTrainerPokemon, IMessage> {
        @Override
        public IMessage onMessage(StoreTrainerPokemon message, MessageContext ctx) {
            GuiTrainerEditor.pokemonList.add(message.data);
            return null;
        }
    }
}

