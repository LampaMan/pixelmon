/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.api.events.PixelmonTradeEvent;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCTrader;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types.TradeEvolution;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class AcceptNPCTradePacket
implements IMessage {
    private int offerSlot;
    private int traderId;

    public AcceptNPCTradePacket() {
    }

    public AcceptNPCTradePacket(int offerSlot, int traderId) {
        this.offerSlot = offerSlot;
        this.traderId = traderId;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.offerSlot);
        buffer.writeInt(this.traderId);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.offerSlot = buffer.readInt();
        this.traderId = buffer.readInt();
    }

    public static class Handler
    implements IMessageHandler<AcceptNPCTradePacket, IMessage> {
        @Override
        public IMessage onMessage(AcceptNPCTradePacket message, MessageContext ctx) {
            if (message.offerSlot >= 0 && message.offerSlot <= 5) {
                FMLCommonHandler.instance().getMinecraftServerInstance().addScheduledTask(() -> {
                    EntityPlayerMP player = ctx.getServerHandler().player;
                    PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorage(player).orElse(null);
                    NPCTrader trader = EntityNPC.locateNPCServer(player.world, message.traderId, NPCTrader.class).orElse(null);
                    if (storage == null || trader == null) {
                        return;
                    }
                    EntityPixelmon playerOffer = storage.getPokemon(storage.getIDFromPosition(message.offerSlot), player.world);
                    if (playerOffer == null || !trader.getExchange().equals(playerOffer.getPokemonName())) {
                        return;
                    }
                    EntityPixelmon traderOffer = (EntityPixelmon)PixelmonEntityList.createEntityByName(trader.getOffer(), player.world);
                    traderOffer.setShiny(trader.getIsShiny());
                    traderOffer.getLvl().setLevel(trader.getLevel());
                    traderOffer.setForm(trader.getForm());
                    if (MinecraftForge.EVENT_BUS.post(new PixelmonTradeEvent.NPCTradeEvent(player, playerOffer.writeToNBT(new NBTTagCompound()), trader, traderOffer.writeToNBT(new NBTTagCompound())))) {
                        player.closeScreen();
                        return;
                    }
                    storage.changePokemon(message.offerSlot, null);
                    storage.addToParty(traderOffer, message.offerSlot);
                    if (!PixelmonConfig.reuseTraders) {
                        trader.setDead();
                    }
                    player.closeScreen();
                    traderOffer.getEvolutions(TradeEvolution.class).forEach(evo -> {
                        if (evo.canEvolve(traderOffer, playerOffer.getSpecies())) {
                            evo.doEvolution(traderOffer);
                        }
                    });
                });
            }
            return null;
        }
    }
}

