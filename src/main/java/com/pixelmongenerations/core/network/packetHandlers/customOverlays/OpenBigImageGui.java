/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.customOverlays;

import com.pixelmongenerations.core.Pixelmon;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class OpenBigImageGui
implements IMessage {
    public String texture;
    public int width;
    public int height;

    public OpenBigImageGui(String texture, int width, int height) {
        this.texture = texture;
        this.width = width;
        this.height = height;
    }

    public OpenBigImageGui() {
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.texture = ByteBufUtils.readUTF8String(buf);
        this.width = buf.readInt();
        this.height = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, this.texture);
        buf.writeInt(this.width);
        buf.writeInt(this.height);
    }

    public static class Handler
    implements IMessageHandler<OpenBigImageGui, IMessage> {
        @Override
        public IMessage onMessage(OpenBigImageGui message, MessageContext ctx) {
            Pixelmon.PROXY.openBigImage(message);
            return null;
        }
    }
}

