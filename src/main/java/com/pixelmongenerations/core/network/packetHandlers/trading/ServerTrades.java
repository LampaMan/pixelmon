/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.trading;

import com.pixelmongenerations.common.block.tileEntities.TileEntityTradeMachine;
import com.pixelmongenerations.core.network.packetHandlers.trading.EnumServerTradesMode;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ServerTrades
implements IMessage {
    EnumServerTradesMode mode;
    int pos = 0;
    boolean ready = false;
    BlockPos tradeMachine;

    public ServerTrades() {
    }

    public ServerTrades(EnumServerTradesMode mode, BlockPos tradeMachine) {
        this.mode = mode;
        this.tradeMachine = tradeMachine;
    }

    public static ServerTrades getTradePacket(BlockPos tradeMachine) {
        return new ServerTrades(EnumServerTradesMode.Trade, tradeMachine);
    }

    public static ServerTrades getDeRegisterPacket(BlockPos tradeMachine) {
        return new ServerTrades(EnumServerTradesMode.DeRegisterTrader, tradeMachine);
    }

    public ServerTrades(int pos, BlockPos tradeMachine) {
        this(EnumServerTradesMode.SelectPokemon, tradeMachine);
        this.pos = pos;
    }

    public ServerTrades(boolean ready, BlockPos tradeMachine) {
        this(EnumServerTradesMode.Ready, tradeMachine);
        this.ready = ready;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeShort(this.mode.ordinal());
        buffer.writeInt(this.pos);
        buffer.writeBoolean(this.ready);
        buffer.writeLong(this.tradeMachine.toLong());
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        short ind = buffer.readShort();
        for (EnumServerTradesMode m : EnumServerTradesMode.values()) {
            if (m.ordinal() != ind) continue;
            this.mode = m;
        }
        this.pos = buffer.readInt();
        this.ready = buffer.readBoolean();
        this.tradeMachine = BlockPos.fromLong(buffer.readLong());
    }

    public static class Handler
    implements IMessageHandler<ServerTrades, IMessage> {
        @Override
        public IMessage onMessage(ServerTrades message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            BlockPos p = message.tradeMachine;
            if (player.getDistanceSq((double)p.getX() + 0.5, (double)p.getY() + 0.5, (double)p.getZ() + 0.5) <= 64.0) {
                player.getServerWorld().addScheduledTask(() -> {
                    TileEntity te = player.getServerWorld().getTileEntity(p);
                    if (te != null && te instanceof TileEntityTradeMachine) {
                        TileEntityTradeMachine tradeMachine = (TileEntityTradeMachine)te;
                        if (message.mode == EnumServerTradesMode.SelectPokemon) {
                            if (tradeMachine.player1 == player) {
                                tradeMachine.setPos1(message.pos);
                            } else if (tradeMachine.player2 == player) {
                                tradeMachine.setPos2(message.pos);
                            }
                        } else if (message.mode == EnumServerTradesMode.DeRegisterTrader) {
                            tradeMachine.removePlayer(player);
                        } else if (message.mode == EnumServerTradesMode.Ready) {
                            tradeMachine.ready(player, message.ready);
                        } else if (message.mode == EnumServerTradesMode.Trade) {
                            tradeMachine.trade();
                        }
                    }
                });
            }
            return null;
        }
    }
}

