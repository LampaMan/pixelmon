/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.battles.ShowSpectateMessage;
import io.netty.buffer.ByteBuf;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class CheckPlayerBattle
implements IMessage {
    UUID uuid;

    public CheckPlayerBattle() {
    }

    public CheckPlayerBattle(EntityPlayer player) {
        this.uuid = player.getUniqueID();
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.uuid = new UUID(buffer.readLong(), buffer.readLong());
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeLong(this.uuid.getMostSignificantBits());
        buffer.writeLong(this.uuid.getLeastSignificantBits());
    }

    public static class Handler
    implements IMessageHandler<CheckPlayerBattle, IMessage> {
        @Override
        public IMessage onMessage(CheckPlayerBattle message, MessageContext ctx) {
            EntityPlayer player = ctx.getServerHandler().player.world.getPlayerEntityByUUID(message.uuid);
            if (player != null && BattleRegistry.getBattleExcludeSpectate(player) != null) {
                this.showSpectateMessage(ctx.getServerHandler().player, message.uuid);
            }
            return null;
        }

        private void showSpectateMessage(EntityPlayerMP player, UUID uuid) {
            ShowSpectateMessage message = new ShowSpectateMessage(uuid);
            Pixelmon.NETWORK.sendTo(message, player);
        }
    }
}

