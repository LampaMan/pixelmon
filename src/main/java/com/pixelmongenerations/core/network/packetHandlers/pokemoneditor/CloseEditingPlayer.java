/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pokemoneditor;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiImportExport;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiPokemonEditorAdvanced;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiPokemonEditorIndividual;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiPokemonEditorParty;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class CloseEditingPlayer
implements IMessage {
    @Override
    public void toBytes(ByteBuf buf) {
    }

    @Override
    public void fromBytes(ByteBuf buf) {
    }

    public static class Handler
    implements IMessageHandler<CloseEditingPlayer, IMessage> {
        @Override
        public IMessage onMessage(CloseEditingPlayer message, MessageContext ctx) {
            GuiScreen currentScreen = Minecraft.getMinecraft().currentScreen;
            if (currentScreen instanceof GuiPokemonEditorParty || currentScreen instanceof GuiPokemonEditorIndividual || currentScreen instanceof GuiPokemonEditorAdvanced || currentScreen instanceof GuiImportExport) {
                GuiHelper.closeScreen();
            }
            return null;
        }
    }
}

