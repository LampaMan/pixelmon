/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.dialogue;

import com.pixelmongenerations.api.dialogue.Choice;
import com.pixelmongenerations.api.dialogue.Dialogue;
import com.pixelmongenerations.api.events.dialogue.DialogueChoiceEvent;
import com.pixelmongenerations.core.network.packetHandlers.dialogue.DialogueNextAction;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.UUID;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class DialogueChoiceMade
implements IMessage {
    private Choice choice;

    public DialogueChoiceMade() {
    }

    public DialogueChoiceMade(Choice choice) {
        this.choice = choice;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.choice = new Choice(buffer);
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        this.choice.toBytes(buffer);
    }

    public static class Handler
    implements IMessageHandler<DialogueChoiceMade, DialogueNextAction> {
        @Override
        public DialogueNextAction onMessage(DialogueChoiceMade message, MessageContext ctx) {
            DialogueChoiceEvent event = new DialogueChoiceEvent(ctx.getServerHandler().player, message.choice);
            try {
                UUID uuid = event.getPlayer().getUniqueID();
                if (Choice.handleMap.containsKey(uuid) && Choice.handleMap.get(uuid).get(event.getChoice().choiceID) != null) {
                    Choice.handleMap.get(uuid).get(event.getChoice().choiceID).accept(event);
                }
                if (!event.isCanceled()) {
                    MinecraftForge.EVENT_BUS.post(event);
                }
                Choice.handleMap.keySet().removeIf(uuidKey -> ctx.getServerHandler().player.getServer().getPlayerList().getPlayerByUUID((UUID)uuidKey) == null);
            }
            catch (Exception e) {
                e.printStackTrace();
                event.setAction(DialogueNextAction.DialogueGuiAction.CLOSE);
                event.setNewDialogues(new ArrayList<Dialogue>());
            }
            return new DialogueNextAction(event.getAction(), event.getNewDialogues());
        }
    }
}

