/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.clientStorage;

import com.pixelmongenerations.client.ServerStorageDisplay;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class Remove
implements IMessage {
    int[] pokemonId;

    public Remove() {
    }

    public Remove(int[] pokemonId) {
        this.pokemonId = pokemonId;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.pokemonId[0]);
        buffer.writeInt(this.pokemonId[1]);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.pokemonId = new int[]{buffer.readInt(), buffer.readInt()};
    }

    public static class Handler
    implements IMessageHandler<Remove, IMessage> {
        @Override
        public IMessage onMessage(Remove message, MessageContext ctx) {
            ServerStorageDisplay.remove(message.pokemonId);
            return null;
        }
    }
}

