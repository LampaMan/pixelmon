/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pokemoneditor;

import com.pixelmongenerations.client.gui.pokemoneditor.GuiPokemonEditorParty;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.ItemPokemonEditor;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.ChangePokemonOpenGUI;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ChangePokemon
implements IMessage {
    UUID playerID;
    int slot;
    EnumSpecies newPokemon;

    public ChangePokemon() {
    }

    public ChangePokemon(int slot, EnumSpecies newPokemon) {
        this.playerID = GuiPokemonEditorParty.editedPlayerUUID;
        this.slot = slot;
        this.newPokemon = newPokemon;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        PixelmonMethods.toBytesUUID(buf, this.playerID);
        buf.writeInt(this.slot);
        buf.writeInt(this.newPokemon.ordinal());
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.playerID = PixelmonMethods.fromBytesUUID(buf);
        this.slot = buf.readInt();
        this.newPokemon = EnumSpecies.getFromOrdinal(buf.readInt());
    }

    public static class Handler
    implements IMessageHandler<ChangePokemon, IMessage> {
        @Override
        public IMessage onMessage(ChangePokemon message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            ctx.getServerHandler().player.getServer().addScheduledTask(() -> {
                Optional<PlayerStorage> optstorage;
                if (ItemPokemonEditor.checkPermission(player) && (optstorage = PixelmonStorage.pokeBallManager.getPlayerStorageFromUUID(player.getServer(), message.playerID)).isPresent()) {
                    PlayerStorage storage = optstorage.get();
                    EntityPixelmon pokemon = (EntityPixelmon)PixelmonEntityList.createEntityByName(message.newPokemon.name, player.getEntityWorld());
                    pokemon.friendship.initFromCapture();
                    if (message.slot == -1) {
                        storage.addToParty(pokemon);
                    } else {
                        storage.addToParty(pokemon, message.slot);
                    }
                    EntityPlayerMP editingPlayer = ctx.getServerHandler().player;
                    ItemPokemonEditor.updateSinglePokemon(editingPlayer, message.playerID, message.slot);
                    Pixelmon.NETWORK.sendTo(new ChangePokemonOpenGUI(message.slot), editingPlayer);
                }
            });
            return null;
        }
    }
}

