/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pokemoneditor;

import com.pixelmongenerations.common.item.ItemPokemonEditor;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.DeletePokemon;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.UpdateEditedParty;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.UpdateEditedPokemon;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.UpdatePlayerPokemon;
import io.netty.buffer.ByteBuf;
import java.util.List;
import java.util.UUID;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class UpdatePlayerParty
extends UpdateEditedParty {
    UUID playerID;

    public UpdatePlayerParty() {
    }

    public UpdatePlayerParty(List<PixelmonData> party) {
        super(party);
    }

    @Override
    protected UpdateEditedPokemon createPokemonPacket(PixelmonData data) {
        return new UpdatePlayerPokemon(data);
    }

    @Override
    protected UpdateEditedPokemon readPokemonData(ByteBuf buf) {
        UpdatePlayerPokemon data = new UpdatePlayerPokemon();
        data.fromBytes(buf);
        this.playerID = data.playerID;
        return data;
    }

    public static class Handler
    implements IMessageHandler<UpdatePlayerParty, IMessage> {
        @Override
        public IMessage onMessage(UpdatePlayerParty message, MessageContext ctx) {
            if (!ItemPokemonEditor.checkPermission(ctx.getServerHandler().player)) {
                return null;
            }
            UpdatePlayerPokemon.Handler handler = new UpdatePlayerPokemon.Handler();
            for (int i = 0; i < message.party.size(); ++i) {
                UpdatePlayerPokemon data = (UpdatePlayerPokemon)message.party.get(i);
                if (data == null) {
                    DeletePokemon.deletePokemon(message.playerID, i, null);
                    continue;
                }
                handler.onMessage(data, ctx);
            }
            return null;
        }
    }
}

