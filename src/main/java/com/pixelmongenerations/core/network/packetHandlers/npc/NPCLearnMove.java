/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.npc;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.enums.EnumNPCType;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class NPCLearnMove
implements IMessage {
    int slot;
    EnumNPCType npcType;

    public NPCLearnMove() {
    }

    public NPCLearnMove(int slot, EnumNPCType npcType) {
        this.slot = slot;
        this.npcType = npcType;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.slot = buf.readInt();
        this.npcType = EnumNPCType.getFromOrdinal(buf.readInt());
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.slot);
        buf.writeInt(this.npcType.ordinal());
    }

    public static class Handler
    implements IMessageHandler<NPCLearnMove, IMessage> {
        @Override
        public IMessage onMessage(NPCLearnMove message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
            if (optstorage.isPresent()) {
                PlayerStorage storage = optstorage.get();
                if (storage.partyPokemon[message.slot] != null) {
                    int[] id = PixelmonMethods.getID(storage.partyPokemon[message.slot]);
                    if (message.npcType == EnumNPCType.Relearner) {
                        player.openGui(Pixelmon.INSTANCE, EnumGui.Relearner.getIndex(), player.world, id[0], id[1], 0);
                    } else if (message.npcType == EnumNPCType.Tutor) {
                        player.openGui(Pixelmon.INSTANCE, EnumGui.Tutor.getIndex(), player.world, id[0], id[1], 0);
                    }
                }
            }
            return null;
        }
    }
}

