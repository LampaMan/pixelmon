/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.storage.playerData.ExternalMoveData;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ExternalMove
implements IMessage {
    int moveIndex;
    long timeUsed;
    int[] pokemonId;

    public ExternalMove() {
    }

    public ExternalMove(int[] pokemonId, int moveIndex, long timeLastUsed) {
        this.pokemonId = pokemonId;
        this.moveIndex = moveIndex;
        this.timeUsed = timeLastUsed;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.pokemonId = new int[]{buf.readInt(), buf.readInt()};
        this.moveIndex = buf.readInt();
        this.timeUsed = buf.readLong();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.pokemonId[0]);
        buf.writeInt(this.pokemonId[1]);
        buf.writeInt(this.moveIndex);
        buf.writeLong(this.timeUsed);
    }

    public static class Handler
    implements IMessageHandler<ExternalMove, IMessage> {
        @Override
        public IMessage onMessage(ExternalMove message, MessageContext ctx) {
            PixelmonData poke = ServerStorageDisplay.get(message.pokemonId);
            for (ExternalMoveData d : poke.getExternalMoves()) {
                if (d.moveIndex != message.moveIndex) continue;
                d.timeLastUsed = message.timeUsed;
            }
            return null;
        }
    }
}

