/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.ranch;

import com.pixelmongenerations.common.block.tileEntities.TileEntityRanchBlock;
import com.pixelmongenerations.common.item.ItemRanchUpgrade;
import com.pixelmongenerations.core.network.packetHandlers.ranch.EnumRanchServerPacketMode;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class RanchBlockServerPacket
implements IMessage {
    EnumRanchServerPacketMode mode;
    int x;
    int y;
    int z;
    int[] id;
    boolean[] extendDirection;

    public RanchBlockServerPacket() {
    }

    public RanchBlockServerPacket(int x, int y, int z, EnumRanchServerPacketMode mode) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.mode = mode;
    }

    public RanchBlockServerPacket(int x, int y, int z, int[] id, EnumRanchServerPacketMode mode) {
        this(x, y, z, mode);
        this.id = id;
    }

    public RanchBlockServerPacket(int x, int y, int z, EnumRanchServerPacketMode mode, boolean[] extendDirection) {
        this(x, y, z, mode);
        this.extendDirection = extendDirection;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
        this.mode = EnumRanchServerPacketMode.getFromOrdinal(buf.readShort());
        switch (this.mode) {
            case RemovePokemon: 
            case AddPokemon: {
                this.id = new int[]{buf.readInt(), buf.readInt()};
                break;
            }
            case ExtendRanch: {
                this.extendDirection = new boolean[]{buf.readBoolean(), buf.readBoolean(), buf.readBoolean(), buf.readBoolean()};
            }
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.x);
        buf.writeInt(this.y);
        buf.writeInt(this.z);
        buf.writeShort(this.mode.ordinal());
        switch (this.mode) {
            case RemovePokemon: 
            case AddPokemon: {
                buf.writeInt(this.id[0]);
                buf.writeInt(this.id[1]);
                break;
            }
            case ExtendRanch: {
                buf.writeBoolean(this.extendDirection[0]);
                buf.writeBoolean(this.extendDirection[1]);
                buf.writeBoolean(this.extendDirection[2]);
                buf.writeBoolean(this.extendDirection[3]);
            }
        }
    }

    public static class Handler
    implements IMessageHandler<RanchBlockServerPacket, IMessage> {
        @Override
        public IMessage onMessage(RanchBlockServerPacket message, MessageContext ctx) {
            ctx.getServerHandler().player.getServer().addScheduledTask(() -> {
                EntityPlayerMP player = ctx.getServerHandler().player;
                TileEntityRanchBlock rb = (TileEntityRanchBlock)player.world.getTileEntity(new BlockPos(message.x, message.y, message.z));
                if (rb == null) {
                    return;
                }
                block0 : switch (message.mode) {
                    case AddPokemon: {
                        rb.addPokemon(player, message.id);
                        break;
                    }
                    case RemovePokemon: {
                        rb.removePokemon(player, message.id);
                        break;
                    }
                    case CollectEgg: {
                        rb.claimEgg(player);
                        break;
                    }
                    case ExtendRanch: {
                        rb.getBounds().Extend(player, message.extendDirection[0] ? 1 : 0, message.extendDirection[1] ? 1 : 0, message.extendDirection[2] ? 1 : 0, message.extendDirection[3] ? 1 : 0);
                        if (player.capabilities.isCreativeMode) break;
                        if (player.getHeldItemMainhand().getItem() instanceof ItemRanchUpgrade) {
                            player.inventory.clearMatchingItems(player.getHeldItemMainhand().getItem(), player.getHeldItemMainhand().getMetadata(), 1, player.getHeldItemMainhand().getTagCompound());
                            break;
                        }
                        for (ItemStack s : player.inventory.mainInventory) {
                            if (!(s.getItem() instanceof ItemRanchUpgrade)) continue;
                            player.inventory.clearMatchingItems(s.getItem(), s.getMetadata(), 1, s.getTagCompound());
                            break block0;
                        }
                        break;
                    }
                }
            });
            return null;
        }
    }
}

