/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles.rules;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class DisplayBattleQueryRules
implements IMessage {
    int queryIndex;
    boolean isProposing;

    public DisplayBattleQueryRules() {
    }

    public DisplayBattleQueryRules(int queryIndex, boolean isProposing) {
        this.queryIndex = queryIndex;
        this.isProposing = isProposing;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.queryIndex);
        buf.writeBoolean(this.isProposing);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.queryIndex = buf.readInt();
        this.isProposing = buf.readBoolean();
    }

    public static class Handler
    implements IMessageHandler<DisplayBattleQueryRules, IMessage> {
        @Override
        public IMessage onMessage(DisplayBattleQueryRules message, MessageContext ctx) {
            Minecraft mc = Minecraft.getMinecraft();
            mc.addScheduledTask(() -> mc.player.openGui(Pixelmon.INSTANCE, EnumGui.BattleRulesPlayer.getIndex(), mc.player.world, message.queryIndex, message.isProposing ? 1 : 0, 0));
            return null;
        }
    }
}

