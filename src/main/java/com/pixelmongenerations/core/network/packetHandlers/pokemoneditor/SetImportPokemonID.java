/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pokemoneditor;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiPartyEditorBase;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SetImportPokemonID
implements IMessage {
    int partySlot;
    int[] pokemonID;

    public SetImportPokemonID() {
    }

    public SetImportPokemonID(int partySlot, int[] pokemonID) {
        this.partySlot = partySlot;
        this.pokemonID = pokemonID;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.partySlot);
        PixelmonMethods.toBytesPokemonID(buf, this.pokemonID);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.partySlot = buf.readInt();
        this.pokemonID = PixelmonMethods.fromBytesPokemonID(buf);
    }

    public static class Handler
    implements IMessageHandler<SetImportPokemonID, IMessage> {
        @Override
        public IMessage onMessage(SetImportPokemonID message, MessageContext ctx) {
            PixelmonData pokemon = null;
            GuiScreen currentScreen = Minecraft.getMinecraft().currentScreen;
            List<PixelmonData> pokemonList = null;
            if (currentScreen instanceof GuiPartyEditorBase) {
                GuiPartyEditorBase partyEditor = (GuiPartyEditorBase)currentScreen;
                pokemonList = partyEditor.pokemonList;
            } else {
                pokemonList = ServerStorageDisplay.editedPokemon;
            }
            if (pokemonList != null && pokemonList.size() > message.partySlot) {
                pokemon = pokemonList.get(message.partySlot);
            }
            if (pokemon != null) {
                pokemon.pokemonID = message.pokemonID;
            }
            return null;
        }
    }
}

