/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.npc;

import com.pixelmongenerations.client.gui.npcEditor.GuiShopkeeperEditor;
import com.pixelmongenerations.common.entity.npcs.registry.ClientShopkeeperData;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.entity.npcs.registry.ShopkeeperData;
import com.pixelmongenerations.core.util.helper.ArrayHelper;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.List;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SetShopkeeperClient
implements IMessage {
    List<ClientShopkeeperData> data = new ArrayList<ClientShopkeeperData>();

    public SetShopkeeperClient() {
    }

    public SetShopkeeperClient(String language) {
        for (ShopkeeperData shopkeeper : ServerNPCRegistry.getEnglishShopkeepers()) {
            ArrayList<String> names = new ArrayList<String>();
            for (int i = 0; i < shopkeeper.countNames(); ++i) {
                names.add(ServerNPCRegistry.shopkeepers.getTranslatedName(language, shopkeeper.id, i));
            }
            this.data.add(new ClientShopkeeperData(shopkeeper.id, shopkeeper.getTextures(), names));
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ArrayHelper.encodeList(buf, this.data);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.data.clear();
        int listSize = buf.readInt();
        for (int i = 0; i < listSize; ++i) {
            this.data.add(new ClientShopkeeperData(buf));
        }
    }

    public static class Handler
    implements IMessageHandler<SetShopkeeperClient, IMessage> {
        @Override
        public IMessage onMessage(SetShopkeeperClient message, MessageContext ctx) {
            GuiShopkeeperEditor.shopkeeperData = message.data;
            return null;
        }
    }
}

