/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.network;

import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.Spectator;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.battles.BattleMessage;
import java.util.ArrayList;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;

public class ChatHandler {
    public static void sendChat(Entity entityLiving, String string, Object ... data) {
        ChatHandler.sendChat(entityLiving, ChatHandler.getMessage(string, data));
    }

    public static void sendChat(ICommandSender receiver, TextComponentTranslation message) {
        if (receiver != null && receiver instanceof EntityPlayerMP) {
            receiver.sendMessage(message);
        }
    }

    public static void sendChat(ICommandSender owner, ICommandSender owner2, String string, Object ... data) {
        TextComponentTranslation textComponentTranslation = ChatHandler.getMessage(string, data);
        ChatHandler.sendChat(owner, textComponentTranslation);
        ChatHandler.sendChat(owner2, textComponentTranslation);
    }

    public static void sendFormattedChat(ICommandSender receiver, TextFormatting chatFormat, String string, Object ... data) {
        TextComponentTranslation textComponentTranslation = new TextComponentTranslation(string, data);
        textComponentTranslation.getStyle().setColor(chatFormat);
        ChatHandler.sendChat(receiver, textComponentTranslation);
    }

    public static void sendBattleMessage(Entity user, String string, Object ... data) {
        TextComponentTranslation textComponentTranslation = new TextComponentTranslation(string, data);
        textComponentTranslation.getStyle().setColor(TextFormatting.GRAY);
        ChatHandler.sendBattleMessage(user, textComponentTranslation);
    }

    public static void sendBattleMessage(Entity user, TextComponentTranslation chat) {
        if (user != null && user instanceof EntityPlayerMP) {
            Pixelmon.NETWORK.sendTo(new BattleMessage(chat), (EntityPlayerMP)user);
        }
    }

    public static void sendBattleMessage(ArrayList<BattleParticipant> participants, String string, Object ... data) {
        ChatHandler.sendBattleMessage(participants, ChatHandler.getMessage(string, data));
    }

    public static void sendBattleMessage(ArrayList<BattleParticipant> participants, TextComponentTranslation message) {
        for (BattleParticipant p : participants) {
            ChatHandler.sendBattleMessage(p.getEntity(), message);
        }
        ArrayList<Spectator> spectators = participants.get((int)0).bc.spectators;
        spectators.forEach(spectator -> spectator.sendBattleMessage(message));
    }

    public static TextComponentTranslation getMessage(String string, Object ... data) {
        TextComponentTranslation message = new TextComponentTranslation(string, data);
        message.getStyle().setColor(TextFormatting.GRAY);
        return message;
    }

    public static void sendMessageToAllPlayers(MinecraftServer minecraftServer, String string) {
        TextComponentTranslation translation = new TextComponentTranslation(string, new Object[0]);
        minecraftServer.getPlayerList().sendMessage(translation);
    }
}

