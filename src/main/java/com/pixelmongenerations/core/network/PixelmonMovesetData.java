/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.enums.EnumType;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;

public class PixelmonMovesetData {
    public int attackIndex;
    public EnumType type;
    public int pp;
    public int ppBase;
    public boolean disabled = false;
    private Attack attack;

    public static PixelmonMovesetData create(Moveset moveset, int i) {
        if (moveset.size() <= i) {
            return null;
        }
        PixelmonMovesetData p = new PixelmonMovesetData();
        p.attackIndex = moveset.get((int)i).getAttackBase().attackIndex;
        p.pp = moveset.get((int)i).pp;
        p.ppBase = moveset.get((int)i).ppBase;
        p.type = moveset.get((int)i).getAttackBase().attackType;
        p.disabled = moveset.get(i).getDisabled();
        return p;
    }

    public static PixelmonMovesetData create(NBTTagCompound nbt, int i) {
        Attack a;
        if (nbt.getInteger("PixelmonNumberMoves") <= i) {
            return null;
        }
        PixelmonMovesetData p = new PixelmonMovesetData();
        if (!nbt.hasKey("PixelmonMoveID" + i)) {
            a = DatabaseMoves.getAttack(nbt.getString("PixelmonMoveName" + i));
            if (a == null) {
                return null;
            }
            p.attackIndex = a.getAttackBase().attackIndex;
        } else {
            p.attackIndex = nbt.getInteger("PixelmonMoveID" + i);
            a = new Attack(p.attackIndex);
        }
        if (a.getAttackBase() == null) {
            return null;
        }
        p.type = a.getAttackBase().attackType;
        p.pp = nbt.getInteger("PixelmonMovePP" + i);
        p.ppBase = nbt.getInteger("PixelmonMovePPBase" + i);
        return p;
    }

    public void writeData(ByteBuf data) {
        data.writeInt(this.attackIndex);
        data.writeShort(this.type.getIndex());
        data.writeShort(this.pp);
        data.writeShort(this.ppBase);
        data.writeBoolean(this.disabled);
    }

    public void readData(ByteBuf data) {
        this.attackIndex = data.readInt();
        this.type = EnumType.parseType(data.readShort());
        this.pp = data.readShort();
        this.ppBase = data.readShort();
        this.disabled = data.readBoolean();
    }

    public void updatePokemon(NBTTagCompound p, int i) {
        p.setInteger("PixelmonMoveID" + i, this.attackIndex);
        p.setInteger("PixelmonMovePP" + i, this.pp);
        p.setInteger("PixelmonMovePPBase" + i, this.ppBase);
    }

    public static PixelmonMovesetData createPacket(Attack a) {
        PixelmonMovesetData p = new PixelmonMovesetData();
        p.attackIndex = a.getAttackBase().attackIndex;
        p.pp = a.pp;
        p.ppBase = a.ppBase;
        p.type = a.getAttackBase().attackType;
        return p;
    }

    public Attack getAttack() {
        if (this.attack == null || this.attack.getAttackBase().attackIndex != this.attackIndex) {
            this.attack = DatabaseMoves.getAttack(this.attackIndex);
        }
        return this.attack;
    }
}

