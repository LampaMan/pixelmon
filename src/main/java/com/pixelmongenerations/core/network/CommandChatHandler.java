/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.network;

import net.minecraft.command.ICommandSender;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;

public class CommandChatHandler {
    public static void sendChat(ICommandSender commandSender, String string, Object ... data) {
        TextComponentTranslation chatTranslation = new TextComponentTranslation(string, data);
        chatTranslation.getStyle().setColor(TextFormatting.GRAY);
        commandSender.sendMessage(chatTranslation);
    }

    public static void sendFormattedChat(ICommandSender commandSender, TextFormatting format, String string, Object ... data) {
        TextComponentTranslation chatTranslation = new TextComponentTranslation(string, data);
        chatTranslation.getStyle().setColor(format);
        commandSender.sendMessage(chatTranslation);
    }
}

