/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.network;

public enum EnumUpdateType {
    HP,
    Nickname,
    Name,
    Stats,
    Friendship,
    Moveset,
    Status,
    CanLevel,
    HeldItem,
    Texture,
    Egg,
    Target,
    Ability,
    Clones,
    Enchants,
    Wormholes,
    AbundantActivations,
    Nature,
    PseudoNature,
    PokeRus,
    Mark;


    public static EnumUpdateType getType(int index) {
        for (EnumUpdateType type : EnumUpdateType.values()) {
            if (type.ordinal() != index) continue;
            return type;
        }
        return null;
    }
}

