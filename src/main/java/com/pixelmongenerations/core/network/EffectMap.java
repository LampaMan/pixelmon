/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.network;

import com.pixelmongenerations.common.battle.attacks.EffectBase;
import java.util.Arrays;
import java.util.HashMap;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class EffectMap
extends HashMap<String, Class<? extends EffectBase>> {
    @Override
    public Class<? extends EffectBase> put(String key, Class<? extends EffectBase> value) {
        if (this.containsKey(key) || this.containsValue(value)) {
            return null;
        }
        try {
            if (Arrays.stream(value.getConstructors()).noneMatch(constructor -> constructor.isVarArgs() || constructor.getParameterCount() == 0)) {
                throw new Exception();
            }
            return super.put(key, value);
        }
        catch (Exception e) {
            e.printStackTrace();
            FMLCommonHandler.instance().exitJava(0, false);
            return null;
        }
    }
}

