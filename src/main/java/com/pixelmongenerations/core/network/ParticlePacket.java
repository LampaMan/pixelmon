/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network;

import com.pixelmongenerations.core.Pixelmon;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ParticlePacket
implements IMessage {
    private int particleType;
    private double x;
    private double y;
    private double z;
    private double speedX;
    private double speedY;
    private double speedZ;
    private int num1;
    private int num2;
    private int num3;

    public ParticlePacket() {
    }

    public ParticlePacket(int particleType, double xCoord, double yCoord, double zCoord, double xSpeed, double ySpeed, double zSpeed, int num1, int num2, int num3) {
        this.particleType = particleType;
        this.x = xCoord;
        this.y = yCoord;
        this.z = zCoord;
        this.speedX = xSpeed;
        this.speedY = ySpeed;
        this.speedZ = zSpeed;
        this.num1 = num1;
        this.num2 = num2;
        this.num3 = num3;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.particleType = buf.readInt();
        this.x = buf.readDouble();
        this.y = buf.readDouble();
        this.z = buf.readDouble();
        this.speedX = buf.readDouble();
        this.speedY = buf.readDouble();
        this.speedZ = buf.readDouble();
        this.num1 = buf.readInt();
        this.num2 = buf.readInt();
        this.num3 = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.particleType);
        buf.writeDouble(this.x);
        buf.writeDouble(this.y);
        buf.writeDouble(this.z);
        buf.writeDouble(this.speedX);
        buf.writeDouble(this.speedY);
        buf.writeDouble(this.speedZ);
        buf.writeInt(this.num1);
        buf.writeInt(this.num2);
        buf.writeInt(this.num3);
    }

    public static class Handler
    implements IMessageHandler<ParticlePacket, IMessage> {
        @Override
        public IMessage onMessage(ParticlePacket message, MessageContext ctx) {
            Pixelmon.PROXY.spawnParticle(message.particleType, message.x, message.y, message.z, message.speedX, message.speedY, message.speedZ, message.num1, message.num2, message.num3);
            return null;
        }
    }
}

