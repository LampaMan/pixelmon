/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util;

import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class MoveCostListEntry {
    public final EntityPlayer player;
    public final List<ItemStack> items;

    public MoveCostListEntry(EntityPlayer player, List<ItemStack> items) {
        this.player = player;
        this.items = items;
    }
}

