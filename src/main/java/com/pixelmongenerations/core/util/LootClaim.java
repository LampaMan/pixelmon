/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.base.Preconditions
 */
package com.pixelmongenerations.core.util;

import com.google.common.base.Preconditions;
import java.util.UUID;

public class LootClaim {
    private final UUID playerID;
    private final long timeClaimed;

    public LootClaim(UUID playerID, long timeClaimed) {
        this.playerID = (UUID)Preconditions.checkNotNull((Object)playerID);
        this.timeClaimed = timeClaimed;
    }

    public UUID getPlayerID() {
        return this.playerID;
    }

    public long getTimeClaimed() {
        return this.timeClaimed;
    }
}

