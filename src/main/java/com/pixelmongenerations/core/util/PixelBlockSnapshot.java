/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableMap
 *  com.google.gson.JsonSyntaxException
 */
package com.pixelmongenerations.core.util;

import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonSyntaxException;
import com.pixelmongenerations.common.block.tileEntities.TileEntityGymSign;
import com.pixelmongenerations.core.Pixelmon;
import java.util.ConcurrentModificationException;
import java.util.Map;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLever;
import net.minecraft.block.BlockStandingSign;
import net.minecraft.block.BlockVine;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.registries.IForgeRegistry;

public class PixelBlockSnapshot {
    public static final String PIXEL_SNAPSHOT_BLOCK_NAME = "blockName";
    public final BlockPos pos;
    public transient IBlockState replacedBlock;
    private final NBTTagCompound nbt;
    public transient World world;
    public final ResourceLocation blockIdentifier;
    public final int meta;

    public static PixelBlockSnapshot readFromNBT(World newWorld, NBTTagCompound tag) {
        NBTTagCompound nbt = tag.getBoolean("hasTE") ? tag.getCompoundTag("tileEntity") : null;
        PixelBlockSnapshot snap = new PixelBlockSnapshot(new BlockPos(tag.getInteger("x"), tag.getInteger("y"), tag.getInteger("z")), tag.hasKey("blockMod") ? tag.getString("blockMod") : "minecraft", tag.getString(PIXEL_SNAPSHOT_BLOCK_NAME), tag.getInteger("metadata"), nbt);
        snap.world = newWorld;
        return snap;
    }

    public PixelBlockSnapshot(World world, BlockPos pos, IBlockState state) {
        this.world = world;
        this.pos = pos;
        this.replacedBlock = state;
        this.blockIdentifier = state.getBlock().getRegistryName();
        this.meta = state.getBlock().getMetaFromState(state);
        TileEntity te = world.getTileEntity(pos);
        if (te != null) {
            this.nbt = new NBTTagCompound();
            te.writeToNBT(this.nbt);
        } else {
            this.nbt = null;
        }
    }

    public PixelBlockSnapshot(World world, BlockPos pos, IBlockState state, NBTTagCompound nbt) {
        this.world = world;
        this.pos = pos;
        this.replacedBlock = state;
        this.blockIdentifier = state.getBlock().getRegistryName();
        this.meta = state.getBlock().getMetaFromState(state);
        this.nbt = nbt;
    }

    public PixelBlockSnapshot(BlockPos pos, String modid, String blockName, int meta, NBTTagCompound nbt) {
        this.pos = pos;
        this.blockIdentifier = new ResourceLocation(modid, blockName);
        this.meta = meta;
        this.nbt = nbt;
    }

    public PixelBlockSnapshot(BlockPos pos, IBlockState state, NBTTagCompound nbt) {
        this.pos = pos;
        this.blockIdentifier = state.getBlock().getRegistryName();
        this.meta = state.getBlock().getMetaFromState(state);
        this.nbt = nbt;
    }

    public static PixelBlockSnapshot getBlockSnapshot(World world, BlockPos pos) {
        return new PixelBlockSnapshot(world, pos, world.getBlockState(pos));
    }

    public static PixelBlockSnapshot readFromNBT(NBTTagCompound tag) {
        NBTTagCompound nbt = tag.getBoolean("hasTE") ? null : tag.getCompoundTag("tileEntity");
        return new PixelBlockSnapshot(new BlockPos(tag.getInteger("x"), tag.getInteger("y"), tag.getInteger("z")), tag.hasKey("blockMod") ? tag.getString("blockMod") : "minecraft", tag.getString(PIXEL_SNAPSHOT_BLOCK_NAME), tag.getInteger("metadata"), nbt);
    }

    public IBlockState getCurrentBlock() {
        return this.world.getBlockState(this.pos);
    }

    public TileEntity getTileEntity() {
        if (this.nbt != null) {
            return TileEntity.create(this.world, this.nbt);
        }
        return null;
    }

    public boolean restore() {
        return this.restore(false);
    }

    public boolean restore(boolean force) {
        return this.restore(force, true);
    }

    public boolean restore(boolean force, boolean applyPhysics) {
        Block replacedBlock;
        IBlockState current = this.getCurrentBlock();
        IBlockState replaced = this.getReplacedBlock();
        Block currentBlock = current.getBlock();
        if (currentBlock != (replacedBlock = replaced.getBlock()) || currentBlock.getMetaFromState(current) != replacedBlock.getMetaFromState(replaced)) {
            if (force) {
                this.world.setBlockState(this.pos, replaced, applyPhysics ? 3 : 2);
            } else {
                return false;
            }
        }
        this.world.setBlockState(this.pos, replaced, applyPhysics ? 3 : 2);
        this.world.notifyBlockUpdate(this.pos, current, replaced, applyPhysics ? 3 : 2);
        TileEntity te = null;
        if (this.nbt != null && (te = this.world.getTileEntity(this.pos)) != null) {
            te.readFromNBT(this.nbt);
        }
        return true;
    }

    public boolean restoreToLocation(World world, BlockPos pos, boolean force, boolean applyPhysics) {
        IBlockState current = this.getCurrentBlock();
        IBlockState replaced = this.getReplacedBlock();
        if (current.getBlock() != replaced.getBlock() || current.getBlock().getMetaFromState(current) != replaced.getBlock().getMetaFromState(replaced)) {
            if (force) {
                world.setBlockState(pos, replaced, applyPhysics ? 3 : 2);
            } else {
                return false;
            }
        }
        world.setBlockState(pos, replaced, applyPhysics ? 3 : 2);
        world.notifyBlockUpdate(pos, current, replaced, applyPhysics ? 3 : 2);
        TileEntity te = null;
        if (this.nbt != null && (te = world.getTileEntity(pos)) != null) {
            te.readFromNBT(this.nbt);
        }
        return true;
    }

    public void writeToNBT(NBTTagCompound compound) {
        if (!this.blockIdentifier.getNamespace().equals("minecraft")) {
            compound.setString("blockMod", this.blockIdentifier.getNamespace());
        }
        compound.setString(PIXEL_SNAPSHOT_BLOCK_NAME, this.blockIdentifier.getPath());
        compound.setInteger("x", this.pos.getX());
        compound.setInteger("y", this.pos.getY());
        compound.setInteger("z", this.pos.getZ());
        compound.setInteger("metadata", this.meta);
        compound.setBoolean("hasTE", this.nbt != null);
        if (this.nbt != null) {
            compound.setTag("tileEntity", this.nbt);
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        PixelBlockSnapshot other = (PixelBlockSnapshot)obj;
        if (!this.pos.equals(other.pos)) {
            return false;
        }
        if (this.meta != other.meta) {
            return false;
        }
        if (!(this.nbt == other.nbt || this.nbt != null && this.nbt.equals(other.nbt))) {
            return false;
        }
        if (!(this.world == other.world || this.world != null && this.world.equals(other.world))) {
            return false;
        }
        return this.blockIdentifier == other.blockIdentifier || this.blockIdentifier != null && this.blockIdentifier.equals(other.blockIdentifier);
    }

    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + this.pos.getX();
        hash = 73 * hash + this.pos.getY();
        hash = 73 * hash + this.pos.getZ();
        hash = 73 * hash + this.meta;
        hash = 73 * hash + (this.nbt != null ? this.nbt.hashCode() : 0);
        hash = 73 * hash + (this.world != null ? this.world.hashCode() : 0);
        hash = 73 * hash + (this.blockIdentifier != null ? this.blockIdentifier.hashCode() : 0);
        return hash;
    }

    public IBlockState getReplacedBlock() {
        if (this.replacedBlock == null) {
            IForgeRegistry<Block> blockRegistry = GameRegistry.findRegistry(Block.class);
            try {
                this.replacedBlock = blockRegistry.getValue(this.blockIdentifier).getStateFromMeta(this.meta);
            }
            catch (Exception e) {
                System.out.println("Cannot find Block " + this.blockIdentifier.getNamespace() + ": " + this.blockIdentifier.getPath());
            }
        }
        return this.replacedBlock;
    }

    public void restoreToLocationWithRotation(BlockPos pos, EnumFacing facing) {
        this.restoreToLocationWithRotation(pos, facing, null);
    }

    public void restoreToLocationWithRotation(BlockPos pos, EnumFacing facing, ItemStack replacement) {
        TileEntity te;
        IBlockState replaced = this.getReplacedBlock();
        replaced = this.checkOrientation(replaced, facing);
        try {
            this.world.setBlockState(pos, replaced, 3);
        }
        catch (ConcurrentModificationException e) {
            return;
        }
        this.world.notifyBlockUpdate(pos, replaced, replaced, 2);
        if (replaced.getBlock() instanceof BlockLever) {
            for (EnumFacing enumfacing : EnumFacing.values()) {
                this.world.notifyNeighborsOfStateChange(pos.offset(enumfacing), replaced.getBlock(), false);
            }
        }
        if (this.nbt != null && (te = this.world.getTileEntity(pos)) != null) {
            block8: {
                this.nbt.setInteger("x", pos.getX());
                this.nbt.setInteger("y", pos.getY());
                this.nbt.setInteger("z", pos.getZ());
                try {
                    te.readFromNBT(this.nbt);
                }
                catch (JsonSyntaxException e) {
                    if (!Pixelmon.devEnvironment) break block8;
                    e.printStackTrace();
                }
            }
            ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(pos);
            if (te instanceof TileEntityGymSign) {
                ((TileEntityGymSign)te).setDroppable(false);
                ((TileEntityGymSign)te).setItemInSign(replacement);
            }
        }
    }

    private IBlockState checkOrientation(IBlockState replaced, EnumFacing facing) {
        if (facing != EnumFacing.EAST) {
            ImmutableMap<IProperty<?>, Comparable<?>> properties = replaced.getProperties();
            if (properties.containsKey(BlockLever.FACING)) {
                BlockLever.EnumOrientation orientation = replaced.getValue(BlockLever.FACING);
                if (orientation == BlockLever.EnumOrientation.DOWN_X) {
                    if (facing == EnumFacing.NORTH || facing == EnumFacing.SOUTH) {
                        orientation = BlockLever.EnumOrientation.DOWN_Z;
                    }
                } else if (orientation == BlockLever.EnumOrientation.DOWN_Z) {
                    if (facing == EnumFacing.NORTH || facing == EnumFacing.SOUTH) {
                        orientation = BlockLever.EnumOrientation.DOWN_X;
                    }
                } else if (orientation == BlockLever.EnumOrientation.UP_X) {
                    if (facing == EnumFacing.NORTH || facing == EnumFacing.SOUTH) {
                        orientation = BlockLever.EnumOrientation.UP_Z;
                    }
                } else if (orientation == BlockLever.EnumOrientation.UP_Z) {
                    if (facing == EnumFacing.NORTH || facing == EnumFacing.SOUTH) {
                        orientation = BlockLever.EnumOrientation.UP_X;
                    }
                } else if (facing == EnumFacing.SOUTH) {
                    if (orientation == BlockLever.EnumOrientation.EAST) {
                        orientation = BlockLever.EnumOrientation.SOUTH;
                    } else if (orientation == BlockLever.EnumOrientation.SOUTH) {
                        orientation = BlockLever.EnumOrientation.WEST;
                    } else if (orientation == BlockLever.EnumOrientation.WEST) {
                        orientation = BlockLever.EnumOrientation.NORTH;
                    } else if (orientation == BlockLever.EnumOrientation.NORTH) {
                        orientation = BlockLever.EnumOrientation.EAST;
                    }
                } else if (facing == EnumFacing.WEST) {
                    if (orientation == BlockLever.EnumOrientation.EAST) {
                        orientation = BlockLever.EnumOrientation.WEST;
                    } else if (orientation == BlockLever.EnumOrientation.SOUTH) {
                        orientation = BlockLever.EnumOrientation.NORTH;
                    } else if (orientation == BlockLever.EnumOrientation.WEST) {
                        orientation = BlockLever.EnumOrientation.EAST;
                    } else if (orientation == BlockLever.EnumOrientation.NORTH) {
                        orientation = BlockLever.EnumOrientation.SOUTH;
                    }
                } else if (facing == EnumFacing.NORTH) {
                    if (orientation == BlockLever.EnumOrientation.EAST) {
                        orientation = BlockLever.EnumOrientation.NORTH;
                    } else if (orientation == BlockLever.EnumOrientation.SOUTH) {
                        orientation = BlockLever.EnumOrientation.EAST;
                    } else if (orientation == BlockLever.EnumOrientation.WEST) {
                        orientation = BlockLever.EnumOrientation.SOUTH;
                    } else if (orientation == BlockLever.EnumOrientation.NORTH) {
                        orientation = BlockLever.EnumOrientation.WEST;
                    }
                }
                replaced = replaced.withProperty(BlockLever.FACING, orientation);
            } else if (replaced.getBlock() instanceof BlockStandingSign) {
                int rotation = replaced.getValue(BlockStandingSign.ROTATION);
                if (facing == EnumFacing.WEST) {
                    rotation += 8;
                } else if (facing == EnumFacing.SOUTH) {
                    rotation += 4;
                } else if (facing == EnumFacing.NORTH) {
                    rotation += 12;
                }
                if (rotation > 15) {
                    rotation -= 16;
                }
                replaced = replaced.withProperty(BlockStandingSign.ROTATION, rotation);
            } else if (replaced.getBlock() == Blocks.VINE) {
                boolean east = false;
                boolean west = false;
                boolean south = false;
                boolean north = false;
                if (facing == EnumFacing.WEST) {
                    east = replaced.getValue(BlockVine.WEST);
                    west = replaced.getValue(BlockVine.EAST);
                    north = replaced.getValue(BlockVine.SOUTH);
                    south = replaced.getValue(BlockVine.NORTH);
                } else if (facing == EnumFacing.SOUTH) {
                    east = replaced.getValue(BlockVine.NORTH);
                    west = replaced.getValue(BlockVine.SOUTH);
                    north = replaced.getValue(BlockVine.WEST);
                    south = replaced.getValue(BlockVine.EAST);
                } else if (facing == EnumFacing.NORTH) {
                    east = replaced.getValue(BlockVine.SOUTH);
                    west = replaced.getValue(BlockVine.NORTH);
                    north = replaced.getValue(BlockVine.EAST);
                    south = replaced.getValue(BlockVine.WEST);
                }
                replaced = replaced.withProperty(BlockVine.EAST, east).withProperty(BlockVine.WEST, west).withProperty(BlockVine.SOUTH, south).withProperty(BlockVine.NORTH, north);
            } else {
                for (Map.Entry property : properties.entrySet()) {
                    EnumFacing orientation;
                    if (!(property.getValue() instanceof EnumFacing) || (orientation = (EnumFacing)replaced.getValue((IProperty)property.getKey())) == EnumFacing.UP || orientation == EnumFacing.DOWN) continue;
                    if (facing == EnumFacing.WEST) {
                        replaced = replaced.withProperty((IProperty)property.getKey(), orientation.rotateY().rotateY());
                        continue;
                    }
                    if (facing == EnumFacing.NORTH) {
                        replaced = replaced.withProperty((IProperty)property.getKey(), orientation.rotateYCCW());
                        continue;
                    }
                    if (facing != EnumFacing.SOUTH) continue;
                    replaced = replaced.withProperty((IProperty)property.getKey(), orientation.rotateY());
                }
            }
        }
        return replaced;
    }
}

