/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util.helper;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import net.minecraft.util.ResourceLocation;

public class RCFileHelper {
    public static String encodePath(String path) {
        return path.replaceAll(" ", "%20");
    }

    static Path resourceToPath(URL resource) throws IOException, URISyntaxException {
        FileSystem fs22;
        Objects.requireNonNull(resource, "Resource URL cannot be null");
        URI uri = resource.toURI();
        String scheme = uri.getScheme();
        if (scheme.equals("file")) {
            return Paths.get(uri);
        }
        if (!scheme.equals("jar")) {
            throw new IllegalArgumentException("Cannot convert to Path: " + uri);
        }
        String s = RCFileHelper.encodePath(uri.toString());
        int separator = s.indexOf("!/");
        String entryName = s.substring(separator + 2);
        URI fileURI = URI.create(s.substring(0, separator));
        try {
            fs22 = FileSystems.getFileSystem(fileURI);
            if (fs22.isOpen()) {
                return fs22.getPath(entryName, new String[0]);
            }
        }
        catch (FileSystemNotFoundException fileSystemNotFoundException) {
            // empty catch block
        }
        fs22 = FileSystems.newFileSystem(fileURI, Collections.emptyMap());
        return fs22.getPath(entryName, new String[0]);
    }

    public static Path pathFromResourceLocation(ResourceLocation resourceLocation) throws URISyntaxException, IOException {
        URL resource = RCFileHelper.class.getResource(String.format("/assets/%s/%s", resourceLocation.getNamespace(), resourceLocation.getPath()));
        return resource != null ? RCFileHelper.resourceToPath(resource.toURI().toURL()) : null;
    }

    public static List<Path> listFilesRecursively(Path dir, final DirectoryStream.Filter<Path> filter, final boolean recursive) throws IOException {
        final ArrayList<Path> files = new ArrayList<Path>();
        Files.walkFileTree(dir, (FileVisitor<? super Path>)new SimpleFileVisitor<Path>(){

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if (filter.accept(file)) {
                    files.add(file);
                }
                return recursive ? FileVisitResult.CONTINUE : FileVisitResult.SKIP_SUBTREE;
            }
        });
        return files;
    }

    public static File getValidatedFolder(File file, boolean create) {
        if (create && !file.exists() && !file.mkdir()) {
            System.out.println("Could not create " + file.getName() + " folder");
        }
        return file.exists() ? file : null;
    }

    public static File getValidatedFolder(File parent, String child, boolean create) {
        return RCFileHelper.getValidatedFolder(new File(parent, child), create);
    }
}

