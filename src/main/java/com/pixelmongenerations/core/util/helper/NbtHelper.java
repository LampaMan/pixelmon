/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util.helper;

import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.NBTLink;
import com.pixelmongenerations.core.enums.EnumSpecies;
import net.minecraft.nbt.NBTTagCompound;

public class NbtHelper {
    public static NBTTagCompound fixPokemonData(NBTTagCompound tag) {
        if (!tag.getBoolean("Converted")) {
            EnumSpecies species = EnumSpecies.getFromNameAnyCase(tag.getString("Name"));
            if (species == EnumSpecies.Aegislash && tag.getInteger("Variant") == 8) {
                tag.setInteger("Variant", 0);
                tag.setInteger("specialTexture", 1);
            } else if ((species == EnumSpecies.Noctowl || species == EnumSpecies.Hoothoot) && tag.getInteger("Variant") == 9) {
                tag.setInteger("Variant", -1);
                tag.setInteger("specialTexture", 1);
            } else if ((species == EnumSpecies.Meowth || species == EnumSpecies.Persian) && tag.getInteger("Variant") == 6) {
                tag.setInteger("Variant", -1);
                tag.setInteger("specialTexture", 1);
            } else if (species == EnumSpecies.Vulpix && tag.getInteger("Variant") == 9) {
                tag.setInteger("Variant", 3);
                tag.setInteger("specialTexture", 1);
                Moveset.loadMoveset(new NBTLink(tag)).writeToNBT(tag);
            } else if (tag.getInteger("specialTexture") > 0) {
                tag.setInteger("specialTexture", 1);
            }
            tag.setBoolean("Converted", true);
        }
        return tag;
    }
}

