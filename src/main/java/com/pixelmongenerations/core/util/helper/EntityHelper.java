/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util.helper;

import com.pixelmongenerations.client.models.animations.EnumRotation;
import com.pixelmongenerations.core.util.PixelmonDebug;
import com.pixelmongenerations.core.util.helper.VectorHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;

public class EntityHelper {
    public static double getMotion(Entity entity, EnumRotation axis) {
        switch (axis) {
            case x: {
                return entity.motionX;
            }
            case y: {
                return entity.motionY;
            }
            case z: {
                return entity.motionZ;
            }
        }
        return Double.NaN;
    }

    public static double getLocalMotion(Entity entity, EnumRotation axis) {
        if (axis == EnumRotation.y) {
            return entity.motionY;
        }
        double[] locals = VectorHelper.rotate(entity.motionX, entity.motionZ, -entity.rotationYaw * ((float)Math.PI / 180));
        return axis == EnumRotation.x ? (double)((float)locals[0]) : (double)((float)locals[1]);
    }

    public static void statePlayerLocalMotion() {
        EntityPlayerSP player = Minecraft.getMinecraft().player;
        double motionX = EntityHelper.getLocalMotion(player, EnumRotation.x);
        double motionZ = EntityHelper.getLocalMotion(player, EnumRotation.z);
        System.out.println("Player local motion = " + PixelmonDebug.listObjs(motionX, motionZ));
    }
}

