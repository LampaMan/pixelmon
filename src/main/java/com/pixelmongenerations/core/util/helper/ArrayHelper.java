/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.util.helper;

import com.pixelmongenerations.core.util.IEncodeable;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.List;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class ArrayHelper {
    public static <T> boolean contains(T[] array, T value) {
        if (array == null) {
            return false;
        }
        for (int i = 0; i < array.length; ++i) {
            if (array[i] != value && (array[i] == null || value == null || !array[i].equals(value))) continue;
            return true;
        }
        return false;
    }

    public static <T> boolean arrayHasNull(T[] array) {
        if (array == null || array.length == 0) {
            return true;
        }
        for (T object : array) {
            if (object != null) continue;
            return true;
        }
        return false;
    }

    public static <T> void validateArrayNonNull(T[] array) {
        if (ArrayHelper.arrayHasNull(array)) {
            throw new IllegalArgumentException("Array cannot have null elements.");
        }
    }

    public static void encodeArray(ByteBuf buffer, IEncodeable[] array) {
        if (array == null) {
            buffer.writeInt(0);
            return;
        }
        buffer.writeInt(array.length);
        for (IEncodeable encodeable : array) {
            encodeable.encodeInto(buffer);
        }
    }

    public static void encodeList(ByteBuf buffer, List<? extends IEncodeable> list) {
        if (list == null) {
            buffer.writeInt(0);
            return;
        }
        buffer.writeInt(list.size());
        for (IEncodeable iEncodeable : list) {
            iEncodeable.encodeInto(buffer);
        }
    }

    public static void encodeStringList(ByteBuf buffer, List<String> list) {
        if (list == null) {
            buffer.writeInt(0);
            return;
        }
        ArrayHelper.encodeStringArray(buffer, list.toArray(new String[list.size()]));
    }

    public static void encodeStringArray(ByteBuf buffer, String[] array) {
        if (array == null) {
            buffer.writeInt(0);
            return;
        }
        buffer.writeInt(array.length);
        for (String string : array) {
            if (string == null) {
                string = "";
            }
            ByteBufUtils.writeUTF8String(buffer, string);
        }
    }

    public static List<String> decodeStringList(ByteBuf buffer) {
        ArrayList<String> list = new ArrayList<String>();
        int listLength = buffer.readInt();
        for (int i = 0; i < listLength; ++i) {
            list.add(ByteBufUtils.readUTF8String(buffer));
        }
        return list;
    }

    public static String[] decodeStringArray(ByteBuf buffer) {
        String[] array = new String[buffer.readInt()];
        for (int i = 0; i < array.length; ++i) {
            array[i] = ByteBufUtils.readUTF8String(buffer);
        }
        return array;
    }
}

