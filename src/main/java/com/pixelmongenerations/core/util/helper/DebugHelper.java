/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util.helper;

import com.pixelmongenerations.core.enums.EnumSpecies;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;

public class DebugHelper {
    public static void langCheck(EntityPlayer player) {
        DebugHelper.checkMissingPokemonLangEntries(player.getEntityWorld());
    }

    private static void checkMissingPokemonLangEntries(World world) {
        for (EnumSpecies pokemon : EnumSpecies.values()) {
            String langEntryName = "pixelmon." + pokemon.name.toLowerCase() + ".name";
            if (!I18n.translateToLocal(langEntryName).equals(langEntryName)) continue;
            System.out.println("Pokemon " + pokemon.name + " Doesn't have a lang entry " + langEntryName);
        }
    }
}

