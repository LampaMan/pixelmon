/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util.functional;

import com.pixelmongenerations.core.util.functional.TriConsumer;
import com.pixelmongenerations.core.util.functional.TriPedicate;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

@FunctionalInterface
public interface TriFunction<A, B, C, D> {
    public D apply(A var1, B var2, C var3);

    default public <E> TriFunction<A, B, C, E> andThen(Function<? super D, ? extends E> after) {
        Objects.requireNonNull(after);
        return (a, b, c) -> after.apply((D)this.apply(a, b, c));
    }

    default public TriPedicate<A, B, C> predicate(Predicate<D> after) {
        Objects.requireNonNull(after);
        return (a, b, c) -> after.test(this.apply(a, b, c));
    }

    default public TriConsumer<A, B, C> consume(Consumer<D> after) {
        Objects.requireNonNull(after);
        return (a, b, c) -> after.accept(this.apply(a, b, c));
    }
}

