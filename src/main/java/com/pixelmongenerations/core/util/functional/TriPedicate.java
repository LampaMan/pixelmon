/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util.functional;

@FunctionalInterface
public interface TriPedicate<A, B, C> {
    public boolean test(A var1, B var2, C var3);
}

