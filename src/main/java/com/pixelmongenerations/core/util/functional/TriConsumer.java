/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util.functional;

public interface TriConsumer<A, B, C> {
    public void accept(A var1, B var2, C var3);
}

