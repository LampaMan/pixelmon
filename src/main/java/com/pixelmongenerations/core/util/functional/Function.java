/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util.functional;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;

@FunctionalInterface
public interface Function<A, B>
extends java.util.function.Function<A, B> {
    default public Predicate<A> predicate(Predicate<B> after) {
        Objects.requireNonNull(after);
        return a -> after.test(this.apply(a));
    }

    default public Consumer<A> consume(Consumer<B> after) {
        Objects.requireNonNull(after);
        return a -> after.accept(this.apply(a));
    }
}

