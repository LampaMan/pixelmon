/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util;

import com.pixelmongenerations.core.enums.EnumMovement;
import net.minecraft.entity.player.EntityPlayerMP;

public interface IMovementHandler {
    public void handleMovement(EntityPlayerMP var1, EnumMovement[] var2);
}

