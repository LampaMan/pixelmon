/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.util;

import io.netty.buffer.ByteBuf;

public interface IEncodeable {
    public void encodeInto(ByteBuf var1);

    public void decodeInto(ByteBuf var1);
}

