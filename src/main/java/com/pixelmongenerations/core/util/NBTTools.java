/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.Gson
 *  com.google.gson.GsonBuilder
 */
package com.pixelmongenerations.core.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTPrimitive;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.nbt.NBTTagFloat;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraftforge.common.util.BlockSnapshot;

public class NBTTools {
    public static final Gson gson = new GsonBuilder().create();

    public static String serializeBlockSnapshot(BlockSnapshot snapshot) {
        NBTTagCompound nbt = new NBTTagCompound();
        snapshot.writeToNBT(nbt);
        return gson.toJson(NBTTools.nbtToMap(nbt));
    }

    public static BlockSnapshot deserializeBlockSnapshot(String json) {
        Map map = (Map)gson.fromJson(json, Map.class);
        return BlockSnapshot.readFromNBT(NBTTools.nbtFromMap(map));
    }

    public static Map<String, Object> nbtToMap(NBTTagCompound nbt) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        for (String key : nbt.getKeySet()) {
            try {
                NBTBase base = nbt.getTag(key);
                if (base instanceof NBTTagString) {
                    map.put(key, ((NBTTagString)base).getString());
                    continue;
                }
                if (base instanceof NBTPrimitive) {
                    map.put(key, ((NBTPrimitive)base).getDouble());
                    continue;
                }
                if (!(base instanceof NBTTagCompound)) continue;
                map.put(key, NBTTools.nbtToMap((NBTTagCompound)base));
            }
            catch (Exception exception) {}
        }
        return map;
    }

    public static NBTTagCompound nbtFromMap(Map<String, Object> map) {
        NBTTagCompound nbt = new NBTTagCompound();
        for (String key : map.keySet()) {
            try {
                if (map.get(key) instanceof String) {
                    nbt.setString(key, (String)map.get(key));
                    continue;
                }
                if (map.get(key) instanceof Map) {
                    nbt.setTag(key, NBTTools.nbtFromMap((Map)map.get(key)));
                    continue;
                }
                Double d = (Double)map.get(key);
                if ((double)Math.round(d) == d) {
                    nbt.setInteger(key, (int)Math.round(d));
                    continue;
                }
                nbt.setDouble(key, d);
            }
            catch (Exception exception) {}
        }
        return nbt;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static NBTTagCompound loadNBT(File file) throws IOException {
        FileInputStream in = null;
        NBTTagCompound result = null;
        try {
            in = new FileInputStream(file);
            result = CompressedStreamTools.readCompressed(in);
        }
        catch (IOException e) {
            try {
                result = CompressedStreamTools.read(file);
            }
            finally {
                in.close();
            }
        }
        return result;
    }

    public static void saveNBT(NBTTagCompound nbt, File file, boolean compressed) throws IOException {
        file.getParentFile().mkdirs();
        file.createNewFile();
        NBTTools.saveNBT(nbt, new FileOutputStream(file), compressed);
    }

    public static void saveNBT(NBTTagCompound nbt, OutputStream out, boolean compressed) throws IOException {
        if (compressed) {
            CompressedStreamTools.writeCompressed(nbt, out);
        } else {
            CompressedStreamTools.write(nbt, new DataOutputStream(out));
        }
    }

    public static NBTTagList newDoubleNBTList(double ... numbers) {
        NBTTagList nbttaglist = new NBTTagList();
        for (double d0 : numbers) {
            nbttaglist.appendTag(new NBTTagDouble(d0));
        }
        return nbttaglist;
    }

    public static NBTTagList newFloatNBTList(float ... numbers) {
        NBTTagList nbttaglist = new NBTTagList();
        for (float f : numbers) {
            nbttaglist.appendTag(new NBTTagFloat(f));
        }
        return nbttaglist;
    }
}

