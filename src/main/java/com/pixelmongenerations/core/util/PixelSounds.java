/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.HashBasedTable
 *  com.google.common.collect.Maps
 *  com.google.common.collect.Streams
 *  com.google.common.collect.Table
 *  com.google.gson.JsonElement
 *  com.google.gson.JsonObject
 *  com.google.gson.JsonParser
 */
package com.pixelmongenerations.core.util;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Maps;
import com.google.common.collect.Streams;
import com.google.common.collect.Table;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.pixelmongenerations.api.Tuple;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.EnumCreators;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumForms;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class PixelSounds {
    public static SoundEvent pc = PixelSounds.create("pixelmon.block.PC");
    public static SoundEvent pokelootObtained = PixelSounds.create("pixelmon.block.PokelootObtained");
    public static SoundEvent cameraShutter = PixelSounds.create("pixelmon.item.CameraShutter");
    public static SoundEvent healerActive = PixelSounds.create("pixelmon.block.healerActivate");
    public static SoundEvent pokeballClose = PixelSounds.create("pixelmon.block.PokeballClose");
    public static SoundEvent pokeballRelease = PixelSounds.create("pixelmon.block.PokeballRelease");
    public static SoundEvent pokeballCapture = PixelSounds.create("pixelmon.block.PokeballCapture");
    public static SoundEvent pokeballCaptureSuccess = PixelSounds.create("pixelmon.block.PokeballCaptureSuccess");
    public static SoundEvent flute = PixelSounds.create("pixelmon.item.Flute");
    public static SoundEvent zygardeCell = PixelSounds.create("pixelmon.other.ZygardeCell");
    public static SoundEvent womrhole = PixelSounds.create("pixelmon.other.Wormhole");
    public static SoundEvent ui_click = PixelSounds.create("ui_click");
    public static SoundEvent get_item = PixelSounds.create("get_item");
    public static SoundEvent lava_crystal_shatter = PixelSounds.create("pixelmon.block.lava_crystal_shatter");
    public static SoundEvent lugia_shrine_song = PixelSounds.create("pixelmon.item.LugaShrineSong");
    public static SoundEvent meloettasRelicSong = PixelSounds.create("pixelmon.item.MeloettasRelicSong");
    public static SoundEvent track1 = PixelSounds.create("pixelmon.music.track1");
    public static SoundEvent track2 = PixelSounds.create("pixelmon.music.track2");
    public static SoundEvent track3 = PixelSounds.create("pixelmon.music.track3");
    public static SoundEvent track4 = PixelSounds.create("pixelmon.music.track4");
    public static SoundEvent track5 = PixelSounds.create("pixelmon.music.track5");
    public static SoundEvent track6 = PixelSounds.create("pixelmon.music.track6");
    public static SoundEvent track7 = PixelSounds.create("pixelmon.music.track7");
    public static SoundEvent track8 = PixelSounds.create("pixelmon.music.track8");
    public static SoundEvent track9 = PixelSounds.create("pixelmon.music.track9");
    public static SoundEvent track10 = PixelSounds.create("pixelmon.music.track10");
    public static SoundEvent track11 = PixelSounds.create("pixelmon.music.track11");
    public static SoundEvent battle1 = PixelSounds.create("pixelmon.music.battle1");
    public static SoundEvent battle2 = PixelSounds.create("pixelmon.music.battle2");
    public static SoundEvent battle3 = PixelSounds.create("pixelmon.music.battle3");
    public static SoundEvent battle4 = PixelSounds.create("pixelmon.music.battle4");
    public static SoundEvent battle5 = PixelSounds.create("pixelmon.music.battle5");
    public static SoundEvent battle6 = PixelSounds.create("pixelmon.music.battle6");
    public static SoundEvent battle7 = PixelSounds.create("pixelmon.music.battle7");
    public static SoundEvent battle8 = PixelSounds.create("pixelmon.music.battle8");
    public static SoundEvent battle9 = PixelSounds.create("pixelmon.music.battle9");
    public static SoundEvent battle10 = PixelSounds.create("pixelmon.music.battle10");
    public static SoundEvent day = PixelSounds.create("pixelmon.music.day");
    public static SoundEvent night = PixelSounds.create("pixelmon.music.night");
    public static Table<EnumSpecies, IEnumForm, Map<BaseStats.SoundType, SoundEvent>> pixelmonSounds;
    public static HashMap<EnumCreators, ArrayList<Tuple<EnumSpecies, SoundEvent>>> creatorSounds;

    public static void registerSounds(IForgeRegistry<SoundEvent> registry) {
        registry.registerAll(new SoundEvent[]{pc, pokelootObtained, cameraShutter, healerActive, pokeballClose, pokeballRelease, pokeballCapture, pokeballCaptureSuccess, flute, zygardeCell, womrhole, ui_click, get_item, lava_crystal_shatter, lugia_shrine_song, track1, track2, track3, track4, track5, track6, track7, track8, track9, track10, track11, battle1, battle2, battle3, battle4, battle5, battle6, battle7, battle8, battle9, battle10, day, night});
        pixelmonSounds = HashBasedTable.create();
        creatorSounds = new HashMap();
        PixelSounds.registerCries(registry);
    }

    private static SoundEvent create(String name) {
        ResourceLocation resourcelocation = new ResourceLocation("pixelmon:" + name);
        return (SoundEvent)new SoundEvent(resourcelocation).setRegistryName(resourcelocation);
    }

    private static SoundEvent registerSound(IForgeRegistry<SoundEvent> registry, String soundNameIn) {
        SoundEvent event = PixelSounds.create(soundNameIn);
        registry.register(event);
        return event;
    }

    private static void registerCries(IForgeRegistry<SoundEvent> registry) {
        String prefix = "pixelmon.mob.";
        InputStream istream = PixelSounds.class.getResourceAsStream("/assets/pixelmon/sounds.json");
        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(istream));
        JsonObject json = new JsonParser().parse((Reader)bufferedreader).getAsJsonObject();
        for (EnumSpecies enumSpecies : EnumSpecies.values()) {
            SoundEvent event;
            EnumMap map = Maps.newEnumMap(BaseStats.SoundType.class);
            JsonElement ele = json.get("pixelmon.mob." + enumSpecies.name.toLowerCase());
            if (ele != null) {
                event = PixelSounds.registerSound(registry, prefix + enumSpecies.name.toLowerCase());
                map.put(BaseStats.SoundType.Neutral, event);
            }
            if (json.get("pixelmon.mob." + enumSpecies.name.toLowerCase() + "F") != null) {
                event = PixelSounds.registerSound(registry, prefix + enumSpecies.name.toLowerCase() + "F");
                map.put(BaseStats.SoundType.Female, event);
            }
            if (json.get("pixelmon.mob." + enumSpecies.name.toLowerCase() + "M") != null) {
                event = PixelSounds.registerSound(registry, prefix + enumSpecies.name.toLowerCase() + "M");
                map.put(BaseStats.SoundType.Male, event);
            }
            for (IEnumForm form : Streams.concat(EnumSpecies.formList.get(enumSpecies).stream(), Stream.of(EnumForms.NoForm)).distinct().collect(Collectors.toList())) {
                pixelmonSounds.put(enumSpecies, form, map);
            }
        }
        for (Enum enum_ : EnumCreators.values()) {
            prefix = "pixelmon.creatormob." + enum_.name().toLowerCase() + ".";
            ArrayList<Tuple<EnumSpecies, SoundEvent>> voices = new ArrayList<Tuple<EnumSpecies, SoundEvent>>();
            for (Tuple<EnumSpecies, String> voice : ((EnumCreators)enum_).getVoices()) {
                SoundEvent event = PixelSounds.registerSound(registry, prefix + voice.getFirst().name.toLowerCase());
                voices.add(Tuple.of(voice.getFirst(), event));
            }
            creatorSounds.put((EnumCreators)enum_, voices);
        }
    }

    public static void linkPixelmonSounds() {
        for (EnumSpecies pixelmon : EnumSpecies.values()) {
            for (IEnumForm form : Streams.concat(EnumSpecies.formList.get(pixelmon).stream(), Stream.of(EnumForms.NoForm)).distinct().collect(Collectors.toList())) {
                Optional<BaseStats> statsOptional;
                if (!pixelmonSounds.contains((Object)pixelmon, (Object)form) || !(statsOptional = Entity3HasStats.getBaseStats(pixelmon.name, (int)form.getForm())).isPresent()) continue;
                BaseStats stats = statsOptional.get();
                Map map = (Map)pixelmonSounds.get((Object)pixelmon, (Object)form);
                if (map.containsKey((Object)BaseStats.SoundType.Neutral)) {
                    stats.addSound(BaseStats.SoundType.Neutral, (SoundEvent)map.get((Object)BaseStats.SoundType.Neutral));
                }
                if (map.containsKey((Object)BaseStats.SoundType.Female)) {
                    stats.addSound(BaseStats.SoundType.Female, (SoundEvent)map.get((Object)BaseStats.SoundType.Female));
                }
                if (!map.containsKey((Object)BaseStats.SoundType.Male)) continue;
                stats.addSound(BaseStats.SoundType.Male, (SoundEvent)map.get((Object)BaseStats.SoundType.Male));
            }
        }
    }

    public static Optional<SoundEvent> getSoundFor(EntityPixelmon pixelmon) {
        Map<BaseStats.SoundType, SoundEvent> soundMap;
        Map<IEnumForm, Map<BaseStats.SoundType, SoundEvent>> sounds = pixelmonSounds.row(pixelmon.getSpecies());
        if (sounds != null && (soundMap = sounds.get(pixelmon.getFormEnum())) != null) {
            Gender gender = EntityPixelmon.getRandomGender(pixelmon.baseStats);
            BaseStats.SoundType type = gender == Gender.Male ? BaseStats.SoundType.Male : (gender == Gender.Female ? BaseStats.SoundType.Female : (gender == Gender.None ? BaseStats.SoundType.Neutral : BaseStats.SoundType.Neutral));
            if (soundMap.containsKey(type)) {
                return Optional.of(soundMap.get(type));
            }
            if (soundMap.containsKey(BaseStats.SoundType.Neutral)) {
                return Optional.of(soundMap.get(BaseStats.SoundType.Neutral));
            }
        }
        return Optional.empty();
    }

    public static SoundEvent lookForCreatorSound(EnumSpecies species, String nickName) {
        EnumCreators creator = EnumCreators.getCreatorVoiceFor(species, nickName);
        if (creator == null) {
            return null;
        }
        ArrayList<Tuple<EnumSpecies, SoundEvent>> voices = creatorSounds.get((Object)creator);
        for (Tuple<EnumSpecies, SoundEvent> voice : voices) {
            if (voice.getFirst() != species) continue;
            return voice.getSecond();
        }
        return null;
    }
}

