/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Streams
 *  com.google.gson.JsonArray
 *  com.google.gson.JsonDeserializationContext
 *  com.google.gson.JsonDeserializer
 *  com.google.gson.JsonElement
 *  com.google.gson.JsonObject
 *  com.google.gson.JsonParseException
 *  com.google.gson.JsonSerializationContext
 *  com.google.gson.JsonSerializer
 */
package com.pixelmongenerations.core.util;

import com.google.common.collect.Streams;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import java.awt.Color;
import java.lang.reflect.Type;

public class Adapters {

    public static class PokemonGroupAdapter
    implements JsonSerializer<PokemonGroup>,
    JsonDeserializer<PokemonGroup> {
        public PokemonGroup deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return new PokemonGroup(Streams.stream(json.getAsJsonArray()).map(a -> context.deserialize(a, PokemonGroup.PokemonData.class)));
        }

        public JsonElement serialize(PokemonGroup src, Type typeOfSrc, JsonSerializationContext context) {
            JsonArray array = new JsonArray();
            src.getMembers().stream().map(((JsonSerializationContext)context)::serialize).forEachOrdered(((JsonArray)array)::add);
            return array;
        }
    }

    public static class PokemonDataAdapter
    implements JsonSerializer<PokemonGroup.PokemonData>,
    JsonDeserializer<PokemonGroup.PokemonData> {
        public PokemonGroup.PokemonData deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            EnumSpecies species = EnumSpecies.getFromNameAnyCase(json.getAsJsonObject().getAsJsonPrimitive("species").getAsString());
            IEnumForm form = null;
            if (json.getAsJsonObject().has("form")) {
                form = species.getFormEnum(json.getAsJsonObject().getAsJsonPrimitive("form").getAsInt());
            }
            return new PokemonGroup.PokemonData(species, form);
        }

        public JsonElement serialize(PokemonGroup.PokemonData src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject object = new JsonObject();
            object.addProperty("species", src.getSpecies().name);
            if (src.getForm() != null) {
                object.addProperty("form", (Number)src.getForm().getForm());
            }
            return object;
        }
    }

    static class ColorAdapter
    implements JsonSerializer<Color>,
    JsonDeserializer<Color> {
        ColorAdapter() {
        }

        public Color deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return Color.decode((String)context.deserialize(json, String.class));
        }

        public JsonElement serialize(Color src, Type typeOfSrc, JsonSerializationContext context) {
            return context.serialize((Object)("#" + Integer.toHexString(src.getRGB()).toUpperCase()));
        }
    }
}

