/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.data.asset.entry;

import com.pixelmongenerations.core.data.asset.type.SoundType;

public class SoundAsset {
    public String soundName;
    public String soundId;
    public SoundType soundType;
    public String soundURL;
    public boolean preLoad;

    public String toString() {
        return "SoundEntry [soundName=" + this.soundName + ", soundId=" + this.soundId + ", soundType=" + (Object)((Object)this.soundType) + ", soundURL=" + this.soundURL + "]";
    }
}

