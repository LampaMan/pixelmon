/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.data.asset.type;

public enum TextureType {
    BACKGROUND,
    BLOCK,
    COSMETIC,
    NPC,
    PARTICLE,
    SPECIAL_TEXTURE,
    SPECIAL_TEXTURE_SPRITE,
    BOARD,
    POPUP,
    WELCOME,
    CAPE,
    ITEM;

}

