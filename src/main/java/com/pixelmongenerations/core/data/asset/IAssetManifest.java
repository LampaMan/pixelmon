/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.Gson
 */
package com.pixelmongenerations.core.data.asset;

import com.google.gson.Gson;
import com.pixelmongenerations.core.network.packetHandlers.SyncManifest;
import java.util.List;

public interface IAssetManifest<T> {
    public static final Gson GSON = new Gson();

    public boolean addAsset(T var1);

    public List<T> getAssets();

    public void setAssets(List<T> var1);

    public SyncManifest.AssetManifestType getType();

    public String toJson();

    public void loadFromJson(String var1);
}

