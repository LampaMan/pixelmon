/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.data.asset;

import com.pixelmongenerations.core.data.asset.AssetState;

public interface IDynamicAsset {
    public AssetState getState();

    public void setState(AssetState var1);

    public String getKey();

    public String getName();

    public void formURLs();

    public void download();

    public void load();

    public void unload();
}

