/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.data.asset;

public interface ManifestTask<A> {
    public void formBuffer(A var1);

    public int getRemainingItems();
}

