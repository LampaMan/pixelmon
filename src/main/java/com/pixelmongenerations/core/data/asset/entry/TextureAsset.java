/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.data.asset.entry;

import com.pixelmongenerations.core.data.asset.type.TextureType;

public class TextureAsset {
    public TextureType textureType;
    public String textureName;
    public String textureId;
    public String textureURL;
    public String glowURL;
    public boolean glowRainbow = false;
    public boolean override = false;
    public boolean preLoad;

    public String toString() {
        return "TextureEntry [textureType=" + (Object)((Object)this.textureType) + ", textureName=" + this.textureName + ", textureId=" + this.textureId + ", textureURL=" + this.textureURL + ", glowURL=" + this.glowURL + ", glowRainbow=" + this.glowRainbow + ", override=" + this.override + "]";
    }
}

