/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.data.asset;

import com.pixelmongenerations.core.data.asset.AssetState;
import com.pixelmongenerations.core.data.asset.IDynamicAsset;
import java.time.Instant;

public class DynamicAsset<A>
implements IDynamicAsset {
    public final A entry;
    private AssetState state;
    public Instant lastAccessed;

    public DynamicAsset(A entry) {
        this.entry = entry;
        this.lastAccessed = Instant.now();
    }

    @Override
    public AssetState getState() {
        return this.state;
    }

    @Override
    public void setState(AssetState state) {
        this.state = state;
    }

    @Override
    public void download() {
    }

    @Override
    public void formURLs() {
    }

    @Override
    public String getKey() {
        return "NonOverridedEntry";
    }

    @Override
    public String getName() {
        return "NonOverridedEntry";
    }

    @Override
    public void load() {
    }

    @Override
    public void unload() {
    }
}

