/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.data.asset.manifest;

import com.pixelmongenerations.core.data.asset.IAssetManifest;
import com.pixelmongenerations.core.data.asset.entry.SoundAsset;
import com.pixelmongenerations.core.network.packetHandlers.SyncManifest;
import java.util.ArrayList;
import java.util.List;

public class SoundManifest
implements IAssetManifest<SoundAsset> {
    private List<SoundAsset> resources = new ArrayList<SoundAsset>();

    @Override
    public SyncManifest.AssetManifestType getType() {
        return SyncManifest.AssetManifestType.SOUND;
    }

    @Override
    public boolean addAsset(SoundAsset resource) {
        return this.resources.add(resource);
    }

    @Override
    public List<SoundAsset> getAssets() {
        return this.resources;
    }

    @Override
    public void setAssets(List<SoundAsset> resources) {
        this.resources = resources;
    }

    @Override
    public String toJson() {
        return GSON.toJson((Object)this);
    }

    @Override
    public void loadFromJson(String json) {
        this.setAssets(((SoundManifest)GSON.fromJson(json, SoundManifest.class)).getAssets());
    }
}

