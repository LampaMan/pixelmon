/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.data.asset.manifest;

import com.pixelmongenerations.core.data.asset.IAssetManifest;
import com.pixelmongenerations.core.data.asset.entry.ValveModelAsset;
import com.pixelmongenerations.core.network.packetHandlers.SyncManifest;
import java.util.ArrayList;
import java.util.List;

public class ValveModelManifest
implements IAssetManifest<ValveModelAsset> {
    private List<ValveModelAsset> resources = new ArrayList<ValveModelAsset>();

    @Override
    public SyncManifest.AssetManifestType getType() {
        return SyncManifest.AssetManifestType.VALVE_MODEL;
    }

    @Override
    public boolean addAsset(ValveModelAsset resource) {
        return this.resources.add(resource);
    }

    @Override
    public List<ValveModelAsset> getAssets() {
        return this.resources;
    }

    @Override
    public void setAssets(List<ValveModelAsset> resources) {
        this.resources = resources;
    }

    @Override
    public String toJson() {
        return GSON.toJson((Object)this);
    }

    @Override
    public void loadFromJson(String json) {
        this.setAssets(((ValveModelManifest)GSON.fromJson(json, ValveModelManifest.class)).getAssets());
    }
}

