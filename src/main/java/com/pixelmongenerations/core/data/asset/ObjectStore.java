/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.data.asset;

public interface ObjectStore<T> {
    public void addObject(String var1, T var2);

    public T getObject(String var1);

    public void checkForExpiredObjects();
}

