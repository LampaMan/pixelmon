/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.Gson
 *  com.google.gson.GsonBuilder
 */
package com.pixelmongenerations.core.data.trainers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.spawning.SpawnRegistry;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.data.trainers.TrainerSpawn;
import com.pixelmongenerations.core.enums.EnumNPCType;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;

public class TrainerSpawns {
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private static HashMap<EnumNPCType, ArrayList<TrainerSpawn>> SPAWNS;

    public static void setup() throws FileNotFoundException {
        File baseDir = new File(Pixelmon.modDirectory + "/pixelmon");
        String path = Pixelmon.modDirectory + "/pixelmon/npcs/";
        File npcsDir = new File(path);
        if (PixelmonConfig.useExternalJSONFilesNPCs) {
            baseDir.mkdir();
            Pixelmon.LOGGER.info("Creating NPCs spawns.");
            TrainerSpawns.extract(npcsDir);
        }
        InputStream istream = !PixelmonConfig.useExternalJSONFilesNPCs ? ServerNPCRegistry.class.getResourceAsStream("/assets/pixelmon/npcs/spawns.json") : new FileInputStream(new File(npcsDir, "spawns.json"));
        InputStream iStream = ServerNPCRegistry.class.getResourceAsStream("/assets/pixelmon/npcs/spawns.json");
        try {
            SPAWNS = ((SpawnSet)GSON.fromJson((Reader)new InputStreamReader(iStream), SpawnSet.class)).spawns;
            iStream.close();
            Pixelmon.LOGGER.info("Loaded Trainer Spawns");
        }
        catch (Exception e) {
            SPAWNS = new HashMap();
            Pixelmon.LOGGER.error("Failed to setup NPC Pokemon Trades");
            e.printStackTrace();
        }
        for (EnumNPCType type : SPAWNS.keySet()) {
            SPAWNS.get((Object)type).forEach(spawn -> SpawnRegistry.addNPCSpawn(spawn.name, spawn.rarity, type, spawn.biomeIds));
        }
    }

    private static void extract(File dataDir) {
        ServerNPCRegistry.extractFile("/assets/pixelmon/npcs/spawns.json", dataDir, "spawns.json");
    }

    private class SpawnSet {
        private HashMap<EnumNPCType, ArrayList<TrainerSpawn>> spawns;

        private SpawnSet() {
        }
    }
}

