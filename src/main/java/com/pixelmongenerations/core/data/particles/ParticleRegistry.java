/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.data.particles;

import java.util.HashMap;
import java.util.Set;

public class ParticleRegistry {
    private static ParticleRegistry INSTANCE;
    private HashMap<String, String> particles = new HashMap();
    public static final String FLOWERS = "flowers";
    public static final String POKE_HEARTS = "pokehearts";
    public static final String MUSICAL_FLOWERS = "musicalflowers";
    public static final String TWO_HEARTS = "twohearts";
    public static final String HEART = "heart";
    public static final String SHINY = "shiny";
    public static final String ULTRA_SHINY = "ultrashiny";
    public static final String GHOST = "ghost";
    public static final String JACKOLANTERN = "jackolantern";
    public static final String ZUBAT = "zubat";
    public static final String SPELL_TAG = "spelltag";
    public static final String SPIDER_WEB = "spiderweb";
    public static final String BAUBLE = "bauble";
    public static final String CANDYCANE = "candycane";
    public static final String CHRISTMASTREE = "christmastree";
    public static final String HOLLY = "holly";
    public static final String STOCKING = "stocking";
    public static final String TWINKLELIGHTS = "twinklelights";
    public static final String SNOWFLAKE1 = "snowflake1";
    public static final String SNOWFLAKE2 = "snowflake2";
    public static final String SNOWFLAKE3 = "snowflake3";
    public static final String SNOWFLAKE4 = "snowflake4";
    public static final String SNOWFLAKE5 = "snowflake5";
    public static final String SNOWFLAKE6 = "snowflake6";
    public static final String SNOWFLAKE7 = "snowflake7";
    public static final String SNOWFLAKE8 = "snowflake8";
    public static final String SNOWFLAKE9 = "snowflake9";
    public static final String KRABBY = "krabby";
    public static final String LEAF = "leaf";
    public static final String PETAL = "petal";
    public static final String SHADOW = "shadow";
    public static final String PURE = "pure";
    public static final String CLOVER = "clover";
    public static final String COINS = "coins";
    public static final String EGG_TORCHIC = "eggtorchic";
    public static final String FLOWER_BUTTERFREE = "flowerbutterfree";
    public static final String HONEY = "honey";
    public static final String HORSESHOES = "horseshoes";
    public static final String RAINBOW = "rainbow";
    public static final String SHADOW_FIRE = "shadowfire";
    public static final String PURIFIED = "purified";
    public static final String PUMPKIN = "pumpkin";
    public static final String MEGA = "mega";
    public static final String GO_CANDY = "gocandy";
    public static final String BATTLE = "battle";

    public ParticleRegistry() {
        this.registerParticle(FLOWERS, FLOWERS);
        this.registerParticle(GHOST, GHOST);
        this.registerParticle(HEART, HEART);
        this.registerParticle(JACKOLANTERN, JACKOLANTERN);
        this.registerParticle(KRABBY, KRABBY);
        this.registerParticle(MUSICAL_FLOWERS, MUSICAL_FLOWERS);
        this.registerParticle(POKE_HEARTS, POKE_HEARTS);
        this.registerParticle(SHINY, SHINY);
        this.registerParticle(SPELL_TAG, SPELL_TAG);
        this.registerParticle(SPIDER_WEB, SPIDER_WEB);
        this.registerParticle(TWO_HEARTS, TWO_HEARTS);
        this.registerParticle(ULTRA_SHINY, ULTRA_SHINY);
        this.registerParticle(BAUBLE, BAUBLE);
        this.registerParticle(CANDYCANE, CANDYCANE);
        this.registerParticle(CHRISTMASTREE, CHRISTMASTREE);
        this.registerParticle(STOCKING, STOCKING);
        this.registerParticle(TWINKLELIGHTS, TWINKLELIGHTS);
        this.registerParticle(SNOWFLAKE1, SNOWFLAKE1);
        this.registerParticle(SNOWFLAKE2, SNOWFLAKE2);
        this.registerParticle(SNOWFLAKE3, SNOWFLAKE3);
        this.registerParticle(SNOWFLAKE4, SNOWFLAKE4);
        this.registerParticle(SNOWFLAKE5, SNOWFLAKE5);
        this.registerParticle(SNOWFLAKE6, SNOWFLAKE6);
        this.registerParticle(SNOWFLAKE7, SNOWFLAKE7);
        this.registerParticle(SNOWFLAKE8, SNOWFLAKE8);
        this.registerParticle(SNOWFLAKE9, SNOWFLAKE9);
        this.registerParticle(LEAF, LEAF);
        this.registerParticle(PETAL, PETAL);
        this.registerParticle(SHADOW, SHADOW);
        this.registerParticle(PURE, PURE);
        this.registerParticle(CLOVER, CLOVER);
        this.registerParticle(COINS, COINS);
        this.registerParticle(EGG_TORCHIC, EGG_TORCHIC);
        this.registerParticle(FLOWER_BUTTERFREE, FLOWER_BUTTERFREE);
        this.registerParticle(HONEY, HONEY);
        this.registerParticle(HORSESHOES, HORSESHOES);
        this.registerParticle(RAINBOW, RAINBOW);
        this.registerParticle(SHADOW_FIRE, SHADOW_FIRE);
        this.registerParticle(PURIFIED, PURIFIED);
        this.registerParticle(PUMPKIN, PUMPKIN);
        this.registerParticle(MEGA, MEGA);
        this.registerParticle(GO_CANDY, GO_CANDY);
        this.registerParticle(BATTLE, BATTLE);
    }

    public void registerParticle(String id, String textureName) {
        this.particles.put(id, textureName);
    }

    public boolean particleExists(String id) {
        return this.particles.containsKey(id);
    }

    public Set<String> getAllParticles() {
        return this.particles.keySet();
    }

    public static ParticleRegistry getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ParticleRegistry();
        }
        return INSTANCE;
    }
}

