/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.data.trades;

import com.pixelmongenerations.core.enums.EnumSpecies;

public class PokemonTrade {
    private EnumSpecies wants;
    private EnumSpecies gives;

    public PokemonTrade(EnumSpecies wants, EnumSpecies gives) {
        this.wants = wants;
        this.gives = gives;
    }

    public EnumSpecies getWantedSpecies() {
        return this.wants;
    }

    public EnumSpecies getGivenSpecies() {
        return this.gives;
    }
}

