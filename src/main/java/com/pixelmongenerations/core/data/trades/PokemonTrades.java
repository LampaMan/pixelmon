/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 *  com.google.gson.Gson
 *  com.google.gson.GsonBuilder
 */
package com.pixelmongenerations.core.data.trades;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixelmongenerations.api.events.npc.NPCTradesEvent;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.data.trades.PokemonTrade;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import net.minecraftforge.common.MinecraftForge;

public class PokemonTrades {
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private static final Random RAND = new Random();
    private static final PokemonTrade DEFAULT_TRADE = new PokemonTrade(EnumSpecies.Rattata, EnumSpecies.Krabby);
    private static ArrayList<PokemonTrade> TRADES = new ArrayList();

    public static void setup() throws FileNotFoundException {
        String path = Pixelmon.modDirectory + "/pixelmon/";
        File baseDir = new File(Pixelmon.modDirectory + "/pixelmon");
        if (PixelmonConfig.useExternalJSONFilesTrades) {
            if (!baseDir.isDirectory()) {
                baseDir.mkdir();
                Pixelmon.LOGGER.info("Creating Pixelmon directory.");
            }
            PokemonTrades.extract(baseDir);
        }
        InputStream iStream = !PixelmonConfig.useExternalJSONFilesTrades ? ServerNPCRegistry.class.getResourceAsStream("/assets/pixelmon/npcs/traders.json") : new FileInputStream(new File(baseDir, "traders.json"));
        try {
            TRADES = ((TradeSet)GSON.fromJson((Reader)new InputStreamReader(iStream), TradeSet.class)).trades;
            iStream.close();
            NPCTradesEvent event = new NPCTradesEvent(TRADES);
            MinecraftForge.EVENT_BUS.post(event);
            TRADES = event.getTrades();
            Pixelmon.LOGGER.info("Loaded Pokemon Trades");
        }
        catch (Exception e) {
            TRADES = new ArrayList();
            Pixelmon.LOGGER.error("Failed to setup NPC Pokemon Trades");
            e.printStackTrace();
        }
    }

    public static List<EnumSpecies> getRandomTrade() {
        PokemonTrade trade = !TRADES.isEmpty() ? TRADES.get(RAND.nextInt(TRADES.size())) : DEFAULT_TRADE;
        return Lists.newArrayList(trade.getWantedSpecies(), trade.getGivenSpecies());
    }

    private static void extract(File dataDir) {
        ServerNPCRegistry.extractFile("/assets/pixelmon/npcs/traders.json", dataDir, "traders.json");
    }

    private class TradeSet {
        private ArrayList<PokemonTrade> trades;

        private TradeSet() {
        }
    }
}

