/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.plugin;

import java.util.ArrayList;

public class LibraryManifest {
    private ArrayList<PixelmonLibrary> libraries;

    public ArrayList<PixelmonLibrary> getLibraries() {
        return this.libraries;
    }

    public static class PixelmonLibrary {
        private String source;
        private String hash;
        private boolean devEnv;

        public String getSource() {
            return this.source;
        }

        public String getHash() {
            return this.hash;
        }

        public boolean isUsedInDev() {
            return this.devEnv;
        }

        public String getName() {
            return this.source.split("/")[4];
        }
    }
}

