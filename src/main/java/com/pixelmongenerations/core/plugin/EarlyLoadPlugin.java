/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.apache.logging.log4j.Level
 */
package com.pixelmongenerations.core.plugin;

import com.pixelmongenerations.core.plugin.LibraryDownloader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import net.minecraftforge.fml.common.FMLLog;
import net.minecraftforge.fml.relauncher.IFMLCallHook;
import net.minecraftforge.fml.relauncher.IFMLLoadingPlugin;
import org.apache.logging.log4j.Level;

@IFMLLoadingPlugin.SortingIndex(value=-2147483648)
public class EarlyLoadPlugin
implements IFMLLoadingPlugin,
IFMLCallHook {
    private static boolean hasLoaded = false;

    @Override
    public Void call() throws Exception {
        FMLLog.log("Pixelmon Early-Init", Level.INFO, "Starting setup...", new Object[0]);
        try {
            LibraryDownloader.init();
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    @Override
    public String[] getASMTransformerClass() {
        String[] asmStrings = new String[]{"com.pixelmongenerations.core.plugin.PixelmonTransformer"};
        if (!hasLoaded) {
            List<String> asm = Arrays.asList(asmStrings);
            for (String s : asm) {
                try {
                    Class<?> c = Class.forName(s);
                    if (c == null) continue;
                    System.out.println("Successfully Registered Transformer");
                }
                catch (Exception ex) {
                    System.out.println("Error while running transformer " + s);
                    return null;
                }
            }
            hasLoaded = true;
        }
        return asmStrings;
    }

    @Override
    public String getModContainerClass() {
        return "com.pixelmongenerations.core.plugin.DummyContainer";
    }

    @Override
    public String getSetupClass() {
        return this.getClass().getName();
    }

    @Override
    public void injectData(Map<String, Object> data) {
    }

    @Override
    public String getAccessTransformerClass() {
        return null;
    }
}

