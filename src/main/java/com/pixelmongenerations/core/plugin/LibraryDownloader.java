/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.Gson
 *  net.minecraft.launchwrapper.Launch
 *  org.apache.logging.log4j.Level
 */
package com.pixelmongenerations.core.plugin;

import com.google.gson.Gson;
import com.pixelmongenerations.core.plugin.LibraryManifest;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import net.minecraft.launchwrapper.Launch;
import net.minecraftforge.fml.common.FMLLog;
import org.apache.logging.log4j.Level;

public class LibraryDownloader {
    public static void init() {
        String manifestJson;
        File modsDir = new File("./mods/");
        modsDir.mkdirs();
        File ignoreFile = new File(modsDir, "skiplibraries.txt");
        File skipBop = new File(modsDir, "skipbopdownload.txt");
        boolean doSkipBop = false;
        if (ignoreFile.exists()) {
            return;
        }
        if (skipBop.exists()) {
            doSkipBop = true;
        } else {
            for (File file : modsDir.listFiles()) {
                if (!file.getName().toLowerCase().contains("biomesoplenty")) continue;
                doSkipBop = true;
                break;
            }
        }
        Gson gson = new Gson();
        try {
            manifestJson = LibraryDownloader.queryForString("https://pixelmongenerations.com/libraries/manifest.json");
        }
        catch (Exception e) {
            FMLLog.log("Pixelmon Early-Init", Level.FATAL, "Failed to download libraries. Game will crash if missing.", new Object[0]);
            e.printStackTrace();
            return;
        }
        boolean developerEnvironment = (Boolean)Launch.blackboard.get("fml.deobfuscatedEnvironment");
        LibraryManifest manifest = (LibraryManifest)gson.fromJson(manifestJson, LibraryManifest.class);
        for (LibraryManifest.PixelmonLibrary library : manifest.getLibraries()) {
            if (developerEnvironment && !library.isUsedInDev() || library.getName().toLowerCase().contains("biomesoplenty") && doSkipBop) continue;
            String libraryName = library.getName();
            FMLLog.log("Pixelmon Early-Init", Level.INFO, "Checking for " + libraryName, new Object[0]);
            File modFile = new File(modsDir, library.getName());
            if (modFile.exists()) continue;
            try {
                FMLLog.log("Pixelmon Early-Init", Level.INFO, "Downloading " + libraryName, new Object[0]);
                ReadableByteChannel readableByteChannel = Channels.newChannel(new URL(library.getSource()).openStream());
                FileOutputStream fileOutputStream = new FileOutputStream(modFile);
                FileChannel fileChannel = fileOutputStream.getChannel();
                fileOutputStream.getChannel().transferFrom(readableByteChannel, 0L, Long.MAX_VALUE);
                fileOutputStream.flush();
                fileOutputStream.close();
                FMLLog.log("Pixelmon Early-Init", Level.INFO, "Downloaded " + libraryName, new Object[0]);
            }
            catch (Exception exception) {
                FMLLog.log("Pixelmon Early-Init", Level.FATAL, "Exception when downloading " + libraryName, new Object[0]);
                exception.printStackTrace();
            }
        }
    }

    private static String queryForString(String address) throws Exception {
        String inputLine;
        System.setProperty("http.agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.11 Safari/537.36");
        URL url = new URL(address);
        HttpURLConnection con = (HttpURLConnection)url.openConnection();
        con.setRequestMethod("GET");
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        StringBuilder content = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        con.disconnect();
        return content.toString();
    }
}

